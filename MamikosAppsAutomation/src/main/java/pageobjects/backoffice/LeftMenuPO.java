package pageobjects.backoffice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;


public class LeftMenuPO {
    WebDriver driver;
    @FindBy(css = "a[href='/backoffice/contract/search']")
    private WebElement searchContractMenu;

    @FindBy(css = "a[href='/admin/account/verification-identity-card")
    private WebElement identityCard;

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    public LeftMenuPO(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /**
     * Click on Admin Log Menu From Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnSearchContractMenu() throws InterruptedException {
        searchContractMenu.click();
    }

    /**
     * Click on Admin Log Menu From Left Bar
     *Verification Identitiy Card
     * @throws InterruptedException
     */
    public void clickOnUserVerificationIdentitiyCard() throws InterruptedException {
        identityCard.click();
    }
}
