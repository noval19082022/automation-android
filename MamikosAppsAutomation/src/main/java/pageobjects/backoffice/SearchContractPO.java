package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Constants;
import utilities.JavaHelpers;

import java.util.List;


public class SearchContractPO {

    JavaHelpers java;
    WebDriver driver;
    @FindBy(className = "box-title")
    private WebElement searchContractHeader;

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @FindBy(css = "input[name='search_value']")
    private WebElement searchTextBox;

    @FindBy(css = "input[type='submit']")
    private WebElement searchContractButton;
    
    private By bookingStatus = By.xpath("//*[@class='tools-contract__action-column']/preceding-sibling::td/span[contains(@class, 'custom-label')]");

    @FindBy(css = "a[class='btn btn-xs btn-warning']")
    private WebElement bookingCancelButton;

    @FindBy(xpath = "(//tbody/tr)[1]")
    private WebElement firstContractList;

    @FindBy(name = "search_by")
    private WebElement searchByDropdownlist;

    @FindBy(xpath = "//Li/a[contains(text(),'Batalkan Kontrak')]")
    private WebElement terminateContractButton;

    @FindBy(xpath = "//input[@placeholder='Masukkan tanggal checkout']")
    private WebElement inputTanggalCheckoutTerminateDate;

    @FindBy(css = "input[value='Akhiri Kontrak']")
    private WebElement terminateContractPopUpButton;

    @FindBy(css = ".callout-success")
    private WebElement calloutSucessAction;

    @FindBy(xpath = "//input[@placeholder='Ketik User Profile atau Email']")
    private WebElement searchTextBoxVerificationAccount;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement searchIconButton;

    @FindBy(xpath = "(//a[@title='Reject'])[1]")
    private WebElement rejectButton;

    @FindBy(xpath = "//select[@onchange='onChangeRejectionOption(this); return false;']")
    private WebElement rejectReasonDropdown;

    @FindBy(xpath = "//option[@value='Gambar Tidak Jelas']")
    private WebElement chooseReason;

    @FindBy(xpath = "//input[@value='Submit Rejection']")
    private WebElement submitButton;

    private By verificationStatus = By.xpath("(//td[contains(text(),'FALSE')])[1]/following::span[1]");

    public SearchContractPO(WebDriver driver) {
        this.driver = driver;
        java = new JavaHelpers();

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /**
     * Enter Search Text in to Search box
     *
     * @param searchText search text
     * @throws InterruptedException
     */
    public void enterTextToSearchTextbox(String searchText) throws InterruptedException {
        searchTextBox.click();
        searchTextBox.sendKeys(searchText);
    }

    /**
     * Click On Search Contract Button
     *
     * @throws InterruptedException
     */
    public void clickOnSearchContractButton() throws InterruptedException {
        searchContractButton.click();
    }

    /**
     * Get Status Value of Searched Booking
     *
     * @return Booking Status
     */
    public List<WebElement> getBookingStatus() {

        return driver.findElements(bookingStatus);
    }

    /**
     * Click On Booking Cancel Button
     *
     * @throws InterruptedException
     */
    public void clickOnBookingCancelButton() throws InterruptedException {

        String attributeValue = bookingCancelButton.getAttribute("href");
        int cid = java.extractNumber(attributeValue);
        String javaScript = "window.location.href = '/backoffice/contract/cancel/" + cid + "';";
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript(javaScript);
        Thread.sleep(3000);
    }

    /**
     * Terminate contract by today date
     * @throws InterruptedException
     */
    public void terminateContractByTodayDate() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        String todayDateHour = java.getTimeStamp("YYYY-MM-dd") + " " + java.getTimeStamp("HH:mm:ss");
        Thread.sleep(2000);
        if (wait.until(ExpectedConditions.visibilityOf(terminateContractButton)) != null) {
            terminateContractButton.click();
            if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
                driver.switchTo().alert().accept();
            }
            else {
                inputTanggalCheckoutTerminateDate.clear();
                inputTanggalCheckoutTerminateDate.sendKeys(todayDateHour);
                Thread.sleep(2000);
                driver.findElement(By.cssSelector(".xdsoft_time.xdsoft_today")).click();
                terminateContractPopUpButton.click();
            }
            wait.until(ExpectedConditions.visibilityOf(calloutSucessAction));
        }
    }

    /**
     * Click On Terminate Button and accept alert
     */
    public void clickOnCancelContractButton() {
        if (this.waitInCaseElementVisible(bookingCancelButton, 1) != null){
            this.javascriptClickOn(bookingCancelButton);
            this.waitTillAlertPresent();
            driver.switchTo().alert().accept();
        }
    }

    /**
     * Get ID contract
     * @return ID contract
     */
    public String getIDContract() throws InterruptedException {
        Thread.sleep( 2000);
        return this.getElementAttributeValue(firstContractList, "contract-id");
    }

    /**
     * Wait for specified duration and check if element is visible or not
     * @param e WebElement object
     * @param duration wait duration in seconds
     * @return WebElement if visible or null if not visible
     */
    public WebElement waitInCaseElementVisible(WebElement e, int duration) {
        WebDriverWait wait = new WebDriverWait(driver, duration);
        try
        {
            return wait.until(ExpectedConditions.visibilityOf(e));
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    //Java-script Helpers
    public void javascriptClickOn(WebElement e) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", e);
    }

    //Alerts
    public void waitTillAlertPresent() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.WEBDRIVER_WAIT_DURATION);
        wait.until(ExpectedConditions.alertIsPresent());
    }

    /**
     * To get Element attribute value
     * @param e WebElement object
     * @param attributeName attribute name e.g. style
     * @return attribute value
     */
    public String getElementAttributeValue(WebElement e, String attributeName) {
        if(isElementAtrributePresent(e,attributeName))
        {
            return e.getAttribute(attributeName);
        }
        return "Attribute" + attributeName +" not found";
    }

    /**
     * To determine whether WebElement has given Attribute or not
     * @param e WebElement object
     * @param attributeName attribute name e.g. style
     * @return boolean
     */
    public boolean isElementAtrributePresent(WebElement e, String attributeName)
    {
        return e.getAttribute(attributeName) != null;
    }

    /**
     * Select Filter Search By
     * @param filterText
     * @throws InterruptedException
     */
    public void selectFilterSearchBy(String filterText) throws InterruptedException {
        searchByDropdownlist.click();
        WebElement optionSearch = driver.findElement(By.xpath("//option[contains(.,'"+filterText+"')]"));
        optionSearch.click();
    }

    /**
     * Enter Search Text in to Search box
     *
     * @param searchText search text
     * @throws InterruptedException
     */
    public void enterTextToVerificationAccount(String searchText) throws InterruptedException {
        searchTextBoxVerificationAccount.click();
        searchTextBoxVerificationAccount.sendKeys(searchText);
    }

    /**
     * Click On Search Icon Button
     *
     * @throws InterruptedException
     */
    public void clickOnSearchIconButton() throws InterruptedException {
        searchIconButton.click();
    }

    /**
     * Click On Reject Button
     *
     * @throws InterruptedException
     */
    public void clickOnRejectButton() throws InterruptedException {
        rejectButton.click();
    }

    /**
     * Click On Reject reason Dropdown
     *
     * @throws InterruptedException
     */
    public void clickOnRejectReasonDropdown() throws InterruptedException {
        rejectReasonDropdown.click();
        chooseReason.click();
    }

    /**
     * Click On Submit Rejection Button
     *
     * @throws InterruptedException
     */
    public void clickOnSubmitButton() throws InterruptedException {
        submitButton.click();
    }

    /**
     * Get Status Verification Rejected
     *
     * @return Verification Status
     */
    public List<WebElement> getVerificationStatus() {
        return driver.findElements(verificationStatus);
    }
}
