package pageobjects.banksimulator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class BankChannelSimulatorPO {
    //JavaHelpers java;
    WebDriver driver;

    public BankChannelSimulatorPO(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @FindBy(id = "paymentsimulatorform-virtual_account")
    private WebElement billingNumberTextBox;

    @FindBy(id = "name")
    private WebElement nameLabel;

    @FindBy(id = "searchva")
    private WebElement searchVirtualAccountButton;

    @FindBy(id = "billing_amount")
    private WebElement paymentAmountLabel;

    @FindBy(id = "paymentsimulatorform-total_paid")
    private WebElement paymentAmountTextBox;

    @FindBy(xpath = "//*[@class='btn btn-success flag-button']")
    private WebElement paymentButton;

    @FindBy(xpath = "//*[@class='note note-success']/p")
    private WebElement bniPaymentMessage;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement searchKodePembayaranButton;

    @FindBy(xpath = "//*[text()='Total Transaksi']/following-sibling::div")
    private WebElement paymentAmountPermataLabel;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement paymentPermataButton;

    /**
     * Enter virtual account number
     *@param virtualAccountNumber is numeric generate by system
     */
    public void enterTextVirtualAccount(String virtualAccountNumber) {
        System.out.print(virtualAccountNumber);
        billingNumberTextBox.sendKeys(virtualAccountNumber);
    }

    /**
     * Click button for search data by virtual account number
     *@throws InterruptedException
     */
    public void clickOnSearchVirtualAccount() throws InterruptedException {
        searchVirtualAccountButton.click();
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountLabel() {
        return paymentAmountLabel.getText();
    }

    /**
     * Enter payment amount to text box
     *@param   paymentAmount is total amount
     */
    public void enterTextPaymentAmount(String paymentAmount) {
        paymentAmountTextBox.sendKeys(paymentAmount);
    }

    /**
     * Click payment button
     */
    public void clickOnPaymentButton() {
        paymentButton.click();
    }

    /**
     * Get tenant name on payment page
     *@return tenant name
     */
    public String getName() throws InterruptedException {
        Thread.sleep(3000);
        return nameLabel.getText();
    }

    /**
     * Get message payment confirmation
     *@return message confirmation
     */
    public String getPaymentMessage() {
        return bniPaymentMessage.getText();
    }

    /**
     * Close driver
     */
    public void closeBrowser(){
        driver.quit();
    }

    /**
     * Enter Kode Pembayaran Permata
     *@param kodePembayaran is numeric generate by system
     */
    public void enterKodePembayaranBankPermata(String kodePembayaran) {
        System.out.print(kodePembayaran);
        billingNumberTextBox.sendKeys(kodePembayaran);
    }

    /**
     * Click button for search data by kode pembayaran
     *@throws InterruptedException
     */
    public void clickOnSearchKodePembayaran() throws InterruptedException {
        searchKodePembayaranButton.click();
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountPermataLabel() {
        return paymentAmountPermataLabel.getText();
    }

    /**
     * Click payment button
     */
    public void clickOnPaymentPermataButton() {
        paymentPermataButton.click();
    }
}

