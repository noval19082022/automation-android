package pageobjects.banksimulator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class BankMandiriChannelSimulatorPO {
    //JavaHelpers java;
    WebDriver driver;

    public BankMandiriChannelSimulatorPO(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @FindBy(id = "paymentsimulatorform-virtual_account")
    private WebElement billingNumberTextBox;

    @FindBy(id = "name")
    private WebElement nameLabel;

    @FindBy(id = "searchva")
    private WebElement searchVirtualAccountButton;

    @FindBy(id = "billing_amount")
    private WebElement paymentAmountLabel;

    @FindBy(id = "paymentsimulatorform-total_paid")
    private WebElement paymentAmountTextBox;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement paymentButton;

    @FindBy(xpath = "//*[@class='alert alert-success']")
    private WebElement mandiriPaymentMessage;

    @FindBy(id = "biller_code")
    private WebElement billCodeTextBox;

    @FindBy(id = "payment_code")
    private WebElement billKeyTextBox;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement searchVirtualAccountButtonMandiri;


    /**
     * Enter virtual account number mandiri
     *@param virtualAccountNumber is numeric generate by system
     */
    public void enterTextVirtualAccountMandiri(String virtualAccountNumber) {
        System.out.print(virtualAccountNumber);
        billKeyTextBox.sendKeys(virtualAccountNumber);
    }

    /**
     * Click button for search data by virtual account number mandiri
     *@throws InterruptedException
     */
    public void clickOnSearchVirtualAccountMandiri() throws InterruptedException {
        searchVirtualAccountButtonMandiri.click();
    }

    /**
     * Enter biller code mandiri
     *@param billerCode is numeric generate by system
     */
    public void enterTextBillerCodeMandiri(String billerCode) throws InterruptedException{
        Thread.sleep(3000);
        billCodeTextBox.click();
        billCodeTextBox.sendKeys(billerCode);
    }

    /**
     * Click button for search data by virtual account number
     *@throws InterruptedException
     */
    public void clickOnSearchVirtualAccount() throws InterruptedException {
        searchVirtualAccountButton.click();
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountLabel() {
        return paymentAmountLabel.getText();
    }

    /**
     * Enter payment amount to text box
     *@param   paymentAmount is total amount
     */
    public void enterTextPaymentAmount(String paymentAmount) {
        paymentAmountTextBox.sendKeys(paymentAmount);
    }

    /**
     * Click payment button
     */
    public void clickOnPaymentButton() {
        paymentButton.click();
    }

    /**
     * Get tenant name on payment page
     *@return tenant name
     */
    public String getName() throws InterruptedException {
        Thread.sleep(3000);
        return nameLabel.getText();
    }

    /**
     * Get message payment confirmation
     *@return message confirmation
     */
    public String getPaymentMessage() {
        return mandiriPaymentMessage.getText();
    }

    /**
     * Close driver
     */
    public void closeBrowser(){
        driver.quit();
    }
}
