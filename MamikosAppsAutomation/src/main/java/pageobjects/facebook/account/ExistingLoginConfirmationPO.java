package pageobjects.facebook.account;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import io.appium.java_client.AppiumDriver;
import utilities.AppiumHelpers;
import utilities.Constants;

public class ExistingLoginConfirmationPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public ExistingLoginConfirmationPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	  //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.WEBDRIVER_WAIT_DURATION), this);
	}
    
    /*
	 * All Elements are identified by @FindBy annotation
	 * @FindBy can accept tagName, partialLinkText, name, linkText, id, css, className, xpath as attributes.
	 */


	private By continueButton = Constants.MOBILE_OS == Platform.ANDROID ? By.cssSelector("button[name='__CONFIRM__']"):
			By.xpath("//XCUIElementTypeButton[@name=\"Lanjut\"]");


	/**
	 * Click on 'Continue' button
	 */
	public void clickOnContinueButton() throws InterruptedException {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			appium.waitInCaseElementVisible(continueButton, 10);
			appium.hardWait(3);
			appium.clickOn(continueButton);
		}
		else
		{
			appium.waitInCaseElementVisible(continueButton, 10);
			appium.tapOrClick(continueButton);
			if(appium.isElementPresent(continueButton)){
				appium.waitInCaseElementVisible(continueButton, 10);
				appium.tapOrClick(continueButton);
			}
		}
	}
}
