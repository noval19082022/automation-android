package pageobjects.facebook.account;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import io.appium.java_client.AppiumDriver;
import utilities.AppiumHelpers;
import utilities.Constants;

public class FacebookLoginPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public FacebookLoginPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	  //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.WEBDRIVER_WAIT_DURATION), this);
	}
    
    /*
	 * All Elements are identified by @FindBy annotation
	 * @FindBy can accept tagName, partialLinkText, name, linkText, id, css, className, xpath as attributes.
	 */ 

	private By emailOrPhoneTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.cssSelector("input#m_login_email"): By.xpath("//XCUIElementTypeOther[@name='main']/XCUIElementTypeTextField");

	private By passwordTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.cssSelector("input#m_login_password"): By.xpath("//XCUIElementTypeOther[@name='main']/XCUIElementTypeSecureTextField");

	private By loginButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@name='login']"): By.id("Log In");



	/**
	     * Fill out form and click on Login button
	     * @param emailOrPhone email or phone info
	     * @param password password info
	     * @throws InterruptedException 
	     */
	    public void fillOutFormAndClickOnLoginButton(String emailOrPhone, String password) throws InterruptedException
	    {
			appium.hardWait(4);
	        appium.tapOrClick(emailOrPhoneTextbox);
	    	appium.enterText(emailOrPhoneTextbox, emailOrPhone, true);
	    	appium.enterText(passwordTextbox, password, true);
			if (Constants.MOBILE_OS == Platform.ANDROID) {
				if (!appium.isElementPresent(loginButton)){
					appium.hideKeyboard();
				}
			}
			appium.hardWait(3);
	    	appium.tapOrClick(loginButton);
	    } 	    
	    
}
