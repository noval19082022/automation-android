package pageobjects.mamikos;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class ForgotPasswordPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ForgotPasswordPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@name=\"phoneNumberTextField\"]")
    @AndroidFindBy(id = "inputEditTextV2MainInput")
    private WebElement phoneNumberEditText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Pilih metode verifikasi\"]")
    @AndroidFindBy(id = "chooseVerificationButton")
    private WebElement chooseVerificationButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]")
    @AndroidFindBy(id = "smsVerificationView")
    private WebElement smsVerificationButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeOther[1]")
    @AndroidFindBy(id = "whatsAppVerificationView")
    private WebElement whatsAppVerificationButton;

    @AndroidFindBy(id = "titleBottomTextView")
    private WebElement titlePageTextView;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Kirim ulang kode\"]")
    @AndroidFindBy(id = "requestCodeButton")
    private WebElement resendOTPButton;

    @AndroidFindBy(id = "inputOtpTimerTextView")
    private WebElement resendOTPCountdownTimer;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTextField")
    @AndroidFindBy(id = "inputPinFirstEditText")
    private WebElement inputPinFirst;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
    @AndroidFindBy(id = "inputPinSecondEditText")
    private WebElement inputPinSecond;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeTextField")
    @AndroidFindBy(id = "inputPinThirdEditText")
    private WebElement inputPinThird;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[4]/XCUIElementTypeTextField")
    @AndroidFindBy(id = "inputPinFourEditText")
    private WebElement inputPinFourth;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Kode tidak valid, harap periksa kembali.\"]")
    @AndroidFindBy(id = "errorInputOtpTextView")
    private WebElement OTPErrorMessage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"ic back default\"]")
    @AndroidFindBy(id = "backImageView")
    private WebElement backButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Ya, Batalkan\"]")
    @AndroidFindBy(id = "cancelButton")
    private WebElement cancelButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Done\"]")
    private WebElement doneButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Pilih Metode Verifikasi\"]")
    private WebElement chooseVerificationMethodiOS;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Verifikasi Akun Pemilik Kos\"]")
    private WebElement ownerVerificationiOS;


    /**
     * Fill Registered Phone Number
     * @param phoneNumber Phone Number
     */
    public void fillRegisteredPhoneNumber(String phoneNumber){
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.enterText(phoneNumberEditText, phoneNumber, false);
        } else {
            appium.enterText(phoneNumberEditText, phoneNumber, false);
            appium.tapOrClick(doneButton);
        }
    }

    /**
     * Click Choose Verification Button
     */
    public void clickOnChooseVerificationButton(){
        appium.waitInCaseElementVisible(chooseVerificationButton, 3);
        appium.clickOn(chooseVerificationButton);
    }

    /**
     * Click on SMS Verification Button
     */
    public void clickOnSMSVerificationButton() {
        appium.clickOn(smsVerificationButton);
    }

    /**
     * Click on WhatsApp Verification Button
     */
    public void clickOnWAVerificationButton(){
        appium.clickOn(whatsAppVerificationButton);
    }

    /**
     * Get Title Page text
     * @return String
     */
    public String getTitlePageText() throws InterruptedException {
        appium.hardWait(3);
        return appium.getText(titlePageTextView);
    }

    /**
     * Is Resend OTP Button Appear?
     * @return True or False
     */
    public Boolean isResendOTPButtonAppear(){
        return appium.waitInCaseElementClickable(resendOTPButton, 65) != null && appium.waitInCaseElementVisible(resendOTPButton, 65) != null;
    }

    /**
     * Click on Resend OTP Button
     */
    public void clickOnResendOTPButton(){
        appium.click(resendOTPButton);
    }

    /**
     * Is Resend OTP Countdown Timer appear?
     * @return True or False
     */
    public Boolean isResendOTPCountdownTimerAppear(){
        return appium.waitInCaseElementVisible(resendOTPCountdownTimer,3) != null;
    }

    /**
     * Fill OTP Code Text Box
     * @param one First letter of OTP Code
     * @param two Second letter of OTP Code
     * @param three Third letter of OTP Code
     * @param four Fourth letter of OTP Code
     */
    public void fillOTPForm(String one, String two, String three, String four){
        appium.enterText(inputPinFirst, one, false);
        appium.enterText(inputPinSecond, two, false);
        appium.enterText(inputPinThird, three, false);
        appium.enterText(inputPinFourth, four, false);
    }

    /**
     * Get OTP Error Messages Text
     * @return String
     */
    public String getOTPErrorMessages() {
        return appium.getText(OTPErrorMessage);
    }

    /**
     * Click On Back Button
     */
    public void clickOnBackButton() throws InterruptedException {
        appium.hardWait(3);
        appium.clickOn(backButton);
    }

    /**
     * Click "Ya, Batalkan" on confirmation dialog
     */
    public void clickOnConfirmationDialog(){
        appium.waitInCaseElementVisible(cancelButton, 5);
        appium.clickOn(cancelButton);
    }

    /**
     * Get Cancel Button Text
     * @return String
     */
    public String getCancelButtonText(){
        return appium.getText(cancelButton);
    }

    /**
     * Get Owner Verification Text iOS
     * @return String
     * @throws InterruptedException
     */
    public String getOwnerVerificationiOS() throws InterruptedException {
        appium.hardWait(3);
        return appium.getText(ownerVerificationiOS);
    }

    /**
     * Get Verification Method iOS
     * @return String
     * @throws InterruptedException
     */
    public String getVerificationMethodiOS() throws InterruptedException {
        appium.hardWait(3);
        return appium.getText(chooseVerificationMethodiOS);
    }
}
