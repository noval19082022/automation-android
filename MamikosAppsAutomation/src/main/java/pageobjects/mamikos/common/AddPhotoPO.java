package pageobjects.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class AddPhotoPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public AddPhotoPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }


    @AndroidFindBy(id = "gallerySourceImageView")
    private WebElement gallerySelector;

    @AndroidFindBy(xpath = "//*[@resource-id='com.google.android.apps.photos:id/image']")
    private WebElement photoButton;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[contains(@content-desc, 'Photo taken on')]")
    private WebElement itemImage;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/iv_photo']")
    private WebElement selectPhotoButton;

    @AndroidFindBy(id = "action_done")
    private WebElement finishButton;


    /**
     * Upload photo from Gallery
     * Must have photo in Gallery
     */
    public void uploadPhoto() throws InterruptedException {
        appium.tapOrClick(gallerySelector);
        if (appium.waitInCaseElementVisible(photoButton, 3) != null) {
            appium.tapOrClick(photoButton);
            appium.tapOrClick(itemImage);
        }
        else {
            appium.tapOrClick(selectPhotoButton);
            appium.tapOrClick(selectPhotoButton);
            appium.tapOrClick(finishButton);
        }
        appium.hardWait(3);
    }
}
