package pageobjects.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class FooterPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public FooterPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
		//This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

	@iOSXCUITFindBy(accessibility = "Login")
	@AndroidFindBy(id ="profileTenantTextView")
	private WebElement loginMenu;

	private By exploreMenu = Constants.MOBILE_OS == Platform.ANDROID ? By.id("exploreTextView"): By.id("Cari");

	@iOSXCUITFindBy(accessibility = "Favorit")
	@AndroidFindBy(id="wishListImageView")
	private WebElement favoriteMenu;

	private By chatMenu = Constants.MOBILE_OS == Platform.ANDROID ? By.id("chatTenantTextView"): By.id("Chat");

	private By chatMenuOwner = Constants.MOBILE_OS == Platform.ANDROID ? By.id("chatOwnerTextView"): By.id("Chat");

	@AndroidFindBy(id="bookingImageView")
	private WebElement bookingTenantMenu;

	@AndroidFindBy(id="profileTenantView")
	private WebElement profileMenu;

	private By profileMenuOwner = Constants.MOBILE_OS == Platform.ANDROID ? By.id("profileOwnerImageView"): By.id("Profil");

	@iOSXCUITFindBy(accessibility="Booking")
	@AndroidFindBy(uiAutomator ="new UiSelector().resourceIdMatches(\".*:id/bookingOwnerTextView\")")
	private WebElement ownerBookingMenu;

	@iOSXCUITFindBy(accessibility="Home")
	@AndroidFindBy(id="homeOwnerTextView")
	private WebElement ownerHomeMenu;

	@iOSXCUITFindBy(accessibility="Chat")
	@AndroidFindBy(id="chatOwnerTextView")
	private WebElement ownerChatMenu;

	private By manageButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("ownerManageImageView"): By.id("Kelola");

	private By textProfileTenant = Constants.MOBILE_OS == Platform.ANDROID ? By.id("profileTenantTextView"): By.id("Profil");

	private By textProfileOwner = Constants.MOBILE_OS == Platform.ANDROID ? By.id("profileOwnerTextView"): By.id("Profil");

	private By tenantHomeMenu = By.id("exploreImageView");


	/**
	 * Click on 'Login' menu
	 */
	public void clickOnLoginMenu() {
		appium.tapOrClick(loginMenu);
	}

	/**
	 * Click on 'Explore' menu
	 */
	public void clickOnExploreMenu()
	{
		appium.waitInCaseElementVisible(exploreMenu,5);
		appium.tapOrClick(exploreMenu);
	}

	/**
	 * Click on 'Favorite' menu
	 */
	public void clickOnFavoriteMenu()
	{
		appium.tapOrClick(favoriteMenu);
	}

	/**
	 * Click on 'Chat' menu
	 */
	public void clickOnChatMenu() throws InterruptedException {
		appium.hardWait(2);
		appium.tapOrClick(chatMenu);
		appium.hardWait(3);
	}
	/**
	 * Click on 'Chat' menu owner
	 */
	public void clickOnChatMenuOwner() throws InterruptedException {
		appium.waitInCaseElementClickable(chatMenuOwner,5);
		appium.tapOrClick(chatMenuOwner);
		appium.hardWait(3);
	}

	/**
	 * Click on 'Profile Menu' menu
	 */
	public void clickOnProfileMenu() throws InterruptedException {
		if (appium.isElementDisplayed(profileMenu)){
			appium.tapOrClick(profileMenu);
			appium.hardWait(3);
		} else {
			appium.tapOrClick(profileMenuOwner);
			appium.hardWait(3);
		}
	}

	/**
	 * explore menu is displayed or not ?
	 * @return true or false
	 */
	public boolean isExploreMenuDisplayed() {
		return appium.waitInCaseElementVisible(exploreMenu, 5)!=null;
	}

	/**
	 * wishList menu is displayed or not ?
	 * @return true or false
	 */
	public boolean isWishListMenuDisplayed() {
		return appium.waitInCaseElementVisible(favoriteMenu, 5)!=null;
	}

	/**
	 * chat menu is displayed or not ?
	 * @return true or false
	 */
	public boolean isChatMenuDisplayed() {
		return appium.waitInCaseElementVisible(chatMenu, 5)!=null;
	}

	/**
	 * Login menu is displayed or not ?
	 * @return true or false
	 */
	public boolean isLoginMenuDisplayed() {
		return appium.waitInCaseElementVisible(loginMenu, 5)!=null;
	}

	//Owner
	/**
	 * Click on owner 'Booking' menu
	 */
	public void clickOnOwnerBookingMenu()
	{
		appium.tapOrClick(ownerBookingMenu);
	}

	/**
	 * Click on owner 'Home' menu
	 */
	public void clickOnOwnerHomeMenu()
	{
		appium.tapOrClick(ownerHomeMenu);
	}

	/**
	 * Click on owner 'Chat' menu
	 */
	public void clickOnOwnerChatMenu() throws InterruptedException {
		appium.tapOrClick(ownerChatMenu);
		appium.hardWait(2);
	}

	/**
	 * Click on owner 'Manage' menu
	 */
	public void clickOnOwnerManageMenu()
	{
		appium.clickOn(manageButton);
	}

	/**
	 * Click on 'Profile' menu as a owner
	 */
	public void clickOnOwnerProfileMenu()
	{
		appium.tapOrClick(textProfileOwner);
	}

	/**
	 * Click on 'Booking Tenant' menu
	 */
	public void clickOnBookingTenantMenu() {
		appium.tapOrClick(bookingTenantMenu);
	}

	/**
	 * Get profile text for assertion
	 * @return string data type
	 */
	public String getProfileText() {
		return appium.getText(textProfileTenant);
	}

	/**
	 * Click on Tenant 'Home' menu
	 */
	public void clickOnTenantHomeMenu()
	{
		appium.tapOrClick(tenantHomeMenu);
	}

	/**
	 * Get profile text for assertion
	 * @return string text icon profil owner
	 */
	public String getProfileTextOwner() {
		return appium.getText(textProfileOwner);
	}
}
