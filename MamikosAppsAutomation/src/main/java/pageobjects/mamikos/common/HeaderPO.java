package pageobjects.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class HeaderPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public HeaderPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(accessibility = "ic green arrown left")
    WebElement backButtoniOS;

    /**
     * Tap on application Back (<) button
     */
    public void iOSBackButton(){

        appium.tapOrClick(backButtoniOS);
    }
}
