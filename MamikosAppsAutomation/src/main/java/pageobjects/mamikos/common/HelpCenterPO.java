package pageobjects.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class HelpCenterPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public HelpCenterPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver, this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "titleTextView")
    private WebElement titleTextOfHelpPage;

    @AndroidFindBy(id = "okButton")
    private WebElement okButton;

    private By waHelpButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='WhatsApp']") : By.xpath("//*[@value='WhatsApp']");
    private By csHelpButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Chat dengan CS']") : By.xpath("//*[@value='Chat dengan CS']");
    private By helpCenterHeadingView = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Pusat Bantuan']") : By.xpath("//*[@value='Pusat Bantuan']");

    @AndroidFindBy(id = "actionHelpCenterButton")
    private WebElement helpCenterButton;

    @AndroidFindBy(id = "closeButton")
    private WebElement closeButton;

    /**
     * Help page header is displayed or not ?
     *
     * @return true or false
     */
    public boolean isHelpPageHeaderDisplayed() {
        return appium.waitInCaseElementVisible(titleTextOfHelpPage, 5) !=null;
    }

    /**
     * Click on Ok button of help page
     */
    public void clickOnOKButton() {
        appium.tapOrClick(okButton);
    }

    /**
     * WA help button is displayed or not ?
     *
     * @return true or false
     */
    public boolean isWaHelpButtonDisplayed() {

        return appium.waitInCaseElementVisible(waHelpButton, 5) !=null;
    }

    /**
     * CS help button is displayed or not ?
     *
     * @return true or false
     */
    public boolean isCsHelpButtonDisplayed() {

        return appium.waitInCaseElementVisible(csHelpButton, 5) !=null;
    }

    /**
     * Help Center button is displayed or not ?
     *
     * @return true or false
     */
    public boolean isHelpCenterDisplayed() {
        return appium.waitInCaseElementVisible(helpCenterHeadingView, 5) != null;
    }

    /**
     * check help option is present or not
     *
     * @param helpOption help option
     * @return true or false
     */
    public boolean isHelpOptionPresent(String helpOption) {
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + helpOption + "']"), 5) != null;
    }

    /**
     * Click on WA Help button
     */
    public void clickOnWaHelpButton() {
        appium.tapOrClick(waHelpButton);
    }

    /**
     * Click on CS Help button
     */
    public void clickOnCsHelpButton() {
        appium.tapOrClick(csHelpButton);
    }

    /**
     * Click on Help center button
     */
    public void clickOnHelpCenterButton() {
        appium.tapOrClick(helpCenterButton);
    }

    /**
     * Click on close button of help popup
     */
    public void clickOnCloseButton() {
        appium.tapOrClick(closeButton);
    }

    /**
     * Help Center page heading in web view is shown or not
     *
     * @return true or false
     */
    public boolean isHelpCenterHeadingViewDisplayed() throws InterruptedException {
        appium.hardWait(5);
        return appium.waitInCaseElementVisible(helpCenterHeadingView, 10)!=null;
    }

    /**
     * Check whether Text appear on page
     * @return true if appear
     */
    public boolean isTextAppear(String text) {
        String xpathLocator;
        if(Constants.MOBILE_OS == Platform.ANDROID){
            xpathLocator = "//*[contains(@text, '" + text + "')]";
        }
        else {
            xpathLocator = "//*[contains(@label, '" + text + "')]";
        }
        return appium.waitInCaseElementVisible(By.xpath(xpathLocator), 5) != null;
    }

}
