package pageobjects.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class LoadingPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public LoadingPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver, this);

    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

//    @iOSXCUITFindBy(accessibility = "In progress")
//    @AndroidFindBy(id = "imageLoadingView")
    private By loadingImg = By.id("imageLoadingView");

    @AndroidFindBy(id = "mainListLoadingView")
    private WebElement loadingLoadMoreAnimation;

    @FindBy(id= "titleLoadingTextView")
    private WebElement loadingAnimation;


    /**
     * Wait for loading finish
     */
    public void waitLoadingFinish() throws InterruptedException {
        if (appium.waitInCaseElementVisible(loadingImg, 5) != null) {
            appium.hardWait(4);
        }
    }

    /**
     * Check whether loading load more appear
     * @return true if appear
     */
    public boolean isLoadingLoadMoreAppear() {
        return appium.waitInCaseElementVisible(loadingLoadMoreAnimation, 5) != null;
    }


    /**
     * Scroll up to refresh the page
     */
    public void refreshPage() throws InterruptedException {
        appium.scroll(0.5, 0.40, 0.80, 2000);
        appium.hardWait(5);
    }

    public boolean isLoadingAnimationPresent() {
        return appium.waitInCaseElementVisible(loadingAnimation, 5) != null;
    }

    /**
     * Wait until loading animation disappear
     * @throws InterruptedException
     */
    public void waitLoadingAnimationDisappear() throws InterruptedException {
        do {
            appium.hardWait(1);
        }
        while (isLoadingAnimationPresent());
    }
}
