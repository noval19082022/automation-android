package pageobjects.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class PopUpPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public PopUpPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);


        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(xpath = "//*[@text='LATER']")
    private WebElement laterButton;

    @AndroidFindBy(id = "logoutTextView")
    private WebElement logoutButton;

    @AndroidFindBy(id = "dismissPopupView")
    private WebElement dismissPopUp;

    @AndroidFindBy(id = "updateAllPropertyButton")
    private WebElement updatePropertyAds;

    @AndroidFindBy(xpath = "//*[@text = 'NANTI SAJA']")
    private WebElement cancelVerifButton;

    @AndroidFindBy(id = "closeInviteEmailTextView")
    private WebElement closeEmailVerifButton;

    @AndroidFindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'com.git.mami.kos:id/ivInviteTrialClose')]")
    private WebElement closeOwnerInvitationBanner;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[contains(@resource-id,'com.git.mami.kos:id/instantBookingTooltipView')]")
    private WebElement closeInstantBookingTooltip;

    @AndroidFindBy(id = "updateAllPropertyButton")
    private WebElement updateAllKosButton;

    private By continueButtonFromBookingPopup = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Lanjut']"): By.xpath("//XCUIElementTypeButton[@name='Lanjut']");


    //Later button in pop up direct booking offer
    @AndroidFindBy(id = "leftButton")
    private WebElement directBookingLaterButton;

    @AndroidFindBy(xpath = "//*[@text='Bluetooth']")
    private WebElement turnOnButtonOfBluetoothPopup;

    @AndroidFindBy(id = "instantBookingProfileTooltipView")
    private WebElement instantBookPopUpProfile;

    @AndroidFindBy(id= "exitTextView")
    private WebElement exitButton;

    @AndroidFindBy(id = "bookingBenefitViewPager")
    private WebElement bookingBenefitPopUp;

    @AndroidFindBy(xpath = "//*[@text='Dari mana kamu tahu tentang Mamikos?']")
    private WebElement knowMamikosPopUpText;

    @AndroidFindBy(id = "nextButton")
    private WebElement confirmButton;

    @AndroidFindBy(id = "mamipointButton")
    private WebElement mamiPointButton;

    @AndroidFindBy(id = "declareNextTextView")
    private WebElement declareNextButton;

    @AndroidFindBy(id = "historyRewardNextTextView")
    private WebElement historyRewardNextButton;

    @AndroidFindBy(id = "historyPointNextTextView")
    private WebElement historyPointNextButton;

    @AndroidFindBy(id = "guidelineNextTextView")
    private WebElement guidelineNextButton;

    @AndroidFindBy(id = "redeemNextTextView")
    private WebElement redeemNextButton;

    @AndroidFindBy(id = "com.git.mami.kos:id/ftueFinancialReportView")
    private WebElement laporanKeuanganTooltip;

    @AndroidFindBy(id = "webviewDialog")
    private WebElement popUpWhereAreYouKnowMamikos;

    @AndroidFindBy(id = "closeWebImageView")
    private WebElement iconCloseWebImageView;

    private By iUnderstandButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("nextButton"): By.xpath("//XCUIElementTypeStaticText[@name='Saya Mengerti']");

    @AndroidFindBy(id = "contentView")
    private WebElement popUpGoodsAndServices;

    @AndroidFindBy(id = "cancelButton")
    private WebElement cancelButton;

    @FindBy(id="ivOwnerInvitation")
    private WebElement ownerInvitation;

    @FindBy(id="btnOwnerInvite")
    private WebElement ownerInvitationOkButton;

    @AndroidFindBy(id = "nextButton")
    private WebElement dekatmuDownloadButton;

    private By textMessageView = By.id("messageTextView");

    @FindBy(id = "warningMessageTextView")
    private WebElement textWarningMessageTV;

    @FindBy(xpath = "//*[@text='Atur Biaya Tambahan di Kos Anda']")
    private WebElement textBookingFTUE;

    private By continueButtonIos = By.xpath("//*[@value='Lanjut']");

    /**
     * Click on 'LATER' button on pop up
     */
    public void clickOnLaterButton() {
        if (appium.waitInCaseElementVisible(laterButton, 5) != null) {
            appium.clickOn(laterButton);
        }
    }

    /**
     * Click on 'LOGOUT' button on pop up
     */
    public void clickOnLogoutButton() {
        appium.tapOrClick(logoutButton);
    }

    /**
     * Dismiss owner update property ads pop up
     */
    public void dismissOwnerUpdatePropertyAdsPopup() {
        if (appium.waitInCaseElementVisible(updatePropertyAds, 5) != null) {
            appium.back();
        }
    }

    /**
     * Dismiss owner pop up
     */
    public void dismissOwnerPopup() {
        if (appium.waitInCaseElementVisible(dismissPopUp, 5) != null) {
            appium.back();
        }
    }


    /**
     * Click tutup the verify number pop up
     */
    public void clickOnCancelButton() {
        if (appium.waitInCaseElementVisible(cancelVerifButton, 10) != null) {
            appium.tapOrClick(cancelVerifButton);
        }
    }

    /**
     * Click close the verify email pop up
     */
    public void clickOnCancelEmailVerifButton() {
        if (appium.waitInCaseElementVisible(closeEmailVerifButton, 10) != null) {
            appium.tapOrClick(closeEmailVerifButton);
        }
    }

    /**
     * Click close the invitation premium account banner pop up
     */
    public void clickOnCloseOwnerInvitationPremium() {
        if (appium.waitInCaseElementVisible(closeOwnerInvitationBanner, 10) != null) {
            appium.tapOrClick(closeOwnerInvitationBanner);
        }
    }

    /**
     * Check the install booking tooltip pop up is present
     * @return true if instant booking pop up appear
     */
    public boolean isCloseInstanBookingTootip() {
        return appium.waitInCaseElementVisible(closeInstantBookingTooltip, 3) != null;
    }

    /**
     * Click close the install booking tooltip pop up
     */
    public void clickOnCloseInstantBookingTooltip() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            if (isCloseInstanBookingTootip()) {
                appium.tap(500, 500);
            }
        } else {
            appium.tap(500, 500);
        }
    }

    /**
     * Click close the install booking tooltip pop up
     */
    public void clickOnUpdateAllKos() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            if (appium.isElementDisplayed(updateAllKosButton)) {
                appium.tapOrClick(updateAllKosButton);
            }
        } else {
            appium.tap(500, 500);
        }
    }

    /**
     * Check if pop up contain continue button
     * @return true or false
     */
    private boolean isContinueButtonPresent() {
        return appium.waitInCaseElementVisible(continueButtonFromBookingPopup, 2) != null;
    }

    /**
     * Check if pop up contain confirm button
     * @return true or false
     */
    private boolean isConfirmButtonPresent() {
        return appium.waitInCaseElementVisible(confirmButton, 3) != null;
    }

    /**
     * Tap on Next button on of Booking popup
     */
    public void goThroughBookingPopUps() {
        int i = 0;
        do {
            i++;
            appium.clickOn(continueButtonFromBookingPopup);
            if (i==5){
                break;
            }
        }
        while (isContinueButtonPresent());
        appium.clickOn(iUnderstandButton);
    }


    /**
     * Tap on update all kos after login as owner on of Booking popup
     */
    public void clickUpdateAllKos() {
        if (appium.waitInCaseElementVisible(continueButtonFromBookingPopup, 3) != null) {
            do {
                appium.tapOrClick(continueButtonFromBookingPopup);
            } while (appium.isElementPresent(continueButtonFromBookingPopup));
        }
    }

    /**
     * Click on 'Nanti Saja' button if pop up appear
     */
    public void closeDirectBookingPopUp() {
        if (appium.waitInCaseElementVisible(directBookingLaterButton, 10) != null) {
            appium.tapOrClick(directBookingLaterButton);
        }
    }

    /**
     * check bluetooth popup is peresent or not
     *
     * @return true or false
     */
    public boolean isBluetoothPopupPresent() {
        return appium.waitInCaseElementVisible(turnOnButtonOfBluetoothPopup, 5) != null;
    }

    /**
     * Click close the install booking tooltip pop up in owner profile
     */
    public void closeInstantBookingTooltipInProfile() {
        if (appium.waitInCaseElementVisible(instantBookPopUpProfile, 5) != null) {
            appium.tap(500, 500);
        }
    }

    /**
     * Click on exit button with text "Keluar"
     */
    public void clickOnExitButton() {
        appium.clickOn(exitButton);
    }

    /**
     * Check pop up present or not
     * @return true / false
     */
    public boolean isMamikosSurveyPresent() {
        return appium.waitInCaseElementVisible(knowMamikosPopUpText, 10) != null;
    }

    /**
     * Click on get point button if button exist
     */
    public void clickOnGetPointButton() throws InterruptedException {
        appium.hardWait(5);
        if (appium.isElementDisplayed(mamiPointButton)) {
            appium.tapOrClick(mamiPointButton);
            appium.tapOrClick(declareNextButton);
            appium.tapOrClick(historyRewardNextButton);
            appium.tapOrClick(historyPointNextButton);
            appium.tapOrClick(guidelineNextButton);
            appium.tapOrClick(redeemNextButton);
            appium.back();
        }
    }

    /**
     * * * Check if Laporan Keuangan tooltip is present
     */
    public boolean isLaporanKeuanganTooltipPresent() {
        return appium.waitInCaseElementVisible(laporanKeuanganTooltip, 3) != null;
    }

        /**
    /**
     * Click on Laporan Keuangan tooltip
     */
    public void clickOnLaporanKeuanganTooltip() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            if (isLaporanKeuanganTooltipPresent()) {
                appium.tapOrClick(laporanKeuanganTooltip);
            }
        }
    }

    /**
     * Click on icon close when popup where are you know mamikos is display
     */
    public void closePopUpWhereAreYouKnowMamikos() {
        if (appium.isElementDisplayed(popUpWhereAreYouKnowMamikos)) {
            appium.tapOrClick(iconCloseWebImageView);
        }
    }

    /**
     * Close manage additional cost if appear
     */
    public void closeExtraCostPopUp() {
        if (appium.waitInCaseElementVisible(iUnderstandButton, 3) != null) {
            appium.tapOrClick(iUnderstandButton);
        }
    }

    /**
     * Click on icon close when popup Barang Jasa pindah ke Aplikasi DEKATMU
     */
    public void closePopUNotice() {
        if (appium.isElementDisplayed(popUpGoodsAndServices)) {
            appium.tapOrClick(cancelButton);
        }
    }

    /**
     * Click on next button FTUE Booking Benefit
     */
    public void clickContinueBtn() {
        appium.clickOn(continueButtonFromBookingPopup);
    }

    /**
     * Check DEKATMU download button present / not
     * @return true / false
     */
    public boolean isDownloadDEKATMUButtonPresent() {
        return appium.waitInCaseElementVisible(dekatmuDownloadButton, 3) != null;
    }

    /**
     * Check if owner invitation element present in screen
     * Owner invitation e.g Premium Invitation Pop Up
     * @return true / false
     */
    private boolean isOwnerInvitationPresent() {
        return appium.waitInCaseElementVisible(ownerInvitation, 3) != null;
    }

    /**
     * tap on owner invitation oke button if it available
     * Owner invitation e.g Premium Invitation Pop Up
     */
    public void tapOnCloseButtonOwnerInvitation() {
        while (isOwnerInvitationPresent()) {
            appium.clickOn(ownerInvitationOkButton);
        }
    }

    /**
     * Click on next button FTUE Booking Benefit
     * @return true / false
     */
    public boolean checkDownloadDEKATMURedirection() {
        appium.clickOn(dekatmuDownloadButton);
        String xpathLocator = "//*[@text='Dekatmu - Komunitas Jual Barang Bekas di Sekitarmu']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 10) != null;
    }

    /**
     * Check FTUE appear
     * @return true / false
     */
    public boolean checkFTUEBBK() {
        return appium.waitInCaseElementVisible(textMessageView, 10) != null;
    }

    /**
     * get message view text
     * @return string data type
     */
    public String getMessageViewText() {
        return appium.getText(textMessageView);
    }

    /**
     * Get warning message text view
     * @return String data type
     */
    public String getWarningMessageTextView() {
        return appium.getText(textWarningMessageTV);
    }

    /**
     * Get cancel button pop up text
     * @return String data type
     */
    public String getCancelButtonText() {
        return appium.getText(cancelButton);
    }

    /**
     * Get confirm button pop up text
     * @return String data type
     */
    public String getNextButtonText() {
        return appium.getText(confirmButton);
    }

    /**
     * Click on cancel button on pop-up
     */
    public void clickOnCancelButtonAlertPopUp() {
        if (appium.waitInCaseElementVisible(cancelButton, 5) != null) {
            appium.clickOn(cancelButton);
        }
    }

    /**
     * Click on next button on pop-up
     */
    public void clickOnNextButtonAlertPopUp() {
        if (appium.waitInCaseElementVisible(confirmButton, 5) != null) {
            appium.clickOn(confirmButton);

        }
    }

    /**
     * Check is booking ftue visible in the view port
     * @return visible true otherwise false
     */
    public boolean isBookingFTUEVisible() {
        return appium.waitInCaseElementVisible(textBookingFTUE, 5) != null;
    }

    /**
     * Click on next button FTUE After choose server iOS
     */
    public void clickContinueBtnIos() {
        if (appium.waitInCaseElementVisible(continueButtonIos, 5) != null) {
            appium.tapOrClick(continueButtonIos);
        }
    }
}
