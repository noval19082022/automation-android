package pageobjects.mamikos.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import utilities.AppiumHelpers;


public class ToastMessagePO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public ToastMessagePO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
	}
    
    /**
     * To verify whether given toast message displayed or not
     * @param expectedToastMessage expected toast message
     * @return true or false
     */
	public boolean isToastMessageDisplayed(String expectedToastMessage)
	{
		String xpathExpression = "//*[@text='" + expectedToastMessage + "']";
		try
		{
			appium.waitTillElementsCountIsEqualTo(By.xpath(xpathExpression), 1);
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

	/**
	 * Verify toast message appear or not
	 * @param toast is message we want to verify
	 * @return true if toast appear
	 */
	public boolean verifyToastMessage(String toast) {
		long startTime = System.currentTimeMillis(); //fetch starting time
		boolean neededStatus;
		do {
			String xmlFormat = driver.getPageSource();
			neededStatus = xmlFormat.contains(toast);
		} while (!(neededStatus) && (((System.currentTimeMillis() - startTime) <= (11 * 1000))));
		return neededStatus;
	}
}
