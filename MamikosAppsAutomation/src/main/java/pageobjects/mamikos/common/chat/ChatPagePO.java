package pageobjects.mamikos.common.chat;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pageobjects.mamikos.tenant.detail.KostDetailsPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import java.util.ArrayList;
import java.util.List;

public class ChatPagePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ChatPagePO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */


    @iOSXCUITFindBy(accessibility = "ic send chat enable")
    @AndroidFindBy(id = "sentChatButton")
    private WebElement sendBtn;

    @AndroidFindBy(id = "firstTitleTextView")
    private WebElement chatGroupName;

    private By chatTitletext = Constants.MOBILE_OS == Platform.ANDROID ? By.id("messageEditText"): By.xpath("//*[@value='Ketik pesan Anda']/parent::XCUIElementTypeOther");

    //tenant
    @iOSXCUITFindBy(accessibility = "HUBUNGI IKLAN")
    private WebElement backFromChat;

    //owner
    @iOSXCUITFindBy(accessibility = "Chat")
    private WebElement ownerBackFromChat;

    @AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc='mamikos']")
    private WebElement emptyChatImage;

    @AndroidFindBy(id = "emptyStateView")
    private WebElement emptyChatBackground;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.git.mami.kos:id/emptyStateView']/android.widget.TextView")
    private WebElement emptyChatText;

    @AndroidFindBy(id = "confirmSendMessageButton")
    private WebElement sendQuestion;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.TextView")
    private WebElement latestChat;

    private By ajukanSewaButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("primaryButton"):
            By.id("Ajukan Sewa");

    private By searchTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("etSearch"):
            By.xpath("//XCUIElementTypeTextField");

    private By titleChatText = Constants.MOBILE_OS == Platform.ANDROID ? By.id("tvTitle"):
            By.id("Chat tidak ditemukan");

    private By subtitleChatText = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text=\"What's your phone number?\"]"):
            By.id("Coba ganti kata kunci Anda atau ganti filter pencarian.");

    private By subtitleChatText2 = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text=\"My phone is 00000000001\"]"):
            By.id("What's your phone number?");

    @AndroidFindBy(id = "tvSubtitle")
    private WebElement subtitleChatText3;

    @AndroidFindBy(id = "mainChatView")
    private WebElement listChat;

    private By filterChatSelect = Constants.MOBILE_OS == Platform.ANDROID ? By.id("sortingView"):
            By.xpath("//XCUIElementTypeTextField/ancestor::XCUIElementTypeOther[2]/following-sibling::XCUIElementTypeOther[1]");

    private By tampikanButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("submitButton"): By.id("Tampilkan");

    @AndroidFindBy(id = "broadcastIconCV")
    private WebElement broadcastButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Tenant Khusus At Android ']")
    private WebElement tapChatlist;

    private By chatListTitletext = Constants.MOBILE_OS == Platform.ANDROID ? By.id("chatTitleView"):
            By.xpath("//XCUIElementTypeNavigationBar[@name='Chat']");

    private By doneButton = By.xpath("//XCUIElementTypeButton[@name='Done']");

    private By getChatMessage = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//*[@resource-id='com.git.mami.kos:id/tvMessage'])[4]"): By.xpath("");

    /**
     * Get chat message (from bottom)
     *
     * @param position integer position from bottom, >= 1
     * @return element text
     */
    public String getMessage(int position) {
        String idChat;
        List<WebElement> webElements;
        WebElement lastElement;
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            // element message in chat page
            idChat = "//*[@text='Update Kamar']/following::android.widget.EditText/preceding::android.widget.TextView[2]";
            webElements = driver.findElements(By.xpath(idChat));
            lastElement = webElements.get(position);
            return appium.getText(lastElement);
        } else {
            idChat = "//XCUIElementTypeTable/XCUIElementTypeCell[last()]/XCUIElementTypeTextView";
            return appium.getText(By.xpath(idChat));
        }
    }

    /**
     * Enter chat message in textbox
     *
     * @param text string text that we want to enter
     *             will hit enter after enter text
     */
    public void enterText(String text) {
        appium.enterText(chatTitletext, text, true);
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.tapOrClick(sendBtn);
        }else {
            appium.tapOrClick(doneButton);
            appium.tap(350,764);
        }
    }

    /**
     * Click back button in top left
     * will back to question page
     */
    public void clickBackFromChat() {
        if (appium.waitInCaseElementClickable(backFromChat, 5) != null) {
            appium.tapOrClick(backFromChat);
        } else {
            appium.tapOrClick(ownerBackFromChat);
        }
    }

    /**
     * Get chat group name text
     *
     * @return chat group name
     */
    public String getChatGroupName() {
        return appium.getText(chatGroupName);
    }

    /**
     * Verify image appear when chat list empty
     *
     * @return true if image appear
     */
    public boolean isEmptyChatImageAppear() {
        return appium.waitInCaseElementVisible(emptyChatImage, 3) != null
                && appium.waitInCaseElementVisible(emptyChatBackground, 2) != null;
    }

    /**
     * Get text in chat page when chat list empty
     *
     * @return text empty chat
     */
    public String getEmptyChatText() {
        return appium.getText(emptyChatText);
    }

    /**
     * Check if the Chat title is appears
     *
     * @return status chat appears / not
     */
    public boolean isChatTitleAppers() {
        return appium.waitInCaseElementVisible(chatTitletext, 3) != null;
    }

    /**
     * List all questions in pop up
     *
     * @return list questions
     */
    public List<String> listQuestions() {
        String xpathLocator = "//*[contains(@class, 'android.widget.RadioButton')]";
        List<WebElement> elements = appium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> questionsList = new ArrayList<>();
        for (WebElement s : elements) {
            questionsList.add((appium.getText(s)).replaceAll("\\s", ""));
        }
        return questionsList;
    }

    /**
     * Click one of question in radio button
     *
     * @param text is retrieved from table data in the feature file
     */
    public void clickQuestion(String text) {
        String xpathLocator = "//android.widget.RadioButton[contains(@text, '" + text + "')]";
        appium.clickOn(By.xpath(xpathLocator));
    }

    /**
     * Click send in question pop up
     */
    public void clickSend() {
        appium.scrollToElementByResourceId("confirmSendMessageButton");
        appium.clickOn(sendQuestion);
    }

    /**
     * Get latest chat
     *
     * @return String latest chat (most bottom chat)
     * @throws InterruptedException
     */
    public String getLatestChatText() throws InterruptedException {
        appium.hardWait(5);
        List<WebElement> webElements = driver.findElements(By.id("tvMessage"));
        return appium.getText(webElements.get(webElements.size() - 1));
    }

    /**
     * Click "Ajukan Sewa" button on detail chat
     */
    public void clickAjukanSewa() {
        appium.tapOrClick(ajukanSewaButton);
    }

    /**
     * Click on 'Search' textbox
     */
    public void enterTextToSearchTextbox(String searchText) throws InterruptedException {
        appium.enterText(searchTextbox, searchText, true);
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.hideKeyboard();
            ((PressesKey) driver).pressKey(new KeyEvent().withKey(AndroidKey.ENTER));
        }
        appium.hardWait(3);
    }

    /**
     * tap chat in chatlist
     */
    public void tapChatInChatlist() throws InterruptedException {
        appium.tapOrClick(tapChatlist);
    }

    /**
     * Get title chat not found
     *
     * @return Chat tidak ditemukan
     */
    public String getTextTitleChat() {
        return appium.getText(titleChatText);
    }

    /**
     * Get subtitle message chat
     *
     * @return "Coba ganti kata kunci Anda atau ganti filter pencarian."
     * <p>Chat in detail list</p>
     */
    public String getTextSubitleChat() throws InterruptedException {
        appium.hardWait(3);
        if (Constants.MOBILE_OS == Platform.IOS){
            appium.scroll(0.5, 0.2, 0.8, 2000);
        }
        if (appium.isElementPresent(subtitleChatText)) {
            return appium.getText(subtitleChatText);
        } else if (appium.isElementPresent(subtitleChatText2)) {
            return appium.getText(subtitleChatText2);
        } else {
            return appium.getText(subtitleChatText3);
        }
    }


    /**
     * is element with text visible
     * @param text desired text
     * @return visible true otherwise false
     */
    public boolean isElementWithText(String text) throws InterruptedException {
        String textEl = "";
        if (Constants.MOBILE_OS == Platform.ANDROID){
            textEl = "//*[contains(@text,'"+text+"')]";
        } else {
            textEl = "//*[@name='"+text+"']";
        }
        appium.hardWait(5);
        MobileElement element = (MobileElement) driver.findElementByXPath(textEl);
        return appium.waitInCaseElementVisible(element, 5) != null;
    }

    /**
     * Click Filter Chat button on detail chat
     */
    public void clickFilterChat() {
        appium.tapOrClick(filterChatSelect);
    }

    /**
     * select filter chat
     * @param filter desired text
     * @return Belum dibaca or Semua
     */
    public void selectFilterChat(String filter){
        String xpathLocator = "";
        if(Constants.MOBILE_OS == Platform.ANDROID){
            xpathLocator = "//*[@text='"+filter+"']";
        }else {
            xpathLocator = "//*[@name='"+filter+"']";
        }
        MobileElement element = (MobileElement) driver.findElementByXPath(xpathLocator);
        appium.tapOrClick(element);
    }

    /**
     * Click Filter Tampilkan button on filter chat
     */
    public void clickTampilkanButton(){
        appium.tapOrClick(tampikanButton);
    }

    /**
     * set webelement for survey textbox
     * @param index
     * @return element
     */
    public WebElement surveyTextBox(int index){
        return driver.findElementByXPath("(//android.widget.EditText)["+index+"]");
    }

    /**
     * is element with text enable
     * @param element desired element
     * @return visible true otherwise false
     */
    public boolean surveyTextBoxEnable(WebElement element){
        return appium.isElementEnabled(element) != null;
    }

    /**
     * Get survey textbox
     * @return element string
     */
    public String getSurveyTextBox(WebElement element){
        return appium.getText(element);
    }

    /**
     * Click Broadcast button on list chat
     */
    public void clickBroadcastButton() {
        appium.tapOrClick(broadcastButton);
    }

    /**
     * Check if the Chat title is appears
     *
     * @return status chat appears / not
     */
    public boolean isChatListTitleAppers() {
        return appium.waitInCaseElementVisible(chatListTitletext, 3) != null;
    }

    /**
     * Check if pusat bantuan button is present in viewport
     * @return true or false
     */
    public boolean isChatTitlePresent(String text) {
        String textEl = "//*[@name='"+text+"']";
        MobileElement element = (MobileElement) driver.findElementByXPath(textEl);
        return appium.waitInCaseElementVisible(element, 5) != null;
    }

    /**
     * Check chat title with scroll down
     */
    public void isChatTitlePresentAfterScrollDown(String tenantName) {
        int i = 0;
        while (!isChatTitlePresent(tenantName)){
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
    }

    /**
     * Check chat title with scroll up
     */
    public void isChatTitlePresentAfterScrollUp(String tenantName) {
        int i = 0;
        while (!isChatTitlePresent(tenantName)){
            appium.scroll(0.5, 0.2, 0.8, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
    }
    /**
     * Get chat message name text
     *
     * @return chat message name
     */
    public Boolean getChatMessage() {
        return appium.waitInCaseElementVisible(getChatMessage, 10) != null;
    }

}
