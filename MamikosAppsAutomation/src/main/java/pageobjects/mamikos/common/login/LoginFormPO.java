package pageobjects.mamikos.common.login;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class LoginFormPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public LoginFormPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver, this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    private By mobileNumberTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("phoneNumberEditText"): By.xpath("//*[@name='Nomor Handphone']/following-sibling::XCUIElementTypeTextField");

    private By passwordTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("passwordEditText"): By.xpath("//*[@name='Password']/following-sibling::XCUIElementTypeSecureTextField");

    private By loginButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("loginOwnerButton"): By.xpath("(//XCUIElementTypeButton[@name='Login'])[2]");

    @AndroidFindBy(id = "textinput_error")
    private WebElement errorMessage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='sendOtpButton']")
    @AndroidFindBy(id = "forgetPasswordButton")
    private WebElement forgetPasswordButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Masukkan kode verifikasi yang kami kirimkan melalui SMS ke nomor:\"]\n")
    @AndroidFindBy(id = "verificationTextView")
    private WebElement verificationText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='icon not visible']")
    @AndroidFindBy(id = "text_input_password_toggle")
    private WebElement passwordToggle;

    private By doneButton = By.xpath("//XCUIElementTypeButton[@name='Done']");


    /**
     * Fill out login form and click on 'LOGIN' button
     *
     * @param phoneValue    phone value
     * @param passwordValue password value
     */
    public void fillFormAndClickOnLoginButton(String phoneValue, String passwordValue) {
        appium.waitInCaseElementVisible(mobileNumberTextbox,5);
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.tapOrClick(mobileNumberTextbox);
            String mobileNumberTextbox = "phoneNumberEditText";
            appium.enterText(By.id(mobileNumberTextbox), "081362464341", false);
            appium.tapOrClick(passwordTextbox);
            String passwordTextbox = "passwordEditText";
            appium.enterText(By.id(passwordTextbox), "1d0lt3stb4ru", false);
        }else {
        appium.tapOrClick(mobileNumberTextbox);
        appium.enterText(mobileNumberTextbox, phoneValue, true);
        appium.tapOrClick(doneButton);
        appium.enterText(passwordTextbox, passwordValue, false);
        appium.tapOrClick(doneButton);
        }
        appium.clickOn(loginButton);
    }

    /**
     * fill login form po for owner
     * @param phoneValue desired phone number
     * @param passwordValue credential password
     * @param clearText boolean clear text before phone number and password input
     */
    public void fillFormAndClickOnLoginButton(String phoneValue, String passwordValue, boolean clearText) {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.waitInCaseElementVisible(mobileNumberTextbox,5);
            appium.clickOn(mobileNumberTextbox);
            appium.enterText(mobileNumberTextbox, phoneValue, clearText);
            appium.enterText(passwordTextbox, passwordValue, clearText);
            appium.hideKeyboard();
            appium.clickOn(loginButton);
        }else{
            appium.waitInCaseElementVisible(mobileNumberTextbox,5);
            appium.tapOrClick(mobileNumberTextbox);
            appium.enterText(mobileNumberTextbox, phoneValue, clearText);
            appium.tapOrClick(doneButton);
            appium.tapOrClick(passwordTextbox);
            appium.enterText(passwordTextbox, passwordValue, clearText);
            appium.tapOrClick(doneButton);
            appium.clickOn(loginButton);
        }
    }

    /**
     * Fill out login form and click on password toggle button
     * @param phoneValue phone value
     * @param passwordValue password value
     */
    public void fillFormAndClickOnPasswordToggle(String phoneValue, String passwordValue){
        appium.enterText(mobileNumberTextbox, phoneValue, false);
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.tapOrClick(doneButton);
        }
        appium.enterText(passwordTextbox, passwordValue, false);
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.tapOrClick(doneButton);
        }
        appium.tapOrClick(passwordToggle);
    }

    /**
     * Get password text
     * @return password
     */
    public String getPasswordText(){
        return appium.getText(passwordTextbox);
    }

    /**
     * Get Password Attribute
     * @return true or false
     */
    public Boolean getPasswordAttribute(){
        return appium.getElementAttributeValue(passwordTextbox, "password").equalsIgnoreCase("false") &&
                appium.isElementAtrributePresent(passwordTextbox, "password");
    }

    /**
     * Fill Owner Mobile Number and Click on Forget Password Button
     *
     * @param ownerMobileNumber
     */
    public void fillMobileNumberAndClickOnKirimButton(String ownerMobileNumber) {
        appium.waitInCaseElementVisible(mobileNumberTextbox, 5);
        appium.enterText(mobileNumberTextbox, ownerMobileNumber, false);
        if(Constants.MOBILE_OS == Platform.IOS){
            appium.tapOrClick(doneButton);
        }
        appium.tapOrClick(forgetPasswordButton);
    }

	/**
	 * Get error message from login page
	 * @return error text
	 */
	public String getErrorMessage(String iosErrorMessage) {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            return appium.getText(errorMessage);
        } else {
            return appium.getText(By.xpath("//XCUIElementTypeStaticText[@value='"+iosErrorMessage+"']"));
        }
    }


    /**
     * Get Verification text from OTP screen
     * @return verification text
     */
    public String getVerificationText() {
        appium.waitInCaseElementVisible(verificationText, 5);
        return appium.getText(verificationText);
    }

}
