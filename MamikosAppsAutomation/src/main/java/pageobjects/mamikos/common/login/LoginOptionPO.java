package pageobjects.mamikos.common.login;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class LoginOptionPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public LoginOptionPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver, this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    //Owner
//    @iOSXCUITFindBy(id = "Pemilik Kos")
//    @AndroidFindBy(id = "ownerLoginComponent")
//    private WebElement adOwnerButton;
    private By adOwnerButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("ownerLoginComponent"): By.id("Pemilik Kos");


    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Login\"]")
    @AndroidFindBy(xpath = "//*[@text='LOG IN']")
    private WebElement logInLink;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Lupa password?']")
    @AndroidFindBy(id = "forgotPasswordTextView")
    private WebElement forgotPasswordLink;

    //Tenant
    private By tenantLoginButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("tenantLoginComponent"): By.id("Pencari Kos");

    private By continueToFacebookButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("loginFacebookView"): By.xpath("//XCUIElementTypeStaticText[@name='Sign in with Facebook']");

    @iOSXCUITFindBy(accessibility = "ic close black")
    private WebElement closePopUp;

    @FindBy(id = "phoneOrEmailEditText")
    private WebElement inputPhoneNumber;

    @FindBy(id = "passwordEditText")
    private WebElement inputPhoneNumberPassword;

    @AndroidFindBy(id = "com.git.mami.kos:id/loginTenantButton")
    private WebElement loginTenantPhoneNumberButton;

    //Owner

    /**
     * Click on 'Add Owner' button
     */
    public void clickOnAdOwnerButton() {
        appium.waitTillElementIsClickable(adOwnerButton);
        appium.tapOrClick(adOwnerButton);
    }

    /**
     * Click on Forgot Password Link
     */
    public void clickOnForgotPasswordLink(){
        if(appium.waitInCaseElementVisible(forgotPasswordLink, 5) == null) {
            appium.scrollToElementByText("Lupa password?");
            appium.tapOrClick(forgotPasswordLink);
        }
        else {
            appium.tapOrClick(forgotPasswordLink);
        }
    }

    //Tenant

    /**
     * Click on 'Tenant' button
     */
    public void clickOnTenantButton() {
        appium.tapOrClick(tenantLoginButton);
    }

    /**
     * Click on 'Continue To Facebook' button
     */
    public void clickOnContinueToFacebookButton() throws InterruptedException {
        appium.hardWait(5);
        appium.scroll(0.5, 0.8, 0.2, 2000);
        appium.tap(128,244);
    }

    /**
     * Click on 'x' button to close pop up
     */
    public void clickClosePopUp() {
        appium.tapOrClick(closePopUp);
    }

    /**
     * Set tenant phone number
     * @param phoneNumber tenant phone number e.g 087708777777
     */
    public void setTenantPhoneNumber(String phoneNumber) throws InterruptedException {
        appium.enterText(inputPhoneNumber, phoneNumber, true);
    }

    /**
     * Set tenant phone number password
     * @param password tenant's password
     */
    public void setTenantPhoneNumberPassword(String password) throws InterruptedException {
        appium.enterText(inputPhoneNumberPassword, password, true);
    }

    /**
     * Click on tenant login button
     */
    public void clickOnLoginButton() {
        appium.clickOn(loginTenantPhoneNumberButton);
    }
}
