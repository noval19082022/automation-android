package pageobjects.mamikos.common.login;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class LoginPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public LoginPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    private By loginTenantButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("loginTenantButton"): By.xpath("(//XCUIElementTypeButton[@name='Login'])[2]");

    private By mamiHelpButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text=\"MamiHelp\"]"): By.xpath("//*[@name='MamiHelp']/preceding-sibling::XCUIElementTypeButton");

    @AndroidFindBy(id = "com.git.mami.kos:id/termTextView")
    private WebElement termTextLink;

    @AndroidFindBy(id = "titleLoginTextView")
    private WebElement loginPageTitle;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Pencari Kos\"]")
    @AndroidFindBy(id = "tenantLoginComponent")
    private WebElement tenantButton;

    private By phoneTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("phoneOrEmailEditText"): By.xpath("//*[@name='Nomor Handphone']/following-sibling::XCUIElementTypeTextField");

    private By passwordTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("passwordEditText"): By.xpath("//*[@name='Password']/following-sibling::XCUIElementTypeSecureTextField");


    private By doneButton = By.xpath("//XCUIElementTypeButton[@name='Done']");

    private By textInvalidLogin = Constants.MOBILE_OS == Platform.ANDROID ? By.id("textinput_error"): By.xpath("//*[@name='Password']/following-sibling::XCUIElementTypeStaticText");

    private By facebookButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("loginFacebookView"): By.xpath("");

    private By emailTextBox = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.EditText[@content-desc=\"Nama Pengguna\"]"): By.xpath("");

    private By passwordTextboxFb = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.EditText[@content-desc=\"Kata Sandi\"]"): By.xpath("");

    private By loginFbButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.view.ViewGroup[@content-desc=\"Masuk\"]"): By.xpath("");
    private By emailTextBoxWebview = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Lupa Kata Sandi?']/preceding::android.widget.EditText[2]"): By.xpath("");
    private By passwordTextboxFbWebview = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Lupa Kata Sandi?']/preceding::android.widget.EditText[1]"): By.xpath("");
    private By loginFbButtonWebview = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Lupa Kata Sandi?']/preceding::android.widget.Button"): By.xpath("");
    private By continueButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Batalkan']/preceding::android.widget.Button"): By.xpath("");
    /**
     * Click on mami help button
     */
    public void clickOnMamiHelpButton() {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.scrollToElementByText("MamiHelp");
        }
            appium.tapOrClick(mamiHelpButton);
    }

    /**
     * Login button is displayed or not ?
     *
     * @return true or false
     */
    public boolean isLoginButtonDisplayed() {
        return appium.waitInCaseElementVisible(tenantButton, 5)!=null;
    }

    /**
     * Check term and conditions text is Visible
     * @return status
     */
    public boolean isTermTextVisible(){
        return appium.waitInCaseElementVisible(termTextLink, 3) != null;
    }

    /**
     * Check login page title displayed or not
     * @return true or false
     */
    public boolean isLoginPageTitleDisplayed(){
        return appium.waitInCaseElementVisible(loginPageTitle, 5)!=null;
    }

    /**
     * Login as Tenant user to application
     *
     * @param number    phone
     * @param password password
     * @throws InterruptedException
     */
    public void loginAsTenantToApplication(String number, String password) throws InterruptedException {
        appium.enterText(phoneTextbox, number, false);
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.tapOrClick(doneButton);
        }
        appium.enterText(phoneTextbox, password, false);
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.tapOrClick(doneButton);
        }
        appium.clickOn(loginTenantButton);
        appium.hardWait(5);
    }

    public void loginAsTenantToApplication(String number, String password, boolean clearText) throws InterruptedException {
        appium.enterText(phoneTextbox, number, clearText);
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.tapOrClick(doneButton);
        }
        appium.enterText(passwordTextbox, password, clearText);
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.tapOrClick(doneButton);
        }
        appium.clickOn(loginTenantButton);
        appium.hardWait(5);
    }

    public String getInvalidLoginMessageText() {
        return appium.getText(textInvalidLogin);
    }

    /**
     * Click on tenant login facebook button
     */
    public void clickOnFacebookButton() throws Exception {
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.clickOn(facebookButton);
        }else {
            appium.tap(128,244);
        }
    }

    /**
     * input email and password facebook
     */
    public void loginAsTenantWithFacebook(String email, String password, boolean clearText) throws InterruptedException {
        appium.hardWait(10);
        appium.enterText(emailTextBoxWebview, email, clearText);
        appium.enterText(passwordTextboxFbWebview, password, clearText);
        appium.clickOn(loginFbButtonWebview);
        appium.clickOn(continueButton);

    }
    /**
     * clear data FB
     */
    public void clearDataFb() throws Exception {
        appium.ClearFacebookData();
    }

    /**
     * clear data chrome
     */
    public void clearWebviewAndroid() throws Exception {
        appium.ClearWebviewFacebook();
    }
}
