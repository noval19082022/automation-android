package pageobjects.mamikos.common.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.logging.Logger;

public class ProfilePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;
    private static final Logger LOGGER = Logger.getLogger(ProfilePO.class.getName());

    public ProfilePO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

    @iOSXCUITFindBy(accessibility = "Pengaturan")
    @AndroidFindBy(xpath = "//*[@text='Pengaturan']")
    private WebElement settingsOption;

    private By bookingHistoryMenu = Constants.MOBILE_OS == Platform.ANDROID ? By.id("historyBookingTextView"): By.xpath("//*[@name='Riwayat pengajuan sewa']/preceding-sibling::XCUIElementTypeButton");

    @AndroidFindBy(id = "countReminderPaymentTextView")
    private WebElement paymentReminderText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"ic close black\"]")
    private WebElement closeButton;

    @AndroidFindBy(xpath = "//*[@text='Kontrak']")
    private WebElement contractMenu;

    @AndroidFindBy(id = "termsAndConditionsTextView")
    private WebElement termConditionButton;

    @AndroidFindBy(id = "privacyPolicyTextView")
    private WebElement privacyPolicyButton;

    @AndroidFindBy(id = "progressBarWeb")
    private WebElement loadingWebView;

    @AndroidFindBy(uiAutomator = "//*[@text='Pengaturan']")
    private WebElement settingsLabel;

    @AndroidFindBy(id="logoutTextView")
    private WebElement logoutButton;

    @AndroidFindBy(id= "exitTextView")
    private WebElement exitButton;

    @AndroidFindBy(id= "invoiceButton")
    private WebElement billsTabMenu;

    @AndroidFindBy(id= "contractButton")
    private WebElement contractTabMenu;

    @AndroidFindBy(id= "paymentBillButton")
    private WebElement paymentBillButton;

    @AndroidFindBy(id= "notifTitleTextView")
    private WebElement notifDropdown;

    private By tenantName = Constants.MOBILE_OS == Platform.ANDROID ? By.id("fullNameTextView") : By.id("nameLabel");

    private By tenantPhoneNumber = Constants.MOBILE_OS == Platform.ANDROID ? By.id("phoneNumberTextView") : By.id("phoneLabel");

    @AndroidFindBy(id = "actionBookingButton")
    private WebElement kosSayaButton;

    @AndroidFindBy(id = "roomNameTextView")
    private WebElement kosSayaPage;


    /**
     * Click On settings option
     */
    public void clickOnSettingsOption() throws InterruptedException {
        appium.scroll(0.5, 0.8, 0.2, 2000);
        if (appium.waitInCaseElementVisible(settingsOption, 3) != null) {
            appium.tapByElementLocation(settingsOption);
            appium.hardWait(3);
        } else {
            appium.tap(500, 500);
            appium.hardWait(1);
        }
    }

    /**
     * Click on Booking History menu
     */
    public void clickOnBookingHistorytButton() throws InterruptedException {
        appium.hardWait(5);
        appium.tapOrClick(bookingHistoryMenu);
    }

    /** Get payment reminder text
     * @return  payment reminder text
     */
    public String getPaymentReminderText()
    {
        return appium.getText(paymentReminderText);
    }

    /** Click on contract menu
     */
    public void clickOnContractMenu(){
        appium.tapOrClick(contractMenu);
    }

    /**
     * Click term and condition Button
     */
    public void clickTermCondition() throws InterruptedException {
        appium.tapOrClick(termConditionButton);
        appium.waitTillElementsCountIsEqualTo(By.id("progressBarWeb"), 0);
        appium.hardWait(3);
    }

    /**
     * Click privacy policy Button
     * @throws InterruptedException
     */
    public void clickPrivacyPolicy() throws InterruptedException {
        appium.tapOrClick(privacyPolicyButton);
        appium.waitTillElementsCountIsEqualTo(By.id("progressBarWeb"), 0);
        appium.hardWait(3);
    }

    /**
     * User logout
     */
    public void userLogout() {
        appium.scrollToElementByText("Pengaturan");
        appium.tapOrClick(settingsLabel);
        appium.tapOrClick(logoutButton);
        appium.tapOrClick(exitButton);
    }

    /**
     * Click on bills tab menu kon kost saya
     */
    public void clickOnBillsTabMenu() {
        appium.tapOrClick(billsTabMenu);
    }

    /**
     * Click on contract tab menu kon kost saya
     */
    public void clickOnContractTabMenu() {
        appium.tapOrClick(contractTabMenu);
    }

    /**
     * Click on payment button for bayar tagihan
     */
    public void clickOnPaymentBillButton() {
        appium.tapOrClick(paymentBillButton);
    }

    /**
     * Click on checkbox settings
     * @param option is option menu
     */
    public void clickOnCheckboxSettings(String option){
        String element = "//android.widget.TextView[@text='"+ option +"']/following-sibling::android.widget.CheckBox";
        driver.findElement(By.xpath(element)).click();
    }

    /**
     * Click on bills tab menu kon kost saya
     */
    public void clickOnNotificationDropdown(){
        appium.tapOrClick(notifDropdown);
    }

    /**
     * Get profile name
     */
    public String getTenantName() {
        return appium.getText(tenantName);
    }

    /**
     * Get Tenant Phone Number
     * @return phone number
     */
    public String getTenantPhone(){
        return appium.getText(tenantPhoneNumber);
    }

    /**
     * Click kost saya button
     */
    public void clickKosSayaButton() throws InterruptedException{
        appium.hardWait(3);
        appium.tapOrClick(kosSayaButton);
    }

    /**
     * Verify page kos saya
     */
    public boolean isKosSayaPage() {
        return appium.isElementDisplayed(kosSayaPage);
    }

}
