package pageobjects.mamikos.common.profile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class SettingsPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public SettingsPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

		@iOSXCUITFindBy(xpath= "//*[@name='ic_setting_logout']/preceding-sibling::XCUIElementTypeButton")
	    @AndroidFindBy(id="logoutTextView")
	    private WebElement logoutButton;

	    
	    /**
	     * Click on logout button
	     */
	    public void clickOnLogoutButton() throws InterruptedException {
	    	appium.hardWait(3);
	    	appium.tapOrClick(logoutButton);
	    } 	
	    
	    
}
