package pageobjects.mamikos.common.register;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class RegisterPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RegisterPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Daftar sekarang']")
    @AndroidFindBy(xpath = "//*[@text='Daftar sekarang']")
    private WebElement registerAccountButton;


    /**
     * Click on Register Account Button from Login Popup
     */
    public void clickOnRegisterAccountButton() {
        appium.tapOrClick(registerAccountButton);
    }


}
