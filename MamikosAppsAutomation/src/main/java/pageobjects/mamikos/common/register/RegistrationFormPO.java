package pageobjects.mamikos.common.register;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class RegistrationFormPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RegistrationFormPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver,this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Daftar Pemilik Kos']")
    @AndroidFindBy(id = "personalDataTextView")
    private WebElement pageHeaderText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Log in']")
    @AndroidFindBy(id = "loginTextView")
    private WebElement loginButtonFromRegisterPage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Nama Lengkap']")
    @AndroidFindBy(id = "fullNameEditText")
    private WebElement ownerFullNameTextBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Nomor Handphone']")
    @AndroidFindBy(id = "phoneNumberEditText")
    private WebElement ownerMobieNumberTextBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField[@value='Alamat Email']")
    @AndroidFindBy(id = "emailEditText")
    private WebElement ownerEmailTextBox;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeSecureTextField[@value='Password']")
    @AndroidFindBy(id = "passwordEditText")
    private WebElement ownerPasswordTextBox;

    @AndroidFindBy(id = "text_input_password_toggle")
    private WebElement eyeIcon;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Daftar']")
    @AndroidFindBy(id = "ownerRegisterButton")
    private WebElement ownerRegisterButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Done\"]")
    private WebElement doneButton;

    @AndroidFindBy(id = "verify1EditText")
    private WebElement otpTextBox1;

    @AndroidFindBy(id = "verify2EditText")
    private WebElement otpTextBox2;

    @AndroidFindBy(id = "verify3EditText")
    private WebElement otpTextBox3;

    @AndroidFindBy(id = "verify4EditText")
    private WebElement otpTextBox4;

    @AndroidFindBy(id = "verifyButton")
    private WebElement verifyButton;

    @AndroidFindBy(id = "textinput_error")
    private WebElement errorMessage;

    @AndroidFindBy(id = "codeVerificationView")
    private WebElement codeVerificationContainer;

    @AndroidFindBy(id = "requestCodeButton")
    private WebElement requestCodeButton;

    @AndroidFindBy(id = "verifyMessageTextView")
    private WebElement requestVerificationText;



    /**
     * Is Request Code Button Present?
     * @return true or false
     */
    public Boolean isRequestCodeButtonPresent(){
        return appium.waitInCaseElementVisible(requestCodeButton, 65) != null;
    }

    /**
     * Is Resend Verification Code Countdown Present
     * @return true or false
     */
    public Boolean isResendVerificationCodeCountdownPresent(){
        return appium.waitInCaseElementVisible(requestVerificationText, 5) != null;
    }

    /**
     * Click on Request Code Button
     */
    public void clickOnRequestCodeButton(){
        appium.clickOn(requestCodeButton);
    }

    /**
     * is Each of OTP text box Present?
     * @return true or false
     */
    public Boolean isEachOfOtpTextBoxPresent(){
        return appium.waitInCaseElementVisible(otpTextBox1, 5) != null &&
                appium.waitInCaseElementVisible(otpTextBox2, 5) != null &&
                appium.waitInCaseElementVisible(otpTextBox3, 5) != null &&
                appium.waitInCaseElementVisible(otpTextBox4, 5) != null;
    }

    /**
     * Get User Phone Number Text
     * @param phone
     * @return Phone Number
     */
    public String getPhoneNumberText(String phone){
        return appium.getText(By.xpath("//*[contains(@text, '"+phone+"')]"));
    }

    /**
     * is Code Verification Container Present ?
     * @return true or false
     */
    public Boolean isCodeVerificationContainerPresent(){
        return appium.waitInCaseElementVisible(codeVerificationContainer, 5) != null;
    }

    /**
     * Get Error Messages
     * @return error message
     */
    public String getErrorMessages(){
        return appium.getText(errorMessage);
    }

    /**
     * Get Page header text
     * @return page header
     */
    public String getPageHeaderText(){
        return appium.getText(pageHeaderText);
    }

    /**
     * Click on Login Button from Register page
     */
    public void clickOnLoginButton(){
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByText("Log in");
            appium.tapOrClick(loginButtonFromRegisterPage);
        } else {
            appium.scrollDown(1,2);
            appium.tapOrClick(doneButton);
            appium.tapOrClick(loginButtonFromRegisterPage);
        }
    }

    /**
     * Fill owner Registration Form
     * @param ownerName name
     * @param ownerMobileNumber mobile number
     * @param ownerEmailId email id
     * @param ownerPassword password
     */
    public void fillOwnerDetails(String ownerName, String ownerMobileNumber, String ownerEmailId, String ownerPassword){
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.enterText(ownerFullNameTextBox, ownerName, false);
            appium.enterText(ownerMobieNumberTextBox, ownerMobileNumber, false);
            appium.enterText(ownerEmailTextBox, ownerEmailId, false);
            appium.enterText(ownerPasswordTextBox, ownerPassword, false);
        } else {
            appium.enterText(ownerFullNameTextBox, ownerName, false);
            appium.tapOrClick(doneButton);
            appium.enterText(ownerMobieNumberTextBox, ownerMobileNumber, false);
            appium.tapOrClick(doneButton);
            appium.enterText(ownerEmailTextBox, ownerEmailId, false);
            appium.tapOrClick(doneButton);
            appium.enterText(ownerPasswordTextBox, ownerPassword, false);
            appium.tapOrClick(doneButton);
        }
    }

    /**
     * Click on Registration Button
     */
    public void clickOnRegisterButton(){
        appium.tapOrClick(ownerRegisterButton);
    }

    /**
     * Enter details in registration form
     * @param ownerName name
     * @param ownerNumber number
     * @param ownerEmailId email id
     */
    public void fillFormWithoutPassword(String ownerName, String ownerNumber, String ownerEmailId){
        appium.enterText(ownerFullNameTextBox, ownerName, false);
        appium.enterText(ownerMobieNumberTextBox, ownerNumber, false);
        appium.enterText(ownerEmailTextBox, ownerEmailId, false);
    }

    /**
     * Verify  register button is shown disable
     * @return register button
     */
    public boolean isRegisterButtonDisabled() {
        return appium.isElementEnabled(ownerRegisterButton);
    }

    /**
     * Enter OTP data
     * @param code1 code1
     * @param code2 code2
     * @param code3 code3
     * @param code4 code4
     * @throws InterruptedException exception
     */
    public void enterOTP(String code1, String code2 ,String code3, String code4) throws InterruptedException {
        appium.hardWait(3);
        appium.enterText(otpTextBox1, code1, false);
        appium.enterText(otpTextBox2, code2, false);
        appium.enterText(otpTextBox3, code3, false);
        appium.enterText(otpTextBox4, code4, false);
        appium.tapOrClick(verifyButton);
    }

    /**
     * Click On Eye Icon
     */
    public void clickOnEyeIcon(){
        appium.tapOrClick(eyeIcon);
    }

    /**
     * Get Password text
     * @return password
     */
    public String getPasswordText(){
        return appium.getText(ownerPasswordTextBox);
    }
}
