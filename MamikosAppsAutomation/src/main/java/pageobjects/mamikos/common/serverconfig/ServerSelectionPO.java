package pageobjects.mamikos.common.serverconfig;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import utilities.AppiumHelpers;
import utilities.Constants;

public class ServerSelectionPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public ServerSelectionPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);

		//This initElements method will create all Android Elements
		PageFactory.initElements(driver, this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

	private By keyDropdownField = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='kay']"): By.xpath("//*[@value='kay10']");

	private By payDropdownField = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='pay']"): By.xpath("//*[@value='pay']");

	private By confirmiOSButton = By.xpath("//*[@name='confirmButton']");

	@iOSXCUITFindBy(xpath = "//*[@value='api/v2']")
	private WebElement apiDropdownField;

	private By confirmButton = By.xpath("//*[@text='CONFIRM']");
		
	private By doneButton = By.xpath("//XCUIElementTypeButton[@name='Done']");

	@iOSXCUITFindBy(className="XCUIElementTypePickerWheel")
	WebElement pickerWheel;

	/**
	     * Select Server Configuration details i.e. Key/Pay and click on Confirm button
	     * @param keyDropdownValue key dropdown value
	     * @param payDropdownValue pay dropdown value
	     */
	    public void selectServerDetailsAndClickOnConfirmButton(String keyDropdownValue,String payDropdownValue) throws InterruptedException {
			appium.hardWait(5);
	    	appium.clickOn(keyDropdownField);
	    	appium.clickOn(By.xpath("//*[@text='" + keyDropdownValue + "']"));
	    	
	    	appium.clickOn(payDropdownField);
	    	appium.waitTillElementIsVisible(By.xpath("//*[@text='" + payDropdownValue + "']"), 2);
	    	appium.clickOn(By.xpath("//*[@text='" + payDropdownValue + "']"));
	    	
	    	appium.clickOn(confirmButton);
	    }

		/** Select Server Configuration details i.e. Key/Pay and click on Confirm button
		 * @param keyDropdownValue key dropdown value
		 * @param payDropdownValue pay dropdown value
		 * @throws InterruptedException
		 */
		public void selectServerForiOS(String keyDropdownValue, String payDropdownValue) throws InterruptedException {
			appium.waitTillElementIsClickable(keyDropdownField);
			appium.tapOrClick(keyDropdownField);
			MobileElement elKey = (MobileElement) driver.findElement(By.className("XCUIElementTypePickerWheel"));
			elKey.setValue(keyDropdownValue);
			appium.tapOrClick(doneButton);
			appium.tapOrClick(payDropdownField);
			MobileElement elPay = (MobileElement) driver.findElement(By.className("XCUIElementTypePickerWheel"));
			elPay.setValue(payDropdownValue);
			appium.tapOrClick(doneButton);
			appium.tapOrClick(confirmiOSButton);
		}

}
