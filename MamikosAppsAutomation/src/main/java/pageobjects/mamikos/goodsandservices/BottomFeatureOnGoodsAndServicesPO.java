package pageobjects.mamikos.goodsandservices;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class BottomFeatureOnGoodsAndServicesPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public BottomFeatureOnGoodsAndServicesPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver,this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "filterView")
    private WebElement filterIcon;

    @AndroidFindBy(id = "sortFilterView")
    private WebElement sortIcon;

    @AndroidFindBy(id = "displayView")
    private WebElement mapIcon;

    @AndroidFindBy(id = "minimumPriceEditText")
    private WebElement minPriceTextBox;

    @AndroidFindBy(id = "maximumPriceEditText")
    private WebElement maximumPriceTextBox;

    @AndroidFindBy(id = "confirmFilterTextView")
    private WebElement searchButton;

    @AndroidFindBy(id = "resetFilterTextView")
    private WebElement resetButton;

    /**
     * Click on 'show map'
     */
    public void clickOnMapIcon() {
        appium.tapOrClick(mapIcon);
    }

    /**
     * Click on 'show map'
     */
    public void clickOnFilterIcon() {
        appium.tapOrClick(filterIcon);
    }

    /**
     * Check map view button is present or not
     * @return true or false
     */
    public boolean mapIconIsPresent(){
        return appium.isElementDisplayed(mapIcon);
    }

    /**
     * Check filter view button is present or not
     * @return true or false
     */
    public boolean filterIconIsPresent() {
        return appium.isElementDisplayed(filterIcon);
    }

    /**
     * Check map sort button is present or not
     * @return true or false
     */
    public boolean sortIconIsPresent() {
        return appium.isElementDisplayed(sortIcon);
    }

    /**
     * Input minimum price
     * @param minPrice is minimum price goods and services
     */
    public void inputMinPrice(String minPrice) {
        appium.enterText(minPriceTextBox, minPrice, true);
    }

    /**
     * Input minimum price
     * @param maxPrice is maximum price goods and services
     */
    public void inputMaxPrice(String maxPrice) {
        appium.enterText(maximumPriceTextBox, maxPrice, true);
    }

    /**
     * Click on search button
     */
    public void clickOnSearchButton() {
        appium.clickOn(searchButton);
    }

    /**
     * Click on reset button
     */
    public void resetButton() {
        appium.clickOn(resetButton);
    }

    /**
     * Get minimum price range
     */
    public String getMinimumPriceRange() {
        return appium.getText(minPriceTextBox);
    }

    /**
     * Get maximum price range
     */
    public String getMaximumPriceRange() {
        return appium.getText(maximumPriceTextBox);
    }
}
