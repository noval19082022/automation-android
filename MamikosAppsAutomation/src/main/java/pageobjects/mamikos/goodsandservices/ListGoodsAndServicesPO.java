package pageobjects.mamikos.goodsandservices;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class ListGoodsAndServicesPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ListGoodsAndServicesPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(xpath = "(//*[@resource-id='com.git.mami.kos:id/tv_price_mp'])[1]")
    private WebElement firstIndexPriceLabel;

    @AndroidFindBy(xpath = "(//*[@resource-id='com.git.mami.kos:id/tv_name_mp'])[1]")
    private WebElement firstIndexTitleLabel;

    /**
     * Get price from first top list goods and services on screen
     */
    public String getPriceGoodsAndServices() {
        return appium.getText(firstIndexPriceLabel);
    }

    /**
     * Verify element title of goods and services is present
     * @return true or false
     */
    public boolean GoodsAndServicesTitleIsPresent() {
        return appium.isElementPresent(By.xpath("//*[@resource-id='com.git.mami.kos:id/tv_name_mp']"));
    }


    /**
     * Verify element price of goods and services is present
     * @return true or false
     */
    public boolean GoodsAndServicesPriceIsPresent() {
        return appium.isElementPresent(By.xpath("//*[@resource-id='com.git.mami.kos:id/tv_price_mp']"));
    }

    /**
     * Verify element label of goods and services is present
     * @return true or false
     */
    public boolean GoodsAndServicesLabelIsPresent() {
        return appium.isElementPresent(By.xpath("//*[@resource-id='com.git.mami.kos:id/gv_labelMarket']"));
    }

    /**
     * Verify element image of goods and services is present
     * @return true or false
     */
    public boolean GoodsAndServicesImageIsPresent() {
        return appium.isElementPresent(By.xpath("//*[@resource-id='com.git.mami.kos:id/iv_photo_mp']"));
    }

    public String getTitleGoodsAndServices() {
        return appium.getText(firstIndexTitleLabel);
    }
}
