package pageobjects.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

import java.util.List;

public class DashboardPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public DashboardPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    @AndroidFindBy(id = "mamipointButton")
    private WebElement getPointButton;

    private By managePriceButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Atur Harga']/following::android.widget.TextView[1]") : By.id("Atur Harga");

    @FindBy(id="dashboardSectionRecyclerView")
    private WebElement dashboardSection;


    //---- Financial Statement Element----//
    @FindBy(xpath = "//*[@text='Lihat laporan keuangan']")
    private WebElement seeFinancialStatements;

    private By textTitleIncome = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Pendapatan']"): By.id("");

    @FindBy(id = "captionIncomeSection")
    private WebElement textSubtitleIncome;

    @FindBy(id = "titleTotalIncome")
    private WebElement textTotalIncome;

    @FindBy(id = "titleMonthlyIncome")
    private WebElement textMonthlyIncome;
    
    @FindBy(id = "valueTotalIncome")
    private WebElement totalIncomePrice;

    @FindBy(id = "valueMonthlyIncome")
    private WebElement monthlyIncomePrice;

    @FindBy(id = "monthText")
    private WebElement incomeFilterText;

    @FindBy(id ="montylyDropdownIcon")
    private WebElement filterMonthlyIncome;

    private final By filterMonthList = By.id("rootSelectionView");
    //---- Financial Statement Element----//

    private By availabilityRoomButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Atur Ketersediaan Kamar']"): By.id("Atur Ketersediaan Kamar");


    /**
     * tap on get point button / dapatkan point button
     */
    public void tapOnGetPointButton() {
        appium.tapOrClick(getPointButton);
    }

    /**
     * Check if update room and price button is present in viewport
     * @return true or false
     */
    public boolean isUpdateRoomPriceButtonPresent() {
        return appium.waitInCaseElementVisible(managePriceButton, 1) != null;
    }

    /**
     * tap on update room and price button
     */
    public void tapOnUpdateRoomAndPrice() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isUpdateRoomPriceButtonPresent());
        appium.tapOrClick(managePriceButton);
    }

    /**
     * check if is in owner dashboard section
     * @return true if user is in owner dashboard otherwise false
     */
    public boolean isInOwnerDashboard() {
        return appium.waitInCaseElementVisible(dashboardSection, 2) != null;
    }

    /**
     * Navigate back to owner dashboard
     */
    public void backToOwnerDashboard() {
        do {
            appium.back();
        }
        while (!isInOwnerDashboard());
    }

    /**
     * Check if financial statement is visible
     * @return visible true otherwise false
     */
    public boolean isFinancialStatementsVisible() {
        return appium.waitInCaseElementVisible(seeFinancialStatements, 15) != null;
    }

    /**
     * Scroll to financial statement
     */
    public void scrollToFinancialStatement() {
        int maxLoop = 0;
        do {
            if (maxLoop == 2) {
                break;
            }
            appium.scroll(0.5, 0.8, 0.4, 2000);
            maxLoop++;
        }while (!isFinancialStatementsVisible());
    }

    /**
     * Check if title label is present in viewport
     * @return true or false
     */
    public boolean isTitleLabelPresent() {
        return appium.waitInCaseElementVisible(textTitleIncome, 1) != null;
    }

    /**
     * Get title text on income box
     * @return string data type
     */
    public String getIncomeTitle() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isTitleLabelPresent());
        return appium.getText(textTitleIncome);
    }

    /**
     * Get subtitle text on income box
     * @return String data type
     */
    public String getIncomeSubtitle() {
        return appium.getText(textSubtitleIncome);
    }

    /**
     * Get total income title text
     * @return String data type
     */
    public String getTotalIncomeSubtitle() {
        appium.waitInCaseElementVisible(textTotalIncome, 5);
        return appium.getText(textTotalIncome);
    }

    /**
     * Get total income price
     * @return integer data type
     */
    public int getTotalIncomePrice() throws InterruptedException{
        appium.hardWait(3);
        return JavaHelpers.extractNumber(appium.getText(totalIncomePrice));
    }

    /**
     * Get monthly income subtitle text
     * @return String data type
     */
    public String getMonthlyIncomeSubtitle() {
        appium.scrollToElementByText("Pendapatan");
        return appium.getText(textMonthlyIncome);
    }

    /**
     * Get monthly income price number
     * @return integer data type
     */
    public int getMonthlyIncomePrice() {
        return JavaHelpers.extractNumber(appium.getText(monthlyIncomePrice));
    }

    /**
     * Get current filter text
     * @return filter text month and year
     */
    public String getIncomeFilterText() {
        return appium.getText(incomeFilterText);
    }

    /**
     * Click on monthly income filter
     * @param index month index current is 0, previous 1 to 14
     */
    public void setMonthlyIncomeFilter(int index) {
        appium.clickOn(filterMonthlyIncome);
        appium.waitTillAllElementsAreLocated(filterMonthList).get(index).click();
    }

    /**
     * tap on update room
     */
    public void tapOnUpdateRoomAvailability() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isUpdateRoomPriceButtonPresent());
        appium.tapOrClick(availabilityRoomButton);
    }
}
