package pageobjects.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class NotificationPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public NotificationPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    @AndroidFindBy(id = "updateTextView")
    private WebElement notifUpdateTab;

    @AndroidFindBy(id = "billTextView")
    private WebElement notifBillTab;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Back\"]")
    @AndroidFindBy(id = "backImageView")
    private WebElement backButton;

    private By firstNotification = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Info & Promo']/following::android.widget.LinearLayout[3]/preceding::android.widget.TextView[1]"): By.xpath("(//*[@name='Hari ini'])[1]/following-sibling::XCUIElementTypeStaticText[1]");

    @AndroidFindBy(xpath = "(//android.widget.LinearLayout[contains(@resource-id,'com.git.mami.kos:id/titleTextView')])[2]")
    private WebElement successPaymentNotification;

    private By notificationIcon = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.ImageView[@resource-id='com.git.mami.kos:id/ownerNotificationIconCV']"): By.xpath("//*[@name='owner khusus AT android']/following-sibling::XCUIElementTypeStaticText");


    /**
     * Check element Notif Update Tab is Displayed
     * @return Status
     */
    public boolean isNotifUpdateTabDisplayed(){
       return notifUpdateTab.isDisplayed();
    }


    /**
     * Check element Notif Tagihan Tab is Displayed
     * @return Status
     */
    public boolean isNotifBillTabDisplayed(){
        return notifBillTab.isDisplayed();
    }

    /**
     * Click Back button
     * @return Status
     */
    public void clickBackButton() {
        if(Constants.MOBILE_OS== Platform.ANDROID){
            appium.back();
        }
        else {
            backButton.click();
        }
    }

    /**
     * Click on notification icon
     */
    public void clickNotificationIcon() throws InterruptedException {
        appium.hardWait(2);
        appium.tapOrClick(notificationIcon);
    }

    /**
     * Click on first notification
     */
    public void clickOnFirstNotification() {
        appium.tapOrClick(firstNotification);
    }

    /**
     * Get notification message
     */
    public String getNotificationMessage() {
        return appium.getText(firstNotification);
    }

    /**
     * Get notification message success payment
     */
    public String getNotificationSuccessPaymentMessage() {
        return appium.getText(successPaymentNotification);
    }
}
