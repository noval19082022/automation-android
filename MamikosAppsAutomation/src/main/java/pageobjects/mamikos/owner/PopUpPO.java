package pageobjects.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class PopUpPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public PopUpPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    @AndroidFindBy(id = "cancelButton")
    private WebElement cancelButton;

    /**
     * Click on dismiss button on pop up with element id's cancelButton
     */
    public void dismissPopUp() {
        if (isPopUpCancelButtonVisible()) {
            appium.tapOrClick(cancelButton);
        }
    }

    /**
     * Wait in case pop up with cancel button visible
     * @return true if element present
     */
    private boolean isPopUpCancelButtonVisible() {
        return appium.waitInCaseElementVisible(cancelButton, 3) != null;
    }
}
