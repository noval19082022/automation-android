package pageobjects.mamikos.owner.apartmentads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

import java.util.List;


public class AddApartmentsAdsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public AddApartmentsAdsPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }


    @AndroidFindBy(id="apartmentOwnerTextView")
    private WebElement apartmentTab;

    @AndroidFindBy(id="entryIdOwner")
    private WebElement buttonOwner;

    @AndroidFindBy(id="apartmentNameEditText")
    private WebElement apartmentNameInput;

    @AndroidFindBy(id="et_name_unit_apartment")
    private WebElement unitNameInput;

    @AndroidFindBy(id="et_no_unit_apartment")
    private WebElement unitNumberInput;

    @AndroidFindBy(id="sp_type_unit_apartment")
    private WebElement unitTypeSpinner;

    @AndroidFindBy(id="et_lantai_apartment")
    private WebElement unitFloorInput;

    @AndroidFindBy(id="et_luas_apartment")
    private WebElement unitAreaInput;

    @AndroidFindBy(id="idrPriceRadioButton")
    private WebElement indonesianPriceRadio;

    @AndroidFindBy(id="cbPriceDaily")
    private WebElement apartmentDailyPriceCheck;

    @AndroidFindBy(id="cbPriceWeekly")
    private WebElement apartmentWeeklyPriceCheck;

    @AndroidFindBy(id="cbPriceMonthly")
    private WebElement apartmentMonthlyPriceCheck;

    @AndroidFindBy(id="cbPriceYearly")
    private WebElement apartmentYearlyPriceCheck;

    @AndroidFindBy(id="et_price_apt_daily")
    private WebElement apartmentDailyPriceInput;

    @AndroidFindBy(id="et_price_apt_weekly")
    private WebElement apartmentWeeklyPriceInput;

    @AndroidFindBy(id="et_price_apt_monthly")
    private WebElement apartmentMonthlyPriceInput;

    @AndroidFindBy(id="et_price_apt_yearly")
    private WebElement apartmentYearlyPriceInput;

    @AndroidFindBy(id="descriptionEditText")
    private WebElement descriptionInput;

    @AndroidFindBy(id="additionalPriceButton")
    private WebElement additionalPriceButton;

    @AndroidFindBy(id="cb_min_rent")
    private WebElement minRentCheckbox;

    @AndroidFindBy(id="cb_maintenance")
    private WebElement maintenanceCheckbox;

    @AndroidFindBy(id="et_maintenance_apartment")
    private WebElement maintenanceApartInput;

    @AndroidFindBy(id="cb_parkir")
    private WebElement parkingCheckbox;

    @AndroidFindBy(id="et_parkir_apartment")
    private WebElement parkingApartInput;

    @AndroidFindBy(id="sp_min_rent_apartment")
    private WebElement minRentApartSpinner;

    @AndroidFindBy(id = "cb_fac")
    private List<WebElement> cbFacilities;

    @AndroidFindBy(id="rb_furnished")
    private WebElement radioFurnished;

    @AndroidFindBy(id = "gallerySourceImageView")
    private WebElement gallerySelector;

    @AndroidFindBy(id ="addPhotoTextView")
    private WebElement uploadPhotoButton;

    @AndroidFindBy(xpath = "//*[@resource-id='com.google.android.apps.photos:id/image']")
    private WebElement photoButton;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[contains(@content-desc, 'Photo taken on')]")
    private WebElement itemImage;

    @AndroidFindBy(id = "bt_next_apartment")
    private WebElement btnNextAparment;

    @AndroidFindBy(id="openOwnerPageButton")
    private WebElement openOwnerPageButton;

    @AndroidFindBy(id="titleApartmentTextView")
    private WebElement apartmentTitleText;

    @AndroidFindBy(id="priceTextView")
    private WebElement apartmentPriceText;

    @AndroidFindBy(id="apartmentNameTextView")
    private WebElement apartmentNameText;

    @AndroidFindBy(id="sizeTagTextView")
    private WebElement apartmentSizeText;

    @AndroidFindBy(id="furnishTagTextView")
    private WebElement apartmentFurnisedText;

    @AndroidFindBy(id="bedCountTextView")
    private WebElement apartmentBedText;

    @AndroidFindBy(id="bathRoomCountTextView")
    private WebElement apartmentBathText;

    @AndroidFindBy(id="floorCountTextView")
    private WebElement apartmentFloorText;

    @AndroidFindBy(id="stateApartmentTextView")
    private WebElement apartmentStateText;


    /**
     * Click on 'Apartment' on My Ads menu
     */
    public void clickOnApartmentTab(){
        appium.tapOrClick(apartmentTab);
    }

    /**
     * Click on 'As an Owner' Button
     */
    public void clickOnAsAnOwnerButton(){
        appium.tapOrClick(buttonOwner);
    }
    
    /**
     * Fill out Apartment Info form
     */
    public void fillOutApartmentInfoForm(String apartmentName, String unitName, String unitNumber, String roomType,
                                         String unitFloor, String unitArea, String dailyPrice, String weeklyPrice,
                                         String monthlyPrice, String yearlyPrice, String description, String minRent,
                                         String maintenanceApart, String parkingApart) throws InterruptedException {

        appium.enterText(apartmentNameInput, apartmentName, false);
        appium.enterText(unitNameInput, unitName, false);
        appium.enterText(unitNumberInput, unitNumber, false);
        appium.tapOrClick(unitTypeSpinner);
        appium.clickOn(By.xpath("//*[@text='"+ roomType +"']"));
        appium.enterText(unitFloorInput, unitFloor, false);
        appium.enterText(unitAreaInput, unitArea, false);
        appium.tapOrClick(indonesianPriceRadio);
        appium.tapOrClick(apartmentDailyPriceCheck);
        appium.enterText(apartmentDailyPriceInput, dailyPrice, false);
        appium.scroll(0.5, 0.8, 0.2, 2000);
        appium.tapOrClick(apartmentWeeklyPriceCheck);
        appium.enterText(apartmentWeeklyPriceInput, weeklyPrice, false);
        appium.tapOrClick(apartmentMonthlyPriceCheck);
        appium.enterText(apartmentMonthlyPriceInput, monthlyPrice, false);
        appium.tapOrClick(apartmentYearlyPriceCheck);
        appium.enterText(apartmentYearlyPriceInput, yearlyPrice, false);
        appium.enterText(descriptionInput, description, false);
        appium.tapOrClick(additionalPriceButton);
        appium.tapOrClick(minRentCheckbox);
        appium.tapOrClick(minRentApartSpinner);
        appium.scrollToElementByText(minRent);
        appium.clickOn(By.xpath("//*[@text='" + minRent +"']"));
        appium.tapOrClick(maintenanceCheckbox);
        appium.enterText(maintenanceApartInput, maintenanceApart, false);
        appium.tapOrClick(parkingCheckbox);
        appium.enterText(parkingApartInput, parkingApart, false);
        appium.scrollToElementByText("Fasilitas Kamar *");
        appium.tapOrClick(radioFurnished);
        for(WebElement cb:cbFacilities){
            cb.click();
        }
        appium.scrollToElementByText("LANJUTKAN");
        appium.tapOrClick(uploadPhotoButton);
        appium.tapOrClick(gallerySelector);
        appium.tapOrClick(photoButton);
        appium.tapOrClick(itemImage);
        if(appium.waitInCaseElementVisible(gallerySelector, 5) == null){
            appium.tapOrClick(uploadPhotoButton);
            appium.tapOrClick(gallerySelector);
            appium.tapOrClick(photoButton);
            appium.tapOrClick(itemImage);
        }
        if(appium.waitInCaseElementVisible(gallerySelector, 5) == null){
            appium.tapOrClick(uploadPhotoButton);
            appium.tapOrClick(gallerySelector);
            appium.tapOrClick(photoButton);
            appium.tapOrClick(itemImage);
        }
        appium.hardWait(5);
        appium.tapOrClick(btnNextAparment);
        appium.tapOrClick(openOwnerPageButton);
    }

    /**
     * Get Apartment Title Text
     * @return getApartmentTitle Apartment Title Text
     */
    public String getApartmentTitle(){
        appium.waitTillElementIsVisible(apartmentTitleText);
        return apartmentTitleText.getText();
    }

    /**
     * Get Apartment Price Text
     * @return getApartmentPrice Apartment Price Text
     */
    public String getApartmentPrice(){
        appium.waitTillElementIsVisible(apartmentPriceText, 7);
        return apartmentPriceText.getText();
    }

    /**
     * Get Apartment Name Text
     * @return getAparmentName Apartment Name Text
     */
    public String getApartmentName(){
        appium.waitTillElementIsVisible(apartmentNameText, 5);
        return apartmentNameText.getText();
    }

    /**
     * Get Apartment Size Text
     * @return getApartmentSize Apartment Size Text
     */
    public String getApartmentSize(){
        return apartmentSizeText.getText();
    }

    /**
     * Get Apartment Furnishing Text
     * @return getApartmentFurnishing Apartment Furnishing Text
     */
    public String getApartmentFurnishing(){
        return apartmentFurnisedText.getText();
    }

    /**
     * Get Apartment Bed Text
     * @return getApartmentBed Apartment Bed Text
     */
    public String getApartmentBed(){
        return apartmentBedText.getText();
    }

    /**
     * Get Apartment Bath Text
     * @return getApartmentBath Apartment Bath Text
     */
    public String getApartmentBath(){
        return apartmentBathText.getText();
    }

    /**
     * Get Apartment Floor Text
     * @return getApartmentFloor Apartment Floor Text
     */
    public String getApartmentFloor(){
        return apartmentFloorText.getText();
    }
}
