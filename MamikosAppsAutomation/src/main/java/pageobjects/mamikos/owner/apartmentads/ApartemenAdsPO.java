package pageobjects.mamikos.owner.apartmentads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class ApartemenAdsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ApartemenAdsPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    //---------------- Click Growth Apartemen -----------------

    @AndroidFindBy(id = "growUpApartmentButton")
    private WebElement clickGrowthButton;

    @AndroidFindBy(id = "statisticApartmentSpinner")
    private WebElement rangeTimeDropDown;

    @AndroidFindBy(id = "clickCountApartmentTextView")
    private WebElement clickCountText;

    @AndroidFindBy(id = "premiumOffButton")
    private WebElement premiumOffButton;

    @AndroidFindBy(id = "premiumOnButton")
    private WebElement premiumOnButton;

    @AndroidFindBy(id = "allocatedBalancePriceTextView")
    private WebElement allocationBalanceText;



    /**
     * Click "click growth" button
     */
    public void clickOnGrowUpApartemenButton() {
        appium.waitInCaseElementVisible(clickGrowthButton, 10);
        appium.clickOn(clickGrowthButton);
    }

    /**
     * Click statistic time range
     */
    public void chooseTimeRangeStatictic() {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.waitInCaseElementVisible(rangeTimeDropDown, 10);
        appium.clickOn(rangeTimeDropDown);

    }

    /**
     * Check statistic time range is present
     * @return status true / false
     */
    public boolean isTimeRangeTextPresent(Object range) {
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + range + "']"), 5) != null;
    }

    /**
     * get Amount balance
     * @return balance
     */
    public String getAllocationBalance() throws InterruptedException {
        appium.hardWait(5);
        appium.waitInCaseElementVisible(allocationBalanceText, 10);
        return appium.getText(allocationBalanceText);
    }

}
