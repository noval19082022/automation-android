package pageobjects.mamikos.owner.booking;

import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

import java.util.HashMap;

public class BookingConfirmContractPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;
	public static final String PROPERTYFILE="src/main/resources/constants.properties";
	private String os = JavaHelpers.setSystemVariable(PROPERTYFILE, "MobileOperatingSystem");
	TouchAction touchAction ;

	public BookingConfirmContractPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
		touchAction= new TouchAction(driver);
    	
    	 //This initElements method will create all Android Elements
	//	PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

	@AndroidFindBy(id="depositFeeEditText")
	private WebElement depositeFeeTextbox;

	@AndroidFindBy(id="penaltyFeeEditText")
	private WebElement penaltyFeeTextbox;

	@AndroidFindBy(id="penaltyDurationEditText")
	private WebElement penaltyFeeDurationTextbox;

	@AndroidFindBy(id="penaltyDurationTypeEditText")
	private WebElement penaltyFeeDurationType;

	private By saveButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("saveButton"): By.xpath("//XCUIElementTypeButton[@name='Simpan Data Penyewa']");

	@AndroidFindBy(id="durationTextView")
	private WebElement durationLabel;

	@AndroidFindBy(id="acceptButton")
	private WebElement acceptButton;

	@AndroidFindBy(id="nextButton")
	private WebElement yesAcceptButton;

	@AndroidFindBy(id="saveButton")
	private WebElement saveTenantDataButton;

	@AndroidFindBy(id="bookingOwnerTextView")
	private WebElement bookingRequestList;

	@AndroidFindBy(xpath="//android.widget.TextView[contains(@text,'Butuh konfirmasi')]/parent::android.widget.LinearLayout")
	private WebElement needConfirmationTab;

	@AndroidFindBy(id="userNameTextView")
	private WebElement tenantNameLabel;

	@AndroidFindBy(id="bookingRecyclerView")
	private WebElement bookingEmptyLabel;

	@AndroidFindBy(id="messageTextView")
	private WebElement titleMessageLabel;

	@AndroidFindBy(id="dialogTerminateContractConfirmButton")
	private WebElement terminateButton;

	@AndroidFindBy(xpath="//android.widget.ImageView[@resource-id='com.git.mami.kos:id/warningContractImageView']/following-sibling::android.widget.TextView")
	private WebElement warningMessageLabel;

	private By titleSection = Constants.MOBILE_OS == Platform.ANDROID ? By.id("titleTextView"): By.xpath("//XCUIElementTypeStaticText[@name='Atur Kontrak Tagihan']");

	@AndroidFindBy(id = "depositFeeEditText")
	private WebElement depositFeeTextBox;

	@AndroidFindBy(id = "penaltyFeeEditText")
	private WebElement penaltyFeeTextBox;

	@AndroidFindBy(id = "additionalPriceView")
	private WebElement additionalPriceLabel;

	@AndroidFindBy(id = "addPriceDataButton")
	private WebElement additionalCostLabel;

	@AndroidFindBy(id = "newPriceTitleEditText")
	private WebElement additionalCostName;

	@AndroidFindBy(id = "newPriceValueEditText")
	private WebElement additionalCostPrice;

	@AndroidFindBy(id = "saveNewPriceButton")
	private WebElement additionalKosSaveButton;

	@AndroidFindBy(id = "saveDataPriceButton")
	private WebElement saveDataAdditionalKosButton;

	@AndroidFindBy(id = "downPaymentActivatedSwitch")
	private WebElement downPaymentToggleButton;

	@AndroidFindBy(id = "percentSpinner")
	private WebElement downPaymentDropdownLis;

	@AndroidFindBy(id = "finishButton")
	private WebElement downPaymentFinishButton;

	@AndroidFindBy(id = "disclaimerV2Text")
	private WebElement terminateSuccessTextBox;

	@AndroidFindBy(id = "nextButton")
	private WebElement sayaMengertiButton;

	@iOSXCUITFindBy(accessibility = "Tagihan Penyewa")
	@AndroidFindBy(xpath = "//*[@text='Tagihan Penyewa']")
	private WebElement tagihanPenyewaTab;

	@iOSXCUITFindBy(accessibility = "Belum Bayar")
	@AndroidFindBy(xpath = "//*[@text='Belum Dibayar']")
	private WebElement belumBayarButton;

	@iOSXCUITFindBy(accessibility = "Total Tagihan yang Belum Dibayar")
	private WebElement belumBayarTab;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Di Mamikos\"]")
	@AndroidFindBy(xpath = "//*[@text='Di Mamikos']")
	private WebElement diMamikosTab;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Ditransfer\"]")
	@AndroidFindBy(xpath = "//*[@text='Ditransfer']")
	private WebElement diTransferTab;

	@iOSXCUITFindBy(accessibility = "Di Mamikos")
	@AndroidFindBy(xpath = "//*[@text='Di Mamikos']")
	private WebElement diMamikosButton;

	@iOSXCUITFindBy(accessibility = "Ditransfer")
	@AndroidFindBy(xpath = "//*[@text='Ditransfer']")
	private WebElement diTransferButton;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Lihat Tagihan Penyewa\"]")
	@AndroidFindBy(id = "billingManagementButton")
	private WebElement showTagihanPenyewaButton;

	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"Booking Langsung\"])[2]")
	@AndroidFindBy(id = "titleManageBookingText")
	private WebElement bookingLangsungTab;

	@iOSXCUITFindBy(accessibility = "Lihat Booking")
	@AndroidFindBy(xpath = "//*[@text='Lihat Booking']")
	private WebElement showBoookingButton;

	@iOSXCUITFindBy(accessibility = "Permintaan Sewa")
	@AndroidFindBy(id = "noReqBookingText")
	private WebElement rentRequestButton;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Kelola Booking Langsung\"]")
	@AndroidFindBy(id = "btnManageBooking")
	private WebElement kelolaBookingButton;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Tagihan di\"]/parent::XCUIElementTypeOther")
	@AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"mamikos\"])[1]")
	private WebElement tagihanKostNameDropdown;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Pondok Kencana Indah squad payment\"]")
	private WebElement tagihanKostName;

	@iOSXCUITFindBy(accessibility = "Hitungan Sewa")
	@AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"mamikos\"])[3]")
	private WebElement hitunganSewaDropdown;

	private By icnBack = Constants.MOBILE_OS == Platform.ANDROID ? By.id("backImageView"): By.id("Back");

	@AndroidFindBy(xpath = "//*[@text='Tagihan']")
	private WebElement tagihanButton;

	@AndroidFindBy(id = "titleEmptyCheckInTextView")
	private WebElement defaultTextTagihanPage;

	@AndroidFindBy(id = "findKosTextView")
	private WebElement cariKosButton;

	@AndroidFindBy(id = "contractButton")
	private WebElement kontrakTab;

	@AndroidFindBy(id = "roomNameTextView")
	private WebElement kostName;

	@AndroidFindBy(id = "roomNumberTextView")
	private WebElement roomNumber;

	@AndroidFindBy(id = "titleRentTextView")
	private WebElement rentText;

	@AndroidFindBy(id = "billingDateTextView")
	private WebElement billingDate;

	@AndroidFindBy(id = "checkInDateTextView")
	private WebElement checkInDateText;

	@AndroidFindBy(id = "checkOutDateTextView")
	private WebElement checkOutDateText;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Booking Menunggu']")
	private WebElement bookingIsWaitingButton;

	private By detailBookingPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Pengajuan Booking']"): By.xpath("//XCUIElementTypeStaticText[@name='Kelola Booking']");

	private By pengajuanBookingTitle = By.id("Pengajuan Booking");

	private By laporanKeuanganTitle = By.id("Laporan Keuangan");

	private By kelolaTagihanTitle = By.id("Kelola Tagihan");

	private By penyewaTitle = By.id("Penyewa");

	private By icnBackArrow = By.id("ic back arrow");

	private By selanjutnyaButton = By.id("Selanjutnya");

	private By selesaiButton = By.id("Selesai");


	/**
	 * Enter deposit/penalty info and click on Save
	 * @param depositFeeText deposit fee
	 * @param penaltyFeeText penalty fee
	 * @param penaltyFeeDurationText penalty fee duration
	 * @param penaltyFeeDurationTypeText  penalty fee duration type
	 * @throws InterruptedException
	 */
	public void enterDepositePanaltyInfo(String depositFeeText, String penaltyFeeText, String penaltyFeeDurationText, String penaltyFeeDurationTypeText) throws InterruptedException {
		appium.enterText(depositeFeeTextbox, depositFeeText, false);
		appium.scrollToElementByResourceId("penaltyFeeEditText");
		appium.enterText(penaltyFeeTextbox, penaltyFeeText, false);
		appium.scrollToElementByResourceId("penaltyDurationEditText");
		appium.enterText(penaltyFeeDurationTextbox, penaltyFeeDurationText, false);
		appium.enterText(penaltyFeeDurationTextbox, penaltyFeeDurationText, true);
		appium.scrollToElementByResourceId("penaltyDurationTypeEditText");
		appium.tapOrClick(penaltyFeeDurationType);
		appium.clickOn(By.xpath("//*[@text='" + penaltyFeeDurationTypeText + "']"));
		clickOnSaveButton();
	}

	/**
	 * Check if save button is present in viewport
	 * @return true or false
	 */
	public boolean isSaveButtonPresent() {
		return appium.waitInCaseElementVisible(saveButton, 1) != null;
	}

	/**
	 * Click on Save button
	 * @throws InterruptedException
	 */
	public void clickOnSaveButton() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID){
			appium.scrollToElementByResourceId("saveButton");
		} else {
			int i = 0 ;
			do {
				appium.scroll(0.5, 0.8, 0.2, 2000);
				if(i == 10) {
					break;
				}
				i++;
			}
			while (!isSaveButtonPresent());
		}
		appium.tapOrClick(saveButton);
		appium.hardWait(3);
	}

	/**
	 * Get rent duratin
	 * return String
	 */
	public String getRentDuration() throws InterruptedException{
		appium.hardWait(5);
		return appium.getText(durationLabel);
	}

	public void clickOnAcceptButton() throws InterruptedException{
		appium.hardWait(3);
		appium.tapOrClick(acceptButton);
	}

	/**
	 * Click yes button for verification accept booking
	 */
	public void clickOnYesAcceptButton() {
		appium.tapOrClick(yesAcceptButton);
	}

	/**
	 * Click save data for confirmation accept booking
	 */
	public void clickOnSaveTenantData() {
		appium.scrollToElementByResourceId("saveButton");
		appium.tapOrClick(saveTenantDataButton);
	}

	/**
	 * Click booking request
	 */
	public void clickOnBookingRequest() {
		appium.scrollToElementByResourceId("learnMoreButton");
		appium.tapOrClick(bookingRequestList);
	}

	/**
	 * Get tenant name
	 * return String
	 */
	public String getTenantName() {
		return appium.getText(tenantNameLabel);
	}

	/**
	 * Verify element booking data is empty
	 */
	public void bookingDataEmpty() {
		appium.isElementDisplayed(bookingEmptyLabel);
	}

	public void confirmTerminateBooking() {
		appium.waitTillElementIsVisible(terminateButton);
		appium.tapOrClick(terminateButton);
	}

	/**
	 * Get title message terminate
	 */
	public String getTitleMessage() {
		return appium.getText(titleMessageLabel);
	}

	/**
	 * Get warning message terminate contract
	 */
	public String getWarningMessage() {
		return appium.getText(warningMessageLabel);
	}

	/**
	 * Get title text example "Atur Kontrak Tagihan"
	 * @return string data type
	 */
	public String getSectionTitleText() {
		return appium.getText(titleSection);
	}

	/**
	 * Check is in booking confirm contract po
	 * @return true if is in booking confirm contract po by check in title text
	 */
	public boolean isInBookingContractConfirmation() {
		return getSectionTitleText().equalsIgnoreCase("Atur Kontrak Tagihan");
	}

	/**
	 * Enter text to deposit fee text box
	 * @param  depositFee is value deposit fee
	 * @throws InterruptedException
	 */
	public void enterDepositFee(String depositFee) {

		appium.scrollToElementByResourceId("depositFeeEditText");
		appium.enterText(depositFeeTextBox, depositFee, true);
	}

	/**
	 * Enter text to penalty fee text box
	 * @param  penaltyFee is value penalty fee
	 */
	public void enterPenaltyFee(String penaltyFee) {
		appium.scrollToElementByResourceId("penaltyFeeEditText");
		appium.enterText(penaltyFeeTextBox, penaltyFee, true);
		appium.scrollToElementByResourceId("penaltyDurationEditText");
		appium.enterText(penaltyFeeDurationTextbox, "1", true);
		appium.scrollToElementByResourceId("penaltyDurationTypeEditText");
		appium.tapOrClick(penaltyFeeDurationType);
		appium.clickOn(By.xpath("//*[@text='Hari']"));
	}

	/**
	 * Create additional cost and enter value additional cost
	 * @param  additionalCostsLabel is label additional cost
	 * * @param  additionalCosts is value additional cost
	 */
	public void enterAdditionalCost(String additionalCostsLabel, String additionalCosts) {
		appium.scrollToElementByText("Tambah biaya lainnya");
		appium.tapOrClick(additionalPriceLabel);
		appium.tapOrClick(sayaMengertiButton);
		appium.tapOrClick(additionalCostLabel);
		appium.enterText(additionalCostName, additionalCostsLabel, true);
		appium.enterText(additionalCostPrice, additionalCosts, true);
		appium.tapOrClick(additionalKosSaveButton);
		appium.tapOrClick(saveDataAdditionalKosButton);
	}

	/**
	 * Switch down payment toggle button and enter value down payment
	 * @param  downPayment is value down payment
	 */
	public void enterDownPayment(String downPayment) throws InterruptedException {
		appium.scrollToElementByResourceId("downPaymentActivatedSwitch");
		appium.tapOrClick(downPaymentToggleButton);
		appium.tapOrClick(yesAcceptButton);
		appium.tapOrClick(downPaymentDropdownLis);
		appium.hardWait(1);
		WebElement element = driver.findElement(By.xpath("//*[@resource-id='android:id/text1'][contains(@text, '" + downPayment + "')]"));
		appium.tapOrClick(element);
		appium.tapOrClick(downPaymentFinishButton);
	}

	/**
	 * Get warning message terminate contract
	 */
	public String getTerminateContractSuccess() {
		return appium.getText(terminateSuccessTextBox);
	}

	public void confirmTagihanPenyewaTab() {
		appium.scroll(0.5, 0.20, 0.80, 2000);
		appium.waitTillElementIsVisible(tagihanPenyewaTab);
	}

	public void confirmBelumBayarButton() {
		appium.waitTillElementIsVisible(belumBayarButton);
	}

	public void confirmDiMamikosButton() {
		appium.waitTillElementIsVisible(diMamikosButton);
	}

	public void confirmDiTransferButton() {
		appium.waitTillElementIsVisible(diTransferButton);
	}

	public void confirmLihatTagihanPenyewaButton() {
		appium.waitInCaseElementVisible(showTagihanPenyewaButton,3);
	}

	public void confirmBookingLangsungTab() {
		appium.waitTillElementIsVisible(bookingLangsungTab);
	}

	public void confirmRentRequestButton() {
		appium.waitTillElementIsVisible(rentRequestButton);
	}

	public void confirmLihatBookingButton() {
		appium.waitTillElementIsVisible(showBoookingButton);
	}

	public void confirmKelolaBookingButton() {
		appium.waitTillElementIsVisible(kelolaBookingButton);
	}

	public void clickBelumBayarButton() {
		appium.tapOrClick(belumBayarButton);
	}

	public void clickLihatTagihanPenyewaButton() {
//		appium.scroll(-0.5, -0.20, -0.80, 2000);
		appium.scroll(0.5, 0.20, 0.80, 2000);
		appium.tapOrClick(showTagihanPenyewaButton);
	}

	public void confirmBelumBayarTab() {
		appium.waitInCaseElementVisible(belumBayarButton,3);
	}

	public void confirmDiMamikosTab() {
		appium.waitTillElementIsVisible(diMamikosTab);
	}

	public void confirmDiTransferTab() {
		appium.waitInCaseElementVisible(diTransferTab,3);
	}

	public void clickDiMamikosButton() {
		appium.scroll(0.5, 0.20, 0.80, 2000);
		appium.tapOrClick(diMamikosButton);
	}

	public void clickDiTransferButton() {
		appium.scroll(0.5, 0.20, 0.80, 2000);
		appium.tapOrClick(diTransferButton);
	}

	public void clicktagihanKostNameDropdown() {
		appium.waitInCaseElementVisible(tagihanKostNameDropdown,3);
		appium.tapOrClick(tagihanKostNameDropdown);
	}

	public void chooseTagihanKostName(String kostName) {
		if (os.equalsIgnoreCase("android"))
		{
			WebElement element = driver.findElement(By.xpath("//*[@text='"+ kostName +"']"));
			appium.tapOrClick(element);
		}
		else if (os.equalsIgnoreCase("iOS"))
		{
			WebElement element = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+ kostName +"']"));
			appium.tapOrClick(element);
		}
	}

	/**
	 * Get tagihan kost name
	 */
	public String getTagihanKostName(String kostName) {
		String kostNameDisplay = "";
		if (os.equalsIgnoreCase("android"))
		{
			WebElement element = driver.findElement(By.xpath("//*[@text='"+ kostName +"']"));
			kostNameDisplay = appium.getText(element);
		}
		else if (os.equalsIgnoreCase("iOS"))
		{
			WebElement element = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+kostName+"']"));
			kostNameDisplay = appium.getElementAttributeValue(element, "value");
		}
		return kostNameDisplay;

	}

	/**
	 * Choose hitungan sewa
	 */
	public void chooseHitunganSewa(String hitunganSewa) {
		appium.tapOrClick(hitunganSewaDropdown);
		if (os.equalsIgnoreCase("android"))
		{
			WebElement element = driver.findElement(By.xpath("//*[@text='"+ hitunganSewa +"']"));
			appium.tapOrClick(element);
		}
		else if (os.equalsIgnoreCase("iOS"))
		{
			WebElement element = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+hitunganSewa+"']"));
			appium.tapOrClick(element);
		}
	}

	/**
	 * Get hitungan sewa
	 */
	public String getHitunganSewa(String hitunganSewa) {
		String hitunganSewaDisplay = "";

		if (os.equalsIgnoreCase("android"))
		{
			WebElement element = driver.findElement(By.xpath("//*[@text='"+ hitunganSewa +"']"));
			hitunganSewaDisplay = appium.getText(element);
		}
		else if (os.equalsIgnoreCase("iOS"))
		{
			WebElement element = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='"+hitunganSewa+"']"));
			hitunganSewaDisplay = appium.getElementAttributeValue(element, "value");
		}
	return hitunganSewaDisplay;
	}

	/**
	 * Click button back
	 */
	public void clickIcnBack() {
		appium.tapOrClick(icnBack);
	}

	/**
	 * Click tagihan button on profile page
	 */
	public void clickTagihanButton() {
		appium.waitInCaseElementVisible(tagihanButton,2);
		appium.tapOrClick(tagihanButton);
	}

	/**
	 * Display default page tagihan menu
	 * @return text default tagihan page
	 */
	public String getTextDefaultTagihanPage (){
		appium.waitInCaseElementVisible(cariKosButton,2);
		appium.waitInCaseElementVisible(defaultTextTagihanPage,2);
		return appium.getText(defaultTextTagihanPage);
	}

	/**
	 * Click kontrak tab on kost saya page
	 */
	public void clickKontrakTab(){
		appium.tapOrClick(kontrakTab);
	}

	/**
	 * Verify data kontrak on kontrak tab
	 */
	public void verifyDataKontrak (){
		appium.isElementDisplayed(kostName);
		appium.isElementDisplayed(roomNumber);
		appium.isElementDisplayed(rentText);
		appium.isElementDisplayed(billingDate);
		appium.isElementDisplayed(checkInDateText);
		appium.isElementDisplayed(checkOutDateText);
		appium.isElementDisplayed(durationLabel);
	}

	/**
	 * Click on Booking Is Waiting Button
	 */
	public void clickOnBookingIsWaitingButton() {
		appium.click(bookingIsWaitingButton);
	}

	/**
	 * @return return true if detail booking page present. Otherwise false
	 */
	public boolean detailBookingPageIsPresent() {
		return appium.isElementPresent(detailBookingPageTitle);
	}

	/**
	 * Check if pusat bantuan button is present in viewport
	 * @return true or false
	 */
	public boolean isPengajuanBookingTitlePresent() {
		return appium.waitInCaseElementVisible(penyewaTitle, 1) != null;
	}

	/**
	 * Click Pengajuan Booking Title
	 */
	public void clickOnPengajuanBookingTitle() {
		int i = 0;
		do {
			appium.scroll(0.5, 0.8, 0.2, 2000);
			if(i == 10) {
				break;
			}
			i++;
		}
		while (!isPengajuanBookingTitlePresent());
		appium.tapOrClick(pengajuanBookingTitle);
	}

	/**
	 * Click Laporan Keuangan Title
	 */
	public void clickOnLaporanKeuanganTitle() {
		appium.tapOrClick(laporanKeuanganTitle);
	}

	/**
	 * Click Kelola Tagihan Title
	 */
	public void clickOnKelolaTagihanTitle() {
		appium.tapOrClick(kelolaTagihanTitle);
	}

	/**
	 * Click Penyewa Title
	 */
	public void clickOnPenyewaTitle() {
		appium.tapOrClick(penyewaTitle);
	}

	/**
	 * Click icn back on beli saldo mamiads page
	 */
	public void clickOnIcnBack() throws InterruptedException{
		appium.hardWait(3);
		if (appium.waitInCaseElementVisible(icnBack,5) != null) {
			appium.clickOn(icnBack);
		}else {
			appium.clickOn(icnBackArrow);
		}
	}

	/**
	 * Click popup on laporan keuangan page
	 */
	public void clickOnPopUpLaporanKeuangan() throws InterruptedException{
		appium.hardWait(5);
		if (appium.waitInCaseElementVisible(selanjutnyaButton, 3) != null) {
			do {
				appium.clickOn(selanjutnyaButton);
			} while (appium.isElementPresent(selanjutnyaButton));
			appium.clickOn(selesaiButton);
			}
		}

}
