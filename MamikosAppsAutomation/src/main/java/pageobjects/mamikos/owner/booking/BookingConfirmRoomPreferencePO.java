package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;
import utilities.Constants;

public class BookingConfirmRoomPreferencePO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public BookingConfirmRoomPreferencePO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(driver,this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

	@AndroidFindBy(id="roomNumberEditText")
	private WebElement roomNumberTextbox;

	@AndroidFindBy(id="nextButton")
	private WebElement nextButton;

	@AndroidFindBy(id="selectedTextView")
	private WebElement roomNumberDropdownList;


	@AndroidFindBy(id="confirmButton")
	private WebElement applyButton;

	private By roomNumberDropdown = Constants.MOBILE_OS == Platform.ANDROID ? By.id("selectedTextView"): By.xpath("//XCUIElementTypeButton[@name='Silakan pilih nomor kamar']");

	/**
	 * Enter room number text
	 * @param roomNumberText text
	 */
	public void enterRoomNumberText(String roomNumberText)
	{
		appium.enterText(roomNumberTextbox, roomNumberText, false);
	}

	/**
	 * Click on Next button
	 * @throws InterruptedException
	 */
	public void clickOnNextButton() throws InterruptedException {
		appium.waitTillElementIsClickable(nextButton);
		appium.hardWait(2);
		appium.tapOrClick(nextButton);
	}

	/**
	 * Select room
	 * @throws InterruptedException
	 */
	public void selectRoom(String roomNumber) throws InterruptedException {
		appium.clickOn(roomNumberDropdownList);
		appium.hardWait(3);
		appium.scrollToElementByText(roomNumber);
		appium.hardWait(2);
		appium.clickOn(By.xpath("//*[@text='" + roomNumber + "']"));
		appium.waitTillElementIsClickable(applyButton);
		appium.hardWait(2);
		appium.tapOrClick(applyButton);
	}

	/**
	 * Click on room number dropdown
	 * @throws InterruptedException
	 */
	public void clickOnRoomNumberDropdown() throws InterruptedException {
		appium.clickOn(roomNumberDropdown);
		appium.hardWait(3);
	}
}
