package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;
import utilities.Constants;

public class BookingDetailsPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public BookingDetailsPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(driver,this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 
    
	@AndroidFindBy(id="idBookingTextView")
	private WebElement bookingId;

	private By bookingIdBy = By.id("idBookingTextView");

	@AndroidFindBy(id="roomNameTextView")
	private WebElement roomName;

	@AndroidFindBy(id="roomPriceTextView")
	private WebElement roomPrice;

	@AndroidFindBy(id="dateOrderTextView")
	private WebElement orderDate;

	@AndroidFindBy(id="statusDetailTextView")
	private WebElement status;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='MASUK:']/following-sibling::android.widget.TextView[1]")
	private WebElement bookingDate;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='DURASI:']/following-sibling::android.widget.TextView[1]")
	private WebElement bookingDuration;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='KELUAR:']/following-sibling::android.widget.TextView[1]")
	private WebElement bookingExitDate;

	@AndroidFindBy(id="userNameTextView")
	private WebElement tenantName;

	@AndroidFindBy(id="phoneNumberTextView")
	private WebElement tenantPhone;

	private By acceptButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("acceptButton"): By.xpath("//XCUIElementTypeButton[@name='Terima']");

	private By rejectButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("rejectButton"): By.xpath("//XCUIElementTypeButton[@name='Tolak']");

	//Confirmation message
	@AndroidFindBy(id="messageTextView")
	private WebElement messageTitle;

	@AndroidFindBy(id="cancelButton")
	private WebElement cancelButton;

	private By nextButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("nextButton"): By.id("Ya");

	@AndroidFindBy(id="com.git.mami.kos:id/statusDetailTextView")
	private WebElement bookingStatus;

	private By tncButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("agreeTermsConditionCheckBox"): By.xpath("//XCUIElementTypeSwitch[@value='0']");

	private By confirmRejectButton = By.id("okButton");

	private By bookingDetailLink = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text=\"Baca detail pengajuan\"]"): By.id("Baca detail pengajuan");

	private By nextRejectButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.Button[@resource-id='com.git.mami.kos:id/cancelButton']/following-sibling::android.widget.Button"): By.xpath("//XCUIElementTypeStaticText[@name='Ya, Tolak']");

	private By sayaMengertiButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("nextButton"): By.id("Saya mengerti");

	/** Get Booking ID
	 * @return text
	 */
	public String getBookingId()
	{
		return appium.getText(bookingId);
	}

	/** Get Room name
	 * @return  Room name
	 */
	public String getRoomName()
	{
		return appium.getText(roomName);
	}

	/** Get Room price
	 * @return  Room price
	 */
	public String getRoomPrice()
	{
		return appium.getText(roomPrice);
	}

	/** Get Order date
	 * @return  order date
	 */
	public String getOrderDate()
	{
		return appium.getText(orderDate);
	}

	/** Get Status name
	 * @return Status
	 */
	public String getStatusName()
	{
		return appium.getText(status);
	}

	/** Get Booking Date
	 * @return date
	 */
	public String getBookingDate()
	{
		return appium.getText(bookingDate);
	}

	/** Get Booking duration
	 * @return duration
	 */
	public String getBookingDuration()
	{
		return appium.getText(bookingDuration);
	}

	/** Get Booking exit date
	 * @return exit date
	 */
	public String getBookingExitDate()
	{
		return appium.getText(bookingExitDate);
	}

	/** Get Tenant name
	 * @return Tenant name
	 */
	public String getTanentName()
	{
		return appium.getText(tenantName);
	}

	/** Get Tenant phone
	 * @return Tenant phone
	 */
	public String getTanentPhone()
	{
		return appium.getText(tenantPhone);
	}

	/**
	 * Click on Accept button
	 */
	public void clickOnAcceptButton() throws  InterruptedException {
		appium.hardWait(3);
		if (Constants.MOBILE_OS == Platform.ANDROID){
			String elementResourceId = "acceptButton";
			appium.scrollToElementByResourceId(elementResourceId);
		}
		appium.tapOrClick(acceptButton);
	}

	/**
	 * Click on Reject button
	 */
	public void clickOnRejectButton() throws InterruptedException{
		appium.hardWait(3);
		appium.tapOrClick(rejectButton);
	}

	//Confirmation message

	/** Get message title
	 * @return text
	 */
	public String getMessageTitle()
	{
		return appium.getText(messageTitle);
	}

	/**
	 * Click on Next button
	 * @throws InterruptedException
	 */
	public void clickOnNextButton() throws InterruptedException
	{
		appium.tapOrClick(nextButton);
		appium.hardWait(5);
	}

	/**
	 * Click on Cancel button
	 */
	public void clickOnCancelButton()
	{
		appium.tapOrClick(cancelButton);
	}

	/**
	 * Check if booking status is present in the view port
	 * @return true if booking id present otherwise false
	 * @throws InterruptedException
	 */
	public boolean isBookingStatusPresent() throws InterruptedException {
		return appium.waitInCaseElementVisible(bookingStatus, 1) != null;
	}

	/**
	* Click on Reason Reject Booking
	*/
	public void clickOnReasonRejectBooking(String reason) throws InterruptedException{
		appium.hardWait(2);
		if(Constants.MOBILE_OS == Platform.ANDROID){
			appium.clickOn(By.xpath("//*[@text='" + reason + "']"));
		}else {
			appium.tapOrClick(By.xpath("//XCUIElementTypeStaticText[@name='"+reason+"']"));
		}
	}

	/**
	 * Click on TnC Reject button
	 */
	public void clickOnTnCRejectButton() {
		appium.tapOrClick(tncButton);
	}

	/**
	 * Click on Confirm Reject button
	 */
	public void clickOnConfirmRejectButton()
	{
		appium.clickOn(confirmRejectButton);
	}

	/**
	 * Click on bookingdetail link
	 * direct to detail booking page
	 */
	public void clickOnBookingDetailNeedConfirmationLink() throws InterruptedException{
		appium.hardWait(4);
		appium.tapOrClick(bookingDetailLink);
	}

	/**
	 * Click on Next button Reject Booking
	 * @throws InterruptedException
	 */
	public void clickOnNextRejectButton() throws InterruptedException {
		appium.hardWait(3);
		appium.tapOrClick(nextRejectButton);
	}

	/**
	 * Click on Saya Mengerti button Reject Booking
	 * @throws InterruptedException
	 */
	public void clickOnSayaMengertiButton() throws InterruptedException {
		appium.hardWait(3);
		appium.tapOrClick(sayaMengertiButton);
	}

}
