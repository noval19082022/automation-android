package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;
import utilities.Constants;

public class BookingListingPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public BookingListingPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(driver,this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 
    
	@AndroidFindBy(id="userNameTextView")
	private WebElement tenantName;

	@AndroidFindBy(id="statusTextView")
	private WebElement status;

	@AndroidFindBy(id="roomNameTextView")
	private WebElement roomName;

	@AndroidFindBy(id="checkInDateTextView")
	private WebElement checkInDate;

	@AndroidFindBy(id="durationTextView")
	private WebElement duration;

	private By viewDetailsLink = Constants.MOBILE_OS == Platform.ANDROID ? By.id("detailBookingTextView"): By.xpath("(//XCUIElementTypeStaticText[@name=\"Lihat Detail\"])[1]");

	@AndroidFindBy(id="infoBookingTextView")
	private WebElement bookingInfo;

	@AndroidFindBy(id="acceptButton")
	private WebElement acceptButton;

	@AndroidFindBy(id="rejectButton")
	private WebElement rejectButton;

	@AndroidFindBy(id="titleTextView")
	private WebElement bookingTittleText;

	//Confirmation message
	@AndroidFindBy(id="messageTextView")
	private WebElement messageTitle;

	@AndroidFindBy(id="warningMessageTextView")
	private WebElement warningMessage;

	@AndroidFindBy(id="nextButton")
	private WebElement nextButton;

	@AndroidFindBy(id="cancelButton")
	private WebElement cancelButton;



	/** Get Tenant name
	 * @return Tenant name
	 */
	public String getTanentName()
	{
		return appium.getText(tenantName);
	}

	/** Get Status name
	 * @return Status
	 */
	public String getStatusName()
	{
		return appium.getText(status);
	}

	/** Get Room name
	 * @return  Room name
	 */
	public String getRoomName()
	{
		return appium.getText(roomName);
	}

	/** Get CheckIn date
	 * @return  date
	 */
	public String getCheckInDate()
	{
		return appium.getText(checkInDate);
	}

	/** Get Duration
	 * @return  duration
	 */
	public String getDuration()
	{
		return appium.getText(duration);
	}

	/**
	 * Click on View Details link
	 */
	public void clickOnViewDetailsLink()
	{
		appium.tapOrClick(viewDetailsLink);
	}

	/** Get Booking info text
	 * @return  booking info
	 */
	public String getBookingInfoText()
	{
		return appium.getText(bookingInfo);
	}

	/**
	 * Click on Accept button
	 */
	public void clickOnAcceptButton()
	{
		appium.tapOrClick(acceptButton);
	}

	/**
	 * Click on Reject button
	 */
	public void clickOnRejectButton()
	{
		appium.tapOrClick(rejectButton);
	}


	//Confirmation message
	/** Get message title
	 * @return text
	 */
	public String getMessageTitle()
	{
		return appium.getText(messageTitle);
	}

	/** Get message warming text
	 * @return text
	 */
	public String getWarningMessageText()
	{
		return appium.getText(warningMessage);
	}

	/**
	 * Click on Next button
	 */
	public void clickOnNextButton()
	{
		appium.tapOrClick(nextButton);
	}

	/**
	 * Click on Cancel button
	 */
	public void clickOnCancelButton()
	{
		appium.tapOrClick(cancelButton);
	}

	public String getBookingTittleText() {
		return appium.getText(bookingTittleText);
	}

	public void clickOnBookingDetailNeedConfirmationBookingIndex(int index) throws InterruptedException{
		if(Constants.MOBILE_OS == Platform.ANDROID){
			appium.waitTillElementsCountIsMoreThan(viewDetailsLink, 0);
			appium.waitTillAllElementsAreLocated(viewDetailsLink).get(index).click();
		} else {
			appium.hardWait(3);
			appium.tapOrClick(viewDetailsLink);
		}
	}
}
