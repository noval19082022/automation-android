package pageobjects.mamikos.owner.booking;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class RoomNumberPO
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public RoomNumberPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(driver,this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 

	private By firstRoomAvailableRadioButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//*[@text='Pilih nomor kamar']/following::android.widget.RadioButton)[1]"): By.xpath("//*[@name='Pilih di Tempat ']");

	private By applyButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("confirmRoomNumberButtonCV"): By.id("submitButton");

	private By nextButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("nextButton"): By.xpath("(//XCUIElementTypeStaticText[@name='Lanjutkan'])[4]");

	/**
	 * Click on first available room on the list
	 */
	public void clickOnFirstAvailableRoom() throws InterruptedException {
			appium.tapOrClick(firstRoomAvailableRadioButton);
		}

	/**
	 * Click on apply button
	 */
	public void clickOnApplyButton() {
		appium.waitTillElementIsClickable(applyButton);
		appium.clickOn(applyButton);
	}

	/**
	 * Click on next button when available
	 */
	public void clickOnNextButton() throws InterruptedException{
		appium.hardWait(5);
		appium.tapOrClick(nextButton);
	}
}
