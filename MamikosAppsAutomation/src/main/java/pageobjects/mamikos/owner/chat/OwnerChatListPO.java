package pageobjects.mamikos.owner.chat;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;
import utilities.Constants;

public class OwnerChatListPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public OwnerChatListPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    /** Tap chat message (from list)
     * @param position integer position from top, start from 0
     */
    public void tapChatFromList(int position) throws InterruptedException {
        By chatList = Constants.MOBILE_OS == Platform.ANDROID ? By.id("tvSubtitle"): By.xpath("//XCUIElementTypeTable/XCUIElementTypeCell");
        // chat list in chat page
        List<WebElement> webElements = driver.findElements(chatList);
        appium.tapOrClick(webElements.get(position));
        appium.hardWait(2);
    }

}
