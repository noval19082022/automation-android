package pageobjects.mamikos.owner.editprofile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.List;

public class EditProfilePO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public EditProfilePO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "closeInviteEmailTextView")
    private WebElement closeInviteEmailTextView;

    @AndroidFindBy(id = "personalInformationView")
    private WebElement editProfileTextView;

    private By nameEditText = Constants.MOBILE_OS == Platform.ANDROID ? By.id("nameEditText"): By.xpath("//XCUIElementTypeButton[@name='Edit foto']/following-sibling::XCUIElementTypeTextField[1]");

    private By phoneNumberEditText = Constants.MOBILE_OS == Platform.ANDROID ? By.id("phoneNumberEditText"): By.xpath("//XCUIElementTypeButton[@name='Edit foto']/following-sibling::XCUIElementTypeTextField[2]");

    private By emailEditText = Constants.MOBILE_OS == Platform.ANDROID ? By.id("emailEditText"): By.xpath("//XCUIElementTypeButton[@name='Edit foto']/following-sibling::XCUIElementTypeTextField[3]");

    @AndroidFindBy(id = "paymentView")
    private WebElement paymentMenu;

    @AndroidFindBy(id = "mainTextInputAutoComplete")
    private List<WebElement> bankNameAndAddress;

    @AndroidFindBy(id = "mainTextInputEditText")
    private List<WebElement> bankAccountNameAndNumber;

    private By textinputerror = Constants.MOBILE_OS == Platform.ANDROID ? By.id("textinput_error"): By.xpath("//*[@value='Nama harus diisi minimal 4 karakter']");

    @AndroidFindBy(id = "messageTextView")
    private WebElement toastMessageUpdateProfile;
    /**
     * Click close on Invitation Email Banner Toast
     */
    public void clickOnInviteEmailBanner(){
        appium.clickOn(closeInviteEmailTextView);
    }

    /**
     * Click on 'Edit Profile'
     */
    public void clickOnEditProfile(){
        appium.clickOn(editProfileTextView);
    }

    /**
     * Get Name Edit Text
     * @return Name
     */
    public String getNameEditText() throws InterruptedException {
        appium.waitInCaseElementVisible(nameEditText, 10);
        appium.hardWait(3);
        return appium.getText(nameEditText);
    }

    /**
     * Get Phone Number Edit Text
     * @return Phone Number
     */
    public String getPhoneNumberEditText() throws InterruptedException {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.hideKeyboard();
        }
        appium.waitTillElementIsVisible(phoneNumberEditText,10);
        appium.hardWait(2);
        return appium.getText(phoneNumberEditText);
    }

    /**
     * Get Email Edit Text
     * @return Email
     */
    public String getEmailEditText() throws InterruptedException {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.hideKeyboard();
        }
           appium.waitInCaseElementVisible(emailEditText, 10);
           appium.hardWait(3);
           return appium.getText(emailEditText);
    }

    /**
     * Click on Bank Account Tab
     */
    public void clickOnPaymentMenu(){
        appium.clickOn(paymentMenu);
    }

    /**
     * Get Bank Name Text
     * @return Bank Name
     */
    public String getBankNameText() throws InterruptedException {
        appium.hardWait(3);
        return appium.getText(bankNameAndAddress.get(0));
    }

    /**
     * Get Bank Account Number Text
     * @return Bank Account
     */
    public String getBankAccountNumberText(){
        return appium.getText(bankAccountNameAndNumber.get(0));
    }

    /**
     * Get Bank Account Name Text
     * @return Bank Account Name
     */
    public String getBankAccountNameText(){
        return appium.getText(bankAccountNameAndNumber.get(1));
    }

    /**
     * Input owner's full name in nama lengkap field
     * @param name is owner full name
     */
    public void inputOwnerName(String name) throws InterruptedException{
        appium.enterText(nameEditText, name, true);
        appium.hardWait(3);
        appium.tapOrClick(nameEditText);
    }

    /**
     * Get Bank Input name error text
     * @return Nama harus diisi minimal 4 karakter.
     */
    public String getTextInputError(){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.hideKeyboard();
            appium.waitInCaseElementVisible(textinputerror, 10);
        }
        return appium.getText(textinputerror);
    }

    /**
     * Get Bank Input name error text
     * @return Nama harus diisi minimal 4 karakter.
     */
    public String getToastMessageUpdateProfile(){
        return appium.getText(toastMessageUpdateProfile);
    }

    /**
     * @return return true if message error edit name is present. Otherwise false
     */
    public boolean isErrorMessageEditNamePresent() {
        return appium.isElementPresent(textinputerror);
    }
}
