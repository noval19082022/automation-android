package pageobjects.mamikos.owner.jobads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class JobAdsCompanyNamePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public JobAdsCompanyNamePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);


        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id="actionLaterButton")
    private WebElement actionLaterButton;

    @AndroidFindBy(xpath = "//*[@text='BARU']")
    private WebElement newPopup;

    @AndroidFindBy(id="companyLocationView")
    private WebElement companyLocationInput;

    @AndroidFindBy(id="updateAllPropertyButton")
    private WebElement updateAllPropertyButton;

    @AndroidFindBy(id="vacancyOwnerTextView")
    private WebElement vacancyOwnerTab;

    @AndroidFindBy(id="companyNameEditText")
    private WebElement companyInputText;

    @AndroidFindBy(id="companyAddressEditText")
    private WebElement companyAddressInput;

    @AndroidFindBy(id="addPhotoButton")
    private WebElement addPhotoButton;

    @AndroidFindBy(id="nextFormButton")
    private WebElement submitFormButton;

    @AndroidFindBy(id="markerA2AnchoredImageView")
    private WebElement markerAnchor;

    @AndroidFindBy(xpath = "//*[@text='OK']")
    private WebElement okButton;

    @AndroidFindBy(xpath = "//*[@resource-id='com.google.android.apps.photos:id/image']")
    private WebElement photoButton;

    @AndroidFindBy(id="companyLocationCheckBox")
    private WebElement companyLocationCheckBox;

    @AndroidFindBy(id="addVacancyScroll")
    private WebElement scrollView;

    @AndroidFindBy(xpath = "//*[contains(@content-desc, 'Photo taken on')]")
    private WebElement itemImage;

    @AndroidFindBy(id="deletePhotoImageView")
    private WebElement deletePhotoImage;

    /**
     * Click on 'New' Pop Up
     */
    public void clickOnNewPopUp(){
        appium.tapOrClick(newPopup);
    }

    /**
     * Click on 'Later' Pop Up on Iklan Saya Menu
     */
    public void clickOnLaterButton(){
        appium.tapOrClick(actionLaterButton);
    }

    /**
     * Click on 'Vacancy Owner' Tab
     */
    public void clickOnVacancyOwnerTab(){
        appium.tapOrClick(vacancyOwnerTab);
    }

    /**
     * Fill out First Form in 'Add Job Ad' Screen
     * @param companyName Company Name
     * @param companyAddress Company Address
     */
    public void fillForm(String companyName, String companyAddress) throws InterruptedException {
        appium.enterText(companyInputText, companyName, false);
        appium.tapOrClick(companyLocationInput);
        appium.tapOrClick(markerAnchor);
        appium.waitInCaseElementVisible(okButton,1);
        appium.tapOrClick(okButton);
        appium.enterText(companyAddressInput, companyAddress, false);
        appium.tapOrClick(companyLocationCheckBox);
        appium.scrollToElementByText("+");
        if(appium.waitInCaseElementVisible(deletePhotoImage, 1) != null)
        {
            appium.tapOrClick(deletePhotoImage);
        }
        appium.tapOrClick(addPhotoButton);
        appium.tapOrClick(photoButton);
        appium.tapOrClick(itemImage);
        appium.hardWait(5);
        appium.waitInCaseElementVisible(submitFormButton, 1);
    }

    /**
     * Click on the 'Submit' Button after fill out the form
     */
    public void clickOnSubmitButton(){
        appium.tapOrClick(submitFormButton);
    }
}
