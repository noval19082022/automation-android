package pageobjects.mamikos.owner.jobads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class JobAdsEditMenuPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public JobAdsEditMenuPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id="statisticVacancyButton")
    private WebElement editDataIklan;

    @AndroidFindBy(id="editVacancyButton")
    private WebElement editVacancyButton;

    @AndroidFindBy(id="companyNameEditText")
    private WebElement companyInputText;

    @AndroidFindBy(id="reactiveVacancyButton")
    private WebElement reactiveVacancyButton;

    @AndroidFindBy(id="android:id/button1")
    private WebElement yesConfirmationButton;

    @AndroidFindBy(id="openVacancyButton")
    private WebElement openVacancyButton;

    @AndroidFindBy(id="deleteVacancyButton")
    private WebElement deleteVacancyButton;

    @AndroidFindBy(id="mainItemOwnerVacancy")
    private WebElement mainItemContainer;

    /**
     * Click on 'Edit Ads Data' Button
     */
    public void clickOnEditAdsData(){
        appium.tapOrClick(editDataIklan);
    }

    /**
     * Click on 'Edit Vacancy' Button
     */
    public void clickOnEditVacancyButton(){
        appium.tapOrClick(editVacancyButton);
        appium.waitInCaseElementVisible(companyInputText, 5);
    }

    /**
     * Click on 'Reactive Vacancy' Button
     */
    public void clickOnReactiveVacancyButton(){
        appium.tapOrClick(reactiveVacancyButton);
    }

    /**
     * Click on 'Action Confirmation' Pop up
     */
    public void clickOnActionConfirmationPopUp(){
        appium.tapOrClick(yesConfirmationButton);
    }

    /**
     * CLick on 'Delete Vacancy' Button
     */
    public void clickOnDeleteVacancyButton(){
        appium.tapOrClick(deleteVacancyButton);
    }

    /**
     * Is Open Vacancy Button have "enabled" attribute
     * @return true. Open Vacancy Button should have attribute "enabled"
     */
    public Boolean isOpenVacancyEnabled(){
        return appium.getElementAttributeValue(openVacancyButton, "enabled") != null;
    }

    /**
     * Get Open Vacancy Text
     * @return Open Vacancy Text 'DITUTUP'
     */
    public String getOpenVacancyText(){
        return appium.getText(openVacancyButton);
    }

    /**
     * Is Main Container Content Null
     * @return Main Container Content
     */
    public Boolean isMainContainerNull(){
        return appium.waitInCaseElementVisible(mainItemContainer, 3) == null;
    }
}
