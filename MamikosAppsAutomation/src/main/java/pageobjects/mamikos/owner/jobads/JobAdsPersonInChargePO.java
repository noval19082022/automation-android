package pageobjects.mamikos.owner.jobads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class JobAdsPersonInChargePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public JobAdsPersonInChargePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);


        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id="ownerNameEditText")
    private WebElement ownerNameInput;

    @AndroidFindBy(id="ownerPhoneEditText")
    private WebElement ownerPhoneInput;

    @AndroidFindBy(id="ownerEmailEditText")
    private WebElement ownerEmailInput;

    @AndroidFindBy(id="expiredDateView")
    private WebElement expiredDate;

    @AndroidFindBy(id="com.git.mami.kos:id/mdtp_next_month_arrow")
    private WebElement nextMonthArrow;

    @AndroidFindBy(id="com.git.mami.kos:id/mdtp_ok")
    private WebElement dateModalOk;

    @AndroidFindBy(id="nameCompanyTextView")
    private WebElement companyNameTextView;

    @AndroidFindBy(id="jobNameTextView")
    private WebElement jobNameTextView;

    @AndroidFindBy(id="stateCompanyTextView")
    private WebElement jobStatusText;

    @AndroidFindBy(id="jobSalaryTextView")
    private WebElement jobSalaryText;

    @AndroidFindBy(id = "statisticCompanyButton")
    private WebElement editPromoLabel;

    /**
     * Fill out Third Form in 'Add Job Ad' Screen
     * @param personInCharge Person In Charge
     * @param phoneNumber Phone Number
     * @param email Email
     * @param expDate Expired Date
     */
    public void fillThirdForm(String personInCharge, String phoneNumber, String email, String expDate) {
        appium.enterText(ownerNameInput, personInCharge, false);
        appium.enterText(ownerPhoneInput, phoneNumber, false);
        appium.enterText(ownerEmailInput, email, false);
        appium.tapOrClick(expiredDate);

        if(!isExpiredDatePresent(expDate)) {
            appium.tapOrClick(nextMonthArrow);
        }
        appium.clickOn(By.xpath("//android.view.View[@content-desc='" + expDate + "']"));
        appium.tapOrClick(dateModalOk);
    }

    /**
     * Is the 'Expired Date' Present
     */
    public boolean isExpiredDatePresent(String expDate) {
        return appium.waitInCaseElementVisible(By.xpath("//android.view.View[@content-desc='" + expDate + "']"),3) != null;
    }

    /**
     * Is 'Job Status' Present
     */
    public Boolean isJobStatusPresent(){
        return appium.waitInCaseElementVisible(jobStatusText,3) != null;
    }

    /**
     * Get 'Job Status' Text
     */
    public String getJobStatusText(){
        return appium.getText(jobStatusText);
    }

    /**
     * Is 'Job Salary' Present
     */
    public Boolean isJobSalaryPresent(){
        return appium.waitInCaseElementVisible(jobSalaryText,3) != null;
    }

    /**
     * Get 'Job Salary' Text
     */
    public String getJobSalaryText(){
        return appium.getText(jobSalaryText);
    }

    /**
     * Is 'Company Name' Present
     */
    public Boolean isCompanyNamePresent(){
        return appium.waitInCaseElementVisible(companyNameTextView, 3) != null;
    }

    /**
     * Is 'Job Name' Present
     */
    public Boolean isJobNamePresent(){
        return appium.waitInCaseElementVisible(jobNameTextView,3) != null;
    }

    /**
     * Get 'Company Name' Text
     */
    public String getCompanyNameText(){
        return appium.getText(companyNameTextView);
    }

    /**
     * Get 'Job Name' Text
     */
    public String getJobNameText(){
        return appium.getText(jobNameTextView);
    }

    /**
     * Click on 'Edit & Promote' Label
     */
    public void clickOnEditPromoteLabel() throws InterruptedException {
        appium.tapOrClick(editPromoLabel);
        appium.waitInCaseElementVisible(companyNameTextView,3);
        appium.hardWait(5);
    }
}
