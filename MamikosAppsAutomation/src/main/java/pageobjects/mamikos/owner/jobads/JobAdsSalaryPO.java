package pageobjects.mamikos.owner.jobads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class JobAdsSalaryPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public JobAdsSalaryPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);


        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id="vacancyNameEditText")
    private WebElement vacancyNameInput;

    @AndroidFindBy(id="vacancyEducationSpinner")
    private WebElement vacancyEducationSpinner;

    @AndroidFindBy(id="jobStatusSpinner")
    private WebElement jobStatusSpinner;

    @AndroidFindBy(id="vacancyDescriptionEditText")
    private WebElement vacancyDescriptionInput;

    @AndroidFindBy(id="vacancyMinSalaryEditText")
    private WebElement vacancyMinSalaryInput;

    @AndroidFindBy(id="vacancyMaxSalaryEditText")
    private WebElement vacancyMaxSalaryInput;

    @AndroidFindBy(id="visibilitySalaryCheckBox")
    private WebElement showSalaryCheckbox;

    @AndroidFindBy(id="unitSalarySpinner")
    private WebElement unitSalarySpinner;

    /**
     * Fill out Second Form in 'Add Job Ad' Screen
     * @param jobName Job Name
     * @param minStudy Minimum Study
     * @param jobStat Job Status
     * @param jobDesc Job Description
     * @param minSal Minimum Salary
     * @param maxSal Maximum Salary
     * @param salType Salary Type
     */
    public void fillSecondForm(String jobName, String minStudy, String jobStat, String jobDesc, String minSal, String maxSal, String salType){
        appium.enterText(vacancyNameInput, jobName, false);

        appium.tapOrClick(vacancyEducationSpinner);
        appium.clickOn(By.xpath("//*[@text='" + minStudy + "']"));

        appium.tapOrClick(jobStatusSpinner);
        appium.clickOn(By.xpath("//*[@text='" + jobStat +"']"));

        appium.enterText(vacancyDescriptionInput, jobDesc, false);
        appium.enterText(vacancyMinSalaryInput, minSal, false);
        appium.enterText(vacancyMaxSalaryInput, maxSal, false);

        appium.tapOrClick(showSalaryCheckbox);
        appium.tapOrClick(unitSalarySpinner);
        appium.clickOn(By.xpath("//*[@text='" + salType +"']"));
    }


}
