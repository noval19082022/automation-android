package pageobjects.mamikos.owner.kostads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class KostAdsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public KostAdsPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "viewMore")
    private WebElement expandKostDropdown;

    @AndroidFindBy(id = "chooseFilterTimeView")
    private WebElement filterTimeRange;

    @AndroidFindBy(id = "clickCountCardView")
    private WebElement clickStatisticText;

    @AndroidFindBy(id = "chatCountCardView")
    private WebElement chatStatisticText;

    @AndroidFindBy(id = "loveCountCardView")
    private WebElement favStatisticText;

    @AndroidFindBy(id = "reviewCountCardView")
    private WebElement reviewStatisticText;

    @AndroidFindBy(id = "setPremiumBtn")
    private WebElement setPremiumButton;

    @AndroidFindBy(id = "searchAdsEditText")
    private WebElement searchAdsBar;

    @AndroidFindBy(id = "edFormSearch")
    private WebElement searchMyKosBar;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/titleSuggestionMyRoomTextView']")
    private WebElement searchResultKos;

    @AndroidFindBy(id = "editButtonActiveView")
    private WebElement editKosButton;

    @iOSXCUITFindBy(accessibility = "Atur")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/ownerManageFragmentMenus']/*[1]/*[2]/*[1]")
    private WebElement arrangeDataButton;


    /**
     * Click expand kos button
     */
    public void clickExpandKost() {
        appium.clickOn(expandKostDropdown);
    }

    /**
     * Click filter time range
     */
    public void clickStatisticTimeRange() {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.clickOn(filterTimeRange);
    }

    /**
     * Check statistic click is present
     * @return true / false
     */
    public boolean isStatisticClickPresent() {
        return appium.waitInCaseElementVisible(clickStatisticText, 10) != null;
    }

    /**
     * Check statistic chat is present
     * @return true / false
     */
    public boolean isStatisticChatPresent() {
        return appium.waitInCaseElementVisible(chatStatisticText, 10) != null;
    }

    /**
     * Check statistic favorite is present
     * @return true / false
     */
    public boolean isStatisticFavoritePresent() {
        return appium.waitInCaseElementVisible(favStatisticText, 10) != null;
    }

    /**
     * Check statistic review is present
     * @return true / false
     */
    public boolean isStatisticReviewPresent() {
        return appium.waitInCaseElementVisible(reviewStatisticText, 10) != null;
    }

    /**
     * Click set premium button
     */
    public void clickSetPremium() {
        appium.clickOn(setPremiumButton);
    }

    /**
     * Click search ads bar then input text to search kos
     * @param kosName is text we want to search
     */
    public void searchMyKos(String kosName) {
        appium.clickOn(searchAdsBar);
        appium.enterText(searchMyKosBar, kosName, true);
    }

    /**
     * Click search kos first result
     */
    public void clickFirstSearchResult() {
        appium.clickOn(searchResultKos);
    }

    /**
     * Scroll then click edit kos data
     */
    public void clickEditKosData() {
        if (appium.waitInCaseElementVisible(editKosButton, 3) == null) {
            appium.scroll(0.5, 0.70, 0.30, 1000);
            appium.scroll(0.5, 0.70, 0.30, 1000);
        }
        appium.clickOn(editKosButton);
    }

    /**
     * Click arrange kos/apartment data button
     */
    public void clickArrange() {
        appium.clickOn(arrangeDataButton);
    }
}
