package pageobjects.mamikos.owner.mamiads;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;
import org.openqa.selenium.Platform;


public class BeliSaldoPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;
    TouchAction touchAction ;


    public BeliSaldoPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        touchAction= new TouchAction(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @iOSXCUITFindBy(accessibility = "Saldo MamiAds")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Saldo MamiAds']")
    private WebElement saldoMamiadsTitle;


    private By beliSaldoMamiadsButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Beli Saldo']") : By.id("Beli Saldo");

    @iOSXCUITFindBy(accessibility = "Beli Saldo MamiAds")
    @AndroidFindBy(id = "titleBuySaldoTextView")
    private WebElement titleBeliSaldoMamiadsText;

    @iOSXCUITFindBy(accessibility = "Riwayat")
    @AndroidFindBy(id = "historyTextView")
    private WebElement riwayatMamiadsButton;

    private By statistikIklanTab = By.id("//*[@text='Statistik Iklan']");

    private By iklanSayaTab = By.id("//*[@text='Iklan Saya']");

    @iOSXCUITFindBy(accessibility = "Riwayat Saldo")
    @AndroidFindBy(id = "titleCollapsingToolbarTextView")
    private WebElement titleRiwayatMamiadsText;

    private By cobaSekarangButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.Button[@text='Coba Sekarang']") : By.id("Coba Sekarang");


    /**
     * Click Saldo Mamiads Title
     */
    public void clickOnSaldoMamiadsTitle() {
        appium.tapOrClick(saldoMamiadsTitle);
    }

    /**
     * Click Beli Saldo Mamiads Button
     */
    public void clickOnBeliSaldoMamiadsButton() throws InterruptedException {
        appium.waitTillElementIsVisible(beliSaldoMamiadsButton,10);
        appium.hardWait(5);
        appium.tapOrClick(beliSaldoMamiadsButton);
    }

    /**
     * get title page beli saldo mamiads
     * @return title page beli saldo
     */
    public String getTitleBeliSaldoText() {
        appium.waitTillElementIsVisible(titleBeliSaldoMamiadsText,10);
        return appium.getText(titleBeliSaldoMamiadsText);
    }

    /**
     * Click Riwayat Mamiads Button
     */
    public void clickOnRiwayatButton() {
        appium.clickOn(riwayatMamiadsButton);
    }

    /**
     * get title page beli saldo mamiads
     * @return title page riwayat mamiads
     */
    public String getTitleRiwayatMamiadsText() throws InterruptedException {
        appium.waitInCaseElementVisible(titleRiwayatMamiadsText,5);
        appium.hardWait(3);
        return appium.getText(titleRiwayatMamiadsText);
    }

    /**
     * Click on Coba Sekarang button after click menu Mamiads
     */
    public void clickOnCobaSekarangButton() throws InterruptedException{
        do {
            appium.tapOrClick(cobaSekarangButton);
            appium.hardWait(3);
        } while (appium.isElementPresent(cobaSekarangButton));
    }

    /**
     * Click tab statistik iklan
     */
    public void clickOnTabStatistikIklanButton() {
        appium.tapOrClick(statistikIklanTab);
    }

    /**
     * Click tab statistik iklan
     */
    public void clickOnTabIklanSayaButton() {
        appium.clickOn(iklanSayaTab);
    }

    /**
     * Click icn back on beli saldo mamiads page
     */
    public void clickOnIcnBack() {
        touchAction.tap(PointOption.point(16,63)).perform();
    }
}
