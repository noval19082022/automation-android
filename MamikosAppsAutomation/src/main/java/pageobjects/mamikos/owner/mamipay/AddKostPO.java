package pageobjects.mamikos.owner.mamipay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class AddKostPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public AddKostPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Back\"]")
    @AndroidFindBy(id = "backImageView")
    private WebElement backButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Tambah Data Iklan\"]")
    @AndroidFindBy(id = "titleToolbarTextView")
    private WebElement adsTitle;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Tanya CS\"]")
    @AndroidFindBy(id = "formAskCsTextView")
    private WebElement askCSButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"nameLabel\"]")
    @AndroidFindBy(id = "addKostCV")
    private WebElement addNewKostButton;

    //--------------Add Kost for account that already have listing-----------------

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"+ Tambah Properti\"]")
    @AndroidFindBy(id = "addPropertyButtonCV")
    private WebElement addPropertyButtonInBottom;

    @AndroidFindBy(id = "btn_addKost_owner")
    private WebElement addAsNewKostButton;

    @AndroidFindBy(id = "rightButton")
    private WebElement addKostButtonInPopUp;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[2]//XCUIElementTypeTextField")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/nameKosView']//*[@resource-id='com.git.mami.kos:id/textInputEditText']")
    private WebElement kosNameField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[2]/following-sibling::*[1]//XCUIElementTypeTextField")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/inputRoomTypeCV']//*[@resource-id='com.git.mami.kos:id/textInputEditText']")
    private WebElement roomTypeField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[3]//XCUIElementTypeTextView")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/descriptionKosView']//*[@resource-id='com.git.mami.kos:id/textInputEditText']")
    private WebElement descriptionField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[6]/*[1]/*[1]/*[2]/*[2]/*[1]//XCUIElementTypeTextField")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/inputManagerNameCV']//*[@resource-id='com.git.mami.kos:id/textInputEditText']")
    private WebElement managerNameField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[6]/*[1]/*[1]/*[2]/*[2]/*[2]//XCUIElementTypeTextField")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/inputManagerPhoneCV']//*[@resource-id='com.git.mami.kos:id/textInputEditText']")
    private WebElement managerPhoneField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[5]//XCUIElementTypeTextField")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/timeBuildKosView']//*[@resource-id='com.git.mami.kos:id/textInputEditText']")
    private WebElement kosYearBuildDropdown;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/*[3]")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/nameKosView']//*[@resource-id='com.git.mami.kos:id/textErrorView']")
    private WebElement errorKosNameField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[2]/following-sibling::*[1]//XCUIElementTypeTextField/parent::*/parent::*/parent::*/following-sibling::*")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/inputRoomTypeCV']//*[@resource-id='com.git.mami.kos:id/textErrorView']")
    private WebElement errorRoomTypeField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/genderView']//*[@resource-id='com.git.mami.kos:id/errorView']")
    private WebElement errorKosTypeField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/descriptionKosView']//*[@resource-id='com.git.mami.kos:id/textErrorView']")
    private WebElement errorDescField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/timeBuildKosView']//*[@resource-id='com.git.mami.kos:id/textErrorView']")
    private WebElement errorKosYearBuildField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[6]/*[1]/*[1]/*[2]/*[2]/*[1]//XCUIElementTypeTextField/parent::*/parent::*/parent::*/following-sibling::*")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/inputManagerNameCV']//*[@resource-id='com.git.mami.kos:id/textErrorView']")
    private WebElement errorManagerNameField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[6]/*[1]/*[1]/*[2]/*[2]/*[2]//XCUIElementTypeTextField/parent::*/parent::*/parent::*/following-sibling::*")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/inputManagerPhoneCV']//*[@resource-id='com.git.mami.kos:id/textErrorView']")
    private WebElement errorManagerPhoneField;

    @AndroidFindBy(id = "et_agent_kost_address")
    private WebElement kosAddressField;

    @AndroidFindBy(id = "et_agent_owner_name")
    private WebElement kosOwnerNameField;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Lanjutkan\"]")
    @AndroidFindBy(id = "nextButton")
    private WebElement nextButton;

    @iOSXCUITFindBy(accessibility = "positiveButton")
    @AndroidFindBy(id = "okButton")
    private WebElement chooseButton;

    @AndroidFindBy(id = "campurKostRadioButton")
    private WebElement mixedKostRadioButton;

    @AndroidFindBy(id = "putraKostRadioButton")
    private WebElement maleKostRadioButton;

    @AndroidFindBy(id = "putriKostRadioButton")
    private WebElement femaleKostRadioButton;

    @AndroidFindBy(id = "dailyPriceKostEditText")
    private WebElement dailyPriceKostField;

    @AndroidFindBy(id = "weeklyPriceKostEditText")
    private WebElement weeklyPriceKostField;

    @AndroidFindBy(id = "monthlyPriceKostEditText")
    private WebElement monthlyPriceKostField;

    @AndroidFindBy(id = "threeMinimumPaymentEditText")
    private WebElement threeMonthMinimumPaymentField;

    @AndroidFindBy(id = "sixMinimumPaymentEditText")
    private WebElement sixMonthMinimumPaymentField;

    @AndroidFindBy(id = "yearlyPriceKostEditText")
    private WebElement yearlyPriceKostField;

    @AndroidFindBy(id = "minimumPaymentSpinner")
    private WebElement minimumPaymentDropdown;

    @AndroidFindBy(id = "notePriceEditText")
    private WebElement additionalPriceField;

    @AndroidFindBy(id = "nameAgentKostEditText")
    private WebElement nameInputPersonField;

    @AndroidFindBy(id = "emailAgentKostEditText")
    private WebElement emailInputPersonField;

    @AndroidFindBy(id = "phoneAgentEditText")
    private WebElement phoneInputPersonField;

    @AndroidFindBy(id = "titleBathroomFacilityView")
    private WebElement expandBathroomFacilityButton;

    @AndroidFindBy(id = "titlePublicFacilityView")
    private WebElement expandPublicFacilityButton;

    @AndroidFindBy(id = "titleParkingFacilityView")
    private WebElement expandParkingFacilityButton;

    @AndroidFindBy(id = "titleAroundFacilityView")
    private WebElement expandAroundFacilityButton;

    @AndroidFindBy(id = "typeOneRoomSizeView")
    private WebElement roomSize3X3Button;

    @AndroidFindBy(id = "typeTwoRoomSizeView")
    private WebElement roomSize3X4Button;

    @AndroidFindBy(id = "typeThreeRoomSizeView")
    private WebElement roomSize4X4Button;

    @AndroidFindBy(id = "widthRoomEditText")
    private WebElement widthRoomField;

    @AndroidFindBy(id = "heightRoomEditText")
    private WebElement heightRoomField;

    @AndroidFindBy(id = "buildYearSpinner")
    private WebElement buildYearDropdown;

    @AndroidFindBy(xpath = "//*[contains(@resource-id, 'textInputTotalRoom')]//android.widget.EditText")
    private WebElement numberOfRoomsField;

    @AndroidFindBy(xpath = "//*[contains(@resource-id, 'textInputAvailableRoom')]//android.widget.EditText")
    private WebElement numberOfRoomAvailableField;

    @AndroidFindBy(id = "noteEditText")
    private WebElement otherNotesField;

    @AndroidFindBy(id = "propertyPolicyCheckBox")
    private WebElement propertyPolicyCheckBox;

    @AndroidFindBy(id = "addCoverButton")
    private WebElement addPhotoCoverButton;

    @AndroidFindBy(xpath = "//*[contains(@resource-id, 'photoBuildingGridView')]//android.widget.Button")
    private WebElement addBuildingPhotoButton;

    @AndroidFindBy(xpath = "//*[contains(@resource-id, 'photoRoomView')]//android.widget.Button")
    private WebElement addPhotoRoomButton;

    @AndroidFindBy(xpath = "//*[contains(@resource-id, 'photoBathroomView')]//android.widget.Button")
    private WebElement addPhotoBathRoomButton;

    @AndroidFindBy(id = "fb_photo_fac")
    private WebElement addOtherFacilityPhotoButton;

    @AndroidFindBy(id = "saveFormButton")
    private WebElement saveFormButton;

    @AndroidFindBy(id = "fullViewSaveRoomButton")
    private WebElement doneButton;

    @AndroidFindBy(id = "titleTextView")
    private WebElement successMessageText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Mulai\"]")
    @AndroidFindBy(id = "nextButtonView")
    private WebElement doneSuccessMessageButton;

    @AndroidFindBy(id = "activateBookingView")
    private WebElement activateBookingPopUp;

    @AndroidFindBy(id = "leftButton")
    private WebElement laterButton;

    @AndroidFindBy(id = "et_desc_photo")
    private WebElement photoFacilityDescField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/adPropertyRecyclerView']//android.widget.LinearLayout[1]//*[@resource-id='com.git.mami.kos:id/ownerKostGenderTextView']")
    private WebElement firstKostTypeLabel;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/adPropertyRecyclerView']//android.widget.LinearLayout[1]//*[@resource-id='com.git.mami.kos:id/statusActiveView']")
    private WebElement firstKostStatusLabel;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/adPropertyRecyclerView']//android.widget.LinearLayout[1]//*[@resource-id='com.git.mami.kos:id/kostNameTextView']")
    private WebElement firstKostNameLabel;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/adPropertyRecyclerView']//android.widget.LinearLayout[1]//*[@resource-id='com.git.mami.kos:id/kostAddressTextView']")
    private WebElement firstKostAddressLabel;

    @AndroidFindBy(id = "inputLabelTextView")
    private WebElement errorMessageKosNameLabel;

    @AndroidFindBy(id = "addPropertyButton")
    private WebElement addKosInHomeButton;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@resource-id='com.git.mami.kos:id/mainView']/*[1]/*[1]/*[1]/android.widget.TextView[1]")
    private WebElement titleInstantBookingBenefitLabel;

    @AndroidFindBy(id = "activationBookingKosButton")
    private WebElement activationBookingKosButton;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/monthlyPriceKostView']//*[@resource-id='com.git.mami.kos:id/textinput_error']")
    private WebElement errorMessageMonthlyPriceField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/dailyPriceKostView']//*[@resource-id='com.git.mami.kos:id/textinput_error']")
    private WebElement errorMessageDailyPriceField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/weeklyPriceKostView']//*[@resource-id='com.git.mami.kos:id/textinput_error']")
    private WebElement errorMessageWeeklyPriceField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/threeMinimumPaymentView']//*[@resource-id='com.git.mami.kos:id/textinput_error']")
    private WebElement errorMessage3MonthlyPriceField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/sixMinimumPaymentView']//*[@resource-id='com.git.mami.kos:id/textinput_error']")
    private WebElement errorMessage6MonthlyPriceField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/yearlyPriceKostView']//*[@resource-id='com.git.mami.kos:id/textinput_error']")
    private WebElement errorMessageYearlyPriceField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/messageWarningTextView']")
    private WebElement errorMessageRoomField;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[2]/following-sibling::*[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]")
    @AndroidFindBy(id = "checkBox")
    private WebElement roomTypeCheckbox;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name=\"AddressFieldView\"])[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]")
    private WebElement kosAdminCheckbox;

    @iOSXCUITFindBy(accessibility = "Putri")
    @AndroidFindBy(id = "femaleView")
    private WebElement femaleButton;

    @iOSXCUITFindBy(accessibility = "Putra")
    @AndroidFindBy(id = "maleView")
    private WebElement maleButton;

    @iOSXCUITFindBy(accessibility = "Campur")
    @AndroidFindBy(id = "mixedView")
    private WebElement mixedButton;

    @AndroidFindBy(xpath = "//*[@text='Selesai']")
    private WebElement keyboardDoneButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypePickerWheel")
    private WebElement yearPickerWheel;


    /**
     * Check element Back button is Displayed
     * @return Status
     */
    public boolean isBackButtonDisplayed() {
        return backButton.isDisplayed();
    }

    /**
     * Check element Title is Displayed
     * @return Status
     */
    public boolean isTitleDisplayed() {
        return adsTitle.isDisplayed();
    }

    /**
     * Check element Kost Baru button is Displayed
     * @return Status
     */
    public boolean isAddNewKostButtonDisplayed() {
        return addNewKostButton.isDisplayed();
    }

    /**
     * Click Add Property Button
     */
    public void clickAddPropertyButton() {
        appium.clickOn(addPropertyButtonInBottom);
    }

    /**
     * Click New Kost Button
     */
    public void clickNewKostButton() {
        appium.clickOn(addNewKostButton);
    }

    /**
     * Click Add As New Kost Button
     */
    public void clickAddAsNewKostButton() throws InterruptedException {
        appium.clickOn(addAsNewKostButton);
        appium.hardWait(2);
    }

    /**
     * Click Add Kost Button in Pop Up
     */
    public void clickAddKostButtonInPopUp() {
        if (appium.waitInCaseElementVisible(addKostButtonInPopUp, 5) != null) {
            appium.clickOn(addKostButtonInPopUp);
        }
    }

    /**
     * Fill kost name field
     * @param name is kost name
     */
    public void fillKostName(String name) {
        appium.enterText(kosNameField, name, true);
        if (Constants.MOBILE_OS == Platform.IOS) {
            driver.findElementByName("Done").click();
        }
    }

    /**
     * Fill kost address field
     * @param address is kost address
     */
    public void fillKostAddress(String address) {
        appium.enterText(kosAddressField, address, false);
    }

    /**
     * Fill kost owner name if exist
     * @param ownerName is the kost owner name
     */
    public void fillKostOwnerName(String ownerName) {
        if (appium.waitInCaseElementVisible(kosOwnerNameField, 3) != null) {
            appium.enterText(kosOwnerNameField, ownerName, false);
        }
    }

    /**
     * Fill kost manager name field
     * @param managerName is kost manager name
     */
    public void fillKostManagerName(String managerName) {
        appium.enterText(managerNameField, managerName, false);
        if (Constants.MOBILE_OS == Platform.IOS) {
            driver.findElementByName("Done").click();
        }
    }

    /**
     * Fill kost manager phone field
     * @param managerPhone is manager phone number
     */
    public void fillKostManagerPhone(String managerPhone) {
        appium.enterText(managerPhoneField, managerPhone, false);
        if (Constants.MOBILE_OS == Platform.IOS) {
            driver.findElementByName("Done").click();
        }
    }

    /**
     * Click Next Button
     */
    public void clickNextButton() {
        appium.clickOn(nextButton);
    }

    /**
     * Click Kost Type Radio Button
     * @param selection is String kost type
     */
    public void clickKostTypeRadioButton(String selection) {
        if (selection.equals("Campur")) {
            appium.clickOn(mixedKostRadioButton);
        } else if (selection.equals("Putra")) {
            appium.clickOn(maleKostRadioButton);
        } else {
            appium.clickOn(femaleKostRadioButton);
        }
    }

    /**
     * Fill daily kost renting price field
     * @param dailyPrice is the weekly renting price
     */
    public void fillDailyKostPrice(String dailyPrice) {
        appium.enterText(dailyPriceKostField, dailyPrice, false);
    }

    /**
     * Fill weekly kost renting price field
     * @param weeklyPrice is the weekly renting price
     */
    public void fillWeeklyKostPrice(String weeklyPrice) {
        appium.enterText(weeklyPriceKostField, weeklyPrice, false);
    }

    /**
     * Fill monthly kost renting price field
     * @param monthlyPrice is the monthly renting price
     */
    public void fillMonthlyKostPrice(String monthlyPrice) {
        appium.enterText(monthlyPriceKostField, monthlyPrice, false);
    }

    /**
     * Fill minimum 3 month kost
     * @param threeMonthly is the 3 monthly renting price
     */
    public void fill3MonthlyKostPrice(String threeMonthly) {
        appium.enterText(threeMonthMinimumPaymentField, threeMonthly, false);
    }

    /**
     * Fill minimum 6 month kost
     * @param sixMonthly is the 6 monthly renting price
     */
    public void fill6MonthlyKostPrice(String sixMonthly) {
        appium.enterText(sixMonthMinimumPaymentField, sixMonthly, false);
    }

    /**
     * Fill yearly kost price
     * @param yearly is the yearly renting price
     */
    public void fillYearlyKostPrice(String yearly) {
        appium.enterText(yearlyPriceKostField, yearly, false);
    }

    /**
     * Select minimum payment
     * @param minPayment is minimum payment
     */
    public void selectMinPayment(String minPayment) {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.clickOn(minimumPaymentDropdown);
        String xpathLocator = "//android.widget.CheckedTextView[@text='" + minPayment + "']";
        WebElement minPaymentSelection = driver.findElement(By.xpath(xpathLocator));
        appium.clickOn(minPaymentSelection);
    }

    /**
     * Fill additional kost price
     * @param additionalPrice is the additional renting price
     */
    public void fillAdditionalKostPrice(String additionalPrice) {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.enterText(additionalPriceField, additionalPrice, false);
    }

    /**
     * Fill kost agent name
     * @param agentName is the kost agent name
     */
    public void fillKostAgentName(String agentName) {
        appium.enterText(nameInputPersonField, agentName, false);
    }

    /**
     * Fill kost agent email
     * @param agentEmail is the kost agent email
     */
    public void fillKostAgentEmail(String agentEmail) {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.enterText(emailInputPersonField, agentEmail, false);
    }

    /**
     * Fill kost agent phone
     * @param agentPhone is the kost agent phone
     */
    public void fillKostAgentPhone(String agentPhone) {
        appium.enterText(phoneInputPersonField, agentPhone, false);
    }

    /**
     * Click Expand Bathroom Facility button
     */
    public void clickExpandBathroomFacility() {
        appium.waitInCaseElementVisible(expandBathroomFacilityButton, 2);
        appium.clickOn(expandBathroomFacilityButton);
    }

    /**
     * Click Expand Public Facility button
     */
    public void clickExpandPublicFacility() {
        appium.clickOn(expandPublicFacilityButton);
    }

    /**
     * Click Expand Parking Facility button
     */
    public void clickExpandParkingFacility() {
        appium.clickOn(expandParkingFacilityButton);
    }

    /**
     * Click Expand Around Facility button
     */
    public void clickExpandAroundFacility() {
        if (appium.waitInCaseElementVisible(expandAroundFacilityButton, 2) == null) {
            appium.scroll(0.5, 0.60, 0.20, 2000);
        }
        appium.clickOn(expandAroundFacilityButton);
    }

    /**
     * Set room size by clicking existing option or manual type
     * @param size is String text for size
     *             can use size outside available option and it will set manually by typing
     */
    public void setRoomSize(String size) {
        switch (size) {
            case "3 X 3":
                appium.clickOn(roomSize3X3Button);
                break;
            case "3 X 4":
                appium.clickOn(roomSize3X4Button);
                break;
            case "4 X 4":
                appium.clickOn(roomSize4X4Button);
                break;
            default:
                String width = size.substring(0, size.indexOf(' '));
                String height = size.substring(0, size.indexOf('X') + 1);
                appium.enterText(widthRoomField, width, false);
                appium.enterText(heightRoomField, height, false);
                appium.hideKeyboard();
                break;
        }
    }

    /**
     * Click year built dropdown and select the year
     */
    public void setYearBuiltOrLastRenov(String year) {
        appium.clickOn(buildYearDropdown);
        appium.scrollToElementByText(year);
        appium.clickElementByText(year);
    }

    /**
     * Fill total room field
     * @param totalRooms is the total kos rooms
     */
    public void fillTotalRooms(String totalRooms) {
        appium.enterText(numberOfRoomsField, totalRooms, false);
    }

    /**
     * Fill total available room field
     * @param availableRooms is the total kos rooms that's available
     */
    public void fillTotalAvailableRooms(String availableRooms) {
        appium.enterText(numberOfRoomAvailableField, availableRooms, false);
    }

    /**
     * Fill notes field
     * @param note is the text for notes
     */
    public void fillNotes(String note) {
        if (appium.waitInCaseElementVisible(otherNotesField, 2) == null) {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        appium.enterText(otherNotesField, note, false);
    }

    /**
     * Fill description field
     * @param desc is the text for kos description
     */
    public void fillDescription(String desc) {
        if (appium.waitInCaseElementVisible(descriptionField, 2) == null) {
            if (Constants.MOBILE_OS == Platform.ANDROID) {
                appium.scrollToElementByText("Ceritakan hal menarik tentang kos Anda.");
            }
            else {
                appium.scroll(0.5, 0.80, 0.20, 2000);
            }
        }
        appium.enterText(descriptionField, desc, true);
    }

    /**
     * Click property policy
     */
    public void clickPropertyPolicy() {
        appium.clickOn(propertyPolicyCheckBox);
    }

    /**
     * Click add cover photo
     */
    public void clickPhotoCover() {
        appium.clickOn(addPhotoCoverButton);
    }

    /**
     * Click add room photo
     */
    public void clickPhotoRoom() {
        appium.clickOn(addPhotoRoomButton);
    }

    /**
     * Click add bathroom photo
     */
    public void clickPhotoBathRoom() {
        appium.clickOn(addPhotoBathRoomButton);
    }

    /**
     * Click add building photo
     */
    public void clickBuildingPhoto() {
        appium.clickOn(addBuildingPhotoButton);
    }

    /**
     * Click add other facility photo
     */
    public void clickOtherFacilityPhoto() {
        appium.clickOn(addOtherFacilityPhotoButton);
    }

    /**
     * Fill facility photo's description
     * @param description is the text for photo description
     */
    public void fillFacilityPhotoDesc(String description) {
        appium.enterText(photoFacilityDescField, description, false);
    }

    /**
     * Click next/done in pop up page if exist
     * @throws InterruptedException
     */
    public void clickNextInAttention() throws InterruptedException {
        if (appium.waitInCaseElementVisible(doneSuccessMessageButton, 7) != null) {
            appium.clickOn(doneSuccessMessageButton);
            appium.hardWait(5);
        }
    }

    /**
     * Click done button
     */
    public void clickDone() throws InterruptedException {
        appium.clickOn(doneButton);
        appium.hardWait(3);
    }

    /**
     * Get success message
     * @return String text success message
     */
    public String getSuccessMessage() {
        return appium.getText(successMessageText);
    }

    /**
     * Click later in activate booking pop up
     */
    public void clickLaterInActivateBookingPopUp() {
        appium.clickOn(laterButton);
    }

    /**
     * Check if activate booking pop up appear
     * @return boolean true if appear
     */
    public boolean isActivateBookingPopUpAppear() {
        return appium.waitInCaseElementVisible(activateBookingPopUp, 5) != null;
    }

    /**
     * Get first kost gender type
     * @return String gender type
     */
    public String getFirstKostType() {
        return appium.getText(firstKostTypeLabel);
    }

    /**
     * Get first kost status
     * @return String kost status
     */
    public String getFirstKostStatus() {
        return appium.getText(firstKostStatusLabel);
    }

    /**
     * Get first kost name
     * @return String kost name
     */
    public String getFirstKostName() {
        return appium.getText(firstKostNameLabel);
    }

    /**
     * Get first kost address
     * @return String kost address
     */
    public String getFirstKostAddress() {
        return appium.getText(firstKostAddressLabel);
    }

    /**
     * Wait certain text appear/not,
     * if not appear then scroll down
     * @param text is text we want to search
     */
    public void waitThenScrollToText(String text) {
        if (appium.waitInCaseElementVisible(By.xpath("//*[@text='"+ text +"']"), 3) == null) {
            appium.scroll(0.5, 0.60, 0.30, 1000);
        }
    }

    /**
     * Get error message in kos name field
     * @return String error message
     */
    public String getErrorMessageKosNameField() {
        return appium.getText(errorMessageKosNameLabel);
    }

    /**
     * Click add kos button in Home
     */
    public void clickAddKosInHome() {
        appium.clickOn(addKosInHomeButton);
    }

    /**
     * Get instant booking benefit title
     * @return String page title
     */
    public String getTitleInstantBookingPage() {
        return appium.getText(titleInstantBookingBenefitLabel);
    }

    /**
     * Click button activate instant booking
     */
    public void clickActivateInstantBooking() {
        appium.clickOn(activationBookingKosButton);
    }

    /**
     * Get text in kos address field
     * @return String kos address
     */
    public String getKosAddress() {
        return appium.getText(errorMessageKosNameLabel);
    }

    /**
     * Get error text in daily price field
     * @return String error message
     */
    public String getErrorMessageDailyPrice() {
        return appium.getText(errorMessageDailyPriceField);
    }

    /**
     * Get error text in weekly price field
     * @return String error message
     */
    public String getErrorMessageWeeklyPrice() {
        return appium.getText(errorMessageWeeklyPriceField);
    }

    /**
     * Get error text in monthly price field
     * @return String error message
     */
    public String getErrorMessageMonthlyPrice() {
        return appium.getText(errorMessageMonthlyPriceField);
    }

    /**
     * Get error text in 3 monthly price field
     * @return String error message
     */
    public String getErrorMessage3MonthlyPrice() {
        return appium.getText(errorMessage3MonthlyPriceField);
    }

    /**
     * Get error text in 6 monthly price field
     * @return String error message
     */
    public String getErrorMessage6MonthlyPrice() {
        return appium.getText(errorMessage6MonthlyPriceField);
    }

    /**
     * Get error text in yearly price field
     * @return String error message
     */
    public String getErrorMessageYearlyPrice() {
        return appium.getText(errorMessageYearlyPriceField);
    }

    /**
     * Get error text in room total field
     * @return String error message
     */
    public String getErrorMessageRoom() {
        return appium.getText(errorMessageRoomField);
    }

    /**
     * Verify if add photo cover is appear
     * @return true if appear
     */
    public boolean isAddCoverPhotoAppear() {
        return appium.waitInCaseElementVisible(addBuildingPhotoButton, 3) != null;
    }

    /**
     * Scroll down until property policy
     */
    public void scrollDownUntilPropPolicy() {
        appium.scrollToElementByResourceId("propertyPolicyCheckBox");
    }

    /**
     * Verify if total available room field is disabled
     * @return true if disabled
     */
    public boolean isAvailableRoomDisabled() {
        return !appium.isElementEnabled(numberOfRoomAvailableField);
    }

    /**
     * Verify if total room field is disabled
     * @return true if disabled
     */
    public boolean isTotalRoomDisabled() {
        return !appium.isElementEnabled(numberOfRoomsField);
    }

    /**
     * Click checkbox room type
     */
    public void clickRoomTypeCheckbox() {
        if (Constants.MOBILE_OS == Platform.IOS) {
            if (appium.waitInCaseElementVisible(roomTypeCheckbox, 3) == null) {
                appium.scroll(0.5, 0.80, 0.20, 2000);
                appium.scroll(0.5, 0.80, 0.20, 2000);
                appium.scroll(0.5, 0.80, 0.20, 2000);
                appium.clickOn(kosAdminCheckbox);
            }
            else {
                appium.clickOn(roomTypeCheckbox);
            }
        }
        else {
            for (int i=0; i < 5; i++) {
                if (appium.waitInCaseElementVisible(roomTypeCheckbox, 3) == null) {
                    appium.scroll(0.5, 0.80, 0.20, 2000);
                }
                else {
                    break;
                }
            }
            appium.clickOn(roomTypeCheckbox);
        }
    }

    /**
     * Fill room type field
     * @param roomType is the room type name
     */
    public void fillRoomType(String roomType) {
        appium.enterText(roomTypeField, roomType, true);
        if (Constants.MOBILE_OS == Platform.IOS) {
            driver.findElementByName("Done").click();
        }
    }

    /**
     * Get error message in kos name
     * @return error message
     */
    public String getErrorKosName() {
        return appium.getText(errorKosNameField);
    }

    /**
     * Get error message in room type
     * @return error message
     */
    public String getErrorRoomType() {
        return appium.getText(errorRoomTypeField);
    }

    /**
     * Get error message in kos gender type
     * @return error message
     */
    public String getErrorKosType() {
        return appium.getText(errorKosTypeField);
    }

    /**
     * Get error message in kos description
     * @return error message
     */
    public String getErrorKosDesc() {
        return appium.getText(errorDescField);
    }

    /**
     * Click selected gender
     * @param gender is the gender selected
     */
    public void clickGender(String gender) {
        if (appium.waitInCaseElementVisible(femaleButton, 3) == null) {
            if (Constants.MOBILE_OS == Platform.ANDROID) {
                appium.scrollToElementByText("Putri");
            }
            appium.scroll(0.5, 0.70, 0.30, 2000);
        }
        if (gender.equals("female")) {
            appium.clickOn(femaleButton);
        }
        else if (gender.equals("male")) {
            appium.clickOn(maleButton);
        }
        else {
            appium.clickOn(mixedButton);
        }
    }

    /**
     * Get error message in manager name field
     * @return String error message
     */
    public String getErrorMessageManagerName() {
        return appium.getText(errorManagerNameField);
    }

    /**
     * Get error message in kos year build
     * @return String error message
     */
    public String getErrorKosYearBuild() {
        return appium.getText(errorKosYearBuildField);
    }

    /**
     * Get error message in manager phone field
     * @return String error message
     */
    public String getErrorMessageManagerPhone() {
        return appium.getText(errorManagerPhoneField);
    }

    /**
     * Click kos name field
     */
    public void clickKosNameField() {
        appium.clickOn(kosNameField);
        appium.clickOn(keyboardDoneButton);
    }

    /**
     * Click kos rule checkbox
     * @param rule is kos rule
     */
    public void clickKosRule(String rule) {
        WebElement element;
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            if (appium.waitInCaseElementVisible(By.xpath("//*[@text='"+ rule +"']"), 2) == null) {
                appium.scrollToElementByText(rule);
            }
            element = driver.findElementByXPath("//*[@text='"+ rule +"']/preceding-sibling::*");
        } else {
            if (appium.waitInCaseElementVisible(By.xpath("//*[@name='"+ rule +"']"), 2) == null) {
                appium.scroll(0.5, 0.70, 0.30, 2000);
            }
            element = driver.findElementByXPath("//*[@name='"+ rule +"']");
        }
        appium.clickOn(element);
    }

    /**
     * Select/set kos year built
     * @param year is kos year built
     */
    public void setKosYearBuilt(String year) {
        WebElement element;
        if (appium.waitInCaseElementVisible(kosYearBuildDropdown, 3) == null) {
            if (Constants.MOBILE_OS == Platform.ANDROID) {
                appium.scrollToElementByText("Tahun");
            }
            else {
                appium.scroll(0.5, 0.80, 0.20, 2000);
                appium.scroll(0.5, 0.70, 0.30, 2000);
            }
            appium.scroll(0.5, 0.70, 0.30, 2000);
        }
        appium.clickOn(kosYearBuildDropdown);
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            element = driver.findElementByXPath("//*[@text='"+ year +"']");
            appium.clickOn(element);
        }
        else {
            appium.enterText(yearPickerWheel, year, false);
        }
        appium.clickOn(chooseButton);
    }
}
