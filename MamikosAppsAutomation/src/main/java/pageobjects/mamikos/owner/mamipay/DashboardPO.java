package pageobjects.mamikos.owner.mamipay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class DashboardPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public DashboardPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeNavigationBar[@name=\"mamikos.OwnerDashboardView\"]/XCUIElementTypeImage[1]")
    @AndroidFindBy(id = "ownerProfileAvatar")
    private WebElement labelMamikos;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeNavigationBar[@name=\"mamikos.OwnerDashboardView\"]/XCUIElementTypeImage[2]")
    @AndroidFindBy(id = "notificationIconCV")
    private WebElement iconNotif;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Tambah Kos\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Tambah Iklan']/parent::*")
    private WebElement addPropertyButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Home\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/homeOwnerTextView')]")
    private WebElement iconHome;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Chat\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/chatOwnerTextView')]")
    private WebElement iconChat;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Tambah Iklan\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/ownerManageTextView')]")
    private WebElement iconManage;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Booking\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/ownerStatisticTextView')]")
    private WebElement iconStatistics;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Profile\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/profileOwnerTextView')]")
    private WebElement iconProfile;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"mamiHelpView\"]")
    @AndroidFindBy(xpath = "//android.widget.ImageButton[contains(@resource-id,'com.git.mami.kos:id/supportOwnerFloatingActionButton')]")
    private WebElement helpIcon;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Keuangan\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/titleFinanceText')]")
    private WebElement titleFinanceText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Belum ada pemasukan saat ini.\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Belum ada pemasukan saat ini.')]")
    private WebElement emptyFinanceText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Kelola Booking\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/titleManageBookingText')]")
    private WebElement titleManageBookingText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Lihat semua\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Lihat semua')]")
    private WebElement viewAllTextMB;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Belum ada permintaan sewa saat ini.\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/noReqBookingText')]")
    private WebElement emptyManageBookingText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Kelola Tagihan dan Booking\"]")
    @AndroidFindBy(xpath = "//android.widget.Button[contains(@resource-id,'com.git.mami.kos:id/btnManageBooking')]")
    private WebElement manageBookingButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Premium\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Premium')]")
    private WebElement titlePremiumText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Ingin kamar kos cepat terisi? Dapatkan fitur Premium untuk tingkatkan posisi iklan kos Anda.\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/textNonPremiumDescription')]")
    private WebElement nonPremiumDesc;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Lihat Paket\"]")
    @AndroidFindBy(xpath = "//android.widget.Button[contains(@resource-id,'com.git.mami.kos:id/activatePremium1')]")
    private WebElement activatePremiumButton;

    @iOSXCUITFindBy(accessibility = "managePremiumButton")
    private WebElement managePremiumButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Kamar\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/roomTextView')]")
    private WebElement titleRoomText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Update Kamar & Harga\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Atur Ketersediaan Kamar']/parent::*")
    private WebElement viewUpdateRoom;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Atur Harga']/parent::*")
    private WebElement viewUpdatePrice;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Daftar ke Booking Langsung']/parent::*")
    private WebElement registerInstantBookingButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Kosong\"]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[contains(@resource-id,'com.git.mami.kos:id/roomAvailableView')]")
    private WebElement roomAvailableView;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Update Semua Kos\"]")
    @AndroidFindBy(xpath = "//android.widget.Button[contains(@resource-id,'com.git.mami.kos:id/btnUpdateAllRooms')]")
    private WebElement updateAllRoomsButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Penyewa\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/residentKosTextView')]")
    private WebElement titleTenantText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeStaticText[@name=\"Penyewa\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/textTitleEmptyTenant')]")
    private WebElement emptyTenantText;

    @iOSXCUITFindBy(xpath="///XCUIElementTypeStaticText[@name=\"Silakan tambah penyewa terlebih dahulu.\"]")
    @AndroidFindBy(id = "textDescEmptyTenant")
    private WebElement emptyTenantDecsText;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"+ Tambah Penyewa\"]")
    @AndroidFindBy(xpath = "//android.widget.Button[contains(@resource-id,'com.git.mami.kos:id/addedTenantButton')]")
    private WebElement addTenantButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[7]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[contains(@resource-id,'com.git.mami.kos:id/posterCardView')]")
    private WebElement bannerPromo;

    @AndroidFindBy(id = "nonBookingCardButton")
    private WebElement activateNowBBKButton;

    //--------------- Premium Active ----------------
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name,'Paket')]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Paket')]")
    private WebElement premiumPackageTitleText;

    @iOSXCUITFindBy(accessibility = "expirationLabel")
    @AndroidFindBy(id = "textExpiredDate")
    private WebElement premiumExpiredDateText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Beli Saldo\"]")
    @AndroidFindBy(id = "balanceTextView")
    private WebElement premiumBuyBalanceTextlink;

    @iOSXCUITFindBy(accessibility = "balanceLabel")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Rp')]")
    private WebElement premiumBalanceText;

    @AndroidFindBy(id = "balancePerDaySwitchView")
    private WebElement activateDailyBalanceSwitcher;

    //------------ Finance Active widget -------------
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Penghasilan Total\"]")
    @AndroidFindBy(id = "totalText")
    private WebElement totalText;

    @iOSXCUITFindBy(accessibility = "totalIncomeAmountLabel")
    @AndroidFindBy(id = "valueTotalText")
    private WebElement valueTotalText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Pembayaran Sewa\"]")
    @AndroidFindBy(id = "rentPaymentText")
    private WebElement rentPaymentText;

    @iOSXCUITFindBy(accessibility = "monthlyIncomeLabel")
    @AndroidFindBy(id = "valuerentPaymentText")
    private WebElement valueRentPaymentText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"dateView\"]")
    @AndroidFindBy(id = "monthText")
    private WebElement optionMonthText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell[2]")
    @AndroidFindBy(xpath = "//android.widget.FrameLayout[contains(@index, '1')]")
    private WebElement previousMonth;

    //----------- Renter active list widget ------------
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/residentKosTextView')]")
    private WebElement titleTenantActive;

    @AndroidFindBy(xpath = "//android.widget.ImageView[contains(@resource-id,'com.git.mami.kos:id/userImageView')]")
    private WebElement tenantImage;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/nameTextView')]")
    private WebElement tenantName;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/roomTextView')]")
    private WebElement tenantRoomNumber;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/kostTextView')]")
    private WebElement tenantKostName;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/statusPaymentTextView')]")
    private WebElement tenantStatusPayment;


    /**
     * Check element label Mamikos is Displayed
     *
     * @return Status
     */
    public boolean isLabelMamikosDisplayed() {
        return labelMamikos.isDisplayed();
    }

    /**
     * Check element icon Notification is Displayed
     *
     * @return Status
     */
    public boolean isIconNotifDisplayed() {
        return iconNotif.isDisplayed();
    }

    /**
     * Check element Add Kost Button is Displayed
     *
     * @return Status
     */
    public boolean isAddKostButtonDisplayed() {
        return addPropertyButton.isDisplayed();
    }

    /**
     * Check element Icon Home is Displayed
     *
     * @return Status
     */
    public boolean isIconHomeDisplayed() {
        return iconHome.isDisplayed();
    }

    /**
     * Check element Icon Chat is Displayed
     *
     * @return Status
     */
    public boolean isIconChatDisplayed() {
        return iconChat.isDisplayed();
    }

    /**
     * Check element Icon Manage is Displayed
     *
     * @return Status
     */
    public boolean isIconManageDisplayed() {
        return iconManage.isDisplayed();
    }

    /**
     * Check element Icon Statistics is Displayed
     *
     * @return Status
     */
    public boolean isIconStatisticsDisplayed() {
        return iconStatistics.isDisplayed();
    }

    /**
     * Check element Icon Profile is Displayed
     *
     * @return Status
     */
    public boolean isIconProfileDisplayed() {
        return iconProfile.isDisplayed();
    }

    /**
     * Click element Notif Tagihan Tab
     */
    public void clickIconNotification() {
        appium.clickOn(iconNotif);
    }

    /**
     * Click element Tambah Kost button
     */
    public void clickAddKostButton() {
        appium.clickOn(addPropertyButton);
    }

    /**
     * Check element Title Finance is Displayed
     *
     * @return Status
     */
    public boolean isTitleFinanceDisplayed(){
        return titleFinanceText.isDisplayed();
    }

    /**
     * Check element Empty Finance Text is Displayed
     *
     * @return Status
     */
    public boolean isEmptyFinanceTextDisplayed() {
        return emptyFinanceText.isDisplayed();
    }

    /**
     * Check element Title Manage Booking Text is Displayed
     *
     * @return Status
     */
    public boolean isTitleManageBookingDisplayed() {
        return titleManageBookingText.isDisplayed();
    }

    /**
     * Check element View All Text Manage Booking Text is Displayed
     *
     * @return Status
     */
    public boolean isViewAllTextMBDisplayed() {
        return viewAllTextMB.isDisplayed();
    }

    /**
     * Check element Empty Manage Booking Text is Displayed
     *
     * @return Status
     */
    public boolean isEmptyManageBookingDisplayed() {
        return emptyManageBookingText.isDisplayed();
    }

    /**
     * Check element Manage Booking Button is Displayed
     *
     * @return Status
     */
    public boolean isManageBookingButtonDisplayed() {
        return manageBookingButton.isDisplayed();
    }

    /**
     * Check element Title Premium Text is Displayed
     *
     * @return Status
     */
    public boolean isTitlePremiumTextDisplayed() {
        return titlePremiumText.isDisplayed();
    }

    /**
     * Check element NonPremium Desc is Displayed
     *
     * @return Status
     */
    public boolean isNonPremiumDescDisplayed() {
        return nonPremiumDesc.isDisplayed();
    }

    /**
     * Check element Activate Premium Button is Displayed
     *
     * @return Status
     */
    public boolean isActivatePremiumButtonDisplayed() {
        return activatePremiumButton.isDisplayed();
    }

    /**
     * Check element Title Room Text is Displayed
     *
     * @return Status
     */
    public boolean isTitleRoomTextDisplayed() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Kamar");
        } else {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return titleRoomText.isDisplayed();
    }

    /**
     * Check element View Update Room Price is Displayed
     *
     * @return Status
     */
    public boolean isViewUpdateRoomPriceDisplayed() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Kamar");
        }
        return viewUpdateRoom.isDisplayed();
    }

    /**
     * Check element Room Available View is Displayed
     *
     * @return Status
     */
    public boolean isRoomAvailableViewDisplayed() {
        return roomAvailableView.isDisplayed();
    }

    /**
     * Check element Update All Rooms Button is Displayed
     *
     * @return Status
     */
    public boolean isUpdateAllRoomsButtonDisplayed() {
        return updateAllRoomsButton.isDisplayed();
    }

    /**
     * Check element Title Tenant Text is Displayed
     *
     * @return Status
     */
    public boolean isTitleTenantTextDisplayed() {
        return titleTenantText.isDisplayed();
    }

    /**
     * Check element Empty Tenant Text is Displayed
     *
     * @return Status
     */
    public boolean isEmptyTenantTextDisplayed() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("+ Tambah Penyewa");
        } else {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return emptyTenantText.isDisplayed();
    }

    /**
     * Check element Empty Tenant Decs Text is Displayed
     *
     * @return Status
     */
    public boolean isEmptyTenantDecsTextDisplayed() {
        return emptyTenantDecsText.isDisplayed();
    }

    /**
     * Check element Room Available View is Displayed
     *
     * @return Status
     */
    public boolean isAddTenantButtonDisplayed() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("+ Tambah Penyewa");
        } else {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return appium.waitInCaseElementVisible(addTenantButton, 3) != null;
    }

    /**
     * Check element Banner Promo is Displayed
     *
     * @return Status
     */
    public boolean isBannerPromoDisplayed(){
        return appium.waitInCaseElementVisible(bannerPromo, 1) != null;
    }

    /**
     * Check element Help Icon is Displayed
     *
     * @return Status
     */
    public boolean isHelpIconDisplayed() {
        return helpIcon.isDisplayed();
    }


    //------ PREMIUM WIDGET -------
    /**
     * Check element Premium package title is Displayed
     *
     * @return Status
     */
    public boolean isPremiumPackageTitleDisplayed() {
        return premiumPackageTitleText.isDisplayed();
    }

    /**
     * Check element Premium expired date is Displayed
     *
     * @return Status
     */
    public boolean isPremiumExpiredDateDisplayed() {
        return premiumExpiredDateText.isDisplayed();
    }

    /**
     * Check element 'Beli Saldo' textlink is Displayed
     *
     * @return Status
     */
    public boolean isPremiumBuyBalanceTextlinkDisplayed() {
        return premiumBuyBalanceTextlink.isDisplayed();
    }

    /**
     * Check element Balance is Displayed
     *
     * @return Status
     */
    public boolean isPremiumBalanceDisplayed() {
        return premiumBalanceText.isDisplayed();
    }

    /**
     * Check element Setting Premium Button is Displayed
     *
     * @return Status
     */
    public boolean isSettingPremiumButtonDisplayed() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return activatePremiumButton.isDisplayed();
        } else {
            return managePremiumButton.isDisplayed();
        }
    }

    /**
     * Click manage premium button
     */
    public void clickManagePremiumButton() {
        appium.waitInCaseElementVisible(activatePremiumButton, 5);
        appium.clickOn(activatePremiumButton);
    }

    // --------- Finance Active widget ----------
    /**
     * Check element Total Text is Displayed
     *
     * @return Status
     */
    public boolean isTotalTextDisplayed() {
        return totalText.isDisplayed();
    }

    /**
     * Check element Value Total Text is Displayed
     *
     * @return Status
     */
    public boolean isValueTotalTextDisplayed() {
        return appium.waitInCaseElementVisible(valueTotalText, 3) != null;
    }

    /**
     * Check element Rent Payment Text is Displayed
     *
     * @return Status
     */
    public boolean isRentPaymentTextDisplayed() {
        return rentPaymentText.isDisplayed();
    }

    /**
     * Check element Value Rent Payment Text is Displayed
     *
     * @return Status
     */
    public boolean isValueRentPaymentTextDisplayed() {
        return valueRentPaymentText.isDisplayed();
    }

    /**
     * Check element Option Month Text is Displayed
     * Tap and choose previous month
     * Check finance balance in previous month is Displayed
     * @return Status
     */
    public boolean selectPreviousMonthAndCheckTheFinanceBalance() {
        appium.tapOrClick(optionMonthText);
        appium.tapOrClick(previousMonth);
        return isValueRentPaymentTextDisplayed();
    }

    /**
     * Scroll to element '+ Tambah Penyewa'
     * Check element Title Widget Text is Displayed
     * @return Status
     */
    public boolean isTitleRenterDisplayed() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("+ Tambah Penyewa");
        } else {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return titleTenantActive.isDisplayed();
    }

    /**
     * Check element Tenant image is Displayed
     * @return Status
     */
    public boolean isTenantImageDisplayed() {
        return tenantImage.isDisplayed();
    }

    /**
     * Check element Tenant name Text is Displayed
     * @return Status
     */
    public boolean isTenantNameDisplayed() {
        return tenantName.isDisplayed();
    }

    /**
     * Check element Tenant room number Text is Displayed
     * @return Status
     */
    public boolean isTenantRoomNumberDisplayed() {
        return tenantRoomNumber.isDisplayed();
    }

    /**
     * Check element Tenant kost name Text is Displayed
     * @return Status
     */
    public boolean isTenantKostNameDisplayed() {
        return tenantKostName.isDisplayed();
    }

    /**
     * Check element Tenant status payment Text is Displayed
     * @return Status
     */
    public boolean isTenantStatusPaymentDisplayed() {
        return tenantStatusPayment.isDisplayed();
    }

    /**
     * Click activate daily balance premium switcher
     */
    public void clickActivateDailyBalance() {
        appium.waitInCaseElementVisible(activateDailyBalanceSwitcher, 5);
        appium.clickOn(activateDailyBalanceSwitcher);
    }

    /**
     * Scroll down to update room then Click it
     */
    public void clickUpdateRoom() {
        appium.scrollToElementByResourceId("titleManageKosSection");
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.clickOn(viewUpdateRoom);
    }

    /**
     * Scroll down to update price then Click it
     */
    public void clickUpdatePrice() {
        appium.scrollToElementByResourceId("titleManageKosSection");
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.clickOn(viewUpdatePrice);
    }

    /**
     * Tap on activate BBK now button if appear
     */
    public void clickOnActivateNowBBK() {
        if (appium.waitInCaseElementVisible(activateNowBBKButton, 5) != null) {
            appium.clickOn(activateNowBBKButton);
        }
    }

    /**
     * Scroll down to register instant booking then Click it
     */
    public void clickRegisterInstantBooking() {
        appium.scrollToElementByResourceId("titleManageKosSection");
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.clickOn(registerInstantBookingButton);
    }
}