package pageobjects.mamikos.owner.mamipay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.PageFactory;
import pageobjects.mamikos.common.FooterPO;
import utilities.AppiumHelpers;
import utilities.Constants;

public class FiturPromisiPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;
    FooterPO footer;

    public FiturPromisiPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        footer = new FooterPO(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    private By mamiAdsPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.id("titleCollapsingToolbarTextView"): By.id("MamiAds");

    private By pesanBroadcastPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//*[@text='Broadcast Chat'])[1]"): By.xpath("(//*[@value='Broadcast Chat'])[1]");

    private By promoIklanPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Promo Juli']"): By.id("Promo Iklan");

    private By propertiSekitarPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Cek Properti Sekitar']"): By.id("Cek Properti Sekitar");

    private By proPhotoPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.id("titleTextView"): By.xpath("//*[@value='Pilih Paket Pro Photo']");

    private By cobaSekarangButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Coba Sekarang']"): By.id("Coba Sekarang");


    /** verify menu on fitur promosi
     * @param menuFiturPromosi is menu fitur promosi
     */
    public boolean menuFiturPromosiPresent(String menuFiturPromosi) {
        By el = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='" + menuFiturPromosi + "']"): By.xpath("//*[@value='" + menuFiturPromosi + "']");
        return appium.waitInCaseElementVisible(el,3) != null;
    }

    /**
     * @return return true if mamiAds page present. Otherwise false
     */
    public boolean mamiAdsPageIsPresent() throws InterruptedException {
        return appium.waitInCaseElementVisible(mamiAdsPageTitle, 10) != null;
    }

    /**
     * @return return true if pesan Broadcast page present. Otherwise false
     */
    public boolean pesanBroadcastPageIsPresent() throws InterruptedException {
        return appium.waitInCaseElementVisible(pesanBroadcastPageTitle, 3) != null;
    }

    /**
     * @return return true if promo Iklan page present. Otherwise false
     */
    public boolean promoIklanPageIsPresent() throws InterruptedException {
        return appium.waitInCaseElementVisible(promoIklanPageTitle, 3) != null;
    }

    /**
     * @return return true if cek properti Sekitar page present. Otherwise false
     */
    public boolean propertiSekitarPageIsPresent(){
        appium.scrollToElementByText("Cek Properti Sekitar");
        return appium.waitInCaseElementVisible(propertiSekitarPageTitle, 10) != null;
    }

    /**
     * @return return true if pro Photo page present. Otherwise false
     */
    public boolean proPhotoPageIsPresent() throws InterruptedException {
        return appium.waitInCaseElementVisible(proPhotoPageTitle, 3) != null;
    }

    /**
     * Click on Coba Sekarang button after click menu Mamiads
     */
    public void clickOnCobaSekarangButton() throws InterruptedException {
        if (appium.waitInCaseElementVisible(cobaSekarangButton, 5) != null) {
            appium.hardWait(2);
            appium.clickOn(cobaSekarangButton);
        }
    }

    /**
     * Click Fitur Promosi Menu
     */
    public void clickOnFiturPromosiTab(String tabFiturPromosi) {
        By fiturPromosiTab = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='" + tabFiturPromosi + "']"): By.xpath("//*[@value='" + tabFiturPromosi + "']");
        appium.clickOn(fiturPromosiTab);
    }
}

