package pageobjects.mamikos.owner.mamipay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class KostDetailsPO {
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

	public KostDetailsPO(AppiumDriver<WebElement> driver) {
		this.driver = driver;
		appium = new AppiumHelpers(driver);

		//This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	/*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

	@AndroidFindBy(id = "com.git.mami.kos:id/invoiceTextView")
	private WebElement manageBillKostText;

	@AndroidFindBy(id = "com.git.mami.kos:id/namePropertyTextView")
	private WebElement kostName;

	@AndroidFindBy(id = "com.git.mami.kos:id/viewMamiPay")
	private WebElement bookingRequest;

	@AndroidFindBy(id = "com.git.mami.kos:id/viewUpdatePrice")
	private WebElement updateRoomAndPrice;

	By unpaidBillAmmount = By.xpath("//android.widget.TextView[contains(text(), 'tagihan belum bayar')]");

	By manageBillKostTextBy = By.id("com.git.mami.kos:id/invoiceTextView");

	/**
	 * @return true if manage bill kost text is present. Otherwise false.
	 * @throws InterruptedException
	 */
	public Boolean isManageBillKostTextIsPresent() throws InterruptedException {
		return appium.isElementPresent(manageBillKostTextBy);
	}

	/**
	 * Click or tap on manage bill kost button
	 */
	public void clickOrTapManageBillKostButton(){
		appium.tapOrClick(manageBillKostText);
	}
}