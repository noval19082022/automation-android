package pageobjects.mamikos.owner.mamipay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pageobjects.mamikos.common.FooterPO;
import utilities.AppiumHelpers;
import utilities.Constants;

public class ManageBillAndBookingPO {
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;
	FooterPO footer;

	public ManageBillAndBookingPO(AppiumDriver<WebElement> driver) {
		this.driver = driver;
		appium = new AppiumHelpers(driver);
		footer = new FooterPO(driver);
		//This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	/*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

	@AndroidFindBy(id="com.git.mami.kos:id/actionCancelButton")
	private WebElement nantiSajaButton;

	@AndroidFindBy(xpath = "//*[@content-desc='Navigate up']")
	private WebElement buttonArrowBack;

	@AndroidFindBy(xpath ="//*[@text='Tutup']")
	private WebElement tutupButton;

	@AndroidFindBy(xpath = "//*[@text='BARU']")
	private WebElement baruTextTooltip;

	@AndroidFindBy(id = "btnManageBooking")
	private WebElement btnManageBillAndBooking;

	private By btnManageBillAndBookingBy = By.id("btnManageBooking");
	private By btnManageBillAndBookingByIOS = By.xpath("//XCUIElementTypeButton[@name=\"Kelola Tagihan dan Booking\"]");

	//Empty Data
	@AndroidFindBy(xpath="//*[@resource-id='com.git.mami.kos:id/emptyDataView']/android.widget.ImageView")
	private WebElement imageSkeletonList;

	@AndroidFindBy(id = "emptyKostTextView")
	private WebElement belumAdaKostText;

	private By belumAdaKostTextBy = By.id("emptyKostTextView");

	private By belumAdaKostTextByiOS = By.xpath("//XCUIElementTypeStaticText[@name=\"Anda belum mendaftarkan Kos\"]");
	private By belumAdaKostTextByiOSLineTwo = By.xpath("//XCUIElementTypeStaticText[@name=\"Silakan tambahkan kos Anda terlebih dahulu.\"]");

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Tambah Kos\"]")
	@AndroidFindBy(id="addPropertyButton")
	private WebElement addNewKostButton;

	//Kost data available
	@AndroidFindBy(id = "com.git.mami.kos:id/totalWaitingTextView")
	private WebElement tenantWaitingForApproval;

	@AndroidFindBy(id = "propetyRecyclerView")
	private WebElement allKostList;

	private By tenantWaitingForApprovalXpath = By.xpath("//android.widget.FrameLayout[%s]/android.view.ViewGroup/*[@resource-id='com.git.mami.kos:id/totalWaitingTextView']");

	private By allKostListBy = By.id("propetyRecyclerView");

	private By boxKostList = By.xpath("//androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[%s]/android.view.ViewGroup");

	private By kostImageThumbnail = By.xpath("//android.widget.FrameLayout[%s]/android.view.ViewGroup/android.widget.ImageView[@resource-id='com.git.mami.kos:id/propertyImageView']");

	private By kostName = By.xpath("//android.widget.FrameLayout[%s]/android.view.ViewGroup/android.widget.TextView[@resource-id='com.git.mami.kos:id/propertyNameTextView']");

	private By kostLocation = By.xpath("//android.widget.FrameLayout[%s]/android.view.ViewGroup/android.widget.TextView[@resource-id='com.git.mami.kos:id/propertyNameTextView']");

	private By availableRoom = By.xpath("//android.widget.FrameLayout[%s]/android.view.ViewGroup/android.widget.TextView[@resource-id='com.git.mami.kos:id/roomAvailableTextView']");

	private By lastUpdateDate = By.xpath("//android.widget.FrameLayout[%s]/android.view.ViewGroup/android.widget.TextView[@resource-id='com.git.mami.kos:id/statusUpdateTextView']");

	private By addNewKostButtonBy = By.xpath("//android.widget.LinearLayout[@resource-id='com.git.mami.kos:id/emptyDataView']/android.widget.Button[@text='Tambah iklan kost']");

	private By addNewKostButtonByiOS = By.xpath("//XCUIElementTypeButton[@name=\"Tambah Kos\"]");

	private By pengajuanBookingPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.id("titleBottomTextView"): By.id("Pengajuan Booking");

	private By laporanKeuanganPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.id("titleTextView"): By.xpath("//*[@value='Laporan Keuangan']");

	private By kelolaTagihanPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Kelola Tagihan']"): By.id("Kelola Tagihan");

	private By penyewaPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.id("centerTitle"): By.xpath("//*[@value='Penyewa']");

	/**
	 * @param targeSelect use variable By kostName, kostLocation, availabelRoom, lastUpdateDate.
	 * @param listNumber list number 1 and soo on.
	 * @return return WebElement after reformat %s.
	 */
	public WebElement kostListAttributeElement(By targeSelect, int listNumber) {
		String targetFormatXpath = targeSelect.toString().replace("By.xpath: ", "");
		targetFormatXpath = String.format(targetFormatXpath, String.valueOf(listNumber));
		return driver.findElement(By.xpath(targetFormatXpath));
	}

	/**
	 * @param targetSelect use variable By kostName, kostLocation, availabelRoom, lastUpdateDate.
	 * @param listNumber list number 1 and soo on
	 * @return By after reformat %s.
	 */
	public By kostListAttributeBy(By targetSelect, int listNumber) {
		String targetFormatXpath = targetSelect.toString().replace("By.xpath: ", "");
		targetFormatXpath = String.format(targetFormatXpath, String.valueOf(listNumber));
		return By.xpath(targetFormatXpath);
	}

	/*
	 *Kost list available method
	 */
	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return true if kost list contain image thumbnail. Otherwise false
	 */
	public boolean kostImageThumbnailIsPresent(int listNumber) throws InterruptedException {
		By image = kostListAttributeBy(kostImageThumbnail, listNumber);
		return appium.isElementPresent(image);
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return true if kost list contain kost name. Otherwise false.
	 * @throws InterruptedException
	 */
	public boolean kostNameIsPresent(int listNumber) throws InterruptedException{
		By name = kostListAttributeBy(kostName, listNumber);
		return appium.isElementPresent(name);
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return true if kost list contain available room status. Otherwise false.
	 * @throws InterruptedException
	 */
	public boolean kostAvailableRoomStatusIsPresent(int listNumber) throws InterruptedException {
		By room = kostListAttributeBy(availableRoom, listNumber);
		return appium.isElementPresent(room);
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return true if kost list contain kost location. Otherwis false.
	 * @throws InterruptedException
	 */
	public boolean kostLocationIsPresent(int listNumber) throws InterruptedException{
		By location = kostListAttributeBy(kostLocation, listNumber);
		return appium.isElementPresent(location);
	}

	public boolean lastUpdateDateIsPresent(int listNumber) throws InterruptedException {
		By date = kostListAttributeBy(lastUpdateDate, listNumber);
		return appium.isElementPresent(date);
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return true if target
	 */
	public boolean boxKostListPresent(int listNumber) throws InterruptedException {
		By boxList = kostListAttributeBy(boxKostList, listNumber);
		return appium.isElementPresent(boxList);
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return kost name with type data of String
	 */
	public String getKostNameText(int listNumber) {
		return appium.getText(kostListAttributeElement(kostName, listNumber));
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return kost location with type data of String
	 */
	public String getKostLocationText(int listNumber) {
		return appium.getText(kostListAttributeElement(kostLocation, listNumber));
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return kost available room with type data of String
	 */
	public String getAvailableRoomText(int listNumber) {
		return appium.getText(kostListAttributeElement(availableRoom, listNumber));
	}

	/**
	 * @param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return kost last update date with type
	 */
	public String getLastUpdateDateText(int listNumber) {
		return appium.getText(kostListAttributeElement(lastUpdateDate, listNumber));
	}

	/**
	 * @param lisNumber input with integer. Based on kost list position. The top position equal to 1.
	 * Click or tap on box kost list.
	 */
	public void tapOrClickBillKostList(int lisNumber) {
		appium.tapOrClick(kostListAttributeElement(boxKostList, lisNumber));
	}

	/*
	 * Empty List part method
	 */
	/**
	 * @return true if add new kost button Is present
	 */
	public boolean addNewKostButtonIsPresent(){
		return (Constants.MOBILE_OS==Platform.ANDROID)
				? appium.waitInCaseElementVisible(addNewKostButton, 10) != null
				: appium.waitInCaseElementVisible(addNewKostButtonByiOS, 10) != null;
	}

	/**
	 * @return true if element is present. Otherwise false
	 */
	public boolean addNewKostButtonIsDisplayed() throws InterruptedException{
		return appium.isElementPresent(addNewKostButtonBy);
	}

	/**
	 * @return true if element is present. Otherwise false
	 */
	public boolean belumAdaKostTextIsPresent() throws InterruptedException{
		return (Constants.MOBILE_OS==Platform.ANDROID)
				? (appium.isElementPresent(belumAdaKostTextBy))
				: (appium.isElementPresent(belumAdaKostTextByiOS));
	}

	/**
	 * @return belum ada kost text
	 */
	public String getBelumAdaKostText() {
		return (Constants.MOBILE_OS==Platform.ANDROID)
				? (appium.getText(belumAdaKostText)).toLowerCase()
				: (appium.getText(belumAdaKostTextByiOS)+" " + "\n "+appium.getText(belumAdaKostTextByiOSLineTwo)).toLowerCase();
	}

	/**
	 * @return text on tambahkan iklan button
	 */
	public String getTambahIklanKostText() {
		return appium.getText(addNewKostButton);
	}

	/*
	 *Kost List available method
	 */
	/**
	 * @return return true if kost list present. Otherwise false
	 */
	public boolean allKostListIsPresent() throws InterruptedException {
		return appium.isElementPresent(allKostListBy);
	}

	/**
	 * Tap or click on tooltip by finding it text "BARU"
	 * @throws InterruptedException
	 */
	public void tapOrClickTooltip() throws InterruptedException {
		appium.tapOrClick(baruTextTooltip);
	}

	/**
	 * @@param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 */
	public void tapOrClickOnKostList(int listNumber) {
		WebElement kostList = kostListAttributeElement(boxKostList, listNumber);
		appium.tapOrClick(kostList);
	}

	/**
	 * @@param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return By of tenant waiting for approval based on kost list inputted.
	 */
	public By getByTenantWaitingForApproval(int listNumber) {
		return  kostListAttributeBy(tenantWaitingForApprovalXpath, listNumber);
	}

	/**
	 * @@param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return WebElement of tenant waiting for approval based on kost list inputted.
	 */
	public WebElement getElementTenantWaitingForApproval(int listNumber) {
		return kostListAttributeElement(tenantWaitingForApprovalXpath, listNumber);
	}

	/**
	 * @@param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 * @return true is coresponding kost list have tenant waiting for approval. Otherwise false.
	 * @throws InterruptedException
	 */
	public boolean tenantWaitForApprovalIsPresent(int listNumber) throws InterruptedException {
		return appium.isElementPresent(getByTenantWaitingForApproval(listNumber));
	}

	/**
	 * @return true if manage bill and booking is present, otherwise false.
	 * @throws InterruptedException
	 */
	public boolean btnManageBillAndBookingIsPresent() throws InterruptedException{
		return (Constants.MOBILE_OS==Platform.ANDROID)
				? appium.isElementPresent(btnManageBillAndBookingBy)
				: appium.isElementPresent(btnManageBillAndBookingByIOS);
	}

	/**
	 * @@param listNumber input with integer. Based on kost list position. The top position equal to 1.
	 */
	public void tapOrClickOnTenantWaitingApprovalText(int listNumber) throws InterruptedException {
		if(tenantWaitForApprovalIsPresent(listNumber)) {
			appium.tapOrClick(getElementTenantWaitingForApproval(listNumber));
		}
	}

	/**
	 * Click or tap on manage bill and booking button
	 */
	public void tapOrClickManageBillAndBooking() {
		appium.tapOrClick(btnManageBillAndBooking);
	}

	/**
	 * Click or tap on Nanti Saja button
	 */
	public void tapOrClickNantiSajaButton() {
		appium.clickOn(nantiSajaButton);
	}

	/**
	 * @return return true if pengajuan booking page present. Otherwise false
	 */
	public boolean pengajuanBookingPageIsPresent() throws InterruptedException {
		return appium.isElementPresent(pengajuanBookingPageTitle);
	}

	/**
	 * @return return true if laporan keuangan page present. Otherwise false
	 */
	public boolean  laporanKeuanganPageIsPresent() throws InterruptedException {
		return appium.isElementPresent(laporanKeuanganPageTitle);
	}

	/**
	 * @return return true if kelola tagihan page present. Otherwise false
	 */
	public boolean kelolaTagihanPageIsPresent() throws InterruptedException {
		return appium.isElementPresent(kelolaTagihanPageTitle);
	}

	/**
	 * @return return true if penyewa page present. Otherwise false
	 */
	public boolean penyewaPageIsPresent() throws InterruptedException {
		return appium.isElementPresent(penyewaPageTitle);
	}
}