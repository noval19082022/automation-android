package pageobjects.mamikos.owner.mamipay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;


public class RegisterFormPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RegisterFormPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "skipButton")
    private WebElement skipIntro;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/nameInputCV']//*[@resource-id='com.git.mami.kos:id/inputEditText']")
    private WebElement fullNameField;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/bankAccountNumberCV']//*[@resource-id='com.git.mami.kos:id/inputEditText']")
    private WebElement bankAccountField;

    @AndroidFindBy(id = "nextButton")
    private WebElement nextButtonmamipay;

    @AndroidFindBy(id = "mainTextInputAutoComplete")
    private WebElement dropDownBankNamemamipay;

    @AndroidFindBy(id = "labelErrorTextView")
    private WebElement warningTextBankName;

    @AndroidFindBy(id = "termConditionBookingActivateCheckBox")
    private WebElement termConditionCheckbox;


    /**
     * Insert text to full name field
     * @param fullname is full name
     */
    public void insertTextFullName(String fullname) {
        appium.clickOn(fullNameField);
        appium.enterText(fullNameField, fullname, true);
    }

    /**
     * Click bank account number
     */
    public void clickBankAccountField() {
        appium.clickOn(bankAccountField);
    }

    /**
     * Clicks on Next Button form mamipay
     */
    public void clickNextbuttonform() {
        appium.clickOn(nextButtonmamipay);
    }

    /**
     * Clicks on dropdown bank name
     */
    public void clickdropDownBankNameamipay() {
        appium.clickOn(dropDownBankNamemamipay);
    }

    /**
     * Fill Bank Name
     */
    public void fillBankName(String bankName) {
        appium.enterText(dropDownBankNamemamipay, bankName, true);
    }

    /**
     * Get Text Error from bank name
     */
    public String textErrorBankName() {
        appium.waitTillElementIsVisible(warningTextBankName);
        return warningTextBankName.getText();
    }

    /**
     * Insert text to bank number field
     * @param number is bank account number
     */
    public void insertTextBankNumber(String number) {
        appium.clickOn(bankAccountField);
        appium.enterText(bankAccountField, number, true);
    }

    /**
     * Click full name field
     */
    public void clickFullNameField() {
        appium.clickOn(fullNameField);
    }

    /**
     * Get text in bank account number field
     * @return text from bank number field
     */
    public String getBankAccNo() {
        return appium.getText(bankAccountField);
    }

    /**
     * Click term and condition agreement checkbox
     */
    public void clickTermsCondition() {
        appium.clickOn(termConditionCheckbox);
    }
}