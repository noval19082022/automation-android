package pageobjects.mamikos.owner.mamipay;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class UpdateKostPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public UpdateKostPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "currentPriceView")
    private WebElement formEditRoomAvailable;

    @AndroidFindBy(id = "tv_zero_update")
    private WebElement emptyRoom;

    @AndroidFindBy(id = "totalRoomView")
    private WebElement totalRoomDropdown;

    @AndroidFindBy(id = "okButton")
    private WebElement chooseButton;

    @AndroidFindBy(xpath = "//androidx.recyclerview.widget.RecyclerView/*[1]//*[@resource-id='com.git.mami.kos:id/roomNameEditText']")
    private WebElement firstRoomNameField;

    @AndroidFindBy(id = "com.git.mami.kos:id/labelErrorView")
    private WebElement errorMessageRoomNameLabel;

    @AndroidFindBy(id = "com.git.mami.kos:id/labelFloorErrorView")
    private WebElement errorMessageFloorLabel;

    @AndroidFindBy(id = "com.git.mami.kos:id/keyboardDoneButton")
    private WebElement keyboardDoneButton;

    @AndroidFindBy(xpath = "//androidx.recyclerview.widget.RecyclerView/*[1]//*[@resource-id='com.git.mami.kos:id/floorEditText']")
    private WebElement firstFloorField;

    @AndroidFindBy(id = "descriptionTextView")
    private WebElement addKosSuccessDescLabel;

    @AndroidFindBy(id = "searchEditText")
    private WebElement searchRoomNoBar;

    @AndroidFindBy(id = "frameDisableCheckBox")
    private WebElement monthlyCheckbox;

    @AndroidFindBy(id = "anotherPriceTextView")
    private WebElement seeMorePricesButton;

    @AndroidFindBy(id = "inputPriceEditText")
    private WebElement monthlyPriceField;

    @AndroidFindBy(id = "textErrorView")
    private WebElement errorMessagePriceLabel;

    @AndroidFindBy(id = "updatePriceButton")
    private WebElement updatePriceButton;

    @AndroidFindBy(id = "optionMoreImageView")
    private WebElement threeDotsButton;

    @AndroidFindBy(id = "addRoomView")
    private WebElement addRoomButton;

    @AndroidFindBy(id = "deleteRoomView")
    private WebElement deleteRoomButton;

    @AndroidFindBy(id = "positifButton")
    private WebElement confirmDeleteButton;


    /**
     * @return true if Edit Room Available form Is present
     */
    public boolean isFormEditRoomAvailablePresent(){
        return appium.waitInCaseElementVisible(formEditRoomAvailable, 10) != null;
    }


    /**
     * @return true if element is displayed
     */
    public boolean isFormEditRoomAvailableDisplayed() {
        return formEditRoomAvailable.isDisplayed();
    }


    /**
     * Get the text "Tidak ada Property Terverifikasi ..."
     * @return text 
     */
    public String isEmptyRoomInfoDisplayed() {
        String emptyText = appium.getText(emptyRoom);
        return emptyText;
    }

    /**
     * Click kos name with specified text
     * @param kosName is kos name
     */
    public void clickKosName(String kosName) {
        String xpathLocator = "//android.widget.TextView[@text='" + kosName + "']/ancestor::*[1]";
        WebElement kos = driver.findElement(By.xpath(xpathLocator));
        appium.clickOn(kos);
    }

    /**
     * Click room total number with specified text
     * @param number is room total number
     */
    public void clickRoomTotalNumber(String number) {
        String xpathLocator = "//*[@text='" + number + "']";
        WebElement roomTotal = driver.findElement(By.xpath(xpathLocator));
        appium.clickOn(roomTotal);
    }

    /**
     * Click choose button in room total pop up
     */
    public void clickChoose() {
        appium.clickOn(chooseButton);
    }

    /**
     * Insert text to first room name/number field then hit done
     * @param roomNameNumber is text for room name or number
     */
    public void inputRoomNameNumber(String roomNameNumber) {
        appium.enterText(firstRoomNameField, roomNameNumber, true);
    }

    /**
     * Get error message in room name/number field
     * @return String error message
     */
    public String getErrorMessageRoomNameField() {
        return appium.getText(errorMessageRoomNameLabel);
    }

    /**
     * Get error message in floor field
     * @return String error message
     */
    public String getErrorMessageFloorField() {
        return appium.getText(errorMessageFloorLabel);
    }

    /**
     * Insert text to first floor (optional) field then hit done
     * @param floor is text for room name or number
     */
    public void inputFloor(String floor) {
        appium.enterText(firstFloorField, floor, true);
    }

    /**
     * Get description text in add kos success page
     * @return String description
     */
    public String getAddKosPopUpDesc() {
        return appium.getText(addKosSuccessDescLabel).replace("\n", " ").trim();
    }

    /**
     * Input room number/name in search bar
     * @param roomNo is room number/name
     */
    public void inputRoomNo(String roomNo) {
        appium.enterText(searchRoomNoBar, roomNo, true);
    }

    /**
     * Click total room dropdown
     */
    public void clickTotalRoomDropdown() {
        if (appium.waitInCaseElementVisible(totalRoomDropdown, 3) == null ) {
            appium.scroll(0.5, 0.3, 0.60, 2000);
        };
        appium.clickOn(totalRoomDropdown);
    }

    /**
     * Get text in first room name/number field
     * @return String room name / number
     */
    public String getFirstRoomNameNumber() {
        return appium.getText(firstRoomNameField);
    }

    /**
     * Get text in first floor field
     * @return String floor name
     */
    public String getFirstFloor() {
        return appium.getText(firstFloorField);
    }

    /**
     * Click monthly price checkbox
     */
    public void clickMonthlyPriceCheckbox() {
        appium.clickOn(monthlyCheckbox);
    }

    /**
     * Click see other prices
     */
    public void clickSeeOtherPrices() {
        appium.clickOn(seeMorePricesButton);
    }

    /**
     * Input text in monthly price field
     * @param price is price to enter
     */
    public void inputMonthlyPrice(String price) {
        appium.enterText(monthlyPriceField, price, true);
    }

    /**
     * Get text in error message below price field
     * @return String error message
     */
    public String getErrorMessagePrice() {
        if (!this.isErrorMessageAppear()) {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return appium.getText(errorMessagePriceLabel);
    }

    /**
     * Get element in input price based on time
     * @param time is the time (daily/weekly/monthly, and so on)
     * @return String element
     */
    private String getElementPrice(String time) {
        String element;
        switch (time) {
            case "daily":
                element = "com.git.mami.kos:id/inputPriceKostDaily";
                break;
            case "weekly":
                element = "com.git.mami.kos:id/inputPriceKostWeek";
                break;
            case "monthly":
                element = "com.git.mami.kos:id/inputPriceKostMonth";
                break;
            case "3 monthly":
                element = "com.git.mami.kos:id/inputPriceKost3Month";
                break;
            case "6 monthly":
                element = "com.git.mami.kos:id/inputPriceKost6Month";
                break;
            case "yearly":
                element = "com.git.mami.kos:id/inputPriceKostYear";
                break;
            default:
                throw new IllegalArgumentException("Invalid entry");
        }
        return "//*[@resource-id='" + element + "']";
    }

    /**
     * Click Input price checkbox to activate it
     * @param time is the time (daily/weekly/monthly, and so on)
     */
    public void clickInputPriceCheckbox(String time) {
        String element = this.getElementPrice(time) + "//*[@resource-id='com.git.mami.kos:id/editableCheckBox']";
        WebElement target = driver.findElement(By.xpath(element));
        if (appium.waitInCaseElementVisible(target, 3) == null) {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        appium.clickOn(target);
    }

    /**
     * Input price in price field
     * @param time is the time (daily/weekly/monthly, and so on)
     * @param price is the price that we want to enter
     */
    public void inputPrice(String time, String price) {
        String element = this.getElementPrice(time) + "//*[@resource-id='com.git.mami.kos:id/inputPriceEditText']";
        WebElement target;
        try
        {
            target = driver.findElement(By.xpath(element));
        }
        catch (Exception e)
        {
            appium.scroll(0.5, 0.80, 0.20, 2000);
            target = driver.findElement(By.xpath(element));
        }
        appium.enterText(target, price, true);
    }

    /**
     * Click update price button
     */
    public void clickUpdatePriceButton() {
        int i = 0;
        while (appium.waitInCaseElementVisible(updatePriceButton, 2) == null && i < 7) {
            appium.scroll(0.5, 0.80, 0.20, 2000);
            i++;
        }
        appium.clickOn(updatePriceButton);
    }

    /**
     * Verify is Error message appear
     * @return true if appear
     */
    public boolean isErrorMessageAppear() {
        return appium.waitInCaseElementVisible(errorMessagePriceLabel, 3) != null;
    }

    /**
     * Click three dots button
     */
    public void click3DotsButton() {
        appium.clickOn(threeDotsButton);
    }

    /**
     * Click add room button
     */
    public void clickAddRoomButton() {
        appium.waitInCaseElementVisible(addRoomButton, 3);
        appium.clickOn(addRoomButton);
    }

    /**
     * Click delete room button
     */
    public void clickDeleteRoomButton() {
        appium.waitInCaseElementVisible(deleteRoomButton, 3);
        appium.clickOn(deleteRoomButton);
        if (appium.waitInCaseElementVisible(confirmDeleteButton, 3) != null) {
            appium.clickOn(confirmDeleteButton);
        }
    }

    /**
     * Set nth element for room field
     * @param number is nth element
     * @return String for element
     */
    private String setNthRoomElement(String number) {
        return "//androidx.recyclerview.widget.RecyclerView/*["+ number +"]";
    }

    /**
     * Insert text to nth room name/number field then hit done
     * @param n is nth element
     * @param roomNameNumber is text for room name or number
     */
    public void inputNthRoomNameNumber(String n, String roomNameNumber) {
        String element = this.setNthRoomElement(n) + "//*[@resource-id='com.git.mami.kos:id/roomNameEditText']";
        appium.enterText(driver.findElement(By.xpath(element)), roomNameNumber, true);
    }

    /**
     * Insert text to nth floor (optional) field then hit done
     * @param n is nth element
     * @param floor is text for room name or number
     */
    public void inputNthFloor(String n, String floor) {
        String element = this.setNthRoomElement(n) + "//*[@resource-id='com.git.mami.kos:id/floorEditText']";
        appium.enterText(driver.findElement(By.xpath(element)), floor, true);
    }

}