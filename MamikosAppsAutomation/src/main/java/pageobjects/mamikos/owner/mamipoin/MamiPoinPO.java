package pageobjects.mamikos.owner.mamipoin;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class MamiPoinPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public MamiPoinPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(xpath = "//*[@name='triangle_dialog_up']//following::XCUIElementTypeOther/XCUIElementTypeStaticText")
    @AndroidFindBy(id = "mamiPointTooltipTextView")
    private WebElement mamipointTooltip;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='MamiPoin']/parent::XCUIElementTypeOther")
    @AndroidFindBy(id = "mamipointTooltipView")
    private WebElement mamipointLandingPageOnboardingTooltip;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Selanjutnya']/following::XCUIElementTypeButton)[1]")
    @AndroidFindBy(xpath = "//*[@text='Selanjutnya']")
    private WebElement nextButton;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='Selesai']/following::XCUIElementTypeButton)[1]")
    @AndroidFindBy(xpath = "//*[@text='Selesai']")
    private WebElement finishButton;

    @iOSXCUITFindBy(accessibility = "onTapRedeemPoint")
    @AndroidFindBy(id = "redeemPointView")
    private WebElement redeemPointButton;

    @iOSXCUITFindBy(accessibility = "historyGiftButton")
    @AndroidFindBy(id = "myRewardView")
    private WebElement rewardHistoryButton;

    @iOSXCUITFindBy(accessibility = "Cek seluruh hadiah yang telah Anda tukarkan dengan poin Anda di sini.")
    private WebElement rewardHistoryOnBoarding;

    @iOSXCUITFindBy(accessibility = "Semua poin yang didapat dan aktivitas yang telah dilakukan tercatat di sini.")
    private WebElement pointHistoryOnBoarding;

    @iOSXCUITFindBy(accessibility = "Pelajari cara-cara untuk mendapatkan poin di bagian ini.")
    private WebElement termAndConditionOnBoarding;

    @iOSXCUITFindBy(accessibility = "Anda dapat menukar poin Anda dengan hadiah di bagian ini.")
    private WebElement redeemPointOnBoarding;

    @iOSXCUITFindBy(accessibility = "Ini adalah jumlah poin Anda saat ini yang dapat Anda tukarkan dengan berbagai hadiah.")
    private WebElement mamipointLandingPageOnboardingTooltipText;

    @AndroidFindBy(id = "mamipointTenantView")
    private WebElement mamiPoinTenantEntryPointButton;

    @AndroidFindBy(id = "mamipointButton")
    private WebElement informasiPoinButton;

    @AndroidFindBy(xpath = "//*[@text='Riwayat Poin']")
    private WebElement riwayatPoinButton;

    @AndroidFindBy(xpath = "//*[@text='Dapatkan Poin']")
    private WebElement dapatkanPoinButton;

    @AndroidFindBy(id = "historyPointTenantRecyclerView")
    private WebElement riwayatPoinList;

    @AndroidFindBy(id = "tenantViewPager")
    private WebElement dapatkanPoinSection;

    @AndroidFindBy(id = "floatingMamipointView")
    private WebElement poinSaya;

    @AndroidFindBy(id = "emptyHistoryTenantImageView")
    private WebElement imageOfEmptyRiwayatPoin;

    @AndroidFindBy(id = "emptyPointInfoView")
    private WebElement tidakAdaPoinYangTersedia;

    @AndroidFindBy(id = "toGuidelineTextView")
    private WebElement lihatCaranyaButton;

    @AndroidFindBy(id = "descExpiredPointTextView")
    private WebElement descriptionOnInformasiPoinTenant;

    @AndroidFindBy(id = "tablePointView")
    private WebElement tableTitleOnInformasiPoinTenant;

    @AndroidFindBy(id = "datePointExpiredTextView")
    private WebElement expiredDateOnInformasiPoinTenant;

    @AndroidFindBy(id = "valuePointExpiredTextView")
    private WebElement expiredPoinOnInformasiPoinTenant;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='myTotalPointLabel'])[1]")
    @AndroidFindBy(id = "mamipoinValueTextView")
    private WebElement ownerPointLabel;

    @iOSXCUITFindBy(id = "MamiPoin")
    @AndroidFindBy(id = "ownerProfileMamipoinCard")
    private WebElement mamiPoinOwnerCard;

    @iOSXCUITFindBy(accessibility = "guideButton")
    @AndroidFindBy(id = "mamipointGuidelineView")
    private WebElement termAndConditionButton;

    @iOSXCUITFindBy(accessibility = "historyPointButton")
    @AndroidFindBy(id = "historyPointView")
    private WebElement pointHistoryButton;

    /**
     * Verify MamiPoin Card is Displayed or not
     * @return boolean
     */
    public boolean isMamiPoinCardDisplayed(){
        return appium.waitInCaseElementVisible(mamiPoinOwnerCard,3)!=null;
    }

    /**
     * Verify MamiPoin Onboarding Tooltip is Displayed or not in MamiPoin Landing Page
     *
     * @return boolean
     */
    public boolean isMamiPoinOnboardingDisplayed(){
        return appium.waitInCaseElementVisible(mamipointLandingPageOnboardingTooltip,3)!=null;
    }

    /**
     * Get MamiPoin Landing Page Onboarding Text
     *
     * @return text of mamipoin landing page onboarding
     */
    public String getMamiPoinLandingPageOnboardingText(){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(mamipointLandingPageOnboardingTooltipText,"value");
        }

    }

    /**
     * Click on Next Button on MamiPoin landing Page Onboarding
     */
    public void clickOnNextButton(){
        appium.clickOn(nextButton);
    }

    /**
     * Click on Finish Button on MamiPoin landing Page Onboarding
     */
    public void clickOnFinishButton(){
        appium.clickOn(finishButton);
    }

    /**
     * Click on Redeem Point Button
     */
    public void clickOnRedeemPointButton(){
        appium.clickOn(redeemPointButton);
    }

    /**
     * Click on MamiPoin Tenant Entry Point Button
     */
    public void clickOnMamiPoinTenantEntryPointButton(){
        appium.clickOn(mamiPoinTenantEntryPointButton);
    }

    /**
     * Click on Informasi Poin Button
     */
    public void clickOnInformasiPoinButton(){
        appium.clickOn(informasiPoinButton);
    }

    /**
     * Click on Riwayat Poin Button
     */
    public void clickOnRiwayatPoinButton(){
        appium.clickOn(riwayatPoinButton);
    }

    /**
     * Click on Dapatkan Poin Button
     */
    public void clickOnDapatkanPoinButton(){
        appium.clickOn(dapatkanPoinButton);
    }

    /**
     * Click on Reward History Button
     */
    public void clickOnRewardHistoryButton(){
        appium.clickOn(rewardHistoryButton);
    }

    /**
     * Get Reward History Onboarding Text
     *
     * @return text of reward history onboarding on mamipoin landing page
     */
    public String getRewardHistoryOnboardingText() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(rewardHistoryOnBoarding,"value");
        }
    }

    /**
     * Get Point History Onboarding Text
     *
     * @return text of point history onboarding on mamipoin landing page
     */
    public String getPointHistoryOnboardingText() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(pointHistoryOnBoarding,"value");
        }
    }

    /**
     * Get Term And Condition Onboarding Text
     *
     * @return text of term and condition onboarding on mamipoin landing page
     */
    public String getTermAndConditionOnboardingText() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(termAndConditionOnBoarding,"value");
        }
    }

    /**
     * Get Redeem Point Onboarding Text
     *
     * @return text of redeem point onboarding on mamipoin landing page
     */
    public String getRedeemPointOnboardingText() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(redeemPointOnBoarding,"value");
        }
    }

    /**
     * Verify Poin Saya is Displayed
     *
     * @return boolean
     */
    public boolean verifyPoinSayaDisplayed(){
        return appium.waitInCaseElementVisible(poinSaya,5) !=null;
    }

    /**
     * Verify Decription on Informasi Poin Tenant is Displayed
     *
     * @return boolean
     */
    public boolean verifyDescriptionOnInformasiPoinTenantDisplayed(){
        return appium.waitInCaseElementVisible(descriptionOnInformasiPoinTenant,5) !=null;
    }

    /**
     * Verify Table Title on Informasi Poin Tenant is Displayed
     *
     * @return boolean
     */
    public boolean verifyTableTitleOnInformasiPoinTenantDisplayed(){
        return appium.waitInCaseElementVisible(tableTitleOnInformasiPoinTenant,5) !=null;
    }

    /**
     * Verify Expired Date on Informasi Poin Tenant is Displayed
     *
     * @return boolean
     */
    public boolean verifyExpiredDateOnInformasiPoinTenantDisplayed(){
        return appium.waitInCaseElementVisible(expiredDateOnInformasiPoinTenant,5) !=null;
    }

    /**
     * Verify Expired Poin on Informasi Poin Tenant is Displayed
     *
     * @return boolean
     */
    public boolean verifyExpiredPoinOnInformasiPoinTenantDisplayed(){
        return appium.waitInCaseElementVisible(expiredPoinOnInformasiPoinTenant,5) !=null;
    }

    /**
     * Verify Riwayat Poin is Displayed
     *
     * @return boolean
     */
    public boolean verifyRiwayatPoinDisplayed(){
        return appium.waitInCaseElementVisible(riwayatPoinList,5) !=null;
    }

    /**
     * Verify Image of empty Riwayat Poin is Displayed
     *
     * @return boolean
     */
    public boolean verifyImageOfEmptyRiwayatPoinDisplayed(){
        return appium.waitInCaseElementVisible(imageOfEmptyRiwayatPoin,5) !=null;
    }

    /**
     * Verify Tidak Ada Poin Yang Tersedia is Displayed
     *
     * @return boolean
     */
    public boolean verifyTidakAdaPoinYangTersediaDisplayed(){
        return appium.waitInCaseElementVisible(tidakAdaPoinYangTersedia,5) !=null;
    }

    /**
     * Verify Lihat Caranya Button is Displayed
     *
     * @return boolean
     */
    public boolean verifyLihatCaranyaButtonDisplayed(){
        return appium.waitInCaseElementVisible(lihatCaranyaButton,5) !=null;
    }

    /**
     * Verify Dapatkan Poin Section is Displayed
     *
     * @return boolean
     */
    public boolean verifyDapatkanPoinSectionDisplayed(){
        return appium.waitInCaseElementVisible(dapatkanPoinSection,5) !=null;
    }

    /**
     * Get User Point Value
     *
     * @return point value
     */
    public int getOwnerMamiPoinValue(){
        String mamiPoinText = appium.getText(ownerPointLabel);
        String poinOnly = mamiPoinText.replaceAll("[^0-9]", "");
        return Integer.parseInt(poinOnly);
    }

    /**
     * CLick on MamiPoin Owner Entry Point
     */
    public void clickOnMamiPoinOwnerEntryPoint(){
        appium.clickOn(mamiPoinOwnerCard);
    }

    /**
     * Click on Term and Condition Button
     */
    public void clickOnTermAndConditionButton() {
        appium.clickOn(termAndConditionButton);
    }

    /**
     * Verify Owner MamiPoin Value is Displayed or not
     *
     * @return boolean
     */
    public boolean isOwnerMamiPoinValueDisplayed(){
        return appium.waitInCaseElementVisible(ownerPointLabel,5)!=null;
    }

    /**
     * Verify Reward History Button is Displayed or not
     *
     * @return boolean
     */
    public boolean isRewardHistoryButtonDisplayed() {
        return appium.waitInCaseElementVisible(rewardHistoryButton,5)!=null;
    }

    /**
     * Verify Point History Button is Displayed or not
     *
     * @return boolean
     */
    public boolean isPointHistoryButtonDisplayed() {
        return appium.waitInCaseElementVisible(pointHistoryButton,5)!=null;
    }

    /**
     * Verify Term And Condition Button is Displayed or not
     *
     * @return boolean
     */
    public boolean isTermAndConditionButtonDisplayed() {
        return appium.waitInCaseElementVisible(termAndConditionButton,5)!=null;
    }

    /**
     * Verify Redeem Point Button is Displayed or not
     *
     * @return boolean
     */
    public boolean isRedeemPointButtonDisplayed() {
        return appium.waitInCaseElementVisible(redeemPointButton,5)!=null;
    }
}
