package pageobjects.mamikos.owner.mamipoin;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class RedeemRewardPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RedeemRewardPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(xpath = "//*[@label='Alamat Pengiriman Hadiah']/following-sibling::*[@name='inputTextView']")
    @AndroidFindBy(id = "targetRedeemGoodsEditText")
    private WebElement shippingAddressField;

    @iOSXCUITFindBy(xpath = "//*[@label='Catatan']/following-sibling::*[@name='inputTextView']")
    @AndroidFindBy(id = "notesRedeemEditText")
    private WebElement noteField;

    @iOSXCUITFindBy(accessibility = "redeeemButton")
    @AndroidFindBy(id = "redeemPointButton")
    private WebElement redeemPointButton;

    @iOSXCUITFindBy(accessibility = "Yakin mau menukar poin untuk hadiah ini?")
    @AndroidFindBy(id = "messageTextView")
    private WebElement popUpConfirmationText;

    @iOSXCUITFindBy(accessibility = "Anda akan menukar 1 poin untuk mendapatkan hadiah ini")
    @AndroidFindBy(id = "warningMessageTextView")
    private WebElement popUpWarningText;

    @iOSXCUITFindBy(accessibility = "secondStepDateLabel")
    @AndroidFindBy(id = "onProcessRewardTextView")
    private WebElement rewardProcessingDate;

    @iOSXCUITFindBy(accessibility = "thirdStepDateLabel")
    @AndroidFindBy(id = "successTextView")
    private WebElement finalStatusDate;

    @iOSXCUITFindBy(xpath = "//*[@label='No Meter Listrik Anda']/preceding-sibling::*[@name='inputTextField']")
    @AndroidFindBy(id = "targetRedeemPLNEditText")
    private WebElement meterNumberField;

    @AndroidFindBy(id = "mamipoinValueTextView")
    private WebElement pointValueText;

    @iOSXCUITFindBy(accessibility = "Poin Anda tidak cukup")
    @AndroidFindBy(xpath = "//*[@text='Poin Anda tidak cukup']")
    private WebElement poinTidakCukup;

    @iOSXCUITFindBy(accessibility = "ic back default")
    private WebElement backButton;

    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"Done\"]")
    private WebElement doneButton;

    @iOSXCUITFindBy(xpath = "//*[@label='Nama']/preceding-sibling::*[@name='inputTextField']")
    @AndroidFindBy(id = "nameRedeemEditText")
    private WebElement nameRedemptionField;

    @iOSXCUITFindBy(xpath = "//*[@label='No HP yang bisa dihubungi']/preceding-sibling::*[@name='inputTextField']")
    @AndroidFindBy(id = "phoneRedeemEditText")
    private WebElement contactPhoneNumberField;

    @iOSXCUITFindBy(xpath = "//*[@label='No HP Penerima Voucher Pulsa']/preceding-sibling::*[@name='inputTextField']")
    @AndroidFindBy(id = "targetRedeemEditText")
    private WebElement recipientPhoneNumberField;

    @iOSXCUITFindBy(xpath = "//*[@label='No HP penerima sama dengan Pemilik Akun']")
    @AndroidFindBy(id = "targetRedeemCheckbox")
    private WebElement phoneNumberCheckbox;

    @iOSXCUITFindBy(accessibility = "Back")
    private WebElement backButtonOnRedemptionForm;

    /**
     * Verify warning Poin Anda Tidak Cukup is Displayed or not in Detail Hadiah
     *
     * @return boolean
     */
    public boolean isWarningPoinAndaTidakCukupDisplayed(){
        return appium.waitInCaseElementVisible(poinTidakCukup,5) !=null;
    }

    /**
     * Set Reward Shipping Address
     * @param address shipping address
     */
    public void setShippingAddress(String address){
        appium.enterText(shippingAddressField,address,true);
        if (Constants.MOBILE_OS==Platform.IOS){
            appium.tapOrClick(doneButton);
        }
    }

    /**
     * Set Redemption Note
     * @param note note
     */
    public void setRedemptionNote(String note){
        appium.enterText(noteField,note,true);
        if (Constants.MOBILE_OS==Platform.IOS){
            appium.tapOrClick(doneButton);
        }
    }

    /**
     * Set Electricity Meter Number
     *
     * @param number elctricity meter number
     */
    public void setElectricityMeterNumber(String number){
        appium.enterText(meterNumberField,number,true);
        if (Constants.MOBILE_OS==Platform.IOS){
            appium.tapOrClick(doneButton);
        }
    }

    /**
     * Verify reward type is PLN or not
     *
     * @return boolean
     */
    public boolean isRewardTypeIsPLN(){
        return appium.waitInCaseElementVisible(meterNumberField,3)!=null;
    }

    /**
     * Click on Redeem Point Button On Redemption Data
     */
    public void clickOnRedeemPointButton(){
        appium.clickOn(redeemPointButton);
    }

    /**
     * Get Pop Up Confirmation Message Text
     *
     * @return text of pop up confirmation
     */
    public String getPopUpMessage(){
        return appium.getText(popUpConfirmationText);
    }

    /**
     * Get Pop Up Warning Message Text
     *
     * @return text of pop up warning message
     */
    public String getPopUpWarningMessage(){
        return appium.getText(popUpWarningText);
    }

    /**
     * Click on Confirmation Button on Pop up
     *
     * @param action cancel or redeem
     */
    public void clickOnConfirmationButton(String action){
        if(Constants.MOBILE_OS==Platform.ANDROID){
            appium.clickOn(By.xpath("//*[@text='"+action+"']"));
        }
        else {
            appium.clickOn(By.xpath("//XCUIElementTypeButton[@name='"+action+"']"));
        }
    }

    /**
     * Get Date of Reward Processing
     *
     * @return text of reward processing date
     */
    public String getRewardProcessingDate() throws InterruptedException {
        appium.hardWait(1);
        return appium.getText(rewardProcessingDate);
    }

    /**
     * Get Date of Reward Rejected/Succeed
     *
     * @return text of reward rejected/succeed date
     */
    public String getRejectedOrSucceedDate(){
        return appium.getText(finalStatusDate);
    }

    /**
     * Get MamiPoin Value
     *
     * @return point
     */
    public String getPoint(){
        return appium.getText(pointValueText);
    }

    /**
     * Click on Back Button
     */
    public void clickOnBackButton() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.back();
        }
        else {
            if(appium.waitInCaseElementVisible(backButton,3)!=null){
                appium.clickOn(backButton);
            }
            else {
                appium.clickOn(backButtonOnRedemptionForm);
            }
        }
    }

    /**
     * Get Name Text on Redemption Form
     *
     * @return text of Name field
     */
    public String getName(){
        return appium.getText(nameRedemptionField);
    }

    /**
     * Get Contact Phone Number Text on Redemption Form
     *
     * @return text of contact phone number field
     */
    public String getContactPhoneNumber(){
        return appium.getText(contactPhoneNumberField);
    }

    /**
     * Verify Shipping Address Field is Displayed or not
     *
     * @return boolean
     */
    public boolean isShippingAddressDisplayed(){
        return appium.waitInCaseElementVisible(shippingAddressField,5)!=null;
    }

    /**
     * Verify Notes Field is Displayed or not
     *
     * @return boolean
     */
    public boolean isNotesDisplayed(){
        return appium.waitInCaseElementVisible(noteField,5)!=null;
    }

    /**
     * Verify Meter Number Field is Displayed or not
     *
     * @return boolean
     */
    public boolean isMeterNumberFieldDisplayed(){
        return appium.waitInCaseElementVisible(meterNumberField,5)!=null;
    }

    /**
     * Verify Recipient Phone Number Field is Displayed or not
     *
     * @return boolean
     */
    public boolean isRecipientPhoneNumberDisplayed(){
        return appium.waitInCaseElementVisible(recipientPhoneNumberField,5)!=null;
    }

    /**
     * Verify Phone Number Checkbox is Displayed or not
     *
     * @return boolean
     */
    public boolean isPhoneNumberCheckboxDisplayed() {
        return appium.waitInCaseElementVisible(phoneNumberCheckbox,5)!=null;
    }

    /**
     * Get Phone Number Checkbox Text
     *
     * @return text of phone number checkbox
     */
    public String getPhoneNumberCheckboxText(){
        return appium.getText(phoneNumberCheckbox);
    }
}

