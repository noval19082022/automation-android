package pageobjects.mamikos.owner.mamipoin;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class RewardDetailPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RewardDetailPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(accessibility = "onTapRedeemPoint")
    @AndroidFindBy(id = "redeemButton")
    private WebElement redeemButton;

    @iOSXCUITFindBy(accessibility = "titleLabel")
    @AndroidFindBy(id = "nameRewardTextView")
    private WebElement rewardTitle;

    @iOSXCUITFindBy(accessibility = "Poin Tukar")
    @AndroidFindBy(xpath = "//*[@text='Poin Tukar']")
    private WebElement poinTukarLabel;

    @iOSXCUITFindBy(accessibility = "pointLabel")
    @AndroidFindBy(id = "redeemedPointTextView")
    private WebElement pointLabel;

    @iOSXCUITFindBy(accessibility = "Batas Penukaran")
    @AndroidFindBy(xpath = "//*[@text='Batas Penukaran']")
    private WebElement batasPenukaranLabel;

    @iOSXCUITFindBy(accessibility = "dateLabel")
    @AndroidFindBy(id = "expiredRedeemTextView")
    private WebElement dateLabel;

    @iOSXCUITFindBy(accessibility = "Deskripsi")
    @AndroidFindBy(xpath = "//*[@text='Deskripsi']")
    private WebElement descriptionLabel;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='automate lack point']")
    @AndroidFindBy(id = "descRewardTextView")
    private WebElement descriptionText;

    @iOSXCUITFindBy(accessibility = "onTapTermAndCondition")
    @AndroidFindBy(accessibility = "Syarat dan Ketentuan")
    private WebElement termAndConditionTab;

    @iOSXCUITFindBy(accessibility = "onTapHowToRedeem")
    @AndroidFindBy(accessibility = "Cara Penukaran")
    private WebElement howToRedeemTab;

    @iOSXCUITFindBy(accessibility = "automate lack point")
    @AndroidFindBy(id = "tncMamipointScrollView")
    private WebElement termAndConditionList;

    /**
     * Click on Redeem Button
     */
    public void clickOnRedeemButton() throws InterruptedException {
        appium.hardWait(2);
        appium.tapOrClick(redeemButton);
    }

    /**
     * Verify Tukar Poin button is Displayed or not in Detail Hadiah
     *
     * @return boolean
     */
    public boolean isTukarPoinButtonDisplayed(){
        return appium.waitInCaseElementVisible(redeemButton,3)!=null;
    }

    /**
     * Verify Redeem Point Button is Enable or not
     *
     * @return boolean
     */
    public boolean isRedeemButtonIsEnable() throws InterruptedException {
        appium.hardWait(3);
        return redeemButton.isEnabled();
    }

    /**
     * Verify Reward Title button is Displayed or not in Detail Hadiah
     *
     * @return boolean
     */
    public boolean isRewardTitleDisplayed(){
        return appium.waitInCaseElementVisible(rewardTitle,3)!=null;
    }

    /**
     * Verify Poin Tukar Label is Displyed or not
     *
     * @return boolean
     */
    public boolean isPoinTukarLabelDisplayed() {
        return appium.waitInCaseElementVisible(poinTukarLabel,3)!=null;
    }

    /**
     * Verify Point Label is Displayed or not
     *
     * @return boolean
     */
    public boolean isPointLabelDisplayed() {
        return appium.waitInCaseElementVisible(pointLabel,3)!=null;
    }

    /**
     * Verify Batas Penukaran Label is Displayed or not
     *
     * @return boolean
     */
    public boolean isBatasPenukaranLabelDisplayed() {
        return appium.waitInCaseElementVisible(batasPenukaranLabel,3)!=null;
    }

    /**
     * Verify Date Label is Displayed or not
     *
     * @return boolean
     */
    public boolean isDateLabelDisplayed() {
        return appium.waitInCaseElementVisible(dateLabel,3)!=null;
    }

    /**
     * Verify Description Label is Displayed or not
     *
     * @return boolean
     */
    public boolean isDescriptionLabelDisplayed() {
        return appium.waitInCaseElementVisible(descriptionLabel,3)!=null;
    }

    /**
     * Verify Description Text is Displayed or not
     *
     * @return boolean
     */
    public boolean isDescriptionTextDisplayed() {
        return appium.waitInCaseElementVisible(descriptionText,3)!=null;
    }

    /**
     * Verify Term And Condition tab is Displayed or not
     *
     * @return boolean
     */
    public boolean isTermAndConditionTabDisplayed() {
        return appium.waitInCaseElementVisible(termAndConditionTab,3)!=null;
    }

    /**
     * Verify How to Redeem tab is Displayed or not
     *
     * @return boolean
     */
    public boolean getHowToRedeemTab() {
        return appium.waitInCaseElementVisible(howToRedeemTab,3)!=null;
    }

    /**
     * Verify Term And Condition List is Displayed or not
     *
     * @return boolean
     */
    public boolean isTermAndConditionListDisplayed() {
        return appium.waitInCaseElementVisible(termAndConditionList,3)!=null;
    }

    /**
     * Get Reward Point Value
     *
     * @return reward point
     */
    public int getRewardPoint() {
        String mamiPoinText = appium.getText(pointLabel);
        String poinOnly = mamiPoinText.replaceAll("[^0-9]", "");
        return Integer.parseInt(poinOnly);
    }
}
