package pageobjects.mamikos.owner.mamipoin;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.List;

public class RewardHistoryPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RewardHistoryPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(accessibility = "Riwayat Hadiah")
    @AndroidFindBy(xpath = "//*[@text='Riwayat Hadiah']")
    private WebElement rewardHistoryHeader;

    @iOSXCUITFindBy(className = "XCUIElementTypeCollectionView")
    @AndroidFindBy(id = "soonBillView")
    private WebElement rewardHistoryFilter;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='titleLabel']")
    @AndroidFindBy(id = "titleRewardTextView")
    private WebElement rewardTitle;

    @AndroidFindBy(id = "statusRewardImageView")
    private WebElement rewardStatusIcon;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='onTapLookGift']")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/defaultVoucherView']/android.widget.TextView[2]")
    private WebElement seeStatusText;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/emptyRewardView']/android.widget.ImageView")
    private WebElement emptyRewardHistoryImage;

    @iOSXCUITFindBy(accessibility = "Belum Ada Hadiah")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/emptyRewardView']/android.widget.TextView[1]")
    private WebElement emptyRewardHistoryTitle;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Belum Ada Hadiah']/following-sibling::XCUIElementTypeStaticText")
    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/emptyRewardView']/android.widget.TextView[2]")
    private WebElement emptyRewardHistoryMessage;

    @iOSXCUITFindBy(accessibility = "valueRewardLabel")
    @AndroidFindBy(xpath = "//*[@text='ID Penukaran']/following-sibling::android.widget.TextView")
    private WebElement redemptionIdLabel;

    @iOSXCUITFindBy(accessibility = "valueDescriptionLabel")
    @AndroidFindBy(xpath = "//*[@text='Keterangan']/following-sibling::android.widget.TextView")
    private WebElement redemptionNotes;

    @iOSXCUITFindBy(accessibility = "thirdStepTitleLabel")
    @AndroidFindBy(id = "successRedeemTextView")
    private WebElement redemptionFinalStatus;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Hubungi Kami']")
    @AndroidFindBy(id = "contactAdminMamipoinButton")
    private WebElement contactUsButton;

    @iOSXCUITFindBy(accessibility = "Butuh bantuan?")
    @AndroidFindBy(xpath = "//*[@text='Butuh bantuan?']")
    private WebElement needHelpTitle;

    @iOSXCUITFindBy(accessibility = "Silakan hubungi CS Mamikos via WhatsApp")
    @AndroidFindBy(id = "notifNeedHelpView")
    private WebElement contactCSText;

    @iOSXCUITFindBy(xpath = "//*[@value='Pertanyaan Umum']")
    @AndroidFindBy(xpath = "//*[@text='Pertanyaan Umum']")
    private WebElement faqTitle;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton/following-sibling::XCUIElementTypeStaticText")
    @AndroidFindBy(id = "faqTextView")
    private List<WebElement> questionList;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeCell/XCUIElementTypeButton")
    @AndroidFindBy(id = "showQuestionImageView")
    private List<WebElement> expandCollapseButtonList;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton/preceding-sibling::XCUIElementTypeStaticText")
    @AndroidFindBy(id = "descQuestionTextView")
    private WebElement answerList;

    private By rewardHistoryCard = By.xpath("//XCUIElementTypeStaticText[@name='titleLabel']/parent::XCUIElementTypeCell");

    private By redemptionStatus = By.xpath("//XCUIElementTypeStaticText[@name='pointLabel']");

    private By rewardRedemptionCard = By.id("defaultVoucherView");

    private By rewardStatusText = By.id("statusRewardTextView");

    /**
     * Verify Reward History Header is Displayed or not
     *
     * @return boolean
     */
    public boolean isRewardHistoryHeaderDisplayed(){
        return appium.waitInCaseElementVisible(rewardHistoryHeader,3)!=null;
    }

    /**
     * Verify Reward History Filter is Displayed or not
     *
     * @return boolean
     */
    public boolean isRewardHistoryFilterDisplayed(){
        return appium.waitInCaseElementVisible(rewardHistoryFilter,3)!=null;
    }

    /**
     * Verify Reward Redemption Card is Displayed or not
     *
     * @return boolean
     */
    public boolean isRewardRedemptionCardDisplayed(){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.waitInCaseElementVisible(rewardRedemptionCard,3)!=null;
        }
        else {
            return appium.waitInCaseElementVisible(rewardHistoryCard,3)!=null;
        }

    }

    /**
     * Verify Reward Title is Displayed or not
     *
     * @return boolean
     */
    public boolean isRewardTitleDisplayed(){
        return appium.waitInCaseElementVisible(rewardTitle,3)!=null;
    }

    /**
     * Get Text of Reward Titla
     *
     * @return text of reward title
     */
    public String getRewardTitleText(){
        return appium.getText(rewardTitle);
    }

    /**
     * Verify Reward Status and text is Displayed or not
     *
     * @return boolean
     */
    public boolean isRewardStatusDisplayed(){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.waitInCaseElementVisible(rewardStatusIcon,3)!=null &&
                    appium.waitInCaseElementVisible(rewardStatusText,3)!=null;
        }
        else {
            return appium.waitInCaseElementVisible(redemptionStatus,3)!=null;
        }
    }

    /**
     * Click on See Status Text
     */
    public void clickOnSeeStatusRedemption(){
        appium.clickOn(seeStatusText);
    }

    /**
     * Click on Reward History Filter
     *
     * @param status state of redeemed reward
     */
    public void clickOnFilter(String status) throws InterruptedException {
        if(status.equals("Penukaran gagal")){
            appium.hardWait(2);
            appium.scrollHorizontal(0.2, 0.80, 0.20, 2000);
        }
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.clickOn(By.xpath("//*[@text='"+status+"']"));
        }
        else{
            appium.clickOn(By.xpath("//XCUIElementTypeStaticText[@name='"+status+"']"));
        }

    }

    /**
     * Get number of Redemption Card List on Reward History
     *
     * @return number of redemption card
     */
    public int getRedemptionCardNumber(){
        List<WebElement> redemptionCardList;
        if(Constants.MOBILE_OS == Platform.ANDROID){
            redemptionCardList = driver.findElements(rewardRedemptionCard);
        }
        else {
            redemptionCardList = driver.findElements(rewardHistoryCard);
        }
        return redemptionCardList.size();
    }

    /**
     * Get Redemption Status
     *
     * @return text of redemption status
     */
    public String getRedemptionStatus(int index) {
        List<WebElement> redemptionStatusList;
        if(Constants.MOBILE_OS == Platform.ANDROID){
            redemptionStatusList = driver.findElements(rewardStatusText);
        }
        else {
            appium.scroll(0.5,0.50,0.45,2000);
            redemptionStatusList = driver.findElements(redemptionStatus);
        }
        return appium.getText(redemptionStatusList.get(index));
    }

    /**
     * Verify Empty Reward History Image is Displayed or not
     *
     * @return boolean
     */
    public boolean isEmptyRewardHistoryImageDisplayed(){
        return appium.waitInCaseElementVisible(emptyRewardHistoryImage,3)!=null;
    }

    /**
     * Get Empty Reward History Title
     *
     * @return text of empty reward history title
     */
    public String getEmptyRewardHistoryTitle(){
        return appium.getText(emptyRewardHistoryTitle);
    }

    /**
     * Get Empty Reward History Message
     *
     * @return text of empty reward history message
     */
    public String getEmptyRewardHistoryMessage(){
        return appium.getText(emptyRewardHistoryMessage);
    }

    /**
     * Verify Redemption Id is Displayed or not
     *
     * @return boolean
     */
    public boolean isRedemptionIdDisplayed(){
        return appium.waitInCaseElementVisible(redemptionIdLabel,3)!=null;
    }

    /**
     * Get Text of Redemption Notes
     *
     * @return redemption notes
     */
    public String getRedemptionNotes() {
        return appium.getText(redemptionNotes);
    }

    /**
     * Get Redemption Final Status Text
     *
     * @return text of redemption final status
     */
    public String getRedemptionFinalStatus(){
        return appium.getText(redemptionFinalStatus);
    }

    /**
     * Verify Contact Us Button Displayed or not
     *
     * @return boolean
     */
    public boolean isContactUsButtonDisplayed() throws InterruptedException {
        appium.hardWait(5);
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByResourceId("contactAdminMamipoinButton");
        }
        else {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return appium.waitInCaseElementVisible(contactUsButton,5)!=null;
    }

    /**
     * Verify Need Help Section Displayed or not
     *
     * @return boolean
     */
    public boolean isNeedHelpSectionDisplayed(){
        return appium.waitInCaseElementVisible(needHelpTitle,5)!=null &&
                appium.waitInCaseElementVisible(contactCSText,5)!=null;
    }

    /**
     * Verify FAQ Title Displayed or not
     *
     * @return boolean
     */
    public boolean isFAQTitleDisplayed(){
        return appium.waitInCaseElementVisible(faqTitle,5)!=null;
    }

    /**
     * Get Text of Questions
     *
     * @return boolean
     */
    public String getQuestionsText(int index){
        if (Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(questionList.get(index));
        }
        else {
            return appium.getElementAttributeValue(questionList.get(index),"value");
        }
    }

    /**
     * Click on Expand/Collapse Button on Question
     *
     * @param index
     */
    public void clickOnExpandCollapseButton(int index) throws InterruptedException {
        appium.hardWait(3);
        appium.click(expandCollapseButtonList.get(index));
    }

    /**
     * Verify Answer is Displayed or not
     *
     * @return boolean
     */
    public boolean isAnswerDisplayed() {
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.waitTillElementsCountIsEqualTo(By.id("descQuestionTextView"),1);
            return appium.waitInCaseElementVisible(answerList,5)!=null;
        }
        else {
            return appium.waitInCaseElementVisible(By.xpath("//XCUIElementTypeStaticText[@name='Setelah Anda menukarkan poin, Anda cukup menunggu hingga penerimaan hadiah. Tim Mamikos akan menghubungi Anda untuk konfirmasi dan penerimaan hadiah khusus.']"),5)!=null ||
                    appium.waitInCaseElementVisible(By.xpath("//XCUIElementTypeStaticText[@name='Jangka waktu proses penukaran hadiah bergantung pada jenis hadiah. Pastikan untuk terus pantau status penukaran hadiah pada halaman ini.']"),5)!=null;
        }

    }

    /**
     * Verify Question is Collapsed
     *
     */
    public void isQuestionCollapsed() {
        appium.waitTillElementsCountIsEqualTo(By.id("descQuestionTextView"),0);
    }
}
