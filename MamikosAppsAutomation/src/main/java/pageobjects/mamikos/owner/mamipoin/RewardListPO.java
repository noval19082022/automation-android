package pageobjects.mamikos.owner.mamipoin;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

import java.util.List;

public class RewardListPO {

    JavaHelpers java;
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RewardListPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(accessibility = "Pastikan poin Anda cukup untuk ditukarkan dengan hadiah yang Anda inginkan.")
    @AndroidFindBy(id = "mamiPointTooltipTextView")
    private WebElement mamipointTooltip;

    @iOSXCUITFindBy(accessibility = "Anda dapat menukar poin Anda sesuai dengan jumlah yang dibutuhkan hadiah terkait.")
    private WebElement rewardTooltip;

    @iOSXCUITFindBy(accessibility = "Tekan tombol Bantuan untuk kembali mempelajari cara penukaran poin.")
    private WebElement helpTooltip;

    @iOSXCUITFindBy(accessibility = "Bantuan")
    @AndroidFindBy(id = "helpMamipointImageView")
    private WebElement helpButton;

    @iOSXCUITFindBy(accessibility = "ic back default")
    @AndroidFindBy(id = "backPointImageView")
    private WebElement backButton;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeCell/XCUIElementTypeStaticText[@name='titleLabel'])")
    @AndroidFindBy(id = "titleAvailableRewardTextView")
    private List<WebElement> rewardTitle;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='pointLabel'])[1]")
    @AndroidFindBy(id = "mamipoinValueTextView")
    private WebElement pointLabel;

    @AndroidFindBy(id = "mamipoinValueFloatingTextView")
    private WebElement floatingPointLabel;

    @AndroidFindBy(id = "textView8")
    private WebElement floatingMamiPoinLabel;

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='titleLabel'])[1]")
    private WebElement mamiPoinTitle;

    /**
     * Get MamiPoin Landing Page Onboarding Text
     *
     * @return text of mamipoin landing page onboarding
     */
    public String getMamiPoinLandingPageOnboardingText(){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(mamipointTooltip,"value");
        }
    }

    /**
     * Get Reward Onboarding Text
     *
     * @return text of reward onboarding
     */
    public String getRewardOnboardingText(){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(rewardTooltip,"value");
        }
    }

    /**
     * Get Help Onboarding Text
     *
     * @return text of help onboarding
     */
    public String getHelpOnboardingText(){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(mamipointTooltip);
        }
        else {
            return appium.getElementAttributeValue(helpTooltip,"value");
        }
    }

    /**
     * Verify MamiPoin Onboarding Tooltip is Displayed or not in Reward List Page
     *
     * @return boolean
     */
    public boolean isMamiPoinOnboardingDisplayed(){
        return appium.waitInCaseElementVisible(mamipointTooltip,3)!=null;
    }

    /**
     * Click on Help Button
     */
    public void clickOnHelpButton(){
        appium.clickOn(helpButton);
    }

    /**
     * Verify Help Button is Displayed or not
     *
     * @return boolean
     */
    public boolean isHelpButtonDisplayed(){
        return appium.waitInCaseElementVisible(helpButton,5)!=null;
    }

    /**
     * Verify Back Button is Displayed or not
     *
     * @return boolean
     */
    public boolean isBackButtonDisplayed(){
        return appium.waitInCaseElementVisible(backButton,5)!=null;
    }

    /**
     * Verify Reward is Displayed or Not
     *
     * @param rewardName reward name
     * @return boolean
     */
    public boolean isRewardDisplayed(String rewardName) {
        boolean found = false;
        String rewardTitleText;
        for (WebElement webElement : rewardTitle) {
            if (Constants.MOBILE_OS == Platform.ANDROID) {
                rewardTitleText = appium.getText(webElement);
            }
            else {
                rewardTitleText = appium.getElementAttributeValue(webElement, "value");
            }
            appium.scroll(0.5,0.5,0.4,2000);
            found = rewardTitleText.equals(rewardName);
            if(found){
                break;
            }
        }
        return found;
    }

    /**
     * Click on See Reward
     *
     * @param rewardName reward name
     */
    public void clickOnSeeReward(String rewardName){
        if(Constants.MOBILE_OS==Platform.ANDROID){
            appium.scrollToElementByText(rewardName);
            appium.clickOn(By.xpath("//*[@text='"+rewardName+"']/following-sibling::android.widget.TextView"));
        }
        else {
            for(int i=0; i<rewardTitle.size(); i++){
                if (appium.waitInCaseElementVisible(By.xpath("//*[@value='"+rewardName+"']/following-sibling::XCUIElementTypeButton"),3)!=null){
                    break;
                }
                appium.scroll(0.5,0.5,0.2,2000);
            }
            appium.clickOn(By.xpath("//*[@value='"+rewardName+"']/following-sibling::XCUIElementTypeButton"));
        }
    }

    /**
     * Get Owner Mamipoint
     *
     * @return text of point label
     */
    public String getPointLabelTextOnRewardList() {
        if (Constants.MOBILE_OS==Platform.ANDROID){
            return appium.getText(pointLabel);
        }
        else {
            return appium.getElementAttributeValue(pointLabel,"value");
        }
    }

    /**
     * Scroll Down Reward List
     */
    public void scrollDownRewardList() throws InterruptedException {
        appium.hardWait(2);
        appium.scroll(0.5, 0.80, 0.20, 2000);
    }

    /**
     * Verify MamiPoin Header is Displayed or not
     *
     * @return boolean
     */
    public boolean verifyMamiPoinHeaderDisplayed() {
        if (Constants.MOBILE_OS==Platform.ANDROID){
            return appium.waitTillElementIsVisible(floatingMamiPoinLabel,3)!=null && appium.waitTillElementIsVisible(floatingPointLabel,3)!=null;
        }
        else {
            return appium.waitInCaseElementVisible(mamiPoinTitle,3)!=null && appium.waitInCaseElementVisible(pointLabel,3)!=null ;
        }
    }

    /**
     * Get Reward Name Text
     *
     * @param index
     * @return Reward Name
     */
    public String getRewardName(int index){
        return appium.getText(rewardTitle.get(index));
    }

    /**
     * Get User Point Value
     *
     * @return user point
     */
    public int getUserPoint() {
        String mamiPoinText = appium.getText(pointLabel);
        String poinOnly = mamiPoinText.replaceAll("[^0-9]", "");
        return Integer.parseInt(poinOnly);
    }
}
