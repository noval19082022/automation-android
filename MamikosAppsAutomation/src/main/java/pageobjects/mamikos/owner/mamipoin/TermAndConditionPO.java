package pageobjects.mamikos.owner.mamipoin;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class TermAndConditionPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public TermAndConditionPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Syarat dan Ketentuan']")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Syarat dan Ketentuan']")
    private WebElement termAndConditionHeader;

    @iOSXCUITFindBy(accessibility = "Back")
    @AndroidFindBy(id = "backImageView")
    private WebElement backButton;

    @iOSXCUITFindBy(className = "XCUIElementTypeTextView")
    @AndroidFindBy(className = "android.view.View")
    private WebElement termAndConditionList;

    @iOSXCUITFindBy(accessibility = "Perbandingan Skema Poin")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Perbandingan Skema Poin']")
    private WebElement pointSchemeTitle;

    @iOSXCUITFindBy(accessibility = "Anda dapat melihat skema poin lebih detail")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Anda dapat melihat skema poin lebih detail']")
    private WebElement pointSchemeMessage;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Pelajari Lebih Lanjut']")
    @AndroidFindBy(id = "learnMoreButton")
    private WebElement learnMoreButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='URL']")
    @AndroidFindBy(id = "com.sec.android.app.sbrowser:id/location_bar_edit_text")
    private WebElement windowUrl;

    /**
     * Verify Back Button is Displayed or not
     *
     * @return boolean
     */
    public boolean isBackButtonDisplayed() {
        return appium.waitInCaseElementVisible(backButton,5)!=null;
    }

    /**
     * Verify Term And Condition Header is Displayed or not
     *
     * @return boolean
     */
    public boolean isTermAndConditionHeaderDisplayed() {
        return appium.waitInCaseElementVisible(termAndConditionHeader,5)!=null;
    }

    /**
     * Verify Term And Condition List is Displayed or not
     *
     * @return boolean
     */
    public boolean isTermAndConditionListDisplayed() {
        return appium.waitInCaseElementVisible(termAndConditionList,5)!=null;
    }

    /**
     * Verify Point Scheme Section is Displayed or not
     *
     * @return boolean
     */
    public boolean isPointSchemeSectionDisplayed() throws InterruptedException {
        appium.hardWait(3);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        return appium.waitInCaseElementVisible(pointSchemeTitle,5)!=null &&
                appium.waitInCaseElementVisible(pointSchemeMessage,5)!=null &&
                appium.waitInCaseElementVisible(learnMoreButton,5)!=null;

    }

    /**
     * Click on Link Content on TnC List
     *
     * @param content tnc
     */
    public void clickOnLink(String content) {
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText(content);
            appium.clickOn(By.xpath("//android.view.View[@content-desc='"+content+"']/android.widget.TextView"));
        }
        else {
            appium.scroll(0.5, 0.20, 0.80, 2000);
            appium.scroll(0.5, 0.20, 0.80, 2000);
            appium.clickOn(By.xpath("//XCUIElementTypeLink[@name='"+content+"']"));
        }
    }

    /**
     * Get Current Window Url Text
     *
     * @return text of url
     */
    public String getMamikosUrl() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(windowUrl);
        }
        else{
            System.out.println("ini loh "+ appium.getElementAttributeValue(windowUrl,"value"));
            return appium.getElementAttributeValue(windowUrl,"value").substring(0,12);
        }
    }
}
