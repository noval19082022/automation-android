package pageobjects.mamikos.owner.premium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class BalanceAllocationPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public BalanceAllocationPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "manageBalanceButton")
    private WebElement manageBalanceButton;

    @AndroidFindBy(id = "clearTextView")
    private WebElement clearTextView;

    @AndroidFindBy(id = "confirmationButton")
    private WebElement confirmationButton;

    @AndroidFindBy(id = "twoTextView")
    private WebElement twoTextView;

    @AndroidFindBy(id = "seeApartmentTextView")
    private WebElement viewApartmentAds;

    @AndroidFindBy(id = "manageAdsButton")
    private WebElement manageAdsButton;

    @AndroidFindBy(id = "zeroTextView")
    private WebElement zeroTextView;

    @AndroidFindBy(id = "allocatedBalancePriceTextView")
    private WebElement allocatedBalanceText;

    /**
     * Scroll to Manage Balance and Click Manage Balance Button
     */
    public void clickOnManageBalanceAllocationButton(){
        appium.scrollToElementByText("Kelola Alokasi Saldo");
        appium.tapOrClick(manageBalanceButton);
    }

    /**
     * Fill on Allocation Amount
     */
    public void fillAllocationAmount(){
        for (int i = 0; i < 5; i++) {
            appium.tapOrClick(clearTextView);
        }
        appium.tapOrClick(twoTextView);
        for (int i = 0; i < 4; i++) {
            appium.tapOrClick(zeroTextView);
        }
    }

    /**
     * Click on Confirmation Button
     */
    public void clickOnConfirmationButton(){
        appium.tapOrClick(confirmationButton);
    }

    /**
     * Get Allocation Balance Text
     * @return Allocation Balance
     */
    public String getAllocationBalanceText(){
        return appium.getText(allocatedBalanceText);
    }

    /**
     * Click on View Aparment Ads
     */
    public void clickOnViewApartmentAds(){
        appium.tapOrClick(viewApartmentAds);
    }

    /**
     * Click on Manage Ads Button
     */
    public void clickOnManageAdsButton(){
        appium.tapOrClick(manageAdsButton);
    }
}
