package pageobjects.mamikos.owner.premium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class ManageAdsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ManageAdsPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "topAdsSwitchView")
    private WebElement adsSwitchView;

    @AndroidFindBy(id = "adsBalancePriceTextView")
    private WebElement adsBalanceText;

    @AndroidFindBy(xpath = "//android.widget.EditText[contains(@resource-id, 'com.git.mami.kos:id/filterTimeEditText']")
    private WebElement filterTimeDropDown;

    @AndroidFindBy(id = "viewCountPropertyTextView")
    private WebElement viewCountPropertyText;

    @AndroidFindBy(id = "likeCountPropertyTextView")
    private WebElement likeCountPropertyText;

    @AndroidFindBy(id = "clickCountApartmentTextView")
    private WebElement clickStatisticText;

    @AndroidFindBy(id = "manageBalanceButton")
    private WebElement manageBalanceButton;

    @AndroidFindBy(id = "allocationAmountEditText")
    private WebElement allocationSaldoField;

    @AndroidFindBy(id = "confirmationButton")
    private WebElement confirmationSaldoButton;

    @AndroidFindBy(id = "oneTextView")
    private WebElement oneTextButton;

    @AndroidFindBy(id = "zeroTextView")
    private WebElement zeroTextButton;



    /**
     * Check active balance is present
     * @return status true / false
     */
    public boolean isAdsBalancePricePresent() {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        return appium.waitInCaseElementVisible(adsBalanceText, 10) != null;
    }

    /**
     * Check active ads switcher is present
     * @return status true / false
     */
    public boolean isAdsSwitchViewPresent() {
        return appium.waitInCaseElementVisible(adsSwitchView, 10) != null;
    }

    /**
     * Click ads switcher
     */
    public void activatedPromotedAds(){
        appium.clickOn(adsSwitchView);
    }

    /**
     * Click filter time dropdown
     */
    public void clickFilterTime() {
        appium.clickOn(filterTimeDropDown);
    }

    /**
     * Check views count ads present
     * @return status true / false
     */
    public boolean isViewCountPropertyTextPresent() {
        return appium.waitInCaseElementVisible(viewCountPropertyText, 10) != null;
    }

    /**
     * Check likes count ads present
     * @return status true / false
     */
    public boolean isLikeCountPropertyTextPresent() {
        return appium.waitInCaseElementVisible(likeCountPropertyText, 10) != null;
    }

    /**
     * Check is the click statistic present
     * @return status true / false
     */
    public boolean isClickStatisticPresent() {
        return appium.waitInCaseElementVisible(clickStatisticText, 10) != null;
    }

    /**
     * Click manage balance allocation
     */
    public void clickManageBalanceAllocation() {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.clickOn(manageBalanceButton);
    }

    /**
     * enter allocation balance for ads manually
     * cause the button confirmation will not enable if using entertext method
     */
    public void setAllocationBalance(){
        appium.waitInCaseElementVisible(allocationSaldoField, 10);
        allocationSaldoField.clear();
        appium.clickOn(oneTextButton);
        appium.clickOn(zeroTextButton);
        appium.clickOn(zeroTextButton);
        appium.clickOn(zeroTextButton);
        appium.clickOn(zeroTextButton);
        appium.clickOn(confirmationSaldoButton);
    }

}
