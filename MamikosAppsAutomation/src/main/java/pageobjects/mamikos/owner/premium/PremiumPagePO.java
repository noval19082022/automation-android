package pageobjects.mamikos.owner.premium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class PremiumPagePO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public PremiumPagePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "seeKosTextView")
    private WebElement previewKostAds;

    @AndroidFindBy(id = "manageAdsButton")
    private WebElement manageAdsButton;

    @AndroidFindBy(id = "balancePerDaySwitchView")
    private WebElement activeDailyBalanceSwitcher;

    @AndroidFindBy(id = "messageTextView")
    private WebElement nonActivatePremiumPopUp;

    @AndroidFindBy(id = "cancelButton")
    private WebElement nonActivatePremiumButton;

    @AndroidFindBy(id = "nextButton")
    private WebElement cancelButton;


    /**
     * Click preview kost ads button
     * @throws InterruptedException
     */
    public void clickPreviewKostAds() throws InterruptedException {
        appium.hardWait(5);
        appium.clickOn(previewKostAds);
    }

    /**
     * Click manage ads button
     */
    public void clickManageKosButton() {
        appium.waitInCaseElementVisible(manageAdsButton,5);
        appium.clickOn(manageAdsButton);
    }

    /**
     * Click activate daily balance premium switcher
     * @throws InterruptedException
     */
    public void clickActivateDailyBalance() throws InterruptedException {
        appium.hardWait(5);
        appium.clickOn(activeDailyBalanceSwitcher);
    }

    /**
     * Check Confirm Non Active Pop Up is present
     * @return true / false
     */
    public boolean isConfirmNonActivePopUpPresent(){
       return appium.waitInCaseElementVisible(nonActivatePremiumPopUp, 5) != null;
    }

    /**
     * Click nonactivate daily balance premium button
     */
    public void clickNonactivateDailyBalance() {
        appium.clickOn(nonActivatePremiumButton);
    }

    /**
     * Click cancel nonactivate daily balance premium button
     */
    public void clickCancelNonActiveDailyBalance() {
        appium.clickOn(cancelButton);
    }
}
