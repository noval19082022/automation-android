package pageobjects.mamikos.owner.premium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class UpgradePremiumPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;
    TouchAction touchAction ;

    public UpgradePremiumPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        touchAction= new TouchAction(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "activatePremium1")
    private WebElement upgradeButton;

    @AndroidFindBy(id = "seePackageTextView")
    private WebElement packageButton;

    @AndroidFindBy(id = "choosePackageButton")
    private WebElement pilihButton;

    @AndroidFindBy(xpath = "//android.widget.ImageView[@package='com.git.mami.kos']")
    private WebElement backButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Paket 1 hari']")
    private WebElement yearpremiumButton;

    @AndroidFindBy(id = "nextButton")
    private WebElement nextButton;

    @AndroidFindBy(xpath = "//android.widget.ImageView[@index='2']")
    private WebElement closeButton;

    @AndroidFindBy(id = "ivInviteTrialClose")
    private WebElement closefreeButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Bank BNI']")
    private WebElement bniButton;

    @AndroidFindBy(id = "chooseButton")
    private WebElement pilihMetodeButton;

    @AndroidFindBy(id = "bankNameTextView")
    private WebElement bankName;

    @AndroidFindBy(id = "balanceTextView")
    private WebElement beliSaldoButton;

    @AndroidFindBy(id = "confirmPaymentButton")
    private WebElement konfirmasiPembayaranButton;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='SUBMIT']")
    private WebElement submitButton;

    @AndroidFindBy(id = "textinput_error")
    private WebElement warningText;

    @AndroidFindBy(id = "et_payment_confirm_owner_name")
    private WebElement nameTextfield;

    @AndroidFindBy(id = "confirmChangePackageButton")
    private WebElement changePackageButton;

    @AndroidFindBy(id = "titlePackageNameTextView")
    private WebElement saldoText;

//    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Oke']/following::XCUIElementTypeButton[1]")
//    @AndroidFindBy(xpath = "//*[@text='Oke']")
//    private WebElement balancePerDayPopUp;
    private By balancePerDayPopUp = By.xpath("//*[@text='Oke']");

    @AndroidFindBy(id = "ftueOkeTextView")
    private WebElement okeOnFTUEPopUp;

    private By backPreviousButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("backImageView"): By.id("Back");

    @AndroidFindBy(id = "paymentButton")
    private WebElement bayarButton;

    @AndroidFindBy(id = "productNameTextView")
    private WebElement packageNameText;

    /**
     * Click on Atur Premium
     *
     * @throws InterruptedException
     */
    public void clickOnAturPremium() throws InterruptedException {
        if (appium.waitInCaseElementClickable(upgradeButton, 3) != null) {
            appium.tapOrClick(upgradeButton);
        }
    }

    /**
     * Click on Lihat Paket
     *
     * @throws InterruptedException
     */
    public void clickOnLihatPaket() throws InterruptedException {
        if (appium.waitInCaseElementClickable(packageButton, 3) != null) {
            appium.tapOrClick(packageButton);
        }
    }

    /**
     * Click on Pilih Paket
     *
     * @throws InterruptedException
     */
    public void clickOnPilihPaket() throws InterruptedException {
        appium.tapOrClick(pilihButton);
        appium.hardWait(3);
    }

    /**
     * Click on back button
     *
     * @throws InterruptedException
     */
    public void tapbackbtn() throws InterruptedException {
        appium.tapOrClick(backButton);
    }

    /**
     * Click on 12 bulan
     *
     * @throws InterruptedException
     */
    public void clickOnpackage() throws InterruptedException {
        appium.tapOrClick(yearpremiumButton);
        appium.hardWait(3);
    }

    /**
     * Close premium faq
     *
     * @throws InterruptedException
     */
    public void closefaq() throws InterruptedException {
        appium.tapOrClick(closeButton);
        appium.hardWait(3);
    }

    /**
     * Close pop up premium free
     *
     * @throws InterruptedException
     */
    public void closePremiumFree() throws InterruptedException {
        appium.tapOrClick(closefreeButton);
        appium.hardWait(3);
    }

    /**
     * Click on Bank BNI
     *
     * @throws InterruptedException
     */
    public void clickBankBNI() throws InterruptedException {
        appium.tapOrClick(bniButton);
    }

    /**
     * Click on Pilih metode
     *
     * @throws InterruptedException
     */
    public void clickPilihMetode() throws InterruptedException {
        appium.tapOrClick(pilihMetodeButton);
    }

    /**
     * Get Bank Name from the page
     *
     * @return Bank Name
     */
    public String getBankName() {
        return appium.getText(bankName);
    }

    /**
     * Click on Beli Saldo
     *
     * @throws InterruptedException
     */
    public void clickOnBeliSaldo() {
        appium.tapOrClick(beliSaldoButton);
    }

    /**
     * Click on Konfirmasi Pembayaran
     *
     * @throws InterruptedException
     */
    public void clickOnKonfirmasiPembayaran() {
        appium.tapOrClick(konfirmasiPembayaranButton);
    }

    /**
     * Click on Konfirmasi Pembayaran
     *
     * @throws InterruptedException
     */
    public void clickOnSubmitBtn() {
        appium.tapOrClick(submitButton);
    }

    /**
     * Get Warning text
     *
     * @return Warning Text
     */
    public String getWarningText() {
        return appium.getText(warningText);
    }

    /**
     * Fill out login form and click on 'LOGIN' button
     *
     * @param name   name value
     */
    public void fillNameform(String name){
        appium.enterText(nameTextfield, name, false);
    }

    /**
     * Click on Ganti Paket
     *
     * @throws InterruptedException
     */
    public void clickOnChangePackage() {
        appium.tapOrClick(changePackageButton);
    }

    /**
     * Choose on Paket Price
     *
     * @throws InterruptedException
     */
    public void chooseOnPackage(String price) {
        WebElement postprice = driver.findElement(By.xpath("//android.widget.TextView[@text=" + price + "]"));
        appium.tapOrClick(postprice);
    }

    /**
     * Get Saldo iklan
     *
     * @return amount saldo
     */
    public String getSaldoIklan(){
        return appium.getText(saldoText);
    }

    /**
     * Click on Balance Per Day Pop Up
     */
    public void clickOnBalancePerDayPopUp(){
        if (appium.waitInCaseElementClickable(balancePerDayPopUp, 3) != null) {
            appium.tapOrClick(balancePerDayPopUp);
        }
    }

    public void clickOnFTUE() {
        for (int a=1; a<6; a++){
            if (appium.waitInCaseElementClickable(okeOnFTUEPopUp, 3) != null) {
                appium.tapOrClick(okeOnFTUEPopUp);
            }
        }
    }

    public void clickOnBack(int x, int y) throws InterruptedException {
        appium.hardWait(3);
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.tapOrClick(backPreviousButton);
        }else {
            appium.tap(x,y);
        }

    }

    public void clickOnOnboardingPremium() {
        for(int a=1;a<5;a++){
            if (appium.waitInCaseElementClickable(nextButton, 3) != null) {
                appium.tapOrClick(nextButton);
            }
        }
    }

    public void clickOnPackage(String text) {
        WebElement packagename = driver.findElement(By.xpath("//android.widget.TextView[@text='" + text + "']"));
        if (appium.waitInCaseElementClickable(packagename, 3) != null) {
            appium.clickOn(packagename);
        }
    }

    public String getBayarButton() {
        return appium.getText(bayarButton);
    }

    public boolean getBayarSekarang() {
        return appium.isElementDisplayed(bayarButton);

    }

    public boolean getTextPackageName(String packageName) {
        return appium.getText(packageNameText).contains(packageName);
    }

    /**
     * Click on Back icon Button
     */
    public void clickOnBackIconButton() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.tapOrClick(backPreviousButton);
        } else {
            touchAction.tap(PointOption.point(16,63)).perform();
        }
    }
}
