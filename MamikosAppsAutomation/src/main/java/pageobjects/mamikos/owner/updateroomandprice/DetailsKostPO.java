package pageobjects.mamikos.owner.updateroomandprice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class DetailsKostPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public DetailsKostPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    private By anotherPriceButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("anotherPriceTextView"): By.id("Lihat harga lainnya");

    private By inputMainPrice = Constants.MOBILE_OS == Platform.ANDROID ? By.id("inputPriceEditText"): By.id("priceTextField");

    private By updatePriceButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("updatePriceButton"): By.name("updateButton");

    private By exceptMonthPriceView = Constants.MOBILE_OS == Platform.ANDROID ? By.id("exceptMonthPriceView"): By.xpath("//*[@value='Harga Per Tahun']");

    private String priceList = Constants.MOBILE_OS == Platform.ANDROID ? ("//android.widget.TextView[@resource-id='com.git.mami.kos:id/titleInputPrice']"): ("//*[@resource-id='com.git.mami.kos:id/titleInputPrice' and @text='%s']");


    @AndroidFindBy(xpath = "//*[@text='Biaya Deposit']/following-sibling::*[@resource-id='com.git.mami.kos:id/typeSwitchView']")
    private WebElement depositPriceToggle;

    @AndroidFindBy(xpath = "//*[@text='Biaya Deposit']/following-sibling::*[@text='OFF']")
    private WebElement depositPriceToggleOff;

    @AndroidFindBy(id = "inputEditTextV2MainInput")
    private WebElement mainInput;

    @AndroidFindBy(id ="titleDepositCostTextView")
    private WebElement depositTitle;

    @AndroidFindBy(id ="savePriceButton")
    private WebElement savePriceButton;

    @AndroidFindBy(id ="nameAdditionalPriceTextView")
    private WebElement additionalPriceActiveText;

    @AndroidFindBy(id="valueAdditionalPriceTextView")
    private WebElement additionalPriceActivePrice;

    @AndroidFindBy(id="deleteAdditionalPriceTextView")
    private WebElement deleteButtonOfActivePrice;

    @AndroidFindBy(id="messageTextView")
    private WebElement popUpMessageText;

    @AndroidFindBy(id="nextButton")
    private WebElement nextButton;

    @AndroidFindBy(id="editAdditionalPriceTextView")
    private WebElement editActiveAdditionalPrice;

    @AndroidFindBy( xpath ="//*[@text='Biaya Denda']/following-sibling::*[@resource-id='com.git.mami.kos:id/typeSwitchView']")
    private WebElement dendaPriceToogle;

    @AndroidFindBy(xpath = "//*[@text='Biaya Denda']/following-sibling::*[@text='OFF']")
    private WebElement dendaPriceToggleOff;

    @AndroidFindBy(id = "titleDepositCostTextView")
    private WebElement dendaTitle;

    @AndroidFindBy(xpath ="//*[@resource-id='com.git.mami.kos:id/priceTotalInputLayout']/*[@resource-id='com.git.mami.kos:id/inputEditTextV2MainInput']")
    private WebElement inputDendaPrice;

    @AndroidFindBy(xpath="//*[@resource-id='com.git.mami.kos:id/totalInputLayout']/*[@resource-id='com.git.mami.kos:id/inputEditTextV2MainInput']")
    private WebElement inputDendaTime;

    @AndroidFindBy(xpath="//*[@text='Denda']//following-sibling::*[@resource-id='com.git.mami.kos:id/editView']//*[@resource-id='com.git.mami.kos:id/editAdditionalPriceTextView']")
    private WebElement editDendaButton;

    @AndroidFindBy(xpath="//*[@text='Denda']//following-sibling::*[@resource-id='com.git.mami.kos:id/deleteView']/*[@resource-id='com.git.mami.kos:id/deleteAdditionalPriceTextView']")
    private WebElement deleteDendaButton;

    @AndroidFindBy(id = "titleTextOnlyTextView")
    private WebElement popUpConfirmation;

    @AndroidFindBy(xpath = "//*[@text='Biaya Lainnya Per Bulan']/following-sibling::*[@resource-id='com.git.mami.kos:id/typeSwitchView']")
    private WebElement otherPriceToggle;

    @AndroidFindBy(id = "titleOtherCostTextView")
    private WebElement titleOtherCostText;

    @AndroidFindBy(xpath = "//*[@text='Nama Biaya']/following-sibling::*[@resource-id='com.git.mami.kos:id/inputEditTextV2MainInput']")
    private WebElement priceNameInput;

    @AndroidFindBy(xpath = "//*[@text='Total']/following-sibling::*[@resource-id='com.git.mami.kos:id/inputEditTextV2MainInput']")
    private WebElement priceNumberInput;

    @AndroidFindBy(id = "addAdditionalPriceButton")
    private WebElement addMoreOtherPriceButton;

    @AndroidFindBy(xpath = "//*[@text='Biaya Uang Muka (DP)']/following-sibling::*[@resource-id='com.git.mami.kos:id/typeSwitchView']")
    private WebElement downPaymentToggle;

    @AndroidFindBy(id= "titleFineCostTextView")
    private WebElement fineTitle;

    @AndroidFindBy(id = "android:id/text1")
    private WebElement downPaymentPercentageChoice;

    @AndroidFindBy(id = "detailPriceView")
    private WebElement downPaymentDetailPriceView;

    @AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/editView']/preceding::*[contains(@text, 'DP') and contains(@text, '%')]")
    private WebElement dpActivePercentage;

    @AndroidFindBy(xpath = "//*[contains(@text, 'DP') and contains(@text, '%')]/following-sibling::*[@resource-id='com.git.mami.kos:id/valueAdditionalPriceTextView']")
    private WebElement dpActivePriceRange;

    @AndroidFindBy(xpath = "//*[contains(@text, 'DP')]//following-sibling::*//*[@resource-id='com.git.mami.kos:id/deleteAdditionalPriceTextView']")
    private WebElement downPaymentDeleteButton;

    @AndroidFindBy(xpath = "//*[contains(@text, 'DP')]//following-sibling::*//*[@resource-id='com.git.mami.kos:id/editAdditionalPriceTextView']")
    private WebElement downPaymentChangesButton;

    private By totalRoomChoice = By.id("totalRoomView");

    private By pilihButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("okButton"): By.id("positiveButton");

    private By updateRoomButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("fullViewSaveRoomButton"): By.id("actionButton");


    /**
     * Format element price list to specific price list
     * @param priceName e.g Harga Per Bulan
     * @return web element
     */
    private WebElement priceListTarget(String priceName) {
        priceList = String.format(priceList, priceName);
        return driver.findElementByXPath(priceList);
    }

    /**
     * Tap or click on another price button
     */
    public void clickOnAnotherPriceButton(String currentText) {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            scrollToAnotherPriceButton(currentText);
            appium.tapOrClick(anotherPriceButton);
        } else {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            String priceList = "//*[@value='" + currentText + "']";
            WebElement element = driver.findElementByXPath(priceList);
            appium.tapOrClick(element);
        }
    }
    /**
     * Get price list attribute value
     * @param attName attribute name e.g checkable
     * @param priceName price list name e.g Harga Per Bulan
     * @return string text
     */
    public String getPriceListStatus(String attName, String priceName) {
        return appium.getElementAttributeValue(priceListTarget(priceName), attName);
    }

    /**
     * Return true if element present otherwise false
     * @param priceName price list name e.g Harga Per Bulan
     * @return true or false
     */
    public boolean isPriceAvailable(String priceName) {
        return appium.waitInCaseElementVisible(priceListTarget(priceName), 2) != null;
    }

    /**
     * Scroll to target element price list
     * @param priceName e.g Harga Per Bulan
     */
    public void scrollToPriceNameList(String priceName) {
        if (!isPriceAvailable(priceName)) {
            appium.scrollToElementByText(priceName);
        }
    }

    /**
     * Check if element is visible, return true if visible otherwise false
     * @return
     */
    public boolean isAnotherPriceButtonVisible() {
        return appium.waitInCaseElementVisible(anotherPriceButton, 2) != null;
    }

    /**
     * Scroll to element another price button if it not visible
     * @param currentText Tampilkan lebih sedikit or Tampilkan lebih banyak
     */
    public void scrollToAnotherPriceButton(String currentText) {
        if (!isAnotherPriceButtonVisible()) {
            appium.scrollToElementByText(currentText);
        }
    }

    /**
     * Set main price
     * @param price kost target price e.g 3000000
     */
    public void setInputMainPrice(String price) {
        if (Constants.MOBILE_OS == Platform.IOS){
            appium.scroll(0.5, 0.2, 0.8, 2000);
        }
        appium.enterText(inputMainPrice, price, true);
    }

    /**
     * Check update price button is present in the screen
     * @return true if button present otherwise false
     */
    public boolean isUpdatePriceButtonPresent() {
        return appium.waitInCaseElementVisible(updatePriceButton, 2) != null;
    }

    /**
     * Tap or click on update price button
     */
    public void tapOnUpdatePriceButton() {
        int i = 0;
        while (!isUpdatePriceButtonPresent()) {
            appium.scrollDown(2, 2);
            i++;
            if(i==4) {
                break;
            }
        }
        appium.tapOrClick(updatePriceButton);
    }

    /**
     * Check if another price box is visible in the viewport
     * @return
     */
    public boolean isAnotherPriceIsPresent() {
        return appium.waitInCaseElementVisible(exceptMonthPriceView, 3) != null;
    }

    /**
     * Tap on deposit price toggle to activate or deactivate
     */
    public void tapOnDepositPriceToggle() {
        appium.scrollToElementByText("Biaya Deposit");
        appium.tapOrClick(depositPriceToggle);
    }

    /**
     * Pop up is deposit pop up
     * @return boolean
     */
    public boolean isDepositPopUpPresent() {
        return appium.waitInCaseElementVisible(depositTitle, 3) != null;
    }

    /**
     * Set deposit price
     * @param depositPrice e.g 800000
     */
    public void setMainAdditionalPrice(String depositPrice) {
        appium.enterText(mainInput, depositPrice, false);
        appium.hideKeyboard();
    }

    /**
     * Save price button on deposit etc popup
     */
    public void tapOnSavePriceButton() {
        appium.tapOrClick(savePriceButton);
    }

    /**
     * Get text of active additional price
     * @return text of active addtional price e.g Deposit
     */
    public String getActiveAdditionalPriceText() {
        appium.scrollToElementByText("Biaya Uang Muka (DP)");
        return appium.getText(additionalPriceActiveText);
    }

    /**
     * Get text of active additional price active price
     * @return string e.g Rp200.000
     */
    public String additionalPriceActivePriceText() throws InterruptedException {
        appium.hardWait(5);
        return appium.getText(additionalPriceActivePrice);
    }

    /**
     * Delete active additional price
     */
    public void tapOnDeleteAdditionalPriceButton() {
        appium.tapOrClick(deleteButtonOfActivePrice);
    }

    /**
     * Title text confirmation pop up
     * @return string text e.g Yakin hapus biaya Deposit?
     */
    public String getPopUpConfirmationAdditionalPriceText() {
        return appium.getText(popUpMessageText);
    }

    /**
     * Can be reusable for another case. Next buttton e.g Ya, Hapus
     */
    public void tapOnNextButton() {
        appium.tapOrClick(nextButton);
    }

    /**
     * Check if deposit toggle button is in inactive state
     * @return boolean true or false
     */
    public boolean isDepositPriceInactive() {
        return appium.waitInCaseElementVisible(depositPriceToggleOff, 5) != null;
    }

    /**
     * Tap on ubah button on active additional price
     */
    public void tapOnChangeButtonActiveAdditionalPrice() {
        appium.tapOrClick(editActiveAdditionalPrice);
    }

    /**
     * Tap on denda price toggle to activate or deactivate
     */
    public void tapOnDendaPriceToogle() throws InterruptedException {
        appium.hardWait(3);
        appium.tapOrClick(dendaPriceToogle);
        appium.hardWait(3);
    }

    /**
     * Pop up is deposit pop up
     * @return boolean
     */
    public boolean isDendaPopUpPresent() {
        return appium.waitInCaseElementVisible(dendaTitle, 3) != null;
    }

    /**
     * Set Denda Price
     */
    public void setDendaPrice(String dendaPrice){
        appium.enterText(inputDendaPrice, dendaPrice,false);
    }

    /**
     * Set Denda Time
     */
    public void setDendaTime(String dendaTime){
        appium.enterText(inputDendaTime, dendaTime, false);
    }

    /**
     * Tap on Ubah denda
     */
   public void tapOnUbahDenda() throws InterruptedException {
       appium.scrollToElementByText("Denda");
       appium.tapOrClick(editDendaButton);
       appium.hardWait(3);
   }

    /**
     * Tap on delete active Denda button
     * @throws InterruptedException
     */
   public void tapOnDeleteDendaButton() throws InterruptedException {
       appium.tapOrClick(deleteDendaButton);
       appium.hardWait(2);
   }

    /**
     * Get text string from pop-up appear
     * @return text string e.g "Biaya berhasil diubah"
     */
   public String getPopUpConfirmationText() {
       appium.waitTillElementIsVisible(popUpConfirmation, 60);
       return appium.getText(popUpConfirmation);
   }

    /**
     * Tap on Other Price Toggle / Biaya Lainnya Per Bulan Toggle
     */
    public void tapOnOtherPriceToggle() {
        appium.tap(otherPriceToggle);
    }

    /**
     * Take other price title text
     * @return String data type e.g "Biaya lainnya per bulan"
     */
    public String getOtherPriceTitleText() {
        return appium.getText(titleOtherCostText);
    }

    /**
     * Set other price name on the input box
     * @param priceName other price name e.g "Biaya listrik"
     * @param clear true/false based on input box precondition
     */
    public void setOtherPriceName(String priceName, boolean clear) {
        appium.enterText(priceNameInput, priceName, clear);
    }

    /**
     * Set other price number on the input box
     * @param priceNumber other price's price e.g "50.000"
     * @param clear true/false based on input box precondition
     */
    public void setOtherPriceNumber(int priceNumber, boolean clear) {
        appium.enterText(priceNumberInput, String.valueOf(priceNumber), clear);
    }

    /**
     * Get unseen text ON/OFF base on other price active/innactive state
     * @return text ON/OFF
     */
    public String getOtherPriceStateText() {
        return appium.getText(otherPriceToggle);
    }

    /**
     * Take other price name
     * @param name set with other price's name e.g "Biaya listrik"
     * @return text string
     */
    public String getOtherPriceName(String name) {
        String otherPriceName = "//*[@text='"+name+"']";
        return appium.getText(driver.findElementByXPath(otherPriceName));
    }

    /**
     * Take other price's number with Rp
     * @param name name set with other price's name e.g "Biaya listrik"
     * @param price set with other price's number with Rp. e.g "Rp50.000
     * @return text string
     */
    public String getOtherPriceNumber(String name, String price) {
        String otherPriceNumber = "//*[@text='"+name+"']/following-sibling::*[@text='"+ price +"']";
        return appium.getText(driver.findElementByXPath(otherPriceNumber));
    }

    /**
     * Tap on change/edit/ubah on other price's list.
     * @param priceName set with other price's name e.g "Biaya listrik"
     */
    public void tapOnChangeWithPriceName(String priceName) {
        String priceToEdit = "//*[@text='"+ priceName +"']/following-sibling::*[@resource-id='com.git.mami.kos:id/editView']";
        appium.tap(driver.findElementByXPath(priceToEdit));
    }

    /**
     * Tap on delete/hapus on price list based on name set. Can also be use for Denda, Deposit and DP
     * @param priceName set with other price's name e.g "Biaya listrik"
     */
    public void deleteAdditionPriceFromTheList(String priceName) {
        String deletePriceNameOnTheList = "//*[@text='"+ priceName +"']/following-sibling::*[@resource-id='com.git.mami.kos:id/deleteView']";
        appium.tap(driver.findElementByXPath(deletePriceNameOnTheList));
    }

    /**
     *
     * @param priceName set other price's name e.g "Biaya listrik kami"
     * @return boolean true/false based on other price present or not
     */
    public boolean isAdditionalPricePresent(String priceName) {
        String priceListName = "//*[@resource-id='com.git.mami.kos:id/additionalPriceRecyclerView']//*[@text='"+ priceName +"']";
        try {
            appium.waitInCaseElementVisible(driver.findElementByXPath(priceListName), 5);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap add more other price, for add second++ price on other price per month
     */
    public void tapOnAddMoreOtherPriceButton() {
        appium.tap(addMoreOtherPriceButton);
    }

    /**
     * Take attribute value as text
     * @param priceName set with text name you desired
     * @param attribute name e.g displayed
     * @return
     */
    public String getPriceNameAttribute(String priceName, String attribute) {
        String priceListName = "//*[@resource-id='com.git.mami.kos:id/additionalPriceRecyclerView']//*[@text='"+ priceName +"']";
        return appium.getElementAttributeValue(driver.findElementByXPath(priceListName), attribute);
    }

    /**
     * Check down payment is toggle is present in view port
     * @return true or false
     */
    public boolean isDownPaymentTogglePresent() {
        return appium.waitInCaseElementVisible(downPaymentToggle, 5) != null;
    }

    /**
     * Tap on down payment toggle, will scroll to the toggle first if it not present in screen
     * @throws InterruptedException
     */
    public void tapOnDownPaymentToggle() throws InterruptedException {
        appium.hardWait(2);
        if(!isDownPaymentTogglePresent()) {
            do {
                appium.scroll(0.5, 0.8, 0.2, 2000);
            }
            while (!isDownPaymentTogglePresent());
        }
        appium.tap(downPaymentToggle);
    }

    /**
     * Get additional price input text title
     * @param addPriceTitleToCheck and with deposit, down payment, other price
     * @return text string e.g Biaya Lainnya Per Bulan
     */
    public String getAdditionalPriceInputTitle(String addPriceTitleToCheck) {
        switch (addPriceTitleToCheck) {
            case "deposit":
            case "down payment":
                return appium.getText(depositTitle);
            case "other price":
                return appium.getText(titleOtherCostText);
            default:
                return appium.getText(fineTitle);
        }
    }

    /**
     * Check percentage choice in down payment is present in screen
     * @return true or false
     */
    public boolean isDownPaymentPercentageChoicePresent() {
        return appium.waitInCaseElementVisible(downPaymentPercentageChoice, 5) != null;
    }

    /**
     * Check if price detail is present on downpayment options screen
     * @return
     */
    public boolean isDownPaymentDetailPriceViewPresent() {
        return appium.waitInCaseElementVisible(downPaymentDetailPriceView, 5) != null;
    }

    /**
     * Get string text of default percentage of down payment
     * @return String text e.g 10% dari harga sewa
     */
    public String getDownPaymentDefaultPercentage() {
        return appium.getText(downPaymentPercentageChoice);
    }

    /**
     * Take DP per period text
     * @param index element index 1, 2, 3, 4, 5 etc
     * @return detail price text e.g DP Sewa Per Bulan
     */
    public String getDPAvailablePriceList(int index) {
        String detailPriceElement = "//android.widget.LinearLayout["+ index +"]/android.widget.TextView[1]";
        return appium.getText(driver.findElementByXPath(detailPriceElement));
    }

    /**
     * Get current condition of down payment toggle base on element text
     * @return ON/OFF
     */
    public String getDownPaymentActiveState() {
        return appium.getText(downPaymentToggle);
    }

    /**
     * Get active dp percentage text
     * @return string text e.g "DP 10%"
     */
    public String getActiveDPPercentage() {
        return appium.getText(dpActivePercentage);
    }

    /**
     * get active dp price range tex
     * @return string text e.g 100000-200000
     */
    public String getDPActivePriceRangeText() {
        return appium.getText(dpActivePriceRange);
    }

    /**
     * Tap on down payment delete button
     */
    public void tapsOnDownPaymentDeleteButton() {
        appium.tap(downPaymentDeleteButton);
    }

    /**
     * Tap on down payment change button
     */
    public void tapsOnDownPaymentChangeButton() {
        appium.tapOrClick(downPaymentChangesButton);
    }

    /**
     * Tap on dropdown percentage choice
     */
    public void tapOnDPDropDown() {
        appium.tapOrClick(downPaymentPercentageChoice);
    }

    /**
     * Choose down payment percentage
     * @param percentage set with desired percentage e.g 30%
     */
    public void chooseDPPercentage(String percentage) {
        String percentageEl = "//*[@resource-id='android:id/text1' and @text='"+percentage+"']";
        WebElement element = driver.findElementByXPath(percentageEl);
        appium.tapOrClick(element);
    }

    /**
     * Set total room
     * @param total total kost owner
     */
    public void inputTotalRoom(String total) {
        appium.tapOrClick(totalRoomChoice);
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            String totalRoom = "//*[@text='"+total+"']";
            WebElement element = driver.findElementByXPath(totalRoom);
            appium.tapOrClick(element);
        } else {
            MobileElement elKey = (MobileElement) driver.findElement(By.className("XCUIElementTypePicker"));
            elKey.setValue(total);
        }
    }

    /**
     * Tap or click on pilh total room
     */
    public void tapOnUpdateRoom() {
        appium.tapOrClick(updateRoomButton);
    }

    /**
     * click pilih button
     */
    public void tapPilihButton() {
        appium.tapOrClick(pilihButton);
    }

    /**
     * Get price list attribute value
     * @param attName attribute name e.g checkable
     * @param priceName price list name e.g Harga Per Bulan
     * @return string text
     */
    public String getPriceListStatusIos(String attName, String priceName) {
        String priceList = "//*[@value='"+priceName+"']";
        WebElement element = driver.findElementByXPath(priceList);
        return appium.getElementAttributeValue(element, attName);
    }

}
