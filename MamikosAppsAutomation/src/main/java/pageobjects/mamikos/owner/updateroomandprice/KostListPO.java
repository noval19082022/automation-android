package pageobjects.mamikos.owner.updateroomandprice;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.List;

public class KostListPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public KostListPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    @FindBy(id = "titleToolbarTextView")
    private WebElement textTitleToolbar;

    private By kostListPage = By.id("kostListRecyclerView");

    /**
     * Check is in kost list price update
     * @return list present true otherwise false
     */
    private boolean isInKostListPriceUpdate() {
        return appium.waitInCaseElementVisible(kostListPage, 5) != null;
    }

    /**
     * Click or tap on kost name in the list
     * @param kostName kost name e.g Kose Mamiset Automation
     */
    public void clickOnKostList(String kostName) throws InterruptedException {
        appium.hardWait(5);
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.scrollToElementByText("kost automation new");
            String kost = "//*[@text='"+ kostName +"']";
            WebElement kostList = driver.findElementByXPath(kost);
            appium.waitTillElementIsVisible(kostList);
            do {
                appium.tapOrClick(kostList);
            }
            while (isInKostListPriceUpdate());
        }else{
            String kost = "//*[@value='"+ kostName +"']";
            WebElement kostList = driver.findElementByXPath(kost);
            appium.waitTillElementIsVisible(kostList);
            appium.tapOrClick(kostList);
        }
    }

    /**
     * Get list of kost list element
     * @return List<WebElement> of kost list
     */
    public List<WebElement> getKostList() {
        return appium.waitTillAllElementsAreLocated(kostListPage);
    }

    /**
     * Check page Update Harga kost appear
     * @return true / false
     */
    public boolean checkPageHargaKost() {
        return appium.waitInCaseElementVisible(textTitleToolbar, 10) != null;
    }

    /**
     * Get title text of current page.
     * @return String data type
     */
    public String getPageTitle() {
        return appium.getText(textTitleToolbar);
    }
}
