package pageobjects.mamikos.tenant;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class NotificationPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public NotificationPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "notificationHomeImageView")
    private WebElement notificationIcon;

    private By firstNotification = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Info & Promo']/following::android.widget.LinearLayout[3]/preceding::android.widget.TextView[1]"): By.xpath("//*[@name='Info & Promo']/following::XCUIElementTypeCell[1]/XCUIElementTypeStaticText[1]");

    @AndroidFindBy(id = "backImageView")
    private WebElement backImageView;

    private By rejectNotification = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Info & Promo']/following::android.widget.LinearLayout[3]/preceding::android.widget.TextView[1]"): By.xpath("(//XCUIElementTypeStaticText[@name='Yah, permintaan booking tidak disetujui'])[1]");

    /**
     * Click on notification icon
     */
    public void clickNotificationIcon() throws InterruptedException {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.tapOrClick(notificationIcon);
        }
        appium.hardWait(3);
        appium.tap(342,63);
    }


    /**
     * Get notification title from top notification list
     */
    public String getFirstNotificationTitle() {
        appium.waitInCaseElementVisible(firstNotification,10);
        return appium.getText(firstNotification);
    }

    /**
     * Click top notification on notification list
     */
    public void clickOnFirstNotification() throws InterruptedException{
        appium.hardWait(5);
        appium.tapOrClick(firstNotification);
    }

    /**
     * Click back image view
     */
    public void clickOnBackImageView() {
        appium.tapOrClick(backImageView);
    }

    /**
     * Get reject notification title from top notification list
     */
    public String getRejectNotificationTitle() {
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.scroll(0.5, 0.8, 0.2, 2000);
        }
        return appium.getText(rejectNotification);
    }
}
