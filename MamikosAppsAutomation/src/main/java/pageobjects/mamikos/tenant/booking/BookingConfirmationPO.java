package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

import javax.naming.CompositeName;
import javax.naming.ConfigurationException;

public class BookingConfirmationPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public BookingConfirmationPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(driver,this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 

	@AndroidFindBy(id="kosNameTextView")
	private WebElement roomName;

	@AndroidFindBy(id="rentPriceTextView")
	private WebElement roomPrice;

	@AndroidFindBy(id="rentTypeTextView")
	private WebElement rentType;

	@AndroidFindBy(id="dropDownDurationTextView")
	private WebElement rentDuration;

	@AndroidFindBy(xpath="//*[@resource-id=\"com.git.mami.kos:id/valueRenterNameView\"]//*[@resource-id=\"com.git.mami.kos:id/mainTextInputLayout\"]//android.widget.EditText")
	private WebElement tenantName;

	@AndroidFindBy(xpath="//android.widget.RadioButton[@checked=\"true\"]/following-sibling::android.widget.LinearLayout/android.widget.TextView")
	private WebElement tenantGender;

	@AndroidFindBy(xpath="//*[@resource-id='com.git.mami.kos:id/phoneNumberInputView']/*[@resource-id='com.git.mami.kos:id/inputEditTextV2MainInput']")
	private WebElement tenantMobile;

	@AndroidFindBy(id="jobTextView")
	private WebElement tenantJobTitle;

//	    @AndroidFindBy(id="termCheckBox")
//	    private WebElement termAndConditionCheckBox;
	private By termAndConditionCheckBox = By.id("termCheckBox");

//	    @AndroidFindBy(id="continueTextView")
//	    private WebElement continueButton;
	private By continueButton = By.id("continueTextView");

	@AndroidFindBy(id="titleBookingSuccessTextView")
	private WebElement bookingSuccessfullyLabel;

	@AndroidFindBy(id="dismiss_text")
	private WebElement clearAllLabel;

	@AndroidFindBy(id="com.git.mami.kos:id/textinput_error")
	private WebElement validationErrorText;

	@AndroidFindBy(id = "com.git.mami.kos:id/nextButton")
	private WebElement nextButton;

	@AndroidFindBy(id ="com.git.mami.kos:id/maleRadioButton")
	private WebElement maleButton;

	@AndroidFindBy(id ="com.git.mami.kos:id/femaleRadioButton")
	private WebElement femaleButton;

	@AndroidFindBy(xpath ="//*[@resource-id='com.git.mami.kos:id/disclaimerGenderView']/*[@resource-id='com.git.mami.kos:id/messageAlertTextView']")
	private WebElement alertMessageText;

	@AndroidFindBy(id = "com.git.mami.kos:id/incrementImageView")
	private WebElement incrementRenterButton;

	@AndroidFindBy(id ="com.git.mami.kos:id/decrementImageView")
	private WebElement decrementRenterButton;

	@FindBy(id = "com.git.mami.kos:id/counterRenterView")
	private WebElement counterRenterBox;

	@FindBy(id = "marriedCheckBox")
	private WebElement marriedCheckBox;

	@FindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/phoneNumberInputView']/*[@resource-id='com.git.mami.kos:id/inputEditTextV2Warning']")
	private WebElement phoneNumberInvalidText;

	@FindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/phoneEditText']/*[@resource-id='com.git.mami.kos:id/inputEditTextV2Warning']")
	private WebElement phoneNumberInvalidTextEdit;

	@FindBy(id="previewBookingTitle")
	private WebElement titleText;

	@FindBy(id = "previewBookingEditUserBooking")
	private WebElement editRenterInfoButton;

	@FindBy(id = "submitReviewButton")
	private WebElement saveButton;

	@FindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/phoneEditText']/*[@resource-id='com.git.mami.kos:id/inputEditTextV2MainInput']")
	private WebElement inputPhoneNumberEdit;

	private By downPaymentPreviewPrice = By.xpath("//android.widget.TextView[@text='Rp400.000']");
	private By firstPaymentPreviewPrice = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Rp401.000']"): By.xpath("//*[@text='Total Pembayaran Pertama (DP)']/following-sibling::*[@resource-id='com.git.mami.kos:id/paymentPreviewTotalPriceValue']");
	private By percenDPText = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='20% dari harga sewa']"): By.id("20% dari harga sewa");
	private By repaymentPrice = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Rp1.600.000']"): By.id("paymentPreviewRemainingValue");
	private By totalRepaymentPreviewPrice = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Rp2.301.000']"):  By.xpath("//*[@text='Total Pelunasan']/following-sibling::*[@resource-id='com.git.mami.kos:id/paymentPreviewTotalPriceValue']");

	private By ajukanSewaButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("bookingBtnCv"): By.id("Ajukan Sewa");

	private By tncCheckbox = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.CheckBox[@index='0']"): By.xpath("//*[@name='Biaya sewa kos']/following::XCUIElementTypeOther[12]");

	private By submitButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("btnSend"): By.id("Kirim pengajuan ke pemilik");


	/** Get Room name
	 * @return  Room name
	 */
	public String getRoomName()
	{
		return appium.getText(roomName);
	}

	/** Get Room price
	 * @return  Room price
	 */
	public String getRoomPrice()
	{
		return appium.getText(roomPrice);
	}

	/** Get rent type
	 * @return  rent type
	 */
	public String getRentType()
	{
		return appium.getText(rentType);
	}

	/** Get rent duration
	 * @return  rent duration
	 */
	public String getRentDuration()
	{
		return appium.getText(rentDuration);
	}

	/** Get tenant name
	 * @return  tenant name
	 */
	public String getTenantName()
	{
		return appium.getText(tenantName);
	}

	/** Get tenant gender
	 * @return  tenant gender
	 */
	public String getTenantGender()
	{
		return appium.getText(tenantGender);
	}

	/** Get tenant mobile
	 * @return  tenant mobile
	 */
	public String getTenantMobile()
	{
		return appium.getText(tenantMobile);
	}

	/** Get tenant job title
	 * @return  tenant job title
	 */
	public String getTenantJobTitle()
	{
		return appium.getText(tenantJobTitle);
	}

	/**
	 * Select Terms & Condition checkbox and click on Continue button
	 */
	public void selectTermsAndConditionCheckboxAndClickOnContinue()
	{
		appium.scrollToElementByResourceId("continueTextView");
		//Click
		int x = driver.findElement(termAndConditionCheckBox).getLocation().getX();
		int y = driver.findElement(termAndConditionCheckBox).getLocation().getY();

		appium.tap(x, y);
		appium.tapOrClick(continueButton);
	}

	/**
	 * Check is price list is present
	 * @param bookingPrice input with Pwr Bulan, Per Minggu, Per Tahun, Per 6 Bulan, and Per Hari
	 * @return true or fase
	 * @throws InterruptedException
	 */
	public boolean isBookingPricePresent(String bookingPrice) throws InterruptedException {
			String stringPrice = "//*[@text='"+ bookingPrice + "']";
			By pricePresent = By.xpath(stringPrice);
			return appium.isElementPresent(pricePresent);
	}

	/**
	 * Verify booking successfully
	 */
	public void messageBookingSuccessfullyIsPresent() {
		appium.isElementDisplayed(bookingSuccessfullyLabel);
	}

	/**
	 * ajukan sewa button in pengajuan sewa
	 */
	public void clickOnAjukanSewaButton() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			appium.scrollToElementContainsByText("Ajukan Sewa");
		} else {
			scrollToAjukanSewa();
		}
		appium.tapOrClick(ajukanSewaButton);
	}

	/**
	 * term and condition checkbox
	 */
	public void clickOnTermsAndConditionCheckBox() {
		appium.tapOrClick(tncCheckbox);
	}

	/**
	 * term and condition checkbox
	 */
	public void clickOnSubmitToOwner() throws InterruptedException{
		appium.tapOrClick(submitButton);
		appium.hardWait(5);
	}

	/**
	 * Empty tenant name
	 */
	public void emptyTenantName() {
		appium.enterText(tenantName, "", true);
	}

	/**
	 * Input tenant name in booking confirmation/Submission page 2
	 * @param name input with target
	 * @param deleteChar boolean
	 */
	public void inputTenantName(String name, boolean deleteChar) {
		appium.enterText(tenantName, name, deleteChar);
	}

	/**
	 * Get validation error e.g Mohon masukkan nama
	 * @return
	 */
	public String getValidationErrorString() {
		return appium.getText(validationErrorText);
	}

	/**
	 * Get clickable status
	 * @return boolean
	 */
	public boolean getNextButtonStatus(String attribute) {
		return appium.getElementAttributeValue(nextButton, attribute).equals("true");
	}

	/**
	 * Get female radio button attribute status/value
	 * @param attribute
	 * @return String data type
	 */
	public String getFemaleButtonStatus(String attribute) {
		return appium.getElementAttributeValue(femaleButton, attribute);
	}

	/**
	 * Get male radio button attribute status/value
	 * @param attribute
	 * @return String data type
	 */
	public String getMaleButtonStatus(String attribute) {
		return appium.getElementAttributeValue(maleButton, attribute);
	}

	/**
	 * Get allert message text e.g kost upras sembilanlapan, Halmahera Utara Halmahera bisa di tempati Pasangan Suami Istri
	 * @return String text
	 */
	public String getAllertMessageText() {
		return appium.getText(alertMessageText);
	}

	/**
	 * Increase or decrease renter amount
	 * @param amount input with number max 3
	 * @param increaseOrDecrease input with increase or decrease
	 * @throws InterruptedException
	 */
	public void changeRenterAmount(int amount, String increaseOrDecrease) throws InterruptedException {
		switch (increaseOrDecrease.toLowerCase()) {
			case "increase":
				for (int i = 0; i <= amount; i++) {
					appium.tapOrClick(incrementRenterButton);
					appium.hardWait(1);
				}
				break;
			case "decrease":
				for (int i = amount; i != 0; i--) {
					appium.tapOrClick(decrementRenterButton);
					appium.hardWait(1);
				}
				break;
			default:
				throw new Error("Must be increase or decrease");
		}
	}

	/**
	 * Check is counter renter view is visible
	 * @return boolean data type
	 */
	public boolean isCounterRenterViewVisible() {
		return appium.waitInCaseElementVisible(incrementRenterButton, 2) != null;
	}

	/**
	 * Scroll down until increment button from renter amount is visible
	 */
	public void scrollDownToRenterAmount() {
		while (!isCounterRenterViewVisible()) {
			appium.scroll(0.5, 0.8, 0.2, 2000);
		}
	}

	/**
	 * Get counter renter attribute value
	 * @param attribute
	 * @return
	 */
	public String getCounterRenterStatus(String attribute) {
		return appium.getElementAttributeValue(counterRenterBox, attribute);
	}

	/**
	 * Get married check box attribute value
	 * @param attribute element attribute e.g displayed, clickable etc
	 * @return text
	 */
	public String getMarriedCheckBoxStatus(String attribute) {
		return appium.getElementAttributeValue(marriedCheckBox, attribute);
	}

	/**
	 * Tap or click married checkbox if checked is false
	 */
	public void checkOnMarriedPeopleCheckBox() {
		if(getMarriedCheckBoxStatus("checked").equalsIgnoreCase("false")) {
			appium.tapOrClick(marriedCheckBox);
		}
	}

	/**
	 * Input tenant phone number
	 * @param phoneNumber phone number in format e.g 085777777777
	 */
	public void inputTenantPhoneNumber(String phoneNumber) {
		appium.enterText(tenantMobile, phoneNumber, true);
	}

	/**
	 * Edit tenant phone number
	 * @param phoneNumber phone number in format e.g 085777777777
	 */
	public void inputTenantPhoneNumberEdit(String phoneNumber) {
		appium.enterText(inputPhoneNumberEdit, phoneNumber, true);
	}

	/**
	 * Get phone number invalid alert text
	 * @return string e.g Nomor handphone tidak valid
	 */
	public String getInvalidPhoneNumberText() {
		return appium.getText(phoneNumberInvalidText);
	}

	/**
	 * Get phone number invalid alert text
	 * @return string e.g Nomor handphone tidak valid
	 */
	public String getInvalidPhoneNumberTextEdit() {
		return appium.getText(phoneNumberInvalidTextEdit);
	}

	/**
	 * Get title text of current booking confirmation page
	 * @return text e.g
	 */
	public String getPreviewBookingTitleText() {
		appium.waitTillElementIsVisible(titleText, 30);
		return appium.getText(titleText);
	}

	/**
	 * Click edit renter button to edit renter info
	 */
	public void clickOnRenterInfoEditButton() {
		appium.tapOrClick(editRenterInfoButton);
	}

	/**
	 * Click on save button to back to booking confirmation page
	 */
	public void clickOnSaveButton() {
		appium.scrollToElementByResourceId("submitReviewButton");
		appium.tapOrClick(saveButton);
	}

	/**
	 * Get phone number attribute value
	 * @param attribute
	 * @return string of element attribute value
	 */
	public String getPnoneNumberAttributeStatus(String attribute) {
		return appium.getElementAttributeValue(tenantMobile, attribute);
	}

	/**
	 * Get submit booking button attribute value
	 * @param attribute e.g checkable etc
	 * @return String value of attribute
	 */
	public String getSubmitBookingButtonStatus(String attribute) {
		appium.scrollToElementByText("Ajukan Sewa");
		return appium.getElementAttributeValue(continueButton,attribute);
	}

	/**
	 * get downpayment preview price
	 * @return String data type
	 */
	public String getDownPaymentPreviewPrice(String value) {
		String elementXpath = "";
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			elementXpath = "//android.widget.TextView[@text='" + value + "']";
		} else {
			elementXpath = "//XCUIElementTypeStaticText[@name='"+value+"']";
		}
		return appium.getText(By.xpath(elementXpath));
	}

	/**
	 * get first payment price
	 * @return int data type
	 */
	public String getFirstPaymentPrice(String paymentPrice) {
		String elementXpath = "";
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			elementXpath = "//android.widget.TextView[@text='" + paymentPrice + "']";
		} else {
			elementXpath = "(//XCUIElementTypeStaticText[@name='"+paymentPrice+"'])[1]";
		}
		return appium.getText(By.xpath(elementXpath));
	}

	/**
	 * Get title alert text
	 * @return String data type
	 */
	public String getTitlePercenDPText() {
			return appium.getText(percenDPText);
	}

	/**
	 * Get repayment/settlement price
	 * @return int data type
	 */
	public String getRepaymentPrice(String getPaymentPrice) {
		String elementXpath = "";
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			elementXpath = "//android.widget.TextView[@text='"+getPaymentPrice+"']";
		} else {
			elementXpath = "//XCUIElementTypeStaticText[@name='"+getPaymentPrice+"']";
		}
		return appium.getText(By.xpath(elementXpath));
	}

	/**
	 * Get total repayment price
	 * @return int data type
	 */
	public String getTotalRepaymentPrice(String totalRepaymentPrice) {
		String elementXpath = "";
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			elementXpath = "//android.widget.TextView[@text='" + totalRepaymentPrice + "']";
		}else {
			elementXpath = "//XCUIElementTypeStaticText[@name='"+totalRepaymentPrice+"']";
		}
		return appium.getText(By.xpath(elementXpath));
	}

	/**
	 * Scroll to ajukan sewa button
	 * @throws InterruptedException
	 */
	public void scrollToAjukanSewa() throws InterruptedException {
		int maxLoop = 0;
		do {
			appium.scroll(0.5, 0.8, 0.2, 2000);
			appium.hardWait(1);
			if (maxLoop == 3) {
				break;
			}
			maxLoop++;
		}
		while (!isPengajuanSewaVisible());
	}

	/**
	 * check is pengajuan sewa button is visible
	 * @return visible true otherwise false
	 */
	public boolean isPengajuanSewaVisible() {
			return appium.waitInCaseElementVisible(ajukanSewaButton, 2) != null;
		}
}
