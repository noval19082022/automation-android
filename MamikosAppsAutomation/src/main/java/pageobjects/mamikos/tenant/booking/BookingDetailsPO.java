package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.MobileBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import static io.appium.java_client.MobileCommand.prepareArguments;

public class BookingDetailsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;
    private SearchListingPO searchListing;
    private JavaHelpers java= new JavaHelpers();

    public BookingDetailsPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        searchListing = new SearchListingPO(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver, this);
    }

    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

//    @AndroidFindBy(id = "bookTextView")
//    private WebElement bookingButton;
    private By bookingButton = By.id("bookTextView");

    @iOSXCUITFindBy(accessibility = "ic back shadow")
    @AndroidFindBy(id = "com.git.mami.kos:id/backRoomImageView")
    private WebElement backBtn;

    @AndroidFindBy(id = "contactRoomAvailableTextView")
    private WebElement roomAvailability;

    @AndroidFindBy(id = "genderRoomTextView")
    private WebElement genderTypeLabel;

    @AndroidFindBy(id = "com.git.mami.kos:id/minimumPayTextView")
    private WebElement minPay;

    @AndroidFindBy(id = "detailFacilityButton")
    private WebElement detailFacility;

    @iOSXCUITFindBy(accessibility = "ic contact room")
    @AndroidFindBy(id = "chatOwnerButtonCv")
    private WebElement chatButton;

    @AndroidFindBy(id = "priceView")
    private WebElement pricePopupView;

    @AndroidFindBy(id = "roomPriceTextView")
    private WebElement priceTextView;

    @AndroidFindBy(id = "photoRoomDefaultImageView")
    private WebElement defaultPhotoOfRoom;

    @AndroidFindBy(id = "priceDetailTextView")
    private WebElement priceDetailsText;

    @AndroidFindBy(id = "minimumDetailPaymentTextView")
    private WebElement detailPaymentText;

    @AndroidFindBy(id = "electricPaymentTextView")
    private WebElement electricPaymentText;

    @AndroidFindBy(xpath = "//*[@text='Harga Sewa']")
    private WebElement priceTitleText;

    @AndroidFindBy(id = "seeAllPriceTextView")
    private WebElement seeAllPriceLinkText;

    @AndroidFindBy(id = "titleTextView")
    private WebElement validationTextForGender;

    @AndroidFindBy(id = "infoDailyPriceImageView")
    private WebElement dailyPriceInfoButton;

    @AndroidFindBy(id = "titleDailyPriceTextView")
    private WebElement dailyPriceText;

    private By dailyPriceTextBy = By.id("titleDailyPriceTextView");

    private By weeklyPriceText = By.id("weeklyPriceTextView");

    private By monthlyPriceText = By.id("monthlyPriceTextView");

    private By semiAnnuallyPriceText = By.id("semiannuallyPriceTextView");

    private By yearlyPriceText = By.xpath("//*[@text='Per Tahun']");

    private By loveButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("loveRoomIconCV"): By.xpath("(//XCUIElementTypeApplication[@name='mamikos']/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/descendant::XCUIElementTypeOther)[15]");

    @AndroidFindBy(id = "shareRoomImageView")
    private WebElement shareButton;

    @AndroidFindBy(id = "reportRoomTextView")
    private WebElement reportRoomLinkedText;

    @AndroidFindBy(id = "titleToolbarTextView")
    private WebElement reportPageHeading;

    @AndroidFindBy(xpath= "//android.widget.ImageButton[@content-desc='Navigate up']")
    private WebElement reportPageBackButton;

    @AndroidFindBy(id = "detailReviewButton")
    private WebElement seeAllReviewButton;

    @AndroidFindBy(id = "tvListReviewToolbarTitle")
    private WebElement reviewPageHeadingText;

    @AndroidFindBy(id = "ivListReviewBack")
    private WebElement reviewPageBackButton;

    @AndroidFindBy(id = "relatedRoomImageView")
    private WebElement otherKostRoom;

    @AndroidFindBy(id = "vacancyRelatedSalaryTextView")
    private WebElement vacancyRelateText;

    @AndroidFindBy(id = "sortReviewSpinner")
    private WebElement reviewShortingDropdown;

    @AndroidFindBy(id = "tv_reviews_time")
    private WebElement reviewDateText;

    @AndroidFindBy(id = "nameRoomTextView")
    private WebElement roomNameText;

    @AndroidFindBy(id = "content_preview_text")
    private WebElement mediaShareLink;

    @AndroidFindBy(id = "android:id/content")
    private WebElement bookingRestrictionPopUp;

    @AndroidFindBy(id = "com.git.mami.kos:id/cancelTextView")
    private WebElement cancelButtonRestrictionPopUp;

    @AndroidFindBy(id = "confirmTextView")
    private WebElement bookingButtonOnBookingDate;

    @AndroidFindBy(id = "ratingView")
    private  WebElement kostRatingLabel;

    @AndroidFindBy(id = "locationRoomTextView")
    private WebElement kostAreaLabel;

    @AndroidFindBy(id = "roomAvailibilityView")
    private WebElement roomAvailabilityLabel;

    @AndroidFindBy(id = "contactRoomUpdateTextView")
    private  WebElement kostLastUpdateText;

    //---------------- FTUE -------------------
    private By nextButton = By.id("nextButton");

    private By confirmButton = By.id("confirmTextView");

    //------------ OWNER PROFILE --------------
    @AndroidFindBy(id = "ownerNameTextView")
    private WebElement ownerNameText;

    @AndroidFindBy(id = "ownerNameTitleTextView")
    private WebElement ownerStatusText;

    @AndroidFindBy(id = "bookingActiveFromTextView")
    private WebElement ownerActiveBookingText;

    @AndroidFindBy(id = "ownerProfileImageView")
    private WebElement ownerProfilePicture;

    @AndroidFindBy(id = "learnBookingStatisticTextView")
    private WebElement detailStatisticButton;

    //---------- KOS RULE ------------

    @AndroidFindBy(id = "kosRuleTitleTextView")
    private WebElement kosRuleTitle;

    @AndroidFindBy(id = "facilityNameTextView")
    private WebElement kosRuleListText;

    @AndroidFindBy(id = "seeAllKosRuleTextView")
    private WebElement seeAllKosRuleButton;

    @AndroidFindBy(id = "kosRuleMainImageView")
    private WebElement kosRuleMainImg;

    @AndroidFindBy(id = "kosRuleImageView")
    private WebElement kosRuleImg;

    @AndroidFindBy(id = "backIcon")
    private WebElement kosRuleBackButton;

    @AndroidFindBy(id = "closeIconCv")
    private WebElement closeButtonImg;

    @AndroidFindBy(id = "photosCountTextView")
    private WebElement photoCountText;

    @AndroidFindBy(id = "photoRoomFullImageView")
    private WebElement photoRoomFull;

    @AndroidFindBy(id = "thumbnailBorderView")
    private WebElement photoThumbnail;

    //---------------- REPORT KOST ------------------

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[6]//android.widget.CheckBox[@resource-id='com.git.mami.kos:id/rb_choice']")
    private WebElement reportCategoryOther;

    @AndroidFindBy(id = "et_desc_report")
    private WebElement reportDesc;

    @AndroidFindBy(id = "btn_confirm_report")
    private WebElement reportSubmitButton;

    private By statusPengajuanButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("chatOwnerTextView"): By.id("Lihat status pengajuan");


    /**
     * Click on 'Booking' button
     *
     * @throws InterruptedException
     */
    public void clickOnBookingButton() throws InterruptedException {
        appium.hardWait(5);
        appium.tapOrClick(bookingButton);
        appium.hardWait(5);
    }

    /**
     * Set boarding start date and click on Booking Button on booking detail page
     * @param date Booking Date
     */
    public void setStartDate(String date) throws InterruptedException {
        appium.hardWait(1);
        By element = By.xpath("//android.widget.CheckedTextView[@content-desc='" + date + "'][@enabled='true']");
        appium.clickOn(element);
    }

    // tap back button in kosan details
    public void tapBack() {
        appium.tapOrClick(backBtn);
    }

    // tap listing title in kosan list / search result
    public void tapTitle() {
        searchListing.tapTitle();
    }

    /**
     * Get listing room minimum payment from top listing
     *
     * @param fromTop is number, from top to x
     */
    public List<String> getListMinPay(int fromTop) throws InterruptedException {
        List<String> MinPay = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            this.tapTitle();
            if(appium.waitInCaseElementVisible(nextButton,3) != null){
                this.dismissFTUE_screen();
            }
            appium.waitTillElementIsVisible(genderTypeLabel, 7);
            String MinPayment = this.getMinPay();
            MinPay.add(MinPayment);
            this.tapBack();
            appium.hardWait(2);
            appium.scroll(0.5, 0.90, 0.20, 2000);
        }
        //back to top
        for (int i = 0; i < fromTop; i++) {
            appium.scroll(0.5, 0.20, 0.80, 2000);
        }
        return MinPay;
    }

    // Get text of minimum payment from room details
    public String getMinPay() {
        return appium.getText(minPay);
    }

    // tap facility detail in kosan details
    public void tapFasDetail() throws InterruptedException {
        if(appium.waitInCaseElementVisible(nextButton,5) != null){
            this.dismissFTUE_screen();
        }
        appium.waitTillElementIsVisible(genderTypeLabel);
        appium.scrollToElementByResourceId("detailFacilityButton");
        appium.tapOrClick(detailFacility);
    }

    // Get text of room availability from room details
    public String getRoomAvail() {
        return appium.getText(roomAvailability);
    }

    /**
     * Get listing room availability from top listing
     * @param fromTop is number, from top to x
     */
    public List<String> getListRoomAvail(int fromTop) {
        List<String> listRoomAvail = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            this.tapTitle();
            String roomAvail = this.getRoomAvail();
            listRoomAvail.add(roomAvail);
            this.tapBack();
            searchListing.waitListingAppear();
            appium.scroll(0.5, 0.90, 0.20, 2000);
        }
        //back to top
        for (int i = 0; i < fromTop; i++) {
            appium.scroll(0.5, 0.20, 0.80, 2000);
        }
        return listRoomAvail;
    }
    /**
     * Check room availability present or not in kost details
     */
    public boolean checkRoomAvailability() {
        return appium.waitInCaseElementVisible(roomAvailability, 3) != null;
    }

    /**
     * Scroll to chat elements
     * Tap chat button
     */
    public void tapChat() throws InterruptedException {
        appium.scrollToElementByResourceId("chatOwnerButtonCv");
        if (appium.waitInCaseElementVisible(chatButton,3) == null) {
            appium.scroll(0.5, 0.60, 0.0, 2000);
        }
        appium.hardWait(1);
        appium.tapOrClick(chatButton);
    }

    /**
     * @return true if FTUE element nextButton is present otherwise false.
     * @throws InterruptedException
     */
    public Boolean isFTUE_screenPresent() throws InterruptedException{
        return appium.isElementPresent(nextButton) || appium.isElementPresent(confirmButton);
    }

    /**
     * Click on next button if it present in the screen.
     * @throws InterruptedException
     */
    public void dismissFTUE_screen() throws InterruptedException {
        do {
            appium.waitTillElementsCountIsEqualTo(nextButton, 1);
            appium.clickOn(nextButton);
        }
        while (isFTUE_screenPresent());
    }

    /**
     * Click On Price Text View
     */
    public void clickOnPriceText(){
        appium.tapOrClick(priceTextView);
    }

    /**
     * Get price value of kost
     * @return price value
     */
    public String getRoomPriceText(){
        return appium.getText(priceTextView);
    }

    /**
     * Get Price Details text from price popup
     * @return price details text
     */
    public String getPriceDetailsText(){
        return appium.getText(priceDetailsText);
    }

    /**
     * Get Details Payment text from price popup
     * @return detilsPaymentText
     */
    public String getDetailPaymentText(){
        return appium.getText(detailPaymentText);
    }

    /**
     * Get electric Payment Text from price popup
     * @return electricPayment
     */
    public String getElectricPaymentText(){
        return appium.getText(electricPaymentText);
    }

    /**
     * Get Price Title Text from price popup
     * @return  priceTitle
     */
    public String getPriceTitle(){
        return appium.getText(priceTitleText);
    }

    /**
     * Verify  Booking button is shown disable
     * @return booking button
     */
    public boolean isBookingButtonDisabled(){
        return appium.isElementEnabled(driver.findElement(bookingButton));
    }

    /**
     * Get Validation message
     * @return validation message
     */
    public String getValidationTextForGender(){
        return appium.getText(validationTextForGender);
    }

    /**
     * Click on drag view of price popup to dismiss
     */
    public void clickOnOutSideOfPricePopup(){
        appium.clickOn(defaultPhotoOfRoom);
    }

    /**
     * Check Price popup view is displayed
     * @return Price Popup
     */
    public boolean isPricePopupViewDisplayed(){
        return appium.isElementDisplayed(pricePopupView);
    }

    /**
     * Click on See All Price lInk Text
     */
    public void clickOnSeeAllPriceLinkText(){
        appium.tapOrClick(seeAllPriceLinkText);
    }

    /**
     * Click on Info button of Daily Price option
     */
    public void clickOnDailyPriceInfoButton(){
        appium.tapOrClick(dailyPriceInfoButton);
        appium.tapOrClick(dailyPriceInfoButton);
    }

    /**
     * Get Daily Price text
     * @return daily price text view
     */
    public String getDailyPriceText(){
        return appium.getText(dailyPriceText);
    }

    /**
     * Click on Love Button
     */
    public void clickOnLoveButton() throws InterruptedException {
        appium.clickOn(loveButton);
        appium.hardWait(3);
    }

    /**
     * Click on Share Button
     */
    public void clickOnShareButton(){
        appium.tapOrClick(shareButton);
    }

    /**
     * Click on Report Room Option
     */
    public void clickOnReportRoomLinkedText(){
        appium.scrollToElementByResourceId("reportRoomTextView");
        appium.clickOn(reportRoomLinkedText);
    }

    /**
     * Get Report kost page heading
     * @return report page heading
     */
    public String getReportPageHeading(){
        return appium.getText(reportPageHeading);
    }

    /**
     *  Click on back button of report kost page
     */
    public void clickOnReportPageBackButton(){
        appium.tapOrClick(reportPageBackButton);
    }

    /**
     * Check Booking button is displayed or Not
     * @return booking button
     */
    public boolean isBookingButtonDisplayed(){
        return driver.findElement(bookingButton).isDisplayed();
    }


    /**
     * Scroll down to other room section
     */
    public void navigateToRelatedRoomText(){
        appium.scroll(0.6, 0.80, 0.010, 2000);
        appium.scroll(0.6, 0.80, 0.10, 2000);
        appium.scroll(0.6, 0.80, 0.10, 2000);
    }

    /**
     *Check  Room name preset or not
     * @param roomName kost room name
     * @return Room Name
     */
    public boolean isRoomNamePresent(String roomName){
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + roomName + "']"), 10) != null;
    }

    /**
     * scroll to job vacancy section
     */
    public void scrollToJobVacancy(){
        appium.scroll(0.6, 0.80, 0.010, 2000);
        appium.scroll(0.6, 0.80, 0.10, 2000);
        appium.scroll(0.6, 0.80, 0.010, 2000);
    }

    /**
     * Check Job Vacancy name is present or not
     * @param jobName Job Name
     * @return Job Name
     */
    public boolean isJobVacancyPresent(String jobName){
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + jobName + "']"), 5) != null;
    }

    /**
     * Click on See All Review Button
     */
    public void clickOnSeeAllReviewButton() {
        appium.scroll(0.6, 0.80, 0.010, 2000);
        appium.scroll(0.100, 0.40, 0.005, 2000);
        appium.scroll(0.6, 0.80, 0.10, 2000);
        appium.tapOrClick(seeAllReviewButton);
    }

    /**
     * Get Review Page heading
     * @return review page heading
     */
    public String getReviewPageHeadingText(){
        return appium.getText(reviewPageHeadingText);
    }

    /**
     * Click on Back button of review page
     */
    public void clickOnReviewPageBackButton(){
        appium.tapOrClick(reviewPageBackButton);
    }

    /**
     * Click on Other kost Room
     */
    public void clickOnOtherKostRoom(){
        appium.tapOrClick(otherKostRoom);
    }

    /**
     * Click On vacancy related text
     */
    public void clickOnVacancyRelatedText(){
        appium.tapOrClick(vacancyRelateText);
    }

    /**
     * Click outer side of FTUE
     */
    public void clickOnOuterSideOfPopup(){
        appium.tapOrClick(defaultPhotoOfRoom);
    }

    /**
     * Click on Review shorting button
     */
    public void clickOnReviewFilterButton(){
        appium.tapOrClick(reviewShortingDropdown);
    }

    /**
     * Select review shorting option from dropdown
     * @param reviewShortingOption
     */
    public void selectReviewShortingOptionFromDropdown(String reviewShortingOption){
        String xpathLocator = "//*[@text='" + reviewShortingOption + "']";
        appium.waitTillElementIsClickable(By.xpath(xpathLocator)).click();
    }

    /**
     * get review date list
     * @param fromTop
     * @return review date
     */
    public List<String> getListOfReviewDate(int fromTop) {
        List<String> listupdate = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            String update1 = appium.getText(reviewDateText);
            listupdate.add(update1);
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return listupdate;
    }

    /**
     * Is per day tooptip dispalyed or not?
     * @return true or fasle
     * @throws IOException exception
     * @throws URISyntaxException exception
     */
    public boolean isPerDayTooltipDisplayed() throws IOException, URISyntaxException {

        CommandExecutionHelper.execute(driver,new AbstractMap.SimpleEntry<>("setSettings",prepareArguments("settings",prepareArguments("imageMatchThreshold",0.8))));
        return driver.findElement(MobileBy.image(java.getImageB64("/src/test/resources/testData/mamikos/" , "perDayTooltip.png"))).isDisplayed();
    }

    /**
     * Check if price list is present or not
     * @param price input with daily, weekly, monthly, semiannualy, yearly
     * @return true or false
     * @throws InterruptedException
     * @throws IllegalStateException
     */
    public boolean isPricePresent(String price) throws InterruptedException, IllegalStateException {
        boolean pricePresent;
        price = price.toLowerCase();
        switch (price) {
            case "daily":
                pricePresent = appium.isElementPresent(dailyPriceTextBy);
                break;
            case "weekly":
                pricePresent = appium.isElementPresent(weeklyPriceText);
                break;
            case "monthly":
                pricePresent = appium.isElementPresent(monthlyPriceText);
                break;
            case "semiannually":
                pricePresent = appium.isElementPresent(semiAnnuallyPriceText);
                break;
            case "yearly":
                pricePresent = appium.isElementPresent(yearlyPriceText);
                break;
            default:
                throw new IllegalStateException("Unexpected value: please input price with daily, weekly, monthly, semiannually, yearly. " + price + " is not in the price list");
        }
        return pricePresent;
    }

    /**
     * Get room name
     * @return string Room Name
     */
    public String getRoomNameText(){
        appium.waitInCaseElementVisible(roomNameText, 5);
        return appium.getText(roomNameText);
    }

    /**
     * Check media share is present
     * @return true / false
     */
    public boolean isMediaSharePresent() {
        return appium.waitInCaseElementVisible(mediaShareLink, 5) != null;
    }

    /**
     * Check booking restriction pop up present in viewport
     * @return boolean
     */
    public boolean isBookingRestrictionPopUpPresent() {
        return appium.waitTillElementIsVisible(bookingRestrictionPopUp) != null;
    }

    /**
     * Get text from booking restriction
     * @param index input with number 1,2,3,4 current index max is 4
     * @return
     */
    public String getBookingRestrictionText(String index) {
        String popUpString = "//*[@resource-id='android:id/custom']//android.widget.TextView["+ index +"]";
        By popUpStringBy = By.xpath(popUpString);
        return appium.getText(popUpStringBy);
    }

    /**
     * Tap on "Batal" button on booking restriction pop-up
     */
    public void tapOnCancelButtonBookingRestriction() {
        appium.tapOrClick(cancelButtonRestrictionPopUp);
    }

    /**
     * Get attribute value
     * @return String
     */
    public String getRentSubmitButtonStatus(String attributeName) {
        return appium.getElementAttributeValue(bookingButton, attributeName);
    }

    /**
     * Click on 'Booking' button on date section
     *
     * @throws InterruptedException
     */
    public void clickOnBookingButtonOnChooseDate() throws InterruptedException {
        appium.tapOrClick(bookingButtonOnBookingDate);
    }

    /**
     * Scroll to facilities
     * @param facilities
     */
    public void scrollToFacilities(String facilities) {
        while (!isFacilitiesPresent(facilities)){
            appium.scrollToElementByText(facilities);
        }
    }

    /**
     * Check if the element visible or not
     * @param fac
     * @return element status visible or not
     */
    public boolean isFacilitiesPresent(String fac){
        String xpathLocator = "//*[@text='" + fac + "']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 5) != null;
    }

    /**
     * Check Kost name present or not
     * @return true or false
     */
    public boolean isKostNamePresent(){
        return appium.waitTillElementIsVisible(roomNameText, 7) != null;
    }

    /**
     * Check Room gender label present or not
     * @return true or false
     */
    public boolean isRoomGenderPresent(){
        return appium.waitTillElementIsVisible(genderTypeLabel,7) != null;
    }

    /**
     * Check Kost rating label present or not
     * @return true or false
     */
    public boolean isKostRatingPresent(){
        return appium.waitTillElementIsVisible(kostRatingLabel, 7) !=null;
    }

    /**
     * Check Room availability present or not
     * @return true or false
     */
    public boolean isRoomAvailabilityPresent(){
        return appium.waitTillElementIsVisible(roomAvailabilityLabel, 7) != null;
    }

    /**
     * Check Room last update information present or not
     * @return true or false
     */
    public boolean isRoomLastUpdatePresent(){
        return appium.waitTillElementIsVisible(kostLastUpdateText, 7) != null;
    }

    /**
     * Scroll to owner profile section
     */
    public void scrollToOwnerSection(){
        appium.scrollToElementByResourceId("titleMapRoomView");
        appium.scroll(0.5, 0.60, 0.0, 2000);
    }

    /**
     * Check owner name is present or not
     * @return true or false
     */
    public boolean isOwnerNamePresent(){
        return appium.waitInCaseElementVisible(ownerNameText, 5)!=null;
    }

    /**
     * Check owner status is present or not
     * @return true or false
     */
    public boolean isOwnerStatusPresent(){
        return appium.waitInCaseElementVisible(ownerStatusText, 5)!=null;
    }

    /**
     * Check owner active booking info is present or not
     * @return true or false
     */
    public boolean isActiveBookingTextPresent(){
        return appium.waitInCaseElementVisible(ownerActiveBookingText, 5)!=null;
    }

    /**
     * Check owner profile picture is present or not
     * @return true or false
     */
    public boolean isOwnerPicturePresent(){
        return appium.waitInCaseElementVisible(ownerProfilePicture, 5)!=null;
    }

    /**
     * Check detail statistic button is present or not
     * @return true or false
     */
    public boolean isDetailStatisticButtonPresent(){
        return appium.waitInCaseElementVisible(detailStatisticButton, 5)!=null;
    }

    /**
     * Check chat button present or not
     * @return true or false
     */
    public boolean isChatButtonPresent(){
        return appium.waitTillElementIsVisible(chatButton, 5) != null;
    }

    /**
     * Scroll to kos rule section
     */
    public void scrollToKosRuleSection() {
        appium.scrollToElementByResourceId("kosRuleTitleTextView");
    }

    /**
     * Check kos rule list present or not
     * @return true or false
     */
    public boolean isKosRuleListPresent() {
        if (appium.waitInCaseElementVisible(kosRuleListText, 3) == null) {
            appium.scrollToElementByResourceId("kosRuleImageView");
        }
        return appium.waitInCaseElementVisible(kosRuleListText, 3) != null;
    }

    /**
     * Click see all kos rule button
     */
    public void clickSeeAllKosRuleButton() {
        appium.clickOn(seeAllKosRuleButton);
    }

    /**
     * Check kos rule main image present or not
     * @return true or false
     */
    public boolean isKosRuleMainImagePresent() {
        return appium.waitInCaseElementVisible(kosRuleMainImg, 3) != null;
    }

    /**
     * Check kos rule image present or not
     * @return true or false
     */
    public boolean isKosRuleImagePresent() {
        return appium.waitInCaseElementVisible(kosRuleImg, 3) != null;
    }

    /**
     * Click back button from kos rule
     */
    public void backToDetailPageFromKosRule() {
        appium.clickOn(kosRuleBackButton);
    }

    /**
     * Click back button from kos rule
     */
    public void clickKosRuleImg() {
        appium.clickOn(kosRuleMainImg);
    }

    /**
     * Check close icon present or not
     * @return true or false
     */
    public boolean isCloseButtonPresent() {
        return appium.waitInCaseElementVisible(closeButtonImg, 3) != null;
    }

    /**
     * Check photo count present or not
     * @return true or false
     */
    public boolean isPhotosCountPresent() {
        return appium.waitInCaseElementVisible(photoCountText, 3) != null;
    }

    /**
     * Check photo full room present or not
     * @return true or false
     */
    public boolean isPhotoRoomFullPresent() {
        return appium.waitInCaseElementVisible(photoRoomFull, 3) != null;
    }

    /**
     * Check photo thumbnail present or not
     * @return true or false
     */
    public boolean isPhotoThumbnailPresent() {
        return appium.waitInCaseElementVisible(photoThumbnail, 3) != null;
    }

    /**
     * Swipe left photo
     */
    public void swipeLeftKosRuleImg() {
        appium.scrollHorizontal(0.50, 0.80, 0.20, 2000);
    }

    /**
     * Swipe right photo
     */
    public void swipeRightKosRuleImg() {
        appium.scrollHorizontal(0.50, 0.20, 0.80, 2000);
    }

    /**
     * Select report category
     */
    public void selectReportCategory() {
        appium.clickOn(reportCategoryOther);
    }

    /**
     * Set Text Report Desc
     * @param text
     */
    public void inputTextReportDesc(String text) {
        appium.clickOn(reportDesc);
        appium.enterText(reportDesc, text, false);
    }

    /**
     * Click submit report
     */
    public void clickSubmitReport() {
        appium.clickOn(reportSubmitButton);
    }

    /**
     * Check text FTUE Booking Benefit 1 is present
     * @return true or false
     */
    public boolean isFTUEText1Present() {
        String xpathLocator = "//*[@text='Hanya butuh 4 langkah untuk booking kos tanpa harus ketemu pemilik kos.']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 3) != null;
    }

    /**
     * Check text FTUE Booking Benefit 2 is present
     * @return true or false
     */
    public boolean isFTUEText2Present() {
        String xpathLocator = "//*[@text='Udah ngerasa ada kosan yang cocok? Klik “Ajukan Sewa” buat mengajukan sewa ke pemilik kos.']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 3) != null;
    }

    /**
     * Check text FTUE Booking Benefit 3 is present
     * @return true or false
     */
    public boolean isFTUEText3Present() {
        String xpathLocator = "//*[@text='Bantu pemilik kos mengenalimu. Isi data diri dan perkiraan kapan kamu mulai sewa kos.']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 3) != null;
    }

    /**
     * Check text FTUE Booking Benefit 4 is present
     * @return true or false
     */
    public boolean isFTUEText4Present() {
        String xpathLocator = "//*[@text='Setelah ajukan sewa, kamu bakal dikabari pemilik kos apakah kamu bisa ngekos di tempatnya.']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 3) != null;
    }

    /**
     * Check text FTUE Booking Benefit 5 is present
     * @return true or false
     */
    public boolean isFTUEText5Present() {
        String xpathLocator = "//*[@text='Hore! Pemilik kos siap menerimamu! Segera lakukan pembayaran sebelum kedaluwarsa ya.']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 3) != null;
    }

    /**
     * Check text FTUE Booking Benefit 6 is present
     * @return true or false
     */
    public boolean isFTUEText6Present() {
        String xpathLocator = "//*[@text='Setelah pembayaran diterima pemilik, kamar kos sudah siap kamu huni.']";
        By element = By.xpath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 3) != null;
    }

    /**
     * Click on lihat status pengajuan button
     */
    public void clickOnLihatStatusPengajuanButton() throws InterruptedException{
        appium.hardWait(3);
        appium.tapOrClick(statusPengajuanButton);
    }


}
