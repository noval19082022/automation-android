package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.JavaHelpers;

import java.text.ParseException;
import java.util.List;

public class BookingFormFillPO {
    AppiumHelpers appium;
    private final JavaHelpers java;
    AppiumDriver<WebElement> driver;

    public BookingFormFillPO(AppiumDriver<WebElement> driver) {
        java = new JavaHelpers();
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver, this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "titleToolbarTextView")
    private WebElement bookingPageHeader;

    @AndroidFindBy(id = "com.git.mami.kos:id/dateEditText")
    private WebElement dateTextBox;

    @AndroidFindBy(id = "rentSpinner")
    private WebElement rentTypeDropdown;

    @AndroidFindBy(id = "com.git.mami.kos:id/incrementImageView")
    private WebElement increaseDurationButton;

//    @AndroidFindBy(id = "com.git.mami.kos:id/nextButton")
//    private WebElement continueButton;
    private By continueButton = By.id("bookingBtnCv");

    @AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc='mamikos']")
    private WebElement backBtn;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.ScrollView/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\n")
    private WebElement fullName;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.widget.ScrollView/android.view.ViewGroup/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText\n")
    private WebElement phoneNumber;

    @AndroidFindBy(id = "saveTextView")
    private WebElement saveBtn;

    @AndroidFindBy(id = "messageTextView")
    private WebElement popUpMessage;

    @AndroidFindBy(id = "okeTextView")
    private WebElement okButton;

    @AndroidFindBy(id = "cancelButton")
    private WebElement bookingFormCancelButton;

    @AndroidFindBy(id = "subTitleTextView")
    private WebElement subTitleTextOfRentType;

    @AndroidFindBy(id = "valueTextView")
    private WebElement rentCountValueText;

    @AndroidFindBy(id = "decrementImageView")
    private WebElement decreaseRentDurationButton;

    @AndroidFindBy(id = "dropDownDurationTextView")
    private WebElement rentDurationText;

    @AndroidFindBy(xpath = "//*[contains(@text,'Per Bulan')]//ancestor::*[contains(@resource-id,'mainPriceView')]//*[contains(@resource-id,'displayPriceTextView')]")
    private WebElement kostRentValueForPerBulan;

    @AndroidFindBy(id = "subTitleTextView")
    private WebElement rentPaidDurationText;

    private By inputFormSection = By.id("inputBookingView");

    @AndroidFindBy(id = "valueTextView")
    private WebElement rentalDurationText;

    @AndroidFindBy(id = "com.git.mami.kos:id/design_bottom_sheet")
    private WebElement datePickerForm;

    @AndroidFindBy(id = "com.git.mami.kos:id/confirmTextView")
    private WebElement bookingButton;

    private By downPaymentPrice = By.id("mandatoryDpPriceValue");
    private By ringkasanBookingTitle = By.xpath("//*[@text='Ringkasan Pengajuan Sewa']");

    /**
     * Check date picker is present or not
     * @return true if date picker is present
     */
    public boolean isDatePickerPresent() {
        return appium.waitInCaseElementVisible(datePickerForm, 2) != null;
    }

    /**
     * Check if selected date are duplicate
     * @param date Booking Date
     * @return list of web element
     */
    public List<WebElement> checkDuplicateDate(String date) {
        return appium.waitTillAllElementsAreLocated(By.xpath("//android.widget.CheckedTextView[@content-desc=\""+ date +"\"]"));
    }

    private By datePickerSelector(String date) throws ParseException {
        int selectedDatePresent = checkDuplicateDate(date).size();
        int currentDate = Integer.parseInt(java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0));
        By selectedDate;
        if (selectedDatePresent == 2) {
            selectedDate = currentDate >= 28
                    ? By.xpath("//android.widget.CheckedTextView[@content-desc=\""+ date +"\"][2]")
                    : By.xpath("//android.widget.CheckedTextView[@content-desc=\""+ date +"\"][1]");
        }
        else {
            selectedDate = By.xpath("//android.widget.CheckedTextView[@content-desc=\""+ date +"\"]");
        }
        return selectedDate;
    }

    /**
     * Fill out from and click on Continue button
     *
     * @param date         Booking Date
     * @throws InterruptedException
     */
    public void setDate(String date) throws ParseException {
        if (isDatePickerPresent()) {
            appium.clickOn(datePickerSelector(date));
            appium.clickOn(bookingButton);
        }
        else {
            appium.tapOrClick(dateTextBox);
            appium.clickOn(datePickerSelector(date));
        }
    }


    /**
     * Fill out from and click on Continue button
     * @param date Booking Date
     */
    public void setDateToday(String date) throws InterruptedException {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.scrollToElementByResourceId("dateEditText");
        appium.hardWait(1);
        appium.tapOrClick(dateTextBox);
        appium.hardWait(1);
        By element = By.xpath("//android.widget.CheckedTextView[@content-desc='" + date + "'][@checked='true']");
        appium.clickOn(element);
    }

	/**
	 * Click Back button
	 */
	public void tapBackBtn() {
		appium.tapOrClick(backBtn);
	}
    /** Check Full Name field, compare with given text
     * @param name is text to compare
     */
    public boolean checkFullName(String name) {
        return appium.getText(fullName).equals(name);
    }
    /** Check mobile handphone field, compare with given text
     * @param phone is text to compare
     */
    public boolean checkPhoneNo(String phone) {
        return appium.getText(phoneNumber).equals(phone);
    }
    /**
     * Click Save / "Simpan" button
     */
    public void tapSave() {
        appium.tapOrClick(saveBtn);
    }
    /** Validate pop Up message, compare with text given
     * @param message is text to compare
     */
    public boolean checkPopupMsg(String message) {
        return appium.getText(popUpMessage).equals(message);
    }
    /**
     * Click OK button in Validation pop up
     */
    public void tapOk() {
        appium.tapOrClick(okButton);
    }
    /**
     * Clear fullname field
     */
    public void clearName() {
        appium.tapOrClick(fullName);
        appium.enterText(fullName, "", true);
        appium.hideKeyboard();
    }
    /**
     * Insert text in phone number field
     */
    public void enterPhoneNo(String phoneNo) {
        appium.tapOrClick(phoneNumber);
        appium.enterText(phoneNumber, phoneNo, true);
        appium.hideKeyboard();
    }

    /**
     * Tap on + icon of Rent Duration
     */
    public void increaserentDuration(){
        appium.tapOrClick(increaseDurationButton);
    }

    /**
     * Click On Continue Button
     */
    public void clickOnContinueButton() throws InterruptedException {
        appium.hardWait(4);
      //  appium.scrollToElementByResourceId("com.git.mami.kos:id/bookingBtnCv");
        appium.scrollToElementByText("FAQ tentang pengajuan sewa");
        appium.scroll(0.5, 0.8, 0.4, 2000);
        appium.clickOn(continueButton);
        appium.hardWait(2);
    }

    /**
     * Get continue button attribute value
     * @param attribute
     * @return string data type
     */
    public String getContinueButtonStatus(String attribute) {
        return appium.getElementAttributeValue(continueButton, attribute);
    }

    /**
     * Tap On Cancel Button of Booking Form
     */
    public void clickOnCancelButtonOFBookingForm(){
        appium.tapOrClick(bookingFormCancelButton);
    }

    /**
     * Get Page Header
     * @return booking page header
     */
    public String getBookingPageHeaderText(){
        return appium.getText(bookingPageHeader);
    }


    /**
     * Get Rent value from submission page
     * @return rent value
     */
    public String getRentPriceValueForPerBulanFromSubmissionPage(){
        return appium.getText(kostRentValueForPerBulan);
    }

    /**
     * Check daily rent option is present or not
     * @return true or false
     */
    public boolean isRentTypePresentOnSubmissionPage(){
        String rentType = "//*[@text='Per Hari']";
        return appium.waitInCaseElementVisible(By.xpath(rentType), 3)!=null;
    }

    /**
     * Click on rent type option
     * @param rentType rent type
     */
    public void selectRentType(String rentType) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//*[@text='" + rentType +"']"));
        appium.waitTillElementIsClickable(By.xpath("//*[@text='" + rentType +"']"));
        appium.hardWait(3);
        appium.tapOrClick(element);
    }

    /**
     * Get sub title text of rent type
     * @return subtitle text
     */
    public String getSubTitleTextOfRentType(){
        return appium.getText(subTitleTextOfRentType);
    }

    /**
     * Get rent count value
     * @return rent count
     */
    public String getRentCountValueText(){
        return appium.getText(rentCountValueText);
    }

    /**
     * Click on increase rent duration button maximum time
     */
    public void clickOnIncreaseRentButtonMaximumTime(){
        do {
            appium.tapOrClick(increaseDurationButton);
        }
        while (increaseDurationButton.isEnabled());
    }

    /**
     * Check increase rent duration button is enable or not
     * @return true or false
     */
    public boolean isIncreaseRentDurationButtonEnabled(){
        return increaseDurationButton.isEnabled();
    }

    /**
     * Click on decrease rent duration button
     */
    public void clickOnDecreaseRentDurationButton(){
        appium.tapOrClick(decreaseRentDurationButton);
    }

    /**
     * Check decrease rent duration button is enable or not
     * @return true or false
     */
    public boolean isDecreaseRentDurationButtonEnabled(){
        return decreaseRentDurationButton.isEnabled();
    }

    /**
     * Get rent duration text
     * @return rent duration text
     */
    public String getRentDurationText(){
        return appium.getText(rentDurationText);
    }

    /**
     * Get rent paid duration text e.g Dibayar sebulan sekali
     * @return rent paid duration text
     */
    public String getRentPaidDurationText(String duration){
        return appium.getText(By.xpath("//*[contains(@text, '"+duration+"')]"));
    }

    /**
     * Click on each rental price duration e.g Per Bulan, Per Minggu etc
     * @param duration rental price duration
     */
    public void clickOnEachRentalPrice(String duration) {
       appium.clickOn(By.xpath("//*[contains(@text, '"+duration+"')]"));
    }

    /**
     * Check if is in booking form section by look for the id
     * @return true if booking input view id present otherwise false
     * @throws InterruptedException
     */
    public boolean isInBookingSectionBookingInputIDPresent() throws InterruptedException {
        return appium.isElementPresent(inputFormSection);
    }
    /**
     * Get Rental Duration Text e.g 1 Bulan, 1 Minggu, 3 Bulan etc
     * @return rental duration text
     */
    public String getRentalDurationText(){
        return appium.getText(rentalDurationText);
    }

    /**
     * get mandatory down payment price
     * @return int data type
     */
    public int getMandatoryDownPaymentPrice() {
        return JavaHelpers.extractNumber(appium.getText(downPaymentPrice));
    }

    /**
     * check is ringkasan booking visible
     * @return visible true otherwise false
     */
    public boolean isInRingkasanBooking() {
        return appium.waitInCaseElementVisible(ringkasanBookingTitle, 4) != null;
    }
}
