package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

public class BookingHistoryPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public BookingHistoryPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */

	private By status = Constants.MOBILE_OS == Platform.ANDROID ? By.id("bookingStatusTextView"): By.id("Butuh Pembayaran");

	private By roomName = Constants.MOBILE_OS == Platform.ANDROID ? By.id("kostBookingTextView"): By.xpath("//*[@name='Butuh Pembayaran']/following::XCUIElementTypeStaticText[1]");

	@AndroidFindBy(id="bookingDateTextView")
	private WebElement date;

	@AndroidFindBy(id="durationTextView")
	private WebElement duration;

	@AndroidFindBy(id="paymentExpiredDateTextView")
	private WebElement paymentExpiryText;

	@AndroidFindBy(id="actionBookingButton")
	private WebElement nextActionButton;

	@AndroidFindBy(xpath ="(//android.widget.TextView[@resource-id='com.git.mami.kos:id/durationTextView'])[1]")
	private WebElement firstRentPeriod;

	@AndroidFindBy(id ="actionBookingButton")
	private WebElement checkInButton;

	@AndroidFindBy(id ="checkinButton")
	private WebElement checkInInstructionsButton;

	@AndroidFindBy(id ="confirmButton")
	private WebElement checkInConfirmationButton;

	@AndroidFindBy(id = "closeButton")
	private WebElement finishCheckInButton;

	@AndroidFindBy(id = "successInfoTextview")
	private WebElement successfullyInfoLabel;

//	@AndroidFindBy(xpath = "//android.widget.Button[@text='Bayar Sekarang']")
//	private WebElement payNowButton;
	private By payNowButton = By.xpath("//android.widget.Button[@text='Bayar Sekarang']");

	@AndroidFindBy(xpath="//android.widget.LinearLayout[1]//android.widget.TextView[@text='Tunggu Konfirmasi']")
	private WebElement waitingForConfirmationStatus;

	@AndroidFindBy(xpath = "//*[@resource-id='com.git.mami.kos:id/listBookingRecyclerView']/android.widget.LinearLayout[1]")
	private WebElement firstBookingList;

	private By chatPemilikButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Chat Pemilik']"): By.xpath("//XCUIElementTypeButton[@name='Chat Pemilik']");
	private By notNowButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath(""): By.xpath("//*[@name='Not Now']");

	private By imageKost = By.id("bookingImageView");

	private By invoiceView = Constants.MOBILE_OS == Platform.ANDROID ? By.id("invoiceView"): By.id("No. Invoice");

	private By invoiceNumberText = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='No. Invoice']/following-sibling::android.widget.TextView"): By.xpath("//*[@name='No. Invoice']/following::XCUIElementTypeOther[1]");

	private By totalPriceNumber = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Total Pembayaran']/following::android.widget.TextView[1]"): By.xpath("//*[@name='Total Pembayaran']/following::XCUIElementTypeOther[1]/XCUIElementTypeStaticText");

	private By Button = By.xpath("//android.widget.Button[@text='Bayar Sekarang']");

	private By bayarDisiniButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Bayar di sini']"): By.id("Bayar di sini");



	/** Get Status name
	 * @return Status
	 */
	public String getStatusName()
	{
		return appium.getText(status);
	}

	/** Get Room name
	 * @return  Room name
	 */
	public String getRoomName()
	{
		return appium.getText(roomName);
	}

	/** Get date
	 * @return  date
	 */
	public String getDate()
	{
		return appium.getText(date);
	}

	/** Get duration
	 * @return duration
	 */
	public String getDuration()
	{
		return appium.getText(duration);
	}

	/** Get payment expiration text
	 * @return text
	 */
	public String getPaymentExpirationText()
	{
		return appium.getText(paymentExpiryText);
	}

	/**
	 * Click on next action button
	 */
	public void clickOnNextActionButton()
	{
		appium.tapOrClick(nextActionButton);
	}

	/**
	 * Click on pay now button
	 */
	public void clickOnPayNowButton() {
		appium.tapOrClick(payNowButton);
	}

	/**
	 * Click on pay now button
	 */
	public String getRentPeriod() {
		appium.waitTillElementIsVisible(firstRentPeriod);
		return appium.getText(firstRentPeriod);
	}

	/**
	 * Click on check in button
	 * Click on check in button confirmation
	 */
	public void checkIn() throws InterruptedException {
		appium.waitTillElementIsClickable(checkInButton);
		appium.hardWait(2);
		appium.tapOrClick(checkInButton);
		appium.waitTillElementIsClickable(checkInInstructionsButton);
		appium.hardWait(2);
		appium.tapOrClick(checkInInstructionsButton);
		appium.tapOrClick(checkInConfirmationButton);
	}

	/**
	 * Verify check in successfully info is present
	 */
	public boolean checkInSuccessfullyInfoIsPresent() {
		return appium.isElementDisplayed(successfullyInfoLabel);
	}

	/**
	 * Click on finish check in button
	 */
	public void clickOnFinishCheckInButton() {
		appium.tapOrClick(finishCheckInButton);
	}

	/**
	 * Check if first list is in waiting confirmation stat
	 * @return boolean
	 */
	public boolean isBookingStatWaitingForConfirmation() {
		return appium.waitTillElementIsVisible(waitingForConfirmationStatus) != null;
	}

	/**
	 * Click on first booking list
	 */
	public void tapOnFirstBookingList() {
		appium.tapOrClick(firstBookingList);
	}

	/**
	 * Get status name list by index
	 * @param kostListIndex index start from zero
	 * @return String data type
	 */
	public String getStatusNameListIndex(int kostListIndex) throws InterruptedException{
		appium.hardWait(3);
		String kostStatus = "";
		if (Constants.MOBILE_OS == Platform.ANDROID){
			kostStatus = appium.waitTillAllElementsAreLocated(status).get(kostListIndex).getText();
		} else {
			appium.scroll(0.5, 0.8, 0.2, 2000);
			appium.scroll(0.5, 0.2, 0.8, 2000);
			kostStatus = appium.getText(status);
		}
		return kostStatus;
	}

	/**
	 * Get kost name list by index
	 * @param kostListIndex index start from zero
	 * @return String data type
	 */
	public String getKostNameListIndex(int kostListIndex) {
		String kostName = "";
		if (Constants.MOBILE_OS == Platform.ANDROID){
			kostName = appium.waitTillAllElementsAreLocated(roomName).get(kostListIndex).getText();
		}else {
			appium.scroll(0.5, 0.8, 0.2, 2000);
			appium.scroll(0.5, 0.2, 0.8, 2000);
			kostName = appium.getText(roomName);
		}
		return kostName;
	}

	/**
	 * click on booking list index
	 * @param index start from zero
	 */
	public void clickOnBookingListIndex(int index) {
		appium.waitTillAllElementsAreLocated(imageKost).get(index).click();
	}

	/**
	 * wait till invoice visible
	 */
	public void waitTillInvoiceVisible() throws InterruptedException{
		appium.hardWait(5);
		appium.waitInCaseElementVisible(invoiceView, 10);
	}

	/**
	 * check if invoice number present/visible
	 * @return visible true otherwise false
	 */
	public boolean isInvoiceNumberPresent() {
		return appium.waitInCaseElementVisible(invoiceNumberText, 10) != null;
	}

	/**
	 * get invoice total price
	 * @return int data type
	 */
	public int invoiceTotalPrice() {
		return JavaHelpers.extractNumber(appium.getText(totalPriceNumber));
	}

	/**
	 * tap on element with text
	 * @param text kost name
	 */
	public void tapOnKostBookingListHistory(String text) {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			By tapEl = By.xpath("//*[@text='"+text+"']");
			appium.clickOn(tapEl);
		}else{
			String riwayatTab = "//XCUIElementTypeButton[@name='Riwayat']";
			MobileElement element3 = (MobileElement) driver.findElementByXPath(riwayatTab);
			appium.tapOrClick(element3);
			appium.tap(0,139);
		}
	}

	/**
	 * Click on chat pemilik
	 */
	public void tapOnChatPemilik() throws InterruptedException{
		appium.hardWait(3);
		if (Constants.MOBILE_OS == Platform.IOS){
			appium.tapOrClick(notNowButton);
		}
		appium.scroll(0.5, 0.8, 0.2, 2000);
		appium.tapOrClick(chatPemilikButton);
	}

	/**
	 * Check if bayar disini button is present
	 * @return true or false
	 */
	public boolean isBayarDisiniButtonPresent() {
		return appium.waitInCaseElementVisible(bayarDisiniButton, 2) != null;
	}

	/**
	 * Scroll to bayar disini button
	 */
	public void scrollToBayarDisiniButton() {
		int i = 0;
		do {
			appium.scroll(0.5, 0.70, 0.30, 2000);
			if(i == 10) {
				break;
			}
			i++;
		}
		while (!isBayarDisiniButtonPresent());
	}

	/**
	 * tap or click on bayar disini button
	 */
	public void tapsOnBayarDisiniButton() {
		appium.waitInCaseElementVisible(bayarDisiniButton,5);
		appium.tapOrClick(bayarDisiniButton);
	}
}
