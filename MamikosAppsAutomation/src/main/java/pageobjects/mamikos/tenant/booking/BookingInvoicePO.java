package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class BookingInvoicePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public BookingInvoicePO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver,this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(xpath = "(//android.view.View[@content-desc='Masukkan'])[1]")
    private WebElement inputVoucherButton;

    @AndroidFindBy(xpath = "//*[@text='Hapus']")
    private WebElement removeButton;

    @AndroidFindBy(xpath = "//android.widget.EditText")
    private WebElement voucherCodeTextBox;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Pakai']")
    private WebElement useButton;

    @AndroidFindBy(xpath = "(//android.view.View[contains(@text,'Total Pembayaran')]/following-sibling::android.view.View)[1]")
    private WebElement totalRemainingPayment;

    @AndroidFindBy(xpath = "//*[@text='Pembayaran']")
    private WebElement invoiceTitle;

    @AndroidFindBy(xpath = "//*[@class='android.widget.EditText']/following::android.view.View[1]")
    private WebElement voucherAlert;

    @AndroidFindBy(xpath = "android.view.View[contains(text(), 'Voucher Dihapus')]")
    private WebElement removedVoucherToast;

    @AndroidFindBy(xpath = "//android.widget.Image[@text='success-round-glyph']")
    private WebElement successIcon;

    /** click input voucher button for display edit text input voucher
     */
    public void accessVoucherForm() throws InterruptedException {
        appium.hardWait(2);
        appium.tapOrClick(inputVoucherButton);
        appium.tapOrClick(inputVoucherButton);
    }

    /**
     * Click on Remove button to Remove Voucher from Invoice
     */
    public void clickOnRemoveVoucherButton() throws InterruptedException {
        appium.clickOn(removeButton);
    }

    /** Enter text to edit text input voucher code
     * @param voucherCode is voucher code
     */
    public void enterVoucherCode(String voucherCode) throws InterruptedException {
        appium.enterText(voucherCodeTextBox, voucherCode, true);
        appium.hardWait(2);
    }

    /** Click button use for use voucher
     */
    public void clickOnUseButton() {
        appium.tapOrClick(useButton);
    }

    /** Get remaining payment
     * @return remaining payment
     */
    public String getRemainingPayment() throws InterruptedException {
        appium.waitInCaseElementVisible(invoiceTitle,10);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.hardWait(1);
        return appium.getText(totalRemainingPayment);
    }

    /**
     * Get Text of Voucher Alert Message
     *
     * @return text of voucher alert message
     */
    public String getVoucherAlertMessage() throws InterruptedException {
        appium.hardWait(1);
        return appium.getText(voucherAlert);
    }

    /**
     * Verify Sucess Icon is Displayed
     *
     * @return boolean
     */
    public Boolean isSuccessIconDisplayed() {
        return appium.waitInCaseElementVisible(successIcon, 5)!=null;
    }

}