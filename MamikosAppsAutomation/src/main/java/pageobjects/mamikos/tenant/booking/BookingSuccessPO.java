package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;

public class BookingSuccessPO 
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public BookingSuccessPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(driver,this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 

//	    @AndroidFindBy(id="seeBookingButton")
//	    private WebElement viewBookingButton;
		private By viewBookingButton = By.xpath("seeBookingButton");
	    
	    /**
	     * Click on 'View Booking' button
	     */
	    public void clickOnViewBookingButton()
	    {
	    	appium.tapOrClick(viewBookingButton);
	    } 	 
}
