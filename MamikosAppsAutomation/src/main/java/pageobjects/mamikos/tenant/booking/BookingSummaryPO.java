package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class BookingSummaryPO
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public BookingSummaryPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 

	private By kostName = By.id("titleRoomTextView");

	private By kostGenderType = By.id("roomGenderTextView");

	private By kostPrice = By.id("priceTextView");

	private By checkInDate = By.id("checkInDateTextView");

	private By checkOutDate = By.id("checkoutDateTextView");

	private By rentDuration = By.id("dropDownDurationTextView");

	private By renterName = By.id("fullNameTextView");

	/**
	 * Check if kost name present
	 * @return true of false based on kost name present in the screen
	 * @throws InterruptedException
	 */
	public boolean isKostNamePresent() throws InterruptedException {
		return appium.isElementPresent(kostName);
	}

	/**
	 * Check if kost gender present
	 * @return true if kost gender present otherwise false
	 * @throws InterruptedException
	 */
	public boolean isKostGenderPresent() throws InterruptedException {
		return appium.isElementPresent(kostGenderType);
	}

	/**
	 * Check if kost price is present
	 * @return
	 * @throws InterruptedException
	 */
	public boolean isKostPricePresent() throws InterruptedException	 {
		return appium.isElementPresent(kostPrice);
	}

	/**
	 * Get check in date text
	 * @return String data type example "10 Jun 2020"
	 */
	public String getCheckInDateText() {
		return appium.getText(checkInDate);
	}

	/**
	 * Get check out date text
	 * @return String data type example "10 Okt 2020"
	 */
	public String getCheckOutDateText() {
		return appium.getText(checkOutDate);
	}

	/**
	 * Get rent duration text
	 * @return String data type example "4 Bulan"
	 */
	public String getRentDuration() {
		return appium.getText(rentDuration);
	}

	/**
	 * Check is rent duration present in view port
	 * @return true if rent duration present otherwise false
	 * @throws InterruptedException
	 */
	public boolean isRentDurationPresent() throws InterruptedException {
		return appium.isElementPresent(rentDuration);
	}

	/**
	 * Check renter name present
	 * @return true if renter name present otherwise false
	 * @throws InterruptedException
	 */
	public boolean isTenantNamePresent() throws InterruptedException {
		return appium.isElementPresent(renterName);
	}
}
