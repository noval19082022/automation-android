package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class DetailBookingPO
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public DetailBookingPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 

	private By cancelBookingButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("com.git.mami.kos:id/cancelBookingTextView"): By.xpath("//XCUIElementTypeButton[@name='Batalkan Booking']");

	@AndroidFindBy(id = "com.git.mami.kos:id/cancelView")
	private WebElement cancelSection;

	private By yesCancelButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("com.git.mami.kos:id/toggleYesCancelButton"): By.id("Ya, Batalkan");

	@AndroidFindBy(id = "android:id/custom")
	private WebElement popUpBox;

	private By popUpMessage = Constants.MOBILE_OS == Platform.ANDROID ? By.id("tvSubtitle"): By.id("Booking Anda Berhasil dibatalkan");

	/**
	 * Tap on cancel booking button
	 */
	public void tapOnCancelBookingButton() {
		appium.tapOrClick(cancelBookingButton);
	}

	/**
	 * Check visibility of cancel section popup
	 * @return boolean
	 */
	public boolean isInCancelSection() {
		return appium.waitTillElementIsVisible(cancelSection) != null;
	}

	/**
	 * Tap on yes cancel button
	 */
	public void tapOnYesCancelButton() throws InterruptedException{
		appium.hardWait(3);
		appium.tapOrClick(yesCancelButton);
	}

	/**
	 * check and wait for pop up to be appear
	 * @return boolean
	 */
	public boolean isPopUpAppear() {
		return appium.waitTillElementIsVisible(popUpBox) != null;
	}

	/**
	 * Get pop up text
	 * @return String e.g "Booking berhasil dibatalkan"
	 */
	public String getPopUpText() throws InterruptedException {
		appium.hardWait(5);
		return appium.getText(popUpMessage);
	}

	/**
	 * Click icn back on detail booking page
	 */
	public void clickOnIcnBack(int x, int y) throws InterruptedException{
		appium.hardWait(3);
		appium.tap(x,y);
	}
}
