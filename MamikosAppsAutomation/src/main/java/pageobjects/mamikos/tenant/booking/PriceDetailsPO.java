package pageobjects.mamikos.tenant.booking;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class PriceDetailsPO
{
	AppiumHelpers appium;
	AppiumDriver<WebElement> driver;

    public PriceDetailsPO(AppiumDriver<WebElement> driver)
	{
    	this.driver = driver;
    	appium = new AppiumHelpers(driver);
    	
    	 //This initElements method will create all Android Elements
		PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	}
    
    /*
	 * All Android Elements are identified by @AndroidFindBy annotation
	 * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
	 */ 

	@FindBy(id="downPaymentTextView")
	private WebElement downPaymentText;

	@FindBy(id="priceDownPaymentTextView")
	private WebElement downPaymentPriceText;

	/**
	 * Get doy payment text
	 * @return String text e.g Uang Muka (DP)
	 */
	public String getDownPaymentText() {
		return appium.getText(downPaymentText);
	}

	/**
	 * Get down payment price text
	 * @return string text e.g Rp100.000
	 */
	public String getDownPaymentPriceText() {
		return appium.getText(downPaymentPriceText);
	}

	/**
	 * Tap on periode filter on price detail based on index given
	 * @param index input with integer e.g 1,2,3,4,5,6
	 */
	public void tapOnPeriodeFilter(int index) {
		String periodeFilterElement = "//*[@resource-id='com.git.mami.kos:id/filterItemView']["+index+"]";
		appium.tapOrClick(driver.findElementByXPath(periodeFilterElement));
	}
}
