package pageobjects.mamikos.tenant.chat;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import utilities.AppiumHelpers;
import utilities.Constants;

public class ContactAdsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ContactAdsPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='KIRIM PESAN']")
    @AndroidFindBy(id = "confirmSendMessageButton")
    private WebElement bookingBtn;

    @AndroidFindBy(id = "toolbarTextView")
    private WebElement pageTitle;

    @AndroidFindBy(className = "android.widget.RadioButton")
    private WebElement radioBtn;

    @iOSXCUITFindBy(accessibility = "Back")
    private WebElement backFromContactAds;

    private By selectedRadioButton = By.xpath("//android.widget.RadioButton[@checked=\"true\"]");
    
    // tap any option in radio button by text
    public void tapRadioBtn(String text) {
        String xpathLocator;
        if(Constants.MOBILE_OS== Platform.ANDROID) {
            xpathLocator = "//android.widget.RadioButton[contains(@text, '" + text + "')]";
        }
        else {
            xpathLocator = "//XCUIElementTypeStaticText[contains(@value, '" + text + "')]";
        }
        appium.scroll(0.5, 0.8, 0.2, 2000);
        appium.waitTillElementIsVisible(bookingBtn, 5);
        MobileElement element = (MobileElement) driver.findElementByXPath(xpathLocator);
        appium.tapOrClick(element);
    }

    // tap button "Booking"
    public void tapBooking() {
        appium.tapOrClick(bookingBtn);
    }

    // wait page load
    public void waitEnterPage() throws InterruptedException {
        appium.waitInCaseElementVisible(radioBtn, 11);
        appium.hardWait(2);
    }

    /**
     * click back button
     */
    public void clickBackFromContactAds() {
        appium.tapOrClick(backFromContactAds);
    }

    /**
     * Get selected radio button text
     * @return String data type example "Saya butuh cepat nih. Bisa booking sekarang?"
     */
    public String getSelectedRadioButtonChatText() {
        return appium.getText(selectedRadioButton);
    }

    /**
     * Get send message button text
     * @return String data type "BOOKING" || "KIRIM"
     */
    public String getSendMessageButtonText() {
        return appium.getText(bookingBtn);
    }

    /**
     * Check if send message button text is "BOOKING"
     * @return true if button text is "BOOKING" otherwise false
     */
    public boolean isSendMessageButtonContainBooking() {
        return getSendMessageButtonText().equalsIgnoreCase("booking");
    }
}

