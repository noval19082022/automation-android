package pageobjects.mamikos.tenant.detail;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

public class KostDetailsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;
    JavaHelpers java = new JavaHelpers();

    public KostDetailsPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        PageFactory.initElements(driver, this);
    }

    private By seeAllPriceButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("seeAllPriceButtonCV"): By.xpath("//XCUIElementTypeButton[@name='Estimasi pembayaran']");

    private By textDownPaymentPrice = Constants.MOBILE_OS == Platform.ANDROID ? By.id("paymentPreviewDpValue"): By.xpath("//XCUIElementTypeStaticText[@name='Uang Muka (DP)']/following-sibling::XCUIElementTypeStaticText");

    private By btnTutup = Constants.MOBILE_OS == Platform.ANDROID ? By.id("dialogButton"): By.id("Tutup");

    private By textRepaymentPrice = Constants.MOBILE_OS == Platform.ANDROID ? By.id("paymentPreviewRemainingValue"): By.xpath("//XCUIElementTypeStaticText[@name='Pelunasan']/following-sibling::XCUIElementTypeStaticText");

    private By btnAjukanSewa = Constants.MOBILE_OS == Platform.ANDROID ? By.id("activityButtonCV"): By.xpath("(//XCUIElementTypeStaticText[@name='Ajukan Sewa'])[3]");

    private By btnBooking = Constants.MOBILE_OS == Platform.ANDROID ? By.id("confirmationButtonView"): By.id("Ajukan sewa");

    private By pembayaranPageTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//*[@text='Pembayaran']/following::android.widget.TextView)[1]"): By.id("Pembayaran");

    private By rincianPembayaranTitle = By.xpath("//*[@name='Total pembayaran  pertama']");

    private By rincianPelunasanTitle = By.id("Total Pelunasan");

    private By dpTitle = By.id("Bisa bayar DP (uang muka) dulu");

    private By rincianPembayaranDPTitle = By.xpath("//*[@name='Total pembayaran  pertama (DP)']");


    /**
     * Check if ketentuan DP sewa kost is present in viewport
     * @return true or false
     */
    public boolean isTitleDPPresent() {
        return appium.waitInCaseElementVisible(dpTitle, 1) != null;
    }

    /**
     * check is title present
     * @param titleText title text
     * @return visible true otherwise false
     */
    public boolean isTitle(String titleText) {
        By titleEl = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@resource-id='com.git.mami.kos:id/downPaymentTitle' and @text='"+titleText+"']"):
                By.id("Bisa bayar DP (uang muka) dulu");
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Bisa bayar DP (uang muka) dulu");
        } else {
            int i = 0;
            do {
                appium.scroll(0.5, 0.8, 0.2, 2000);
                if(i == 10) {
                    break;
                }
                i++;
            }
            while (!isTitleDPPresent());
        }
        return appium.waitInCaseElementVisible(titleEl, 5) != null;
    }

    /**
     * check is subtitle present
     * @param subtitleText subtitle text
     * @return visible true otherwise false
     */
    public boolean isSubtitle(String subtitleText) {
        By subtitleEl = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@resource-id='com.git.mami.kos:id/percentageDownPaymentTitle' and @text='"+subtitleText+"']"):
                By.id("Biaya DP adalah 20% dari harga sewa yang dipilih.");
        return appium.waitInCaseElementVisible(subtitleEl, 10) != null;
    }

    /**
     * tap on payment estimation
     */
    public void tapOnPaymentEstimation() {
        appium.tapOrClick(seeAllPriceButton);
    }

    /**
     * is element with text visible
     * @param text desired text
     * @return visible true otherwise false
     */
    public boolean isElementWithText(String text) {
        String textEl = "";
        if (Constants.MOBILE_OS == Platform.ANDROID ) {
            textEl = "//*[@text='" + text + "']";
        }else{
            textEl = "//*[@name='" + text + "']";
        }
        MobileElement element = (MobileElement) driver.findElementByXPath(textEl);
        return appium.waitInCaseElementVisible(element, 5) != null;
    }

    /**
     * tap on element with text
     * @param text desired text for element
     */
    public void tapOnElemenetWithText(String text) {
        String menuOnProfile = "";
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            menuOnProfile = "//*[@text='"+text+"']";
        }else{
            menuOnProfile = "//*[@value='"+text+"']";
        }
        MobileElement element = (MobileElement) driver.findElementByXPath(menuOnProfile);
        appium.tapOrClick(element);
    }

    /**
     * Get downpayment price
     * @return int data type
     */
    public int getDownPaymentPrice() {
        return JavaHelpers.extractNumber(appium.getText(textDownPaymentPrice));
    }

    /**
     * tap or click on close button payment detail
     */
    public void tapsOnCloseButtonPaymentDetail() {
        appium.tapOrClick(btnTutup);
    }

    /**
     * Get repayment/settlement price
     * @return int data type
     */
    public int getRepaymentPrice() {
        return JavaHelpers.extractNumber(appium.getText(textRepaymentPrice));
    }

    /**
     * Tap on ajukan sewa button
     */
    public void tapOnAjukanSewa() throws InterruptedException {
        appium.hardWait(3);
        appium.waitInCaseElementVisible(btnAjukanSewa,10);
        appium.tapOrClick(btnAjukanSewa);
    }

    /**
     * choose booking date
     * @param date fill with today, or any desired day date number
     */
    public void chooseBookingDate(String date) {
        if (date.equalsIgnoreCase("today")) {
            date = java.getTimeStamp("d");
        }
        if(Constants.MOBILE_OS == Platform.ANDROID) {
            By dateElement = By.xpath("//*[@text='" + date + "' and @enabled='true']");
            appium.tapOrClick(dateElement);
        }else {
            MobileElement dateElement = (MobileElement) driver.findElementByXPath("//XCUIElementTypeStaticText[@name='"+date+"']");
            appium.tapOrClick(dateElement);
        }
        appium.tapOrClick(btnBooking);
    }

    /**
     * @return return true if pembayara page present. Otherwise false
     */
    public boolean pembayaranPageIsPresent() {
        appium.waitInCaseElementVisible(pembayaranPageTitle,15);
        return appium.isElementPresent(pembayaranPageTitle);
    }

    /**
     * Check if rincian pembayaran is present
     * @return true or false
     */
    public boolean isRincianPembayaranTitlePresent() {
        return appium.waitInCaseElementVisible(rincianPembayaranTitle, 2) != null;
    }

    /**
     * Scroll to rincian pembayaran
     */
    public void scrollToRincianPembayaranPage() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isRincianPembayaranTitlePresent());
    }

    /**
     * Check if rincian pelunasan is present
     * @return true or false
     */
    public boolean isRincianPelunasanTitlePresent() {
        return appium.waitInCaseElementVisible(rincianPelunasanTitle, 2) != null;
    }

    /**
     * Scroll to rincian Pelunasan
     */
    public void scrollToRincianPelunasanPage() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isRincianPelunasanTitlePresent());
    }

    /**
     * tap on element with text by name
     * @param text desired text for element
     */
    public void tapOnElemenetWithTextByName(String text) {
        String menuOnProfile = "";
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            menuOnProfile = "//*[@text='"+text+"']";
        }else{
            menuOnProfile = "//XCUIElementTypeButton[@name='"+text+"']";
        }
        MobileElement element = (MobileElement) driver.findElementByXPath(menuOnProfile);
        appium.tapOrClick(element);
    }

    /**
     * Check if rincian pembayaran is present
     * @return true or false
     */
    public boolean isDetailKostPresent(String text) {
        By detailKostPage = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='" + text + "']") :
                By.xpath("(//XCUIElementTypeStaticText[@name='" + text + "'])[1]");
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            return appium.getText(detailKostPage).equalsIgnoreCase(text);
        } else {
            return appium.waitInCaseElementVisible(detailKostPage, 5) != null;
        }
    }

    /**
     * is element uang muka label visible
     * @param text desired text
     * @return visible true otherwise false
     */
    public boolean isUangMukaDPPresent(String text) {
        By uangMukaDPLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='" + text + "']"):
                By.xpath("(//XCUIElementTypeStaticText[@name='" + text + "'])[1]");
        return appium.waitInCaseElementVisible(uangMukaDPLabel, 5) != null;
    }

    /**
     * is element biaya layanan label visible
     * @param text desired text
     * @return visible true otherwise false
     */
    public boolean isBiayaLayananDPPresent(String text) {
        By biayaLayananDPLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='" + text + "']"):
                By.xpath("(//*[@name='"+text+"'])[2]");
        return appium.waitInCaseElementVisible(biayaLayananDPLabel, 5) != null;
    }

    /**
     * Check if rincian pelunasan is present
     * @return true or false
     */
    public boolean isRincianPembayaranDPTitlePresent() {
        return appium.waitInCaseElementVisible(rincianPembayaranDPTitle, 2) != null;
    }

    /**
     * Scroll to rincian pembayaran Down Payment
     */
    public void scrollToRincianPembayaranDPPage() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isRincianPembayaranDPTitlePresent());
    }

}
