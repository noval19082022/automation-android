package pageobjects.mamikos.tenant.editprofile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class EditProfilePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public EditProfilePO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    private By editProfileView = Constants.MOBILE_OS == Platform.ANDROID ? By.id("editFrameLayout"): By.id("Edit Profile");

    private By profileNameEditText = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.EditText[@resource-id='com.git.mami.kos:id/profileNameEditText']"): By.xpath("//*[@name='Nama Lengkap']/following-sibling::XCUIElementTypeTextField[1]");

    private By genderMaleRadioButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("rb_profile_gender_male"): By.xpath("//*[@name='Jenis Kelamin']/following::XCUIElementTypeButton[1]");

    private By employeeRadioButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.RadioButton[@text=\"Karyawan\"]"): By.xpath("//*[@name='Pekerjaan']/following::XCUIElementTypeButton[2]");

    private By officeNameText = Constants.MOBILE_OS == Platform.ANDROID ? By.id("universityEditText"): By.xpath("//*[@name='Nama instansi/perusahaan']/following-sibling::XCUIElementTypeTextField[1]");

    private By saveButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Nama Instansi']/following::android.widget.FrameLayout[2]"): By.xpath("//XCUIElementTypeButton[@name='Simpan']");

    @AndroidFindBy(id = "employeeRadioButton")
    private WebElement karyawanRadioButton;

    @AndroidFindBy(id = "studentRadioButton")
    private WebElement mahasiswaRadioButton;

    @AndroidFindBy(id = "universityEditText")
    private WebElement profileProfessionEditText;

    @AndroidFindBy(id = "messageTextView")
    private WebElement toastMessageUpdateProfile;

    private By infomariPribadiMenu = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Informasi Pribadi']"): By.xpath("//*[@name='Akun']/following::XCUIElementTypeButton[1]");

    private By doneButton = By.xpath("//XCUIElementTypeButton[@name='Done']");


    /**
     * Click on 'Edit Profile'
     */
    public void clickOnEditProfile() throws InterruptedException {
        appium.waitInCaseElementVisible(editProfileView,5);
        appium.clickOn(editProfileView);
    }

    /**
     * Get Profile Name Text
     * @return Name
     */
    public String getProfileName() throws InterruptedException {
        appium.hardWait(3);
        appium.waitTillElementIsVisible(profileNameEditText,3);
        return appium.getText(profileNameEditText);
    }

    /**
     * Get Gender Text
     * @return Gender
     */
    public String getGender() throws InterruptedException {
        String gender = "";
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.hardWait(2);
            gender = appium.getText(genderMaleRadioButton);
        }else {
            gender = appium.getElementAttributeValue(genderMaleRadioButton,"name");
        }
        return gender;
    }

    /**
     * Get Profession Text
     * @return Profession
     */
    public String getProfession() throws InterruptedException {
        String profession = "";
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Kirim Data");
            appium.hardWait(2);
            profession = appium.getText(employeeRadioButton);
        }else {
            int i = 0;
            boolean condition = true;
            do {
                appium.scroll(0.5, 0.8, 0.2, 2000);
                if (i == 1) {
                    condition = false;
                }
                i++;
            }
            while (condition);
            profession = appium.getElementAttributeValue(employeeRadioButton, "name");
        }
        return profession;
    }

    /**
     * Get Office Name Text
     *
     * @return
     */
    public String getOfficeName() throws InterruptedException {
        appium.hardWait(2);
        return appium.getText(officeNameText);
    }

    /**
     * Edit Name tenant
     *
     * @return
     */
    public void editNameTenant(String name) {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByText("Nama Lengkap *");
            appium.enterText(profileNameEditText, name, true);
            appium.scrollToElementByText("Kirim Data");
        } else {
            appium.scroll(0.5, 0.2, 0.8, 2000);
            appium.enterText(profileNameEditText, name, true);
            appium.tapOrClick(doneButton);
        }
    }

    /**
     * Scroll to button "Simpan"
     * Click button "Simpan"
     *
     * @return
     */
    public void saveProfile() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByText("Kirim Data");
            appium.waitInCaseElementVisible(saveButton,10);
            appium.clickOn(saveButton);
        } else {
            appium.tapOrClick(saveButton);
        }
    }
    /**
     * Scroll to element
     * Select Profession Karyawan
     * @return
     */
    public void clickOnKaryawanRadioButton(){
        appium.scrollToElementByResourceId("employeeRadioButton");
        appium.clickOn(karyawanRadioButton);
    }

    /**
     * Scroll to element
     * Select Profession Mahasiswa
     * @return
     */
    public void clickOnMahasiswaRadioButton(){
        appium.scrollToElementByResourceId("studentRadioButton");
        appium.clickOn(mahasiswaRadioButton);
    }

    /**
     * Scroll to element
     * Edit Tenant Profession
     *
     * @return
     */
    public void editTenantProfession(String profession) {
        appium.enterText(profileProfessionEditText, profession, true);
    }

    /**
     * Get Message when update profile
     *
     * @return Nama user min 3 karakter.
     */
    public String getToastMessageUpdateProfile() {
        appium.waitInCaseElementVisible(toastMessageUpdateProfile,5);
        return appium.getText(toastMessageUpdateProfile);
    }

    /**
     * Click on 'Informasi Pribadi'
     */
    public void clickOnInformasiPribadi() {
        appium.waitTillElementIsVisible(infomariPribadiMenu, 3);
        appium.clickOn(infomariPribadiMenu);
    }

    /**
     * Click on Oke button on popup success edit profile
     */
    public void clickOnOkeButton() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByText("Edit Profile");
        }
    }
}
