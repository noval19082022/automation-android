package pageobjects.mamikos.tenant.explore;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.ArrayList;
import java.util.List;

public class ExplorePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ExplorePO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    private By searchTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("searchView"): By.xpath("(//XCUIElementTypeApplication[@name='mamikos']/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/descendant::XCUIElementTypeButton)[1]");

    //------------- Explore Tab ---------------
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeImage[@name=\"navBarLogoIcon\"]")
    @AndroidFindBy(id = "com.git.mami.kos:id/toolbarHomeImageView")
    private WebElement mamikosLogo;

    @AndroidFindBy(id = "notificationHomeImageView")
    private WebElement notifIcon;

    @iOSXCUITFindBy(accessibility = "greetingLabel")
    @AndroidFindBy(id = "usernameTextView")
    private WebElement usernameText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Kamar Kos\"]")
    @AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"ImageView_MamiKost\"])[2]")
    private WebElement kostImg;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Apartemen\"]")
    @AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"ImageView_MamiKost\"])[3]")
    private WebElement apartementImg;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Barang & Jasa\"]")
    @AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"ImageView_MamiKost\"])[4]")
    private WebElement goodsAndServiceImg;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Lowongan Kerja\"]")
    @AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"ImageView_MamiKost\"])[4]")
    private WebElement jobVacancyImg;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Lowongan Kerja\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text, 'Lowongan Kerja')]")
    private WebElement jobVacancyText;

    @AndroidFindBy(id = "potsButtonView")
    private WebElement QRButton;

    @iOSXCUITFindBy(accessibility = "posterSlider")
    @AndroidFindBy(id = "bannerViewPager")
    private WebElement promoBanner;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Anda Pemilik Iklan?\"]")
    @AndroidFindBy(id = "ownerView")
    private WebElement loginOwnerBanner;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Masuk di Sini\"]")
    @AndroidFindBy(id = "signInOwnerKosTextView")
    private WebElement loginOwnerButton;

    @AndroidFindBy(id = "flashSaleRecyclerView")
    private WebElement flashsaleWidget;

    //------- Testimonial ---------

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
    @AndroidFindBy(id = "testimonialRecyclerView")
    private WebElement testimonialWidget;

    private By testiWidget = Constants.MOBILE_OS == Platform.ANDROID ? By.id("testimonialRecyclerView"): By.xpath("//*[@name='Kata Pemilik Kos']/following::XCUIElementTypeOther[3]");

    private By testimonialTitleText = Constants.MOBILE_OS == Platform.ANDROID ? By.id("testimonialTitleTextView"): By.id("Kata Pemilik Kos");

    private By ownerTestimonialImg = Constants.MOBILE_OS == Platform.ANDROID ? By.id("ownerImageView"): By.xpath("//*[@name='Kata Pemilik Kos']/following::XCUIElementTypeImage[1]");

    private By ownerNameTestimonialText = By.id("nameOwnerTextView");

    private By kostNameTestimonialText = By.id("kosNameTextView");

    private By descTestimonialText = By.id("testimonialTextView");

    //------- Recommendation ---------

    private By locationKosRecommendation = Constants.MOBILE_OS == Platform.ANDROID ? By.id("inputDropdown"): By.xpath("//*[@name='Rekomendasi Kos']/following::XCUIElementTypeButton[1]");

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Jogja\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text, 'Jogja')]")
    private WebElement cityLocRecommendation;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[2]")
    @AndroidFindBy(id = "kosRecommendationRecyclerView")
    private WebElement kosRecommendationWidget;

    //------- Promoted Kost ---------

    @AndroidFindBy(id = "registerFreeKosTextView")
    private WebElement registerFreeKosText;

    @AndroidFindBy(id = "promotedCitiesView")
    private WebElement promotedCitiesDropdown;

    @AndroidFindBy(id = "avatarResultImageView")
    private WebElement imageAvatar;

    @AndroidFindBy(id = "titleResultTextView")
    private WebElement titleResultTextView;

    @AndroidFindBy(id = "actionResultButton")
    private WebElement actionLoginButton;

    @AndroidFindBy(id = "bookingTextView")
    private List<WebElement> RoomTextView;

    private By pusatBantuanButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@resource-id='com.git.mami.kos:id/manageKosMenuRecyclerView']//*[@text='Pusat Bantuan']"):
            By.id("Pusat Bantuan");

    @AndroidFindBy(id = "flashSaleTitleTextView")
    private WebElement flashSaleTitleText;

    private By promoLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.id("timerDayTextView"): By.id("Akan berakhir dalam waktu:");

    private By promoSponsoredIcon = Constants.MOBILE_OS == Platform.ANDROID ? By.id("headerTimerImageView"): By.xpath("(//*[@name='ic_ads'])[1]");

    private By roomImageView = Constants.MOBILE_OS == Platform.ANDROID ? By.id("kosImageView"): By.xpath("//*[@name='Promo Ngebut']/following::XCUIElementTypeImage[2]");

    private By genderTextLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//*[@text='Semua Kota']/following::android.widget.TextView)[1]"): By.xpath("(//*[@name='Campur'])[1]");

    private By priceTextLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.id("priceTextView"): By.xpath("(//*[@name='Campur'])[1]/following::XCUIElementTypeStaticText[5]");

    private By kosNameTextLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//*[@text='Semua Kota']/following::android.widget.TextView)[3]"): By.xpath("(//*[@name='Campur'])[1]/following::XCUIElementTypeStaticText[1]");

    private By locTextLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.id("promoTextView"): By.xpath("(//*[@name='Campur'])[1]/following::XCUIElementTypeStaticText[2]");

    private By seeAllRecommendationLinkedText = Constants.MOBILE_OS == Platform.ANDROID ? By.id("headerTitleTextView"): By.xpath("(//XCUIElementTypeButton[@name='Lihat semua'])[2]");

    private By kostMalangTitle = By.id("Kos Kenno Satu Kedungkandang Malang");

    private By rekomendasiKosTitle = By.xpath("//*[@name='Rekomendasi Kos']");

    private By kosPromotedWidget = Constants.MOBILE_OS == Platform.ANDROID ? By.id("headerTimerImageView"): By.xpath("(//XCUIElementTypeButton[@name='Lihat semua'])[1]");


    /**
     * Click on 'Search' textbox
     */
    public void clickOnSearchTextbox() throws InterruptedException{
        appium.hardWait(3);
        appium.waitTillElementIsVisible(searchTextbox,5);
        appium.tapOrClick(searchTextbox);
    }

    /**
     * Click on 'Notification' icon
     */
    public void clickOnNotificationIcon() {
        appium.clickOn(notifIcon);
    }

    /**
     * Check Mamikos Logo is displayed
     * @return status
     */
    public boolean isMamikosLogoDisplayed() {
        return appium.waitInCaseElementVisible(mamikosLogo, 5) != null;
    }

    /**
     * Check Notification Icon is displayed
     * on iOS there is no notification icon
     * @return status
     */
    public boolean isNotificationIconDisplayed() {
        return appium.waitInCaseElementVisible(notifIcon, 5) != null;
    }

    /**
     * Check Search box is displayed
     * @return status
     */
    public boolean isSearchTextBoxDisplayed() {
        return appium.waitInCaseElementVisible(searchTextbox, 5) != null;
    }

    /**
     * Check Username text is displayed
     * @return status
     */
    public boolean isUsernameDisplayed() {
        return appium.waitInCaseElementVisible(usernameText, 5) != null;
    }

    /**
     * Check Kos Image is displayed
     * @return status
     */
    public boolean isKosImageDisplayed() {
        return appium.waitInCaseElementVisible(kostImg, 5) != null;
    }

    /**
     * Check Apartemen Image is displayed
     * @return status
     */
    public boolean isApartemenImageDisplayed() {
        return appium.waitInCaseElementVisible(apartementImg, 5) != null;
    }

    /**
     * Check Barang dan Jasa Image is displayed
     * @return status
     */
    public boolean isGoodsandServiceImageDisplayed() {
        return appium.waitInCaseElementVisible(goodsAndServiceImg, 5) != null;
    }

    /**
     * Check Job Vacancy Image is displayed
     * @return status
     */
    public boolean isJobVacancyImageDisplayed() {
        appium.scrollHorizontal(0.40, 0.90, 0.10, 2000);
        return appium.waitInCaseElementVisible(jobVacancyImg, 5) != null;
    }

    /**
     * Check Job Vacancy Image is displayed
     * @return status
     */
    public boolean isJobVacancyTextDisplayed() {
        return appium.waitInCaseElementVisible(jobVacancyText, 5) != null;
    }

    /**
     * Check QR Button is displayed
     * @return status
     */
    public boolean isQRButtonDisplayed() {
        return appium.waitInCaseElementVisible(QRButton, 5) != null;
    }

    /**
     * Check Promo Banner is displayed
     * @return status
     */
    public boolean isPromoBannerDisplayed() {
        return appium.waitInCaseElementVisible(promoBanner, 5) != null;
    }

    /**
     * Check Login Owner Banner is displayed
     * @return status
     */
    public boolean isLoginOwnerBannerDisplayed() {
        appium.scrollToElementByResourceId("ownerView");
        return appium.waitInCaseElementVisible(loginOwnerBanner, 5) != null;
    }

    /**
     * Check Login Owner Button is displayed
     * @return status
     */
    public boolean isLoginOwnerButtonDisplayed() {
        appium.scrollToElementByResourceId("signInOwnerKosTextView");
        return appium.waitInCaseElementVisible(loginOwnerButton, 5) != null;
    }

    /**
     * Check flash sale widget is displayed
     * @return status
     * @throws InterruptedException
     */
    public boolean isFlashSaleWidgetDisplayed() throws InterruptedException {
        appium.scrollToElementByResourceId("flashSaleTimerView");
        if (appium.waitInCaseElementVisible(flashSaleTitleText, 5) != null) {
            return appium.waitInCaseElementVisible(flashsaleWidget, 5) != null;
        } else {
            return isKosRecommendationDisplayed();
        }
    }

    /**
     * Check if testimonial is present
     * @return true or false
     */
    public boolean isTestimonialPagePresent() {
        return appium.waitInCaseElementVisible(testimonialTitleText, 1) != null;
    }

    /**
     * Scroll down to Testimonial Widget
     */
    public void scrollDownToTestimonial() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByResourceId("testimonialTextView");
        }else {
            int i = 0;
            do {
                appium.scroll(0.5, 0.8, 0.2, 2000);
                if(i == 10) {
                    break;
                }
                i++;
            }
            while (!isTestimonialPagePresent());
        }
    }

    /**
     * Check Testimonial Widget is displayed
     * @return status
     */
    public boolean isTestimonialWidgetDisplayed() {
        if (appium.waitInCaseElementVisible(flashsaleWidget, 5) != null) {
            appium.scrollToElementByResourceId("testimonialRecyclerView");
        }
        return appium.waitInCaseElementVisible(testimonialWidget, 5) != null;
    }

    /**
     * Convert %s to index 1, 2, 3, of testimonialIndex
     * @param a     fill with element
     * @param index fill with integer data
     * @return By.id after reformat
     */
    private By testimonialIndex(By a, int index) {
        String testimonialTarget = a.toString().replace("By.id: ", "");
        testimonialTarget = String.format(testimonialTarget, Integer.toString(index));
        return By.id(testimonialTarget);
    }

    /**
     * Get total index of testimonial
     * @return size (total index)
     */
    public int getTestimonialSize() {
        List<WebElement> element = driver.findElements(testiWidget);
        int size = element.size();
        return size;
    }

    /**
     * Check Testimonial Title is displayed
     * @return status
     */
    public boolean isTestimonialTitlePresent() {
        return appium.waitInCaseElementVisible(testimonialTitleText,5) != null;
    }

    /**
     * Check the owner image is present
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isOwnerImgPresent(int index) throws InterruptedException {
        return appium.isElementPresent(testimonialIndex(ownerTestimonialImg, index));
    }

    /**
     * Check the owner name is present
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isOwnerNamePresent(int index) throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByResourceId("nameOwnerTextView");
        }
        return appium.isElementPresent(testimonialIndex(ownerNameTestimonialText, index));
    }

    /**
     * Check the owner kos name is present
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isOwnerKosNamePresent(int index) throws InterruptedException {
        return appium.isElementPresent(testimonialIndex(kostNameTestimonialText, index));
    }

    /**
     * Check the testimonial desc is present
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isTestimonialDescPresent(int index) throws InterruptedException {
        return appium.isElementPresent(testimonialIndex(descTestimonialText, index));
    }

    /**
     * Check Kos Recommendation is displayed
     * @return status
     * @throws InterruptedException
     */
    public boolean isKosRecommendationDisplayed() throws InterruptedException {
        appium.hardWait(2);
        appium.scrollToElementByResourceId("kosRecommendationCityTextView");
        appium.tapOrClick(locationKosRecommendation);
        appium.tapOrClick(cityLocRecommendation);
        return appium.waitInCaseElementVisible(kosRecommendationWidget, 5) != null;
    }

    /**
     * Check if See All Recommendation Kos Button is present
     * @return true or false
     */
    public boolean isSeePromoNgebutKosPresent() {
        return appium.waitInCaseElementVisible(kosPromotedWidget, 1) != null;
    }

    /**
     * Check Kos Promoted is displayed
     * @return status
     */
    public boolean isKosPromotedDisplayed() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByResourceId("headerTimerImageView");
        }else {
            int i = 0;
            while (!isSeePromoNgebutKosPresent()){
                appium.scroll(0.3, 0.5, 0.2, 2000);
                i++;
                if(i == 10) {
                    break;
                }
            }
        }
        return appium.waitInCaseElementVisible(kosPromotedWidget, 5) != null;
    }

    /**
     * Click On Register text link from home page
     */
    public void clickOnRegisterFreeKosText() {
        appium.scrollToElementByText("Daftarkan Kos Gratis");
        appium.tapOrClick(registerFreeKosText);
    }

    /**
     * CLick on Kos tab from Home page
     */
    public void clickOnKosTab() {
        appium.tapOrClick(kostImg);
    }

    /**
     * CLick on Goods and Services tab from Home page
     */
    public void clickOnGoodsAndServicesTab() {
        appium.tapOrClick(goodsAndServiceImg);
    }

    /**
     * Click on Job Vacancy tab from home page
     * @throws InterruptedException
     */
    public void clickOnJobVacancyTab() throws InterruptedException {
        appium.hardWait(2);
        appium.scrollHorizontal(0.35, 0.90, 0.10, 2000);
        appium.tapOrClick(jobVacancyImg);
    }

    /**
     * Click on promoted city dropdown
     */
    public void clickOnPromotedCityDropDown() {
        appium.scrollToElementByResourceId("promotedCitiesView");
        appium.tapOrClick(promotedCitiesDropdown);
    }

    /**
     * Is city present ?
     * @param cityName city name
     * @return true or false
     */
    public boolean isCityNamePresent(String cityName) {
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + cityName + "']"), 5) != null;
    }

    /**
     * Click on Apartment tab from home page
     */
    public void clickOnApartmentTab() {
        appium.tapOrClick(apartementImg);
    }

    /**
     * Get Title Result Text
     * @return title result
     */
    public String getTitleResultText() {
        return appium.getText(titleResultTextView);
    }

    /**
     * Is Image Avatar Present ?
     * @return true or false
     */
    public boolean isImageAvatarPresent() {
        return appium.waitInCaseElementVisible(imageAvatar, 5) != null;
    }

    /**
     * Is Action Login Button Present ?
     * @return true or false
     */
    public boolean isActionLoginButtonPresent() {
        return appium.waitInCaseElementVisible(actionLoginButton, 5) != null;
    }

    /**
     * Check if See All Recommendation Kos Button is present
     * @return true or false
     */
    public boolean isSeeAllRecommendationKosButtonPresent() {
        return appium.waitInCaseElementVisible(rekomendasiKosTitle, 1) != null;
    }

    /**
     * Click on Location button
     */
    public void clickOnLocationKosRecommendation() throws InterruptedException {
        appium.hardWait(3);
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.scrollToElementByResourceId("headerTitleTextView");
        }else{
            int i = 0;
            do {
                appium.scroll(0.5, 0.8, 0.2, 2000);
                if(i == 10) {
                    break;
                }
                i++;
            }
            while (!isSeeAllRecommendationKosButtonPresent());
        }
        appium.tapOrClick(locationKosRecommendation);
    }

    /**
     * Check location name is present or not
     * @param locationName location name
     * @return true or false
     */
    public boolean isLocationNamePresent(String locationName) {
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + locationName + "']"), 5) != null;
    }

    /**
     * Recommendation - Get list of Kos Available room
     * @param scrollCount scroll count
     * @return list of room
     */
    public List<String> getListOfKosRoomForRecommendation(int scrollCount) {
        List<String> kosRooms = new ArrayList<>();
        for (int i = 0; i < scrollCount; i++) {
            for (WebElement e : RoomTextView) {
                kosRooms.add(appium.getText(e));
            }
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return kosRooms;
    }

    /**
     * Select location from dropdown
     * @param location location name
     */
    public void selectLocation(String location) {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            String xpathLocator = "//*[@text='" + location + "']";
            appium.clickOn(By.xpath(xpathLocator));
        }else{
            MobileElement pilihKotaButton = (MobileElement) driver.findElementByXPath("//*[@name='"+ location +"']");
            appium.tapOrClick(pilihKotaButton);
        }
    }

    /**
     * Recommendation - verify room image view is shown or not
     * @return true or false
     */
    public boolean isRoomImageViewDisplayedForRecommendation() throws InterruptedException{
        appium.hardWait(4);
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByResourceId("kosImageView");
        }
        return appium.waitInCaseElementVisible(roomImageView, 5) != null;
    }

    /**
     * Click on See All recommendation text
     */
    public void clickOnSeeAllRecommendationLinkedText() {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.scrollToElementByResourceId("headerTitleTextView");
        }
        appium.tapOrClick(seeAllRecommendationLinkedText);
    }

    /**
     * Recommendation - verify gender label is shown or not
     * @return true or false
     */
    public boolean isGenderRecomenKosPresent() {
        return appium.waitInCaseElementVisible(genderTextLabel, 5) != null;
    }

    /**
     * Recommendation - verify price label is shown or not
     * @return true or false
     */
    public boolean isPriceRecomenKosPresent() {
        return appium.waitInCaseElementVisible(priceTextLabel, 5) != null;
    }

    /**
     * Recommendation - verify kos name label is shown or not
     * @return true or false
     */
    public boolean isKosNameRecomenKosPresent() {
        return appium.waitInCaseElementVisible(kosNameTextLabel, 5) != null;
    }

    /**
     * Recommendation - verify Loc label is shown or not
     * @return true or false
     */
    public boolean isLocRecomenKosPresent() {
        return appium.waitInCaseElementVisible(locTextLabel, 5) != null;
    }

    /**
     * Check if mamikos logo in homepage is present
     * @return boolean
     */
    public boolean isInHomepage() {
        return appium.waitInCaseElementVisible(mamikosLogo, 1) != null;
    }

    /**
     * List all listing address in search result
     * @return list of address (String)
     */
    public List<String> listKostAddress() throws InterruptedException {
        String xpathLocator = "//*[@text='Semua Kota']/following::android.widget.TextView[5]/preceding::android.widget.TextView[1]";
        List<WebElement> elements = appium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> addressList = new ArrayList<>();
        for (WebElement a : elements) {
            appium.hardWait(3);
            addressList.add((appium.getText(a)));
        }
        return addressList;
    }

    /**
     * Promoted - verify promo label is shown or not
     * @return true or false
     */
    public boolean isPromotedLabelDisplayed() {
        if(Constants.MOBILE_OS == Platform.IOS) {
            appium.scroll(0.5, 0.2, 0.8, 2000);
        }
        return appium.waitInCaseElementVisible(promoLabel, 5) != null;
    }

    /**
     * Promoted - verify promo image kos is shown or not
     * @return true or false
     */
    public boolean isPromotedImageKosDisplayed() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scroll(0.5, 0.70, 0.30, 2000);
        }
        return appium.waitInCaseElementVisible(roomImageView, 5) != null;
    }

    /**
     * Promoted - verify sponsored icon kos is shown or not
     * @return true or false
     */
    public boolean isPromotedSponsoredDisplayed() {
        return appium.waitInCaseElementVisible(promoSponsoredIcon, 5) != null;
    }
    /**
     * Promoted - verify gender kos is shown or not
     * @return true or false
     */
    public boolean isPromotedGenderDisplayed() {
        return appium.waitInCaseElementVisible(genderTextLabel, 5) != null;
    }

    /**
     * Promoted - verify kost name kos is shown or not
     * @return true or false
     */
    public boolean isPromotedKosNameDisplayed() {
        return appium.waitInCaseElementVisible(kosNameTextLabel, 5) != null;
    }

    /**
     * Promoted - verify kost location kos is shown or not
     * @return true or false
     */
    public boolean isPromotedKosLocDisplayed() {
        return appium.waitInCaseElementVisible(locTextLabel, 5) != null;
    }

    /**
     * Promoted - verify kost price kos is shown or not
     * @return true or false
     */
    public boolean isPromotedKosPriceDisplayed() {
        appium.scroll(0.3, 0.5, 0.2, 2000);
        return appium.waitInCaseElementVisible(priceTextLabel, 5) != null;
    }

    /**
     * Check if pusat bantuan button is present in viewport
     * @return true or false
     */
    public boolean isUpdateRoomPriceButtonPresent() {
        return appium.waitInCaseElementVisible(pusatBantuanButton, 1) != null;
    }

    /**
     * tap on pusat bantuan button
     */
    public void tapOnPusatBantuanButton() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isUpdateRoomPriceButtonPresent());
        appium.tapOrClick(pusatBantuanButton);
    }

    /**
     * Check if kost area Malang is present
     * @return true or false
     */
    public boolean isKostAreaMalangPresent() {
        return appium.waitInCaseElementVisible(kostMalangTitle, 3) != null;
    }
}
