package pageobjects.mamikos.tenant.jobVacancies;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class BottomFeatureOnJobVacancyPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public BottomFeatureOnJobVacancyPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "sortFilterView")
    private WebElement sortFeature;

    @AndroidFindBy(id = "filterView")
    private WebElement filterFeature;

    @AndroidFindBy(id = "sortVacancyViaMamikosTextView")
    private WebElement sortViaMamikos;

    @AndroidFindBy(id = "sortVacancyNewestTextView")
    private WebElement sortVacanciesNewest;

    @AndroidFindBy(id = "sortVacancyOldestTextView")
    private WebElement sortVacanciesLongest;

    @AndroidFindBy(id = "valueVacancyTypeTextView")
    private WebElement jobVacancyTypeEditText;

    @AndroidFindBy(id = "valueEducationTypeTextView")
    private WebElement jobVacancyLastEducationEditText;

    @AndroidFindBy(id = "valueVacancySortTextView")
    private WebElement jobVacancySortEditText;

    @AndroidFindBy(id = "confirmFilterTextView")
    private WebElement searchButton;

    /**
     * Click on icon sort job vacancies
     */
    public void clickOnSortFeature() throws InterruptedException {
        if (!appium.isElementDisplayed(sortFeature)){
            appium.scroll(0.5, 0.20, 0.80, 2000);
        }
        appium.hardWait(2);
        appium.clickOn(sortFeature);
    }

    /**
     * Click on sort job vacancies via mamikos
     */
    public void clickOnSortJobVacanciesViaMamikos() {
        appium.clickOn(sortViaMamikos);
    }

    /**
     * Click on sort newest job vacancies
     */
    public void clickOnSortJobVacanciesNewest() {
        appium.clickOn(sortVacanciesNewest);
    }

    /**
     * Click on sort oldest job vacancies
     */
    public void clickOnSortJobVacanciesLongest() {
        appium.clickOn(sortVacanciesLongest);
    }

    /**
     * Click on icon filter job vacancies
     */
    public void clickOnFilterIcon() throws InterruptedException {
        if (!appium.isElementDisplayed(filterFeature)){
            appium.scroll(0.5, 0.20, 0.80, 2000);
        }
        appium.hardWait(2);
        appium.clickOn(filterFeature);
    }

    /**
     * Click on field job vacancy type
     */
    public void clickOnFieldJobVacancyType() {
        appium.clickOn(jobVacancyTypeEditText);
    }

    /**
     * Select option job vacancy type
     */
    public void selectOnJobVacancyFilterValue(String jobTypeValue) {
        appium.clickOn(By.xpath("//*[@resource-id='com.git.mami.kos:id/tv_choice'][@text='" + jobTypeValue + "']"));
    }

    /**
     * Click on search button
     */
    public void clickOnSearchButton() {
        appium.clickOn(searchButton);
    }

    /**
     * Click on field last education
     */
    public void clickOnFieldLastEducation() {
        appium.clickOn(jobVacancyLastEducationEditText);
    }

    /**
     * Click on field sort
     */
    public void clickOnFieldSort() {
        appium.clickOn(jobVacancySortEditText);
    }
}
