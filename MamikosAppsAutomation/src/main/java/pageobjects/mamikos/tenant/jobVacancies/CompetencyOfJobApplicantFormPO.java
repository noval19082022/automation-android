package pageobjects.mamikos.tenant.jobVacancies;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class CompetencyOfJobApplicantFormPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public CompetencyOfJobApplicantFormPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @AndroidFindBy(id = "applyTwoEducationValue")
    private WebElement lastEducationTextBox;

    @AndroidFindBy(id = "applyTwoSkillValue")
    private WebElement skillAndAbilitiesTextBox;

    @AndroidFindBy(id = "applyTwoExperienceValue")
    private WebElement workExperienceTextBox;

    @AndroidFindBy(id = "applyTwoSalaryValue")
    private WebElement lastSalaryTextBox;

    @AndroidFindBy(id = "applyTwoSalaryExpectValue")
    private WebElement salaryExpectationTextBox;

    @AndroidFindBy(id = "applyButton")
    private WebElement continueButton;

    @AndroidFindBy(accessibility = "Navigate up")
    private WebElement backArrowButton;

    /**
     * Input last education
     * @param lastEducation is last education applicant
     */
    public void inputLastEducation(String lastEducation) {
        appium.enterText(lastEducationTextBox, lastEducation, true);
    }

    /**
     * Input skill and abilities
     * @param skillAndAbilities is skill and abilities applicant
     */
    public void inputSkillAndAbilities(String skillAndAbilities) {
        appium.enterText(skillAndAbilitiesTextBox, skillAndAbilities, true);
    }

    /**
     * Input work experience
     * @param workExperience is work experience applicant
     */
    public void inputWorkExperience(String workExperience) {
        appium.enterText(workExperienceTextBox, workExperience, true);
    }

    /**
     * Input last salary
     * @param lastSalary is last salary applicant
     */
    public void inputLastSalary(String lastSalary) {
        appium.enterText(lastSalaryTextBox, lastSalary, true);
    }

    /**
     * Input last education
     * @param salaryExpectation is last education applicant
     */
    public void inputSalaryExpectations(String salaryExpectation) {
        appium.enterText(lastSalaryTextBox, salaryExpectation, true);
    }

    /**
     * Click on continue button
     */
    public void clickOnContinueButton() {
        appium.clickOn(continueButton);
    }

    /**
     * Click on back arrow button
     */
    public void clickOnBacArrowButton() {
        appium.clickOn(backArrowButton);
    }
}
