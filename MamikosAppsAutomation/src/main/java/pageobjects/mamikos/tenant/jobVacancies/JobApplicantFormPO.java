package pageobjects.mamikos.tenant.jobVacancies;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class JobApplicantFormPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public JobApplicantFormPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @AndroidFindBy(id = "applyOneNameValue")
    private WebElement nameTextBox;

    @AndroidFindBy(id = "applyOneEmailValue")
    private WebElement emailTextBox;

    @AndroidFindBy(id = "applyOnePhoneValue")
    private WebElement phoneNumberTextBox;

    @AndroidFindBy(id = "applyOneAddressValue")
    private WebElement addressTextBox;

    @AndroidFindBy(id = "applyButton")
    private WebElement continueButton;

    /**
     * Input name
     * @param name is full name applicant
     */
    public void inputName(String name) {
        appium.enterText(nameTextBox, name, true);
    }

    /**
     * Input email
     * @param email is email applicant
     */
    public void inputEmail(String email) {
        appium.enterText(emailTextBox, email, true);
    }

    /**
     * Input phone number
     * @param phoneNumber is phone number applicant
     */
    public void inputPhoneNumber(String phoneNumber) {
        appium.enterText(phoneNumberTextBox, phoneNumber, true);
    }

    /**
     * Input phone address
     * @param address is address applicant
     */
    public void inputAddress(String address) {
        appium.enterText(addressTextBox, address, true);
    }

    /**
     * Click on continue button
     */
    public void clickOnContinueButton() {
        appium.clickOn(continueButton);
    }
}
