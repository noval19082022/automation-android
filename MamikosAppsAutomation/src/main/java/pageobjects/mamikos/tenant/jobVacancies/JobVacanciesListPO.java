package pageobjects.mamikos.tenant.jobVacancies;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class JobVacanciesListPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public JobVacanciesListPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @AndroidFindBy(xpath = "(//*[@resource-id='com.git.mami.kos:id/clItemJob'])[1]")
    private WebElement firstJobVacancy;

    @AndroidFindBy(id = "listMainAdsRecyclerView")
    private WebElement listJobVacancy;

    @AndroidFindBy(xpath = "(//*[@resource-id='com.git.mami.kos:id/tv_job_last_education'])[1]")
    private WebElement firstJobVacancyTypeLabel;

    @AndroidFindBy(xpath = "(//*[@resource-id='com.git.mami.kos:id/tv_job_last_education'][@text='SMA/SMK'])[1]")
    private WebElement firstJobVacancyLastEducation;

    /**
     * Verify job vacancy post via mamikos
     */
    public Boolean verifyJobVacanciesViaMamikos(int index) {
        return appium.isElementPresent(By.xpath("(//*[@resource-id='com.git.mami.kos:id/clItemJob']//android.widget.TextView[@resource-id='com.git.mami.kos:id/tv_job_source'])[" + index + "]"));
    }

    /**
     * Get date the job opened since
     */
    public Date getJobVacancyOpenSince(int index) throws ParseException {
        Locale id = new Locale("in", "ID");
        String jobDate = appium.getText(By.xpath("(//*[@resource-id='com.git.mami.kos:id/tv_job_since'])[" + index + "]"));
        SimpleDateFormat pattern = new SimpleDateFormat("dd MMM yyyy", id);

        return pattern.parse(jobDate);
    }

    /**
     * Click on first job vacancy on job vacancy list
     */
    public void clickOnFirstJobVacancy() {
        appium.clickOn(firstJobVacancy);
    }

    /**
     * Verify list job vacancy appeared
     */
    public void verifyListJobVacancyAppeared() {
        appium.isElementDisplayed(listJobVacancy);
    }

    /**
     * Get job vacancy last education
     * @return job vacancy last education
     */
    public String getJobVacancyLastEducationEditText() {
        return appium.getText(firstJobVacancyLastEducation);
    }

    /**
     * Get job vacancy type
     * @return job vacancy type
     */
    public String getFirstJobVacancyTypeLabel() {
        return appium.getText(firstJobVacancyTypeLabel);
    }
}
