package pageobjects.mamikos.tenant.jobVacancies;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class JobVacancyDetailsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public JobVacancyDetailsPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @AndroidFindBy(id = "vacancyCallTextView")
    private WebElement applyNowButton;

    @AndroidFindBy(accessibility = "Navigate up")
    private WebElement backButtonArrow;

    @AndroidFindBy(id = "vcTitle")
    private WebElement jobVacancyTitleLabel;

    @AndroidFindBy(id = "vacancyTypeTextView")
    private WebElement jobVacancyTypeLabel;

    @AndroidFindBy(id = "relatedKostView")
    private WebElement seeBoardingHousesAroundButton;

    @AndroidFindBy(id = "valueSalaryTextView")
    private WebElement salaryLabel;

    @AndroidFindBy(id = "vcCreatedAtValue")
    private WebElement dataJobVacancyOpenLabel;

    @AndroidFindBy(id = "vcClosedAtValue")
    private WebElement dataJobVacancyCloseLabel;

    @AndroidFindBy(id = "vcChargeValue")
    private WebElement phoneNumberResponsiblePersonLabel;

    @AndroidFindBy(id = "vcDescriptionValue")
    private WebElement descriptionLabel;

    @AndroidFindBy(id = "vcMapLayout")
    private WebElement locationMap;

    /**
     * Click on apply now button
     */
    public void clickOnApplyNowButton() throws InterruptedException {
        appium.hardWait(2);
        appium.clickOn(applyNowButton);
    }

    /**
     * Click on back button arrow
     */
    public void clickOnBackButtonArrow() {
        appium.clickOn(backButtonArrow);
    }

    /**
     * Verify element job title is present
     */
    public void jobTitleIsPresent() {
        appium.isElementDisplayed(jobVacancyTitleLabel);
    }

    /**
     * Verify element job type is present
     */
    public void jobTypeIsPresent() {
        appium.isElementDisplayed(jobVacancyTypeLabel);
    }

    /**
     * Verify element see boarding hose arround button is present
     */
    public void seeBoardingHousesAroundButtonIsPresent() {
        appium.isElementDisplayed(seeBoardingHousesAroundButton);
    }

    /**
     * Verify element salary is present
     */
    public void salaryIsPresent() {
        appium.isElementDisplayed(salaryLabel);
    }

    /**
     * Verify element date job vacancy open is present
     */
    public void dateJobVacancyOpenIsPresent() {
        appium.isElementDisplayed(dataJobVacancyOpenLabel);
    }

    /**
     * Verify element date job vacancy close is present
     */
    public void dateJobVacancyCloseIsPresent() {
        appium.isElementDisplayed(dataJobVacancyCloseLabel);
    }

    /**
     * Verify element phone number responsible person is present
     */
    public void phoneNumberResponsiblePersonIsPresent() {
        appium.isElementDisplayed(phoneNumberResponsiblePersonLabel);
    }

    /**
     * Verify element description is present
     */
    public void descriptionIsPresent() {
        appium.isElementDisplayed(descriptionLabel);
    }

    /**
     * Verify element location map is present
     */
    public void locationMapIsPresent() {
        appium.isElementDisplayed(locationMap);
    }
}
