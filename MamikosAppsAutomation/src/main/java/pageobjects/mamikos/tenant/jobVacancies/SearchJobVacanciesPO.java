package pageobjects.mamikos.tenant.jobVacancies;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class SearchJobVacanciesPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public SearchJobVacanciesPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @AndroidFindBy(xpath = "(//*[@resource-id='com.git.mami.kos:id/labelLocationTextView'])[1]")
    private WebElement firstPopularSearch;

    /**
     * Click on first popular search
     */
    public void clickOnFirstPopularSearch() throws InterruptedException {
        appium.waitTillElementIsVisible(firstPopularSearch);
        appium.clickOn(firstPopularSearch);
    }
}
