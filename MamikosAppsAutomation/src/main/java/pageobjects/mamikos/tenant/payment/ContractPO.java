package pageobjects.mamikos.tenant.payment;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class ContractPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ContractPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "titleEmptyCheckInTextView")
    private WebElement titleMessage;

    @AndroidFindBy(id = "detailEmptyCheckInTextView")
    private WebElement bodyMessage;

    @AndroidFindBy(id = "confirmSeekButton")
    private WebElement searchKosButton;

    @AndroidFindBy(id = "valueCheckOutDateTextView")
    private WebElement rentDurationLabel;

    @AndroidFindBy(id = "checkInTextView")
    private WebElement checkInButton;

    @AndroidFindBy(id = "confirmButton")
    private WebElement checkInConfirmationButton;

    @AndroidFindBy(id = "finishRentTextView")
    private WebElement terminateContractLink;

    @AndroidFindBy(id = "dateCheckoutEditText")
    private WebElement dateTerminateContractTextBox;

    @AndroidFindBy(xpath = "(//*[@class='android.widget.RadioGroup'])[1]")
    private WebElement firstDateList;

    @AndroidFindBy(id = "editReviewFinishedFormView")
    private WebElement addReviewBoxLabel;

    @AndroidFindBy(id = "cleanRatingBar")
    private WebElement cleanStarRatingBar;

    @AndroidFindBy(id = "comfortableRatingBar")
    private WebElement comfortableStarRatingBar;

    @AndroidFindBy(id = "safetyRatingBar")
    private WebElement safetyStarRatingBar;

    @AndroidFindBy(id = "priceRatingBar")
    private WebElement priceStarRatingBar;

    @AndroidFindBy(id = "facilityRatingBar")
    private WebElement facilityStarRatingBar;

    @AndroidFindBy(id = "publicFacilityRatingBar")
    private WebElement publicFacilityStarRatingBar;

    @AndroidFindBy(id = "reviewValueEditText")
    private WebElement reviewTextBox;

    @AndroidFindBy(id = "submitReviewButton")
    private WebElement saveReviewButton;

    @AndroidFindBy(id = "finishContractButton")
    private WebElement applyTerminateContractButton;

    @AndroidFindBy(id = "viewTerminateCalendarSelectButton")
    private WebElement clickPilihTerminateDateButton;


    /**
     * Get title message when contract is empty
     */
    public String getTitleMessage() {
        return appium.getText(titleMessage);
    }

    /**
     * Get body message when contract is empty
     */
    public String getBodyMessage() {
        return appium.getText(bodyMessage);
    }

    /**
     * Verify search kos button is present when contract is empty
     */
    public Boolean isPresentSearchKosButton() {
        return appium.isElementDisplayed(searchKosButton);
    }

    /**
     * Get rent period from kost saya
     * @return string
     */
    public String getRentPeriod() throws InterruptedException {
        appium.hardWait(3);
        return appium.getText(rentDurationLabel);
    }

    /**
     * Click on terminate contract link
     */
    public void clickOnTerminateContract() throws InterruptedException {
        appium.hardWait(50);
        appium.tapOrClick(terminateContractLink);
    }

    /**
     * Select terminate reason
     * @param reason for terminate kos
     */
    public void selectReasonTerminateContract(String reason) {
        WebElement element = driver.findElement(By.xpath("//*[@text='" + reason + "']"));
        appium.tapByElementLocation(element);
    }

    /**
     * Select terminate date
     */
    public void selectTerminateDate() throws InterruptedException {
        appium.hardWait(2);
        appium.clickOn(dateTerminateContractTextBox);
    }

    /**
     * Click on give a review
     */
    public void clickOnGiveReview() {
//        appium.scrollToElementByText("");
        appium.tapOrClick(addReviewBoxLabel);
    }

    public void setStarRatingBar(int rating, String option) {
        double rate;
        WebElement element = null;

        if (rating == 1){
            rate = 0.2;
        }else if(rating == 2){
            rate = 0.4;
        }else if(rating == 3){
            rate = 0.6;
        }else if(rating == 4){
            rate = 0.8;
        }else{
            rate = 1.0;
        }

        switch (option) {
            case "Kebersihan":
                appium.setStarRatingBar(cleanStarRatingBar, rate);
                break;
            case "Keamanan":
                appium.setStarRatingBar(safetyStarRatingBar, rate);
                break;
            case "Kenyamanan":
                appium.setStarRatingBar(comfortableStarRatingBar, rate);
                break;
            case "Fasilitas Umum":
                appium.setStarRatingBar(publicFacilityStarRatingBar, rate);
                break;
            case "Fasilitas Kamar":
                appium.setStarRatingBar(facilityStarRatingBar, rate);
                break;
            case "Harga":
                appium.setStarRatingBar(priceStarRatingBar, rate);
                break;
        }
    }

    public void enterKostReview(String text) {
        appium.enterText(reviewTextBox, text, true);
    }

    public void clickOnTerminateContractButton() throws InterruptedException {
        appium.hardWait(1);
        appium.scrollToElementByResourceId("submitReviewButton");
        appium.hardWait(2);
        appium.tapOrClick(saveReviewButton);
    }

    public void clickOnApplyTerminateContract() {
        appium.scrollToElementByResourceId("finishContractButton");
        appium.tapOrClick(applyTerminateContractButton);
    }

    /**
     * Select terminate date
     */
    public void clickPilihTerminateDate() throws InterruptedException {
        appium.hardWait(2);
        appium.clickOn(clickPilihTerminateDateButton);
    }
}
