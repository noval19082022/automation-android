package pageobjects.mamikos.tenant.payment;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

public class PaymentPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;


    //Payment Data
    private String PAYMENT ="src/test/resources/testdata/mamikos/payment.properties";
    private String noHP = JavaHelpers.getPropertyValueForSquad(PAYMENT,"noHPOvo");

    public PaymentPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "titleEmptyCheckInTextView")
    private WebElement titleMessage;

    @AndroidFindBy(xpath = "//*[@text='Bayar Sekarang']")
    private WebElement payNowButton;

    @AndroidFindBy(xpath = "//*[@text='No. Virtual Account']/following-sibling::android.view.View[1]")
    private WebElement virtualAccountNumber;

    @AndroidFindBy(xpath = "//*[@text='Kode Pembayaran']/following-sibling::android.view.View[1]")
    private WebElement kodePembayaranPermata;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Saya Sudah Bayar']")
    private WebElement alreadyPaidButton;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Sudah Bayar']")
    private WebElement alreadyPaidConfirmationButton;

    @AndroidFindBy(xpath = "//*[@text='Pembayaran Berhasil']")
    private WebElement paymentSuccessfullyView;

    @AndroidFindBy(xpath = "//*[@text='Selesai']")
    private WebElement finishButton;

    @AndroidFindBy(id = "com.android.chrome:id/close_button")
    private WebElement closeMobileBrowser;

    private By chooseButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Pilih']"): By.id("Pilih");

    @AndroidFindBy(id = "invoiceEwalletForm")
    private WebElement noOvoTextBox;

    private By inputNoOvoTextBox = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@class='android.widget.EditText' and @index='0']"): By.xpath("//XCUIElementTypeOther[@name='Invoice - MamiPAY']/XCUIElementTypeTextField");

    @AndroidFindBy(xpath = "//*[@text='Kode Perusahaan']/following-sibling::android.view.View[1]")
    private WebElement kodePerusahaanMandiri;

    @AndroidFindBy(xpath = "//*[@text='Saya Sudah Bayar']")
    private WebElement sayaSudahBayarButton;

    @AndroidFindBy(xpath = "//*[@text='Sudah Bayar']")
    private WebElement confirmSayaSudahBayarButton;

    @AndroidFindBy(xpath = "//*[@resource-id='universalInvoiceContainer']/child::android.view.View/android.widget.TextView")
    private WebElement pembayaranBerhasilText;

    private By selesaiPembayaranButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.Button[@text='Saya Sudah Bayar']"): By.id("Selesai");

    private By doneButton = By.xpath("//XCUIElementTypeButton[@name='Done']");

    /**
     * Check if payment method is present in viewport
     * @return true or false
     */
    public boolean isPaymentMethodPresent(String paymentMethod) {
       // By paymentMethodElement = Constants.MOBILE_OS == Platform.ANDROID ? By.id(""+paymentMethod+""): By.id(""+paymentMethod+"");
        By paymentMethodElement = By.id(""+paymentMethod+"");
        return appium.waitInCaseElementVisible(paymentMethodElement, 1) != null;
    }

    /**
     * Select bank for payment method
     */
    public void selectPaymentMethod(String paymentMethod) throws InterruptedException {
        String element = "";
        if (Constants.MOBILE_OS == Platform.ANDROID){
            int counter = 0;
            while (counter < 3) {
                appium.scroll(0.5, 0.70, 0.30, 2000);
                counter++;
            }
            appium.waitInCaseElementVisible(By.xpath("//*[@text='" + paymentMethod +"']"),20);
            element = "//*[@text='" + paymentMethod +"']";
        } else {
            element = "//XCUIElementTypeStaticText[@name='" + paymentMethod + "']";
            int i = 0;
            do {
                appium.scroll(0.5, 0.8, 0.2, 2000);
                if(i == 10) {
                    break;
                }
                i++;
            }
            while (!isPaymentMethodPresent(paymentMethod));
        }
        appium.hardWait(2);
        switch (paymentMethod){
            case "OVO":
                appium.tapOrClick(driver.findElement(By.xpath(element)));
                appium.waitInCaseElementVisible(inputNoOvoTextBox,3);
                appium.tapOrClick(inputNoOvoTextBox);
                appium.enterText(inputNoOvoTextBox, noHP, false);
                if (Constants.MOBILE_OS== Platform.IOS) {
                    appium.tapOrClick(doneButton);
                }
                appium.hardWait(2);
                break;
            default:
                appium.tapOrClick(driver.findElement(By.xpath(element)));
                break;
        }
    }


    /**
     * Click Pay Now Button
     */
    public void clickOnPayNowButton(String button) throws InterruptedException {
        appium.hardWait(5);
        if (Constants.MOBILE_OS ==Platform.ANDROID){
            appium.scrollToElementByText(button);
            appium.clickOn(By.xpath("//*[@text='" + button + "']"));
        }else {
            appium.tapOrClick(By.xpath("//XCUIElementTypeButton[@name='"+button+"']"));
        }
    }

    /**
     * Get virtual account number
     *@return VA
     */
    public String getVirtualAccountNumber() {
        return appium.getText(virtualAccountNumber);
    }

    /**
     * Get Kode Pembayaran Bank Permata
     *@return VA
     */
    public String getKodePembayaranPermata() {
        return appium.getText(kodePembayaranPermata);
    }

    /**
     * Verify payment successfully view is present
     * Finish button
     * Click finish button
     * Close web view
     */
    public void paymentSuccessfullyViewIsPresentAndCloseWebView() {
        appium.isElementDisplayed(paymentSuccessfullyView);
        appium.scrollToElementByText("Selesai");
        appium.tapOrClick(finishButton);
        appium.tapOrClick(closeMobileBrowser);
    }

    /**
     * Verify payment successfully view is present
     * Finish button
     * Click finish button
     */
    public void paymentSuccessfullyViewIsPresent() {
        appium.scroll(0.5, 0.20, 0.80, 2000);
        appium.waitInCaseElementVisible(paymentSuccessfullyView,5);
        appium.scroll(0.5, 0.90, 0.10, 2000);
        appium.tapOrClick(finishButton);
    }

    /**
     * Click on already paid button
     * Click on already paid confirmation button
     * Click on finish button
     */
    public void clickOnAlreadyPaidButton() throws InterruptedException {
        appium.scrollToElementByText("Ubah Metode Pembayaran");
        appium.tapOrClick(alreadyPaidButton);
        appium.tapOrClick(alreadyPaidConfirmationButton);
        appium.isElementDisplayed(paymentSuccessfullyView);
        appium.scrollToElementByText("Selesai");
        appium.tapOrClick(finishButton);
    }

    /**
     * Get Kode perusahaan mandiri
     *@return Kode Perusahaan
     */
    public String getKodePerusahaanMandiri() {
        return appium.getText(kodePerusahaanMandiri);
    }

    /**
     * Click Saya Sudah Bayar Button
     */
    public void clickSayaSudahBayarButton() throws InterruptedException {
        appium.hardWait(5);
        appium.scrollToElementByText("Saya Sudah Bayar");
        appium.hardWait(3);
        appium.tapOrClick(sayaSudahBayarButton);
    }

    /**
     * Click Confirm Saya Sudah Bayar Button
     */
    public void clickConfirmSayaSudahBayarButton() throws InterruptedException {
        appium.waitInCaseElementVisible(confirmSayaSudahBayarButton,3);
        appium.tapOrClick(confirmSayaSudahBayarButton);
    }

    /**
     * Click on Pilih metode Pembayaran
     *
     */
    public void clickPilihMetodePembayaran(){
        appium.waitInCaseElementVisible(chooseButton,10);
        appium.tapOrClick(chooseButton);
    }

    /**
     * Get Text Pembayaran Berhasil
     *@return text
     */
    public String getTextPembayaranBerhasil() {
        return appium.getText(pembayaranBerhasilText);
    }

    /**
     * Check if selesai pembayaran button is present in viewport
     * @return true or false
     */
    public boolean isSelesaiPembayaranButtonPresent() {
        return appium.waitInCaseElementVisible(selesaiPembayaranButton, 1) != null;
    }

    /**
     * Click on Selesai Pembayaran button
     *
     */
    public void clickSelesaiPembayaranButton() throws InterruptedException{
        appium.hardWait(3);
        if (Constants.MOBILE_OS == Platform.ANDROID){
            int counter = 0;
            while (counter < 3) {
                appium.scroll(0.5, 0.70, 0.30, 2000);
                counter++;
            }
            appium.clickOn(selesaiPembayaranButton);
        }else {
            int i = 0;
            do {
                appium.scroll(0.5, 0.8, 0.2, 2000);
                if (i == 10) {
                    break;
                }
                i++;
            } while (!isSelesaiPembayaranButtonPresent());
            appium.tapOrClick(selesaiPembayaranButton);
        }
    }
}
