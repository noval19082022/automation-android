package pageobjects.mamikos.tenant.search;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;


public class ApartmentDetailPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ApartmentDetailPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        ;

        //This initElements method will create all Android Elements
        PageFactory.initElements(driver, this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "com.git.mami.kos:id/imageApartmentView")
    private WebElement apartmentPhotos;

    @AndroidFindBy(id = "com.git.mami.kos:id/conditionApartmentTextView")
    private WebElement apartmentFurnitureType;

    @AndroidFindBy(id = "com.git.mami.kos:id/nameApartmentTextView")
    private WebElement apartmentNameOnDetailPage;

    @AndroidFindBy(id = "com.git.mami.kos:id/addressApartmentTextView")
    private WebElement apartmentAddress;

    @AndroidFindBy(id = "com.git.mami.kos:id/contactApartmentUpdateTextView")
    private WebElement apartmentLastUpdate;

    @AndroidFindBy(id = "shareApartmentImageView")
    private WebElement apartmentShareButton;

    @AndroidFindBy(id = "loveApartmentImageView")
    private WebElement apartmentLoveButton;

    @AndroidFindBy(id = "contactApartmentTextView")
    private WebElement contactApartmentButton;

    @AndroidFindBy(id = "mainMapView")
    private WebElement mapView;


    /**
     * verify element apartment photos is present
     */
    public Boolean isApartmentPhotosPresent() {
        return appium.isElementDisplayed(apartmentPhotos);
    }

    /**
     * verify element apartment photos is present
     */
    public Boolean isApartmentFurnitureTypePresent() {
        return appium.isElementDisplayed(apartmentFurnitureType);
    }

    /**
     * verify element apartment photos is present
     */
    public Boolean isApartmentNameOnDetailPagePresent() {
        return appium.isElementDisplayed(apartmentNameOnDetailPage);
    }

    /**
     * verify element apartment photos is present
     */
    public Boolean isApartmentAddressPresent() {
        return appium.isElementDisplayed(apartmentAddress);
    }

    /**
     * verify element apartment photos is present
     */
    public Boolean isApartmentLastUpdatePresent() {
        return appium.isElementDisplayed(apartmentLastUpdate);
    }

    /**
     * Click on share button of apartment
     */
    public void clickOnApartmentShareButton() throws InterruptedException {
        appium.hardWait(5);
        appium.tapOrClick(apartmentShareButton);
    }

    /**
     * Click on love button
     */
    public void clickOnApartmentLoveButton() throws InterruptedException {
        appium.tapOrClick(apartmentLoveButton);
        appium.hardWait(2);
    }

    /**
     * Click on Contact apartment button
     */
    public void clickOnContactApartmentButton() throws InterruptedException {
        appium.hardWait(3);
        appium.tapOrClick(contactApartmentButton);
    }

    /**
     * Is Map view present ?
     * @return true or false
     */
    public boolean isMapViewPresent() {
        return appium.waitInCaseElementVisible(mapView, 5) != null;
    }

    /**
     * Navigate to recommended boarding section
     */
    public void navigateToRecommendedBoardingSection() {
        appium.scrollToElementByResourceId("relatedVacancyTextView");
    }

    /**
     * Apartment Type is present or not
     *
     * @param apartmentType apartment type
     * @return true or false
     */
    public boolean isApartmentTypePresent(String apartmentType) {
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + apartmentType + "']"), 5) != null;
    }

    /**
     * Navigate to Interesting other apartment section
     */
    public void navigateToOtherInterestingApartmentSection() {
        appium.scrollToElementByResourceId("relatedRoomTextView");
    }

    /**
     * Click on share option from share shet
     * @param shareOption share option
     */
    public void clickOnShareOptionFromShareSheet(String shareOption){
        String xpathLocator ="//*[@text='"+ shareOption +"']";
        appium.clickOn(By.xpath(xpathLocator));
    }

    /**
     * Wait for apartment detail to load
     */
    public void waitApartmentDetailToLoad() {
        appium.waitTillElementIsVisible(apartmentNameOnDetailPage, 13);
    }
}
