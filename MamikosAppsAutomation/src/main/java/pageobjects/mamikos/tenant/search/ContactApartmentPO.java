package pageobjects.mamikos.tenant.search;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class ContactApartmentPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public ContactApartmentPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "toolbarTextView")
    private WebElement contactApartmentPageHeader;

    @AndroidFindBy(id = "backImageView")
    private WebElement backButton;

    @AndroidFindBy(id = "confirmSendMessageButton")
    private WebElement sendButton;


    /**
     * Check contact apartment page header displayed or not
     * @return true or false
     */
    public boolean isContactApartmentPageHeaderDisplayed(){
        return appium.waitInCaseElementVisible(contactApartmentPageHeader, 5)!=null;
    }

    /**
     * Check back button displayed or not
     * @return true or false
     */
    public boolean isBackButtonDisplayed(){
        return appium.waitInCaseElementVisible(backButton, 5)!=null;
    }

    /**
     * Check send button displayed or not
     * @return true or false
     */
    public boolean isSendButtonDisplayed(){
        return appium.waitInCaseElementVisible(sendButton, 5)!=null;
    }

    /**
     * Click on send button
     */
    public void clickOnSendButton(){
        appium.tapOrClick(sendButton);
    }

}
