package pageobjects.mamikos.tenant.search;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pageobjects.mamikos.common.LoadingPO;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.ArrayList;
import java.util.List;

public class FilterPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;
    private LoadingPO loading;

    public FilterPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        loading = new LoadingPO(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "valueApartmentTypeTextView")
    private WebElement unitTypeTextbox;

    private By furnitureTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("valueApartmentFurnishTextView"): By.xpath("//*[@name='Perabotan']/following::XCUIElementTypeTextField[1]");

    private By periodTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("apartmentTimeTextView"): By.xpath("//*[@name='Jangka Waktu']/following::XCUIElementTypeTextField[1]");

    private By minPriceRangeTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("minPriceApartmentTextView"): By.xpath("//*[@name='Range Harga']/following-sibling::XCUIElementTypeTextField[1]");

    private By maxPriceRangeTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("maxPriceApartmentTextView"): By.xpath("//*[@name='Range Harga']/following-sibling::XCUIElementTypeTextField[2]");

    @AndroidFindBy(id = "saveFilterTextView")
    private WebElement saveButton;

    private By resetButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("deleteButtonCV"): By.xpath("//XCUIElementTypeButton[@name='Hapus']");

    @AndroidFindBy(id = "confirmFilterTextView")
    private WebElement searchButton;

    @AndroidFindBy(id = "filterView")
    private WebElement filter;

    @AndroidFindBy(id = "com.git.mami.kos:id/genderTextView")
    private WebElement filterGender;

    @AndroidFindBy(id = "displayKosButtonView")
    private WebElement submitFilterButton;

    @AndroidFindBy(id = "com.git.mami.kos:id/timePeriodTextView")
    private WebElement timePeriod;

    @AndroidFindBy(id = "com.git.mami.kos:id/minPriceEditText")
    private WebElement minPrice;

    @AndroidFindBy(id = "com.git.mami.kos:id/maxPriceEditText")
    private WebElement maxPrice;

    @AndroidFindBy(id = "com.git.mami.kos:id/valueMinimumPaymentTextView")
    private WebElement minPayment;

    @AndroidFindBy(id = "com.git.mami.kos:id/tv_ok")
    private WebElement chooseGender;

    @AndroidFindBy(id = "com.git.mami.kos:id/genderTextView")
    private WebElement listingGender;

    @AndroidFindBy(id = "titleLoadMoreTextView")
    private WebElement loadDataListingLabel;

    @AndroidFindBy(id = "nameApartmentTextView")
    private WebElement apartmentName;

    private By furnitureLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.id("furnishTagTextView"): By.xpath("(//*[@name='Furnished'])[1]");

    @AndroidFindBy(xpath = "(//android.widget.TextView[contains(@resource-id,'com.git.mami.kos:id/roomFacilityTextView')])[1]")
    private WebElement unitTypeLabel;

    private By firstPeriodLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//android.widget.TextView[contains(@resource-id,\"com.git.mami.kos:id/priceTextView\")])[1]"): By.xpath("(//*[@name='Apartemen'])[1]/following-sibling::XCUIElementTypeStaticText[3]");

    @AndroidFindBy(xpath = "(//android.widget.LinearLayout[contains(@resource-id,'com.git.mami.kos:id/mainApartmentItemView')])[1]")
    private WebElement firstListView;

    @AndroidFindBy(id = "titleSortingTextView")
    private WebElement sortingOptionTitle;

    @AndroidFindBy(xpath = "//*[@text='Range Harga']")
    private WebElement priceRangeHeading;

    private By closeFilterIcon = By.id("closeImageView");

    private By cariFilterButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("confirmFilterTextView"): By.xpath("//XCUIElementTypeButton[@name='CARI']");

    private By doneButton = By.xpath("//XCUIElementTypeButton[@name='Done']");

    /** verify current apartment filter list
     * @param filterList is item filter list
     */
    public boolean filterListPresent(String filterList) {
        By filterLIstLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='" + filterList + "']"): By.xpath("//*[@value='" + filterList + "']");
        return appium.waitInCaseElementVisible(filterLIstLabel, 5) != null;
    }

    /** verify current button on page filter list
     * @param button button present on page apartment filter list
     */
    public boolean buttonPresent(Object button) {
        By filterButton = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='" + button + "']"): By.xpath("//*[@value='" + button + "']");
        return appium.waitInCaseElementVisible(filterButton, 5) != null;
    }

    /** select option unit type
     * @param filterBy want to filter using criteria
     * @param filterValue is value filter for the criteria
     */
    public void filterApartment(String filterBy, String filterValue) throws InterruptedException {
        appium.hardWait(5);
        switch (filterBy) {
            case "unit type":
                appium.tapOrClick(unitTypeTextbox);
                break;
            case "furniture":
                appium.tapOrClick(furnitureTextbox);
                break;
            case "time period":
                appium.tapOrClick(periodTextbox);
                break;
        }
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.clickOn(By.xpath("//*[@text='" + filterValue + "']"));
            appium.waitTillElementIsClickable(cariFilterButton);
        }else {
            MobileElement elKey = (MobileElement) driver.findElement(By.className("XCUIElementTypePickerWheel"));
            elKey.setValue(filterValue);
        }
        appium.tapOrClick(cariFilterButton);
    }

    /** select option unit type
     * @param filterBy want to filter using criteria
     * @param minPrice is value filter for the criteria
     * @param maxPrice is value filter for the criteria
     */
    public void filterApartment(String filterBy, String minPrice, String maxPrice) throws InterruptedException {
        appium.hardWait(5);
        if (filterBy.equals("price range")){
            appium.enterText(minPriceRangeTextbox,minPrice,true);
            appium.enterText(maxPriceRangeTextbox,maxPrice,true);
        }
        if (Constants.MOBILE_OS == Platform.IOS) {
            appium.tapOrClick(doneButton);
        }
        appium.tapOrClick(cariFilterButton);
    }

    /** select option unit type
     * @param filterBy want to filter using criteria
     * @param filterValue is value filter for the criteria
     */
    public void resetFilterApartment(String filterBy, String filterValue) throws InterruptedException {
        switch (filterBy) {
            case "unit type":
                appium.isElementDisplayed(unitTypeTextbox);
                appium.tapOrClick(unitTypeTextbox);
                break;
            case "furniture":
                appium.isElementPresent(furnitureTextbox);
                appium.tapOrClick(furnitureTextbox);
                break;
            case "time period":
                appium.isElementPresent(periodTextbox);
                appium.tapOrClick(periodTextbox);
                break;
        }
        appium.clickOn(By.xpath("//*[@text='" + filterValue + "']"));
    }


    /**
     * Tap filter button
     */
    public void tapFilter() {
        appium.waitInCaseElementVisible(filter,5);
        appium.tapOrClick(filter);
    }

    /**
     * Tap filter by time period button in Filter page
     */
    public void filterTime() {
        appium.scrollToElementByResourceId("lineRentTypeView");
    }

    /**
     * tap button "Cari" in filter menu then wait for listing appear
     */
    public void tapSearch() throws InterruptedException {
        appium.hardWait(3);
        appium.tapOrClick(submitFilterButton);
        if(appium.waitInCaseElementVisible(submitFilterButton, 3) != null){
            appium.tapOrClick(submitFilterButton);
        }
        appium.waitInCaseElementVisible(listingGender, 3);
    }

    /** Get listing status by gender from top listing
     * @param fromTop is number, from top to x
     */
    public List<String> getListGender(int fromTop) {
        List<String> listupdate = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            String update1 = appium.getText(listingGender);
            listupdate.add(update1);
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        //back to top
        for (int i = 0; i < fromTop; i++) {
            appium.scroll(0.5, 0.20, 0.80, 2000);
        }
        return listupdate;
    }

    /**
     * tap any option in filter by dynamic xpath, will click it's neighbor
     */
    public void tapOption(String option) {
        String xpathLocator = "//android.widget.TextView[contains(@text, '" + option + "')]";
        MobileElement element = (MobileElement) driver.findElementByXPath(xpathLocator);
        appium.tapOrClick(element);
    }

    /**
     * Click on filter option by Gender
     * Uncheck all checkbox first then check the desired option
     * @param gender
     */
    public void selectGender(String gender) {
        this.tapOption("Campur");
        this.tapOption("Putra");
        this.tapOption("Putri");
        this.tapOption("Campur");
        this.tapOption("Putra");
        this.tapOption("Putri");
        this.tapOption(gender);
    }

    /**
     * Set minimum price in filter
     * @param textPrice
     */
    public void setMinPrice(String textPrice) {
        appium.scrollToElementByResourceId("priceTextView");
        appium.tapOrClick(minPrice);
        appium.enterText(minPrice, textPrice, true);
    }

    /**
     * Set maximal price in filter
     * @param textPrice2
     */
    public void setMaxPrice(String textPrice2) {
        appium.tapOrClick(maxPrice);
        appium.enterText(maxPrice, textPrice2, true);
        appium.hideKeyboard();
    }

    /** view detail apartment from list to display detail apartment
     */
    public void viewApartmentDetail(){
        appium.waitTillElementIsClickable(firstListView);
        appium.tapOrClick(firstListView);
        appium.waitInCaseElementVisible(apartmentName, 5);
    }

    /** Get label unit type display on screen apartment detail
     * @param filterBy for filter apartment by specific criteria
     */
    public String getApartmentUnitType(String filterBy) throws InterruptedException {
        String valueText = null;
        switch (filterBy) {
            case "unit type":
                this.viewApartmentDetail();
                loading.waitLoadingFinish();
                appium.hardWait(5);
                appium.scroll(0.5,0.8, 0.2, 2000);
                valueText = appium.getText(unitTypeLabel);
                break;
            case "furniture":
                valueText = appium.getText(furnitureLabel);
                break;
            case "time period":
                valueText = appium.getText(firstPeriodLabel).replaceAll("^[0-9\\p{Punct}/\\s]+", "").trim();
                break;
        }
        return valueText;
    }

    /** Verify element close on sorting screen is present
     */
    public Boolean isCloseIconPresent() {
        return appium.isElementDisplayed(sortingOptionTitle);
    }

    /**
     * Check filter page heading present ?
     * @return true or false
     */
    public boolean isFilterPageHeadingOfMapPresent(){
        return appium.waitInCaseElementVisible(priceRangeHeading, 5)!=null;
    }
    /**
     * Check reset button present ?
     * @return true or false
     */
    public boolean isResetButtonPresent(){
        return appium.isElementPresent(resetButton);
    }

    /**
     * Check search button present ?
     * @return true or false
     */
    public boolean isSearchButtonPresent(){
        return appium.waitInCaseElementVisible(searchButton, 5)!=null;
    }

    /**
     * Check close button present
     * @return true or false
     */
    public boolean isCloseIconFilterPresent() {
        return appium.isElementPresent(closeFilterIcon);
    }

    /**
     * Check wording of elements is present
     * @return true or false
     * @param word for select element by specific wording
     */
    public boolean isWordingElementPresent(String word) {
        String wordingFilter = "";
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.scrollToElementByText(word);
            wordingFilter = "//*[@text='" + word + "']";
        }else{
            wordingFilter = "//*[@value='" + word + "']";
        }
        return appium.waitInCaseElementVisible(By.xpath(wordingFilter), 5) != null;
    }

    /**
     * click reset button on filter page
     */
    public void clickResetButton() throws InterruptedException {
        appium.clickOn(resetButton);
        appium.hardWait(3);
    }
}
