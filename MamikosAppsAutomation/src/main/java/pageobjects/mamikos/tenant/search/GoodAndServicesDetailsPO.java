package pageobjects.mamikos.tenant.search;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class GoodAndServicesDetailsPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public GoodAndServicesDetailsPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    @AndroidFindBy(id = "marketPriceTextView")
    private WebElement marketPriceText;

    @AndroidFindBy(id = "Navigate up")
    private WebElement backButton;

    @AndroidFindBy(id = "com.git.mami.kos:id/callMarketBigTextView")
    private WebElement chatButton;

    @AndroidFindBy(id = "com.git.mami.kos:id/titleTextView")
    private WebElement textTittle;

    @AndroidFindBy(id = "com.git.mami.kos:id/marketAddressTextView")
    private WebElement textAddress;

    @AndroidFindBy(id = "com.git.mami.kos:id/descriptionValueTextView")
    private WebElement textDescription;

    @AndroidFindBy(id = "Lokasi Barang/Jasa.")
    private WebElement mapsArea;

    @AndroidFindBy(id = "com.git.mami.kos:id/callMarketSmallTextView")
    private WebElement chatbutton2;

    @AndroidFindBy(id = "com.git.mami.kos:id/marketSoldTextView")
    private WebElement soldOutLabel;

    @AndroidFindBy(accessibility = "Navigate up")
    private WebElement backButtonArrow;


    /**
     * Get market price value
     * @return price value
     */
    public String getMarketPrice() throws InterruptedException {
        appium.hardWait(3);
        return appium.getText(marketPriceText);
    }

    /**
     * validate reach detail page
     * @return tittle, price, chat
     */
    public boolean isOnDetailPage(){
        return
                appium.waitInCaseElementVisible(backButton, 5) != null
                        && appium.waitInCaseElementVisible(chatButton,5) != null
                        && appium.waitInCaseElementVisible(textTittle,5) != null
                        && appium.waitInCaseElementVisible(marketPriceText,5) != null;
    }

    /**
     * tap back button
     */
    public void tapBackButton(){
        appium.tap(backButton);
    }

    /**
     * validate chat button is active
     */
    public boolean isChatButtonEnable() {
        try{
            if (chatButton.isEnabled()) {
                return true;
            } else {
                return false;
            }
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * tap chat button
     */
    public void tapChatButton(){
        appium.tap(chatButton);
    }

    /**
     * tap scroll to end detail page
     */
    public void scrollToEndPage(){
        appium.scrollDown(2, 1);
    }

    /**
     * validate incative ads/soldout ads
     */
    public boolean validateInactiveAds() throws InterruptedException {
        return appium.waitInCaseElementVisible(soldOutLabel,5) != null;
    }

    /**
     * Click on back button arrow
     */
    public void clickOnBackButtonArrow() {
        appium.clickOn(backButtonArrow);
    }
}
