package pageobjects.mamikos.tenant.search;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;

import java.util.ArrayList;
import java.util.List;

public class SearchListingPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public SearchListingPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);
        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @AndroidFindBy(xpath = "//android.view.View[@content-desc='25. rooms.']")
    private WebElement dotMap;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Lihat Daftar\"]")
    private WebElement tampilanDaftar;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Lihat Peta\"]")
    private WebElement mapViewTab;

    private By filterIcon = By.id("filterTopIconCV");

    @AndroidFindBy(id = "sortFilterView")
    private WebElement sortingIcon;

    @iOSXCUITFindBy(accessibility = "ic green arrown left")
    @AndroidFindBy(id = "exitSearchImageView")
    private WebElement backButton;

    @AndroidFindBy(id = "com.git.mami.kos:id/searchLocationTextView")
    private WebElement searchLocationEditText;

    @iOSXCUITFindBy(xpath = "\t//XCUIElementTypeStaticText[@name=\"Yogyakarta\"]")
    @AndroidFindBy(xpath = "//*[@text='Yogyakarta']")
    private WebElement yogyakarta;

    @iOSXCUITFindBy(xpath = "//*[@value=‘Putri’]/preceding-sibling::*")
    @AndroidFindBy(id = "roomTitleTextView")
    private WebElement titleKos;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Urutkan\"]")
    @AndroidFindBy(xpath = "//*[@text='Urutkan']")
    private WebElement urutkanFilter;

    @AndroidFindBy(id = "com.git.mami.kos:id/sortRandomTextView")
    private WebElement randomSort;

    @AndroidFindBy(id = "com.git.mami.kos:id/sortDescTextView")
    private WebElement cheaperSort;

    private By expensiveSort = Constants.MOBILE_OS == Platform.ANDROID ? By.id("com.git.mami.kos:id/sortingTextView"): By.id("Harga termahal");

    @AndroidFindBy(id = "com.git.mami.kos:id/sortAvailabilityTextView")
    private WebElement availableSort;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Update terbaru\"]")
    @AndroidFindBy(id = "com.git.mami.kos:id/sortUpdateTextView")
    private WebElement lastUpdateSort;

    @AndroidFindBy(id = "actionApplyFlashSaleButton")
    private WebElement applyFlashSaleButton;

    @AndroidFindBy(id="com.git.mami.kos:id/titleTooltipTextView")
    private WebElement tooltipKosFTUE;

    @iOSXCUITFindBy(xpath = "//*[@type='XCUIElementTypeStaticText']")
    @AndroidFindBy(id = "com.git.mami.kos:id/updateTextView")
    private WebElement status1;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"/ bulan\"]")
    @AndroidFindBy(id = "priceTextView")
    private WebElement price1;

    @AndroidFindBy(id = "rentTypeTextView")
    private WebElement rentTypeText;

    @AndroidFindBy(id = "com.git.mami.kos:id/bookingTextView")
    private WebElement bookingStatus;

    @AndroidFindBy(id = "com.git.mami.kos:id/valueRoomPromoTextView")
    private WebElement sponsored;

    @AndroidFindBy(id = "canBookingTextView")
    private WebElement canBookButton;

    @iOSXCUITFindBy(accessibility = "ic green arrown left")
    @AndroidFindBy(id = "com.git.mami.kos:id/exitSearchImageView")
    private WebElement backLandingButton;

    @AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"mamikos\"])[1]")
    private WebElement mamikosLogo;

    @AndroidFindBy(xpath = "(//androidx.recyclerview.widget.RecyclerView[contains(@resource-id,'com.git.mami.kos:id/listMainAdsRecyclerView')]/child::android.widget.LinearLayout)[1]")
    private WebElement firstListView;

    private By mamikosLogoiOS = MobileBy.AccessibilityId("navBarIcon");

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"Putri\"])[1]")
    @AndroidFindBy(id = "genderTextView")
    private WebElement gender;

    @AndroidFindBy(xpath = "(//android.widget.ImageView[@content-desc=\"ImageView_MamiKost\"])[12]")
    private WebElement love;

    private By loveiOS = MobileBy.name("ic unlove white");

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeButton[@name=\"ic love white\"])[1]")
    private WebElement unLove;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Selamat Datang di Mamikos\"]")
    @AndroidFindBy(id = "titleLoginTextView")
    private WebElement login;

    @AndroidFindBy(xpath = "(//android.widget.LinearLayout[contains(@resource-id,'com.git.mami.kos:id/mainApartmentItemView')])[1]")
    private WebElement firstListViewApartment;

    @AndroidFindBy(xpath = "(//android.widget.RelativeLayout[contains(@resource-id,'com.git.mami.kos:id/roomItemView')])[1]")
    private WebElement firstListViewBoardingHouse;

    @iOSXCUITFindBy(xpath = "\t//XCUIElementTypeStaticText[@name=\"Dilihat\"]")
    @AndroidFindBy(xpath = "//*[@content-desc='Pernah Dilihat']")
    private WebElement seenTab;

    @AndroidFindBy(id = "nameApartmentTextView")
    private WebElement apartmentName;

    private By firstPeriodLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//android.widget.TextView[contains(@resource-id,\"com.git.mami.kos:id/priceTextView\")])[1]"): By.xpath("(//*[@name='Apartemen'])[1]/following-sibling::XCUIElementTypeStaticText[3]");

    @AndroidFindBy(id = "mapResultView")
    private WebElement mapView;

    private By apartmentNameOnListView = Constants.MOBILE_OS == Platform.ANDROID ? By.id("apartmentNameTextView"): By.xpath("(//XCUIElementTypeStaticText[@name='Apartemen'])[1]");

    @AndroidFindBy(id = "roomLocationTextView")
    private WebElement roomLocationText;

    @AndroidFindBy(id ="roomImageView")
    private WebElement roomImage;

    @AndroidFindBy(id = "ratingTextView")
    private WebElement ratingText;

    @AndroidFindBy(id = "roomFacilityTextView")
    private WebElement facilitiesText;

    @AndroidFindBy(id = "locationApartmentTextView")
    private List<WebElement> apartmentLocationText;

    @AndroidFindBy(id = "titleApartmentTextView")
    private WebElement apartmentTitleText;

    @AndroidFindBy(id = "tv_grid_label")
    private List<WebElement> goodAndServiceLabel;

    @AndroidFindBy(xpath = "//*[@class='androidx.recyclerview.widget.RecyclerView']/*[1]")
    private WebElement goodsAndServiceFirstRow;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[1]")
    private WebElement goodsServicePrice;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.TextView[2]")
    private WebElement goodsServiceTittle;

    @AndroidFindBy(id = "com.git.mami.kos:id/rlMarketSold")
    private WebElement soldOutLabel;

    @AndroidFindBy(id = "displayView")
    private WebElement viewMapsButton;

    @AndroidFindBy(id = "flashSaleTextView")
    private WebElement flashSaleButton;

    @AndroidFindBy(id = "favoriteImageView")
    private WebElement favoriteListingMenu;

    //-------- FTUE LISTING ---------

    @AndroidFindBy(id = "titleTooltipTextView")
    private WebElement FTUETitleText;

    @AndroidFindBy(id = "actionApplyFlashSaleButton")
    private WebElement applyFSButton;

    @AndroidFindBy(id = "actionApplyFlashSaleButton")
    private WebElement applyButton;

    //-------- EMPTY STATE LISTING ---------

    @AndroidFindBy(id = "emptyImageView")
    private WebElement emptyImage;

    @AndroidFindBy(id = "tv_zr_recommend_title")
    private WebElement emptyTitleText;

    @AndroidFindBy(id = "iv_zr_close")
    private WebElement closePopUpEmptyButton;

    @AndroidFindBy(id = "emptyButtonView")
    private WebElement searchAnotherAreaButton;

    @AndroidFindBy(id = "flashSaleImageView")
    private WebElement flashSaleIcon;

    @AndroidFindBy(id = "discountPercentageTextView")
    private WebElement discountPercentageText;

    private By sayaMengertiButton = Constants.MOBILE_OS == Platform.ANDROID ? By.id("actionApplyFlashSaleButton"): By.xpath("//XCUIElementTypeButton[@name='Saya Mengerti']");

    private By ijinkanButton = By.id("buttonPrimary");

    private By locationText = By.id("roomLocationTextView");





    // Wait for map loading
    public void waitMapLoad() {
        appium.waitTillElementIsVisible(dotMap, 11);
    }

    // Wait for listing appear
    public void waitListingAppear() {
        appium.waitTillElementIsVisible(titleKos, 11);
    }

    // Tap button List View then wait until listing appear
    public void userTapTampilanDaftarBoardingHouseList() {
        int i = 0;
        do {
            appium.tapOrClick(tampilanDaftar);
            i++;
        } while (String.valueOf(appium.isElementDisplayed(firstListView)).equals("false") || i < 2);
    }

    // Tap button List View then wait until listing appear in Kosan
    public void TapTampilanDaftarKos() {
        appium.tapOrClick(tampilanDaftar);
    }

    // Tap button "Urutkan" to open sort menu
    public void userTapUrutkan() {
        appium.waitInCaseElementVisible(urutkanFilter, 5);
        appium.tapOrClick(urutkanFilter);
    }

    /**
     * Check if filter urutkan by is present
     * @return true or false
     */
    public boolean isUrutkanByFilterPresent() {
        return appium.waitInCaseElementVisible(expensiveSort, 1) != null;
    }

    /**
     * scroll to check if filter urutkan by is present
     *
     */
    public void scrollToUrutkanFilter() {
        int i = 0;
        do {
            appium.scroll(0.5, 0.8, 0.2, 2000);
            if(i == 10) {
                break;
            }
            i++;
        }
        while (!isUrutkanByFilterPresent());
    }

    /**
     * Verify sorting list is present
     */
    public boolean getSortText(Object sort) throws InterruptedException {
        String sortFilter = "";
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.hardWait(3);
            appium.scrollToElementByText("Harga Termahal");
            sortFilter = "//*[@text='" + sort + "']";
        }else{
            sortFilter = "//*[@value='" + sort + "']";
        }
        return appium.waitInCaseElementVisible(By.xpath(sortFilter), 5) != null;
    }

    /**
     * Tap button "Acak" in sorting menu then wait for listing appear
     */
    public void userTapAcak() {
        appium.tapOrClick(randomSort);
        appium.waitTillElementIsVisible(titleKos, 21);
    }

    /**
     * Tap button "Harga Termurah" in sorting menu then wait for listing appear
     */
    public void userTapLowestPrice() {
        appium.tapOrClick(cheaperSort);
        appium.waitTillElementIsVisible(titleKos, 21);
    }

    /**
     * Tap button "Harga Termahal" in sorting menu then wait for listing appear
     */
    public void userTapHighestPrice() {
        appium.tapOrClick(expensiveSort);
        appium.waitTillElementIsVisible(titleKos, 21);
    }

    /**
     * Tap button "kosong ke penuh" in sorting menu then wait for listing appear
     */
    public void userTapKosongKePenuh() {
        appium.tapOrClick(availableSort);
        appium.waitTillElementIsVisible(titleKos, 21);
    }

    /**
     * Tap button "Terakhir Update" in sorting menu then wait for listing appear
     */
    public void userTapLatestUpdate() {
        appium.tapOrClick(lastUpdateSort);
        appium.waitTillElementIsVisible(titleKos, 21);
    }

    // tap listing title in kosan list / search result
    public void tapTitle() {
        appium.tapOrClick(titleKos);
    }

    /**
     * Get list of prices from top x listing
     *
     * @param fromTop is number, from top to x
     */
    public List<Integer> getListOfprice(int fromTop) {
        List<Integer> pricelist = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            String _price = appium.getText(price1);
            int harga = JavaHelpers.extractNumber(_price);
            pricelist.add(harga);
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return pricelist;
    }

    /**
     * Get list of payment time from top x listing
     *
     * @param fromTop is number, from top to x
     */
    public List<String> getListOfTimely(int fromTop) {
        List<String> time = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            String _price = appium.getText(rentTypeText).toLowerCase();
            String timely = _price.substring(_price.lastIndexOf("/") + 1).trim();
            time.add(timely);
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        appium.scrollUp(fromTop, 2000);
        return time;
    }

    /**
     * Get listing status from top listing
     *
     * @param fromTop is number, from top to x
     */
    public List<String> getListOfUpdate(int fromTop) {
        List<String> listupdate = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            String update1 = appium.getText(status1);
            listupdate.add(update1);
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return listupdate;
    }

    /**
     * Make sure the boarding location is in the desired location
     */
    public String getBoardingHouseLocationOnListing() {
        appium.waitInCaseElementVisible(locationText,5);
//        appium.scroll(0.5, 0.80, 0.20, 2000);
        return appium.getText(locationText);
    }

    /**
     * Click on 'Saya Mengerti' button
     * Check 'Bisa Booking' button is displayed
     * Check 'Tampilan Peta' menu is displayed
     * Check 'Tampilan Daftar' menu is displayed
     */
    public boolean tapAndCheckElementDisplayed() {
        if (Constants.MOBILE_OS==Platform.ANDROID) {
            appium.tapOrClick(applyButton);
            if(appium.waitInCaseElementVisible(tooltipKosFTUE,5) != null){
                appium.tapOrClick(tooltipKosFTUE);
            }
            return canBookButton.isDisplayed();
        }
        else
            return mapViewTab.isDisplayed();
    }


    /**
     * Click back button
     * Check Mamikos Logo
     */
    public boolean tapAndCheckLogoDisplayed() throws InterruptedException {
        if (Constants.MOBILE_OS==Platform.ANDROID) {
            return appium.waitInCaseElementVisible(mamikosLogo, 5) != null;
        } else {
                appium.tap(backLandingButton);
                appium.tap(backLandingButton);
                appium.tap(backLandingButton);
            return appium.isElementPresent(mamikosLogoiOS);
        }

    }

    /**
     * Get listing status (Booking and Sponsored) from top listing
     *
     * @param fromTop is number, from top to x
     */
    public List<String> getListAllStatus(int fromTop) {
        List<String> listAllStat = new ArrayList<>();
        for (int i = 0; i < fromTop; i++) {
            if (appium.waitInCaseElementVisible(bookingStatus, 3) != null &&
                    appium.waitInCaseElementVisible(sponsored, 3) != null) {
                String bookingStat = appium.getText(bookingStatus);
                String sponsorStat = appium.getText(sponsored);
                String finalStat = bookingStat + sponsorStat;
                listAllStat.add(finalStat);
            } else if (appium.waitInCaseElementVisible(bookingStatus, 3) == null &&
                    appium.waitInCaseElementVisible(sponsored, 3) != null) {
                String bookingStat = appium.getText(sponsored);
                listAllStat.add(bookingStat + "Only");
            } else if (appium.waitInCaseElementVisible(bookingStatus, 3) != null &&
                    appium.waitInCaseElementVisible(sponsored, 3) == null) {
                String bookingStat = appium.getText(bookingStatus);
                listAllStat.add(bookingStat + "Only");
            } else {
                listAllStat.add("NotBookingNotSponsored");
            }
            appium.scroll(0.5, 0.90, 0.20, 2000);
        }
        return listAllStat;
    }

    /**
     * view detail apartment from list to display detail apartment
     */
    public void viewApartmentDetail() {
        appium.waitTillElementIsClickable(firstListViewApartment);
        appium.tapOrClick(firstListViewApartment);
        appium.waitTillElementIsVisible(apartmentName);
    }

    /**
     * Get price period display on screen apartment detail
     */
    public String getApartmentPeriod(String filterBy, int index) throws InterruptedException {
        By firstPeriodLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.id("priceTextView"):
                By.xpath("(//*[@name='Apartemen'])["+index+"]/following-sibling::XCUIElementTypeStaticText[3]");

        String pricePeriod = null;
        if (filterBy.equals("price range")) {
            pricePeriod = appium.getText(firstPeriodLabel).replaceAll("^[0-9\\p{Punct}/\\s]+", "").trim();
            appium.scroll(0.5, 0.70, 0.30, 2000);
        }
        return pricePeriod;
    }

    /**
     * Get price display on screen apartment detail
     */
    public String getApartmentPrice(int index) throws InterruptedException {
        By priceLabel = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//android.widget.TextView[contains(@resource-id,\"com.git.mami.kos:id/priceTextView\")])[1]"):
                By.xpath("(//*[@name='Apartemen'])["+index+"]/following-sibling::XCUIElementTypeStaticText[2]");
        return appium.getText(priceLabel).replaceAll("\\D+","");
    }

    /**
     * Get price and period display on screen apartment detail
     */
    public String getApartmentPricePeriod() throws InterruptedException {
        String pricePeriod = null;
        pricePeriod = appium.getText(firstPeriodLabel);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        return pricePeriod;
    }

    /**
     * Get Kost price tag Bool
     */
    public boolean isKostPricePresent() {
        return appium.getText(price1) != null;
    }

    /**
     * Get kost name Bool
     */
    public boolean isKostNamePresent() {
        return appium.getText(titleKos) != null;
    }


    /**
     * Get gender true / false
     */

    public boolean isGenderPresent() {
        return appium.getText(gender) != null;
    }

    /**
     * Get 'Log In' text
     */
    public String getLoginText() {
        return appium.getText(login);
    }

    /**
     * Get 'Yogyakarta' Text
     */
    public String getYogyakartaText() {
        return appium.getText(yogyakarta);
    }

    /**
     * Click On 'Yogyakarta' dropdown item lists
     */
    public void clickOnYogyakarta() {
        appium.waitTillElementIsVisible(yogyakarta);
        appium.tapOrClick(yogyakarta);
    }

    /**
     * Click 'Love Button' on a single kost
     */
    public void clickOnLove() throws InterruptedException {
        appium.waitTillElementIsVisible(gender);
        if (Constants.MOBILE_OS==Platform.ANDROID){
        appium.tapOrClick(love);}
        else
        {
            if (appium.isElementPresent(loveiOS)){
                appium.waitTillElementIsClickable(loveiOS).click();
            }
            else {
                appium.tapOrClick(unLove);
            }
        }
    }


    /**
     * Navigate back
     */
    public void back() {
        appium.back();
    }

    /**
     * Click on 'Seen tab'
     */
    public void clickOnSeenTab() throws InterruptedException {
        appium.tapOrClick(seenTab);
        appium.hardWait(2);
        appium.scroll(0.5, 0.40, 0.70, 2000);
    }

    /**
     * Verify cluster map is display on screen
     */
    public boolean displayMapView() {
        return appium.isElementDisplayed(mapView);
    }

    /**
     * Select option sort for sorting apartment list
     *
     * @param sort is sorting by
     */
    public void selectOptionForSorting(String sort) throws InterruptedException {
        appium.waitTillElementIsVisible(By.xpath("//*[@text='" + sort + "']"), 10);
        appium.hardWait(1);
        appium.clickOn(By.xpath("//*[@text='" + sort + "']"));
    }

    /**
     * Verify List View Tab is present on screen
     */
    public Boolean isListViewTabPresent() {
        return appium.isElementDisplayed(tampilanDaftar);
    }

    /**
     * Verify Map View Tab is present on screen
     */
    public Boolean isMapViewTabPresent() {
        return appium.isElementDisplayed(mapViewTab);
    }

    /**
     * Verify Filter Icon is present on screen
     */
    public Boolean isFilterIconPresent() throws InterruptedException{
        appium.hardWait(5);
        return appium.waitInCaseElementVisible(filterIcon, 3) != null;
    }

    /**
     * Verify Sorting Icon is present on screen
     */
    public Boolean isSortingIconPresent() {
        return appium.waitInCaseElementVisible(sortingIcon, 3) != null;
    }

    /**
     * Verify Back Button is present on screen
     */
    public Boolean isBackButtonPresent() {
        return appium.waitInCaseElementVisible(backButton, 3) != null;
    }

    /**
     * Get text from edit text search location
     */
    public String getTextFromEditTextSearchLocation() {
        return appium.getText(searchLocationEditText);
    }

    /**
     * click element apartment name on list view
     */
    public void clickOnApartmentNameOnListViewApartment() {
        appium.tapOrClick(apartmentNameOnListView);
    }

    /**
     * Click on Back button
     */
    public void clickBack() {
        appium.tapOrClick(backButton);
    }

    /**
     * Wait apartment listing appear
     */
    public void waitApartmentAppear() {
        appium.waitTillElementIsVisible(apartmentNameOnListView, 17);
    }

    /**
     * Click on Apply button
     */
    public void clickFTUEKosListingPopUp() throws InterruptedException {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.waitInCaseElementVisible(FTUETitleText, 5);
            while (isFTUE_screenPresent()) {
                if (appium.waitInCaseElementVisible(applyFSButton, 3) != null) {
                    appium.clickOn(applyFSButton);
                } else {
                    appium.clickOn(applyButton);
                    break;
                }
            }
        }else{
            clickSayaMengerti();
        }
    }

    /**
     * @return true if FTUE present, otherwise false.
     */
    public boolean isFTUE_screenPresent() {
        return appium.waitInCaseElementVisible(FTUETitleText, 5) != null
                || appium.waitInCaseElementVisible(applyFSButton, 5) != null;
    }

    /**
     * Check booking language button is present or not
     * @return true or false
     */
    public boolean isBookingLangsungButtonPresent(){
        return appium.waitInCaseElementVisible(canBookButton, 5)!=null;
    }

    /**
     * Check room location is present or not
     * @return true or false
     */
    public boolean isRoomLocationTextPresent(){
        return appium.waitInCaseElementVisible(roomLocationText, 5)!=null;
    }

    /**
     * Check room image is present or not
     * @return true or false
     */
    public boolean isRoomImagePresent(){
        return appium.waitInCaseElementVisible(roomImage, 5)!=null;
    }

    /**
     * Check facilities room text is present or not
     * @return true or false
     */
    public boolean isFacilitiesTextPresent(){
        return appium.waitInCaseElementVisible(facilitiesText, 5)!= null;
    }

    /**
     * Check rating is present or not
     * @return true or false
     */
    public boolean isRatingTextPresent(){
        return appium.waitInCaseElementVisible(ratingText, 5)!= null;
    }

    /**
     * Check favorite icon is present or not
     * @return true or false
     */
    public void isFavoriteIconPresent(){
         appium.waitInCaseElementVisible(love, 5);
    }

    /**
     * Click on Number Cluster of map
     */
    public void clickOnNumberClusterOfMap(String roomCount){
       String xpathLocator = "//android.view.View[@content-desc='" + roomCount + "']";
        appium.clickOn(By.xpath(xpathLocator));
    }

    /**
     * Click on price cluster of map
     */
    public void clickOnPriceClusterOfMap(String roomCount) throws InterruptedException {
        String priceClusterOfMap ="//android.view.View[@content-desc='" + roomCount +"']";
        appium.hardWait(5);
        appium.scrollDown(1,1);
        appium.clickOn(By.xpath(priceClusterOfMap));
    }

    /**
     * Get list of location for  apartment
     * @param scrollCount scroll count
     * @return list of apartment location
     */
    public List<String> getListOfLocationForApartment(int scrollCount) {
        List<String> apartment = new ArrayList<>();
        for (int i = 0; i < scrollCount; i++) {
            for (WebElement e : apartmentLocationText)
            {
                apartment.add(appium.getText(e));
            }
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return apartment;
    }

    /**
     * Click on apartment title
     */
    public void clickOnApartmentTitleText(){
            appium.tapOrClick(apartmentTitleText);
    }

    /**
     * Click on price cluster for apartment
     */
    public void clickOnPriceClusterForApartment(String roomCount){
        String priceClusterForApartment ="//android.view.View[@content-desc='"+ roomCount +"']";
        appium.clickOn(By.xpath(priceClusterForApartment));
    }

    /**
     * Check filter option is present or not
     * @param filterOption filter option
     * @return true or false
     */
    public boolean isFilterOptionPresent(String filterOption){
        return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + filterOption + "']"), 5) != null;
    }

    /**
     * Click on filter button
     */
    public void clickOnFilterButton(){
        appium.tapOrClick(filterIcon);
    }

    /**
     * Check shorting button is present or not
     * @return true or false
     */
    public boolean isShortingButtonPresent(){
        return appium.waitInCaseElementVisible(urutkanFilter,  5) != null;
    }

    /**
     * Click on search from map view
     */
    public void clickOnSearchLocation(){
        appium.tapOrClick(searchLocationEditText);
    }

    /**
     * Check good cluster is present ?
     * @param roomCount roomCount
     * @return true or false
     */
    public boolean isGoodClusterPresent(String roomCount){
        return  appium.waitInCaseElementVisible(By.xpath("//android.view.View[@content-desc='" + roomCount +"']"), 5)!=null;
    }
    /**
     * Check service cluster is present ?
     * @param roomCount roomCount
     * @return true or false
     */
    public boolean isServiceClusterPresent(String roomCount){
       return  appium.waitInCaseElementVisible(By.xpath("//android.view.View[@content-desc='" + roomCount +"']"), 5)!=null;
    }
    /**
     * Check price cluster is present ?
     * @param roomCount roomCount
     * @return true or false
     */
    public boolean isPriceClusterPresent(String roomCount){
        return  appium.waitInCaseElementVisible(By.xpath("//android.view.View[@content-desc='" + roomCount +"']"), 5)!=null;
    }

    /**
     * Check search bar is present ?
     * @return true or false
     */
    public boolean isSearchBarOfMapViewPresent(){
        return appium.waitInCaseElementVisible(searchLocationEditText, 5)!=null;
    }

    /**
     * Get list of good and services
     * @param scrollCount scroll count
     * @return good and service
     */
    public List<String> getListOfGoodAndServices(int scrollCount) {
        List<String> goodAndService = new ArrayList<>();
        for (int i = 0; i < scrollCount; i++) {
            for (WebElement e : goodAndServiceLabel)
            {
                goodAndService.add(appium.getText(e));
            }
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        return goodAndService;
    }

    /**
     * Validate result 1st result present
     */
    public boolean isSearchResultPresent() throws InterruptedException {
        return appium.waitInCaseElementVisible(goodsAndServiceFirstRow, 5) != null
                && appium.waitInCaseElementVisible(viewMapsButton,5) != null;
    }

    /**
     * Choose 1st result present
     */
    public void tapRandomGoodsAndService() {
        appium.tap(goodsAndServiceFirstRow);
    }

    /**
     * Scroll until soldout item
     */
    public boolean isSoldoutItemPresent() throws InterruptedException {
        int y = 0;
        do {
            appium.scroll(0.5, 0.80, 0.20, 100);
            y++;
        } while ((appium.waitInCaseElementVisible(soldOutLabel, 3) == null) && y < 99);
        return appium.waitInCaseElementVisible(soldOutLabel, 5) != null;
    }

    /**
     * Choose 1st result present
     */
    public void tapSoldOutItem() {
        appium.tap(soldOutLabel);
    }

    /**
     * check back button is visible
     * @return boolean
     */
    public boolean isBackButtonVisible() {
        return appium.waitInCaseElementVisible(backButton, 1) != null;
    }

    /**
     * click Favorite menu on kos listing
     */
    public void clickFavoriteMenu() throws InterruptedException {
        appium.tapOrClick(favoriteListingMenu);
        appium.hardWait(3);
    }

    /**
     * check empty image displayed or not
     * @return true or false
     */
    public boolean isEmptyImagePresent(){
        return appium.waitInCaseElementVisible(emptyImage, 5)!=null;
    }

    /**
     * check search another area button displayed or not
     * @return true or false
     */
    public boolean isSearchAnotherAreaButtonPresent(){
        return appium.waitInCaseElementVisible(searchAnotherAreaButton, 5)!=null;
    }

    /**
     * Close Empty Pop Up
     */
    public void dismissPopUpEmpty(){
        appium.waitInCaseElementVisible(emptyTitleText, 10);
        appium.clickOn(closePopUpEmptyButton);
    }

    /**
     * Check flash sale icon is present or not
     * @return true or false
     */
    public void isFlashSaleIconPresent(){
        appium.waitInCaseElementVisible(flashSaleIcon, 5);
    }

    /**
     * Check flash sale icon is present or not
     * @return true or false
     */
    public void isDiscountPercentageTextPresent(){
        appium.waitInCaseElementVisible(discountPercentageText, 5);
    }

    /**
     * Choose sorting
     */
    public void chooseSorting(String sorting) throws InterruptedException{
        appium.hardWait(3);
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            String element ="//*[@text='" + sorting + "']";
            appium.clickOn(By.xpath(element));
        }else{
            appium.tapOrClick(expensiveSort);
        }
    }

    /**
     * Verify sorting list is checked
     * @param sort e.g checkable etc
     * @return String value of attribute
     */
    public String getSortChecked(String sort) throws InterruptedException {
        return appium.getElementAttributeValue(By.xpath("//*[@text='" + sort + "']"), "checked");
    }

    // Click Area tab
    public void clickSayaMengerti() throws InterruptedException{
        appium.hardWait(2);
        appium.waitInCaseElementVisible(sayaMengertiButton, 3);
        appium.tapOrClick(sayaMengertiButton);
    }

    /**
     * Click on Ijinkan Akses Lokasi button on pop up
     */
    public void clickOnIjinkanButton() {
        if (appium.waitInCaseElementVisible(ijinkanButton, 5) != null) {
            appium.clickOn(ijinkanButton);
        }
    }
}