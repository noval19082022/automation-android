package pageobjects.mamikos.tenant.search;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.List;


public class SearchPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public SearchPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    private By searchTextbox = Constants.MOBILE_OS == Platform.ANDROID ? By.id("searchPointAutocompleteEditText"): By.xpath("//XCUIElementTypeTextField[@value='Contohnya Tebet Jakarta Selatan']");

    @AndroidFindBy(id = "searchActionTextView")
    private WebElement mainSearchButton;

    @AndroidFindBy(id = "titleMyLocationTextView")
    private WebElement myCurrentLocation;

    @AndroidFindBy(id = "titleItemHeader")
    private WebElement popularSearch;

    @AndroidFindBy(id = "subTitleHeaderTextView")
    private WebElement searchBasedOnCity;

    @AndroidFindBy(id = "canBookingTextView")
    private WebElement instantBookingButton;

    @AndroidFindBy(id = "goldPlusView")
    private WebElement GPButton;

    By popularCampus = By.xpath(
            "//android.view.ViewGroup[@resource-id='com.git.mami.kos:id/valueHeaderView']/android.widget.RelativeLayout[%s]/android.widget.TextView");

    @iOSXCUITFindBy(accessibility = "ic green arrown left")
    @AndroidFindBy(id = "arrowLeftGreenImageView")
    private WebElement backButton;

    @AndroidFindBy(id = "searchActionImageView")
    private WebElement searchIcon;

    @AndroidFindBy(id = "searchPointAutocompleteEditText")
    private WebElement searchField;

    private By searchIconiOS = MobileBy.AccessibilityId("ic_outrageous_orange_magnifier");

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"mamikos\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]\n")
    @AndroidFindBy(xpath = "(//*[@resource-id='com.git.mami.kos:id/titleLocationTextView'])[1]")
    private WebElement suggestionResult;

    @AndroidFindBy(id = "titleAreaTextView")
    private WebElement textArea;

    @AndroidFindBy(id = "toolbarHomeImageView")
    private WebElement mamikosLogo;

    @AndroidFindBy(id = "titlePropertyTextView")
    private WebElement textName;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Tidak menemukan nama tempat yang sesuai,']")
    @AndroidFindBy(id = "actionEmptyLocationTextView")
    private WebElement textInfoEmptyResult;

    @AndroidFindBy(xpath = "//*[@text='Saya Mengerti']")
    private WebElement sayaMengertiButton;

    @AndroidFindBy(id = "toMapTextView")
    private WebElement tampilanPetaMenu;

    @AndroidFindBy(id = "toListTextView")
    private WebElement tampilanDaftarMenu;

    @AndroidFindBy(id = "exitSearchImageView")
    private WebElement backLandingButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Area\"]")
    @AndroidFindBy(id = "areaSearchPointTextView")
    private WebElement area;

    private By areaTab = Constants.MOBILE_OS == Platform.ANDROID ? By.id("areaSearchPointTextView"): By.xpath("//XCUIElementTypeStaticText[@name='Area']");

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Stasiun & Halte\"]")
    @AndroidFindBy(id = "mrtSearchPointTextView")
    private WebElement stasiunHalteTab;

    @AndroidFindBy(id = "titleMyLocationTextView")
    private WebElement myLocationLabel;

    private By apartmentIcon = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text='Apartemen']"): By.id("Apartemen");

    private By filterIcon = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//android.widget.TextView[@text=\"Filter\"]"): By.id("Filter");

    @AndroidFindBy(id = "searchPointAutocompleteEditText")
    private WebElement searchPointTextbox;

    @AndroidFindBy(id = "roomTitleTextView")
    private List<WebElement> roomList;

    @AndroidFindBy(id = "nextButton")
    private WebElement nextButton;

    @AndroidFindBy(id = "detailFacilityButton")
    private WebElement detailFacilityButton;

    @AndroidFindBy(id = "loginOwnerButton")
    private WebElement loginOwnerButton;

    @AndroidFindBy(id = "loginTenantButton")
    private WebElement loginTenantButton;

    @AndroidFindBy(id = "titleTooltipTextView")
    private WebElement titleTooltipRoomImage;

    /**
     * Input Search Textbox
     * @param place
     */
    public void searchInput(String place){
        appium.enterText(searchPointTextbox, place, false);
    }

    /**
     * Click Search Result by Address
     * @param address address e.g Jalan Palagan
     */
    public void clickOneOfSearchResults(String address){
        appium.clickOn(By.xpath("//*[contains(@text,'" + address + "')]"));
    }

    /**
     * Click on BBK Pop up
     */
    public void clickOnBBKPopup(){
        appium.clickOn(sayaMengertiButton);
    }

    /**
     * Click one of room list results
     */
    public void clickOneOfRoomLists() throws InterruptedException {
        appium.hardWait(1);
        appium.clickOn(roomList.get(0));
    }

    /**
     * Dismiss FTUE Screen
     */
    public void dismissFTUEScreen() throws InterruptedException {
        int counter = 0;
        while(isFTUEScreenPresent())
        {
            if (appium.waitInCaseElementVisible(nextButton, 3) != null){
               appium.clickOn(nextButton);
            }
            counter++;
        }

    }

    /**
     * Is FTUE Screen Present?
     * @return true or false
     * @throws InterruptedException
     */
    public boolean isFTUEScreenPresent() throws InterruptedException {
        return appium.isElementDisplayed(nextButton) && appium.isElementPresent(By.id("nextButton"));
    }

    /**
     * Scroll to 'Fasilitas kost dan kamar'
     * Click Detail Facility Button
     */
    public void clickOnDetailFacility(){
        appium.scrollToElementByText("Fasilitas kost dan kamar");
        appium.clickOn(detailFacilityButton);
    }

    /**
     * is Login Owner Button Present?
     * @return true or false
     */
    public boolean isLoginOwnerButtonPresent(){
        return appium.waitInCaseElementVisible(loginOwnerButton, 5) != null;
    }

    /**
     * is Login Tenant Button Present?
     * @return true or false
     */
    public boolean isLoginTenantButtonPresent(){
        return appium.waitInCaseElementVisible(loginTenantButton, 5) != null;
    }

    /**
     * Enter Text and select result
     * @throws InterruptedException
     */
    public void enterTextToSearchTextboxAndSelectResult(String searchText) throws InterruptedException {
        String xpathLocator;
        appium.hardWait(3);
        appium.waitTillElementIsClickable(searchTextbox);
        appium.tapOrClick(searchTextbox);
        appium.enterText(searchTextbox, searchText, false);
        appium.hideKeyboard();
        appium.hardWait(3);
        if(Constants.MOBILE_OS == Platform.ANDROID) {
           xpathLocator = "//android.widget.TextView[@text='"+searchText+"']";
            By resultLocator = By.xpath(xpathLocator);
            appium.waitTillElementsCountIsMoreThan(resultLocator, 0);
            appium.waitTillAllElementsAreLocated(resultLocator).get(0).click();
        }
        else {
            xpathLocator = "//XCUIElementTypeStaticText[@name='" + searchText + "']";
            By resultLocator = By.xpath(xpathLocator);
            appium.waitTillElementsCountIsEqualTo(resultLocator, 1);
            appium.waitTillAllElementsAreLocated(resultLocator).get(0).click();
        }
    }

    /**
     * Enter Text on search field
     *
     * @throws InterruptedException
     */
    public void enterTextToSearchTextbox(String searchText) throws InterruptedException {
        appium.tapOrClick(searchTextbox);
        appium.enterText(searchTextbox, searchText, false);
    }

    /**
     * Select result autocomplete base on input in search field
     *
     * @throws InterruptedException
     */
    public void selectResultOptionListSearchTextbox(String searchText) throws InterruptedException {
        String xpathLocator = "//*[@text='" + searchText + "']";
        By resultLocator = By.xpath(xpathLocator);
        appium.waitTillElementsCountIsEqualTo(resultLocator, 1);
        appium.waitTillAllElementsAreLocated(resultLocator).get(0).click();
    }

    /**
     * Select second result based on search text
     * @param searchText
     */
    public void selectSecondResultFromSearchList(String searchText){
        String xpathLocator = "//*[@text='" + searchText + "']";
        By resultLocator = By.xpath(xpathLocator);
        appium.waitTillElementsCountIsEqualTo(resultLocator, 2);
        appium.waitTillAllElementsAreLocated(resultLocator).get(1).click();

    }

    @AndroidFindBy(id = "com.google.android.gms:id/message")
    private WebElement popupMessageLabel;

    /**
     * check my current location displayed button displayed or not
     */
    public boolean verifyMyCurrentLocationPresent() {
        return myCurrentLocation.isDisplayed();
    }

    @AndroidFindBy(id = "android:id/button1")
    private WebElement okButton;


    /**
     * click label my location
     */
    public void clickOnMyLocation() {
        appium.tapOrClick(myLocationLabel);
    }

    /**
     * return text message on popup message location service
     */
    public String getPopupMessageLocationService() {
        return appium.getText(popupMessageLabel);
    }

    /**
     * click button on popup message location service
     */
    public void clickOkButton() {
        appium.tapOrClick(okButton);
    }

    /**
     * switch location service on device setting
     */
    public void switchLocationService() {
        appium.toggleLocationServices();
    }

    /**
     * @return true if popular search element is present
     */
    public boolean verifyPencarianPopularHeaderPresent() {
        return popularSearch.isDisplayed();
    }

    /**
     * @return true if search based on city element is present
     */
    public boolean verifySearchBasedOnCityPresent() {
        return searchBasedOnCity.isDisplayed();
    }

    /**
     * @param listNumber input with list number. List number start from 1 to max 10.
     * @return web element of popular search after reformat.
     */
    public WebElement popularSearchElement(int listNumber) {
        String popularCampusText = popularCampus.toString().replace("By.xpath: ", "");
        popularCampusText = String.format(popularCampusText, String.valueOf(listNumber));
        WebElement element = driver.findElement(By.xpath(popularCampusText));
        return element;
    }

    /**
     * @param listNumber
     * @return
     */
    public By getPopularSearchXpath(int listNumber) {
        String popularCampusText = popularCampus.toString().replace("By.xpath: ", "");
        popularCampusText = String.format(popularCampusText, String.valueOf(listNumber));
        By by = By.xpath(popularCampusText);
        return by;
    }

    /**
     * @param text enter with target text example UGM
     * @return web element to be used in another appium method
     */
    public WebElement textElement(String text) {
        WebElement element;
        element = driver.findElement(By.xpath("//*[@text='" + text + "']"));
        return element;
    }

    public By textBy(String target, String idOrXpath) {
        By by;
        if (idOrXpath == "xpath") {
            by = By.xpath("//*[@text='" + target + "']");
        } else {
            by = By.id("" + target + "");
        }
        return by;
    }

    /**
     * @param text tap on element based on text
     */
    public void tapOnElementText(String text) {
        appium.tapOrClick(this.textElement(text));
    }

    /**
     * @param listNumber input with list number. Index list start from 1
     * @return popular search text
     */
    public String getPopularSearchText(int listNumber) {
        return popularSearchElement(listNumber).getText();
    }

    /**
     * @return max number of list in the popular search section
     * @throws InterruptedException
     */
    public int getPopularSearchListSize() throws InterruptedException {
        int size = 1;
        while (popularSearchElement(1).isDisplayed()) {
            boolean isPresent = driver.findElements(getPopularSearchXpath(size)).size() > 0;
            System.out.println(isPresent);
            if (isPresent == false) {
                size = size - 1;
                break;
            }
            size++;
            Thread.sleep(500);
        }
        return size;
    }

    /**
     * @param popular fill with your target popular search example UGM, Jogja, or
     *                Halte Harmoni Central. Tab for the target popular search
     *                already selected.
     * @throws InterruptedException
     */
    public void clickOnPopular(String popular) throws InterruptedException {
        int maxListSize = getPopularSearchListSize();
        for (int i = 1; i <= maxListSize; i++) {
            String popularText = getPopularSearchText(i);
            if (popularText.equals(popular)) {
                appium.tapOrClick(popularSearchElement(i));
                break;
            } else if (popularText.equals(popular) != true && i == maxListSize) {
                System.out.print("Your target kampus, area or MRT not in the popular list");
                break;
            } else {
                Thread.sleep(500);
            }
        }
    }

    /**
     * check bisa booking button
     *
     * @return true if element displayed
     */
    public boolean cariKostPageReachedAndDisplayed() {
        return appium.waitInCaseElementVisible(instantBookingButton,3) != null && appium.waitInCaseElementVisible(GPButton, 3) != null;
    }

    /**
     * @param cityOrCampusName input with city, campus, bus stop/station name.
     * @return true if element based on city, campus, bus stop/station name present,
     * otherwise false.
     */
    public boolean vefiryCityOrCampusListPresent(String cityOrCampusName) {
        By cityOrCampus = textBy(cityOrCampusName, "xpath");
        boolean isPresent = driver.findElements(cityOrCampus).size() > 0;
        return isPresent;
    }

    /**
     * verify the back button is displayed
     */
    public boolean backButtonIsDisplayed() {
        return backButton.isDisplayed();
    }

    /**
     * verify the search icon is displayed
     */
    public boolean searchIconIsDisplayed() throws InterruptedException {
    if (Constants.MOBILE_OS== Platform.ANDROID) {
        return appium.waitInCaseElementVisible(searchIcon, 10) != null;
    }else {
        return appium.isElementPresent(searchIconiOS);
    }
    }

    /**
     * verify the search field is displayed
     */
    public boolean textFieldIsDisplayed() {
        return driver.findElement(searchTextbox).isDisplayed();
    }

    /**
     * Click on search text box Fill out the param on search text ox Get text the
     * first result
     *
     * @param keyword text value
     * @return
     */
    public boolean isSearchResultPresent(String keyword) throws InterruptedException {
        appium.tapOrClick(searchTextbox);
        appium.enterText(searchTextbox, keyword, false);
        appium.hardWait(3);

        if (Constants.MOBILE_OS==Platform.ANDROID) {
            String s = appium.getText(suggestionResult).toLowerCase();
            return s.contains(keyword);
        } else {
            String xpathLocator = "//*[@value='" + keyword + "']";
            MobileElement element = (MobileElement) driver.findElementByXPath(xpathLocator);
            String s = appium.getText(element);
            return s.contains(keyword);

        }
    }

    /**
     * Click on search text box Fill out search text box
     *
     * @param inputedText input text value
     */
    public void tapAndEnterDataToSearchTextbox(String inputedText) throws InterruptedException {
        appium.tapOrClick(searchTextbox);
        appium.hardWait(3);
        appium.enterText(searchTextbox, inputedText, false);
    }

    /**
     * Check the object textArea is displayed
     */
    public boolean textAreaIsDisplayed() {
        return appium.waitInCaseElementVisible(textArea, 10) != null;
    }

    /**
     * Check the object textName is displayed
     */
    public boolean textNameIsDisplayed() {
        return appium.waitInCaseElementVisible(textName, 10) != null;
    }

    /**
     * Clear the text field
     */
    public void clearText() {
        driver.findElement(searchTextbox).clear();
    }

    /**
     * Get empty result message
     */
    public String getResultInfo() {
        return appium.getText(textInfoEmptyResult);
    }

    /**
     * Click on label location
     */
    public void tapAreaOption(String text) {
        appium.clickOn(By.xpath("//*[@text='" + text + "']"));
    }

    /**
     * Click on sugestion result
     */
    public void tapAreaResult() throws InterruptedException {
            appium.tap(suggestionResult);
            appium.hardWait(5);
            if (Constants.MOBILE_OS==Platform.IOS)
            appium.iOSActivateApp(Constants.MAMIKOS_APP_BUNDLED_ID);
    }

    /**
     * Click on 'Search' textbox Get the text in search text box
     *
     * @throws InterruptedException
     */
    public String getSearchTextboxText() throws InterruptedException {
        appium.hardWait(7);
        appium.tapOrClick(searchTextbox);
        return appium.getText(searchTextbox);
    }

    // Click Area tab
    public void clickArea() throws InterruptedException{
        appium.hardWait(2);
        appium.waitInCaseElementVisible(areaTab, 3);
        appium.tapByElementLocation(areaTab);
    }

    // Check element appear or not by text
    public boolean checkElementbyText(String city) {
        String xpathLocator = "//*[contains(@text,'" + city + "')]";
        MobileElement element = (MobileElement) driver.findElementByXPath(xpathLocator);
        return appium.waitInCaseElementVisible(element, 3) != null;
    }

    // tap city name by dynamic xpath
    public void userTapCity(String city) throws InterruptedException {
        if (Constants.MOBILE_OS==Platform.ANDROID) {
            appium.hardWait(3);
            String xpathLocator = "//*[@text='" + city + "']";
            MobileElement element = (MobileElement) driver.findElementByXPath(xpathLocator);
            appium.tapOrClick(element);
        } else {
            String xpathLocator = "//*[@value='" + city + "']";
            MobileElement element = (MobileElement) driver.findElementByXPath(xpathLocator);
            appium.tapOrClick(element);
        }
    }

    // scroll to element by Text
    public void scrollToElement(String elementText) {
        appium.scrollToElementByText(elementText);
    }

    /**
     * Click on 'Search' textbox
     */
    public void clickOnSearchTextbox() {
        appium.waitInCaseElementVisible(searchTextbox,10);
        appium.tapOrClick(searchTextbox);
    }

    /**
     * Click on 'Stasiun & Halte' Tab
     */
    public void clickOnStasiunHalteTab() {
        appium.tapOrClick(stasiunHalteTab);
    }

    /**
     * Get the 'Stasiun & Halte' text from 'Stasiun & Halte' tab
     */
    public String stasiunMenuText() {
        return appium.getText(stasiunHalteTab);
    }

    /**
     * Click on each 'Drop Down Item List' in Stasiun & Halte Tab
     */
    public void clickOnItemDropDown(String cityName) {
        if(Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByText(cityName);
            appium.clickOn(By.xpath("//*[@text='" + cityName + "']"));
        }
        else {
            appium.scroll(0.5, 0.80, 0.5, 500);
            appium.clickOn(By.xpath("//XCUIElementTypeOther[@name=\""+cityName+"\"]"));
            appium.scroll(0.5, 0.80, 0.5, 500);

        }
    }

    /**
     * Get 'Stasiun Name' Text
     * <p>
     * /* @param o
     *
     * @param stasiunName
     * @return
     */
    public boolean isStationPresent(Object stasiunName) {
        if (Constants.MOBILE_OS == Platform.ANDROID)
            return appium.waitInCaseElementVisible(By.xpath("//*[@text='" + stasiunName + "']"), 5) != null;
        else
            return appium.waitInCaseElementVisible(By.xpath("//XCUIElementTypeStaticText[@name=\""+stasiunName+"\"]"), 5) != null;
    }

    /**
     * click icon apartment for search apartment on main page
     */
    public void clickApartmentIcon() throws InterruptedException {
        appium.hardWait(5);
        appium.scrollHorizontal(0.3, 0.80, 0.40, 2000);
        appium.tapOrClick(apartmentIcon);
    }

    /**
     * click icon filter for search apartment using filter feature
     */
    public void clickFilterIcon() {
        appium.clickOn(filterIcon);
    }

    public void clickDeviceBackButton() {
        appium.back();
    }

    /**
     * Click on Room Image Tooltip
     */
    public void clickOnRoomImageTooltip(){
        appium.tapOrClick(titleTooltipRoomImage);
    }

    /**
     * check gold plus button displayed or not
     * @return true or false
     */
    public boolean isGPButtonPresent(){
        return appium.waitInCaseElementVisible(GPButton, 5)!=null;
    }

    /**
     * return text on search text box
     */
    public String getTextSearchTextBoxClear() {
        return appium.getText(searchField);
    }

    /**
     * Verify Filter Icon is present on screen
     */
    public Boolean isFilterIconPresent() throws InterruptedException{
        appium.hardWait(5);
        return appium.waitInCaseElementVisible(filterIcon, 3) != null;
    }
}