package pageobjects.mamikos.tenant.verification;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class verificationPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public verificationPO(AppiumDriver<WebElement> driver) {
            this.driver = driver;
            appium = new AppiumHelpers(driver);

            //This initElements method will create all Android Elements
            PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    private By verificationAkun = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Verifikasi akun']"): By.xpath("]");
    private By kartuIdentitas = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Upload Foto Identitas']/following::android.widget.LinearLayout[2]"): By.xpath("");
    private By kartuIdentitasWithSelfie = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("//*[@text='Upload Foto Identitas']/following::android.widget.LinearLayout[3]"): By.xpath("");
    private By takeCapture = Constants.MOBILE_OS == Platform.ANDROID ? By.id("takePictureImageView"): By.xpath("");
    private By tapDone = Constants.MOBILE_OS == Platform.ANDROID ? By.id("saveImageView"): By.xpath("");
    private By tapTermAndCondition = Constants.MOBILE_OS == Platform.ANDROID ? By.id("privacyIdentityCheckBox"): By.xpath("");
    private By tapSimpanVerificationAkun = Constants.MOBILE_OS == Platform.ANDROID ? By.id("saveIdentityButton"): By.xpath("");


    /**
     * Click on 'Verification Akun'
     */
    public void clickOnVerificationAkun() {
        appium.clickOn(verificationAkun);
    }

    /**
     * Click on 'Kartu Identitas'
     */
    public void clickOnKartuIdentitas() {
        appium.scroll(0.5, 0.70, 0.30, 2000);
        appium.tapOrClick(kartuIdentitas);
    }

    /**
     * Click on 'Take Capture Kartu Identitas'
     */
    public void clickOnTakeCapturePhoto() throws InterruptedException {
        appium.clickOn(takeCapture);
        appium.hardWait(3);
    }

    /**
     * Click on 'Take Capture Kartu Identitas with selfie'
     */
    public void clickOnTakeCapturePhotoWithSelfie() throws InterruptedException {
        appium.clickOn(kartuIdentitasWithSelfie);
        appium.hardWait(3);
    }

    /**
     * Click on 'Done'
     */
    public void clickOnDone() throws InterruptedException {
        appium.clickOn(tapDone);
        appium.hardWait(3);
    }

    /**
     * Click on 'Term And Condition'
     */
    public void clickOnCheckboxTermAndCondition() throws InterruptedException {
        appium.scrollToElementByText("Simpan");
        appium.clickOn(tapTermAndCondition);
        appium.hardWait(3);
    }

    /**
     * Click on 'Simpan Verification Akun'
     */
    public void clickOnSimpanVerificationAkun() throws InterruptedException {
        appium.tapOrClick(tapSimpanVerificationAkun);
    }

    /**
     * Popup Success after upload ID
     */
    public boolean isElementWithText(String text) {
        String textEl = "";
        if (Constants.MOBILE_OS == Platform.ANDROID ) {
            textEl = "//*[@text='" + text + "']";
        }else{
            textEl = "//*[@name='" + text + "']";
        }
        MobileElement element = (MobileElement) driver.findElementByXPath(textEl);
        return appium.waitInCaseElementVisible(element, 5) != null;
    }
}
