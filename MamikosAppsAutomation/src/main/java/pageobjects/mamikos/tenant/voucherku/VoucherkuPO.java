package pageobjects.mamikos.tenant.voucherku;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

import java.util.List;

public class VoucherkuPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public VoucherkuPO(AppiumDriver<WebElement> driver)
    {
        this.driver = driver;
        appium = new AppiumHelpers(driver);;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     *
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName,
     * androidDataMatcher, xpath , priority as attributes.
     */

    @iOSXCUITFindBy(accessibility = "Profil")
    @AndroidFindBy(id = "profileView")
    private WebElement profileButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Voucherku']/preceding-sibling::XCUIElementTypeButton")
    @AndroidFindBy(id = "myVoucherTextView")
    private WebElement voucherkuButton;

    @iOSXCUITFindBy(accessibility = "Tersedia")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Tersedia']")
    private WebElement tersediaTab;

    @iOSXCUITFindBy(accessibility = "Terpakai")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Terpakai']")
    private WebElement terpakaiTab;

    @iOSXCUITFindBy(accessibility = "Kedaluwarsa")
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Kedaluwarsa']")
    private WebElement kadaluwarsaTab;

    @iOSXCUITFindBy(accessibility = "Voucher yang dapat kamu gunakan akan tersedia di sini")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Voucher yang dapat kamu gunakan')]")
    private WebElement emptyVoucherTersediaVerifyText;

    @iOSXCUITFindBy(accessibility = "Voucher yang telah kamu gunakan akan tampil di sini")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Voucher yang telah kamu gunakan')]")
    private WebElement emptyVoucherTerpakaiVerifyText;

    @iOSXCUITFindBy(accessibility = "Voucher yang habis masa berlakunya akan tampil di sini")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Voucher yang habis masa berlakunya')]")
    private WebElement emptyVoucherKedaluwarsaVerifyText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Promo Lainnya']")
    private WebElement promoLainnyaText;

    @iOSXCUITFindBy(accessibility = "Lihat")
    @AndroidFindBy(id = "seePromoTextView")
    private WebElement promoButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='URL']")
    @AndroidFindBy(id = "com.sec.android.app.sbrowser:id/location_bar_edit_text")
    private WebElement promoUrlText;

    @AndroidFindBy(id = "voucherBannerImageView")
    private WebElement voucherBannerImage;

    @AndroidFindBy(id = "titleToolbarTextView")
    private WebElement detailVoucherTitleToolbarText;

    @AndroidFindBy(id = "detailVoucherImageView")
    private WebElement detailVoucherImageView;

    @AndroidFindBy(id = "titleDetailVoucherTextView")
    private WebElement detailVoucherTitleText;

    @AndroidFindBy(id = "dueDateVoucherTextView")
    private WebElement dueDateVoucherText;

    @AndroidFindBy(id = "termConditionTitleTextView")
    private WebElement termsAndConditionsVoucherText;

    @AndroidFindBy(id = "detailVoucherView")
    private WebElement detailVoucherView;

    @AndroidFindBy(id = "titleCodeVoucherTextView")
    private WebElement codeVoucherTitleText;

    @AndroidFindBy(id = "codeVoucherTextView")
    private WebElement codeVoucherText;

    @AndroidFindBy(id = "copyVoucherCardView")
    private WebElement copyVoucherCodeButton;

    @AndroidFindBy(id = "countRemindermyVoucherTextView")
    private WebElement redDotCountReminderVoucher;

    @AndroidFindBy(id = "voucherBannerImageView")
    private List<WebElement> voucherBannerImageList;

    @AndroidFindBy(id = "backImageView")
    private WebElement backArrowButton;

    /**
     * Click on Profile button
     *
     */
    public void clickOnProfile() {
        appium.tapOrClick(profileButton);
    }

    /**
     * Click on Voucherku
     *
     */
    public void clickOnVoucherku() {
        appium.tapOrClick(voucherkuButton);
    }

    /**
     * Click on Tersedia tab
     *
     */
    public void clickOnTersediaTab(){
        appium.tapOrClick(tersediaTab);
    }

    /**
     * Is Tersedia Tab Present ?
     * @return true or false
     */
    public Boolean isTersediaTabPresent(){
        return appium.waitInCaseElementVisible(tersediaTab, 3) != null;
    }

    /**
     * Click on Terpakai tab
     *
     */
    public void clickOnTerpakaiTab() {
        appium.tapOrClick(terpakaiTab);
    }

    /**
     * Is Terpakai Tab Present?
     * @return true or false
     */
    public Boolean isTerpakaiTabPresent(){
        return appium.waitInCaseElementVisible(terpakaiTab, 3) != null;
    }

    /**
     * Click on Kedaluwarsa tab
     *
     */
    public void clickOnKedaluwarsaTab() {
        appium.tapOrClick(kadaluwarsaTab);
    }

    /**
     * Is Kadaluwarsa Tab Present?
     * @return true or false
     */
    public Boolean isKadaluwarsaTabPresent(){
        return appium.waitInCaseElementVisible(kadaluwarsaTab, 3) != null;
    }

    /**
     * Click on Promo page
     *
     */
    public void clickOnPromoPage() {
        appium.tapOrClick(promoButton);
    }

    /**
     * Verify tersedia empty voucher
     *
     * @return true or false
     */
    public boolean verifyTersediaEmptyVoucher(){
        return  appium.waitInCaseElementVisible(emptyVoucherTersediaVerifyText, 2) != null;
    }

    /**
     * Verify terpakai empty voucher
     *
     * @return true or false
     */
    public boolean verifyTerpakaiEmptyVoucher(){
        return  appium.waitInCaseElementVisible(emptyVoucherTerpakaiVerifyText, 2) != null;
    }

    /**
     * Verify kedaluwarsa empty voucher
     *
     * @return true or false
     */
    public boolean verifyKedaluwarsaEmptyVoucher(){
        return  appium.waitInCaseElementVisible(emptyVoucherKedaluwarsaVerifyText, 2) != null;
    }

    /**
     * Get promo url text
     * @return promo url
     */
    public String getPromoUrl() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            return appium.getText(promoUrlText);
        }
        else{
            return appium.getElementAttributeValue(promoUrlText,"value").substring(0,18);
        }
    }

    /**
     * Click on user voucher
     */
    public void clickOnVoucher(){
        appium.clickOn(voucherBannerImage);
    }

    /**
     * Get Detail Voucher Toolbar Title
     * @return Detail Voucher Toolbar Title
     */
    public String getTitleToolbarText(){
        return appium.getText(detailVoucherTitleToolbarText);
    }

    /**
     * Is Detail Voucher Image Present?
     * @return true or false
     */
    public Boolean isDetailVoucherImagePresent(){
        return appium.waitInCaseElementVisible(detailVoucherImageView, 3) != null;
    }

    /**
     * Is Detail Voucher Title Text Present?
     * @return true or false
     */
    public Boolean isDetailVoucherTitleTextPresent(){
        return appium.waitInCaseElementVisible(detailVoucherTitleText, 3) != null;
    }

    /**
     * Get Detail Voucher Title Text
     * @return Detail Voucher Title
     */
    public String getDetailVoucherTitleText(){
        return appium.getText(detailVoucherTitleText);
    }

    /**
     * Is Due Date Voucher Present?
     * @return true or false
     */
    public Boolean isDueDateVoucherPresent(){
        return appium.waitInCaseElementVisible(dueDateVoucherText, 3) != null;
    }

    /**
     * Get Due Date Voucher Text
     * @return Voucher Due Date
     */
    public String getDueDateVoucherText(){
        return appium.getText(dueDateVoucherText);
    }

    /**
     * Is Terms And Condition of Voucher Present?
     * @return true or false
     */
    public Boolean isTermAndConditionVoucherPresent(){
        return appium.waitInCaseElementVisible(termsAndConditionsVoucherText, 3) != null;
    }

    /**
     * Is Detail Voucher Information Present?
     * @return true or false
     */
    public Boolean isDetailVoucherInformationTextPresent(){
        return appium.waitInCaseElementVisible(detailVoucherView, 3) != null;
    }

    /**
     * Is Code Voucher Title Present?
     * @return true or false
     */
    public Boolean isCodeVoucherTitleTextPresent(){
        return appium.waitInCaseElementVisible(codeVoucherTitleText, 3) != null;
    }

    /**
     * Get Code Voucher Title
     * @return Code Voucher
     */
    public String getCodeVoucherTitleText(){
        return appium.getText(codeVoucherTitleText);
    }

    /**
     * Get Code Voucher
     * @return Code Voucher
     */
    public String getCodeVoucherText(){
        return appium.getText(codeVoucherText);
    }

    /**
     * Is Copy Code Voucher Button Present?
     * @return true or false
     */
    public Boolean isCopyCodeVoucherButtonPresent(){
        return appium.waitInCaseElementVisible(copyVoucherCodeButton, 3) != null;
    }

    /**
     * Click on Copy Code Voucher Button
     */
    public void clickOnCopyCodeVoucherButton(){
        appium.clickOn(copyVoucherCodeButton);
    }

    /**
     * Get Red Dot Count Reminder Voucher
     * @return Count Reminder Voucher
     */
    public String getRedDotCountReminderVoucher(){
        return appium.getText(redDotCountReminderVoucher);
    }

    /**
     * Get Available Voucher Lists
     * @return voucher lists
     */
    public Integer countVoucherSize(){
        return voucherBannerImageList.size();
    }

    /**
     * Get Other Promo Text
     * @return Other Promo
     */
    public String getOtherPromoText(){
        return appium.getText(promoLainnyaText);
    }

    /**
     * Is Voucher Banner Image Present?
     * @return true or false
     */
    public Boolean isVoucherBannerImagePresent(){
        return appium.waitInCaseElementVisible(voucherBannerImage, 3) != null;
    }

    /**
     * Is Button Copy Voucher Enabled?
     * @return true or false
     */
    public Boolean isCopyCodeVoucherButtonEnabled() throws InterruptedException {
        appium.hardWait(2);
        try{
            if (copyVoucherCodeButton.isEnabled()) {
                return true;
            } else {
                return false;
            }
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Is Mass Voucher Targeted for Kost City Present ?
     *
     * @return true or false
     */
    public boolean isVoucherTargetedPresent(String voucherCode) throws InterruptedException {
        appium.scrollToElementByText(voucherCode);
        return appium.waitInCaseElementVisible(By.xpath("//android.widget.TextView[2][@text='"+voucherCode+"']"), 5) != null;
    }

    /**
     * Is Mass Voucher Targeted for Other Kost City not Present ?
     *
     * @return true or false
     */
    public boolean isMassVoucherTargetedNotPresent(String voucher) throws InterruptedException {
        try {
            appium.scrollToElementByText(voucher);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Click on back arrow button
     */
    public void clickOnBackArrowButton() {
        appium.clickOn(backArrowButton);
    }
}
