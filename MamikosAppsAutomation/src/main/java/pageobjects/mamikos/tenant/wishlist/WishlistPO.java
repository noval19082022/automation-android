package pageobjects.mamikos.tenant.wishlist;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;
import utilities.Constants;

public class WishlistPO {

    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public WishlistPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */

    //-------------- Before Login --------------
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeNavigationBar[@name=\"Favorit\"]")
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'Favorit')]")
    private WebElement wishlistTitle;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Favorit\"]")
    @AndroidFindBy(xpath = "//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Difavoritkan\"]/android.widget.TextView")
    private WebElement favoriteTab;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Dilihat\"]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"Pernah Dilihat\"]")
    private WebElement historyTab;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name, \"Kost dilihat akan terhapus setelah 9 hari\")]")
    @AndroidFindBy(id = "com.git.mami.kos:id/visitedHintView")
    private WebElement visitedHintText;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable")
    @AndroidFindBy(id = "historyAllSwipeRefreshLayout")
    private WebElement historySeen;

    //------------- After Login --------------
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[contains(@name, 'Cari kost')]")
    @AndroidFindBy(id = "searchOtherKostButton")
    private WebElement searchOtherKostButton;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable")
    @AndroidFindBy(id = "historyUserRecyclerView")
    private WebElement favoriteProperty;

    @AndroidFindBy(id = "roomListView")
    private WebElement listProperty;


    private By propertyTitle = Constants.MOBILE_OS == Platform.ANDROID ? By.xpath("(//android.widget.TextView[@resource-id='com.git.mami.kos:id/roomTitleTextView'])[1]") : By.id("Kost andalusia spanyol eropa timur");

    private By kostName = Constants.MOBILE_OS == Platform.ANDROID ? By.id("roomTitleTextView") : By.id("Kost andalusia spanyol eropa timur 396AS");


    //---------------------------------

    @AndroidFindBy(id = "loveImageView")
    private WebElement favoriteIcon;

    private By favoriteKostEmptyHeader = Constants.MOBILE_OS == Platform.ANDROID ? By.id("headingEmptyStateTextView") : By.xpath("//XCUIElementTypeStaticText[@name='Belum ada kos yang di favorit.']");


    /**
     * Check wishlist title is Visible
     *
     * @return status
     */
    public boolean isWishlistTitleVisible() {
        return appium.waitInCaseElementVisible(wishlistTitle, 3) != null;
    }

    /**
     * Check favorite tab is Visible
     *
     * @return status
     */
    public boolean isFavoriteTabVisible() {
        return appium.waitInCaseElementVisible(favoriteTab, 3) != null;
    }

    /**
     * Check history tab is Visible
     *
     * @return status
     */
    public boolean isHistoryTabVisible() {
        return appium.waitInCaseElementVisible(historyTab, 3) != null;
    }

    /**
     * Check visited hint text is Visible
     *
     * @return status
     */
    public boolean isVisitedHintTextVisible() {
        return appium.waitInCaseElementVisible(visitedHintText, 3) != null;
    }

    /**
     * Check history seen is Visible
     *
     * @return status
     */
    public boolean isHistoryKosSeenVisible() {
        return appium.waitInCaseElementVisible(historySeen, 3) != null;
    }

    /**
     * Click on history tab
     */
    public void clickHistoryTab() throws InterruptedException {
        appium.waitTillElementIsClickable(historyTab);
        appium.hardWait(3);
        appium.clickOn(historyTab);
    }

    /**
     * Check search other button is Visible
     *
     * @return status
     */
    public boolean isSearchOtherKosButtonVisible() {
        return appium.waitInCaseElementVisible(searchOtherKostButton, 5) != null;
    }

    /**
     * Check favorite property is Visible
     *
     * @return status
     */
    public boolean isFavoritePropertyVisible() {
        return appium.waitInCaseElementVisible(favoriteProperty, 3) != null;
    }

    /**
     * Get property name
     *
     * @return
     */
    public String getPropertyName() throws InterruptedException {
        String propertyText = "";
        if (Constants.MOBILE_OS== Platform.IOS) {
            appium.pullDownToRefresh();
        }
            appium.hardWait(2);
            appium.waitInCaseElementVisible(propertyTitle, 3);
            propertyText = appium.getText(propertyTitle);
        
        return propertyText;
    }

    /**
     * Click on property
     */
    public void clickProperty(){
        appium.clickOn(listProperty);
    }

    /**
     * Click on favorite icon
     */
    public void clickFavoriteIcon() {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.tapOrClick(favoriteIcon);
        }else{
            appium.tapOrClick(propertyTitle);
        }
    }

    /**
     * Click on favorite tab
     * @throws InterruptedException
     */
    public void clickFavoriteTab() throws InterruptedException {
        appium.tapOrClick(favoriteTab);
        appium.hardWait(2);
    }

    /**
     * Get Kost name
     * @return kost name
     */
    public String getKostName(){
        String kostNameText = "";
        appium.waitInCaseElementVisible(kostName, 5);
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            kostNameText = appium.getText(kostName);
        }else{
            kostNameText = appium.getText(kostName).substring(0,34);
        }
        return kostNameText;
    }

    /**
     * Check favorite kost is empty
     * @return status
     */
    public boolean isFavoriteKostEmpty(){
        if (Constants.MOBILE_OS == Platform.IOS){
            appium.pullDownToRefresh();
        }
        return appium.waitInCaseElementVisible(favoriteKostEmptyHeader, 5) != null;
    }

    /**
     * refresh page favorite after favorite kost or unfavorite kost
     */
    public void refreshPageFavorite() throws InterruptedException{
        appium.hardWait(2);
        appium.pullDownToRefresh();
    }
}
