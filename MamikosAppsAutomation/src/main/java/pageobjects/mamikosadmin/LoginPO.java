package pageobjects.mamikosadmin;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class LoginPO {

    WebDriver driver;

    @FindBy(xpath = "//input[contains (@name, 'email')]")
    private WebElement emailTextBox;

    @FindBy(xpath = "//input[contains (@name, 'password')]")
    private WebElement passWordTextBox;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginButton;

    public LoginPO(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /**
     * Enter Email and Password then click on Login Button
     * @param email enter Email
     * @param passWord enter Password
     * @throws InterruptedException
     */
    public void enter_credentials_and_click_on_login_button(String email, String passWord) throws InterruptedException {
        emailTextBox.sendKeys(email);
        passWordTextBox.sendKeys(passWord);
        loginButton.click();
    }
}
