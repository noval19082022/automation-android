package pageobjects.mamikosadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class RewardListPO {

    WebDriver driver;

    @FindBy(name = "keyword")
    private WebElement keywordField;

    @FindBy(name = "show_testing")
    private WebElement showTestingDropdown;

    @FindBy(css = "input.btn")
    private WebElement filterButton;

    @FindBy(css = ".fa-eye")
    private WebElement viewRedemptionIcon;

    @FindBy(xpath = "(//span[@class='text-primary'])[1]")
    private WebElement redeemStatusField;

    @FindBy(name = "notes")
    private WebElement notesField;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    private WebElement submitButton;

    @FindBy(name = "status_new")
    private WebElement statusDropdown;

    public RewardListPO(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /**
     * Set Keyword on Search Field
     *
     * @param keyword of searching key
     */
    public void setSearchKeyword(String keyword){
        keywordField.sendKeys(keyword);
    }

    /**
     * Choose Show Testing Filter
     *
     * @param option dropdown option
     */
    public void chooseShowTestingOption(String option){
        WebElement showTestingOption = driver.findElement(By.xpath("//option[.='"+option+"']"));
        showTestingDropdown.click();
        showTestingOption.click();
    }

    /**
     * Click On Filter Button
     */
    public void clickOnFilterButton(){
        filterButton.click();
    }

    /**
     * Click On Redemption Button
     */
    public void clickOnViewRedemptionIcon(){
        viewRedemptionIcon.click();
    }

    /**
     * Click On Redeem Status Field
     */
    public void clickOnRedeemStatusField(){
        redeemStatusField.click();
    }

    /**
     * Choose Option to Change Redemption Status
     */
    public void changeRedemptionStatus(String status){
        WebElement statusOption = driver.findElement(By.xpath("//div[@class='modal-body']//option[.='"+status+"']"));
        statusOption.click();
    }

    /**
     * Set Redemption Notes
     *
     * @param notes redemption notes
     */
    public void setRedemptionNotes(String notes){
        notesField.sendKeys(notes);
    }

    /**
     * Click On Submit Button
     */
    public void clickOnSubmitButton(){
        submitButton.click();
    }

    /**
     * Verify Redemption Status is Changed or not
     *
     * @param status status of redemption
     * @return boolean
     */
    public boolean isRedemptionStatusChanged(String status){
        WebElement redemptionStatus = driver.findElement(By.xpath("(//a[contains(.,'"+status+"')])[1]"));
        return redemptionStatus.isDisplayed();
    }

    /**
     * Click on Status Dropdown
     */
    public void clickOnStatusDropdown(){
        statusDropdown.click();
    }

}
