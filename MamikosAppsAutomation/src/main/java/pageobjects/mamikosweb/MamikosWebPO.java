package pageobjects.mamikosweb;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.JavaHelpers;

public class MamikosWebPO {
    JavaHelpers java;
    WebDriver driver;

    public MamikosWebPO(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all Android Elements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    /*
     * All Android Elements are identified by @AndroidFindBy annotation
     * @AndroidFindBy can uiAutomator, accessibility , id , className , tagName, androidDataMatcher, xpath , priority as attributes.
     */
    @FindBy(className = "btn-login")
    private WebElement enterButton;


    @FindBy(name = "Password")
    private WebElement passwordTextbox;

    @FindBy(id = "email")
    private WebElement FbEmailTextbox;

    @FindBy(className = "login-user-home")
    private WebElement tenantButton;

    //login by FB from top header
    @FindBy(xpath = "//*[@class='modal-login modal in']//*[@class='btn btn-action btn-facebook']")
    private WebElement FbLoginTenantHeader;

    @FindBy(id = "pass")
    private WebElement FbPasswordTextbox;

    @FindBy(id = "loginbutton")
    private WebElement FBLoginButton;

    @FindBy(xpath = "(//label[contains(text(),'Lihat selengkapnya')])[1]")
    private WebElement firstViewMore;

    @FindBy(xpath = "(//button[contains(text(),'Batalkan Booking')])[2]")
    private WebElement cancelBookingButton;

    @FindBy(xpath = "(//button[contains(text(),'Ya, Batalkan')])[2]")
    private WebElement yesCancelButton;

    @FindBy(xpath = "//*[@class='detail-tenant']")
    private WebElement firstDetailTenant;

    /**
     * Login Tenant by Facebook
     */
    public void tenantLogin(String email,String password) throws InterruptedException {
        enterButton.click();
        Thread.sleep(1000);
        tenantButton.click();
        Thread.sleep(2000);
        FbLoginTenantHeader.click();
        Thread.sleep(2000);
        FbEmailTextbox.sendKeys(email);
        Thread.sleep(2000);
        FbPasswordTextbox.sendKeys(password);
        Thread.sleep(2000);
        FBLoginButton.click();
    }

    /**
     * Cancel booking if tenant have
     * @throws InterruptedException
     */
    public void cancelBooking() throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        firstViewMore.click();
        Thread.sleep(2000);
        jse.executeScript("arguments[0].scrollIntoView(true);",firstDetailTenant);
        Thread.sleep(2000);

        if (this.isElementDisplayed(By.xpath("(//button[contains(text(),'Batalkan Booking')])[2]"), 2)){
            Thread.sleep(2000);
            cancelBookingButton.click();
            Thread.sleep(2000);
            yesCancelButton.click();
            Thread.sleep(2000);
        }
    }

    /**
     * Verify element is displayed
     * @param el WebElement object
     * @return boolean
     */
    public Boolean isElementDisplayed(By el, long timeoutInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
            wait.until(ExpectedConditions.visibilityOfElementLocated(el));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
