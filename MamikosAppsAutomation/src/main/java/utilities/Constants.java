package utilities;

import org.openqa.selenium.Platform;

import java.io.File;

public class Constants
{
	public static final String PROPERTYFILE="src/main/resources/constants.properties";	
	
	//Test Run Platform
	public static final Platform MOBILE_OS = setMobileOS();
	public static final String PLATFORM = JavaHelpers.setSystemVariable(PROPERTYFILE ,"Platform");
	public static final String PLATFORM_VERSION = JavaHelpers.setSystemVariable(PROPERTYFILE, "PlatformVersion");
	public static final String DEVICE_NAME= JavaHelpers.setSystemVariable(PROPERTYFILE,"DeviceName");

	//OpenSTF
	public static final String OPENSTF_URL= JavaHelpers.setSystemVariable(PROPERTYFILE,"OpenSTF_URL");
	public static final String OPENSTF_TOKEN= JavaHelpers.setSystemVariable(PROPERTYFILE,"OpenStfAccessToken");
	
	//Kobiton
	public static final String APP_STORAGE_DETAILS= JavaHelpers.setSystemVariable(PROPERTYFILE,"AppStorageDetails");
	public static final String KOBITON_USERNAME= JavaHelpers.setSystemVariable(PROPERTYFILE,"KobitonUsername");
	public static final String KOBITON_APIKEY= JavaHelpers.setSystemVariable(PROPERTYFILE,"KobitonApikey");
	
	//Application details
	public static final String MAMIKOS_APK_FULLNAME= JavaHelpers.getPropertyValue(PROPERTYFILE,"MamikosApkFullName");
	public static final String MAMIKOS_APK_FULLPATH = new File("src/main/resources/apk", MAMIKOS_APK_FULLNAME).getAbsolutePath();
	public static final String MAMIKOS_APP_WAIT_ACTIVITY = JavaHelpers.getPropertyValue(PROPERTYFILE, "MamikosAppWaitActivity");
	public static final String MAMIKOS_APP_FULLNAME = JavaHelpers.getPropertyValue(PROPERTYFILE, "MamikosAppFullName");
	public static final String MAMIKOS_APP_FULLPATH = new File("src/main/resources/app", MAMIKOS_APP_FULLNAME).getAbsolutePath();
	public static final String MAMIKOS_APP_BUNDLED_ID = JavaHelpers.getPropertyValue(PROPERTYFILE, "MamikosAppBundledID");
	public static final String MAMIKOS_APP_PACKAGE_ID = JavaHelpers.getPropertyValue(PROPERTYFILE, "MamikosAppPackageID");

	//Test Server Details
	public static final String SERVER_KEY = JavaHelpers.setSystemVariable(PROPERTYFILE, "key");
	public static final String SERVER_PAY = JavaHelpers.setSystemVariable(PROPERTYFILE, "pay");
	public static final String SERVER_API = JavaHelpers.setSystemVariable(PROPERTYFILE, "api");

	//Appium constants
	public static final int WEBDRIVER_WAIT_DURATION = Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE, "WebDriverWaitDuration"));
	public static final int MINIMUM_WEBDRIVER_WAIT_DURATION = Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE, "MinimumWebDriverWaitDuration"));
	public static final int NEW_COMMAND_TIMEOUT = Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE, "NewCommandTimeout"));
	public static final int SERVER_LUANCH_TIMEOUT = Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE, "ServerLaunchTimeout"));


	//Tenant Facebook Login Details
	public static final String TenantFacebookEmail = JavaHelpers.getPropertyValue(PROPERTYFILE, "TenantFacebookEmail");
	public static final String TenantFacebookPassword = JavaHelpers.getPropertyValue(PROPERTYFILE, "TenantFacebookPassword");
	public static final String TenantFacebookName = JavaHelpers.getPropertyValue(PROPERTYFILE, "TenantFacebookName");

	//Owner Login Details
	public static final String OwnerMobile = JavaHelpers.getPropertyValue(PROPERTYFILE, "OwnerMobileNumber");
	public static final String OwnerPassword = JavaHelpers.getPropertyValue(PROPERTYFILE, "OwnerPassword");

	//Back Office login details
	public static final String BACKOFFICE_URL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_url");
	public static final String BACKOFFICE_LOGIN_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_login_email");
	public static final String BACKOFFICE_LOGIN_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_login_password");

	//Mamikos Web
	public static final String MAMIKOS_WEB = JavaHelpers.getPropertyValue(PROPERTYFILE, "mamikos_web");

	//Mamikos Admin
	public static final String MAMIKOS_ADMIN = JavaHelpers.getPropertyValue(PROPERTYFILE, "mamikos_admin");

	//Other
	public static final String SCREENSHOT_LOCATION = JavaHelpers.getPropertyValue(PROPERTYFILE, "ScreenshotLocation");

	//ChromeDriver path
	public static final String CHROMEDRIVER_DIRECTORY_PATH = "src/main/resources/drivers";

	//Mamikos Admin login details
	public static final String MAMIKOSADMIN_URL = JavaHelpers.getPropertyValue(PROPERTYFILE, "mamikos_admin");
	public static final String MAMIKOSADMIN_LOGIN_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "mamikosadmin_login_email");
	public static final String MAMIKOSADMIN_LOGIN_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "mamikosadmin_login_password");


	//Set Mobile OS
	private static Platform setMobileOS() {
		String os = JavaHelpers.setSystemVariable(PROPERTYFILE, "MobileOperatingSystem");
		if (os.equalsIgnoreCase("android")) {
			return Platform.ANDROID;
		}
		else if (os.equalsIgnoreCase("iOS"))
		{
			return Platform.IOS;
		}
		return null;
	}

	private Constants() 
	{
	    throw new IllegalStateException("Constants class");
	}

}


