package utilities;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class DriverManager
{

	public void capabilities(String scenarioName) throws IOException
	{

		DesiredCapabilities capabilities = new DesiredCapabilities();
		String os = System.getProperty("os.name").toLowerCase();

		if(Constants.MOBILE_OS==Platform.ANDROID)
		{
			capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
			capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
			capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, Constants.MAMIKOS_APP_WAIT_ACTIVITY);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, Constants.NEW_COMMAND_TIMEOUT);
			capabilities.setCapability("uiautomator2ServerLaunchTimeout", Constants.SERVER_LUANCH_TIMEOUT);
			capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
			capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT, true);
			capabilities.setCapability("ignoreUnimportantViews", true);
			capabilities.setCapability("deviceOrientation","portrait");
			capabilities.setCapability("autoGrantPermissions", true);

			if(Constants.PLATFORM.equalsIgnoreCase("kobiton"))
			{
				capabilities.setCapability("sessionName", "Scenario Name:" + scenarioName);
				capabilities.setCapability("sessionDescription", "Started on:" + new JavaHelpers().getTimeStamp("dd MMM yyyy HH:mm:ss"));
				capabilities.setCapability(MobileCapabilityType.APP, Constants.APP_STORAGE_DETAILS);
				capabilities.setCapability("deviceGroup", "KOBITON");
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, Constants.DEVICE_NAME);
				capabilities.setCapability("platformVersion", Constants.PLATFORM_VERSION);
				String kobitonServer = "https://" + Constants.KOBITON_USERNAME + ":" + Constants.KOBITON_APIKEY + "@api.kobiton.com/wd/hub";
				ThreadManager.setDriver(new AndroidDriver<>(new URL(kobitonServer), capabilities));
			}
			else
			{
				capabilities.setCapability("systemPort", ThreadManager.getSystemPort());
				capabilities.setCapability(MobileCapabilityType.APP, Constants.MAMIKOS_APK_FULLPATH);
				capabilities.setCapability(MobileCapabilityType.UDID, ThreadManager.getDeviceUdid());
				capabilities.setCapability("chromedriverExecutableDir", Constants.CHROMEDRIVER_DIRECTORY_PATH);

				ThreadManager.setDriver( new AndroidDriver<>(ThreadManager.getAppiumLocalService().getUrl(), capabilities));
			}
		}
		else
		{
			capabilities.setCapability("platformName", Platform.IOS);
			capabilities.setCapability("platformVersion", ThreadManager.getPlatformVersion());
			capabilities.setCapability("deviceName", ThreadManager.getDeviceName());
			capabilities.setCapability("wdaLocalPort", ThreadManager.getWdaLocalPort());
			capabilities.setCapability("autoAcceptAlerts", true);
			capabilities.setCapability("autoDissmissAlerts", true);
			capabilities.setCapability("app", Constants.MAMIKOS_APP_FULLPATH);
			capabilities.setCapability("locationServicesEnabled", "true");
			capabilities.setCapability("locationServicesAuthorized", "true");
			capabilities.setCapability("permissions", "{\"com.git.MamiKos\": {\"location\": \"inuse\"}}");
			ThreadManager.setDriver( new IOSDriver<>(ThreadManager.getAppiumLocalService().getUrl(), capabilities));
		}
		
		ThreadManager.getDriver().manage().timeouts().implicitlyWait(Constants.MINIMUM_WEBDRIVER_WAIT_DURATION, TimeUnit.SECONDS);
	}
	
	/**
	 * Quit driver instance
	 */
	public void tearDown()
	{
		if(ThreadManager.getDriver()!=null)
		{
			ThreadManager.getDriver().quit();
		}
	}	

}
