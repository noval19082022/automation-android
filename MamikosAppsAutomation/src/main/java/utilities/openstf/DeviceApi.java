package utilities.openstf;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.*;
import utilities.JavaHelpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * This class provides the capability to connect or disconnect device.
 */
public class DeviceApi {
    private OkHttpClient client;
    private static JavaHelpers javaHelper = new JavaHelpers();
    private JsonParser jsonParser;
    private STFService stfService;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final Logger LOGGER = Logger.getLogger(DeviceApi.class.getName());
    private static final String[] WIN_RUNTIME = { "cmd.exe", "/C" };
    private static final String[] OS_LINUX_RUNTIME = { "/bin/bash", "-c" };

    public DeviceApi(STFService stfService) throws IOException {
        FileHandler handler = new FileHandler("logs/openStf.log", true);
        LOGGER.addHandler(handler);
        this.client = new OkHttpClient();
        this.jsonParser = new JsonParser();
        this.stfService = stfService;
    }
    /** Execute cmd command
     * Used to execute adb command
     * @param isWin is OS where script run (Windows=true, non-Windows=false)
     * @param command is shell/bash command to execute, can be multiple
     */
    public static List<String> runProcess(boolean isWin, String... command) {
        LOGGER.info("command to run: ");
        for (String s : command) {
            LOGGER.info(s);
        }
        String[] allCommand = null;
        try {
            if (isWin) {
                allCommand = javaHelper.concat(WIN_RUNTIME, command);
            } else {
                allCommand = javaHelper.concat(OS_LINUX_RUNTIME, command);
            }
            ProcessBuilder pb = new ProcessBuilder(allCommand);
            pb.redirectErrorStream(true);
            Process p = pb.start();
            p.waitFor();
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String _temp = null;
            List<String> line = new ArrayList<String>();
            while ((_temp = in.readLine()) != null) {
                line.add(_temp);
            }
            LOGGER.info("result after command: " + line);
            return line;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * Connect to Device in OpenSTF
     * @param deviceSerial is device serial number
     * if device is in use it will release it
     * @return go to method addDeviceToUser, in the end return device UDID
     */
    public String connectDevice(String deviceSerial) {
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + stfService.getAuthToken())
                .url(stfService.getStfUrl() + "devices/" + deviceSerial)
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();
            if (!isDeviceFound(jsonObject)) {
                return null;
            }

            JsonObject deviceObject = jsonObject.getAsJsonObject("device");
            boolean present = deviceObject.get("present").getAsBoolean();
            boolean ready = deviceObject.get("ready").getAsBoolean();
            boolean using = deviceObject.get("using").getAsBoolean();
            JsonElement ownerElement = deviceObject.get("owner");
            boolean owner = !(ownerElement instanceof JsonNull);

            if (!present || !ready || using || owner) {
                LOGGER.severe("Device is in use");
                releaseDevice(deviceSerial);
            }

            return addDeviceToUser(deviceSerial);
        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }
    /**
     * Check device availability
     * @param jsonObject is response from OpenSTF
     * @return boolean
     */
    private boolean isDeviceFound(JsonObject jsonObject) {
        if (!jsonObject.get("success").getAsBoolean()) {
            LOGGER.severe("Device not found");
            return false;
        }
        return true;
    }
    /**
     * Lock device to user
     * @param deviceSerial is the device serial
     * @return go to method getRemoteURL
     */
    private String addDeviceToUser(String deviceSerial) {
        RequestBody requestBody = RequestBody.create(JSON, "{\"serial\": \"" + deviceSerial + "\"}");
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + stfService.getAuthToken())
                .url(stfService.getStfUrl() + "user/devices")
                .post(requestBody)
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            assert response.body() != null;
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();

            if (!isDeviceFound(jsonObject)) {
                return null;
            }
            LOGGER.info("The device <" + deviceSerial + "> is locked successfully");
            return getRemoteURL(deviceSerial);
        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }
    /**
     * Get remote URL to connect adb
     * @param deviceSerial is the device serial
     * Connect to device via adb remotely after get remote url
     * @return remote url
     */
    private String getRemoteURL(String deviceSerial) {
        RequestBody requestBody = RequestBody.create(JSON, "{\"serial\": \"" + deviceSerial + "\"}");
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + stfService.getAuthToken())
                .url(stfService.getStfUrl() + "user/devices/" + deviceSerial + "/remoteConnect")
                .post(requestBody)
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            assert response.body() != null;
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();
            runProcess(false, "adb connect " + jsonObject.get("remoteConnectUrl"));
            runProcess(false, "adb devices");
            return jsonObject.get("remoteConnectUrl").getAsString();
        } catch (IOException e) {
            throw new IllegalArgumentException("ADB connect error", e);
        }
    }

    /**
     * Release/disconnect device
     * @param deviceSerial is the device serial
     */
    public void releaseDevice(String deviceSerial) {
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + stfService.getAuthToken())
                .url(stfService.getStfUrl() + "user/devices/" + deviceSerial)
                .delete()
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            assert response.body() != null;
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();
            if (!isDeviceFound(jsonObject)) {
                LOGGER.info("The device <" + deviceSerial + "> failed to release");;
            }

            LOGGER.info("The device <" + deviceSerial + "> is released successfully");
        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }
}
