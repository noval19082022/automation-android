package utilities.openstf;

import utilities.Constants;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * This class creates a STF Service object.
 */
public class STFService {

    private String stfUrl;
    private String authToken;

    public STFService(String stfUrl, String authToken) throws MalformedURLException, URISyntaxException {
        this.stfUrl = new URL(stfUrl).toURI().resolve("/api/v1/").toString();
        this.authToken = authToken;
    }

    /**
     * Get openSTF url
     * @return String openSTF url
     */
    public String getStfUrl() {
        return stfUrl;
    }

    /**
     * Get openSTF authentication token
     * @return String token
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Connect to Device in OpenSTF
     * @param deviceSerial is device serial number
     * @return connect to device method then return device UDID
     */
    public static String connectToStfDevice(String deviceSerial) throws IOException, URISyntaxException {
        STFService stfService = new STFService(Constants.OPENSTF_URL,
                Constants.OPENSTF_TOKEN);
        DeviceApi deviceApi = new DeviceApi(stfService);
        return deviceApi.connectDevice(deviceSerial);
    }

    /**
     * Disconnect Device in OpenSTF
     * @param deviceSerial is device serial number
     */
    public static void disconnectDevice(String deviceSerial) throws IOException, URISyntaxException {
        STFService stfService = new STFService(Constants.OPENSTF_URL,
                Constants.OPENSTF_TOKEN);
        DeviceApi deviceApi = new DeviceApi(stfService);
        deviceApi.releaseDevice(deviceSerial);
    }

}
