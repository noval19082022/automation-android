package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    plugin = {"json:target/results/booking-growth/cucumber-report.json",  "html:target/results/booking-growth"},
    features = "src/test/resources/features",
    glue = "steps",
    tags = "@bookingGrowth"
)
public class BookingGrowthTestRunner extends BaseTestRunnerAndroid
{

}
