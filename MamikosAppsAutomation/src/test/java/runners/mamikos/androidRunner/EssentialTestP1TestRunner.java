package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
	plugin = {"json:target/results/essentialTestP1/cucumber-report.json",  "html:target/results/essentialTestP1/cucumber-report.html"},
	features = "src/test/resources/features",
	glue = "steps",
	tags = "@essentialTest1"
)
public class EssentialTestP1TestRunner extends BaseTestRunnerAndroid
{
	
}


