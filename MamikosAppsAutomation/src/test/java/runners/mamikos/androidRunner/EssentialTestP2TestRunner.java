package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
	plugin = {"json:target/results/essentialTestP2/cucumber-report.json",  "html:target/results/essentialTestP2/cucumber-report.html"},
	features = "src/test/resources/features",
	glue = "steps",
	tags = "@essentialTest1"
)
public class EssentialTestP2TestRunner extends BaseTestRunnerAndroid
{
	
}


