package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
	plugin = {"json:target/results/essentialTestP3/cucumber-report.json",  "html:target/results/essentialTestP3/cucumber-report.html"},
	features = "src/test/resources/features",
	glue = "steps",
	tags = "@essentialTest2"
)
public class EssentialTestP3TestRunner extends BaseTestRunnerAndroid
{
	
}


