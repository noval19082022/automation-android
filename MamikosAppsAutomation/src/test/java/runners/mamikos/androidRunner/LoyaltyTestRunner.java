package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/loyalty/cucumber-report.json",  "html:target/results/loyalty"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@loyalty"
)
public class LoyaltyTestRunner extends BaseTestRunnerAndroid
{
}
