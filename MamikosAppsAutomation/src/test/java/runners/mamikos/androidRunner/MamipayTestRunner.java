package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
	plugin = {"json:target/results/mamipay/cucumber-report.json",  "html:target/results/mamipay"},
	features = "src/test/resources/features",
	glue = "steps",
	tags = "@mamipay"
)
public class MamipayTestRunner extends BaseTestRunnerAndroid
{
	
}



