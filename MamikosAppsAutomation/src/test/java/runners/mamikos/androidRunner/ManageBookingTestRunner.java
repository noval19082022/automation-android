package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    plugin = {"json:target/results/manage-booking/cucumber-report.json",  "html:target/results/manage-booking"},
    features = "src/test/resources/features",
    glue = "steps",
    tags = "@manageBooking"
)
public class ManageBookingTestRunner extends BaseTestRunnerAndroid
{

}
