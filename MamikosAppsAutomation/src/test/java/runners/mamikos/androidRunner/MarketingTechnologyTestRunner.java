package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    plugin = {"json:target/results/martech/cucumber-report.json",  "html:target/results/martech"},
    features = "src/test/resources/features",
    glue = "steps",
    tags = "@martech"
)

public class MarketingTechnologyTestRunner extends BaseTestRunnerAndroid
{

}
