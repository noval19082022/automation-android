package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    plugin = {"json:target/results/ownerexp/cucumber-report.json",  "html:target/results/ownerexp"},
    features = "src/test/resources/features",
    glue = "steps",
    tags = "@ownerExp"
)

public class OwnerExperienceTestRunner extends BaseTestRunnerAndroid
{
    
}
