package runners.mamikos.androidRunner;

        import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/payment/cucumber-report.json",  "html:target/results/payment"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@payment"
)
public class PaymentTestRunner extends BaseTestRunnerAndroid
{

}