package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/premium/cucumber-report.json",  "html:target/results/premium"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@upgradepremium"

)
public class PremiumTestRunner extends BaseTestRunnerAndroid
{
}
