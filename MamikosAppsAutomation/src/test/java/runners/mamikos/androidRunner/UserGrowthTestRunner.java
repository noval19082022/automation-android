package runners.mamikos.androidRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    plugin = {"json:target/results/user-growth/cucumber-report.json", "html:target/results/user-growth"},
    features = "src/test/resources/features",
    glue = "steps",
    tags = "@user-growth"
)
public class UserGrowthTestRunner extends BaseTestRunnerAndroid{
}
