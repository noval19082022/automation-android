package runners.mamikos.iOSRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
					plugin = {"json:target/results/regression/cucumber-report.json",  "html:target/results/regression/cucumber-report.html"},
					features = "src/test/resources/features",
					glue = "steps",
					tags = "@regression and @iOS"
					
		)
public class AllTestRunner extends BaseTestRunneriOS
{
	
}


