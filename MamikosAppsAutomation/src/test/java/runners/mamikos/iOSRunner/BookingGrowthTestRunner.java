package runners.mamikos.iOSRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking-growth-ios/cucumber-report.json",  "html:target/results/booking-growth-ios"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@bookingGrowth and @iOS"

)
public class BookingGrowthTestRunner extends BaseTestRunneriOS
{

}
