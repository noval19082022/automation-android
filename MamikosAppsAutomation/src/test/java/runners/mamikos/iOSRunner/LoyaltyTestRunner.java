package runners.mamikos.iOSRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/loyalty/ios/cucumber-report.json",  "html:target/results/loyalty/ios"},
        features = "src/test/resources/features/",
        glue = "steps",
        tags = "@loyalty and @iOS"
)
public class LoyaltyTestRunner extends BaseTestRunneriOS
{

}


