package runners.mamikos.iOSRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/loyalty/ios/cucumber-report.json",  "html:target/results/ownerexp-ios"},
        features = "src/test/resources/features/",
        glue = "steps",
        tags = "@ownerExp and @iOS"
)
public class OwnerExperienceTestRunner extends BaseTestRunneriOS {

}