package runners.mamikos.iOSRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/payment/ios/cucumber-report.json",  "html:target/results/payment/ios"},
        features = "src/test/resources/features/",
        glue = "steps",
        tags = "@payment and @iOS"

)
public class PaymentTestRunner extends BaseTestRunneriOS
{

}


