package runners.mamikos.iOSRunner;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
			plugin = {"json:target/results/user-growth-ios/cucumber-report.json",  "html:target/results/user-growth-ios"},
			features = "src/test/resources/features",
			glue = "steps",
			tags = "@user-growth and @iOS"
)
public class UserGrowthTestRunner extends BaseTestRunneriOS
{
	
}


