package steps.mamikos;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.ForgotPasswordPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;
import java.util.List;

public class ForgotPasswordSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ForgotPasswordPO forgotPasswordPO = new ForgotPasswordPO(driver);

    /* Test Data */
    private String forgotPasswordProps = "src/test/resources/testdata/mamikos/forgotPassword.properties";
    private String ownerPhoneNumber = JavaHelpers.getPropertyValueForSquad(forgotPasswordProps, "OwnerPhoneNumber");
    private String tenantPhoneNumber = JavaHelpers.getPropertyValueForSquad(forgotPasswordProps, "TenantPhoneNumber");

    @And("user fill out forgot password as {string}")
    public void user_fill_out_forgot_password_as(String type){
        String mobileNumber = "";

        if(type.equalsIgnoreCase("Owner Forgot Password")){
            mobileNumber = ownerPhoneNumber;
        }
        else if(type.equalsIgnoreCase("Tenant Forgot Password")){
            mobileNumber = tenantPhoneNumber;
        }
        forgotPasswordPO.fillRegisteredPhoneNumber(mobileNumber);
        forgotPasswordPO.clickOnChooseVerificationButton();
    }

    @When("user click on send verification code via SMS")
    public void user_click_on_send_verification_code_via_SMS() {
        forgotPasswordPO.clickOnSMSVerificationButton();
    }

    @Then("user redirect to {string} OTP Screen")
    public void user_redirect_to_OTP_Screen(String title) throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID)
            Assert.assertEquals(forgotPasswordPO.getTitlePageText(), title, "Title page is not equal to " + title);
        else
            Assert.assertEquals(forgotPasswordPO.getOwnerVerificationiOS(), title, "Title page is not equal to " + title);
    }

    @When("user click on send verification code via WA")
    public void user_click_on_send_verification_code_via_WA() {
        forgotPasswordPO.clickOnWAVerificationButton();
    }

    @Then("user verify resend OTP button and click resend OTP button")
    public void user_verify_resend_OTP_button_and_click_resend_OTP_button() {
        Assert.assertTrue(forgotPasswordPO.isResendOTPButtonAppear(), "Resend OTP Button is not appeared");
        forgotPasswordPO.clickOnResendOTPButton();
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            Assert.assertTrue(forgotPasswordPO.isResendOTPCountdownTimerAppear(), "Resend OTP Countdown timer is not appeared");
        }
    }

    @And("user input OTP with invalid value {string} {string} {string} {string}")
    public void userInputOTPWithInvalidValue(String one, String two, String three, String four) {
        forgotPasswordPO.fillOTPForm(one, two, three, four);
    }

    @Then("user verify OTP error messages {string}")
    public void user_verify_OTP_error_messages(String error) {
        Assert.assertEquals(forgotPasswordPO.getOTPErrorMessages(), error, "OTP error messages is not equal to " + error);
    }

    @And("user click back button on OTP page")
    public void user_click_back_button_on_OTP_page() throws InterruptedException {
        forgotPasswordPO.clickOnBackButton();
    }

    @And("user click {string} from confirmation dialog")
    public void user_click_from_confirmation_dialog(String message) {
        Assert.assertEquals(forgotPasswordPO.getCancelButtonText(), message, "Cancel button text is not equal to " + message);
        forgotPasswordPO.clickOnConfirmationDialog();
    }

    @Then("user redirect to choose verification method screen")
    public void user_redirect_to_screen(List<String> title) throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID)
            Assert.assertEquals(forgotPasswordPO.getTitlePageText(), title.get(0), "Title page is not equal to " + title);
        else
            Assert.assertEquals(forgotPasswordPO.getVerificationMethodiOS(), title.get(1), "Title page is not equal to " + title);
    }
}
