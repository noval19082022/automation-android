package steps.mamikos.account;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.login.LoginFormPO;
import pageobjects.mamikos.common.login.LoginOptionPO;
import pageobjects.mamikos.common.login.LoginPO;
import pageobjects.mamikos.common.register.RegistrationFormPO;
import pageobjects.mamikos.owner.premium.UpgradePremiumPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class LoginAsAOwnerSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private LoginFormPO loginForm = new LoginFormPO(driver);
	private LoginOptionPO loginOption = new LoginOptionPO(driver);
	private FooterPO footer = new FooterPO(driver);
	private PopUpPO popUp = new PopUpPO(driver);
	private LoginPO login = new LoginPO(driver);
	private RegistrationFormPO registrationFrom = new RegistrationFormPO(driver);
	private LoadingPO loading = new LoadingPO(driver);
	private UpgradePremiumPO premiumPO = new UpgradePremiumPO(driver);

	//Test Data
	private String propertyFile="src/test/resources/testdata/mamikos/editProfile.properties";
	private String bookingConstantFileLoc = "src/test/resources/testdata/mamikos/booking.properties";
	private String serverKayPay = Constants.SERVER_KEY + "_" + Constants.SERVER_PAY;
	private String noKostListOwner = JavaHelpers.getPropertyValue(bookingConstantFileLoc, "noPropertyListOwner_"  + serverKayPay);
	private String noKostListOwnerPassword = JavaHelpers.getPropertyValue(bookingConstantFileLoc, "noPropertyListOwnerPassword_"  + serverKayPay);
	private String havePropertyListOwner = JavaHelpers.getPropertyValue(bookingConstantFileLoc, "havePropertyListOwner_" + serverKayPay);
	private String havePropertyListOwnerPassword = JavaHelpers.getPropertyValue(bookingConstantFileLoc, "havePropertyListOwnerPassword_" + serverKayPay);
	private String ownerHaveTenantWaitingForApproval = JavaHelpers.getPropertyValue(bookingConstantFileLoc, "ownerHaveTenantWaitingForApproval_" + serverKayPay);
	private String ownerHaveTenantWaitingForApprovalPassword = JavaHelpers.getPropertyValue(bookingConstantFileLoc, "ownerHaveTenantWaitingForApprovalPassword_" + serverKayPay);
	private String nonActiveMamipayMobile = JavaHelpers.getPropertyValueForSquad(propertyFile,"NonActiveMamipayMobile");
	private String nonActiveMamipayPassword = JavaHelpers.getPropertyValueForSquad(propertyFile,"NonActiveMamipayPassword");
	private String activeMamipayMobile = JavaHelpers.getPropertyValueForSquad(propertyFile,"ActiveMamipayMobile");
	private String activeMamipayPassword = JavaHelpers.getPropertyValueForSquad(propertyFile,"ActiveMamipayPassword");

	//Test Data for Add Voucher
	private String addVoucherProperties="src/test/resources/testdata/mamikos/addVoucher.properties";
	private String ownerMobileNumberForAddVoucher = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"ownerMobileNumberForAddVoucher");
	private String ownerPasswordForAddVoucher = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"ownerPasswordForAddVoucher");
	private String ownerMobileNumberForAddVoucherMamirooms = JavaHelpers.getPropertyValueForSquad(addVoucherProperties, "ownerMobileNumberForAddVoucherMamirooms");
	private String ownerPasswordForAddVoucherMamirooms = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"ownerPasswordForAddVoucherMamirooms");

	//Test Data for Mamipoin
	private String mamipoinProperties="src/test/resources/testdata/mamikos/mamiPoinOwner.properties";
	private String ownerMobileNumberForEmptyRewardHistory = JavaHelpers.getPropertyValueForSquad(mamipoinProperties, "ownerMobileNumberForRewardHistoryEmpty");
	private String ownerPasswordForEmptyRewardHistory = JavaHelpers.getPropertyValueForSquad(mamipoinProperties, "ownerPasswordForRewardHistoryEmpty");
	private String ownerMobileNumberForMamiPoin = JavaHelpers.getPropertyValueForSquad(mamipoinProperties, "ownerMobileNumberForMamiPoin");
	private String ownerPasswordForMamiPoin = JavaHelpers.getPropertyValueForSquad(mamipoinProperties, "ownerPasswordForMamiPoin");

	//Test Data for Mamipay dashboard
	private String mamiPayProperties= "src/test/resources/testdata/mamipay/mamiPay.properties";
	private String newOwnerMobileNumber = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"newOwnerMobileNumber");
	private String newOwnerPassword = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"newOwnerPassword");
	private String propertyOwnerMobileNumber = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"propertyOwnerMobileNumber");
	private String propertyOwnerPassword = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"propertyOwnerPassword");
	private String ownerMobileNumberHavingBalance = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"ownerMobileNumberHavingBalance");
	private String ownerPasswordHavingBalance = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"ownerPasswordHavingBalance");
	private String premiumOwnerMobileNumber = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"premiumOwnerMobileNumber");
	private String premiumOwnerPassword = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"premiumOwnerPassword");
	private String ownerLoginPhoneBBKNotActive = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"ownerLoginPhoneBBKNotActive");
	private String ownerLoginPasswordBBKNotActive = JavaHelpers.getPropertyValueForSquad(mamiPayProperties,"ownerLoginPasswordBBKNotActive");

	//Test Data for upgrade premium account
	private String premiumProperties= "src/test/resources/testdata/mamikos/premium.properties";
	private String ownerMobileNumberForNonPremiumHaveKost = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerMobileNumberForNonPremiumHaveKost");
	private String ownerPasswordForNonPremiumHaveKost = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerPasswordForNonPremiumHaveKost");

	private String ownerMobileNumberForPremiumPayment = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerMobileNumberForPremiumPayment");
	private String ownerPasswordForPremiumPayment = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerPasswordForPremiumPayment");

	private String ownerMobileNumberForPremiumSaldo = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerMobileNumberForPremiumSaldo");
	private String ownerPasswordForPremiumSaldo = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerPasswordForPremiumSaldo");

	private String ownerMobileNumberForPremium = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerMobileNumberForPremium");
    private String ownerPasswordForPremium = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerPasswordForPremium");

	private String ownerMobileNumberForNonPremium = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerMobileNumberForNonPremium");
	private String ownerPasswordForNonPremium = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerPasswordForNonPremium");

	private String ownerMobileNumberForNonKostDiTolak = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerMobileNumberForNonKostDiTolak");
	private String ownerPasswordForNonKostDiTolak = JavaHelpers.getPropertyValueForSquad(premiumProperties,"ownerPasswordForNonKostDiTolak");

	//Test Data Payment
	private String paymentProperties ="src/test/resources/testdata/mamikos/payment.properties";
	private String ownerNumber_payment_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"ownerNumber_payment");
	private String ownerPassword_payment_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"ownerPassword_payment");

	private String ownerNumber_paymentiOS_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"ownerNumber_paymentiOS");
	private String ownerPassword_paymentiOS_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"ownerPassword_paymentiOS");

	//Test Data OB
	private String obProperties ="src/test/resources/testdata/mamikos/OB.properties";
	private String ownerOBPhone = JavaHelpers.getPropertyValueForSquad(obProperties, "ownerOBPhone");
	private String ownerOBPassword = JavaHelpers.getPropertyValueForSquad(obProperties, "ownerOBPassword");

	//Test Data Essential Test
	private String essentialTestOwnerFile = "src/test/resources/testdata/mamikos/essentialTestOwner.properties";
	private String phoneOwnerEssentialTest = JavaHelpers.getPropertyValue(essentialTestOwnerFile, "ownerEssentialWendiCarter_"+Constants.SERVER_KEY+"_"+Constants.SERVER_PAY);
	private String passwordEssentialTest = JavaHelpers.getPropertyValue(essentialTestOwnerFile, "ownerEssentialWendiCarterPass_"+Constants.SERVER_KEY+"_"+Constants.SERVER_PAY);

	private String phoneOwnerEssentialTestNew = JavaHelpers.getPropertyValue(essentialTestOwnerFile, "ownerEssentialUpras_"+Constants.SERVER_KEY+"_"+Constants.SERVER_PAY);
	private String passwordEssentialTestNew = JavaHelpers.getPropertyValue(essentialTestOwnerFile, "ownerEssentialUprasPass_"+Constants.SERVER_KEY+"_"+Constants.SERVER_PAY);

	private String phoneOwnerEssentialTestP2 = JavaHelpers.getPropertyValue(essentialTestOwnerFile, "ownerEssentialTestP2_"+Constants.SERVER_KEY+"_"+Constants.SERVER_PAY);
	private String phoneOwnerEssentialTestP3 = JavaHelpers.getPropertyValue(essentialTestOwnerFile, "ownerEssentialTestP3_"+Constants.SERVER_KEY+"_"+Constants.SERVER_PAY);

	private String passwordEssentialTestGeneral = JavaHelpers.getPropertyValue(essentialTestOwnerFile, "ownerEssentialTestPassGeneral_"+Constants.SERVER_KEY+"_"+Constants.SERVER_PAY);

	@And("user fill out form and login with invalid mobile and password info")
	public void user_fill_out_form_and_login_with_invalid_mobile_and_password_info()
	{
		loginOption.clickOnAdOwnerButton();
		loginForm.fillFormAndClickOnLoginButton("089999999999", "piyush@tets");
	}

	@And("user logs in as Owner {string}")
	public void user_logs_in_as_Owner(String type) throws InterruptedException {
		footer.clickOnLoginMenu();
		loginOption.clickOnAdOwnerButton();
		String mobileNumber="";
		String password="";
		if (type.equalsIgnoreCase("master")){
			mobileNumber = Constants.OwnerMobile;
			password = Constants.OwnerPassword;
		}else if(type.equalsIgnoreCase("wrongpassword")){
			mobileNumber = Constants.OwnerMobile;
			password = "test@123";
		}else if(type.equalsIgnoreCase("wrongnumber")){
			mobileNumber = "08999999999";
			password = Constants.OwnerPassword;
		}else if(type.equalsIgnoreCase("nonactivemamipay")){
			mobileNumber = nonActiveMamipayMobile;
			password = nonActiveMamipayPassword;
		}else if(type.equalsIgnoreCase("activemamipay")){
			mobileNumber = activeMamipayMobile;
			password = activeMamipayPassword;
		}
		else if (type.equalsIgnoreCase("no property list")) {
			mobileNumber = noKostListOwner;
			password = noKostListOwnerPassword;
		}
		else if (type.equalsIgnoreCase("have property list")) {
			mobileNumber = havePropertyListOwner;
			password = havePropertyListOwnerPassword;
		}
		else if (type.equalsIgnoreCase("owner have tenant wait for booking approval")) {
			mobileNumber = ownerHaveTenantWaitingForApproval;
			password = ownerHaveTenantWaitingForApprovalPassword;
		} else if (type.equalsIgnoreCase("add voucher")){
			mobileNumber = ownerMobileNumberForAddVoucher;
			password = ownerPasswordForAddVoucher;
		} else if (type.equalsIgnoreCase("new Owner")){
			mobileNumber = newOwnerMobileNumber;
			password = newOwnerPassword;
		} else if (type.equalsIgnoreCase("property owner")){
			mobileNumber = propertyOwnerMobileNumber;
			password = propertyOwnerPassword;
		} else if (type.equalsIgnoreCase("owner BBK Reject")) {
			mobileNumber = ownerLoginPhoneBBKNotActive;
			password = ownerLoginPasswordBBKNotActive;
		} else if (type.equalsIgnoreCase("owner having balance")){
			mobileNumber = ownerMobileNumberHavingBalance;
			password = ownerPasswordHavingBalance;
		} else if (type.equalsIgnoreCase("premium owner")){
			mobileNumber = premiumOwnerMobileNumber;
			password = premiumOwnerPassword;
		}else if (type.equalsIgnoreCase("nonpremiumhavekost")){
			mobileNumber = ownerMobileNumberForNonPremiumHaveKost;
			password = ownerPasswordForNonPremiumHaveKost;
		}else if (type.equalsIgnoreCase("premiumpayment")){
			mobileNumber = ownerMobileNumberForPremiumPayment;
			password = ownerPasswordForPremiumPayment;
		} else if (type.equalsIgnoreCase("premiumsaldo")){
			mobileNumber = ownerMobileNumberForPremiumSaldo;
			password = ownerPasswordForPremiumSaldo;
		} else if (type.equalsIgnoreCase("premium")){
            mobileNumber = ownerMobileNumberForPremium;
            password = ownerPasswordForPremium;
        } else if (type.equalsIgnoreCase("nonpremium")){
			mobileNumber = ownerMobileNumberForNonPremium;
			password = ownerPasswordForNonPremium;
		} else if (type.equalsIgnoreCase("nonkostditolak")){
			mobileNumber = ownerMobileNumberForNonKostDiTolak;
			password = ownerPasswordForNonKostDiTolak;
		}
		else if (type.equalsIgnoreCase("payment")){
			mobileNumber = ownerNumber_payment_;
			password = ownerPassword_payment_;
		}
		else if (type.equalsIgnoreCase("add voucher mamirooms")){
			mobileNumber = ownerMobileNumberForAddVoucherMamirooms;
			password = ownerPasswordForAddVoucherMamirooms;
		}
		else if (type.equalsIgnoreCase("empty reward history")){
			mobileNumber = ownerMobileNumberForEmptyRewardHistory;
			password = ownerPasswordForEmptyRewardHistory;
		}
		else if (type.equalsIgnoreCase("mamipoin")){
			mobileNumber = ownerMobileNumberForMamiPoin;
			password = ownerPasswordForMamiPoin;
		}

		else if (type.equalsIgnoreCase("paymentiOS")){
			mobileNumber = ownerNumber_paymentiOS_;
			password = ownerPassword_paymentiOS_;
		}

		else if (type.equalsIgnoreCase("ob owner")) {
			mobileNumber = ownerOBPhone;
			password = ownerOBPassword;
		}
		else if (type.equalsIgnoreCase("owner essential test")){
			mobileNumber = phoneOwnerEssentialTest;
			password = passwordEssentialTest;
		}
		else if (type.equalsIgnoreCase("owner essential test new")){
			mobileNumber = phoneOwnerEssentialTestNew;
			password = passwordEssentialTestNew;
		}
		else if (type.equalsIgnoreCase("owner essential test p2")){
			mobileNumber = phoneOwnerEssentialTestP2;
			password = passwordEssentialTestGeneral;
		}
		else if (type.equalsIgnoreCase("owner essential test p3")){
			mobileNumber = phoneOwnerEssentialTestP3;
			password = passwordEssentialTestGeneral;
		}
		loginForm.fillFormAndClickOnLoginButton(mobileNumber,password);
		loading.waitLoadingFinish();
		popUp.clickOnLaterButton();
		popUp.dismissOwnerUpdatePropertyAdsPopup();
		popUp.closeExtraCostPopUp();
		popUp.dismissOwnerPopup();
		popUp.clickOnCloseInstantBookingTooltip();
		popUp.clickOnLaporanKeuanganTooltip();
		premiumPO.clickOnBalancePerDayPopUp();
	}

	@When("user logs in as Owner {string} for check bbk ftue")
	public void user_logs_in_as_owner_for_check_bbk_ftue(String owner) throws InterruptedException {
		footer.clickOnLoginMenu();
		loginOption.clickOnAdOwnerButton();
		String mobileNumber="";
		String password="";
		switch (owner) {
			case "owner essential test":
				mobileNumber = phoneOwnerEssentialTest;
				password = passwordEssentialTest;
				break;
			case "ob owner":
				mobileNumber = ownerOBPhone;
				password = ownerOBPassword;
				break;
			default:
				throw new IllegalArgumentException("Input with valid credential");
		}
		loginForm.fillFormAndClickOnLoginButton(mobileNumber,password);
		loading.waitLoadingFinish();
		popUp.clickOnLaterButton();
	}

	@And("user {string} log in as owner")
	public void user_log_in_as_owner(String owner) {
		footer.clickOnLoginMenu();
		loginOption.clickOnAdOwnerButton();
		String mobileNumber="";
		String password="";
		if (owner.equals("payment")){
			mobileNumber = ownerNumber_payment_;
			password = ownerPassword_payment_;
		}
		loginForm.fillFormAndClickOnLoginButton(mobileNumber,password);
		popUp.clickOnLaterButton();

		try {
			popUp.dismissOwnerPopup();
		}
		catch (Exception e) {
			// Nothing
		}

		try {
			popUp.clickOnCloseInstantBookingTooltip();
		}
		catch (Exception e) {
			// Nothing
		}
		popUp.clickOnCancelEmailVerifButton();
	}

	@And("user click on forgot password button from login page")
	public void userClickOnForgotPasswordButtonFromLoginPage() {
		loginOption.clickOnAdOwnerButton();
		loginOption.clickOnForgotPasswordLink();
	}

	@And("user enter Owner mobile number {string} and Click on Kirim Button")
	public void userEnterOwnerMobileNumberAndClickOnKirimButton(String ownerMobileNumber)
	{
		loginForm.fillMobileNumberAndClickOnKirimButton(ownerMobileNumber);
	}

	@Then("user redirect to OTP screen Which has text {string}")
	public void userRedirectToOTPScreenWhichHasText(String verificationText) {
		String text = loginForm.getVerificationText();
		Assert.assertTrue(text.contains(verificationText),"Verification text contains doesn't match. Actual Text: " + text);
	}

	@Then("correct validation message {string} is displayed")
	public void correctValidationMessageIsDisplayed(String expectedMessage) {
		Assert.assertEquals(loginForm.getErrorMessage(expectedMessage), expectedMessage ,"message is not correct");
	}

	@And("user click on login button from register page")
	public void userClickOnLoginButtonFromRegisterPage() {
			registrationFrom.clickOnLoginButton();
	}

	@And("user enter credential {string} and {string} and click on Login Button")
	public void userEnterCredentialAndAndClickOnLoginButton(String phoneValue, String passwordValue) {
			loginForm.fillFormAndClickOnLoginButton(phoneValue, passwordValue);
		popUp.closeExtraCostPopUp();
		premiumPO.clickOnBalancePerDayPopUp();
	}

	@And("user enter credential {string} and {string} and click on Password toggle")
	public void user_enter_credential_and_click_on_Password_toggle(String phoneValue, String passwordValue) {
		loginOption.clickOnAdOwnerButton();
		loginForm.fillFormAndClickOnPasswordToggle(phoneValue, passwordValue);
	}

	@Then("user verify visible password field with {string}")
	public void user_verify_visible_password_field_with(String pw) {
		if(Constants.MOBILE_OS == Platform.ANDROID) {
			Assert.assertEquals(loginForm.getPasswordText(), pw, "Password is not equal to " + pw);
			Assert.assertTrue(loginForm.getPasswordAttribute(), "Password attribute is not present");
		}
		//iOS can't get selected password field idk why
	}

	@When("user clicks on Login as Owner")
	public void user_clicks_on_login_as_owner() {
		loginOption.clickOnAdOwnerButton();
	}

	@When("owner login with phone number {string}, password {string} and clear text is {string}")
	public void owner_login_with_phone_number_password_and_clear_text_is(String phoneNumber, String password, String clearText) throws InterruptedException {
		boolean clear = clearText.equalsIgnoreCase("true");
		loginForm.fillFormAndClickOnLoginButton(phoneNumber, password, clear);
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			loading.waitLoadingFinish();
		}
		popUp.clickOnLaterButton();
	}

	@When("owner taps later button on staging")
	public void owner_tap_later_button_on_staging() throws InterruptedException {
		loading.waitLoadingFinish();
		if(!Constants.SERVER_KEY.equalsIgnoreCase("Kay")) {
			popUp.clickOnLaterButton();
		}
	}

	@Then("owner can sees manage additional price pop up")
	public void owner_can_sees_need_access_location() {
		Assert.assertEquals(popUp.getMessageViewText(), "Atur Biaya Tambahan di Kos Anda");
	}
}
