package steps.mamikos.account;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.facebook.account.ExistingLoginConfirmationPO;
import pageobjects.facebook.account.FacebookLoginPO;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.login.LoginOptionPO;
import pageobjects.mamikos.common.login.LoginPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.Set;

public class LoginAsATenantSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private LoginOptionPO loginOption = new LoginOptionPO(driver);
    private FooterPO footer = new FooterPO(driver);
    private ExistingLoginConfirmationPO facebookLoginConfirmation = new ExistingLoginConfirmationPO(driver);
    private FacebookLoginPO facebookLogin = new FacebookLoginPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private LoginPO login = new LoginPO(driver);

    //Test Data
    private String bookingDifferentGender = "src/test/resources/testdata/mamikos/booking.properties";
    private String serverKayPay = Constants.SERVER_KEY + "_" + Constants.SERVER_PAY;
    private String bookingFemaleFacebookEmail = JavaHelpers.getPropertyValueForSquad(bookingDifferentGender, "tenant_facebook_email_gender_female");
    private String bookingFemaleFacebookPassword = JavaHelpers.getPropertyValueForSquad(bookingDifferentGender, "tenant_facebook_password_gender_female");
    private String priceAndRenDuration = JavaHelpers.getPropertyValue(bookingDifferentGender, "price_and_rent_duration_" + serverKayPay);
    private String priceAndRenDurationPassword = JavaHelpers.getPropertyValue(bookingDifferentGender, "price_and_rent_duration_password_" + serverKayPay);
    private String tenantWaitingForBookingApprovalEmail = JavaHelpers.getPropertyValue(bookingDifferentGender, "tenant_waiting_for_approval_email_" + serverKayPay);
    private String tenantWaitingForBookingApprovalPassword = JavaHelpers.getPropertyValue(bookingDifferentGender, "tenant_waiting_for_approval_password_" + serverKayPay);

    //Test Data Payment
    private String paymentProperties = "src/test/resources/testdata/mamikos/payment.properties";
    private String emailTenantDoesNotHaveKos_ = JavaHelpers.getPropertyValueForSquad(paymentProperties, "emailTenantDoesNotHaveKos");
    private String passwordTenantDoesNotHaveKos_ = JavaHelpers.getPropertyValueForSquad(paymentProperties, "passwordTenantDoesNotHaveKos");
    private String emailTenant = JavaHelpers.getPropertyValueForSquad(paymentProperties, "emailTenant");
    private String passwordTenant = JavaHelpers.getPropertyValueForSquad(paymentProperties, "passwordTenant");

    //Test Data Loyalty
    //Voucherku
    private String voucherkuProperties = "src/test/resources/testdata/mamikos/voucherku.properties";
    private String tenantFacebookVoucherEmail = JavaHelpers.getPropertyValueForSquad(voucherkuProperties, "tenantFacebookVoucherEmail");
    private String tenantFacebookVoucherPassword = JavaHelpers.getPropertyValueForSquad(voucherkuProperties, "tenantFacebookVoucherPassword");
    private String tenantFacebookNonVoucherEmail = JavaHelpers.getPropertyValueForSquad(voucherkuProperties, "tenantFacebookNonVoucherEmail");
    private String tenantFacebookNonVoucherPassword = JavaHelpers.getPropertyValueForSquad(voucherkuProperties, "tenantFacebookNonVoucherPassword");
    private String tenantPhoneNumberNonVoucher = JavaHelpers.getPropertyValueForSquad(voucherkuProperties,"tenantPhoneNumberNonVoucher");
    private String tenantPhonePasswordNonVoucher = JavaHelpers.getPropertyValueForSquad(voucherkuProperties,"tenantPhonePasswordNonVoucher");
    private String tenantTargetedVoucherPhoneNumber = JavaHelpers.getPropertyValueForSquad(voucherkuProperties, "tenantTargetedVoucherPhoneNumber");
    private String tenantTargetedVoucherPhonePassword = JavaHelpers.getPropertyValueForSquad(voucherkuProperties, "tenantTargetedVoucherPhonePassword");

    //Add Voucher
    private String addVoucherProperties = "src/test/resources/testdata/mamikos/addVoucher.properties";
    private String tenantForAddVoucherNumber = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"tenantForAddVoucherNumber");
    private String tenantForAddVoucherPassword = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"tenantForAddVoucherPassword");

    //MamiPoin
    private String mamiPoinTenantProperties = "src/test/resources/testdata/mamikos/mamiPoinTenant.properties";
    private String mamiPoinTenantPhoneNumber = JavaHelpers.getPropertyValueForSquad(mamiPoinTenantProperties,"tenantPhoneNumberMamiPoin");
    private String mamiPoinTenantPassword = JavaHelpers.getPropertyValueForSquad(mamiPoinTenantProperties,"tenantPhonePasswordMamiPoin");
    private String emptyMamiPoinTenantPhoneNumber = JavaHelpers.getPropertyValueForSquad(mamiPoinTenantProperties,"tenantPhoneNumberemptyMamiPoin");
    private String emptyMamiPoinTenantPassword = JavaHelpers.getPropertyValueForSquad(mamiPoinTenantProperties,"tenantPhonePasswordEmptyMamiPoin");

    //Tenant Data Booking Growth
    private String bookingGrowthProperties = "src/test/resources/testdata/mamikos/bookingGrowth.properties";
    private String BGFemaleFacebookEmail = JavaHelpers.getPropertyValueForSquad(bookingGrowthProperties, "tenant_facebook_email_gender_female");
    private String BGFemaleFacebookPassword = JavaHelpers.getPropertyValueForSquad(bookingGrowthProperties, "tenant_facebook_password_gender_female");
    private String tenantPhoneNumber = JavaHelpers.getPropertyValueForSquad(bookingGrowthProperties, "tenant_Phone_Number");
    private String tenantPhonePassword = JavaHelpers.getPropertyValueForSquad(bookingGrowthProperties, "tenant_Phone_Password");

    //Tenant Data User Growth
    private String editProfileProperties = "src/test/resources/testdata/mamikos/editprofile.properties";
    private String UGTenantPhoneNumber = JavaHelpers.getPropertyValue(editProfileProperties, "TenantPhoneNumber_" + serverKayPay);
    private String UGTenantPassword = JavaHelpers.getPropertyValue(editProfileProperties, "TenantPassword_" + serverKayPay);
    private String UGEditTenantPhoneNumber = JavaHelpers.getPropertyValue(editProfileProperties, "TenantEditPhoneNumber_" + serverKayPay);
    private String UGEditTenantPassword = JavaHelpers.getPropertyValue(editProfileProperties, "TenantEditPassword_" + serverKayPay);

    //Data Marketing Technology Squad
    private String martechProperties = "src/test/resources/testdata/mamikos/marketingTechnology/martech.properties";
    private String firstTenantMartech = JavaHelpers.getPropertyValue(martechProperties, "firstFBAccount_" + serverKayPay);
    private String firstPasswordMartech = JavaHelpers.getPropertyValue(martechProperties, "firstPasswordAccount_" + serverKayPay);

    @And("user clicks on Tanent option")
    public void user_clicks_on_Tanent_option() throws InterruptedException {
        loginOption.clickOnTenantButton();
        loginOption.clickOnContinueToFacebookButton();
    }

    @And("user logs in as Tenant via Facebook credentails {string}")
    public void user_logs_in_as_Tenant_via_Facebook_credentails(String tenant) throws InterruptedException {
        footer.clickOnLoginMenu();
        loginOption.clickOnTenantButton();
        loginOption.clickOnContinueToFacebookButton();
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.setContext("WEBVIEW_chrome");
            appium.hardWait(5);
            Cookie loginCookie = driver.manage().getCookieNamed("c_user");
            if (loginCookie != null) {
                driver.manage().deleteAllCookies();
            }
        }
        String email = "";
        String password = "";
        if (tenant.equalsIgnoreCase("master")) {
            email = Constants.TenantFacebookEmail;
            password = Constants.TenantFacebookPassword;
        } else if (tenant.equalsIgnoreCase("gender female")) {
            email = bookingFemaleFacebookEmail;
            password = bookingFemaleFacebookPassword;
        } else if (tenant.equals("TenantDoesNotHaveKos")) {
            email = emailTenantDoesNotHaveKos_;
            password = passwordTenantDoesNotHaveKos_;
        } else if (tenant.equals("tenantPayment")) {
            email = emailTenant;
            password = passwordTenant;
        } else if (tenant.equalsIgnoreCase("price and rent duration")) {
            email = priceAndRenDuration;
            password = priceAndRenDurationPassword;
        } else if (tenant.equalsIgnoreCase("tenant waiting for booking approval")) {
            email = tenantWaitingForBookingApprovalEmail;
            password = tenantWaitingForBookingApprovalPassword;
        } else if (tenant.equalsIgnoreCase("non voucher")) {
            email = tenantFacebookNonVoucherEmail;
            password = tenantFacebookNonVoucherPassword;
        } else if (tenant.equalsIgnoreCase("voucher")) {
            email = tenantFacebookVoucherEmail;
            password = tenantFacebookVoucherPassword;
        } else if (tenant.equalsIgnoreCase("tenant female")) {
            email = BGFemaleFacebookEmail;
            password = BGFemaleFacebookPassword;
        } else if (tenant.equals("firstMartechAccount")) {
            email = firstTenantMartech;
            password = firstPasswordMartech;
        }

        try {
            appium.hardWait(3);
            facebookLoginConfirmation.clickOnContinueButton();
            appium.hardWait(3);
            appium.back();
        } catch (Exception e) {
            //nothing
        }

        System.out.print("Ini usernya "+ firstTenantMartech);

        facebookLogin.fillOutFormAndClickOnLoginButton(email, password);
        loading.waitLoadingFinish();
        appium.hardWait(5);
        facebookLoginConfirmation.clickOnContinueButton();
        popUp.closePopUpWhereAreYouKnowMamikos();

        appium.setContext("NATIVE_APP");
        loading.waitLoadingFinish();
    }

    @And("user log in as Tenant via phone number as {string}")
    public void user_log_in_as_tenant_via_phone_number_as(String type) throws InterruptedException {
        footer.clickOnLoginMenu();
        loginOption.clickOnTenantButton();

        String number = "";
        String password = "";

        if (type.equalsIgnoreCase("DC Automation")) {
            number = tenantPhoneNumber;
            password = tenantPhonePassword;
        } else if (type.equalsIgnoreCase("UG Edit Profile")) {
            number = UGTenantPhoneNumber;
            password = UGTenantPassword;
        } else if(type.equalsIgnoreCase("non voucher")){
            number = tenantPhoneNumberNonVoucher;
            password = tenantPhonePasswordNonVoucher;
        } else if(type.equalsIgnoreCase("UG Edit Tenant")){
            number = UGEditTenantPhoneNumber;
            password = UGEditTenantPassword;
        } else if (type.equalsIgnoreCase("voucher targeted")) {
            number = tenantTargetedVoucherPhoneNumber;
            password = tenantTargetedVoucherPhonePassword;
        } else if(type.equalsIgnoreCase("MamiPoin Tenant")){
            number = mamiPoinTenantPhoneNumber;
            password = mamiPoinTenantPassword;
        } else if(type.equalsIgnoreCase("Empty MamiPoin Tenant")){
            number = emptyMamiPoinTenantPhoneNumber;
            password = emptyMamiPoinTenantPassword;
        } else if(type.equalsIgnoreCase("add voucher")){
            number = tenantForAddVoucherNumber;
            password = tenantForAddVoucherPassword;
        }
        login.loginAsTenantToApplication(number, password);
    }

    @And("user click on tenant forgot password button from login page")
    public void user_click_on_tenant_forgot_password_button_from_login_page() {
        loginOption.clickOnTenantButton();
        loginOption.clickOnForgotPasswordLink();
    }

    @When("tenant login with phone number {string}, password {string} and clear text is {string}")
    public void tenant_login_with_phone_number_and_password(String phoneNumber, String password, String clearText) throws InterruptedException {
        boolean clear = clearText.equalsIgnoreCase("true");
        login.loginAsTenantToApplication(phoneNumber, password, clear);
    }

    @Then("tenant/owner can see invalid login with message is {string}")
    public void tenant_can_see_invalid_login_with_message_is(String invalidLoginMessage) {
        Assert.assertEquals(login.getInvalidLoginMessageText(), invalidLoginMessage);
    }

    @When("{string} go to login form")
    public void tenant_go_to_login_form(String user) {
        footer.clickOnLoginMenu();
        user_click_login_as(user);
    }

    @When("user click login as {string}")
    public void user_click_login_as(String user){
        switch (user.toLowerCase()) {
            case "owner":
                loginOption.clickOnAdOwnerButton();
                break;
            case "tenant":
                loginOption.clickOnTenantButton();
                break;
            default:
                break;
        }
    }
    @And("user click login by facebook email {string}, password {string} and clear text is {string}")
    public void user_click_login_by_facebook(String email, String password, String clearText) throws Exception {
        login.clickOnFacebookButton();
        if (Constants.MOBILE_OS == Platform.ANDROID){
            boolean clear = clearText.equalsIgnoreCase("true");
            login.loginAsTenantWithFacebook(email, password, clear);
        }
        else {
            appium.hardWait(7);
            facebookLogin.fillOutFormAndClickOnLoginButton(email, password);
            facebookLoginConfirmation.clickOnContinueButton();
        }
    }
    @When("user clear data FB")
    public void user_clear_data_FB() throws Exception {
        login.clearDataFb();
    }

    @And("user clear cookies web view ios")
    public void user_clear_cookies_web_view_ios() throws Exception {
        login.clickOnFacebookButton();
        Set contextName = driver.getContextHandles();
        System.out.println(contextName);
        appium.hardWait(4);
        Set<String> contexts = driver.getContextHandles();
        for (String context : contexts) {
            System.out.println(contexts);
            driver.context((String) contexts.toArray()[1]);
            appium.hardWait(3);
            driver.manage().deleteAllCookies();
            if (!context.equals("NATIVE_APP")) {
                driver.manage().deleteAllCookies();
                break;
            }
        }
    }

    @When("user clear cookies web view android")
    public void user_clear_webview_android() throws Exception {
        login.clearWebviewAndroid();
    }
}