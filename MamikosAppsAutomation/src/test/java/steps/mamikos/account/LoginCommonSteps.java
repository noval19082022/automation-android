package steps.mamikos.account;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.login.LoginPO;
import utilities.ThreadManager;

public class LoginCommonSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private LoginPO login = new LoginPO(driver);


	@And("click on mami help button")
	public void clickOnMamiHelpButton() {
		login.clickOnMamiHelpButton();
	}

	@Then("verify it should redirect to login page")
	public void verifyItShouldRedirectToLoginPage() {
		Assert.assertTrue(login.isLoginButtonDisplayed(), "Login button is not displayed");
	}

	@And("verify login page should be shown")
	public void verifyLoginPageShouldBeShown() {
		Assert.assertTrue(login.isLoginPageTitleDisplayed(), "Login page title is not shown ");
	}
}
