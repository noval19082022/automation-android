package steps.mamikos.account;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.profile.ProfilePO;
import pageobjects.mamikos.common.profile.SettingsPO;
import utilities.Constants;
import utilities.ThreadManager;

public class LogoutCommonSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	
	private ProfilePO profile = new ProfilePO(driver);
	private SettingsPO settings = new SettingsPO(driver);
	private PopUpPO popUp = new PopUpPO(driver);
	private LoadingPO loading = new LoadingPO(driver);

	@And("user logs out as a Tenant/Owner user")
	public void user_logs_out_as_a_X_user() throws InterruptedException {
		profile.clickOnSettingsOption();
		loading.waitLoadingFinish();
		settings.clickOnLogoutButton();
		if(Constants.MOBILE_OS== Platform.ANDROID) {
			popUp.clickOnExitButton();
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
		}
	}

	@When("user log out")
	public void user_log_out_as_tenant_user() throws InterruptedException {
		profile.clickOnSettingsOption();
		settings.clickOnLogoutButton();
		if(Constants.MOBILE_OS== Platform.ANDROID) {
			popUp.clickOnLogoutButton();
			try
			{
				popUp.clickOnLaterButton();
			}
			catch (Exception e)
			{
				//Nothing
			}
		}
	}
}
