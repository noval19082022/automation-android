package steps.mamikos.account;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.login.LoginOptionPO;
import pageobjects.mamikos.common.register.RegisterPO;
import pageobjects.mamikos.common.register.RegistrationFormPO;
import pageobjects.mamikos.tenant.explore.ExplorePO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class RegisterAsAOwnerSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private RegisterPO register = new RegisterPO(driver);
    private RegistrationFormPO registrationForm = new RegistrationFormPO(driver);
    private LoginOptionPO loginOption = new LoginOptionPO(driver);
    private ExplorePO explore = new ExplorePO(driver);

    //Test Data
    private String propertyFile="src/test/resources/testdata/mamikos/register.properties";
    private String tenetMobileNumber = JavaHelpers.getPropertyValue(propertyFile,"tenetMobileNumber");
    private String ownerMobileNumber = JavaHelpers.getPropertyValue(propertyFile,"ownerMobileNumber");


    @And("user click on Daftar kos gratis link text to navigate Registration Page")
    public void userClickOnDaftarKosGratisLinkTextToNavigateRegistrationPage() {
        explore.clickOnRegisterFreeKosText();
    }

    @Then("Owner Registration page is displayed with heading {string}")
    public void ownerRegistrationPageIsDisplayedWithHeading(String pageHeading) {
        Assert.assertEquals(registrationForm.getPageHeaderText(), pageHeading, "Page header doesn't match");
    }

    @And("user click on Register Account button")
    public void userClickOnRegisterAccountButton() {
        loginOption.clickOnAdOwnerButton();
        register.clickOnRegisterAccountButton();

    }

    @And("user enter data as owner name {string}, owner mobile number, owner Email {string}, password {string} in registration form and click on Register Button")
    public void userEnterDataAsOwnerNameOwnerMobileNumberOwnerEmailPasswordInRegistrationFormAndClickOnRegisterButton(String ownerName, String ownerEmail, String passWord) {
        registrationForm.fillOwnerDetails(ownerName, tenetMobileNumber, ownerEmail, passWord);
        registrationForm.clickOnRegisterButton();
    }

    @And("user enter data as owner name {string}, Registered Mobile Number, owner Email {string}, password {string} in registration form and click on Register Button")
    public void userEnterDataAsOwnerNameRegisteredMobileNumberOwnerEmailPasswordInRegistrationFormAndClickOnRegisterButton(String ownerName, String ownerEmail, String passWord) {
        registrationForm.fillOwnerDetails(ownerName, ownerMobileNumber, ownerEmail, passWord);
        registrationForm.clickOnRegisterButton();
    }

    @And("user enter data as owner name {string} mobile number {string}, owner Email {string}, password {string} in registration form and click on Register Button")
    public void user_enter_data_as_owner_name(String name, String mobileNumber, String email, String password) {
        registrationForm.fillOwnerDetails(name, mobileNumber, email, password);
        registrationForm.clickOnRegisterButton();
    }

    @Then("user enter data as owner name {string} mobile number {string}, owner Email {string}, password {string} in registration form and click on Register Button Case 2")
    public void user_enter_data_as_owner_name_case_2(String name, String mobileNumber, String email, String password) {
        registrationForm.fillOwnerDetails(name, mobileNumber, email, password);
        Assert.assertFalse(registrationForm.isRegisterButtonDisabled(), "Register button still enabled");
    }

    @And("user enter data as owner name {string} mobile number {string}, owner Email {string}, in registration form")
    public void user_enter_data_as_owner_name_mobile_number_owner_email_in_registration_form(String ownerName, String ownerMobile, String ownerEmail) {
            registrationForm.fillFormWithoutPassword(ownerName, ownerMobile, ownerEmail);
    }

    @Then("validate the Register button is disable")
    public void validate_the_Register_button_is_disable() {
            Assert.assertFalse(registrationForm.isRegisterButtonDisabled(), "Register Button is not displayed");
    }

    @And("user enter OTP {string} {string} {string} {string} and click on verify button")
    public void user_enter_OTP_and_click_on_verify_button(String code1, String code2, String code3, String code4) throws InterruptedException {
            registrationForm.enterOTP(code1, code2, code3, code4);
    }

    @And("user enter data as owner name {string}, owner Email {string}, password {string} in registration form and Click on eye icon to show password")
    public void user_enter_data_as_owner_name_owner_email_password_in_registration_form_and_click_on_eye_icon_to_show_password(String ownerName, String ownerEmail, String passWord) {
            registrationForm.fillOwnerDetails(ownerName, ownerMobileNumber, ownerEmail, passWord);
            registrationForm.clickOnEyeIcon();
    }

    @And("verify enter password {string} is correct")
    public void verify_enter_password_is_correct(String passwordText) {
            Assert.assertEquals(registrationForm.getPasswordText(), passwordText, "Password doesn't match");
    }

    @Then("user verify error messages {string}")
    public void user_verify_error_messages(String errorMessage) {
        Assert.assertEquals(registrationForm.getErrorMessages(), errorMessage, "Error Message it not equal to " + errorMessage);
    }

    @Then("user verify input verification code page")
    public void user_verify_input_verification_code_page() {
        Assert.assertTrue(registrationForm.isCodeVerificationContainerPresent(), "Code Verification boxes not present");
        Assert.assertTrue(registrationForm.isEachOfOtpTextBoxPresent(), "Each of verification code text boxes is not present");
    }

    @When("user click resend verification code")
    public void user_click_resend_verification_code() {
        Assert.assertTrue(registrationForm.isResendVerificationCodeCountdownPresent(), "Resend verification code countdown is not present");
        Assert.assertTrue(registrationForm.isRequestCodeButtonPresent(), "Request code button is not present");
        registrationForm.clickOnRequestCodeButton();
    }

    @Then("user verify resend verification code countdown")
    public void user_verify_resend_verification_code_countdown() {
        Assert.assertTrue(registrationForm.isResendVerificationCodeCountdownPresent(), "Resend verification code countdown is not present");
    }

    @And("user enter data as owner name {string} mobile number {string}, owner Email {string}, password {string} in registration form")
    public void user_enter_data_as_owner_name_mobile_number_owner_email_password(String ownerName, String phoneNumber, String email, String password) {
        registrationForm.fillOwnerDetails(ownerName, phoneNumber, email, password);
    }
}
