package steps.mamikos.apartmentads;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.owner.apartmentads.AddApartmentsAdsPO;
import pageobjects.mamikos.owner.apartmentads.ApartemenAdsPO;
import utilities.ThreadManager;

import java.util.List;

public class ApartmentAdsSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private AddApartmentsAdsPO apart = new AddApartmentsAdsPO(driver);
    private ApartemenAdsPO myApart = new ApartemenAdsPO(driver);
    private LoadingPO loading = new LoadingPO(driver);

    @When("user click on Apartment Tab")
    public void userClickOnApartmentTab() throws InterruptedException {
        apart.clickOnApartmentTab();
        loading.waitLoadingFinish();
    }

    @And("user click on As An Owner")
    public void userClickOnAsAnOwner() {
        apart.clickOnAsAnOwnerButton();
    }

    @And("user input their Apartment Name {string}, Unit Name {string}, " +
            "Unit Number {string}, Room Type {string}, Unit Floor {string}, " +
            "Unit Area {string}, Daily Price {string}, Weekly Price {string}, " +
            "Monthly Price {string}, Yearly Price {string}, Description {string}, " +
            "Minimal Rent {string}, Apartment Maintenance Fee {string} " +
            "and Apartment Parking Fee {string}")
    public void userFillOutApartmentForm(String apartmentName, String unitName, String unitNumber, String roomType,
                                         String unitFloor, String unitArea, String dailyPrice, String weeklyPrice,
                                         String monthlyPrice, String yearlyPrice, String description, String minRent,
                                         String maintenanceApart, String parkingApart) throws InterruptedException {

        apart.fillOutApartmentInfoForm(apartmentName, unitName, unitNumber, roomType,
                                        unitFloor, unitArea, dailyPrice, weeklyPrice, monthlyPrice,
                                        yearlyPrice, description, minRent, maintenanceApart, parkingApart);
        loading.waitLoadingFinish();
    }

    @Then("user validate Apartment Title should be {string}, Apartment Price should be {string}, Apartment Name should be {string}, Apartment Size should be {string}, Apartement Furnishing should be {string}, Apartment Bed should be {string}, Apartment Bath should be {string}, Apartment Floor should be {string}")
    public void userValidateApartmentData(String apartmentTitle, String apartmentPrice, String apartmentName, String apartmentSize, String apartmentFurnish, String apartmentBed, String apartmentBath, String apartmentFloor) {
        Assert.assertEquals(apart.getApartmentTitle(), apartmentTitle, "Apartment Title is not equal to " + apartmentTitle);
        Assert.assertEquals(apart.getApartmentPrice(), apartmentPrice, "Apartment Price is not equal to " + apartmentPrice);
        Assert.assertEquals(apart.getApartmentName(), apartmentName, "Apartment Name is not equal to " + apartmentName);
        Assert.assertEquals(apart.getApartmentSize(), apartmentSize, "Apartment Size is not equal to " + apartmentSize);
        Assert.assertEquals(apart.getApartmentFurnishing(), apartmentFurnish, "Apartment Furnishing is not equal to " + apartmentFurnish);
        Assert.assertEquals(apart.getApartmentBed(), apartmentBed, "Apartment Bed is not equal to " + apartmentBed);
        Assert.assertEquals(apart.getApartmentBath(), apartmentBath, "Apartment Bath is not equal to " + apartmentBath);
        Assert.assertEquals(apart.getApartmentFloor(), apartmentFloor, "Apartment Floor is not equal to " + apartmentFloor);
    }

    @And("user clicks on growth click button")
    public void user_clicks_on_growth_click_button() {
        myApart.clickOnGrowUpApartemenButton();
    }

    @And("user clicks on statistic time range")
    public void user_clicks_on_statistic_time_range() {
        myApart.chooseTimeRangeStatictic();
    }

    @Then("will display statistic by time")
    public void will_display_statistic_by_time(List<String> timeRange) {
        for ( int i = 0; i < timeRange.size() ; i++){
            Assert.assertTrue(myApart.isTimeRangeTextPresent(timeRange.get(i)), timeRange.get(i) + " is not present");
        }
    }

        @Then("user will see the allocation balance is equals with {string}")
    public void user_will_see_the_allocation_balance_is_equals_with(String saldo) throws InterruptedException {
        boolean balance = myApart.getAllocationBalance().contains(saldo);
        Assert.assertTrue(balance, "Amount balance is not equals with amount inputed!");
    }
}
