package steps.mamikos.backoffice;

import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageobjects.backoffice.LeftMenuPO;
import pageobjects.backoffice.LoginPO;
import pageobjects.backoffice.SearchContractPO;
import utilities.Constants;
import utilities.JavaHelpers;

import java.util.concurrent.TimeUnit;

import static utilities.Constants.MAMIKOSADMIN_URL;

public class BackofficeSteps {

    //Test Data Payment
    private String paymentProperties ="src/test/resources/testdata/mamikos/payment.properties";
    private String propertyName_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"propertyName");

    //Test Data addVoucher
    private String addVoucherProperties="src/test/resources/testdata/mamikos/addVoucher.properties";
    private String kostNameForAddVoucher1 = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"kostNameForAddVoucher1");
    private String kostNameForAddVoucher2 = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"kostNameForAddVoucher2");
    private String kostNameForAddVoucher3 = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"kostNameForAddVoucher3");

    @Given("User navigate to {string} and login via Admin credential after that Click on search contract menu from left bar, search for {string} and click on Cancel the Contract Button from action column")
    public void user_navigate_to_login_via_admin_credential_click_on_search_contract_menu_from_left_bar_and_search_for_click_on_cancel_the_contract_button_from_action_column(String url, String kostName) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1440x900");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--incognito");
        chromeOptions.addArguments("use-fake-ui-for-media-stream");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);

        LoginPO login = new LoginPO(driver);
        LeftMenuPO left = new LeftMenuPO(driver);
        SearchContractPO searchContract = new SearchContractPO(driver);

        login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
        left.clickOnSearchContractMenu();
        searchContract.enterTextToSearchTextbox(kostName);
        searchContract.clickOnSearchContractButton();
        if (searchContract.getBookingStatus().get(0).getText().equalsIgnoreCase("booked")) {
            searchContract.clickOnBookingCancelButton();
        }
        driver.quit();
    }

    @Given("User navigate to {string} and login via Admin credential after that Click on search contract menu from left bar, search for {string} and click on terminate the Contract Button from action column")
    public void user_navigate_to_login_via_admin_credential_click_on_search_contract_menu_from_left_bar_and_search_for_click_on_terminate_the_contract_button_from_action_column(String url, String kostName) throws InterruptedException {
        System.out.println(kostName);
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1440x900");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--incognito");
        chromeOptions.addArguments("use-fake-ui-for-media-stream");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);

        LoginPO login = new LoginPO(driver);
        LeftMenuPO left = new LeftMenuPO(driver);
        SearchContractPO searchContract = new SearchContractPO(driver);

        login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
        left.clickOnSearchContractMenu();
        searchContract.selectFilterSearchBy("Kost Name");
        searchContract.enterTextToSearchTextbox(kostName);
        searchContract.clickOnSearchContractButton();
        if (searchContract.getBookingStatus().get(0).getText().equalsIgnoreCase("booked")) {
            searchContract.terminateContractByTodayDate();
        } else if (searchContract.getBookingStatus().get(0).getText().equalsIgnoreCase("Active")) {
            searchContract.terminateContractByTodayDate();
        }
        driver.quit();
    }

    @Given("user navigate to mamipay, search contract {string} and click on terminate contract")
    public void user_navigate_to_mamipay_search_contract_and_click_on_terminate_contract(String kostName) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1440x900");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--incognito");
        chromeOptions.addArguments("use-fake-ui-for-media-stream");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Constants.BACKOFFICE_URL);

        LoginPO login = new LoginPO(driver);
        LeftMenuPO left = new LeftMenuPO(driver);
        SearchContractPO searchContract = new SearchContractPO(driver);

        login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
        left.clickOnSearchContractMenu();
        searchContract.selectFilterSearchBy("Kost Name");
        if (kostName.equals("payment")){
            searchContract.enterTextToSearchTextbox(propertyName_);
        }
        else if(kostName.equals("add voucher1")){
            searchContract.enterTextToSearchTextbox(kostNameForAddVoucher1);
        }
        else if(kostName.equals("add voucher2")){
            searchContract.enterTextToSearchTextbox(kostNameForAddVoucher2);
        }
        else if(kostName.equals("add voucher3")){
            searchContract.enterTextToSearchTextbox(kostNameForAddVoucher3);
        }
        searchContract.clickOnSearchContractButton();
        Thread.sleep(3000);
        String mainUrl = Constants.BACKOFFICE_URL.substring(0,28)+ "/backoffice/contract/terminate/";
        String contractID = searchContract.getIDContract();
        Thread.sleep(3000);
        driver.get(mainUrl + contractID);
        Thread.sleep(2000);
        driver.quit();
    }
    @Given("User navigate to {string} and login via Admin credential after click search menu identity card, search user {string}")
    public void user_navigate_to_login_via_admin_credential_and_search_menu_identity_card(String url, String profileName) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1440x900");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--incognito");
        chromeOptions.addArguments("use-fake-ui-for-media-stream");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(MAMIKOSADMIN_URL);

        LoginPO login = new LoginPO(driver);
        LeftMenuPO left = new LeftMenuPO(driver);
        SearchContractPO searchContract = new SearchContractPO(driver);

        login.enterCredentialsAndClickOnLoginButton(Constants.MAMIKOSADMIN_LOGIN_EMAIL, Constants.MAMIKOSADMIN_LOGIN_PASSWORD);
        driver.get(url);
        searchContract.enterTextToVerificationAccount(profileName);
        searchContract.clickOnSearchIconButton();
        if (searchContract.getVerificationStatus().get(0).getText().equalsIgnoreCase("Rejected")) {
            driver.quit();
        } else if (searchContract.getVerificationStatus().get(0).getText().equalsIgnoreCase("Waiting")) {
            searchContract.clickOnRejectButton();
            searchContract.clickOnRejectReasonDropdown();
            searchContract.clickOnSubmitButton();
            driver.quit();
        }
    }
}

