package steps.mamikos.booking;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.tenant.booking.BookingSummaryPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;

import static org.testng.Assert.*;

public class BookingSummarySteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private JavaHelpers javaHelpers = new JavaHelpers();
    private BookingSummaryPO bookingSummary = new BookingSummaryPO(driver);
    LocalDate currentDate = LocalDate.now();

    @Then("tenant can sees booking summary displayed with kost name, kost gender, room price, check in date, check out date, rent duration")
    public void tenant_can_sees_booking_summary_displayed_with() throws ParseException, InterruptedException {
        Locale indonesian = new Locale("id", "ID");
        String pattern = "dd MMM yyyy";
        String tommorrowDate = javaHelpers.updateTime("yyyy MMM dd", javaHelpers.getTimeStamp("yyyy MMM dd"), "dd MMMM yyyy", 1, 0, 0, 0);
        Date formatter = new SimpleDateFormat(pattern).parse(tommorrowDate);
        String checkInFullDate = javaHelpers.changeLocalDate(formatter, pattern, indonesian);

        String checkOutFullDate = currentDate.plusMonths(4).plusDays(1).toString();
        String[] dateArray = checkOutFullDate.split("-");
        int dayDate = Integer.parseInt(dateArray[2]);
        int monthDate = Integer.parseInt(dateArray[1]);

        String month = Month.of(monthDate).getDisplayName(TextStyle.SHORT, indonesian);
        checkOutFullDate = Integer.toString(dayDate) + " " + month + " " + dateArray[0];

        assertTrue(bookingSummary.isKostNamePresent(), "Kost name is not present");
        assertTrue(bookingSummary.isKostGenderPresent(), "Kost Gender/Type is not present");
        assertTrue(bookingSummary.isKostPricePresent(), "Kost price is not present");
        assertEquals(bookingSummary.getCheckInDateText(), checkInFullDate, "Check in date is not equal to tomorrow date");
        assertEquals(bookingSummary.getCheckOutDateText(), checkOutFullDate, "Check out date is not equal to 4 month ahead");
        assertTrue(bookingSummary.isRentDurationPresent(), "Rent duration is not present");
        assertEquals(bookingSummary.getRentDuration(), "4 Bulan", "Rent duration not equal");
        assertTrue(bookingSummary.isTenantNamePresent(), "Tenant name is not present");
    }
}
