package steps.mamikos.booking;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.sl.In;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.booking.BookingConfirmContractPO;
import pageobjects.mamikos.owner.booking.BookingConfirmRoomPreferencePO;
import pageobjects.mamikos.owner.booking.BookingDetailsPO;
import pageobjects.mamikos.owner.booking.RoomNumberPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class OwnerBookingDetailSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private BookingDetailsPO bookingDetails = new BookingDetailsPO(driver);
	//private JavaHelpers java = new JavaHelpers();
	private BookingConfirmRoomPreferencePO roomPref = new BookingConfirmRoomPreferencePO(driver);
	private BookingConfirmContractPO contract = new BookingConfirmContractPO(driver);
	private BookingConfirmContractPO manageBooking = new BookingConfirmContractPO(driver);
	private PopUpPO popUp = new PopUpPO(driver);
	private AppiumHelpers appium = new AppiumHelpers(driver);
	private RoomNumberPO roomNumber = new RoomNumberPO(driver);

	//Test Data
	private String propertyFile1="src/test/resources/testdata/mamikos/booking.properties";
	private String koseName = JavaHelpers.getPropertyValueForSquad(propertyFile1,"kostNameAssert");
	private String kosePrice = JavaHelpers.getPropertyValueForSquad(propertyFile1,"kosePrice");
	private String duration = JavaHelpers.getPropertyValueForSquad(propertyFile1,"duration");
	private String status = JavaHelpers.getPropertyValue(propertyFile1,"status");
	private String phoneNumberOwnerSide = JavaHelpers.getPropertyValueForSquad(propertyFile1,"phoneNumberOwnerSide");

	//Test Data Payment
	private String payment="src/test/resources/testdata/mamikos/payment.properties";
	private String nameTenant_ = JavaHelpers.getPropertyValueForSquad(payment,"nameTenant");
	private String depositFee_ = JavaHelpers.getPropertyValueForSquad(payment,"depositFee");
	private String penaltyFee_ = JavaHelpers.getPropertyValueForSquad(payment,"penaltyFee");
	private String additionalCosts_ = JavaHelpers.getPropertyValueForSquad(payment,"additionalCosts");
	private String additionalCostsLabel_ = JavaHelpers.getPropertyValueForSquad(payment,"additionalCostsLabel");
	private String downPayment_ = JavaHelpers.getPropertyValueForSquad(payment,"downPayment");

	//Test Data Add Voucher
	private String addVoucher="src/test/resources/testdata/mamikos/addVoucher.properties";
	private String downPaymentForApplyVoucher = JavaHelpers.getPropertyValue(addVoucher,"downPaymentForApplyVoucher");

	@Then("booking is displayed with Booking ID starting with {string} , Room name, Room price, Order Date as today date, Booking Date as today date , Duration, Exit Date as today date ,  Name, Phone, Status on Booking details page")
	public void booking_is_displayed_with_correct_details_on_Booking_details_page(String bookingIDAlias) throws InterruptedException
	{
		appium.hardWait(5);
		do {
			appium.scrollUp(1, 5);
		}
		while (!bookingDetails.isBookingStatusPresent());
		Assert.assertEquals(bookingDetails.getRoomName(), koseName,"Room name doesn't match");
		Assert.assertEquals(bookingDetails.getRoomPrice(), "Rp" + kosePrice + "/Bulan","Room price doesn't match");
		
		//Need to work with Dev Team to get Ids for locators
		//Assert.assertTrue(bookingDetails.getOrderDate().startsWith(java.getTimeStamp("dd MMM yyyy")),"Order Date doesn't match");
		//Assert.assertEquals(bookingDetails.getBookingDate(), java.getTimeStamp("dd MMM yyyy"),"Booking Date doesn't match");
		//Assert.assertEquals(bookingDetails.getBookingExitDate(), java.getTimeStamp("dd MMM yyyy"),"Exit Date doesn't match");
		//Assert.assertEquals(bookingDetails.getBookingDuration(), duration,"Duration doesn't match");
		
		Assert.assertEquals(bookingDetails.getTanentName(), Constants.TenantFacebookName,"Tenant name doesn't match");
		Assert.assertEquals(bookingDetails.getTanentPhone(), phoneNumberOwnerSide,"Tenant phone doesn't match");
		Assert.assertEquals(bookingDetails.getStatusName(), status,"Status doesn't match");
	}
	

	@When("user clicks on Accept button")
	public void user_clicks_on_Accept_button() throws InterruptedException {
		bookingDetails.clickOnAcceptButton();
	}

	@Then("user sees a confirmation message with correct text")
	public void user_sees_a_confirmation_message_with_correct_text() throws InterruptedException {
		bookingDetails.clickOnNextButton();
	}
	
	@When("user clicks on Next button")
	public void user_clicks_on_Next_button() throws InterruptedException 
	{
		bookingDetails.clickOnNextButton();
	}

	@When("user select room number {string} and clicks on next button")
	public void user_select_room_number_and_clicks_on_next_button(String roomNumber) throws InterruptedException {
		roomPref.selectRoom(roomNumber);
		roomPref.clickOnNextButton();
	}

	@When("user enters deposit fee, penalty fee, additional costs, percentage down payment percent, and click on save button")
	public void user_enters_deposit_fee_penalty_fee_additional_costs_percentage_down_payment_percent_and_click_on_save_button() throws InterruptedException {
		contract.enterDepositFee(depositFee_);
		contract.enterPenaltyFee(penaltyFee_);
		contract.enterAdditionalCost(additionalCostsLabel_, additionalCosts_);
		contract.enterDownPayment(downPayment_);
		manageBooking.clickOnSaveTenantData();
	}

	@When("user enters deposit fee, additional cost and click on save button")
	public void user_enters_deposit_fee_additional_cost_and_click_on_save_button() {
		contract.enterDepositFee(depositFee_);
		contract.enterAdditionalCost(additionalCostsLabel_, additionalCosts_);
		manageBooking.clickOnSaveTenantData();
	}

	@When("owner choose first available room and choose next button")
	public void user_enters_room_number_and_clicks_on_Next_button() throws InterruptedException {
		roomPref.clickOnRoomNumberDropdown();
		roomNumber.clickOnFirstAvailableRoom();
		roomNumber.clickOnApplyButton();
		roomNumber.clickOnNextButton();
	}
	
	@And("user enters deposite panalty info as deposite fee {string} , panalty fee {string} , panalty fee duration {string} , panalty fee duration type {string} and click on Save")
	public void user_enters_deposite_panalty_info_as_deposite_fee_panalty_fee_panalty_fee_duration_panalty_fee_duration_type_and_click_on_Save
				(String depositeFeeText, String panaltyFeeText, String penaltyFeeDurationText, String penaltyFeeDurationTypeText) throws InterruptedException 
	{
		contract.enterDepositePanaltyInfo(depositeFeeText, panaltyFeeText, penaltyFeeDurationText, penaltyFeeDurationTypeText);
		popUp.dismissOwnerPopup();
	}

	@When("user click save tenant data")
	public void user_click_save_tenant_data() {
		manageBooking.clickOnSaveTenantData();
	}

	@And("user enter down payment")
	public void user_enter_down_payment() throws InterruptedException {
		contract.enterDownPayment(downPaymentForApplyVoucher);
	}

	@When("owner taps on save button")
	public void owner_taps_on_save_button() throws InterruptedException {
		contract.clickOnSaveButton();
		popUp.dismissOwnerPopup();
	}

	@When("owner taps on Pengajuan Booking")
	public void owner_taps_on_pengajuan_booking() {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			appium.scrollToElementContainsByText("Pengajuan Booking");
			appium.clickElementByText("Pengajuan Booking");
		}else{
			manageBooking.clickOnPengajuanBookingTitle();
		}
	}

	@When("owner taps on Laporan Keuangan")
	public void owner_taps_on_laporan_keuangan() throws InterruptedException {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			appium.clickElementByText("Laporan Keuangan");
		}else{
			manageBooking.clickOnLaporanKeuanganTitle();
			manageBooking.clickOnPopUpLaporanKeuangan();
		}
	}

	@When("owner taps on Kelola Tagihan")
	public void owner_taps_on_kelola_tagihan() {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			appium.scrollToElementContainsByText("Kelola Tagihan");
			appium.clickElementByText("Kelola Tagihan");
		}else{
			manageBooking.clickOnKelolaTagihanTitle();
		}
	}

	@When("owner taps on Penyewa")
	public void owner_taps_on_penyewa() throws InterruptedException {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			appium.scrollToElementContainsByText("Penyewa");
			appium.scroll(0.5, 0.8, 0.4, 2000);
			appium.hardWait(3);
			appium.clickElementByText("Penyewa");
		}else{
			manageBooking.clickOnPenyewaTitle();
		}
	}

	@Then("system display page Detail Booking")
	public void system_display_page_detail_booking() {
		Assert.assertTrue(manageBooking.detailBookingPageIsPresent(), "Detail Booking page is not present");
	}

	@When("user clicks on Reject button")
	public void user_clicks_on_reject_button() throws InterruptedException{
		bookingDetails.clickOnRejectButton();
	}

	@And("owner choose {string} on reason to reject booking")
	public void owner_choose_on_reason_to_reject_booking(String reason) throws InterruptedException {
		bookingDetails.clickOnReasonRejectBooking(reason);
	}

	@And("owner click TnC button")
	public void owner_click_TnC_Button() {
		bookingDetails.clickOnTnCRejectButton();
	}

	@And("owner click confirm to reject booking")
	public void owner_click_confirm_to_reject_booking() {
		bookingDetails.clickOnConfirmRejectButton();
	}

	@And("user clicks on Accept button on pengajuan booking page")
	public void user_clicks_on_accept_button_on_pengajuan_booking_page() {
	}

	@And("owner clicks on Booking Details")
	public void owner_clicks_on_booking_details() throws InterruptedException{
		bookingDetails.clickOnBookingDetailNeedConfirmationLink();
	}

	@When("user clicks on Next button reject booking")
	public void user_clicks_on_Next_button_reject_booking() throws InterruptedException {
		bookingDetails.clickOnNextRejectButton();
	}

	@When("user click saya mengerti button")
	public void user_clicks_saya_mengerti_button() throws InterruptedException {
		bookingDetails.clickOnSayaMengertiButton();
	}
}
