package steps.mamikos.booking;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.booking.BookingListingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OwnerBookingListingSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private BookingListingPO bookingListing = new BookingListingPO(driver);
	private JavaHelpers javaHelpers = new JavaHelpers();

	//Test Data
	private String propertyFile1="src/test/resources/testdata/mamikos/booking.properties";
	private String koseName = JavaHelpers.getPropertyValueForSquad(propertyFile1,"kostNameAssert");
	private String status = JavaHelpers.getPropertyValue(propertyFile1,"status");
	private String duration = JavaHelpers.getPropertyValueForSquad(propertyFile1,"duration");
	private String bookingText = JavaHelpers.getPropertyValue(propertyFile1,"bookingText");

	//Test data for Add Voucher
	private String addVoucherProperties="src/test/resources/testdata/mamikos/addVoucher.properties";
	private String statusForAddVoucher = JavaHelpers.getPropertyValue(addVoucherProperties,"statusForAddVoucher");
	private String rentDurationForAddVoucher = JavaHelpers.getPropertyValue(addVoucherProperties,"rentDurationForAddVoucher");
	private String bookingTextForADdVoucher = JavaHelpers.getPropertyValue(addVoucherProperties, "bookingTextForAddVoucher");
	private String tenantNameForAddVoucher = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"tenantNameForAddVoucher");
	private String kostNameForAddVoucher = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"kostNameForAddVoucher");



	@Then("booking is displayed with Name, Status, Room name, Checkin Date as tomorrow date, Duration, Booking text for {string} on Booking listing page")
	public void booking_is_displayed_with_correct_details_on_Booking_listing_page(String type) throws ParseException
	{
		Locale indonesian = new Locale("id", "ID");
		String pattern = "d MMMM yyyy";
		String tomorrowDate = javaHelpers.updateTime("yyyy MMM dd", javaHelpers.getTimeStamp("yyyy MMM dd"), "dd MMM yyyy", 1, 0, 0, 0);
		Date formatter = new SimpleDateFormat(pattern).parse(tomorrowDate);
		String checkInFullDate = javaHelpers.changeLocalDate(formatter, pattern, indonesian);

		String tenantName ="";
		String roomStatus ="";
		String roomName  ="";
		String rentDuration = "";
		String roomBookingText = "";
		if (type.equalsIgnoreCase("booking")){
			tenantName = Constants.TenantFacebookName;
			roomStatus = status;
			roomName = koseName;
			rentDuration = duration;
			roomBookingText = bookingText;
		}else if (type.equalsIgnoreCase("add voucher")){
			tenantName = tenantNameForAddVoucher;
			roomStatus = statusForAddVoucher;
			roomName = kostNameForAddVoucher;
			rentDuration = rentDurationForAddVoucher;
			roomBookingText = bookingTextForADdVoucher;
		}
		Assert.assertEquals(bookingListing.getTanentName(), tenantName,"Tenant name doesn't match");
		Assert.assertEquals(bookingListing.getStatusName(), roomStatus,"Status doesn't match");
		Assert.assertEquals(bookingListing.getRoomName(), roomName,"Room name doesn't match");
		Assert.assertEquals(bookingListing.getCheckInDate(),checkInFullDate,"Check In date doesn't match");
		Assert.assertEquals(bookingListing.getDuration(), rentDuration,"Duration doesn't match");
		Assert.assertTrue(bookingListing.getBookingInfoText().contains(roomBookingText),"Booking info text doesn't match");
	}
	
	@When("user clicks on Booking Details button")
	public void user_clicks_on_Booking_Details_button() 
	{
		bookingListing.clickOnViewDetailsLink();
	}

	@And("I can reach owner {string} section")
	public void i_can_reach_owner_booking_section(String booking) {
		Assert.assertEquals(bookingListing.getBookingTittleText(), booking, "Text is not equal to" + booking);
	}

	@When("user clicks on Booking Details index {int}")
	public void user_clicks_on_Booking_Details_index(int index) throws InterruptedException{
		bookingListing.clickOnBookingDetailNeedConfirmationBookingIndex(index);
	}
}
