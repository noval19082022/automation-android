package steps.mamikos.booking;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.tenant.booking.BookingHistoryPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class TenantBookingHistorySteps {
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private BookingHistoryPO history = new BookingHistoryPO(driver);
	private JavaHelpers java = new JavaHelpers();
	private PopUpPO popUp = new PopUpPO(driver);
	private LoadingPO loading = new LoadingPO(driver);

	//Test Data
	private String propertyFile1 = "src/test/resources/testdata/mamikos/booking.properties";
	private String koseName = JavaHelpers.getPropertyValueForSquad(propertyFile1, "koseName");
	private String status = JavaHelpers.getPropertyValue(propertyFile1, "bookingStatusOwnerSide");
	private String duration = JavaHelpers.getPropertyValueForSquad(propertyFile1, "duration");

	@Then("booking is displayed with Status, Room name, Checkin Date as tomorrow date, Duration, Payment expiry as tomorrow date for {string} on Booking listing page")
	public void booking_is_displayed_with_Status_Room_name_Checkin_Date_as_today_date_Duration_Payment_expiry_text_on_Booking_listing_page
			(String type) throws ParseException {
		String roomStatus = "";
		String roomName = "";
		String rentDuration = "";
		if (type.equalsIgnoreCase("booking")) {
			roomStatus = status;
			roomName = koseName;
			rentDuration = duration;
		}
		Assert.assertEquals(history.getStatusName(), roomStatus, "Status doesn't match");
		Assert.assertEquals(history.getRoomName(), roomName, "Room name doesn't match");
		String checkInDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "dd MMM yyyy", 1, 0, 0, 0);
		String todayDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "dd MMM yyyy", 0, 0, 0, 0);
		Assert.assertEquals(history.getDate(), checkInDate, "Check In date doesn't match");
		Assert.assertEquals(history.getDuration(), rentDuration, "Duration doesn't match");
		String text = history.getPaymentExpirationText();
		Assert.assertTrue(text.contains(todayDate), "Payment expiry text doesn't contain date. Actual Text: " + text);
	}

	@When("user click pay now button")
	public void user_click_pay_now_button() {
		history.clickOnPayNowButton();
	}

	@Then("user can sees first list is in waiting for confirmation stat")
	public void user_can_sees_first_list_is_in_waiting_for_confirmation_stat() {
		Assert.assertTrue(history.isBookingStatWaitingForConfirmation(), "Booking is not in need confirmation stat");
	}

	@When("user tap on first booking list")
	public void user_tap_on_first_booking_list() {
		history.tapOnFirstBookingList();
	}

	@Then("tenant can sees kost name {string} with {string} status from kost index {int}")
	public void tenant_can_sees_kost_name_with_status(String kostName, String kostStatus, int kostListIndex) throws InterruptedException{
		Assert.assertEquals(history.getStatusNameListIndex(kostListIndex), kostStatus);
		Assert.assertEquals(history.getKostNameListIndex(kostListIndex), kostName);
	}

	@When("tenant click on bayar disini button")
	public void tenant_click_on_bayar_disini_button() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
		}
		history.scrollToBayarDisiniButton();
		history.tapsOnBayarDisiniButton();
	}

	@Then("tenant can sees invoice view with price is {int}")
	public void tenant_can_sees_invoice_view_with_price_is(int kostPrice) throws InterruptedException{
		history.waitTillInvoiceVisible();
		Assert.assertTrue(history.isInvoiceNumberPresent());
		Assert.assertEquals(history.invoiceTotalPrice(), kostPrice);
	}

	@And("user click kost {string}")
	public void user_click_kost(String kostName) {
		history.tapOnKostBookingListHistory(kostName);
	}

	@And("user tap chat pemilik in detail booking")
	public void user_tap_chat_pemilik_in_detail_booking() throws InterruptedException{
		history.tapOnChatPemilik();
	}
}
