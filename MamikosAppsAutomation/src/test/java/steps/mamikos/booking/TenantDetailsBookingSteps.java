package steps.mamikos.booking;

import io.appium.java_client.AppiumDriver;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.ToastMessagePO;
import pageobjects.mamikos.tenant.booking.*;
import pageobjects.mamikos.tenant.explore.ExplorePO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.testng.Assert.*;

public class TenantDetailsBookingSteps
{
	
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private DetailBookingPO detailBooking = new DetailBookingPO(driver);
	private SearchPO search = new SearchPO(driver);

	@When("user tap on cancel booking button")
	public void user_tap_on_cancel_booking_button() {
		detailBooking.tapOnCancelBookingButton();
	}

	@Then("user should sees cancel booking section")
	public void user_should_sees_cancel_booking_section() {
		Assert.assertTrue(detailBooking.isInCancelSection(), "You are not in cancel booking section");
	}

	@When("user tap yes cancel booking button")
	public void user_tap_yes_cancel_booking_button() throws InterruptedException{
		detailBooking.tapOnYesCancelButton();
	}

	@Then("user can sees success cancel button pop-up")
	public void user_can_sees_success_cancel_button_pop_up() throws InterruptedException {
		Assert.assertEquals(detailBooking.getPopUpText(), "Booking Anda berhasil dibatalkan", "Message is not appear/wrong");
	}

	@And("user click back to booking detail")
	public void user_click_back_to_booking_detail() throws InterruptedException{
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			search.clickDeviceBackButton();
		} else {
			detailBooking.clickOnIcnBack(16, 68);
		}
	}
}
