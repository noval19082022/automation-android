package steps.mamikos.booking;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.ToastMessagePO;
import pageobjects.mamikos.tenant.booking.BookingInvoicePO;
import pageobjects.mamikos.tenant.payment.PaymentPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class TenantInvoiceStep {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private BookingInvoicePO invoice = new BookingInvoicePO(driver);
    private PaymentPO payment = new PaymentPO(driver);
    private ToastMessagePO toast = new ToastMessagePO(driver);

    //Test Data for Add Voucher
    private String addVoucherProperties="src/test/resources/testdata/mamikos/addVoucher.properties";
    private String remainingPaymentBeforeUseVoucherMonthly = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentBeforeUseVoucherMonthly");
    private String remainingPaymentAfterUseVoucherMonthly = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentAfterUseVoucherMonthly");
    private String remainingPaymentBeforeUseVoucher3Monthly = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentBeforeUseVoucher3Monthly");
    private String remainingPaymentAfterUseVoucher3Monthly = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentAfterUseVoucher3Monthly");
    private String remainingPaymentBeforeUseVoucherMonthlyWithDP = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentBeforeUseVoucherMonthlyWithDP");
    private String remainingPaymentAfterUseVoucherMonthlyWithDP = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentAfterUseVoucherMonthlyWithDP");
    private String remainingPaymentBeforeUseVoucherMonthlySettlement = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentBeforeUseVoucherMonthlySettlement");
    private String remainingPaymentAfterUseVoucherMonthlySettlement = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"remainingPaymentAfterUseVoucherMonthlySettlement");

    @When("user access voucher form")
    public void user_access_voucher_form() throws InterruptedException {
        invoice.accessVoucherForm();
    }

    @When("user remove voucher")
    public void user_remove_voucher() throws InterruptedException {
        invoice.clickOnRemoveVoucherButton();
    }

    @When("input {string} to field voucher code")
    public void input_to_field_voucher_code(String voucherCode) throws InterruptedException {
        invoice.enterVoucherCode(voucherCode);
    }

    @When("click use button")
    public void click_use_button() {
        invoice.clickOnUseButton();
    }

    @And("user select payment method {string}")
    public void user_select_payment_method(String paymentMethod) throws InterruptedException {
        payment.selectPaymentMethod(paymentMethod);
    }

    @When("system display remaining payment {string} use voucher for payment {string}")
    public void system_display_remaining_payment_use_voucher_for_payment(String condition, String paymentPeriod) throws InterruptedException {
        String remainingPayment="";
        if(condition.equalsIgnoreCase("before") && paymentPeriod.equals("monthly")){
            remainingPayment = remainingPaymentBeforeUseVoucherMonthly;
        }
        else if(condition.equalsIgnoreCase("after") && paymentPeriod.equals("monthly")){
            remainingPayment = remainingPaymentAfterUseVoucherMonthly;
        }
        else if(condition.equalsIgnoreCase("before") && paymentPeriod.equals("3 monthly")){
            remainingPayment = remainingPaymentBeforeUseVoucher3Monthly;
        }
        else if(condition.equalsIgnoreCase("after") && paymentPeriod.equals("3 monthly")){
            remainingPayment = remainingPaymentAfterUseVoucher3Monthly;
        }
        else if(condition.equalsIgnoreCase("before") && paymentPeriod.equals("monthlyWithDP")){
            remainingPayment = remainingPaymentBeforeUseVoucherMonthlyWithDP;
        }
        else if(condition.equalsIgnoreCase("after") && paymentPeriod.equals("monthlyWithDP")){
            remainingPayment = remainingPaymentAfterUseVoucherMonthlyWithDP;
        }
        else if(condition.equalsIgnoreCase("before") && paymentPeriod.equals("monthlySettlement")){
            remainingPayment = remainingPaymentBeforeUseVoucherMonthlySettlement;
        }
        else if(condition.equalsIgnoreCase("after") && paymentPeriod.equals("monthlySettlement")){
            remainingPayment = remainingPaymentAfterUseVoucherMonthlySettlement;
        }
        Assert.assertEquals(invoice.getRemainingPayment(), remainingPayment,"Remaining payment doesn't match");
    }

    @Then("user see voucher alert message {string}")
    public void user_see_voucher_alert_message(String message) throws InterruptedException {
        Assert.assertEquals(invoice.getVoucherAlertMessage(),message,"message doesn't match");
    }

    @Then("voucher applied successfully")
    public void voucher_applied_successfully() {
        Assert.assertTrue(toast.isToastMessageDisplayed("Voucher Dipakai"));
        Assert.assertTrue(invoice.isSuccessIconDisplayed());
    }

    @Then("voucher removed successfully")
    public void voucher_removed_successfully() {
        Assert.assertTrue(toast.isToastMessageDisplayed("Voucher Dihapus"));
    }
}