package steps.mamikos.booking;

import io.appium.java_client.AppiumDriver;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.But;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.ToastMessagePO;
import pageobjects.mamikos.owner.premium.UpgradePremiumPO;
import pageobjects.mamikos.tenant.booking.BookingConfirmationPO;
import pageobjects.mamikos.tenant.booking.BookingDetailsPO;
import pageobjects.mamikos.tenant.booking.BookingFormFillPO;
import pageobjects.mamikos.tenant.booking.BookingSuccessPO;
import pageobjects.mamikos.tenant.detail.KostDetailsPO;
import pageobjects.mamikos.tenant.explore.ExplorePO;
import pageobjects.mamikos.tenant.search.FilterPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.testng.Assert.*;

public class TenantKostBookingSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private FooterPO footer = new FooterPO(driver);
    private BookingDetailsPO bookingDetails = new BookingDetailsPO(driver);
    private BookingFormFillPO bookingFillform = new BookingFormFillPO(driver);
    private BookingConfirmationPO bookingConfirmation = new BookingConfirmationPO(driver);
    private BookingSuccessPO bookingSuccess = new BookingSuccessPO(driver);
    private FilterPO filter = new FilterPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private JavaHelpers java = new JavaHelpers();
    private ToastMessagePO toast = new ToastMessagePO(driver);
    private PopUpPO popup = new PopUpPO(driver);
    private SearchPO search = new SearchPO(driver);
    private ExplorePO explore = new ExplorePO(driver);
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private KostDetailsPO kostDetail = new KostDetailsPO(driver);
    private UpgradePremiumPO premiumPO = new UpgradePremiumPO(driver);

    //Test Data
    private String propertyFile1 = "src/test/resources/testdata/mamikos/booking.properties";
    private String koseName = JavaHelpers.getPropertyValueForSquad(propertyFile1, "koseName");
    private String kosePrice = JavaHelpers.getPropertyValueForSquad(propertyFile1, "kosePrice");
    private String gender = JavaHelpers.getPropertyValueForSquad(propertyFile1, "gender");
    private String phone = JavaHelpers.getPropertyValueForSquad(propertyFile1, "phone");
    private String priceDetailsHeading = JavaHelpers.getPropertyValue(propertyFile1, "priceDetailsHeading");
    private String minimumPaymentHeading = JavaHelpers.getPropertyValue(propertyFile1, "minimumPaymentHeading");
    private String electricityCostHeading = JavaHelpers.getPropertyValue(propertyFile1, "electricityCostHeading");
    private String rentalPriceHeading = JavaHelpers.getPropertyValue(propertyFile1, "rentalPriceHeading");
    private String perDay = JavaHelpers.getPropertyValue(propertyFile1, "perDay");
    private String maleKoseValidationMessage = JavaHelpers.getPropertyValue(propertyFile1, "maleKoseValidationMessage");
    private String femaleKoseValidationMessage = JavaHelpers.getPropertyValue(propertyFile1, "femaleKoseValidationMessage");
    private String bookingDetailsPageHeading1 = JavaHelpers.getPropertyValue(propertyFile1, "bookingDetailsPageHeading1");
    private String rentInfoForMonth = JavaHelpers.getPropertyValue(propertyFile1, "rentInfoForMonth");
    private String rentInfoForWeek = JavaHelpers.getPropertyValue(propertyFile1, "rentInfoForWeek");
    private String rentTypeIsPerWeek = JavaHelpers.getPropertyValue(propertyFile1, "rentTypeIsPerWeek");
    private String rentCountOfPerWeek = JavaHelpers.getPropertyValue(propertyFile1, "rentCountOfPerWeek");
    private String rentTypeIsPer3Month = JavaHelpers.getPropertyValue(propertyFile1, "rentTypeIsPer3Month");
    private String rentCountOf3Month = JavaHelpers.getPropertyValue(propertyFile1, "rentCountOf3Month");
    private String rentTypeIsPer6month = JavaHelpers.getPropertyValue(propertyFile1, "rentTypeIsPer6month");
    private String rentCountOf6Month = JavaHelpers.getPropertyValue(propertyFile1, "rentCountOf6Month");
    private String rentTypeIsPerYear = JavaHelpers.getPropertyValue(propertyFile1, "rentTypeIsPerYear");
    private String rentCountOfPerYear = JavaHelpers.getPropertyValue(propertyFile1, "rentCountOfPerYear");
    private String maximumRentCountForWeek = JavaHelpers.getPropertyValue(propertyFile1, "maximumRentCountForWeek");
    private String maximumRentCountForMonth = JavaHelpers.getPropertyValue(propertyFile1, "maximumRentCountForMonth");
    private String duration = JavaHelpers.getPropertyValueForSquad(propertyFile1, "duration");
    private String rentCountOfPerMonth = JavaHelpers.getPropertyValue(propertyFile1, "rentCountOfPerMonth");
    private String increaseRentCountOfPerMonth = JavaHelpers.getPropertyValue(propertyFile1, "increaseRentCountOfPerMonth");

    @And("user clicks on Booking button on Kost details page")
    public void user_clicks_on_Booking_button_on_Kost_details_page() throws InterruptedException, ParseException {
        bookingDetails.clickOnBookingButton();
    }
    @When("user input boarding start date is {string} and clicks on booking button")
    public void user_input_boarding_start_date_is_and_clicks_on_booking_button(String bookingDate) throws ParseException, InterruptedException {
        int incrementDate = 0;
        if (bookingDate.equalsIgnoreCase("tomorrow")) {
            incrementDate = 1;
        }
        String date = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", incrementDate, 0, 0, 0);
        bookingDetails.setStartDate(date);
        bookingDetails.clickOnBookingButtonOnChooseDate();
    }

    @When("user select payment period {string} and click on continue button")
    public void user_select_payment_period(String period) throws InterruptedException {
        bookingFillform.selectRentType(period);
        bookingFillform.clickOnContinueButton();
    }

    @When("user/tenant/I fill/fills out date as tomorrow date")
    public void xFillOutDateAsTomorrowDate() throws ParseException, InterruptedException {
        String tomorrowDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        bookingFillform.setDate(tomorrowDate);
        appium.hardWait(1);
    }

    @And("user fills out rent duration equals to 4 Bulan and clicks on Continue button")
    public void user_fills_out_Rent_Duration_and_clicks_on_Continue_button() throws InterruptedException, ParseException {
        for (int i = 1; i < 4; i++) {
            bookingFillform.increaserentDuration();
        }
        bookingFillform.clickOnContinueButton();
    }

    @When("user input boarding start date is today and clicks on continue button")
    public void user_input_boarding_start_date_is_today_and_clicks_on_continue_button() throws ParseException, InterruptedException {
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        bookingFillform.setDateToday(today);
        bookingFillform.clickOnContinueButton();
    }

    @Then("booking confirmation screen displayed with  Kost name, Room price, Tenant Name, Gender, Phone and click on next button")
    public void booking_confirmation_screen_displayed_with_Kost_name_Room_price_Tenant_Name_Gender_Phone() throws InterruptedException {
        assertEquals(bookingConfirmation.getRoomName(), koseName, "Room Name doesn't match");
        assertEquals(bookingConfirmation.getRoomPrice(), "Per Bulan Rp " + kosePrice, "Room Price doesn't match");
        assertEquals(bookingConfirmation.getTenantName(), Constants.TenantFacebookName, "Tenant name doesn't match");
        assertEquals(bookingConfirmation.getTenantGender(), gender, "Tenant gender doesn't match");
        assertEquals(bookingConfirmation.getTenantMobile(), phone, "Tenant mobile doesn't match");
        bookingFillform.clickOnContinueButton();
    }

    @Then("booking confirmation screen displayed with  Kost name, Room price, Tenant Name, Gender, Phone for {string} and click on next button")
    public void booking_confirmation_screen_displayed_with_Kost_name_Room_price_Tenant_Name_Gender_Phone(String type) throws InterruptedException {
        String roomName = "";
        String roomPrice = "";
        String tenantName = "";
        String tenantGender = "";
        String tenantPhone = "";
        if (type.equalsIgnoreCase("booking")) {
            roomName = koseName;
            roomPrice = kosePrice;
            tenantName = Constants.TenantFacebookName;
            tenantGender = gender;
            tenantPhone = phone;
        }
        assertEquals(bookingConfirmation.getRoomName(), roomName, "Room Name doesn't match");
        assertEquals(bookingConfirmation.getRoomPrice(), "Per Bulan Rp " + roomPrice, "Room Price doesn't match");
        assertEquals(bookingConfirmation.getTenantName(), tenantName, "Tenant name doesn't match");
        assertEquals(bookingConfirmation.getTenantGender(), tenantGender, "Tenant gender doesn't match");
        assertEquals(bookingConfirmation.getTenantMobile(), tenantPhone, "Tenant mobile doesn't match");
        bookingFillform.clickOnContinueButton();
    }

    @When("user selects T&C checkbox and clicks on Book button")
    public void user_selects_T_C_checkbox_and_clicks_on_Book_button() throws InterruptedException{
        bookingConfirmation.clickOnAjukanSewaButton();
        bookingConfirmation.clickOnTermsAndConditionCheckBox();
        bookingConfirmation.clickOnSubmitToOwner();
    }

    @Then("system display message booking successfully")
    public void system_display_message_booking_successfully() throws InterruptedException {
        bookingConfirmation.messageBookingSuccessfullyIsPresent();
    }

    @When("user click next button")
    public void user_click_next_button() throws InterruptedException {
        bookingFillform.clickOnContinueButton();
        loading.waitLoadingFinish();
    }

    @And("user navigates to main page after booking")
    public void user_navigates_to_main_page_after_booking() {
        try {
            //If booking success page displayed
            bookingSuccess.clickOnViewBookingButton();
            do {
                appium.back();
            }
            while (!explore.isMamikosLogoDisplayed());
        } catch (Exception e) {
            //If booking success page not displayed, just go back to main screen
            do {
                appium.back();
            }
            while (!explore.isMamikosLogoDisplayed());
        }
    }

    @When("user navigates to main page after booking successfully")
    public void user_navigates_to_main_page_after_booking_successfully() {

        for (int i = 0; i < 5; i++) {
            appium.back();
        }
        do {
            appium.back();
        } while (!footer.isLoginMenuDisplayed());
    }

    @Then("user navigates to profile page after terminate contract successfully")
    public void user_navigates_to_profile_page_after_terminate_contract_succesfully() {
        do {
            appium.back();
        } while (!footer.isLoginMenuDisplayed());
    }

    @And("user tap back button in Booking form page")
    public void userTapBackButtonInBookingFormPage() {
        bookingFillform.tapBackBtn();
    }

    @Then("user redirect to Kost Details page")
    public void userRedirectToKostDetailsPage() {
        assertTrue(bookingDetails.checkRoomAvailability(), "Not in Kost Detail page");
    }

    @Then("user see full name and phone number")
    public void userSeeFullNameAndPhoneNumber() {
        assertTrue(bookingFillform.checkFullName(koseName), "Full name not auto filled");
        assertTrue(bookingFillform.checkPhoneNo(phone), "Phone number not auto filled");
    }

    @And("user tap save without filling the gender")
    public void userTapSaveWithoutFillingTheGender() {
        bookingFillform.tapSave();
    }

    @Then("user see validation message {string}")
    public void userSeeValidationMessage(String genderMessage) throws InterruptedException {
        assertTrue(bookingFillform.checkPopupMsg(genderMessage), "Error message gender form is wrong");
        bookingFillform.tapOk();
        bookingDetails.clickOnBookingButton();
    }

    @When("user tap save without filling full name")
    public void userTapSaveWithoutFillingFullName() {
        bookingFillform.clearName();
        bookingFillform.tapSave();
    }

    @Then("user see toast message {string}")
    public void user_see_toast_message(String message) {
        assertTrue(toast.isToastMessageDisplayed(message), "Toast message not appear");
    }

    @When("user fill phone number with invalid number, example : {string} then tap save")
    public void userFillPhoneNumberWithInvalidNumberExampleThenTapSave(String number) {
        bookingFillform.enterPhoneNo(number);
        bookingFillform.tapSave();
    }

    @And("Tenant tap on FTUE screen and dismiss it")
    public void tenantTapOnFTUE_screenAndDismissIt() throws InterruptedException {
        bookingDetails.dismissFTUE_screen();
    }

    @And("user click on price of details page and price details popup should be shown")
    public void userClickOnPriceOfDetailsPageAndPriceDetailsPopupShouldBeShown() {
        popup.goThroughBookingPopUps();
        bookingDetails.clickOnPriceText();
        assertEquals(bookingDetails.getPriceDetailsText(), priceDetailsHeading, "Heading text doesn't match");
    }

    @Then("user click on price of details page and validate price details, minimum payment, electricity cost, rental price heading should be shown")
    public void userClickOnPriceOfDetailsPageAndValidateDataShouldBeShown() {
        popup.goThroughBookingPopUps();
        bookingDetails.clickOnPriceText();
        assertEquals(bookingDetails.getPriceDetailsText(), priceDetailsHeading, "Heading text doesn't match");
        assertEquals(bookingDetails.getDetailPaymentText(), minimumPaymentHeading, "Detail Payment Text doesn't match");
        assertEquals(bookingDetails.getElectricPaymentText(), electricityCostHeading, "Electric Payment text doesn't match");
        assertEquals(bookingDetails.getPriceTitle(), rentalPriceHeading, "Price Title text doesn't match");

    }

    @Then("booking button should be disable on Kost details page")
    public void bookingButtonShouldBeDisableOnKostDetailsPage() {
        assertTrue(bookingDetails.isBookingButtonDisabled(), "Booking Button is not displayed");
    }

    @Then("validation message {string} should be shown")
    public void validationMessageShouldBeShown(String type) {
        String validationMessage = "";
        if (type.equalsIgnoreCase("male kose")) {
            validationMessage = maleKoseValidationMessage;
        } else if (type.equalsIgnoreCase("female kose")) {
            validationMessage = femaleKoseValidationMessage;
        }

        assertEquals(bookingDetails.getValidationTextForGender(), validationMessage, "Validation message does't match");
    }

    @Then("user should navigate to booking details page")
    public void userShouldNavigateToBookingDetailsPage() {
        assertEquals(bookingFillform.getBookingPageHeaderText(), bookingDetailsPageHeading1, "Page Header doesn't match");
    }


    @Then("click out side of popup and popup should be dismiss")
    public void clickOutSideOfPopupAndPopupShouldBeDismiss() {
        bookingDetails.clickOnOutSideOfPricePopup();
        assertFalse(bookingDetails.isPricePopupViewDisplayed(), "Price Popup doesn't dismiss");
    }

    @Then("user click on see all price link text and price popup view should be dismiss")
    public void userClickOnSeeAllPriceLinkTextAndPricePopupViewShouldBeDismiss() {
        bookingDetails.clickOnSeeAllPriceLinkText();
        assertFalse(bookingDetails.isPricePopupViewDisplayed(), "Price Popup doesn't dismiss");
    }

    @And("Per Day price option should be shown in price popup and click on info button")
    public void perDayPriceOptionShouldBeShownInPricePopupAndClickOnInfoButton() {
        assertEquals(bookingDetails.getDailyPriceText(), perDay, "text doesn't match");
        bookingDetails.clickOnDailyPriceInfoButton();
    }


    @And("user click on back button to navigate previous page")
    public void userClickOnBackButtonToNavigatePreviousPage() throws InterruptedException {
        popup.goThroughBookingPopUps();
        bookingDetails.tapBack();
    }

    @And("user click on love button")
    public void userClickOnLoveButton() throws InterruptedException {
        bookingDetails.clickOnLoveButton();
    }

    @When("user click on share button kost")
    public void user_click_on_share_button_kost() {
        bookingDetails.clickOnShareButton();
    }

    @Then("share social media pop up appear")
    public void share_social_media_pop_up_appear() {
        bookingDetails.isMediaSharePresent();
    }

    @And("user back to Home page")
    public void userBackToHomePage() {
        for (int i = 0; i < 3; i++) {
            if (Constants.MOBILE_OS == Platform.ANDROID) {
                appium.back();
            }else{
                premiumPO.clickOnBackIconButton();
            }
        }
    }

    @And("user click on report kost linked text")
    public void userClickOnReportKostLinkedText() {
        bookingDetails.clickOnReportRoomLinkedText();
    }

    @Then("report kos heading {string} should be shown")
    public void reportKosHeadingShouldBeShown(String pageHeading) {
        assertEquals(bookingDetails.getReportPageHeading(), pageHeading, "Report Page Heading is not shown");
    }

    @Then("click on back button to navigate back to Kost details page and verify Booking Button is displayed")
    public void clickOnBackButtonToNavigateBackToKostDetailsPageAndVerifyBookingButtonIsDisplayed() {
        bookingDetails.clickOnReportPageBackButton();
        assertTrue(bookingDetails.isBookingButtonDisplayed(), "Booking button is not displayed");
    }

    @And("user scroll down to Other Interesting Boarding Houses section")
    public void userScrollDownToOtherInterestingBoardingHousesSection() {
        bookingDetails.navigateToRelatedRoomText();
    }

    @Then("user verify other kost list should be shown")
    public void userVerifyOtherKostListShouldBeShown(List<String> roomName) {
        for (int i = 0; i < roomName.size(); i++) {
            assertTrue(bookingDetails.isRoomNamePresent(roomName.get(i)), roomName.get(i) + " is not present");
        }

    }

    @And("user swipe right end verify below list")
    public void userSwipeRightEndVerifyBelowList(List<String> roomName) {
        appium.scrollHorizontal(0.2, 0.80, 0.20, 2000);
        appium.scrollHorizontal(0.2, 0.80, 0.20, 2000);
        for (int i = 0; i < roomName.size(); i++) {
            assertTrue(bookingDetails.isRoomNamePresent(roomName.get(i)), roomName.get(i) + " is not present");
        }
    }

    @Then("scroll down to Job vacancy section and verify below element should shown")
    public void scrollDownToJobVacancySectionAndVerifyBelowElementShouldShown(List<String> jobName) {
        bookingDetails.scrollToJobVacancy();
        for (int i = 0; i < jobName.size(); i++) {
            assertTrue(bookingDetails.isJobVacancyPresent(jobName.get(i)), jobName.get(i) + " is not present");
        }
    }

    @Then("user click on read all review button and verify Review page gets open with heading {string}")
    public void userClickOnReadAllReviewButtonAndVerifyReviewPageGetsOpenWithHEading(String headingName) {
        bookingDetails.clickOnSeeAllReviewButton();
        assertEquals(bookingDetails.getReviewPageHeadingText(), headingName, "Page header text doesn't match");

    }

    @Then("click on back button of review page to navigate back to Kost details page and verify Booking Button is displayed")
    public void clickOnBackButtonOfReviewPageToNavigateBackToKostDetailsPageAndVerifyBookingButtonIsDisplayed() {
        bookingDetails.clickOnReviewPageBackButton();
        assertTrue(bookingDetails.isBookingButtonDisplayed(), "Booking button is not displayed");
    }

    @Then("user click on other kost and click on back button to navigate back to previous kost details page")
    public void userClickOnOtherKostAndClickOnBackButtonToNavigateBackToPreviousKostDetailsPage() {
        bookingDetails.clickOnOtherKostRoom();
        bookingDetails.tapBack();
        assertTrue(bookingDetails.isBookingButtonDisplayed(), "Booking button is not displayed");
    }

    @Then("click on first job to open vacancy page and click on back button to navigate back to kost details page")
    public void clickOnFirstJobToOpenVacancyPageAndClickOnBackButtonToNavigateBackToKostDetailsPage() throws InterruptedException {
        bookingDetails.clickOnVacancyRelatedText();
        appium.hardWait(5);
        bookingDetails.clickOnReportPageBackButton();
        assertTrue(bookingDetails.isBookingButtonDisplayed(), "Booking button is not displayed");
    }

    @Then("verify FTUE Element not present on that page")
    public void verifyFTUEElementNotPresentOnThatPage() throws InterruptedException {
        assertFalse(bookingDetails.isFTUE_screenPresent(), "FTUE Element is present");
    }

    @And("user click on outer side of FTUE popup and verify popup does not dismiss")
    public void userClickOnOuterSideOfFTUEPopupAndVerifyPopupDoesNotDismiss() throws InterruptedException {
        bookingDetails.clickOnOuterSideOfPopup();
        assertTrue(bookingDetails.isFTUE_screenPresent(), "FTUE popup is not shown");
    }

    @Then("navigate again {string} kost details page and verify FTUE popup should be shown with first card")
    public void navigateAgainKostDetailsPageAndVerifyFTUEPopupShouldBeShownWithFirstCard(String kostName) throws InterruptedException {
        search.enterTextToSearchTextboxAndSelectResult(kostName);
        assertFalse(bookingDetails.isFTUE_screenPresent(), "FTUE popup is present for second time!");

    }

    @And("user click on filter button and select {string}")
    public void userClickOnFilterButtonAndSelect(String filterName) {
        bookingDetails.clickOnReviewFilterButton();
        bookingDetails.selectReviewShortingOptionFromDropdown(filterName);
    }

    @Then("verify below data")
    public void verifyBelowData(List<String> reviewDate) {
        // create list that contain listing status from top
        List<String> Update = bookingDetails.getListOfReviewDate(5);
        // create the duplicate array from listing update status
        List<String> updateCopy = new ArrayList<>(Update);
        // sort duplicate list by number asc
        Collections.sort(updateCopy, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o1) - extractInt(o2);
            }

            int extractInt(String s) {
                String num = s.replaceAll("\\D", "");
                // return 0 if no digits found
                return num.isEmpty() ? 0 : Integer.parseInt(num);
            }
        });
        // Create comparator, compare with each of DefinedOrder list
        Comparator<String> timeComparator =
                Comparator.comparing(
                        c -> c.contains(reviewDate.get(0)));
        Comparator<String> timeComparator1 =
                Comparator.comparing(
                        c -> c.contains(reviewDate.get(1)));
        Comparator<String> timeComparator2 =
                Comparator.comparing(
                        c -> c.contains(reviewDate.get(2)));
        Comparator<String> timeComparator3 =
                Comparator.comparing(
                        c -> c.contains(reviewDate.get(3)));
        Comparator<String> timeComparator4 =
                Comparator.comparing(
                        c -> c.contains(reviewDate.get(4)));
        Collections.sort(updateCopy, timeComparator4.thenComparing(timeComparator3)
                .thenComparing(timeComparator2).thenComparing(timeComparator1)
                .thenComparing(timeComparator));
        assertEquals(Update, updateCopy, "Date not sorted by latest update");
    }

    @Then("tooltip should be shown")
    public void tooltipImageShouldBeShown() throws IOException, URISyntaxException {
        assertTrue(bookingDetails.isPerDayTooltipDisplayed(), "Per Day tooltip not shown or is incorrect");
    }

    @Then("^tenant can see price list$")
    public void tenant_can_see_price_list(List<String> list) throws InterruptedException {
        for (String s : list) {
            assertTrue(bookingDetails.isPricePresent(s), "Price list " + s + " is not present");
        }
    }

    @Then("^tenant can see price list on booking confirmation$")
    public void tenant_can_see_price_list_on_booking_confirmation(List<String> list) throws InterruptedException {
        for (String s : list) {
            assertTrue(bookingConfirmation.isBookingPricePresent(s), "Price list " + s + " in booking confirmation is not present");
        }
    }

    @Then("tenant can not see daily price")
    public void tenant_can_not_see_daily_price() throws InterruptedException {
        assertFalse(bookingConfirmation.isBookingPricePresent("Per Hari"), "Price list daily is present");
    }

    @Then("verify kose price should be shown {string}")
    public void verifyKosePriceShouldBeShown(String kostPrice) {
        assertEquals(bookingDetails.getRoomPriceText(), kostPrice, "Kost price isn't match");
    }

    @Then("verify selected rent type per bulan shown kost own price and also verify daily price option Per Hari should not shown")
    public void verifySelectedRentTypePerBulanShownKostOwnPriceAndAlsoVerifyDailyPriceOptionPerHariShouldNotShown() {
        assertEquals(bookingFillform.getRentPriceValueForPerBulanFromSubmissionPage(), "Rp " + kosePrice, "Rent price does not match");
        assertFalse(bookingFillform.isRentTypePresentOnSubmissionPage(), "Rent Type isn't present on submission page");
    }

    @And("verify selected rent type information {string} should be shown")
    public void verifySelectedRentTypeInformationShouldBeShown(String type) {
        String subTitleText = "";
        if (type.equalsIgnoreCase("month")) {
            subTitleText = rentInfoForMonth;
        } else if (type.equalsIgnoreCase("week")) {
            subTitleText = rentInfoForWeek;
        }
        assertEquals(bookingFillform.getSubTitleTextOfRentType(), subTitleText, "Text doesn't match");
    }

    @Then("verify rent count value is shown {string} according to selected rent type")
    public void verifyRentCountValueIsShownAccordingToSelectedRentType(String type) {
        String rentValue = "";
        if (type.equalsIgnoreCase("week")) {
            rentValue = rentCountOfPerWeek;
        } else if (type.equalsIgnoreCase("month")) {
            rentValue = rentCountOfPerMonth;
        } else if (type.equalsIgnoreCase("3 month")) {
            rentValue = rentCountOf3Month;
        } else if (type.equalsIgnoreCase("6 month")) {
            rentValue = rentCountOf6Month;
        } else if (type.equalsIgnoreCase("year")) {
            rentValue = rentCountOfPerYear;
        } else if (type.equalsIgnoreCase("increaseCount")) {
            rentValue = increaseRentCountOfPerMonth;
        }
        assertEquals(bookingFillform.getRentCountValueText(), rentValue, "Rent value doesn't match");
    }

    @And("user click on {string} rent type")
    public void userClickOnRentType(String type) throws InterruptedException {
        String rentType = "";
        if (type.equalsIgnoreCase("week")) {
            rentType = rentTypeIsPerWeek;
        } else if (type.equalsIgnoreCase("3 month")) {
            rentType = rentTypeIsPer3Month;
        } else if (type.equalsIgnoreCase("6 month")) {
            rentType = rentTypeIsPer6month;
        } else if (type.equalsIgnoreCase("year")) {
            rentType = rentTypeIsPerYear;
        }
        bookingFillform.selectRentType(rentType);
    }

    @And("user click on increase duration button maximum time")
    public void userClickOnIncreaseDurationButtonMaximumTime() {
        bookingFillform.clickOnIncreaseRentButtonMaximumTime();
    }

    @Then("verify rent duration value is shown maximum count of selected rent type {string}")
    public void verifyRentDurationValueIsShownMaximumCountOfSelectedRentType(String type) {
        String rentCountValue = "";
        if (type.equalsIgnoreCase("week")) {
            rentCountValue = maximumRentCountForWeek;
        } else if (type.equalsIgnoreCase("month")) {
            rentCountValue = maximumRentCountForMonth;
        }
        assertEquals(bookingFillform.getRentCountValueText(), rentCountValue, "Rent Count does not match");
    }

    @And("user increase rent duration")
    public void userIncreaseRentDuration() {
        bookingFillform.increaserentDuration();
    }

    @Then("verify increase button should be disabled")
    public void verifyIncreaseButtonShouldBeDisabled() {
        assertFalse(bookingFillform.isIncreaseRentDurationButtonEnabled(), "Increase Rent Duration button is Enable");
    }

    @And("user decrease rent duration")
    public void userDecreaseRentDuration() {
        bookingFillform.clickOnDecreaseRentDurationButton();
    }

    @Then("verify decrease button should be disabled")
    public void verifyDecreaseButtonShouldBeDisabled() {
        assertFalse(bookingFillform.isDecreaseRentDurationButtonEnabled(), "Decrease Rent duration button is enable");
    }

    @Then("click on next button to navigate preview and verify rent duration")
    public void clickOnNextButtonToNavigatePreviewAndVerifyRentDuration() throws InterruptedException {
        bookingFillform.clickOnContinueButton();
        Assert.assertEquals(bookingFillform.getRentDurationText(), duration, "Rent duration is not match");
    }

    @Then("user verify rent paid with {string}")
    public void user_verify_rent_paid_with(String duration) {
        assertEquals(bookingFillform.getRentPaidDurationText(duration), duration, "Rent paid duration is not equal to " + duration);
    }

    @Then("user verify each rental price")
    public void user_verify_each_rental_price(DataTable rentDuration) {
        List<List<String>> duration = rentDuration.asLists(String.class);
        for (int i = 0; i < duration.size(); i++) {
            bookingFillform.clickOnEachRentalPrice(duration.get(0).get(i));
            for (int j = i + 1; j < duration.size(); j++) {
                assertEquals(bookingFillform.getRentPaidDurationText(duration.get(j).get(i)), duration.get(j).get(i), "Rent duration is not equal to " + duration.get(j).get(i));
            }
        }
    }

    @Then("tenant should reach booking form section")
    public void tenant_should_reach_booking_form_section() throws InterruptedException {
        assertEquals(bookingFillform.getBookingPageHeaderText(), bookingDetailsPageHeading1, "Page Header doesn't match");
        assertTrue(bookingFillform.isInBookingSectionBookingInputIDPresent(), "You are not in booking from section");
    }

    @Then("user verify each rental duration")
    public void user_verify_each_rental_duration(DataTable rentDuration) {
        List<List<String>> duration = rentDuration.asLists(String.class);
        for (int i = 0; i < duration.size(); i++) {
            bookingFillform.clickOnEachRentalPrice(duration.get(0).get(i));
            for (int j = i + 1; j < duration.size(); j++) {
                assertEquals(bookingFillform.getRentalDurationText(), duration.get(j).get(i), "Rent duration is not equal to " + duration.get(j).get(i));
            }
        }
    }

    @And("user input rent duration equals to {int} and click on continue button")
    public void user_input_rent_duration_equals_to(int duration) throws InterruptedException {
        appium.hardWait(3);
        appium.scrollToElementByResourceId("incrementImageView");
        for (int i = 1; i < duration; i++) {
            bookingFillform.increaserentDuration();
        }
        bookingFillform.clickOnContinueButton();
    }

    @Then("user can see booking restriction pop-up")
    public void user_can_see_booking_restriction_pop_up() {
        Assert.assertTrue(bookingDetails.isBookingRestrictionPopUpPresent(), "Booking restriction pop-up is not present");
        Assert.assertEquals(bookingDetails.getBookingRestrictionText("1"), "Belum bisa booking", "First text is not Belum bisa booking");
        Assert.assertEquals(bookingDetails.getBookingRestrictionText("2"), "Selesaikan proses booking sebelumnya.", "Second text is not Selesaikan proses booking sebelumnya");
        Assert.assertEquals(bookingDetails.getBookingRestrictionText("3"), "Lihat history booking", "Third text is not Lihat history booking");
        Assert.assertEquals(bookingDetails.getBookingRestrictionText("4"), "Batal", "Fourth text is not Batal");
    }

    @When("user dismiss booking restriction pop-up")
    public void user_dismiss_booking_restriction_pop_up() {
        bookingDetails.tapOnCancelButtonBookingRestriction();
    }

    @When("user back to Home page from kost details")
    public void user_back_to_home_page_from_kost_details() throws InterruptedException {
        while (!explore.isInHomepage()) {
            if (searchListing.isBackButtonVisible()) {
                searchListing.clickBack();
            } else {
                appium.back();
            }
        }
    }

    @When("user input {string} with delete char is {}")
    public void user_input_x_with_delete_char_is_boolean(String name, Boolean delete) {
        bookingConfirmation.inputTenantName(name, delete);
    }

    @When("user can see validation name is {string}")
    public void user_can_see_validation_name_is(String errorMessage) {
        Assert.assertEquals(bookingConfirmation.getValidationErrorString(), errorMessage, "Error message is not " + errorMessage);
    }

    @But("user could not input for more than 50 character")
    public void user_could_not_input_for_more_than_50_character() {
        Assert.assertTrue(bookingConfirmation.getTenantName().length() == 50, "Lenght is" + bookingConfirmation.getTenantName().length());
    }

    @Then("user can see Continue button is not clickable")
    public void user_can_see_Continue_button_is_not_clickable() {
        Assert.assertFalse(bookingConfirmation.getNextButtonStatus("enabled"), "Next/Continue button is clickable");
    }

    @When("user navigates trough FTUE pop up")
    public void user_navigates_trough_ftue_pop_up() {
        popup.goThroughBookingPopUps();
    }

    @Then("user can see Ajukan Sewa button is disabled")
    public void user_can_see_ajukan_sewa_button_is_disabled() {
        assertEquals(bookingDetails.getRentSubmitButtonStatus("enabled"), "false", "Rent submit button is not disabled");
    }

    @Then("user can sees female and male button are changeable")
    public void user_can_sees_female_and_male_button_are_changeable() {
        assertEquals(bookingConfirmation.getFemaleButtonStatus("clickable"), "true", "female button is not changeable");
        assertEquals(bookingConfirmation.getMaleButtonStatus("clickable"), "true", "male button is not changeable");
    }

    @Then("user can sees {string} as alert message")
    public void user_can_sees_x_as_alert_message(String message) {
        assertEquals(bookingConfirmation.getAllertMessageText(), message, "Message is not " + message);
    }

    @Then("user can sees renter amount options")
    public void user_can_sees_renter_amount_options() {
        bookingConfirmation.scrollDownToRenterAmount();
        assertEquals(bookingConfirmation.getCounterRenterStatus("displayed"), "true", "Counter Renter Box is not displayed");
    }

    @When("user {string} renter amount equal to {int}")
    public void user_decrease_or_increase_renter_amount_equal_to_number(String incDec, int amount) throws InterruptedException {
        bookingConfirmation.changeRenterAmount(amount, incDec);
    }

    @Then("user can sees pasangan suami istri checkbox")
    public void user_can_sees_pasangan_suami_istri_checkbox() {
        assertEquals(bookingConfirmation.getMarriedCheckBoxStatus("displayed"), "true", "Message box is not visible");
        assertEquals(bookingConfirmation.getMarriedCheckBoxStatus("checkable"), "true", "Message box is checkable");
        assertEquals(bookingConfirmation.getMarriedCheckBoxStatus("checked"), "false", "Married People box already checked");
        bookingConfirmation.checkOnMarriedPeopleCheckBox();
        assertEquals(bookingConfirmation.getMarriedCheckBoxStatus("checked"), "true", "Married people checkbox is not checked");
    }

    @When("user deletes phone number")
    public void user_deletes_phone_number() {
        bookingConfirmation.inputTenantPhoneNumber("");
    }

    @When("user deletes phone number edit")
    public void user_deletes_phone_number_edit() {
        bookingConfirmation.inputTenantPhoneNumberEdit("");
    }

    @Then("user can sees {string} as empty phone number invalid text")
    public void user_can_sees_x_as_empty_phone_number_invalid_text(String invalidText) {
        assertEquals(bookingConfirmation.getInvalidPhoneNumberText(), invalidText, "Invalid message should " + invalidText);
    }

    @Then("user can sees {string} as empty phone number invalid text edit")
    public void user_can_sees_x_as_empty_phone_number_invalid_text_edit(String invalidText) {
        assertEquals(bookingConfirmation.getInvalidPhoneNumberTextEdit(), invalidText, "Invalid message should " + invalidText);
    }

    @When("user input phone number with {string}")
    public void user_input_phone_number_with_x(String phoneNumber) {
        bookingConfirmation.inputTenantPhoneNumber(phoneNumber);
    }

    @When("user input phone number with {string} edit")
    public void user_input_phone_number_with_x_edit(String phoneNumber) {
        bookingConfirmation.inputTenantPhoneNumberEdit(phoneNumber);
    }

    @When("user tap on Save & Continue button")
    public void user_tap_on_save_and_continue_button() throws InterruptedException {
        bookingFillform.clickOnContinueButton();
    }

    @Then("user should still in booking confirmation page")
    public void user_should_still_in_booking_confirmation_page() {
        assertEquals(bookingConfirmation.getPreviewBookingTitleText(), "Ringkasan Pengajuan Sewa", "You are not in booking confirmation or the booking is success with duplicate phone number");
    }

    @When("user edits booking form")
    public void user_edits_booking_form() {
        bookingConfirmation.clickOnRenterInfoEditButton();
    }

    @Then("user tap on save button")
    public void user_tap_on_save_button() {
        bookingConfirmation.clickOnSaveButton();
    }

    @Then("user can sees phone number input value is {string}")
    public void user_can_sees_phone_number_input_value_is_phoneNumber(String inputValue) {
        assertEquals(bookingConfirmation.getPnoneNumberAttributeStatus("text"), inputValue, "Input value is not " + inputValue + " or is can be input with alphabet character");
    }

    @Then("user can sees continue button is disabled")
    public void user_can_sees_continue_button_is_disabled() {
        assertEquals(bookingFillform.getContinueButtonStatus("enabled"), "false", "Continue button is clickable or enabled is true");
    }

    @Then("user can sees submit booking button is disabled")
    public void user_can_sees_submit_booking_button_is_disabled() {
        assertEquals(bookingConfirmation.getSubmitBookingButtonStatus("enabled"), "false", "Submit booking button enabled is true");
    }

    @Then("user see the identity on Kost details")
    public void user_see_the_identity_on_kost_details() throws InterruptedException {
        Assert.assertTrue(bookingDetails.isKostNamePresent(), "Kost name is not present");
        Assert.assertTrue(bookingDetails.isRoomGenderPresent(), "Kost gender is not present");
        Assert.assertTrue(bookingDetails.isKostRatingPresent(), "Kost rating is not present");
        Assert.assertTrue(bookingDetails.isRoomAvailabilityPresent(), "Room availability is not present");
        Assert.assertTrue(bookingDetails.isRoomLastUpdatePresent(), "Kost last update information is not present");
    }

    @And("user scroll down to owner profile section")
    public void user_scroll_down_to_owner_profile_section() {
        bookingDetails.scrollToOwnerSection();
    }

    @And("user see the elements on owner section")
    public void user_see_the_elements_on_owner_section() {
        Assert.assertTrue(bookingDetails.isOwnerNamePresent(), "Owner name is not present");
        Assert.assertTrue(bookingDetails.isOwnerStatusPresent(), "Owner status is not present");
        Assert.assertTrue(bookingDetails.isActiveBookingTextPresent(), "Active booking information is not present");
        Assert.assertTrue(bookingDetails.isOwnerPicturePresent(), "Owner picture is not present");
        Assert.assertTrue(bookingDetails.isDetailStatisticButtonPresent(), "Learn Staistic button is not present");
        Assert.assertTrue(bookingDetails.isChatButtonPresent(), "Chat button is not present");
    }

    @Then("user verify the wording on owner statistic")
    public void user_verify_the_wording_on_owner_statistic(List<String> elements) {
        for (String i : elements) {
            Assert.assertTrue(filter.isWordingElementPresent(i), i + "is not present");
        }
    }

    @And("user scroll down to kos rule section")
    public void user_scroll_down_to_kos_rule_section() {
        bookingDetails.scrollToKosRuleSection();
    }

    @Then("user can see kos rule list on detail kos rule")
    public void user_can_see_kos_rule_list_on_detail_kos_rule() {
        bookingDetails.isKosRuleListPresent();
        bookingDetails.clickSeeAllKosRuleButton();
        bookingDetails.isKosRuleMainImagePresent();
        bookingDetails.isKosRuleImagePresent();
        bookingDetails.isKosRuleListPresent();
        bookingDetails.backToDetailPageFromKosRule();
    }

    @And("user can see kos rule image on detail kos")
    public void user_can_see_kos_rule_image_on_detail_kos() {
        bookingDetails.isKosRuleMainImagePresent();
        bookingDetails.isKosRuleImagePresent();
        bookingDetails.clickKosRuleImg();
    }

    @And("user swipe the image of kos rule")
    public void user_swipe_the_image_of_kos_rule() {
        bookingDetails.isCloseButtonPresent();
        bookingDetails.isPhotosCountPresent();
        bookingDetails.isPhotoRoomFullPresent();
        bookingDetails.isPhotoThumbnailPresent();
        bookingDetails.swipeLeftKosRuleImg();
        bookingDetails.swipeRightKosRuleImg();
    }

	@When("user taps see all price button")
	public void user_taps_see_all_price_button() {
		bookingDetails.clickOnSeeAllPriceLinkText();
	}

    @And("user select report category and write the description report {string}")
    public void user_select_report_category_and_write_the_description_report(String text) {
        bookingDetails.selectReportCategory();
        bookingDetails.inputTextReportDesc(text);
        bookingDetails.clickSubmitReport();
    }

    @Then("FTUE Booking Benefit appear with slides")
    public void ftue_booking_benefit_appear_with_slides() {
        Assert.assertTrue(bookingDetails.isFTUEText1Present(), "Text 1 is not present!");
        popup.clickContinueBtn();
        Assert.assertTrue(bookingDetails.isFTUEText2Present(), "Text 2 is not present!");
        popup.clickContinueBtn();
        Assert.assertTrue(bookingDetails.isFTUEText3Present(), "Text 3 is not present!");
        popup.clickContinueBtn();
        Assert.assertTrue(bookingDetails.isFTUEText4Present(), "Text 4 is not present!");
        popup.clickContinueBtn();
        Assert.assertTrue(bookingDetails.isFTUEText5Present(), "Text 5 is not present!");
        popup.clickContinueBtn();
        Assert.assertTrue(bookingDetails.isFTUEText6Present(), "Text 6 is not present!");
    }

    @And("user click Saya Mengerti button FTUE")
    public void user_click_saya_mengerti_button_FTUE() throws InterruptedException {
        popup.clickContinueBtn();
    }

    @Then("tenant can sees title with text {string} subtitle with text {string}")
    public void tenant_can_sees_title_with_text_subtitle_with_text(String title, String subtitle) {
        Assert.assertTrue(kostDetail.isTitle(title));
        Assert.assertTrue(kostDetail.isSubtitle(subtitle));
    }

    @When("tenant taps on payment estimation")
    public void tenant_taps_on_payment_estimation() {
        kostDetail.tapOnPaymentEstimation();
    }

    @Then("tenant can sees estimate detail payment prices list below :")
    public void tenant_can_sees_estimate_detail_payment_prices_list_below(List<String> list) {
        for (String s : list) {
            Assert.assertTrue(kostDetail.isElementWithText(s));
        }
    }

    @When("user taps on element with text {string}")
    public void user_taps_on_down_payment_list_button(String text) {
        kostDetail.tapOnElemenetWithText(text);
    }

    @Then("tenant can sees down payment detail")
    public void tenant_can_sees_down_payment_detail() {
        Assert.assertTrue(kostDetail.isUangMukaDPPresent("Uang Muka (DP)"));
        Assert.assertEquals(kostDetail.getDownPaymentPrice(), 400000);
        Assert.assertTrue(kostDetail.isBiayaLayananDPPresent("Biaya Layanan Mamikos"));
        Assert.assertTrue(kostDetail.isElementWithText("Total Pembayaran Pertama (DP)"));
    }

    @When("tenant taps on tutup button")
    public void tenant_taps_on_tutup_button() {
        kostDetail.tapsOnCloseButtonPaymentDetail();
    }

    @Then("tenant can sees repayment detail")
    public void tenant_can_sees_repayment_detail() {
        Assert.assertTrue(kostDetail.isElementWithText("Pembayaran Pertama (Pelunasan)"));
        Assert.assertTrue(kostDetail.isUangMukaDPPresent("Pelunasan"));
        Assert.assertEquals(kostDetail.getRepaymentPrice(), 1600000);
        Assert.assertTrue(kostDetail.isBiayaLayananDPPresent("Biaya Layanan Mamikos"));
        Assert.assertTrue(kostDetail.isElementWithText("Total Pembayaran Pertama (Pelunasan)"));
    }

    @Then("tenant can sees additional price detail")
    public void tenant_can_sees_additional_price_detail() {
        Assert.assertTrue(kostDetail.isUangMukaDPPresent("Biaya Tambahan"));
        Assert.assertTrue(kostDetail.isElementWithText("Cuci Kaki"));
        Assert.assertTrue(kostDetail.isElementWithText("Total Biaya Tambahan"));
    }

    @When("tenant taps on Ajukan Sewa button")
    public void tenant_taps_on_Ajukan_Sewa_button() throws InterruptedException {
        kostDetail.tapOnAjukanSewa();
    }

    @When("tenant chooses today date and taps on booking button")
    public void tenant_chooses_today_date_and_taps_on_booking_button() {
        kostDetail.chooseBookingDate("today");
    }

    @Then("tenant can sees down payment rule on booking form")
    public void tenant_can_sees_down_payment_rule_on_booking_form() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            appium.scrollToElementByText("Total biaya sewa kos");
            Assert.assertTrue(kostDetail.isElementWithText("Biaya sewa kos"));
            kostDetail.scrollToRincianPembayaranPage();
        } else {
            kostDetail.scrollToRincianPembayaranPage();
        }
        Assert.assertTrue(kostDetail.isElementWithText("Total pembayaran pertama"));
        Assert.assertTrue(kostDetail.isElementWithText("Biaya layanan Mamikos"));
        }

    @When("tenant reach Ringkasan Pengajuan Sewa section from pengajuan sewa")
    public void tenant_reach_Ringkasan_Pengajuan_Sewa_section() throws InterruptedException {
        int maxLoop = 0;
        do {
            bookingFillform.clickOnContinueButton();
            if (maxLoop == 2) {
                break;
            }
            maxLoop++;
        }
        while (!bookingFillform.isInRingkasanBooking());
    }

    @Then("tenant can sees down payment information on Ringkasan Pengajuan Sewa")
    public void tenant_can_sees_down_payment_information_on_Ringkasan_Pengajuan_Sewa() throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Total Pembayaran Pertama (DP)");
            Assert.assertTrue(kostDetail.isElementWithText("Total Pembayaran Pertama (DP)"));
        } else {
            kostDetail.scrollToRincianPembayaranDPPage();
            Assert.assertTrue(kostDetail.isElementWithText("Total pembayaran  pertama (DP)"));
        }
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Uang muka (DP)");
        }
        Assert.assertTrue(kostDetail.isElementWithText("Uang muka (DP)"));
        Assert.assertTrue(kostDetail.isUangMukaDPPresent("Biaya layanan Mamikos"));
        Assert.assertEquals(bookingConfirmation.getTitlePercenDPText(), "20% dari harga sewa");

        //get DP price
        String value = "Rp400.000";
        String getDownPaymentPreviewPrice = (bookingConfirmation.getDownPaymentPreviewPrice(value));
        assertEquals(value, getDownPaymentPreviewPrice);

        //payment price
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Total Pembayaran Pertama (DP)");
        }
        String paymentPrice = "Rp401.000";
        String getFirstPaymentPrice = (bookingConfirmation.getFirstPaymentPrice(paymentPrice));
        assertEquals(paymentPrice, getFirstPaymentPrice);

        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Biaya sewa kos");
            Assert.assertTrue(kostDetail.isElementWithText("Biaya sewa kos"));
        } else {
            kostDetail.scrollToRincianPelunasanPage();
        }

        //repayment part assertion
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.scrollToElementByText("Rincian pelunasan");
        }
        Assert.assertTrue(kostDetail.isElementWithText("Rincian pelunasan"));
        if (Constants.MOBILE_OS == Platform.ANDROID){
            Assert.assertTrue((kostDetail.isElementWithText("Sisa pelunasan")));
        } else {
            Assert.assertTrue((kostDetail.isElementWithText("Sisa Pelunasan")));
        }
        Assert.assertTrue(kostDetail.isElementWithText("Deposit"));
        Assert.assertTrue(kostDetail.isBiayaLayananDPPresent("Cuci Kaki"));
        Assert.assertTrue(kostDetail.isBiayaLayananDPPresent("Biaya layanan Mamikos"));

        //get payment price
        String getPaymentPrice = "Rp1.600.000";
        String getRepaymentPrice = (bookingConfirmation.getRepaymentPrice(getPaymentPrice));
        assertEquals(getPaymentPrice, getRepaymentPrice);

        //total payment price
        String totalRepaymentPrice = "Rp2.301.000";
        String getTotalRepaymentPrice = (bookingConfirmation.getTotalRepaymentPrice(totalRepaymentPrice));
        assertEquals(totalRepaymentPrice, getTotalRepaymentPrice);
    }

    @Then("user redirected to Pengajuan Sewa Form")
    public void user_redirected_to_pengajuan_sewa_form(){
        Assert.assertTrue(kostDetail.isElementWithText("Pengajuan Sewa"));
        Assert.assertTrue(kostDetail.isElementWithText("Informasi penyewa"));
        Assert.assertTrue(kostDetail.isElementWithText("Jumlah penyewa"));
    }

    @Then("user redirected to Detail Kost")
    public void user_redirected_to_detail_kost(){
        popup.goThroughBookingPopUps();
        Assert.assertTrue(kostDetail.isDetailKostPresent("Estimasi pembayaran"));
    }

    @Then("user redirected to Pembayaran page")
    public void user_redirected_to_pembayaran_page() {
        Assert.assertTrue(kostDetail.pembayaranPageIsPresent(), "Pembayaran page is not present");
    }

    @And("user tap on lihat status pengajuan booking button")
    public void user_tap_on_lihat_status_pengajuan_booking_button() throws InterruptedException{
        bookingDetails.clickOnLihatStatusPengajuanButton();
    }

    @When("user taps on element with text {string} on detail pembayaran")
    public void user_taps_on_down_payment_list_button_on_detail_pembayaran(String text) {
        kostDetail.tapOnElemenetWithTextByName(text);
    }
}
