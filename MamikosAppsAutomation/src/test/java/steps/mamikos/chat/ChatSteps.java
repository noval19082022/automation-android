package steps.mamikos.chat;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.chat.ChatPagePO;
import pageobjects.mamikos.owner.chat.OwnerChatListPO;
import pageobjects.mamikos.tenant.booking.BookingDetailsPO;
import pageobjects.mamikos.tenant.chat.ContactAdsPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.tenant.detail.KostDetailsPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.List;


public class ChatSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private BookingDetailsPO kosDetail = new BookingDetailsPO(driver);
    private ContactAdsPO contactAds = new ContactAdsPO(driver);
    private ChatPagePO chat = new ChatPagePO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private OwnerChatListPO ownerChat = new OwnerChatListPO(driver);
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private FooterPO menu = new FooterPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private LoadingPO loading = new LoadingPO(driver);

    private KostDetailsPO kostDetail = new KostDetailsPO(driver);

    private LoadingPO loading = new LoadingPO(driver);


    @When("user tap chat button in kos detail")
    public void user_tap_chat_button_in_kos_detail() throws InterruptedException {
        kosDetail.tapChat();
        contactAds.waitEnterPage();
    }

    @When("user select message {string}")
    public void userSelectMessage(String chat) {
        contactAds.tapRadioBtn(chat);
    }

    @And("user tap Send")
    public void userTapSend() {
        contactAds.tapBooking();
        try {
            popUp.clickOnLaterButton();
        }
        catch (Exception e) {

        }
    }

    @Then("user see text {string} and autotext from CS Mamikos like below in chat page")
    public void userSeeTextAndAutotextFromCSMamikosLikeBelowInChatPage(String expect, List<String> autotext) {
        String chatMessage = chat.getMessage(2);
        Assert.assertEquals(chatMessage, expect, "Chat that appear is different");
        String CStext = chat.getMessage(1).replaceAll("\\s", "").trim();
        Assert.assertEquals(CStext, autotext.get(0).replaceAll("\\s", "").trim(),
                "CS Chat that appear is different");
    }

    @And("user enter text {string} in chat page")
    public void userEnterTextInChatPage(String message) {
        chat.enterText(message);
    }

    @And("user navigates to main page after chat")
    public void userNavigatesToMainPageAfterChat() throws InterruptedException {
        if(Constants.MOBILE_OS== Platform.ANDROID) {
            for (int i = 0; i < 6; i++) {
                appium.back();
            }
        }
        else {
            chat.clickBackFromChat();
            try {
                contactAds.clickBackFromContactAds();
                kosDetail.tapBack();
                searchListing.clickBack();
                searchListing.clickBack();
                appium.hardWait(3);
            }
            catch (Exception e) {
                menu.clickOnOwnerHomeMenu();
            }
        }
    }

    @And("^(?:owner|tenant) user tap latest chat")
    public void XUserTapLatestChat() throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        }
        loading.waitLoadingFinish();
        ownerChat.tapChatFromList(0);
        try {
            popUp.clickOnLaterButton();
        }
        catch (Exception e) {

        }
    }

    @Then("user should see owner/tenant chat message : {string}")
    public void userShouldSeeXChatMessage(String chatTenant) throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            loading.waitLoadingFinish();
            appium.scrollToElementByText("What's your phone number?");
           // Assert.assertEquals(chat.getChatMessage(),chatTenant, "Chat from tenant is different/not appear");
            Assert.assertTrue(chat.getChatMessage(), "What's your phone number?");
        } else {
            appium.scrollToElementByText("What's your phone number?");
            String lastChat = chat.getMessage(0);
            Assert.assertEquals(lastChat,chatTenant, "Chat from tenant is different/not appear");
        }
    }

    @Then("verify chat group gets open between {string}")
    public void verifyChatGroupGetsOpenBetween(String chatGroupName) {
        Assert.assertEquals(chat.getChatGroupName(), chatGroupName, "Chat group name is not match");
    }

    @Then("tenant can sees chat {string} is selected")
    public void tenant_can_sees_chat_is_selected(String chatText) {
        Assert.assertEquals(contactAds.getSelectedRadioButtonChatText(), chatText, "Selected message is not " + chatText);
    }

    @Then("tenant can sees send button is Booking")
    public void tenant_can_sees_button_is_booking() {
        appium.scrollDown(1, 1);
        Assert.assertTrue(contactAds.isSendMessageButtonContainBooking(), "Send button is not contain \"BOOKING\"");
    }

    @Then("user see empty background image")
    public void user_see_empty_background_image() {
        Assert.assertTrue(chat.isEmptyChatImageAppear(), "Empty chat image is not appear");
    }

    @Then("user see text {string} in chat list page")
    public void user_see_text_in_chat_list_page(String text) throws InterruptedException {
        popUp.clickOnLaterButton();
        Assert.assertEquals(chat.getTextSubitleChat(), text, "chat message is not appear/wrong");
    }

    @Then("user see phone number field and selectable question options :")
    public void user_see_phone_number_field_and_selectable_question_options(List<String> questions) {
        List<String> questionsList = chat.listQuestions();
        for (int i=0; i<questions.size(); i++) {
            String expect = questions.get(i).replaceAll("\\s", "");
            Assert.assertEquals(questionsList.get(i), expect, "Question " + i + " not match");
        }
    }

    @And("user select question {string}")
    public void userSelectQuestion(String text) {
        chat.clickQuestion(text);
        chat.clickSend();
    }

    @Then("chat room appear with latest message {string}")
    public void chat_room_appear_with_latest_message(String message) throws InterruptedException {
        String lastChat = chat.getLatestChatText().replaceAll("\\s", "");
        String expected = message.replaceAll("\\s", "");
        Assert.assertTrue(lastChat.contains(expected), "Auto reply text is wrong");
    }

    @Then("user redirect to chat room")
    public void user_redirect_to_chat_room() {
        popUp.clickOnLaterButton();
        Assert.assertTrue(chat.isChatTitleAppers(), "Chat room is not appear");
    }

    @When("tenant tap Ajukan Sewa button on chat page")
    public void tenant_tap_ajukan_sewa_button_on_chat_page(){
        chat.clickAjukanSewa();
        if (Constants.MOBILE_OS == Platform.ANDROID){
            kostDetail.chooseBookingDate("today");
        }
    }

    @When("user search chat with value {string}")
    public void user_search_chat_with_value(String search) throws InterruptedException {
            popUp.clickOnLaterButton();
            chat.enterTextToSearchTextbox(search);
        }

    @When("user tap chat in chatlist")
    public void user_tap_chat_in_chatlist() throws InterruptedException {
        chat.tapChatInChatlist();
    }

    @Then("user see message chat is not found")
    public void user_see_message_chat_is_not_found() throws InterruptedException {
        Assert.assertEquals(chat.getTextTitleChat(), "Chat tidak ditemukan","Text is not equal to expected result!");
        Assert.assertEquals(chat.getTextSubitleChat(), "Coba ganti kata kunci Anda atau ganti filter pencarian.", "Text is not equal to expected result!");
    }

    @Then("user see available chat {string} on list chat")
    public void user_see_available_chat_on_list_chat(String kostName) throws InterruptedException {
        Assert.assertTrue(chat.isElementWithText(kostName));
        if (Constants.MOBILE_OS == Platform.ANDROID){
            Assert.assertEquals(chat.getTextTitleChat().trim(),kostName);
        }
    }

    @When("user click filter button on chat menu and select {string}")
    public void user_click_filter_button_on_chat_menu_and_select(String filter) throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        }
        chat.clickFilterChat();
        chat.selectFilterChat(filter);
        chat.clickTampilkanButton();
    }

    @Then("user redirected to survey kos")
    public void user_redirected_to_survet_kos() throws InterruptedException {
        Assert.assertTrue(chat.isElementWithText("Formulir survei kos"));
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            Assert.assertTrue(chat.surveyTextBoxEnable(chat.surveyTextBox(2)));
            Assert.assertEquals(chat.getSurveyTextBox(chat.surveyTextBox(3)),"Pilih tanggal survei kos");
            Assert.assertEquals(chat.getSurveyTextBox(chat.surveyTextBox(4)),"Masukkan waktu survei kos");
        } else {
            Assert.assertTrue(chat.isElementWithText("Nama pencari kos"));
            Assert.assertTrue(chat.isElementWithText("Nama Kos"));
            Assert.assertTrue(chat.isElementWithText("Tanggal survei kos"));
            Assert.assertTrue(chat.isElementWithText("Waktu survei"));
        }

    }

    @When("user scroll up to find chat {string} and down to chat {string}")
    public void user_scroll_up_and_down_the_chat(String upChat, String downChat) throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
            appium.hardWait(3);
            appium.scrollToElementContainsByText(upChat);
            Assert.assertTrue(chat.isElementWithText(upChat), "LText not present!");
            appium.hardWait(3);
            appium.scrollToElementContainsByText(downChat);
            Assert.assertTrue(chat.isElementWithText(downChat), "Text not present!");
        }else {
            appium.hardWait(3);
            chat.isChatTitlePresentAfterScrollDown(downChat);
            appium.hardWait(2);
            chat.isChatTitlePresentAfterScrollUp(upChat);
        }
    }

    @When("user scroll up to find chat {string} and scroll down on chat form")
    public void user_scroll_up_and_down_the_chat_on_chat_form(String upChat) throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID){
            appium.hardWait(3);
         //   appium.scrollToElementContainsByText(upChat);
            int counter = 0;
            while (counter < 10) {
                appium.scroll(0.5, 0.30, 0.70, 1000);
                counter++;
            }
            Assert.assertTrue(chat.isElementWithText(upChat), "Text not present!");
            appium.hardWait(5);
        } else {
            appium.tap(0,220);
            chat.isChatTitlePresentAfterScrollUp(upChat);
            appium.hardWait(2);
            appium.scroll(0.5, 0.8, 0.2, 2000);
        }
    }

    @When("owner click broadcast icon on list chat page")
    public void owner_click_broadcast_icon_on_list_chat() {
        if (Constants.MOBILE_OS == Platform.ANDROID){
            chat.clickBroadcastButton();
        }
    }

    @Then("user redirected to broadcast page")
    public void user_redirected_to_broadcast_page() throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID){
            Assert.assertTrue(chat.isElementWithText("Broadcast Chat"));
        }
    }

    @Then("user redirected to update kos page")
    public void user_redirected_to_update_kos_page() throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            Assert.assertTrue(chat.isElementWithText("Edit Data Kos"));
        } else {
            Assert.assertTrue(chat.isElementWithText("Edit data kos Anda"));
        }
    }
    @Then("user redirect to chat list room")
    public void user_redirect_to_chat_list_room() {
        popUp.clickOnLaterButton();
        Assert.assertTrue(chat.isChatListTitleAppers(), "Chat room is not appear");
    }
}