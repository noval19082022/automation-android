package steps.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.chat.ChatPagePO;
import pageobjects.mamikos.owner.booking.RoomNumberPO;
import utilities.Constants;
import utilities.ThreadManager;

public class FooterSteps {
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private FooterPO footer = new FooterPO(driver);
	private PopUpPO popUp = new PopUpPO(driver);
	private LoadingPO loading = new LoadingPO(driver);
	private ChatPagePO chat = new ChatPagePO(driver);

	//Tenant
	@When("user clicks on Login option on Menu")
	public void user_clicks_on_Login_option_on_Menu() {
		footer.clickOnLoginMenu();
	}

	@When("user navigates to Explore page")
	public void user_navigates_to_Explore_page() throws InterruptedException {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
			footer.clickOnExploreMenu();
		}else {
			footer.clickOnExploreMenu();
		}
	}

	@And("user navigates to profile page")
	public void user_navigates_to_profile_page() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
			footer.clickOnLoginMenu();
		} else {
			footer.clickOnProfileMenu();
		}
	}

	@Given("user as owner navigates to profile page")
	public void user_as_owner_navigates_to_profile_page() throws InterruptedException {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
		}
		footer.clickOnProfileMenu();
	}

	@And("user navigates to Favorite page")
	public void user_navigates_to_favorite_page() throws InterruptedException {
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
			footer.clickOnFavoriteMenu();
		}else {
			footer.clickOnFavoriteMenu();
		}
	}

	@And("user navigates to Chat page")
	public void user_navigates_to_chat_page() throws InterruptedException {
		footer.clickOnChatMenu();
	}

	@And("owner navigates to Chat page")
	public void owner_navigates_to_chat_page() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
		}
		footer.clickOnChatMenuOwner();
	}

	@And("user navigates to Booking Tenant page")
	public void user_navigates_to_booking_tenant_page() {
		footer.clickOnBookingTenantMenu();
	}

	@And("user navigates to Login page")
	public void user_navigates_to_login_page() {
		footer.clickOnLoginMenu();
	}

	//Owner
	@And("owner user navigates to profile page")
	public void owner_user_navigates_to_profile_page() throws InterruptedException {
		footer.clickOnOwnerProfileMenu();
		loading.waitLoadingFinish();
		popUp.closeInstantBookingTooltipInProfile();
	}

	@And("user navigates to Booking page")
	public void user_navigates_to_Booking_page() {
		footer.clickOnOwnerBookingMenu();
	}

	@When("owner user navigates to chat page")
	public void owner_user_navigates_to_chat_page() throws InterruptedException {
		footer.clickOnOwnerChatMenu();
	}

	@When("owner user navigates to manage page")
	public void owner_user_navigates_to_manage_page() throws InterruptedException {
		footer.clickOnOwnerManageMenu();
		loading.waitLoadingFinish();
	}

	@And("owner user navigates to home page")
	public void owner_user_navigates_to_home_page() {
		footer.clickOnOwnerHomeMenu();
	}

	@Then("user verify bottom bar should be shown with all the tabs")
	public void userVerifyBottomBarShouldBeShownWithAllTheTabs() {
		Assert.assertTrue(footer.isExploreMenuDisplayed(), "Explore menu tab is not shown");
		Assert.assertTrue(footer.isWishListMenuDisplayed(), "WishList menu tab is not shown");
		Assert.assertTrue(footer.isChatMenuDisplayed(), "Chat menu tab is not shown");
		Assert.assertTrue(footer.isLoginMenuDisplayed(), "Login menu tab is not shown");
	}

	@Then("tenant can sees profile icon")
	public void tenant_can_sees_profile_icon() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
		}
		Assert.assertTrue(footer.getProfileText().equalsIgnoreCase("Profil"));
	}

	@And("tenant user navigates to home page")
	public void tenant_user_navigates_to_home_page() {
		footer.clickOnTenantHomeMenu();
	}

	@Then("owner can sees profile icon")
	public void owner_can_sees_profile_icon() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
			Assert.assertTrue(footer.getProfileTextOwner().equalsIgnoreCase("Akun"));
		} else {
			Assert.assertTrue(footer.getProfileTextOwner().equalsIgnoreCase("Profil"));
		}
	}
}