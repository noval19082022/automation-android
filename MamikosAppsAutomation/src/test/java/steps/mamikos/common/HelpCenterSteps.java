package steps.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.HelpCenterPO;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.List;

public class HelpCenterSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private HelpCenterPO helpCenter = new HelpCenterPO(driver);

    @And ("verify help page should be open")
    public void VerifyHelpPageShouldBeOpen() {

        Assert.assertTrue(helpCenter.isHelpPageHeaderDisplayed(), "Title of Help page is not shown");
    }

    @And("verify different help option should be shown")
    public void VerifyDifferentHelpOptionShouldBeShown() {
        Assert.assertTrue(helpCenter.isWaHelpButtonDisplayed(), "WA help button is not shown");
        Assert.assertTrue(helpCenter.isCsHelpButtonDisplayed(), "CS help button is not shown");
        Assert.assertTrue(helpCenter.isHelpCenterDisplayed(), "Help Center button is not shown");

    }

    @Then("Click on I Understand button")
    public void clickOnIUnderstandButton() {
        helpCenter.clickOnOKButton();
    }

    @And("click on WA help button and verify below option should be shown")
    public void clickOnWAHelpButtonAndVerifyBelowOptionShouldBeShown(List<String> waOption) {
        helpCenter.clickOnWaHelpButton();
        for ( int i = 0; i < waOption.size() ; i++){
            Assert.assertTrue(helpCenter.isHelpOptionPresent(waOption.get(i)), waOption.get(i) + " is not present");
        }

    }

    @And("Click on back button to close popup")
    public void clickOnBackButtonToClosePopup() {
        helpCenter.clickOnCloseButton();
    }

    @And("click on CS help button and verify below option")
    public void clickOnCSHelpButtonAndVerifyBelowOption(List<String> csHelpOption) {
        helpCenter.clickOnCsHelpButton();
        for ( int i = 0; i < csHelpOption.size() ; i++){
            Assert.assertTrue(helpCenter.isHelpOptionPresent(csHelpOption.get(i)), csHelpOption.get(i) + " is not present");
        }
    }

    @Then("click on Help Center button and verify FAQ page gets open in web view")
    public void clickOnHelpCenterButtonAndVerifyFAQPageGetsOpenInWebView() throws InterruptedException {
        helpCenter.clickOnHelpCenterButton();
        Assert.assertTrue(helpCenter.isHelpCenterHeadingViewDisplayed(), "Page Heading is not shown");
    }

    @Then("owner can sees menu WA help button, CS help button, and Help Center Button")
    public void owner_can_sees_menu_wa_help_button_cs_help_button_and_help_center_button() {
        Assert.assertTrue(helpCenter.isWaHelpButtonDisplayed(), "WA help button is not present");
        Assert.assertTrue(helpCenter.isCsHelpButtonDisplayed(), "CS help button is not present");
        Assert.assertTrue(helpCenter.isHelpCenterDisplayed(), "Help Center button is not present");
    }
}
