package steps.mamikos.common;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.ThreadManager;

public class PopUpSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private AppiumHelpers appium = new AppiumHelpers(driver);
	private PopUpPO popUp = new PopUpPO(driver);
	private LoadingPO loading = new LoadingPO(driver);
    private SearchPO search = new SearchPO(driver);
	
	@And("user is on main page and click on Later option when asked on pop up")
	public void user_is_on_main_page_and_click_on_Later_option_when_asked_on_pop_up() throws InterruptedException {
		popUp.tapOnCloseButtonOwnerInvitation();
		if (Constants.MOBILE_OS== Platform.ANDROID) {
			loading.waitLoadingFinish();
			popUp.clickOnLaterButton();
		}else {
			popUp.clickContinueBtnIos();
		}
	}

	@And("user clicks on Cancel option verification when asked on pop up")
	public void userClicksOnCancelOptionVerificationWhenAskedOnPopUp() {
		popUp.clickOnCancelButton();
	}

	@And("user clicks on Close Email Verification when show on pop up")
	public void userClicksOnCloseEmailVerificationWhenShowOnPopUp() {
		popUp.clickOnCancelEmailVerifButton();
	}

	@And("user clicks on Close Owner Invitation Banner when show on pop up")
	public void userClicksOnCloseOwnerInvitationBannerWhenShowOnPopUp() {
		popUp.clickOnCloseOwnerInvitationPremium();
	}

	@And("user clicks on Close Instant Booking Tooltip when show on pop up")
	public void userClicksOnCloseInstantBookingTooltipWhenShowOnPopUp() {
		popUp.clickOnCloseInstantBookingTooltip();
	}

	@And("user Close Update Property Banner when show on pop up")
	public void userCloseUpdatePropertyBannerWhenShowOnPopUp() {
		popUp.dismissOwnerUpdatePropertyAdsPopup();
	}

    @Then("verify bluetooth popup should be shown")
    public void VerifyBluetoothPopupShouldBeShown() {
		Assert.assertTrue(popUp.isBluetoothPopupPresent(), "bluetooth popup is not present");
		appium.back();
    }

    @And("user click back if mamikos pop up survey appear")
    public void user_click_back_if_mamikos_pop_up_survey_appear() {
        if(popUp.isMamikosSurveyPresent()){
            search.clickDeviceBackButton();
        }
    }

    @Then("user see DEKATMU pop up announcement")
    public void user_see_DEKATMU_pop_up_announcement() {
	    popUp.isDownloadDEKATMUButtonPresent();
        Assert.assertTrue(popUp.checkDownloadDEKATMURedirection(), "Button DEKATMU is not redirection to playstore");
    }
}
