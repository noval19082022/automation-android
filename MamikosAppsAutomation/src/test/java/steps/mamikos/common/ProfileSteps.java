package steps.mamikos.common;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageobjects.mamikos.common.HelpCenterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.profile.ProfilePO;
import utilities.ThreadManager;;import java.util.List;

public class ProfileSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private ProfilePO profile = new ProfilePO(driver);
	private HelpCenterPO helpCenter = new HelpCenterPO(driver);
	private LoadingPO loading = new LoadingPO(driver);


	@Then("user sees {string} text beside History Booking option")
	public void user_sees_text_beside_History_Booking_option(String paymentReminderText) 
	{
		Assert.assertEquals(profile.getPaymentReminderText(), paymentReminderText,"Payment Reminder Text doesn't match");	
	}
	
	@When("user navigates to Booking History page")
	public void user_navigates_to_Booking_History_page() throws InterruptedException {
		profile.clickOnBookingHistorytButton();
	}

	@When("user navigates to Booking History page with reminder {string}")
	public void user_navigates_to_Booking_History_page_with_reminder(String reminder) throws InterruptedException {
		Assert.assertEquals(profile.getPaymentReminderText(), reminder,"Reminder text message doesn't match");
		profile.clickOnBookingHistorytButton();
	}

	@When("user click contract menu")
	public void user_click_contract_menu() {
		profile.clickOnContractMenu();
	}

	@When("user click on terms and service")
	public void user_click_on_terms_and_service() throws InterruptedException {
		profile.clickTermCondition();
		loading.waitLoadingFinish();
	}

	@Then("user redirect to {string} page")
	public void user_redirect_to_terms_and_service_page(String pageTitle) {
		Assert.assertTrue(helpCenter.isTextAppear(pageTitle), "Page is not in " + pageTitle);
	}

	@When("user click on privacy policy")
	public void user_click_on_privacy_policy() throws InterruptedException {
		profile.clickPrivacyPolicy();
		loading.waitLoadingFinish();
	}

	@When("user click on settings")
	public void user_click_on_settings() throws InterruptedException {
		profile.clickOnSettingsOption();
	}

	@And("user click notification dropdown")
	public void user_click_notification_dropdown() {
		profile.clickOnNotificationDropdown();
	}

	@And("user click {string} checkbox in settings page")
	public void user_click_checkbox_in_settings_page(String option) {
		profile.clickOnCheckboxSettings(option);
	}

	@Then("user login is {string}")
	public void user_login_is(String tenantName) {
		Assert.assertEquals(profile.getTenantName(), tenantName, "Tenant name not match");
	}

	@And("user click on bill tab menu")
	public void user_click_on_bill_tab_menu() {
		profile.clickOnBillsTabMenu();
	}

	@And("user click on pay button")
	public void user_click_on_pay_button() {
		profile.clickOnPaymentBillButton();
	}

	@Then("tenant verify display profile name")
	public void tenant_verify_display_profile_name(List<String> information) {
		Assert.assertEquals(profile.getTenantName(), information.get(0), "Tenant name is not equal to " +information.get(0));
		Assert.assertEquals(profile.getTenantPhone(), information.get(1), "Tenant phone number is not equal to " +information.get(1));
	}

	@When("user click kost saya button")
	public void user_click_kost_saya_button() throws InterruptedException {
		profile.clickKosSayaButton();
	}

	@When("system display page kos saya")
	public void system_display_page_kos_saya() throws InterruptedException {
		profile.isKosSayaPage();
	}
}
