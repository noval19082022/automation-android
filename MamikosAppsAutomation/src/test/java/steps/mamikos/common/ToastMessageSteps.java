package steps.mamikos.common;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import pageobjects.mamikos.common.ToastMessagePO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ToastMessageSteps 
{
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private ToastMessagePO toast =new ToastMessagePO(driver);
	
	//Test Data
	private String propertyFile="src/test/resources/testdata/mamikos/signin.properties";
	private String validationMessage = JavaHelpers.getPropertyValue(propertyFile,"validationMessage");
	
	@Then("correct validation message is displayed")
	public void correct_validation_message_is_displayed() 
	{
		Assert.assertTrue(toast.isToastMessageDisplayed(validationMessage), validationMessage + " - toast message is not displayed");
	}

	@Then("system display toast message {string}")
	public void system_display_toast_message(String message) {
		if (!toast.isToastMessageDisplayed(message)) {
			Assert.assertTrue(toast.verifyToastMessage(message), "Toast message " + message + " not appear");
		}
		else {
			Assert.assertTrue(toast.isToastMessageDisplayed(message),  "Toast message " + message + " not appear");
		}
	}
}
