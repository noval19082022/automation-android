package steps.mamikos.explore;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.appmanagement.ApplicationState;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.tenant.explore.ExplorePO;
import pageobjects.mamikos.tenant.search.ApartmentDetailPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.List;

public class ExploreSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ExplorePO explore = new ExplorePO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private PopUpPO popup = new PopUpPO(driver);

    @When("User click field search boarding house")
    public void user_click_field_search_boarding_house() throws InterruptedException{
        explore.clickOnSearchTextbox();
    }

    @Then("user sees main Homepage mobile app")
    public void user_sees_main_homepage_mobile_app() throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            Assert.assertTrue(explore.isNotificationIconDisplayed(), "Notification icon is not displayed!");
        }
        Assert.assertTrue(explore.isSearchTextBoxDisplayed(), "Search box is not displayed!");
        Assert.assertTrue(explore.isMamikosLogoDisplayed(), "Mamikos Logo is not displayed!");
        Assert.assertTrue(explore.isUsernameDisplayed(), "Username is not displayed!");
        Assert.assertTrue(explore.isKosImageDisplayed(), "Kos image is not displayed!");
        Assert.assertTrue(explore.isApartemenImageDisplayed(), "Apartemen image is not displayed!");
        Assert.assertTrue(explore.isGoodsandServiceImageDisplayed(), "Stuffs and Services is not displayed!");
        Assert.assertTrue(explore.isJobVacancyImageDisplayed(), "Job vacancy image is not displayed!");
        Assert.assertTrue(explore.isJobVacancyTextDisplayed(), "Job vacancy text is not displayed!");
        Assert.assertTrue(explore.isQRButtonDisplayed(), "QR Button is not displayed!");
        Assert.assertTrue(explore.isPromoBannerDisplayed(), "Promo banner is not displayed!");
        Assert.assertTrue(explore.isLoginOwnerBannerDisplayed(), "Login owner banner is not displayed!");
        Assert.assertTrue(explore.isLoginOwnerButtonDisplayed(), "Login owner button is not displayed!");
        Assert.assertTrue(explore.isFlashSaleWidgetDisplayed(), "FlashSale widget is not displayed!");
        Assert.assertTrue(explore.isTestimonialWidgetDisplayed(), "Testimonial widget is not displayed!");
        Assert.assertTrue(explore.isKosRecommendationDisplayed(), "Kos recommendation widget is not displayed!");
        Assert.assertTrue(explore.isKosPromotedDisplayed(), "Kos promoted is not displayed!");
    }

    @And("user sees main Homepage mobile app after login")
    public void user_sees_main_homepage_mobile_app_after_login() throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID) {
            Assert.assertTrue(explore.isNotificationIconDisplayed(), "Notification icon is not displayed!");
        }
        Assert.assertTrue(explore.isSearchTextBoxDisplayed(), "Search box is not displayed!");
        Assert.assertTrue(explore.isMamikosLogoDisplayed(), "Mamikos Logo is not displayed!");
        Assert.assertTrue(explore.isUsernameDisplayed(), "Username is not displayed!");
        Assert.assertTrue(explore.isKosImageDisplayed(), "Kos image is not displayed!");
        Assert.assertTrue(explore.isApartemenImageDisplayed(), "Apartemen image is not displayed!");
        Assert.assertTrue(explore.isGoodsandServiceImageDisplayed(), "Stuffs and Services is not displayed!");
        Assert.assertTrue(explore.isJobVacancyImageDisplayed(), "Job vacancy image is not displayed!");
        Assert.assertTrue(explore.isJobVacancyTextDisplayed(), "Job vacancy text is not displayed!");
        Assert.assertTrue(explore.isQRButtonDisplayed(), "QR Button is not displayed!");
        Assert.assertTrue(explore.isPromoBannerDisplayed(), "Promo banner is not displayed!");
        Assert.assertTrue(explore.isFlashSaleWidgetDisplayed(), "FlashSale widget is not displayed!");
        Assert.assertTrue(explore.isKosRecommendationDisplayed(), "Kos recommendation widget is not displayed!");
        Assert.assertTrue(explore.isKosPromotedDisplayed(), "Kos promoted is not displayed!");
    }

    @And("user click on Kos tab")
    public void userClickOnKosTab() {
        explore.clickOnKosTab();
    }

    @And("user click on Goods and Services tab")
    public void userClickOnGoodsAndServicesTab() {
        explore.clickOnGoodsAndServicesTab();
    }

    @And("user click on Job vacancy Tab")
    public void userClickOnJobVacancyTab() throws InterruptedException {
        explore.clickOnJobVacancyTab();
    }

    @Then("click on device home button and verify app should be closed")
    public void clickOnDeviceHomeButtonAndVerifyAppShouldBeClosed(){
        appium.home();
        Assert.assertEquals(appium.appStatus(), ApplicationState.RUNNING_IN_BACKGROUND, "Application state isn't correct");
    }

    @And("user click on see All Promoted Kos linked text from home page and click on city dropdown")
    public void userClickOnSeeAllPromotedKosLinkedTextFromHomePageAndClickOnCityDropdown() {
        explore.clickOnPromotedCityDropDown();
    }

    @Then("user verify city list")
    public void userVerifyCityList(List<String> cityName) {
        for ( int i = 0; i < cityName.size() ; i++){
            Assert.assertTrue(explore.isCityNamePresent(cityName.get(i)), cityName.get(i) + " is not present");
        }
    }

    @And("user click on Apartment Tab from home page")
    public void userClickOnApartmentTabFromHomePage() throws InterruptedException {
        loading.waitLoadingFinish();
        explore.clickOnApartmentTab();
    }

    @When("User clicks Notification Icon on Homepage")
    public void user_clicks_Notification_Icon_on_Homepage() {
        explore.clickOnNotificationIcon();
    }

    @Then("user verify {string} screen")
    public void user_verify_title_screen(String title) {
        Assert.assertEquals(explore.getTitleResultText(), title, "Title result is not equal to " + title);
        Assert.assertTrue(explore.isImageAvatarPresent(), "Image Avatar is not present");
        Assert.assertTrue(explore.isActionLoginButtonPresent(), "Action login button is not present");
    }

    @And("user click on Location button from home page and verify below location list")
    public void user_click_on_location_button_from_home_page_and_verify_below_location_list(List<String> locationName) throws InterruptedException {
        explore.clickOnLocationKosRecommendation();
        for ( int i = 0; i < locationName.size() ; i++){
            Assert.assertTrue(explore.isLocationNamePresent(locationName.get(i)), locationName.get(i) + " is not present");
        }
    }

    @Then("verify below list of kose should be shown")
    public void verifyBelowListOfKoseShouldBeShown(List<String> kost) {
        List<String> allKosts = explore.getListOfKosRoomForRecommendation(2);
        for (int i = 0; i < kost.size(); i++) {
            Assert.assertTrue(allKosts.contains(kost.get(i)), kost.get(i) + " is not present");
        }
    }

    @Then("verify below list of kose should be shown city {string}")
    public void verify_below_list_of_kose_should_be_shown_city(String city) throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            loading.waitLoadingFinish();
            List<String> addressList = explore.listKostAddress();
            for (String a : addressList) {
                Assert.assertTrue(a.toLowerCase().contains(city.toLowerCase()), "Search result " + a + " not in correct location");
            }
        }else {
            Assert.assertTrue(explore.isKostAreaMalangPresent(),"kost at Malang is not present");
        }
    }

    @And("user click on location button and select {string}")
    public void userClickOnLocationButtonAndSelect(String selectLocation) throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            loading.waitLoadingFinish();
        }
        explore.clickOnLocationKosRecommendation();
        explore.selectLocation(selectLocation);
    }

    @Then("verify recommendation kos has kos image and contains")
    public void verify_recommendation_kos_has_kos_image_and_contains() throws InterruptedException{
        Assert.assertTrue(explore.isRoomImageViewDisplayedForRecommendation(), "Room image is not present");
        Assert.assertTrue(explore.isGenderRecomenKosPresent(), "Gender label is not present");
        Assert.assertTrue(explore.isPriceRecomenKosPresent(), "Price label is not present");
        Assert.assertTrue(explore.isKosNameRecomenKosPresent(), "Kos Name label is not present");
        Assert.assertTrue(explore.isLocRecomenKosPresent(), "Rating label is not present");
   }

    @And("Click on see all recommendation Kos linked")
    public void clickOnSeeAllRecommendationKosLinked() throws InterruptedException{
        explore.clickOnSeeAllRecommendationLinkedText();
        searchListing.clickFTUEKosListingPopUp();
    }

    @Then("user see the testimonial elements")
    public void user_see_the_testimonial_elements() throws InterruptedException {
        explore.scrollDownToTestimonial();
        if(Constants.MOBILE_OS == Platform.ANDROID){
            int counter = 0;
            while (counter < 2) {
                appium.scroll(0.5, 0.70, 0.30, 2000);
                counter++;
            }
        }
        Assert.assertTrue(explore.isTestimonialTitlePresent(), "Title not present!");
        if(Constants.MOBILE_OS == Platform.ANDROID){
            for(int i = 1; i <= explore.getTestimonialSize(); i++) {
                Assert.assertTrue(explore.isOwnerImgPresent(i), "Owner profile pic not present!");
                Assert.assertTrue(explore.isOwnerNamePresent(i), "Owner Name not present!");
                Assert.assertTrue(explore.isOwnerKosNamePresent(i), "Kos Name not present!");
                Assert.assertTrue(explore.isTestimonialDescPresent(i), "Testimonial desc not present!");
            }
        }
    }

    @Then("user scroll down to bottom page")
    public void user_scroll_down_to_bottom_page() {
        Assert.assertTrue(explore.isKosPromotedDisplayed(), "Testimonial desc not present!");
    }

    @Then("user sees the promo widget elements")
    public void user_sees_the_promo_widget_elements() {
        Assert.assertTrue(explore.isKosPromotedDisplayed(), "Kos promoted is not displayed!");
        Assert.assertTrue(explore.isPromotedLabelDisplayed(), "Promoted label is not displayed!");
        Assert.assertTrue(explore.isPromotedImageKosDisplayed(), "Promoted Image Kos is not displayed!");
        Assert.assertTrue(explore.isPromotedSponsoredDisplayed(), "Promoted Sponsored Icon is not displayed!");
        Assert.assertTrue(explore.isPromotedGenderDisplayed(), "Promoted Gender is not displayed!");
        Assert.assertTrue(explore.isPromotedKosNameDisplayed(), "Promoted Kos Name is not displayed!");
        Assert.assertTrue(explore.isPromotedKosLocDisplayed(), "Promoted Kos Location is not displayed!");
        Assert.assertTrue(explore.isPromotedKosPriceDisplayed(), "Promoted Kos Price is not displayed!");
    }

    @When("user goes to pusat bantuan page")
    public void user_goes_to_pusat_bantuan_page() {
        explore.tapOnPusatBantuanButton();
    }
}
