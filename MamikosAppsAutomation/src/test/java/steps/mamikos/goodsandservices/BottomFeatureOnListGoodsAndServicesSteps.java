package steps.mamikos.goodsandservices;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.goodsandservices.BottomFeatureOnGoodsAndServicesPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class BottomFeatureOnListGoodsAndServicesSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private BottomFeatureOnGoodsAndServicesPO bottomFeature = new BottomFeatureOnGoodsAndServicesPO(driver);

    private String goodAndServices = "src/test/resources/testdata/mamikos/marketingTechnology/goodsAndServices.properties";
    private String serverKayPay = Constants.SERVER_KEY + "_" + Constants.SERVER_PAY;
    private String defaultMinPrice = JavaHelpers.getPropertyValueForSquad(goodAndServices, "defaultMinPrice_" + serverKayPay);
    private String defaultMaxPrice = JavaHelpers.getPropertyValueForSquad(goodAndServices, "defaultMaxPrice_" + serverKayPay);

    @When("user click filter icon on page list goods and services")
    public void user_click_filter_icon_on_page_list_goods_and_services() {
        bottomFeature.clickOnFilterIcon();
    }

    @When("user click map icon on page list goods and services")
    public void user_click_map_icon_on_page_list_goods_and_services() {
        bottomFeature.clickOnMapIcon();
    }

    @When("user input price range min {string} and max {string} and click search button")
    public void user_input_price_range_min_and_max_and_click_search_button(String minPrice, String maxPrice) {
        bottomFeature.inputMinPrice(minPrice);
        bottomFeature.inputMaxPrice(maxPrice);
        bottomFeature.clickOnSearchButton();
    }

    @When("user input price range min {string} and max {string}")
    public void user_input_price_range_min_and_max(String minPrice, String maxPrice) {
        bottomFeature.inputMinPrice(minPrice);
        bottomFeature.inputMaxPrice(maxPrice);
    }

    @When("user click reset button on filter page for filter goods and services")
    public void user_click_reset_button_on_filter_page_for_filter_goods_and_services() {
        bottomFeature.resetButton();
    }

    @Then("system display default price range")
    public void system_display_default_price_range() {
        Assert.assertEquals(bottomFeature.getMinimumPriceRange(), defaultMinPrice, "Default minimum price range not match");
        Assert.assertEquals(bottomFeature.getMaximumPriceRange(), defaultMaxPrice, "Default maximum price range not match");
    }
}
