package steps.mamikos.goodsandservices;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.tenant.search.GoodAndServicesDetailsPO;
import utilities.ThreadManager;

public class GoodsAndServicesDetailsSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private GoodAndServicesDetailsPO details = new GoodAndServicesDetailsPO(driver);

    @When("user click back button arrow on page goods and services details")
    public void user_click_back_button_arrow_on_page_goods_and_services_details() {
        details.clickOnBackButtonArrow();
    }
}
