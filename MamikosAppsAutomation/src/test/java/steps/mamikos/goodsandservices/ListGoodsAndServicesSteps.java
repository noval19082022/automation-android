package steps.mamikos.goodsandservices;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.goodsandservices.BottomFeatureOnGoodsAndServicesPO;
import pageobjects.mamikos.goodsandservices.ListGoodsAndServicesPO;
import pageobjects.mamikos.tenant.search.GoodAndServicesDetailsPO;
import utilities.AppiumHelpers;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ListGoodsAndServicesSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private ListGoodsAndServicesPO list = new ListGoodsAndServicesPO(driver);
    private BottomFeatureOnGoodsAndServicesPO bottomFeature = new BottomFeatureOnGoodsAndServicesPO(driver);
    private GoodAndServicesDetailsPO details = new GoodAndServicesDetailsPO(driver);

    @Then("system display the {int} highest goods and services for price {string} to {string}")
    public void system_display_the_highest_goods_and_services_for_price_to(Integer numberOfList, String min, String max) throws InterruptedException {
        for (int i = 1 ; i <= numberOfList ; i++) {
            int price = JavaHelpers.extractNumber(list.getPriceGoodsAndServices());
            int maxPrice = Integer.parseInt(max);
            int minPrice = Integer.parseInt(min);
            Assert.assertTrue(price >= minPrice && price <= maxPrice, "Price (" + price + ") should be greater than or equal to Minimal Price (" + minPrice + ") &&  less than or equal to Maximal Price (" + maxPrice + ")");
            appium.scroll(0.5, 0.35, 0.20, 2000);
        }
    }

    @Then("system display content each list goods and services")
    public void system_display_content_each_list_goods_and_services() {
        Assert.assertTrue(list.GoodsAndServicesPriceIsPresent(), "Title goods and services not present");
        Assert.assertTrue(list.GoodsAndServicesTitleIsPresent(), "Title goods and services not present");
        Assert.assertTrue(list.GoodsAndServicesLabelIsPresent(), "Title goods and services not present");
        Assert.assertTrue(list.GoodsAndServicesImageIsPresent(), "Title goods and services not present");
        Assert.assertTrue(bottomFeature.filterIconIsPresent(), "Title goods and services not present");
        Assert.assertTrue(bottomFeature.sortIconIsPresent(), "Title goods and services not present");
        Assert.assertTrue(bottomFeature.mapIconIsPresent(), "Title goods and services not present");
    }

    @Then("user swipe up and verify new List")
    public void user_swipe_up_and_verify_new_List() {
        String bottomList = list.getTitleGoodsAndServices();
        appium.scroll(0.5, 0.8, 0.2, 2000);
        appium.scroll(0.5, 0.8, 0.2, 2000);
        Assert.assertNotEquals(list.getTitleGoodsAndServices(), bottomList, "Goods and services title is equals to before scroll");
    }

    @Then("user swipe down and verify new List")
    public void user_swipe_down_and_verify_new_List() {
        String bottomList = list.getTitleGoodsAndServices();
        appium.scroll(0.5, 0.2, 0.8, 2000);
        appium.scroll(0.5, 0.2, 0.8, 2000);
        Assert.assertNotEquals(list.getTitleGoodsAndServices(), bottomList, "Goods and services title is equals to before scroll");
    }
}
