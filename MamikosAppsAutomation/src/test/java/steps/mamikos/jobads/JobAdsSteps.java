package steps.mamikos.jobads;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.owner.jobads.JobAdsCompanyNamePO;
import pageobjects.mamikos.owner.jobads.JobAdsEditMenuPO;
import pageobjects.mamikos.owner.jobads.JobAdsPersonInChargePO;
import pageobjects.mamikos.owner.jobads.JobAdsSalaryPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class JobAdsSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private JobAdsCompanyNamePO adsCompany = new JobAdsCompanyNamePO(driver);
    private JobAdsSalaryPO adsSalary = new JobAdsSalaryPO(driver);
    private JobAdsPersonInChargePO adsPerson = new JobAdsPersonInChargePO(driver);
    private JavaHelpers java = new JavaHelpers();
    private JobAdsEditMenuPO editMenu = new JobAdsEditMenuPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private String randomText;


    @And("user click on Vacancy Tab")
    public void user_click_on_Vacancy_Tab() throws InterruptedException {
        adsCompany.clickOnVacancyOwnerTab();
        loading.waitLoadingFinish();
    }

    @And("user click Later on Iklan Saya Menu")
    public void user_click_Later_on_Iklan_Saya_Menu(){
        adsCompany.clickOnLaterButton();
    }

    @And("user input their Company Name {string} and Company Address {string}")
    public void user_input_their_Company_Name_and_Company_Address(String companyName, String companyAddress) throws InterruptedException {
        adsCompany.fillForm(companyName, companyAddress);
        adsCompany.clickOnSubmitButton();
    }

    @Then("user input their Job Name {string} and random name, Minimum Study {string}, Job Status {string}, Job Description {string}, Minimum Salary {string}, Maximum Salary {string}, and Salary Type {string}")
    public void user_input_their_Job_Name_and_random_name_Minimum_Study_Job_Status_Job_Description_Minimum_Salary_Maximum_Salary_and_Salary_Type(String jobName, String minStudy, String jobStat, String jobDesc, String minSal, String maxSal, String salType) {
        randomText = " " + java.generateAlphanumeric(4);
        adsSalary.fillSecondForm(jobName + randomText,minStudy,jobStat,jobDesc,minSal,maxSal,salType);
        adsCompany.clickOnSubmitButton();
    }

    @And("user input their Person In Charge {string}, Phone Number {string}, Email {string} and Job Expired Date {int} days after")
    public void user_input_their_person_in_charge_phone_number_email_and_job_expired_date(String person, String phone, String email, Integer incrementDate) throws ParseException {
        Locale indonesian = new Locale("id", "ID");
        String pattern = "dd MMMM yyyy";
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "dd MMMM yyyy", incrementDate, 0, 0, 0);
        Date formatter = new SimpleDateFormat(pattern).parse(today);
        String expDate = java.changeLocalDate(formatter, pattern, indonesian);
        adsPerson.fillThirdForm(person, phone, email, expDate);
        adsCompany.clickOnSubmitButton();
    }

    @And("user click on Edit and Promote Label")
    public void user_click_on_Edit_and_Promote_Label() throws InterruptedException {
        adsPerson.clickOnEditPromoteLabel();
    }

    @Then("user verify Job Status {string}, Job Name {string}, Job Description {string} and random name, and Salary Status {string}")
    public void user_verify_Job_Status_Job_Name_Job_Description_and_random_name_and_Salary_Status(String jobStatus, String companyName, String jobName, String salaryStatus) {
        Assert.assertTrue(adsPerson.isCompanyNamePresent(), "Company name is not present");
        Assert.assertTrue(adsPerson.isJobNamePresent(), "Job name is not present");
        Assert.assertTrue(adsPerson.isJobStatusPresent(), "Job status is not present");
        Assert.assertTrue(adsPerson.isJobSalaryPresent(), "Salary status is not present");
        Assert.assertEquals(adsPerson.getCompanyNameText(), companyName, "Company name is not " + companyName);
        Assert.assertEquals(adsPerson.getJobNameText(), jobName + randomText, "Job name is not " + jobName);
        Assert.assertEquals(adsPerson.getJobStatusText(), jobStatus, "Job status is not " + jobStatus);
        Assert.assertEquals(adsPerson.getJobSalaryText(), salaryStatus, "Salary status is not " + salaryStatus);
    }

    @And("user click on New pop up")
    public void userClickOnNewPopUp() {
        adsCompany.clickOnNewPopUp();
    }

    @When("user click on Edit Ads Data")
    public void user_click_on_Edit_Ads_Data(){
        editMenu.clickOnEditAdsData();
    }

    @And("user click on Reactive Vacancy")
    public void user_click_on_Reactive_Vacancy(){
        editMenu.clickOnReactiveVacancyButton();
        editMenu.clickOnActionConfirmationPopUp();
    }

    @And("user click on Edit Vacancy Button")
    public void user_click_on_Edit_Vacancy_Button(){
        editMenu.clickOnEditVacancyButton();
    }

    @Then("user verify Vacancy Status should be {string}")
    public void user_verify_Vacancy_Status_should_be_Closed(String vacancyStatus) {
        Assert.assertTrue(editMenu.isOpenVacancyEnabled(), "Vacancy Status not closed yet");
        Assert.assertEquals(editMenu.getOpenVacancyText(), vacancyStatus,"Vacancy Status is not " + vacancyStatus);
    }

    @Then("user click on Delete to Delete Job Vacancy")
    public void user_click_on_Delete_to_Delete_Job_Vacancy() {
        editMenu.clickOnDeleteVacancyButton();
        editMenu.clickOnActionConfirmationPopUp();
        Assert.assertTrue(adsPerson.isCompanyNamePresent(), "Company Name still present");
        Assert.assertTrue(adsPerson.isJobNamePresent(), "Job Name still present");
        Assert.assertTrue(adsPerson.isJobSalaryPresent(), "Job Salary still present");
        Assert.assertTrue(editMenu.isMainContainerNull(), "Job Vacancy not deleted yet");
    }
}
