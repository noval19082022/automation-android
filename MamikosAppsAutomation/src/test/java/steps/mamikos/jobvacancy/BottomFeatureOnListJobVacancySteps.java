package steps.mamikos.jobvacancy;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.tenant.jobVacancies.BottomFeatureOnJobVacancyPO;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

public class BottomFeatureOnListJobVacancySteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private BottomFeatureOnJobVacancyPO list = new BottomFeatureOnJobVacancyPO(driver);

    @Given("user sort job vacancies {string}")
    public void user_sort_job_vacancies(String sort) throws InterruptedException {
        list.clickOnSortFeature();
        if (sort.toLowerCase().equals("via mamikos")){
            list.clickOnSortJobVacanciesViaMamikos();
        }else if (sort.toLowerCase().equals("lowongan terbaru")){
            list.clickOnSortJobVacanciesNewest();
        }else
            list.clickOnSortJobVacanciesLongest();
    }


    @When("user filter job vacancies by {string} and {string}")
    public void user_filter_job_vacancies_by_and(String jobVacancyType, String jobTypeValue) throws InterruptedException {
        list.clickOnFilterIcon();
        appium.hardWait(2);
        if (jobVacancyType.equals("Tipe Loker")){
            list.clickOnFieldJobVacancyType();
        }else if (jobVacancyType.equals("Edukasi")){
            list.clickOnFieldLastEducation();
        }else {
            list.clickOnFieldSort();
        }
        list.selectOnJobVacancyFilterValue(jobTypeValue);
        list.clickOnSearchButton();
    }
}
