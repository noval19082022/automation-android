package steps.mamikos.jobvacancy;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.tenant.jobVacancies.CompetencyOfJobApplicantFormPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class CompetencyOfJobApplicantFormSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private CompetencyOfJobApplicantFormPO competency = new CompetencyOfJobApplicantFormPO(driver);

    //Data Marketing Technology Squad
    private String jobVacancy = "src/test/resources/testdata/mamikos/marketingTechnology/jobVacancy.properties";
    private String serverKayPay = Constants.SERVER_KEY + "_" + Constants.SERVER_PAY;
    private String lastEducation = JavaHelpers.getPropertyValue(jobVacancy, "lastEducation_" + serverKayPay);
    private String skillAndAbilities = JavaHelpers.getPropertyValue(jobVacancy, "skillAndAbilities_" + serverKayPay);
    private String workExperience = JavaHelpers.getPropertyValue(jobVacancy, "workExperience_" + serverKayPay);
    private String lastSalary = JavaHelpers.getPropertyValue(jobVacancy, "lastSalary_" + serverKayPay);
    private String salaryExpectation = JavaHelpers.getPropertyValue(jobVacancy, "salaryExpectation_" + serverKayPay);

    @When("user input competency of job applicant and click continue button")
    public void user_input_competency_of_job_applicant_and_click_continue_button() {
        competency.inputLastEducation(lastEducation);
        competency.inputSkillAndAbilities(skillAndAbilities);
        competency.inputWorkExperience(workExperience);
        competency.inputLastSalary(lastSalary);
        competency.inputSalaryExpectations(salaryExpectation);
        competency.clickOnContinueButton();
    }

    @When("user click on arrow button on page competency of job applicant")
    public void user_click_on_arrow_button_on_page_competency_of_job_applicant() {
        competency.clickOnBacArrowButton();
    }
}
