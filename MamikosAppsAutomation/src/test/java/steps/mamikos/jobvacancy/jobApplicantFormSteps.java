package steps.mamikos.jobvacancy;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.tenant.jobVacancies.JobApplicantFormPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class jobApplicantFormSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private JobApplicantFormPO applicantForm = new JobApplicantFormPO(driver);

    //Data Marketing Technology Squad
    private String jobVacancy = "src/test/resources/testdata/mamikos/marketingTechnology/jobVacancy.properties";
    private String serverKayPay = Constants.SERVER_KEY + "_" + Constants.SERVER_PAY;
    private String name = JavaHelpers.getPropertyValue(jobVacancy, "name_" + serverKayPay);
    private String email = JavaHelpers.getPropertyValue(jobVacancy, "email_" + serverKayPay);
    private String phoneNumber = JavaHelpers.getPropertyValue(jobVacancy, "phoneNumber_" + serverKayPay);
    private String address = JavaHelpers.getPropertyValue(jobVacancy, "address_" + serverKayPay);

    @When("user input job applicant data and click continue button")
    public void user_input_job_applicant_data_and_click_continue_button() {
        applicantForm.inputName(name);
        applicantForm.inputEmail(email);
        applicantForm.inputPhoneNumber(phoneNumber);
        applicantForm.inputAddress(address);
        applicantForm.clickOnContinueButton();
    }
}
