package steps.mamikos.jobvacancy;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.jobVacancies.JobVacanciesListPO;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.Date;

public class jobVacanciesListSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private JobVacanciesListPO list = new JobVacanciesListPO(driver);

    @Then("system display job vacancies {string}")
    public void system_display_job_vacancies(String jobSort) throws ParseException {
        Date jobDate1 = null;
        Date jobDate2 = null;
        for (int i = 1; i <= 5; i++){
            if (jobSort.toLowerCase().equals("via mamikos")){
                Assert.assertTrue(list.verifyJobVacanciesViaMamikos(1), jobSort + " Not appeared");
            }else{
                jobDate1 = list.getJobVacancyOpenSince(1);
                if (jobDate2 != null){
                    if (jobSort.toLowerCase().equals("lowongan terbaru")){
                        Assert.assertTrue((jobDate1.compareTo(jobDate2) <= 0), "Job vacancies not sort newest");
                    }else {
                        Assert.assertTrue((jobDate1.compareTo(jobDate2) >= 0), "Job vacancies not sort longest");
                    }
                }
                jobDate2 = jobDate1;
            }
            appium.scroll(0.5, 0.50, 0.20, 2000);
        }
    }

    @When("user clicks on the top job on the list")
    public void user_clicks_on_the_top_job_on_the_list() {
        list.clickOnFirstJobVacancy();
    }

    @Then("system display job vacancies type {string}")
    public void system_display_job_vacancies_type(String filter) {
        for (int i = 1; i <= 5; i++){
            if (filter.equals("Freelance")){
                Assert.assertEquals(list.getFirstJobVacancyTypeLabel().toLowerCase(), filter.toLowerCase(), "Job vacancy type not match ");
            }
            appium.scroll(0.5, 0.50, 0.20, 2000);
        }
    }

    @Then("system display job vacancy list")
    public void system_display_job_vacancy_list() {
        list.verifyListJobVacancyAppeared();
    }

    @Then("system display job vacancies last education {string}")
    public void system_display_job_vacancies_last_education(String filter) {
        for (int i = 1; i <= 5; i++){
            if (filter.equals("Freelance")){
                Assert.assertEquals(list.getJobVacancyLastEducationEditText().toLowerCase(), filter.toLowerCase(), "Job vacancy type not match ");
            }
            appium.scroll(0.5, 0.50, 0.20, 2000);
        }
    }
}
