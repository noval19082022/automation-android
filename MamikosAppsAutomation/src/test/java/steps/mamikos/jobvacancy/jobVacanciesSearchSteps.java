package steps.mamikos.jobvacancy;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.tenant.jobVacancies.SearchJobVacanciesPO;
import utilities.ThreadManager;

public class jobVacanciesSearchSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private SearchJobVacanciesPO searchJobVacancies = new SearchJobVacanciesPO(driver);

    @Given("user click on first popular search")
    public void user_click_on_first_popular_search() throws InterruptedException {
        searchJobVacancies.clickOnFirstPopularSearch();
    }
}
