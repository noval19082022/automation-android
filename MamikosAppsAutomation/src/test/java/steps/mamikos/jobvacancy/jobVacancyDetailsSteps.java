package steps.mamikos.jobvacancy;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.tenant.jobVacancies.JobVacancyDetailsPO;
import utilities.ThreadManager;

public class jobVacancyDetailsSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private JobVacancyDetailsPO details = new JobVacancyDetailsPO(driver);

    @When("user click on apply now button")
    public void user_click_on_apply_now_button() throws InterruptedException {
        details.clickOnApplyNowButton();
    }

    @When("user verify content job vacancy details")
    public void user_verify_content_job_vacancy_details() {
        details.jobTitleIsPresent();
        details.jobTypeIsPresent();
        details.seeBoardingHousesAroundButtonIsPresent();
        details.salaryIsPresent();
        details.dateJobVacancyOpenIsPresent();
        details.dateJobVacancyCloseIsPresent();
        details.phoneNumberResponsiblePersonIsPresent();
        details.descriptionIsPresent();
        details.locationMapIsPresent();
    }

    @When("user click back button arrow on page job vacancy details")
    public void user_click_back_button_arrow_on_page_job_vacancy_details() {
        details.clickOnBackButtonArrow();
    }
}
