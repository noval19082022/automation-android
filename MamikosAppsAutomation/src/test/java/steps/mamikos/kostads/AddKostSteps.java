package steps.mamikos.kostads;

import io.appium.java_client.remote.HideKeyboardStrategy;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import pageobjects.mamikos.common.AddPhotoPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.mamipay.AddKostPO;
import pageobjects.mamikos.owner.mamipay.UpdateKostPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;

public class AddKostSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private AddKostPO addKost = new AddKostPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private AddPhotoPO addPhoto = new AddPhotoPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private String randomKostName;
    private LoadingPO loading = new LoadingPO(driver);
    private UpdateKostPO updateKost = new UpdateKostPO(driver);


    @Then("screen detail Tambah Kost is shown")
    public void screenDetailTambahKostIsShown() {
        Assert.assertTrue(addKost.isBackButtonDisplayed(), "Back Button is not displayed!");
        Assert.assertTrue(addKost.isTitleDisplayed(), "Title is not displayed!");
        Assert.assertTrue(addKost.isAddNewKostButtonDisplayed(), "Add New Kost button is not displayed!");
    }

    @And("User clicks on Add Property button in bottom")
    public void user_clicks_on_Add_Property_button_in_bottom() throws InterruptedException {
        if (addKost.isActivateBookingPopUpAppear()) {
            addKost.clickLaterInActivateBookingPopUp();
        }
        addKost.clickAddPropertyButton();
        loading.waitLoadingFinish();
    }

    @And("user click on add New Kos")
    public void user_click_on_add_New_Kos() throws InterruptedException {
        addKost.clickNewKostButton();
        loading.waitLoadingFinish();
    }

    @And("user click on Add as New Kos")
    public void user_click_on_Add_as_New_Kos() throws InterruptedException {
        addKost.clickAddAsNewKostButton();
        addKost.clickAddKostButtonInPopUp();
    }

    @And("user input Kost Name {string}, Kost Address {string}, Kost Owner Name {string}, Kost Manager Name {string}, and Kost Manager phone number {string}")
    public void user_input_Kost_Name_Kost_Address_Kost_Manager_Name_and_Kost_Manager_phone_number(String kostName, String kostAddress, String ownerName, String managerName, String managerPhone) {
        String randomText = java.generateAlphanumeric(6);
        this.randomKostName = kostName + " " + randomText;
        addKost.fillKostName(randomKostName);
        addKost.fillKostAddress(kostAddress);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        addKost.fillKostOwnerName(ownerName);
        addKost.fillKostManagerName(managerName);
        addKost.fillKostManagerPhone(managerPhone);
        addKost.clickNextButton();
    }

    @And("user select boarder type {string}, input daily price {string}, weekly price {string}, and monthly price {string}")
    public void user_select_boarder_type_input_daily_price_weekly_price_and_monthly_price(String kosType, String dailyPrice, String weeklyPrice, String monthlyPrice) {
        addKost.clickKostTypeRadioButton(kosType);
        addKost.fillDailyKostPrice(dailyPrice);
        addKost.fillWeeklyKostPrice(weeklyPrice);
        addKost.fillMonthlyKostPrice(monthlyPrice);
    }

    @And("user input 3 month min price {string}, 6 month min price {string}, and yearly price {string}")
    public void user_input_3_month_min_price_6_month_min_price_and_yearly_price(String threeMonthPrice, String sixMonthPrice, String yearlyPrice) {
        addKost.fill3MonthlyKostPrice(threeMonthPrice);
        addKost.fill6MonthlyKostPrice(sixMonthPrice);
        addKost.fillYearlyKostPrice(yearlyPrice);
    }

    @And("user select minimum payment {string}, input additional other cost {string}")
    public void user_select_minimum_payment_input_additional_other_cost(String minPay, String addCost) {
        addKost.selectMinPayment(minPay);
        addKost.fillAdditionalKostPrice(addCost);
    }

    @And("user insert inputer name {string}, inputer email {string}, inputer phone {string}, and click next")
    public void user_insert_inputer_name_inputer_email_inputer_phone_and_click_next(String agentName, String agentMail, String agentPhone) {
        addKost.fillKostAgentName(agentName);
        addKost.fillKostAgentEmail(agentMail);
        addKost.fillKostAgentPhone(agentPhone);
        addKost.clickNextButton();
    }

    @And("user pick room facility :")
    public void user_pick_room_facility(List<String> facility) {
        for (String f : facility) {
            appium.clickElementByText(f);
        }
    }

    @And("user pick bathroom facility :")
    public void user_pick_bathroom_facility(List<String> bathroomFacility) {
        appium.scrollToElementByText("Fasilitas Kamar Mandi *");
        addKost.clickExpandBathroomFacility();
        for (String b : bathroomFacility) {
            appium.clickElementByText(b);
        }
    }

    @And("user pick public facility :")
    public void user_pick_public_facility(List<String> publicFacility) {
        appium.scrollToElementByText("Fasilitas Umum");
        addKost.clickExpandPublicFacility();
        for (String p : publicFacility) {
            appium.scrollToElementByText(p);
            appium.clickElementByText(p);
        }
    }

    @And("user pick parking facility :")
    public void user_pick_parking_facility(List<String> parkingFacility) {
        appium.scrollToElementByText("Parkir");
        addKost.clickExpandParkingFacility();
        for (String pub : parkingFacility) {
            addKost.waitThenScrollToText(pub);
            appium.clickElementByText(pub);
        }
    }

    @And("user pick environmental facilities :")
    public void user_pick_environmental_facilities(List<String> aroundFacility) {
        appium.scrollToElementByText("Akses Lingkungan");
        addKost.clickExpandAroundFacility();
        for (String a : aroundFacility) {
            appium.scrollToElementByText(a);
            appium.clickElementByText(a);
        }
    }

    @And("user select room space {string}")
    public void user_select_room_space(String space) {
        appium.scrollToElementByText("Luas Kamar (angka sesuai dengan ukuran meter) *");
        addKost.setRoomSize(space);
    }

    @And("user select year built {string}")
    public void user_select_year_built(String year) {
        appium.scroll(0.5, 0.70, 0.30, 1000);
        addKost.setYearBuiltOrLastRenov(year);
    }

    @And("user input total rooms {string}, total available rooms {string}, notes {string}, kos description {string}, and click next")
    public void user_input_total_rooms_total_available_rooms_notes_kos_description_and_click_next(String totalRooms, String availableRooms, String notes, String description) {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        addKost.fillTotalRooms(totalRooms);
        addKost.fillTotalAvailableRooms(availableRooms);
        addKost.fillNotes(notes);
        addKost.fillDescription(description);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        addKost.clickPropertyPolicy();
        addKost.clickNextButton();
    }

    @And("user add cover photo, building photo, room picture, bathroom picture, other facility photo, kos description {string}, and then click save")
    public void user_add_cover_photo_building_photo_room_picture_bathroom_picture_other_facility_photo_kos_description_and_then_click_save(String desc) throws InterruptedException {
        addKost.clickPhotoCover();
        addPhoto.uploadPhoto();
        loading.waitLoadingFinish();
        addKost.clickBuildingPhoto();
        addPhoto.uploadPhoto();
        loading.waitLoadingFinish();
        addKost.clickPhotoRoom();
        addPhoto.uploadPhoto();
        loading.waitLoadingFinish();
        addKost.clickPhotoBathRoom();
        addPhoto.uploadPhoto();
        loading.waitLoadingFinish();
        appium.scroll(0.5, 0.80, 0.20, 2000);
        addKost.clickOtherFacilityPhoto();
        addPhoto.uploadPhoto();
        appium.hardWait(5);
        loading.waitLoadingFinish();
        appium.scroll(0.5, 0.80, 0.20, 2000);
        addKost.fillFacilityPhotoDesc(desc);
        addKost.clickNextButton();
    }

    @And("user click next/done in pop up page")
    public void user_click_next_done_in_pop_up_page() throws InterruptedException {
        addKost.clickNextInAttention();
        loading.waitLoadingFinish();
    }

    @And("user click done button in room availability page")
    public void user_click_done_button_in_room_availability_page() throws InterruptedException {
        addKost.clickDone();
        loading.waitLoadingFinish();
    }

    @And("user see newly created kos is in most top of the list, with correct name, address {string}, status {string} and type {string}")
    public void user_see_newly_created_kos_is_in_most_top_of_the_list_with_correct_name_address_status_and_type(String address, String status, String type) {
        popUp.closeDirectBookingPopUp();
        Assert.assertEquals(addKost.getFirstKostName(), this.randomKostName, "Kost name is not " + this.randomKostName);
        Assert.assertEquals(addKost.getFirstKostAddress(), address, "Kost address is not " + address);
        Assert.assertEquals(addKost.getFirstKostStatus(), status, "Kost status is not " + status);
        Assert.assertEquals(addKost.getFirstKostType(), type, "Kost type is not " + type);
    }

    @And("user input Kost Name {string}")
    public void user_input_Kost_Name(String kosName) {
        addKost.fillKostName(kosName);
    }

    @And("user click next button in form data")
    public void user_click_next_button_in_form_data() {
        addKost.clickNextButton();
    }

    @Then("user see error message {string} below field")
    public void user_see_error_message_below_field(String errorMessage) {
        Assert.assertEquals(addKost.getErrorMessageKosNameField(), errorMessage, "Error message not appear/wrong");
    }

    @When("user insert text {string} then click done in room name or number field")
    public void user_insert_text_then_click_done_in_room_name_number_field(String roomName) {
        updateKost.inputRoomNameNumber(roomName);
    }

    @Then("user see error message {string} in room allotment page")
    public void user_see_error_message_in_room_allotment_page(String errorMessage) {
        Assert.assertEquals(updateKost.getErrorMessageRoomNameField(), errorMessage, "Error message in room name is not appear/wrong");
    }

    @Then("user see error message {string} in floor field room allotment page")
    public void user_see_error_message_in_floor_field_room_allotment_page(String errorMessage) {
        Assert.assertEquals(updateKost.getErrorMessageFloorField(), errorMessage, "Error message in floor is not appear/wrong");
    }

    @When("user insert text {string} then click done in floor \\(optional) field")
    public void user_insert_text_then_click_done_in_floor_optional_field(String text) {
        updateKost.inputFloor(text);
    }

    @And("user see pop up page content title is {string} and description {string}")
    public void user_see_pop_up_page_content_title_is_and_description(String title, String desc) {
        Assert.assertEquals(addKost.getSuccessMessage(), title, "Room allotment pop up content title is wrong");
        Assert.assertEquals(updateKost.getAddKosPopUpDesc(), desc, "Room allotment pop up content description is wrong");
    }

    @Given("user click add kos in Home")
    public void user_click_add_kos_in_Home() {
        addKost.clickAddKosInHome();
    }

    @Then("user see page title is {string} after add kos")
    public void user_see_page_title_is_after_add_kos(String title) {
        Assert.assertEquals(addKost.getTitleInstantBookingPage(), title, "Page title is wrong, should be " + title);
    }

    @Then("user click activate instant booking")
    public void user_click_activate_instant_booking() {
        addKost.clickActivateInstantBooking();
    }

    @When("user input Kost Address {string}")
    public void user_input_Kost_Address(String kostAddress) {
        addKost.fillKostAddress(kostAddress);
    }

    @And("user see error message {string} above address field")
    public void user_see_error_message_above_address_field(String errMsg) {
        Assert.assertEquals(addKost.getKosAddress(), errMsg, "Error message not appear/wrong in kos address");
    }

    @Given("user select boarder type {string}")
    public void user_select_boarder_type(String kosType) {
        addKost.clickKostTypeRadioButton(kosType);
    }

    @When("user input monthly price {string}")
    public void user_input_monthly_price(String monthlyPrice) {
        addKost.fillMonthlyKostPrice(monthlyPrice);
    }

    @Then("user see error message {string} below monthly price field")
    public void user_see_error_message_below_monthly_price_field(String price) {
        Assert.assertEquals(addKost.getErrorMessageMonthlyPrice(), price, "Error message wrong/not appear in monthly price");
    }

    @When("user input daily price {string}")
    public void user_input_daily_price(String dailyPrice) {
        addKost.fillDailyKostPrice(dailyPrice);
    }

    @Then("user see error message {string} below daily price field")
    public void user_see_error_message_below_daily_price_field(String message) {
        Assert.assertEquals(addKost.getErrorMessageDailyPrice(), message, "Error message wrong/not appear in daily price");
    }

    @Then("user see error message {string} below weekly price field")
    public void user_see_error_message_below_weekly_price_field(String message) {
        Assert.assertEquals(addKost.getErrorMessageWeeklyPrice(), message, "Error message wrong/not appear in weekly price");
    }

    @When("user input weekly price {string}")
    public void user_input_weekly_price(String weeklyPrice) {
        addKost.fillWeeklyKostPrice(weeklyPrice);
    }

    @When("user input minimum {int} monthly price {string}")
    public void user_input_minimum_monthly_price(Integer month, String price) {
        if (month == 3) {
            addKost.fill3MonthlyKostPrice(price);
        } else {
            addKost.fill6MonthlyKostPrice(price);
        }
    }

    @Then("user see error message {string} below {int} monthly price field")
    public void user_see_error_message_below_monthly_price_field(String message, Integer month) {
        if (month == 3) {
            Assert.assertEquals(addKost.getErrorMessage3MonthlyPrice(), message, "Error message wrong/not appear in 3 monthly price");
        } else {
            Assert.assertEquals(addKost.getErrorMessage6MonthlyPrice(), message, "Error message wrong/not appear in 6 monthly price");
        }
    }

    @When("user input yearly price {string}")
    public void user_input_yearly_price(String yearlyPrice) {
        addKost.fillYearlyKostPrice(yearlyPrice);
        appium.scroll(0.5, 0.80, 0.20, 2000);
    }

    @Then("user see error message {string} below yearly price field")
    public void user_see_error_message_below_yearly_price_field(String message) {
        Assert.assertEquals(addKost.getErrorMessageYearlyPrice(), message, "Error message wrong/not appear in yearly price");
    }

    @Then("user see {string} is selected in bathroom facility")
    public void user_see_is_selected_in_bathroom_facility(String facility) {
        Assert.assertTrue(appium.isElementSelectedByText(facility), "Facility is not selected");
    }

    @Then("user see {string} is not selected in bathroom facility")
    public void user_see_is_not_selected_in_bathroom_facility(String facility) {
        Assert.assertFalse(appium.isElementSelectedByText(facility), "Facility failed to unselected");
    }

    @When("user pick facility again :")
    public void user_pick_facility_again(List<String> facility) {
        for (String f : facility) {
            appium.clickElementByText(f);
        }
    }

    @Then("user input total rooms {string}")
    public void user_input_total_rooms(String room) {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        addKost.fillTotalRooms(room);
    }

    @Then("user input total available rooms {string}")
    public void user_input_total_available_rooms(String room) {
        addKost.fillTotalAvailableRooms(room);
    }

    @Then("user see error message {string} below room field")
    public void user_see_error_message_below_room_field(String message) {
        Assert.assertEquals(addKost.getErrorMessageRoom(), message, "Error message wrong/not appear in room field");
    }

    @And("user click on privacy policy checkbox in add kos form")
    public void user_click_on_privacy_policy_checkbox_in_add_kos_form() {
        addKost.clickPropertyPolicy();
    }

    @Then("user is in add photos page in add kos form")
    public void user_is_in_add_photos_page_in_add_kos_form() {
        Assert.assertTrue(addKost.isAddCoverPhotoAppear(), "It's not in add photo page");
    }

    @When("user scroll down until property policy")
    public void user_scroll_down_until_property_policy() {
        addKost.scrollDownUntilPropPolicy();
    }

    @Given("user clicks on start button beside Kos Data")
    public void user_clicks_on_start_button_beside_Kos_Data() throws InterruptedException {
        addKost.clickNextInAttention();
        loading.waitLoadingFinish();
    }

    @When("user input name kos with {string}")
    public void user_input_name_kos_with(String kosName) {
        addKost.fillKostName(kosName);
    }

    @When("user clicks checkbox room type")
    public void user_clicks_checkbox_room_type() {
        addKost.clickRoomTypeCheckbox();
    }

    @When("user input room type with {string}")
    public void user_input_room_type_with(String type) {
        addKost.fillRoomType(type);
    }

    @Then("user see warning kos name {string}")
    public void user_see_warning_kos_name(String error) {
        Assert.assertEquals(addKost.getErrorKosName(), error, "Error message not appear/wrong in kos name");
    }

    @Then("user see warning room type {string}")
    public void user_see_warning_room_type(String error) {
        Assert.assertEquals(addKost.getErrorRoomType(), error, "Error message not appear/wrong in room type");
    }

    @When("user select room type with {string}")
    public void user_select_room_type_with(String gender) {
        addKost.clickGender(gender);
    }

    @When("user input kos description with {string}")
    public void user_input_kos_description_with(String desc) throws InterruptedException {
        addKost.fillDescription(desc);
        if (Constants.MOBILE_OS == Platform.IOS) {
            driver.findElementByName("Done").click();
        }
        appium.hardWait(2);
    }

    @When("user clicks checkbox administrator kos")
    public void user_clicks_checkbox_administrator_kos() {
        addKost.clickRoomTypeCheckbox();
    }

    @When("user input administrator name with {string}")
    public void user_input_administrator_name_with(String managerName) {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        addKost.fillKostManagerName(managerName);
    }

    @When("user input mobile phone number with {string}")
    public void user_input_mobile_phone_number_with(String managerPhone) {
        addKost.fillKostManagerPhone(managerPhone);
    }

    @Then("user see warning administrator name {string}")
    public void user_see_warning_administrator_name(String error) {
        Assert.assertEquals(addKost.getErrorMessageManagerName(), error, "Error message not appear/wrong in manager name");
    }

    @Then("user see warning mobile phone number administrator {string}")
    public void user_see_warning_mobile_phone_number_administrator(String error) {
        Assert.assertEquals(addKost.getErrorMessageManagerPhone(), error, "Error message not appear/wrong in manager phone");
    }

    @When("user click kos name field")
    public void user_click_kos_name_field() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            addKost.clickKosNameField();
        }
        else {
            appium.scroll(0.5, 0.70, 0.30, 2000);
        }
    }

    @When("user select kos rule {string}")
    public void user_select_kos_rule(String rule) {
        addKost.clickKosRule(rule);
    }

    @Then("user see warning kos type {string}")
    public void user_see_warning_kos_type(String error) {
        Assert.assertEquals(addKost.getErrorKosType(), error, "Error message not appear/wrong in kos gender type");
    }

    @Then("user see warning kos description {string}")
    public void user_see_warning_kos_description(String error) {
        Assert.assertEquals(addKost.getErrorKosDesc(), error, "Error message not appear/wrong in kos description");
    }

    @Then("user see warning kos year built {string}")
    public void user_see_warning_kos_year_built(String error) {
        Assert.assertEquals(addKost.getErrorKosYearBuild(), error, "Error message not appear/wrong in kos year build");
    }

    @When("user select kos year built with {string}")
    public void user_select_kos_year_built_with(String year) {
        addKost.setKosYearBuilt(year);
    }

}
