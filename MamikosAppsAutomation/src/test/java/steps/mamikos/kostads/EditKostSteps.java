package steps.mamikos.kostads;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.mamipay.AddKostPO;
import utilities.ThreadManager;

public class EditKostSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private AddKostPO addKost = new AddKostPO(driver);

    @Then("user see total available room field is disabled")
    public void user_see_total_available_room_field_is_disabled() {
        Assert.assertTrue(addKost.isAvailableRoomDisabled(), "Available room is not disabled");
    }

    @Then("user see total room field is disabled")
    public void user_see_total_room_field_is_disabled() {
        Assert.assertTrue(addKost.isTotalRoomDisabled(), "Total room is not disabled");
    }
}
