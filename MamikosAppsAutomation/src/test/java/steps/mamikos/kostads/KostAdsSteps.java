package steps.mamikos.kostads;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.owner.apartmentads.ApartemenAdsPO;
import pageobjects.mamikos.owner.kostads.KostAdsPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import utilities.ThreadManager;

public class KostAdsSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private KostAdsPO kost = new KostAdsPO(driver);
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private LoadingPO loading = new LoadingPO(driver);

    @And("user clicks on expand kost in kost ads page")
    public void user_clicks_on_expand_kost_in_kost_ads_page() {
        kost.clickExpandKost();
    }

    @And("user clicks on kost statistic time range")
    public void user_clicks_on_kost_statistic_time_range() {
        kost.clickStatisticTimeRange();
    }

    @And("user sees the number of click, chat, favorite, and review statistic")
    public void user_sees_the_number_of_click_chat_favorite_and_review_statistic() {
        searchListing.back();
        Assert.assertTrue(kost.isStatisticClickPresent(), "Count click statistic is not present!");
        Assert.assertTrue(kost.isStatisticChatPresent(), "Count chat statistic is not present!");
        Assert.assertTrue(kost.isStatisticFavoritePresent(), "Count favorite statistic is not present!");
        Assert.assertTrue(kost.isStatisticReviewPresent(), "Count review statistic is not present!");
    }

    @And("user clicks on set premium button")
    public void user_clicks_on_set_premium_button() {
        kost.clickSetPremium();
    }

    @Given("user search kos {string} in my ads page and select matching result")
    public void user_search_kos_in_my_ads_page_and_select_matching_result(String kosName) throws InterruptedException {
        kost.searchMyKos(kosName);
        kost.clickFirstSearchResult();
        loading.waitLoadingFinish();
    }

    @Given("user click on edit kos data")
    public void user_click_on_edit_kos_data() throws InterruptedException {
        kost.clickEditKosData();
        loading.waitLoadingFinish();
    }

    @And("user tap arrange button under my property")
    public void user_tap_arrange_button_under_my_property() throws InterruptedException {
        kost.clickArrange();
        loading.waitLoadingFinish();
    }
}
