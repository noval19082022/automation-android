package steps.mamikos.mamikosadmin;

import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageobjects.mamikosadmin.LeftMenuPO;
import pageobjects.mamikosadmin.LoginPO;
import pageobjects.mamikosadmin.RewardListPO;
import utilities.Constants;

import java.util.concurrent.TimeUnit;

public class MamikosAdminSteps {

    @Given("user navigate to mamikos admin, search reward {string} and change status to {string}")
    public void user_navigate_to_mamikos_admin_search_reward_and_change_status_to(String rewardName, String status) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1440x900");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Constants.MAMIKOS_ADMIN);

        LoginPO login = new LoginPO(driver);
        LeftMenuPO leftMenu = new LeftMenuPO(driver);
        RewardListPO rewardList = new RewardListPO(driver);

        login.enter_credentials_and_click_on_login_button(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
        leftMenu.click_on_reward_list_menu();
        rewardList.setSearchKeyword(rewardName);
        rewardList.chooseShowTestingOption("Show Testing");
        rewardList.clickOnFilterButton();

        //change redemption status
        rewardList.clickOnViewRedemptionIcon();
        rewardList.clickOnRedeemStatusField();
        rewardList.clickOnStatusDropdown();
        String notes = "";
        if(status.equals("Rejected")){
            notes = "redemption rejected";
        }
        else if(status.equals("Succeed")){
            notes = "redemption succeed";
        }
        rewardList.changeRedemptionStatus(status);
        rewardList.setRedemptionNotes(notes);
        rewardList.clickOnSubmitButton();
        Thread.sleep(2000);
        rewardList.isRedemptionStatusChanged(status);
        driver.close();
    }
}
