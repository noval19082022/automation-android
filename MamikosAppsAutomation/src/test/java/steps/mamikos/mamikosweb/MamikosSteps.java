package steps.mamikos.mamikosweb;

import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.FindBy;
import pageobjects.backoffice.LeftMenuPO;
import pageobjects.backoffice.SearchContractPO;
import pageobjects.mamikosweb.MamikosWebPO;
import utilities.Constants;
import utilities.JavaHelpers;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MamikosSteps {
    //Test Data Payment
    private String paymentProperties ="src/test/resources/testdata/mamikos/payment.properties";
    private String emailTenant_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"emailTenant");
    private String passwordTenant_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"passwordTenant");

    @Given("user navigate to web mamikos, login via facebook as {string}, and cancel booking")
    public void user_navigate_to_web_mamikos_login_via_facebook_as__and_cancel_booking(String tenant) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1440x900");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);
        //Now Pass ChromeOptions instance to ChromeDriver Constructor to initialize chrome driver which will switch off this browser notification on the chrome browser
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Constants.MAMIKOS_WEB);

        MamikosWebPO mamikos = new MamikosWebPO(driver);
        String email = "";
        String password = "";

        if (tenant.equalsIgnoreCase("tenantPayment")) {
            email = emailTenant_;
            password = passwordTenant_;
        }
        Thread.sleep(5000);
        mamikos.tenantLogin(email,password);
        Thread.sleep(10000);
        driver.get(Constants.MAMIKOS_WEB + "/user/booking");
        Thread.sleep(10000);
        mamikos.cancelBooking();
        driver.close();
    }
}
