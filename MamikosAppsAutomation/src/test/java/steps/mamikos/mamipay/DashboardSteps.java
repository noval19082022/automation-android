package steps.mamikos.mamipay;

import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.mamipay.DashboardPO;
import pageobjects.mamikos.owner.premium.PremiumPagePO;
import utilities.Constants;
import utilities.ThreadManager;

public class DashboardSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private DashboardPO dashboard = new DashboardPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private PremiumPagePO premium = new PremiumPagePO(driver);
    private LoadingPO loading = new LoadingPO(driver);

    @Then("screen dashboard is shown elements")
    public void screenDashboardIsShownElements() {
        Assert.assertTrue(dashboard.isLabelMamikosDisplayed(), "Label Mamikos is not displayed!");
        Assert.assertTrue(dashboard.isIconNotifDisplayed(), "Icon Notification is not displayed!");
        Assert.assertTrue(dashboard.isAddKostButtonDisplayed(), "Add New Kost Button is not displayed!");
        Assert.assertTrue(dashboard.isIconHomeDisplayed(), "Icon Home is not displayed!");
        Assert.assertTrue(dashboard.isIconChatDisplayed(), "Icon Chat is not displayed!");
        Assert.assertTrue(dashboard.isIconManageDisplayed(), "Icon Manage is not displayed!");
        Assert.assertTrue(dashboard.isIconStatisticsDisplayed(), "Icon Statistic is not displayed!");
        Assert.assertTrue(dashboard.isIconProfileDisplayed(), "Icon Profile is not displayed!");
    }

    @And("User clicks on Icon Notification")
    public void userClicksOnIconNotification() {
        dashboard.clickIconNotification();
    }

    @And("User clicks on Add Kost button")
    public void userClicksOnAddKostButton() throws InterruptedException {
        dashboard.clickAddKostButton();
        loading.waitLoadingFinish();
    }

    @Then("screen dashboard is shown elements Bisa Booking and Empty Finance")
    public void screenDashboardIsShownElementsBisaBookingAndEmptyFinance(){
        popUp.clickOnCloseInstantBookingTooltip();
        Assert.assertTrue(dashboard.isLabelMamikosDisplayed(), "Label Mamikos is not displayed!");
        Assert.assertTrue(dashboard.isIconNotifDisplayed(), "Icon Notification is not displayed!");
        Assert.assertTrue(dashboard.isHelpIconDisplayed(), "Help Icon is not displayed!");
        Assert.assertTrue(dashboard.isTitleFinanceDisplayed(), "Title Finance is not displayed!");
        Assert.assertTrue(dashboard.isEmptyFinanceTextDisplayed(), "Empty Finance is not displayed!");
        Assert.assertTrue(dashboard.isTitleManageBookingDisplayed(), "Title Manage Booking Text is not displayed!");
        Assert.assertTrue(dashboard.isViewAllTextMBDisplayed(), "View All Text Manage Booking Text is not displayed!");
        Assert.assertTrue(dashboard.isEmptyManageBookingDisplayed(), "Empty Manage Booking Text is not displayed!");
        Assert.assertTrue(dashboard.isManageBookingButtonDisplayed(), "Manage Booking Button is not displayed!");
        Assert.assertTrue(dashboard.isTitlePremiumTextDisplayed(), "Title Premium Text is not displayed!");
        Assert.assertTrue(dashboard.isNonPremiumDescDisplayed(), "NonPremium Desc is not displayed!");
        Assert.assertTrue(dashboard.isActivatePremiumButtonDisplayed(), "Activate Premium Button is not displayed!");
        Assert.assertTrue(dashboard.isTitleRoomTextDisplayed(), "Title Room Text is not displayed!");
        Assert.assertTrue(dashboard.isViewUpdateRoomPriceDisplayed(), "View Update Room Price is not displayed!");
        Assert.assertTrue(dashboard.isRoomAvailableViewDisplayed(), "View Update Room Price is not displayed!");
        Assert.assertTrue(dashboard.isUpdateAllRoomsButtonDisplayed(), "Update All Rooms Button is not displayed!");
        if(Constants.MOBILE_OS == Platform.ANDROID) {
            Assert.assertTrue(dashboard.isTitleTenantTextDisplayed(), "Title Tenant Text is not displayed!");
            Assert.assertTrue(dashboard.isEmptyTenantTextDisplayed(), "Empty Tenant Text is not displayed!");
            Assert.assertTrue(dashboard.isEmptyTenantDecsTextDisplayed(), "Empty Tenant Decs Text is not displayed!");
            Assert.assertTrue(dashboard.isAddTenantButtonDisplayed(), "Add Tenant Button is not displayed!");
            }

        Assert.assertTrue(dashboard.isBannerPromoDisplayed(), "Banner Promo is not displayed!");
    }

    @Then("screen detail active premium widget")
    public void screenDetailActivePremiumWidget() {
        Assert.assertTrue(dashboard.isPremiumPackageTitleDisplayed(), "Premium package title is not displayed!");
        Assert.assertTrue(dashboard.isPremiumExpiredDateDisplayed(), "Premium expired date is not displayed!");
        Assert.assertTrue(dashboard.isPremiumBuyBalanceTextlinkDisplayed(), "'Beli Saldo' textlink is not displayed!");
        Assert.assertTrue(dashboard.isPremiumBalanceDisplayed(), "Balance is not displayed!");
        Assert.assertTrue(dashboard.isSettingPremiumButtonDisplayed(), "Premium Button is not displayed!");
    }

    @Then("screen detail widget Finance with active balance")
    public void screenDetailWidgetFinanceWithActiveBalance() {
        Assert.assertTrue(dashboard.isTitleFinanceDisplayed(), "Title Finance is not displayed!");
        Assert.assertTrue(dashboard.isTotalTextDisplayed(), "Total text is not displayed!");
        Assert.assertTrue(dashboard.isValueTotalTextDisplayed(), "Value total text is not displayed!");
        Assert.assertTrue(dashboard.isRentPaymentTextDisplayed(), "Rent payment text is not displayed!");
        Assert.assertTrue(dashboard.isValueRentPaymentTextDisplayed(), "Value rent payment text is not displayed!");
        Assert.assertTrue(dashboard.selectPreviousMonthAndCheckTheFinanceBalance(), "Value rent payment in previous month text is not displayed!");
    }

    @Then("screen detail widget Renter with active tenant")
    public void screenDetailWidgetRenterWithActiveTenant() {
        Assert.assertTrue(dashboard.isTitleRenterDisplayed(), "Title 'Penyewa' is not displayed!");
        Assert.assertTrue(dashboard.isTenantImageDisplayed(), "Tenant image is not displayed!");
        Assert.assertTrue(dashboard.isTenantNameDisplayed(), "Tenant name is not displayed!");
        Assert.assertTrue(dashboard.isTenantRoomNumberDisplayed(), "Tenant room number is not displayed!");
        Assert.assertTrue(dashboard.isTenantKostNameDisplayed(), "Tenant kost name is not displayed!");
        Assert.assertTrue(dashboard.isTenantStatusPaymentDisplayed(), "Tenant status payment is not displayed!");
        Assert.assertTrue(dashboard.isAddTenantButtonDisplayed(), "Add Tenant Button is not displayed!");
    }

    @When("user clicks on manage premium from homescreen")
    public void user_clicks_on_manage_premium_from_homescreen() {
        dashboard.clickManagePremiumButton();
    }

    @When("user switch activate daily balance premium")
    public void user_switch_activate_daily_balance_premium() {
        dashboard.clickActivateDailyBalance();
        if(premium.isConfirmNonActivePopUpPresent()){
            premium.clickNonactivateDailyBalance();
        }
    }

    @When("user click activate now instant booking in dashboard")
    public void user_click_activate_now_instant_booking_in_dashboard() {
       dashboard.clickOnActivateNowBBK();
    }

    @And("user tap register to Instant Booking in Manage section")
    public void user_tap_register_to_Instant_Booking_in_Manage_section() throws InterruptedException {
        dashboard.clickRegisterInstantBooking();
        loading.waitLoadingFinish();
    }
}