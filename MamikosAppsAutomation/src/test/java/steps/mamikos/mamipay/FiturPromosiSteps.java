package steps.mamikos.mamipay;

import java.util.List;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.mamipay.FiturPromisiPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.ThreadManager;

public class FiturPromosiSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private FiturPromisiPO fiturPromosi = new FiturPromisiPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);

    private PopUpPO popUp = new PopUpPO(driver);
    private FooterPO footer = new FooterPO(driver);
    private LoadingPO loading = new LoadingPO(driver);



    @Then("user see menu fitur promosi")
    public void user_see_menu_fitur_promosi(List<String> menu) {
        for ( int i = 0; i < menu.size() ; i++){
            if (Constants.MOBILE_OS== Platform.ANDROID) {
                appium.scrollToElementByText(menu.get(i));
            }
            else{
                appium.scrollScreenIOS("down",menu.get(i));
            }
            Assert.assertTrue(fiturPromosi.menuFiturPromosiPresent(menu.get(i)), "Menu " + menu.get(i) + " is not present");
        }
    }

    @When("owner scroll {string} and taps on {string} menu")
    public void owner_taps_on_menu_fitur_promosi(String direction, String menu) throws InterruptedException {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            appium.scrollToElementByText(menu);
            appium.clickElementByText(menu);
            By nextButton = By.id("ftueOkeTextView");
            if (appium.waitInCaseElementVisible(nextButton, 5) != null) {
                for (int i = 0; i < 4; i++) {
                    appium.clickOn(nextButton);
                }
            }
        }else{
            appium.scrollScreenIOS(direction, menu);
            fiturPromosi.clickOnFiturPromosiTab(menu);
        }
    }

    @Then("system display page MamiAds")
    public void system_display_page_mamiAds() throws InterruptedException{
        Assert.assertTrue(fiturPromosi.mamiAdsPageIsPresent(), "MamiAds page is not present");
    }

    @Then("system display page Pesan Broadcast")
    public void system_display_page_pesan_broadcast() throws InterruptedException{
        Assert.assertTrue(fiturPromosi.pesanBroadcastPageIsPresent(), "Pesan Broadcast page is not present");
    }

    @Then("system display page Promo Iklan")
    public void system_display_page_promo_iklan() throws InterruptedException{
        Assert.assertTrue(fiturPromosi.promoIklanPageIsPresent(), "Promo Iklan page is not present");
    }

    @Then("system display page Cek Properti Sekitar")
    public void system_display_page_cek_properti_sekitar() throws InterruptedException{
        Assert.assertTrue(fiturPromosi.propertiSekitarPageIsPresent(), "Cek Properti Sekitar page is not present");
    }

    @Then("system display page Pro Photo")
    public void system_display_page_pro_photo() throws InterruptedException{
        Assert.assertTrue(fiturPromosi.proPhotoPageIsPresent(), "Pro Photo page is not present");
    }

    @And("owner taps button Coba Sekarang")
    public void ownerTapsButtonCobaSekarang() throws InterruptedException {
        fiturPromosi.clickOnCobaSekarangButton();
    }
}
