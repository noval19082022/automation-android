package steps.mamikos.mamipay;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.owner.mamipay.KostDetailsPO;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

public class KostDetailsSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private KostDetailsPO mamipayKostDetails = new KostDetailsPO(driver);
    private LoadingPO load = new LoadingPO(driver);
    AppiumHelpers appium = new AppiumHelpers(driver);

    @Then("I can reach kost detail page for mamipay")
    public void i_can_reach_kost_detail_page_for_mamipay() throws InterruptedException {
        Assert.assertTrue(mamipayKostDetails.isManageBillKostTextIsPresent(),
                "Text manage kost bill not present");
    }
}
