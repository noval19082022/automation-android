package steps.mamikos.mamipay;

import java.util.List;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pageobjects.mamikos.owner.mamipay.ManageBillAndBookingPO;
import utilities.Constants;
import utilities.ThreadManager;

public class ManageBillAndBookingSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ManageBillAndBookingPO manageBill = new ManageBillAndBookingPO(driver);

    @And("Box kost list number {int} contain {string}")
    public void box_kost_list_contains_image_thumbnail(int listNumber, String textTarget) throws InterruptedException {
        switch (textTarget) {
            case "image thumbnail":
                Assert.assertTrue(manageBill.kostImageThumbnailIsPresent(listNumber),
                        "Image thumbnail list number " + listNumber + " is not present");
                break;
            case "kost name":
                Assert.assertTrue(manageBill.kostNameIsPresent(listNumber),
                        "Kost name list number " + listNumber +" is not present");
                break;
            case "available room":
                Assert.assertTrue(manageBill.kostAvailableRoomStatusIsPresent(listNumber),
                        "Kost available room list number "+ listNumber + " is not present");
                break;
            case "kost location":
                Assert.assertTrue(manageBill.kostLocationIsPresent(listNumber),
                        "Kost location list number " + listNumber + " is not present");
                break;
            case "last update date":
                Assert.assertTrue(manageBill.lastUpdateDateIsPresent(listNumber),
                        "Kost last update date list number " + listNumber + " is not present");
                break;
        }
    }

    @And("Box kost list number {int} contain kost name")
    public void box_kost_list_contain_kost_name(int listNumber) throws InterruptedException {
        Assert.assertTrue(manageBill.kostNameIsPresent(listNumber),
                "Kost name list number " + listNumber +" is not present");
    }

    @And("I tap on Kelola Tagihan Dan Booking button")
    public void i_tap_on_kelola_tagihan_dan_booking_button() {
        manageBill.tapOrClickManageBillAndBooking();
    }

    @And("Box kost list number {int} contain available room")
    public void box_kost_list_contain_available_room(int listNumber) throws InterruptedException {
        Assert.assertTrue(manageBill.kostAvailableRoomStatusIsPresent(listNumber),
                "Kost available room list number "+ listNumber + " is not present");
    }

    @And("Box kost list number {int} contain kost location")
    public void box_kost_list_contain_kost_location(int listNumber) throws InterruptedException {
        Assert.assertTrue(manageBill.kostLocationIsPresent(listNumber),
                "Kost location list number " + listNumber + " is not present");
    }

    @And("Box kost list number {int} contain last update date")
    public void box_kost_list_contain_last_update_date(int listNumber) throws InterruptedException {
        Assert.assertTrue(manageBill.lastUpdateDateIsPresent(listNumber),
                "Kost last update date list number " + listNumber + " is not present");
    }

    @And("I tap in the box kost list number {int}.")
    public void i_tap_in_the_box_kost_list_number(int listNumber) {
        manageBill.tapOrClickBillKostList(listNumber);
    }

    @And("I tap on Nanti Saja button")
    public void i_tap_on_nanti_saja_button() {
        manageBill.tapOrClickNantiSajaButton();
    }

    @And("I can see box kost list number {int} have tenant waiting for approval")
    public void i_can_see_box_kost_list_number_have_tenant_waiting_for_approval(int listNumber) throws InterruptedException {
        Assert.assertTrue(manageBill.tenantWaitForApprovalIsPresent(listNumber),
                "Kost list number " + listNumber + " does not contain tenant wait for approval element");
    }

    @And("I tap on tenant waiting for approval text on kost list number {int}")
    public void i_tap_on_tenant_waiting_for_approval_text_on_kost_list_number(int listNumber) throws InterruptedException {
        manageBill.tapOrClickOnTenantWaitingApprovalText(listNumber);
    }

    @And("I is on main page and tap tooltip if it appear")
    public void i_is_on_main_page_and_tap_tooltip_if_it_appear() throws InterruptedException {
        manageBill.tapOrClickTooltip();
    }

    @Then("I can see empty list of Kelola Tagihan dan Booking")
    public void i_see_empty_list_of_kelola_tagihan_dan_booking() throws InterruptedException {
        Assert.assertTrue(manageBill.addNewKostButtonIsPresent(), "Add new kost button not present");
        Assert.assertTrue(manageBill.belumAdaKostTextIsPresent(), "Belum ada kost text element is not present");
        Assert.assertFalse(manageBill.btnManageBillAndBookingIsPresent(),
                "Button manage bill and booking is present");
    }

    @Then("^I can see in empty list text$")
    public void i_can_see_in_empty_list(List<String> list) {
        String addKostButtonText = (Constants.MOBILE_OS== Platform.ANDROID)
                ? list.get(2)
                : list.get(1);
        Assert.assertEquals(manageBill.getBelumAdaKostText(), list.get(0), "Text not equal to " + list.get(0));
        Assert.assertEquals(manageBill.getTambahIklanKostText(), addKostButtonText, "Text not equal to " + addKostButtonText);
    }

    @Then("I can see kost list")
    public void i_can_see_kost_list() throws InterruptedException {
        Assert.assertTrue(manageBill.allKostListIsPresent(), "Kost list is not present");
    }

    @Then("I can see box kost list number {int}")
    public void i_can_see_box_kost_list(int listNumber) throws InterruptedException {
        Assert.assertTrue(manageBill.boxKostListPresent(listNumber), "List number " + listNumber + " is not present.");
    }

    @Then("system display page Pengajuan Booking")
    public void system_display_page_pengajuan_booking() throws InterruptedException{
        Assert.assertTrue(manageBill.pengajuanBookingPageIsPresent(), "Pengajuan Booking page is not present");
    }

    @Then("system display page Laporan Keuangan")
    public void system_display_page_laporan_keuangan() throws InterruptedException{
        Assert.assertTrue(manageBill.laporanKeuanganPageIsPresent(), "Laporan Keuangan page is not present");
    }

    @Then("system display page Kelola Tagihan")
    public void system_display_page_kelola_tagihan() throws InterruptedException{
        Assert.assertTrue(manageBill.kelolaTagihanPageIsPresent(), "Kelola Tagihan page is not present");
    }

    @Then("system display page Penyewa")
    public void system_display_page_penyewa() throws InterruptedException{
        Assert.assertTrue(manageBill.penyewaPageIsPresent(), "Penyewa page is not present");
    }
}
