package steps.mamikos.mamipay;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.mamipay.RegisterFormPO;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

public class RegisterMamipaySteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private RegisterFormPO registerForm = new RegisterFormPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);


    @When("user fill full name in register BBK form with {string}")
    public void user_fill_full_name_in_register_BBK_form_with(String fullname) {
        registerForm.insertTextFullName(fullname);
    }

    @When("user click bank account field in register BBK form")
    public void user_click_bank_account_field_in_register_BBK_form() {
        registerForm.clickBankAccountField();
    }

    @When("user fill bank account number in register BBK form with {string}")
    public void user_fill_bank_account_number_in_register_BBK_form_with(String number) {
        registerForm.insertTextBankNumber(number);
    }

    @When("user click full name field in register BBK form")
    public void user_click_full_name_field_in_register_BBK_form() {
        registerForm.clickFullNameField();
    }

    @Then("user don't see text {string} in bank account number field")
    public void user_don_t_see_text_in_bank_account_number_field(String text) {
        Assert.assertEquals(registerForm.getBankAccNo(), text, "Shouldn't be able to insert alphabet in bank number");
    }

    @Then("user see error message {string} in form activate mamipay")
    public void user_see_error_message_in_form_activate_mamipay(String errorMessage) {
        Assert.assertEquals(registerForm.textErrorBankName(), errorMessage, "Error message not appear");
    }

    @When("user click continue button in from activate mamipay")
    public void user_click_continue_button_in_from_activate_mamipay() {
        registerForm.clickNextbuttonform();
    }

    @Given("user click on drop down list Bank Name in form mamipay")
    public void user_click_on_drop_down_list_Bank_Name_in_form_mamipay() {
        registerForm.clickdropDownBankNameamipay();
        appium.hideKeyboard();
    }

    @Given("user input Bank Name with {string} in form activate mamipay")
    public void user_input_Bank_Name_with_in_form_activate_mamipay(String bankName) {
        registerForm.fillBankName(bankName);
        appium.hideKeyboard();
    }

    @When("user click on term and condition checkbox")
    public void user_click_on_term_and_condition_checkbox() {
        registerForm.clickTermsCondition();
    }
}
