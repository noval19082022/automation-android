package steps.mamikos.mamipoin;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.mamipoin.*;
import pageobjects.mamikos.owner.premium.UpgradePremiumPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MamiPoinSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private MamiPoinPO mamipoin = new MamiPoinPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private RedeemRewardPO redeemReward = new RedeemRewardPO(driver);
    private RewardHistoryPO rewardHistory = new RewardHistoryPO(driver);
    private RewardListPO rewardList = new RewardListPO(driver);
    private RewardDetailPO rewardDetail = new RewardDetailPO(driver);
    private TermAndConditionPO tnc = new TermAndConditionPO(driver);

    //Test Data MamiPoin
    private String mamipoinProperties="src/test/resources/testdata/mamikos/mamiPoinOwner.properties";
    private String mamiPoinLandingPageOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"mamiPoinLandingPageOnboardingText");
    private String rewardHistoryLandingPageOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"rewardHistoryLandingPageOnboardingText");
    private String pointHistoryLandingPageOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"pointHistoryLandingPageOnboardingText");
    private String termAndConditionLandingPageOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"termAndConditionLandingPageOnboardingText");
    private String redeemPointLandingPageOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"redeemPointLandingPageOnboardingText");
    private String redeemRewardOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"redeemRewardOnboardingText");
    private String rewardOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"rewardOnboardingText");
    private String helpOnboardingText = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"helpOnboardingText");

    private String emptyTotalQuotaReward = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyTotalQuotaReward");
    private String emptyDailyQuotaReward = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyDailyQuotaReward");
    private String emptyTotalEachUserQuotaReward = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyTotalEachUserQuotaReward");
    private String emptyDailyEachUserQuota = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyDailyEachUserQuota");
    private String expiredReward = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"expiredReward");
    private String notStartedYetReward = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"notStartedYetReward");
    private String twoLineRewardName = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"twoLineRewardName");
    private String longRewardName = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"longRewardName");
    private String userRedeemRewardName = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"userRedeemRewardName");

    private String address = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"shippingAddress");
    private String note = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"note");
    private String meterNumber = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"electricityMeterNumber");

    private String emptyStateAllRewardHistory = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyStateAllRewardHistory");
    private String emptyStateOnProcessRewardHistory = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyStateOnProcessRewardHistory");
    private String emptyStateSuccessRewardHistory = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyStateSuccessRewardHistory");
    private String emptyStateRejectedRewardHistory = JavaHelpers.getPropertyValueForSquad(mamipoinProperties,"emptyStateRejectedRewardHistory");

    private Integer userPointValue;
    private Integer rewardPointValue;

    @Then("user verify MamiPoin entry point not displayed")
    public void user_verify_mamiPoin_entry_point_not_displayed() {
        Assert.assertFalse(mamipoin.isMamiPoinCardDisplayed());
    }

    @Then("user verify MamiPoin entry point displayed")
    public void user_verify_mamiPoin_entry_point_displayed() throws InterruptedException {
        loading.waitLoadingFinish();
        popUp.clickOnLaterButton();
        popUp.dismissOwnerUpdatePropertyAdsPopup();
        popUp.closeExtraCostPopUp();
        popUp.dismissOwnerPopup();
        Assert.assertTrue(mamipoin.isMamiPoinCardDisplayed());
    }

    @Then("user verify mamipoin onboarding")
    public void user_verify_mamipoin_onboarding() {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            Assert.assertTrue(mamipoin.isMamiPoinOnboardingDisplayed());
        }
        //Mamipoin
        Assert.assertEquals(mamipoin.getMamiPoinLandingPageOnboardingText(),mamiPoinLandingPageOnboardingText,"Text doesn't match");
        mamipoin.clickOnNextButton();
        //Riwayat Hadiah
        Assert.assertEquals(mamipoin.getRewardHistoryOnboardingText(),rewardHistoryLandingPageOnboardingText,"Text doesn't match");
        mamipoin.clickOnNextButton();
        //Riwayat Poin
        Assert.assertEquals(mamipoin.getPointHistoryOnboardingText(),pointHistoryLandingPageOnboardingText,"Text doesn't match");
        mamipoin.clickOnNextButton();
        //Syarat dan Ketentuan
        Assert.assertEquals(mamipoin.getTermAndConditionOnboardingText(),termAndConditionLandingPageOnboardingText,"Text doesn't match");
        mamipoin.clickOnNextButton();
        //Tukar Poin
        Assert.assertEquals(mamipoin.getRedeemPointOnboardingText(),redeemPointLandingPageOnboardingText,"Text doesn't match");
        mamipoin.clickOnFinishButton();
    }

    @When("user clicks on Tukar Poin Button on mamipoin landing page")
    public void user_clicks_on_tukar_poin_button_on_mamipoin_landing_page() {
        mamipoin.clickOnRedeemPointButton();
    }

    @When("user clicks on MamiPoin Tenant entry point button")
    public void user_clicks_on_mamipoin_tenant_entry_point_button() {
        mamipoin.clickOnMamiPoinTenantEntryPointButton();
    }

    @When("user clicks on informasi poin button")
    public void user_clicks_on_informasi_poin_button() {
        mamipoin.clickOnInformasiPoinButton();
    }

    @And("user clicks on riwayat poin button")
    public void user_clicks_on_riwayat_poin_button() {
        mamipoin.clickOnRiwayatPoinButton();
    }

    @And("user clicks on dapatkan poin button")
    public void user_clicks_on_dapatkan_poin_button() {
        mamipoin.clickOnDapatkanPoinButton();
    }

    @Then("user verify reward list onboarding")
    public void user_verify_reward_list_onboarding() {
        //Tukar Hadiah
        Assert.assertEquals(rewardList.getMamiPoinLandingPageOnboardingText(),redeemRewardOnboardingText,"Text doesn't match");
        mamipoin.clickOnNextButton();
        //Hadiah
        Assert.assertEquals(rewardList.getRewardOnboardingText(),rewardOnboardingText,"Text doesn't match");
        mamipoin.clickOnNextButton();
        //Bantuan
        Assert.assertEquals(rewardList.getHelpOnboardingText(),helpOnboardingText,"Text doesn't match");
        mamipoin.clickOnFinishButton();
    }

    @Then("user verify mamipoin onboarding not displayed")
    public void user_verify_mamipoin_onboarding_not_displayed() {
        Assert.assertFalse(mamipoin.isMamiPoinOnboardingDisplayed());
    }

    @Then("user verify Tukar Poin button is not displayed")
    public void user_verify_tukar_poin_button_is_not_displayed() {
        Assert.assertFalse(rewardDetail.isTukarPoinButtonDisplayed());
    }

    @Then("user verify warning Poin Anda tidak cukup is displayed")
    public void user_verify_warning_poin_anda_tidak_cukup_is_displayed() {
        Assert.assertTrue(redeemReward.isWarningPoinAndaTidakCukupDisplayed());
    }

    @Then("user verify reward list onboarding not displayed")
    public void user_verify_reward_list_onboarding_not_displayed() {
        Assert.assertFalse(rewardList.isMamiPoinOnboardingDisplayed());
    }

    @When("user clicks on Help Button")
    public void user_clicks_on_help_button() {
        rewardList.clickOnHelpButton();
    }

    @When("user clicks on Reward History")
    public void user_clicks_on_reward_history() {
        mamipoin.clickOnRewardHistoryButton();
    }

    @Then("user verify Reward History header")
    public void user_verify_reward_history_header() {
        Assert.assertTrue(rewardHistory.isRewardHistoryHeaderDisplayed());
    }

    @And("user verify Reward History filter")
    public void user_verify_reward_history_filter() {
        Assert.assertTrue(rewardHistory.isRewardHistoryFilterDisplayed());
    }

    @And("user verify list of reward redemption history")
    public void user_verify_list_of_reward_redemption_history() {
        Assert.assertTrue(rewardHistory.isRewardRedemptionCardDisplayed() &&
                rewardHistory.isRewardTitleDisplayed() &&
                rewardHistory.isRewardStatusDisplayed());
    }


    @When("user clicks on see status")
    public void user_clicks_on_see_status() {
        rewardHistory.clickOnSeeStatusRedemption();
    }

    @Then("user verify {string} reward is not displayed")
    public void user_verify_reward_is_not_displayed(String condition) {
        String rewardName = "";
        if(condition.equals("empty total quota")){
            rewardName = emptyTotalQuotaReward;
        }
        else if(condition.equals("empty daily quota")){
            rewardName = emptyDailyQuotaReward;
        }
        else if(condition.equals("empty total each user quota")){
            rewardName = emptyTotalEachUserQuotaReward+" "+Constants.MOBILE_OS;
        }
        else if(condition.equals("empty daily each user quota")){
            rewardName = emptyDailyEachUserQuota;
        }
        else if(condition.equals("expired")){
            rewardName = expiredReward;
        }
        else if (condition.equals("not started yet")){
            rewardName = notStartedYetReward;
        }
        Assert.assertFalse(rewardList.isRewardDisplayed(rewardName));
        redeemReward.clickOnBackButton();
        mamipoin.clickOnRedeemPointButton();
    }

    @When("user redeem reward {string}")
    public void user_redeem_reward(String rewardName) throws InterruptedException {
        userPointValue = rewardList.getUserPoint();
        if(rewardName.contains("Daily")){
            rewardName = rewardName+" "+Constants.MOBILE_OS;
        }
        rewardList.clickOnSeeReward(rewardName);
        rewardPointValue = rewardDetail.getRewardPoint();
        rewardDetail.clickOnRedeemButton();
        if(redeemReward.isRewardTypeIsPLN()){
            redeemReward.setElectricityMeterNumber(meterNumber);
        }
        else{
            redeemReward.setShippingAddress(address);
            redeemReward.setRedemptionNote(note);
        }
        redeemReward.clickOnRedeemPointButton();
    }

    @Then("user verify redeem confirmation pop up and click {string}")
    public void user_verify_redeem_confirmation_pop_up_and_click(String action) {
        Assert.assertEquals(redeemReward.getPopUpMessage(),"Yakin mau menukar poin untuk hadiah ini?","message doesn't match");
        if (Constants.MOBILE_OS==Platform.ANDROID){
            Assert.assertEquals(redeemReward.getPopUpWarningMessage(),"Anda akan menukarkan 1 poin untuk mendapatkan hadiah ini","warning doesn't match");
        }
        else {
            Assert.assertEquals(redeemReward.getPopUpWarningMessage(),"Anda akan menukar 1 poin untuk mendapatkan hadiah ini","warning doesn't match");
        }

        redeemReward.clickOnConfirmationButton(action);
    }

    @And("user verify reward {string} date")
    public void user_verify_reward_date(String status) throws InterruptedException {
        Locale localeIndonesia = new Locale("id", "ID");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMM yyyy", localeIndonesia);
        LocalDateTime now = LocalDateTime.now();
        String today = dtf.format(now);
        if(status.equals("processing")){
            Assert.assertTrue(redeemReward.getRewardProcessingDate().contains(today));
        }
        else{
            Assert.assertTrue(redeemReward.getRejectedOrSucceedDate().contains(today));
        }

    }

    @When("user back to Mamipoin Landing Page from detail status")
    public void user_back_to_mamipoin_landing_page_from_detail_status() {
        redeemReward.clickOnBackButton();
        redeemReward.clickOnBackButton();
    }

    @When("user clicks on Tukar Poin Button on redemption page")
    public void user_clicks_on_tukar_poin_button_on_redemption_page() {
        redeemReward.clickOnRedeemPointButton();
    }

    @When("user clicks on reward {string}")
    public void user_clicks_on_reward(String rewardName) {
        rewardList.clickOnSeeReward(rewardName);
    }

    @Then("user verify Tukar Poin Button is disable")
    public void user_verify_tukar_poin_button_is_disable() throws InterruptedException {
        Assert.assertFalse(rewardDetail.isRedeemButtonIsEnable());
    }

    @When("user clicks on {string} Filter")
    public void user_clicks_on_filter(String filter) throws InterruptedException {
        String status = "";
        switch (filter) {
            case "all":
                status = "Semua";
                break;
            case "on process":
                status = "Sedang diproses";
                break;
            case "success":
                status = "Berhasil diterima";
                break;
            case "rejected":
                status = "Penukaran gagal";
                break;
        }
        rewardHistory.clickOnFilter(status);
    }

    @Then("user see {string} redeemed reward")
    public void user_see_redeemed_reward(String filter) {
        List<String> status = new ArrayList<>();
        switch (filter) {
            case "all":
                status.add("Penukaran Gagal");
                status.add("Berhasil Diterima");
                status.add("Penukaran Gagal");
                status.add("Sedang Diproses");
                break;
            case "on process":
                status.add("Sedang Diproses");
                break;
            case "success":
                status.add("Berhasil Diterima");
                break;
            case "rejected":
                status.add("Penukaran Gagal");
                status.add("Penukaran Gagal");
                break;
        }
        for (int i=0; i<rewardHistory.getRedemptionCardNumber(); i++){
            Assert.assertEquals(rewardHistory.getRedemptionStatus(i), status.get(i), "status doesn't match");
        }
    }

    @Then("user verify empty {string} redemption reward")
    public void user_verify_empty_redemption_reward(String filter) {
        String message = "";
        switch (filter) {
            case "all":
                message = emptyStateAllRewardHistory;
                break;
            case "on process":
                message = emptyStateOnProcessRewardHistory;
                break;
            case "success":
                message = emptyStateSuccessRewardHistory;
                break;
            case "rejected":
                message = emptyStateRejectedRewardHistory;
                break;
        }
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            Assert.assertTrue(rewardHistory.isEmptyRewardHistoryImageDisplayed(), "Image doesn't exist");
        }
        Assert.assertEquals(rewardHistory.getEmptyRewardHistoryTitle(), "Belum Ada Hadiah", "Text doesn't match");
        Assert.assertEquals(rewardHistory.getEmptyRewardHistoryMessage(),message,"Text doesn't match");
    }

    @Then("user verify reward {string} status changed to {string}")
    public void user_verify_reward_status_changed_to(String rewardName, String status){
        for (int i=0; i<rewardHistory.getRedemptionCardNumber(); i++){
            boolean found = rewardHistory.getRewardTitleText().equals(rewardName);
            if(found){
                Assert.assertEquals(rewardHistory.getRedemptionStatus(i), status, "status doesn't match");
                break;
            }
        }
    }

    @And("user verify redemption id displayed")
    public void user_verify_redemption_id_displayed() {
        Assert.assertTrue(rewardHistory.isRedemptionIdDisplayed(), "redemption id is not exist");
    }

    @And("user verify {string} redemption notes")
    public void user_verify_redemption_notes(String status) {
        String notes = "";
        if(status.equals("rejected")){
            notes = "redemption rejected";
        }
        else if(status.equals("succeed")){
            notes = "redemption succeed";
        }
        Assert.assertEquals(rewardHistory.getRedemptionNotes(),notes,"notes doesn't match");
    }

    @And("user verify status {string} on redemption detail")
    public void user_verify_status_on_redemption_detail(String status) {
        Assert.assertEquals(rewardHistory.getRedemptionFinalStatus(),status,"final status doesn't match");
    }

    @When("user clicks on back button")
    public void user_clicks_on_back_button() {
        redeemReward.clickOnBackButton();
    }

    @Then("user verify back button displayed")
    public void user_verify_back_button_displayed() {
        Assert.assertTrue(rewardList.isBackButtonDisplayed() || tnc.isBackButtonDisplayed());
    }

    @And("user verify owner mamipoint displayed")
    public void user_verify_owner_mamipoint_displayed() {
        Assert.assertEquals(rewardList.getPointLabelTextOnRewardList(),"668 Poin","Point doesn't match");
    }

    @And("user verify Help button displayed")
    public void user_verify_help_button_displayed() {
        Assert.assertTrue(rewardList.isHelpButtonDisplayed());
    }

    @And("user verify targetted reward list displayed")
    public void user_verify_targetted_reward_list_displayed() {
        Assert.assertTrue(rewardList.isRewardDisplayed(longRewardName) &&
                rewardList.isRewardDisplayed(twoLineRewardName) &&
                rewardList.isRewardDisplayed(userRedeemRewardName));
    }

    @Then("user redirect to detail reward")
    public void user_redirect_to_detail_reward() {
        Assert.assertTrue(rewardDetail.isRewardTitleDisplayed());
        Assert.assertTrue(rewardDetail.isBatasPenukaranLabelDisplayed());
        Assert.assertTrue(rewardDetail.isDateLabelDisplayed());
        Assert.assertTrue(rewardDetail.isPoinTukarLabelDisplayed());
        Assert.assertTrue(rewardDetail.isPointLabelDisplayed());
        Assert.assertTrue(rewardDetail.isDescriptionLabelDisplayed());
        Assert.assertTrue(rewardDetail.isDescriptionTextDisplayed());
        Assert.assertTrue(rewardDetail.isTermAndConditionTabDisplayed());
        Assert.assertTrue(rewardDetail.getHowToRedeemTab());
        Assert.assertTrue(rewardDetail.isTermAndConditionListDisplayed());
    }

    @And("user scroll down reward list")
    public void user_scroll_down_reward_list() throws InterruptedException {
        rewardList.scrollDownRewardList();
    }

    @Then("user verify MamiPoin header is floating")
    public void user_verify_mamiPoin_header_is_floating() {
        Assert.assertTrue(rewardList.verifyMamiPoinHeaderDisplayed());
    }

    @Then("user verify Poin Saya is displayed")
    public void user_verify_poin_saya_is_displayed() {
        Assert.assertTrue(mamipoin.verifyPoinSayaDisplayed());
    }

    @Then("user verify description on informasi poin tenant is displayed")
    public void user_verify_description_on_informasi_poin_tenant_is_displayed() {
        Assert.assertTrue(mamipoin.verifyDescriptionOnInformasiPoinTenantDisplayed());
    }

    @Then("user verify table title on informasi poin tenant is displayed")
    public void user_verify_table_title_on_informasi_poin_tenant_is_displayed() {
        Assert.assertTrue(mamipoin.verifyTableTitleOnInformasiPoinTenantDisplayed());
    }

    @Then("user verify expired date on informasi poin tenant is displayed")
    public void user_verify_expired_date_on_informasi_poin_tenant_is_displayed() {
        Assert.assertTrue(mamipoin.verifyExpiredDateOnInformasiPoinTenantDisplayed());
    }

    @Then("user verify expired poin on informasi poin tenant is displayed")
    public void user_verify_expired_poin_on_informasi_poin_tenant_is_displayed() {
        Assert.assertTrue(mamipoin.verifyExpiredPoinOnInformasiPoinTenantDisplayed());
    }

    @Then("user verify Riwayat Poin is displayed")
    public void user_verify_riwayat_poin_is_displayed() {
        Assert.assertTrue(mamipoin.verifyRiwayatPoinDisplayed());
    }

    @Then("user verify Riwayat Poin is not displayed")
    public void user_verify_riwayat_poin_is_not_displayed() {
        Assert.assertFalse(mamipoin.verifyRiwayatPoinDisplayed());
    }

    @Then("user verify the image of empty Riwayat Poin is displayed")
    public void user_verify_the_image_of_empty_riwayat_poin_is_displayed() {
        Assert.assertTrue(mamipoin.verifyImageOfEmptyRiwayatPoinDisplayed());
    }

    @Then("user verify tidak ada poin yang tersedia is displayed")
    public void user_verify_tidak_ada_poin_yang_tersedia_is_displayed() {
        Assert.assertTrue(mamipoin.verifyTidakAdaPoinYangTersediaDisplayed());
    }

    @Then("user verify lihat caranya button is displayed")
    public void user_verify_lihat_caranya_button_is_displayed() {
        Assert.assertTrue(mamipoin.verifyLihatCaranyaButtonDisplayed());
    }

    @Then("user verify Dapatkan Poin Section is displayed")
    public void user_verify_dapatkan_poin_section_is_displayed() {
        Assert.assertTrue(mamipoin.verifyDapatkanPoinSectionDisplayed());
    }

    @Then("user verify sorted reward based on point ascending")
    public void user_verify_sorted_reward_based_on_point_ascending() {
        List<String> rewardNameList = Arrays.asList(userRedeemRewardName,longRewardName,twoLineRewardName);
        for(int i=0 ; i<3 ; i++){
            Assert.assertEquals(rewardList.getRewardName(i),rewardNameList.get(i));
        }
    }

    @And("user clicks on Tukar Poin Button on detail reward")
    public void user_clicks_on_tukar_poin_button_on_detail_reward() throws InterruptedException {
        rewardDetail.clickOnRedeemButton();
    }


    @Then("user verify field {string} autofilled with {string}")
    public void user_verify_field_autofilled_with(String fieldName, String value) {
        if(fieldName.equals("Nama")){
            Assert.assertEquals(redeemReward.getName(),value,"Name is not match with "+value);
        }
        else if(fieldName.equals("No HP yang bisa dihubungi")){
            Assert.assertEquals(redeemReward.getContactPhoneNumber(),value,"Phone number is not match with "+value);
        }
    }

    @And("user verify field {string} displayed")
    public void user_verify_field_displayed(String fieldName) {
        if (fieldName.equals("Alamat Pengiriman Hadiah")){
            Assert.assertTrue(redeemReward.isShippingAddressDisplayed(),"Field is not exist");
        }
        else if (fieldName.equals("Catatan")){
            Assert.assertTrue(redeemReward.isNotesDisplayed(),"Field is not exist");
        }
        else if (fieldName.equals("No Meter Listrik Anda")){
            Assert.assertTrue(redeemReward.isMeterNumberFieldDisplayed(),"Field is not exist");
        }
        else if (fieldName.equals("No HP Penerima Voucher Pulsa")){
            Assert.assertTrue(redeemReward.isRecipientPhoneNumberDisplayed(),"Field is not exist");
        }
    }

    @And("user verify checkbox {string} displayed")
    public void user_verify_checkbox_displayed(String text) {
        Assert.assertTrue(redeemReward.isPhoneNumberCheckboxDisplayed(),"Checkbox is not exist");
        Assert.assertEquals(redeemReward.getPhoneNumberCheckboxText(),text,"Text is not match with "+text);
    }

    @Then("user verify point deducted")
    public void user_verify_point_deducted() {
        int deductedPoint = userPointValue - rewardPointValue;
        Assert.assertEquals(mamipoin.getOwnerMamiPoinValue(),deductedPoint);
    }

    @And("user clicks on MamiPoin Owner entry point")
    public void user_clicks_on_mamiPoin_owner_entry_point() {
        mamipoin.clickOnMamiPoinOwnerEntryPoint();
    }

    @Then("user verify Contact Us section displayed")
    public void user_verify_contact_us_section_displayed() throws InterruptedException {
        Assert.assertTrue(rewardHistory.isContactUsButtonDisplayed(),"Contact Us Button is not displayed");
        Assert.assertTrue(rewardHistory.isNeedHelpSectionDisplayed(),"Section is not displayed");

    }

    @Then("user verify FAQ title")
    public void user_verify_faq_title() {
        Assert.assertTrue(rewardHistory.isFAQTitleDisplayed(),"FAQ title is not displayed");
    }

    @And("user verify question")
    public void user_verify_question(List<String> question) {
        for (int i = 0; i<question.size(); i++){
            if (Constants.MOBILE_OS == Platform.IOS){
                i = i + 1;
            }
            Assert.assertEquals(question.get(i), rewardHistory.getQuestionsText(i));
        }
    }

    @When("user expand/collapse {string} question")
    public void user_expand_question(String order) throws InterruptedException {
        int index = 0;
        if(order.equals("second")){
            index = 1;
        }
        rewardHistory.clickOnExpandCollapseButton(index);
    }

    @Then("user verify answer displayed")
    public void user_verify_answer_displayed() {
        Assert.assertTrue(rewardHistory.isAnswerDisplayed(), "Answer is not displayed");
    }

    @And("user verify question collapsed")
    public void user_verify_question_collapsed() {
        if (Constants.MOBILE_OS == Platform.ANDROID){
            rewardHistory.isQuestionCollapsed();
        }
        else {
            Assert.assertFalse(rewardHistory.isAnswerDisplayed(), "Answer is displayed");
        }
    }

    @When("user clicks on Term and Condition")
    public void user_clicks_on_term_and_condition() {
        mamipoin.clickOnTermAndConditionButton();
    }

    @And("user verify Term and Condition header")
    public void user_verify_term_and_condition_header() {
        Assert.assertTrue(tnc.isTermAndConditionHeaderDisplayed());
    }

    @And("user verify Term and Condition list")
    public void user_verify_term_and_condition_list() {
        Assert.assertTrue(tnc.isTermAndConditionListDisplayed());
    }

    @And("user verify Point Scheme section")
    public void user_verify_point_scheme_section() throws InterruptedException {
        Assert.assertTrue(tnc.isPointSchemeSectionDisplayed());
    }

    @When("user click on {string} link")
    public void user_click_on_link(String content) {
        tnc.clickOnLink(content);
    }

    @Then("user verify on mamikos page {string}")
    public void user_verify_on_mamikos_page(String url) {
        Assert.assertEquals(tnc.getMamikosUrl(),url, "url is not match");
    }

    @And("user verify Reward History Button")
    public void user_verify_reward_history_button() {
        Assert.assertTrue(mamipoin.isRewardHistoryButtonDisplayed());
    }

    @And("user verify Point History Button")
    public void user_verify_point_history_button() {
        Assert.assertTrue(mamipoin.isPointHistoryButtonDisplayed());
    }

    @And("user verify Term and Condition Button")
    public void user_verify_term_and_condition_button() {
        Assert.assertTrue(mamipoin.isTermAndConditionButtonDisplayed());
    }

    @And("user verify Redeem Point Button")
    public void user_verify_redeem_point_button() {
        Assert.assertTrue(mamipoin.isRedeemPointButtonDisplayed());
    }

    @And("user verify mamipoin displayed")
    public void user_verify_mamipoin_displayed() {
        Assert.assertTrue(mamipoin.isOwnerMamiPoinValueDisplayed());
    }
}
