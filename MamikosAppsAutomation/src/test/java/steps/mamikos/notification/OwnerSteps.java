package steps.mamikos.notification;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.owner.NotificationPO;
import utilities.Constants;
import utilities.ThreadManager;

public class OwnerSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private NotificationPO notif = new NotificationPO(driver);
    private LoadingPO loading = new LoadingPO(driver);


    @Then("screen detail notification with tab Update and tap Tagihan is shown")
    public void screenDetailNotificationWithTabUpdateAndTapTagihanIsShown() {
        if(Constants.MOBILE_OS== Platform.ANDROID) {
            Assert.assertTrue(notif.isNotifUpdateTabDisplayed(), "Notification Update tab is not displayed!");
            Assert.assertTrue(notif.isNotifBillTabDisplayed(), "Notification Bill tab is not displayed!");
        }
    }

    @And("user tap back to Homepage")
    public void userTapBackToHomepage() throws InterruptedException {
        notif.clickBackButton();
        loading.waitLoadingFinish();
    }
}
