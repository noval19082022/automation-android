package steps.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.sl.In;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.DashboardPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DashboardSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private DashboardPO dashboardPO = new DashboardPO(driver);
    private PopUpPO popup = new PopUpPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private AppiumHelpers appium = new AppiumHelpers(driver);

    @Then("owner can sees title {string} with subtitle {string}")
    public void owner_can_sees_title_with_subtitle(String title, String subtitle) {
        dashboardPO.scrollToFinancialStatement();
        Assert.assertEquals(dashboardPO.getIncomeTitle(), title);
        Assert.assertEquals(dashboardPO.getIncomeSubtitle(), subtitle);
    }

    @Then("owner can sees {string} and {string} with their income")
    public void owner_can_sees_and_with_their_income(String titleTotal, String titleMonthly) throws InterruptedException {
        Assert.assertEquals(dashboardPO.getTotalIncomeSubtitle(), titleTotal);
        Assert.assertTrue(dashboardPO.getTotalIncomePrice() >= 0);
        Assert.assertEquals(dashboardPO.getMonthlyIncomeSubtitle(), titleMonthly);
        Assert.assertTrue(dashboardPO.getMonthlyIncomePrice() >= 0);
    }

    @Then("owner can sees month name")
    public void owner_can_sees_month_name() {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            String currentMonth = appium.getTimeStampIndonesia("MMM yyyy");
            Assert.assertEquals(dashboardPO.getIncomeFilterText(), currentMonth);
        } else {
            String currentMonth = java.getTimeStamp("MMM YYYY");
            Assert.assertEquals(dashboardPO.getIncomeFilterText(), currentMonth);
        }
    }
    @When("owner choose previous month on income month filter options")
    public void owner_choose_previous_month_on_income_month_filter_options() {
        dashboardPO.setMonthlyIncomeFilter(1);
    }

    @Then("owner can sees BBK FTUE pop-up")
    public void owner_can_sees_bbk_ftue_pop_up() {
        if(popup.checkFTUEBBK()){
            Assert.assertEquals(popup.getMessageViewText(), "Atur Biaya Tambahan di Kos Anda");
            Assert.assertEquals(popup.getWarningMessageTextView(), "Anda sekarang bisa menentukan biaya DP, deposit, denda, dan biaya tambahan lainnya untuk kos Anda.");
            Assert.assertEquals(popup.getCancelButtonText(), "Saya Mengerti");
            Assert.assertEquals(popup.getNextButtonText(), "Atur Sekarang");
        }
    }

    @When("owner taps on Saya Mengerti button")
    public void owner_taps_on_saya_mengerti_button() {
        popup.clickOnCancelButtonAlertPopUp();
    }

    @Then("owner can sees BBK FTUE pop-up is dismissed")
    public void owner_can_sees_bbk_ftue_pop_up_is_dismissed() {
        Assert.assertFalse(popup.isBookingFTUEVisible());
    }

    @When("owner taps on Atur Sekarang button")
    public void owner_taps_on_atur_sekarang_button() {
        popup.clickOnNextButtonAlertPopUp();
    }
}
