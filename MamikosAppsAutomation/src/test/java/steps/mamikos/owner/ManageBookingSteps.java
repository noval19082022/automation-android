package steps.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.booking.BookingConfirmContractPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ManageBookingSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private BookingConfirmContractPO manageBooking = new BookingConfirmContractPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private FooterPO footer = new FooterPO(driver);

    //Test Data Payment
    private String paymentProperties ="src/test/resources/testdata/mamikos/payment.properties";
    private String nameTenant_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"nameTenant");

    @When("user accept booking form squad {string} with booking duration {string}")
    public void user_clicks_accept_button_on_booking_with_booking_duration(String squadBooking, String duration) throws InterruptedException {
        popUp.clickOnUpdateAllKos();
        popUp.clickOnGetPointButton();
        manageBooking.clickOnBookingRequest();
        if (squadBooking.equals("payment")){
            Assert.assertEquals(manageBooking.getRentDuration().toLowerCase(), duration, "Duration not match");
            Assert.assertEquals(manageBooking.getTenantName(), nameTenant_, "Tenant name not match");
            manageBooking.clickOnAcceptButton();
        }
        manageBooking.clickOnYesAcceptButton();
    }

    @Then("system display empty booking data with status need confirmation")
    public void system_display_empty_booking_data_with_status_need_confirmation() throws InterruptedException {
        manageBooking.bookingDataEmpty();
    }

    @When("user confirm terminate kos")
    public void user_confirm_terminate_kos() {
        Assert.assertTrue(manageBooking.getTitleMessage().contains(nameTenant_), "Tenant name not " + nameTenant_);
        manageBooking.confirmTerminateBooking();
    }
    @Then("system display warning message {string}")
    public void system_display_warning_message(String text) {
        Assert.assertEquals(manageBooking.getWarningMessage(), text, "Warning message not " + text);
    }

    @Then("system display terminate contract success {string}")
    public void system_display_terminate_contract_succes(String text) {
        Assert.assertEquals(manageBooking.getTerminateContractSuccess(), text, "Warning message not " + text);
    }

    @When("system display tagihan penyewa tab")
    public void system_display_tab_tagihan_penyewa() {
        manageBooking.confirmTagihanPenyewaTab();
    }

    @When("system display belum bayar button")
    public void system_display_button_belum_bayar() {
        manageBooking.confirmBelumBayarButton();
    }

    @When("system display ditransfer button")
    public void system_display_button_ditransfer() {
        manageBooking.confirmDiTransferButton();
    }

    @When("system display di mamikos button")
    public void system_display_button_di_mamikos() {
        manageBooking.confirmDiMamikosButton();
    }

    @When("system display lihat tagihan penyewa button")
    public void system_display_button_lihat_tagihan_penyewa() {
        manageBooking.confirmLihatTagihanPenyewaButton();
    }

    @When("system display booking langsung tab")
    public void system_display_booking_langsung_tab(){
        manageBooking.confirmBookingLangsungTab();
    }

    @Then("system display lihat booking button")
    public void system_display_lihat_booking_button(){
        manageBooking.confirmLihatBookingButton();
    }

    @Then("system display permintaan sewa button")
    public void system_display_permintaan_sewa_button(){
        manageBooking.confirmRentRequestButton();
    }

    @Then("system display kelola booking langsung button")
    public void system_display_kelola_booking_langsung_button(){
        manageBooking.confirmKelolaBookingButton();
    }

    @When("user click belum bayar button")
    public void user_click_belum_bayar_button(){
        manageBooking.clickBelumBayarButton();
    }

    @When("user click lihat tagihan penyewa button")
    public void user_click_lihat_tagihan_penyewa_button(){
        manageBooking.clickLihatTagihanPenyewaButton();
    }

    @Then("system display belum bayar tab")
    public void system_display_belum_bayar_tab(){
        manageBooking.confirmBelumBayarTab();
    }

    @When("user click di mamikos button")
    public void user_click_di_mamikos_button(){
        manageBooking.clickDiMamikosButton();
    }

    @Then("system display di mamikos tab")
    public void system_display_di_mamikos_tab(){
        manageBooking.confirmDiMamikosTab();
    }

    @When("user click ditransfer button")
    public void user_click_ditransfer_button(){
        manageBooking.clickDiTransferButton();
    }

    @Then("system display ditransfer tab")
    public void system_display_ditransfer_tab(){
        manageBooking.confirmDiTransferTab();
    }

    @When("user click tagihan kost name dropdown")
    public void user_click_tagihan_kost_name_dropdown(){
        manageBooking.clicktagihanKostNameDropdown();
    }

    @When("user choose {string} list kost")
    public void user_choose_list_kost(String kostName){
        manageBooking.chooseTagihanKostName(kostName);
    }

    @Then("system display detail tagihan kost {string}")
    public void system_display_detail_tagihan_kost(String kostName){
        Assert.assertEquals(manageBooking.getTagihanKostName(kostName), kostName, "kost name is different");
    }

    @When("user choose {string} hitungan sewa")
    public void user_choose_hitungan_sewa(String hitunganSewa){
        manageBooking.chooseHitunganSewa(hitunganSewa);
    }

    @When("system display {string} hitungan sewa")
    public void system_display_hitungan_sewa(String hitunganSewa){
        Assert.assertEquals(manageBooking.getHitunganSewa(hitunganSewa), hitunganSewa, "hitungan sewa is different");
    }

    @When("user click back on ios")
    public void user_click_back_on_ios(){
        manageBooking.clickIcnBack();
    }

    @When("user click tagihan penyewa button on profile page")
    public void user_click_tagihan_penyewa_button_on_profile_page(){
        manageBooking.clickTagihanButton();
    }

    @When("system display {string} and cari kos button")
    public void system_display_Belum_Ada_Kos_yang_Disewa_and_cari_kos_button (String defaultPage) {
        Assert.assertEquals(manageBooking.getTextDefaultTagihanPage(),defaultPage, "text is different");

    }

    @When("user click kontrak tab")
    public void user_click_kontrak_tab(){
        manageBooking.clickKontrakTab();
    }

    @Then("system display data kontrak")
    public void system_display_data_kontrak (){
        manageBooking.verifyDataKontrak();
    }

    @Then("user navigates to Booking Request")
    public void user_navigates_to_booking_request() {
        manageBooking.clickOnBookingIsWaitingButton();
    }

}
