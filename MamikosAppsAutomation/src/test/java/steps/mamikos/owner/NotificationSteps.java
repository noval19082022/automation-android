package steps.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.NotificationPO;
import utilities.Constants;
import utilities.ThreadManager;

public class NotificationSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private NotificationPO notification = new NotificationPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);

    @When("user as owner click first notification with message {string}")
    public void user_as_owner_click_first_notification_with_message(String text) throws InterruptedException {
        popUp.clickOnCancelEmailVerifButton();
        notification.clickNotificationIcon();
        Assert.assertTrue(notification.getNotificationMessage().contains(text), "Notification message not " + text);
        notification.clickOnFirstNotification();
    }


    @And("user clicks notification button")
    public void user_clicks_notification_button() throws InterruptedException{
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        }
        notification.clickNotificationIcon();
    }

    @Then("user see notification with text {string}")
    public void user_see_notification(String textNotification) {
        notification.getNotificationMessage();
        Assert.assertEquals(notification.getNotificationMessage(), textNotification,"Notification is " + notification.getNotificationMessage());
    }

    @Then("user see notification payment success with text {string}")
    public void user_see_notification_payment_success(String textNotification) {
        Assert.assertTrue(notification.getNotificationSuccessPaymentMessage().contains(textNotification), "Notification message not " + textNotification);
    }

    @And("user click first notification")
    public void user_click_first_notification() {
        notification.clickOnFirstNotification();
    }
}
