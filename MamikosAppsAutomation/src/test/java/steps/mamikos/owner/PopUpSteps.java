package steps.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.mamikos.owner.PopUpPO;
import utilities.ThreadManager;

public class PopUpSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private PopUpPO popUp = new PopUpPO(driver);

    @When("user dismiss pop-up")
    public void user_dismiss_popup() {
        popUp.dismissPopUp();
    }
}
