package steps.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.owner.DashboardPO;
import pageobjects.mamikos.owner.booking.BookingConfirmContractPO;
import pageobjects.mamikos.owner.updateroomandprice.DetailsKostPO;
import pageobjects.mamikos.owner.updateroomandprice.KostListPO;
import pageobjects.mamikos.tenant.booking.PriceDetailsPO;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.List;

public class UpdateRoomAndPriceSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private KostListPO kostList = new KostListPO(driver);
    private DashboardPO dashboard = new DashboardPO(driver);
    private DetailsKostPO detailKost = new DetailsKostPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private PriceDetailsPO priceDetails = new PriceDetailsPO(driver);
    private BookingConfirmContractPO manageBooking = new BookingConfirmContractPO(driver);

    @When("user goes to update room and price page")
    public void user_goes_update_room_and_price_page() {
        dashboard.tapOnUpdateRoomAndPrice();
    }

    @When("user taps on kost list with name {string}")
    public void user_taps_on_kost_list_with_name(String kostName) throws InterruptedException {
        kostList.clickOnKostList(kostName);
    }

    @Then("^user can sees price list :$")
    public void user_can_sees_price_list(List<String> list) throws InterruptedException {
        for (String priceList : list) {
            if (Constants.MOBILE_OS == Platform.ANDROID) {
                detailKost.scrollToPriceNameList(priceList);
                Assert.assertEquals(detailKost.getPriceListStatus("displayed", priceList), "true");
            } else {
                Assert.assertEquals(detailKost.getPriceListStatusIos("value", priceList), priceList);
            }
        }
    }
    @When("user taps on {string}")
    public void user_taps_on_see_other_price_button(String currentText) {
        detailKost.clickOnAnotherPriceButton(currentText);
    }

    @Then("^user cannot sees price list :$")
    public void user_cannot_sees_price_list(List<String> list) throws InterruptedException {
        for (String s : list) {
            Thread.sleep(3000);
            Assert.assertEquals(detailKost.getPriceListStatus("enabled", s), "false", s+ " is visible");
        }
    }

    @When("user updates main price to {string}")
    public void user_updates_main_price_to(String price) throws InterruptedException {
        detailKost.setInputMainPrice(price);
        detailKost.tapOnUpdatePriceButton();
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            loading.waitLoadingFinish();
        }
    }

    @Then("user cannot sees other price options")
    public void user_cannot_sees_other_price_options() {
        Assert.assertFalse(detailKost.isAnotherPriceIsPresent(), "Another price is present");
    }

    @When("user activates deposit toggle")
    public void user_activates_deposit_toggle() {
        detailKost.tapOnDepositPriceToggle();
    }

    @Then("user can sees {string} price popup")
    public void user_can_sees_deposit_price_popup(String name) {
        if(name.equalsIgnoreCase("deposit")) {
            Assert.assertEquals(detailKost.getAdditionalPriceInputTitle("deposit"), "Biaya Deposit", "Popup is not deposit popup");
        }
        else if(name.equalsIgnoreCase("down payment")) {
            Assert.assertEquals(detailKost.getAdditionalPriceInputTitle("down payment"), "Biaya Uang Muka", "Popup is not deposit popup");
        }
        else if(name.equalsIgnoreCase("denda")){
            Assert.assertEquals(detailKost.getAdditionalPriceInputTitle("denda"), "Biaya Denda", "Popup is not deposit popup");
        }
        else {
            Assert.assertEquals(detailKost.getAdditionalPriceInputTitle(name), "Biaya Lainnya Per Bulan", "Popup is not deposit popup");
        }
    }

    @When("user inputs deposit price for {string}")
    public void user_inputs_deposit_price_for_x(String depositPrice) {
        detailKost.setMainAdditionalPrice(depositPrice);
    }

    @When("user taps on save price button")
    public void user_taps_on_save_price_button() {
        detailKost.tapOnSavePriceButton();
        Assert.assertEquals(detailKost.getPopUpConfirmationText(), "Biaya berhasil diubah", "Pop-up message is wrong");
    }

    @Then("user can sees message confirmation is {string}")
    public void user_can_sees_message_confirmation_is_x(String confirmationMessage) {
        Assert.assertEquals(detailKost.getPopUpConfirmationText(), confirmationMessage, "Message is not " + confirmationMessage);
    }

    @Then("user can sees {string} added to the list")
    public void user_can_sees_deposit_added_to_the_list(String additionalName) {
        if (additionalName.equalsIgnoreCase("deposit")) {
            Assert.assertEquals(detailKost.getActiveAdditionalPriceText(), "Deposit", "Active additional price is not deposit");
        }
        else if(additionalName.equalsIgnoreCase("denda")) {
            Assert.assertEquals(detailKost.getActiveAdditionalPriceText(), "Denda", "Active additional price is not denda");
        }

    }

    @Then("user can sees deposit price is {string}")
    public void user_can_sees_deposit_price_is_x(String depositPrice) throws InterruptedException {
        Assert.assertEquals(detailKost.additionalPriceActivePriceText(), depositPrice, "Active price is not set or equal");
    }

    @When("user taps on delete button")
    public void user_taps_on_delete_button() {
        detailKost.tapOnDeleteAdditionalPriceButton();
    }

    @Then("confirmation popup for deleting {string} price is appear")
    public void confirmation_popup_for_deleting_x_price_is_appear(String confirmationName) {
        switch (confirmationName) {
            case "deposit":
                Assert.assertEquals(detailKost.getPopUpConfirmationAdditionalPriceText(), "Yakin hapus biaya Deposit ?", "Confirmation popup is not deposit");
                break;
            case "denda":
                Assert.assertEquals(detailKost.getPopUpConfirmationAdditionalPriceText(), "Yakin hapus biaya Denda ?", "Confirmation popup is not Denda");
                break;
            case "down payment":
                Assert.assertTrue(detailKost.getPopUpConfirmationAdditionalPriceText().contains("Yakin hapus biaya DP"), "Confirmation pop is not for Down Payment");
                break;
            default:
                Assert.assertEquals(detailKost.getPopUpConfirmationAdditionalPriceText(), "Yakin hapus biaya " + confirmationName +" ?", "Confirmation popup is not Denda");
                break;
        }
    }

    @When("user taps on yes delete button")
    public void user_taps_on_yes_delete_button() throws InterruptedException {
        detailKost.tapOnNextButton();
        loading.waitLoadingAnimationDisappear();
    }

    @Then("user cannot sees price list in the {string} section")
    public void user_cannot_sees_price_list_in_the_x_section(String typeName) {
        if(typeName.equalsIgnoreCase("deposit")){
            Assert.assertTrue(detailKost.isDepositPriceInactive(), "Deposit is still active");
        }
        else if(typeName.equalsIgnoreCase("denda")){
            Assert.assertTrue(detailKost.isDepositPriceInactive(), "Denda is still active");
        }
    }

    @When("user taps on change deposit price button")
    public void user_taps_on_change_deposit_price_button() {
        detailKost.tapOnChangeButtonActiveAdditionalPrice();
    }

    @And("user activates denda toggle")
    public void user_activates_denda_toggle() throws InterruptedException{
        detailKost.tapOnDendaPriceToogle();
    }

    @And("user inputs denda price with {string} and biaya dibebankan price with {string}")
    public void user_inputs_denda_price_with_x_and_biaya_dibebankan_price_with_y(String dendaPrice, String dendaTime){
        detailKost.setDendaPrice(dendaPrice);
        detailKost.setDendaTime(dendaTime);
    }

    @When("user taps on change denda price button")
    public void user_taps_on_change_denda_price_button() throws InterruptedException{
        detailKost.tapOnUbahDenda();
    }

    @And("user inputs denda price with {string}")
    public void user_inputs_denda_price_with_price(String dendaPrice) throws InterruptedException{
        detailKost.setDendaPrice(dendaPrice);
    }

    @When("user taps on delete button on denda section")
    public void user_taps_on_delete_button_on_denda_section() throws InterruptedException {
        detailKost.tapOnDeleteDendaButton();
    }

    @When("user activates other price toggle")
    public void user_activates_other_price_toggle() {
        detailKost.tapOnOtherPriceToggle();
    }

    @Then("user can sees other price's input box")
    public void user_can_sees_other_prices_input_box() {
        Assert.assertEquals(detailKost.getOtherPriceTitleText(), "Biaya Lainnya Per Bulan", "Pop up box title text is wrong");
    }

    @When("user inputs other price name with {string}, total price with {int}")
    public void user_inputs_other_price_name_with_x_total_price_with_x(String priceName, int priceNumber) {
        detailKost.setOtherPriceName(priceName, false);
        detailKost.setOtherPriceNumber(priceNumber, false);
    }

    @Then("user can sees other price is in the active state")
    public void user_can_sees_other_price_is_in_the_active_state() {
        Assert.assertEquals(detailKost.getOtherPriceStateText(), "ON", "Other price is not active");
    }

    @Then("user can sees other price name is {string}, with price is {string}")
    public void user_can_sees_other_price_name_is_x_with_price_is_x(String otherPriceName, String otherPriceNumber) {
        Assert.assertEquals(detailKost.getOtherPriceName(otherPriceName), otherPriceName, "Price name is wrong");
        Assert.assertEquals(detailKost.getOtherPriceNumber(otherPriceName, otherPriceNumber), otherPriceNumber, "Price name is wrong");
    }

    @Then("user change {string} name to {string} and price to {int}")
    public void user_change_x_name_to_x_and_price_to_x(String priceName, String priceNameEdit, int priceNumberEdit) {
        detailKost.tapOnChangeWithPriceName(priceName);
        detailKost.setOtherPriceName(priceNameEdit, true);
        detailKost.setOtherPriceNumber(priceNumberEdit, true);
    }

    @When("user deletes {string} from other price")
    public void user_deletes_x_from_other_price(String priceName) {
        detailKost.deleteAdditionPriceFromTheList(priceName);
    }

    @Then("{string} is disappear from the list")
    public void x_is_disappear_from_the_list(String priceName) {
        Assert.assertFalse(detailKost.isAdditionalPricePresent(priceName), "Additional Price Present");
    }

    @When("user taps on add more other price button")
    public void user_taps_on_add_more_other_price_button() {
        detailKost.tapOnAddMoreOtherPriceButton();
    }

    @When("user taps on down payment toggle")
    public void user_activates_down_payment_toggle() throws InterruptedException {
        detailKost.tapOnDownPaymentToggle();
    }

    @Then("user can sees down payment input price and detail price view")
    public void user_can_sees_down_payment_input_price() {
        Assert.assertTrue(detailKost.isDownPaymentPercentageChoicePresent(), "Down payment input box is not present");
        Assert.assertTrue(detailKost.isDownPaymentDetailPriceViewPresent(), "Down payment detail price view is not present");
    }

    @Then("user can sees default percentage is {string}")
    public void user_can_sees_default_percentage_is_x(String inputPercentage) {
        Assert.assertEquals(detailKost.getDownPaymentDefaultPercentage(), "10%", "Default percentage is not 10%");
    }

    @Then("^user can sees down payment price list :$")
    public void user_can_sees_down_payment_list(List<String> list) {
        for(int i = 0; i <= 4; i++) {
            Assert.assertEquals(detailKost.getDPAvailablePriceList(i + 1), list.get(i), "Available price is not: " + list.get(i));
        }
    }

    @When("user can sees down payment is in active state")
    public void user_can_sees_down_payment_is_in_active_state() {
        Assert.assertEquals(detailKost.getDownPaymentActiveState(), "ON", "Down Payment is not active");
        Assert.assertEquals(detailKost.getActiveDPPercentage(), "DP 10%", "Down payment active percentage is wrong");
        Assert.assertTrue(detailKost.getDPActivePriceRangeText().contains(" - Rp"), "Price range is not visible");
    }

    @Then("user can sees down payment text price")
    public void user_can_sees_down_payment_text_price() {
        Assert.assertEquals(priceDetails.getDownPaymentText(), "Uang Muka (DP)", "Down payment text is wrong");
        Assert.assertTrue(priceDetails.getDownPaymentPriceText().contains("Rp"), "Price is not present");
    }

    @When("user taps on delete down payment button")
    public void user_taps_on_delete_down_payment_button() {
        detailKost.tapsOnDownPaymentDeleteButton();
    }

    @When("user back to owner dashboard")
    public void user_back_to_owner_dashboard() {
        dashboard.backToOwnerDashboard();
    }

    @Then("user can see down payment state is {string}")
    public void user_can_see_down_payment_state_is_x(String dpState) {
        Assert.assertEquals(detailKost.getDownPaymentActiveState(), dpState, "Dp is not " + dpState);
    }

    @When("user taps on update price button")
    public void user_tap_on_update_price_button() {
        detailKost.tapOnUpdatePriceButton();
    }

    @Then("user can sees down payment percentage is {string} with price range {string}")
    public void user_can_sees_down_payment_percentage_is_x_with_price_range_x(String percentage, String priceRange) {
        Assert.assertTrue(detailKost.getActiveDPPercentage().contains(percentage), "Percentage set is not " + percentage);
        Assert.assertEquals(detailKost.getDPActivePriceRangeText(), priceRange, "Price range is not " + priceRange);
    }

    @Then("^user can sees dp is active with price are data below :$")
    public void user_can_sees_dp_is_avtive_with_price_are_data_below(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println("number " + i+1);
            System.out.println(list.get(i));
            priceDetails.tapOnPeriodeFilter(i+1);
            Assert.assertEquals(priceDetails.getDownPaymentText(), "Uang Muka (DP)", "Text is not Uang Muka (DP)");
            Assert.assertEquals(priceDetails.getDownPaymentPriceText(), list.get(i), "Price is not " + list.get(i));
        }
    }

    @When("user taps on down payment change's button")
    public void userTapsOnDownPaymentChangesButton() {
        detailKost.tapsOnDownPaymentChangeButton();
    }

    @When("user choose {string} as down payment price")
    public void user_choose_x_as_down_payment_price(String percentage) {
        detailKost.tapOnDPDropDown();
        detailKost.chooseDPPercentage(percentage);
    }

    @Then("owner can sees Update Harga kost section")
    public void owner_can_sees_update_harga_kost_section() {
        if(kostList.checkPageHargaKost()){
            Assert.assertEquals(kostList.getPageTitle(), "Update Harga");
            Assert.assertTrue(kostList.getKostList().size() > 0);
        }
    }

    @When("user goes to update room avaibility")
    public void user_goes_update_room_avaibility() {
        dashboard.tapOnUpdateRoomAvailability();
    }

    @When("user updates total room to {string}")
    public void user_updates_total_room_to(String total) throws InterruptedException{
        manageBooking.clickOnPopUpLaporanKeuangan();
        detailKost.inputTotalRoom(total);
        detailKost.tapPilihButton();
        detailKost.tapOnUpdateRoom();
    }
}