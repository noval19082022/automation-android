package steps.mamikos.owner;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.mamipay.DashboardPO;
import pageobjects.mamikos.owner.mamipay.UpdateKostPO;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

public class UpdateRoomSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private DashboardPO dashboard = new DashboardPO(driver);
    private UpdateKostPO updateKos = new UpdateKostPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);

    @And("user tap update room in Manage section")
    public void user_tap_update_room_in_Manage_section() throws InterruptedException {
        dashboard.clickUpdateRoom();
        loading.waitLoadingFinish();
    }

    @And("user tap update price in Manage section")
    public void user_tap_update_price_in_Manage_section() throws InterruptedException {
        dashboard.clickUpdatePrice();
        loading.waitLoadingFinish();
    }

    @Given("user select Kos {string} in update room page")
    public void user_select_Kos_in_update_room_page(String kosName) throws InterruptedException {
        updateKos.clickKosName(kosName);
        loading.waitLoadingFinish();
        popUp.clickUpdateAllKos();
    }

    @And("user update room total to {string}")
    public void user_update_room_total_to(String roomTotal) {
        updateKos.clickTotalRoomDropdown();
        updateKos.clickRoomTotalNumber(roomTotal);
        updateKos.clickChoose();
    }

    @And("user search room number {string}")
    public void user_search_room_number(String roomNo) {
        updateKos.inputRoomNo(roomNo);
    }

    @Then("user see text {string} in room name or number field")
    public void user_see_text_in_room_name_or_number_field(String roomNo) {
        Assert.assertEquals(updateKos.getFirstRoomNameNumber(), roomNo, "Room name is not updated");
    }

    @And("user see text {string} in floor \\(optional) field")
    public void user_see_text_in_floor_optional_field(String floor) {
        Assert.assertEquals(updateKos.getFirstFloor(), floor, "Floor is not updated");
    }

    @When("user click see other prices")
    public void user_click_see_other_prices() {
        updateKos.clickSeeOtherPrices();
    }

    @When("user click check box in price per month")
    public void user_click_check_box_in_price_per_month() {
        updateKos.clickMonthlyPriceCheckbox();;
    }

    @Then("user see error message {string} below price field in update price page")
    public void user_see_error_message_below_price_field_in_update_price_page(String message) {
        Assert.assertEquals(updateKos.getErrorMessagePrice(), message, "Error message below price is wrong");
    }

    @When("user input price {string} in update price monthly")
    public void user_input_price_in_update_price_monthly(String monthlyPrice) {
        updateKos.inputMonthlyPrice(monthlyPrice);
    }

    @When("user click check box in {string} price")
    public void user_click_check_box_in_price(String time) {
        updateKos.clickInputPriceCheckbox(time);
    }

    @When("user input price {string} in update price {string}")
    public void user_input_price_in_update_price(String price, String time) {
        updateKos.inputPrice(time, price);
    }

    @When("user click update price button")
    public void user_click_update_price_button() {
        updateKos.clickUpdatePriceButton();
    }

    @Then("user didn't see error message below price field in update price page")
    public void user_didn_t_see_error_message_below_price_field_in_update_price_page() {
        Assert.assertFalse(updateKos.isErrorMessageAppear(), "Error message below price is appear");
    }

    @Given("user tap on 3 dots button")
    public void user_tap_on_3_dots_button() {
        updateKos.click3DotsButton();
    }

    @When("user tap on Add Room button")
    public void user_tap_on_Add_Room_button() {
        updateKos.clickAddRoomButton();
    }

    @When("user tap on Delete Room button")
    public void user_tap_on_Delete_Room_button() {
        updateKos.clickDeleteRoomButton();
    }

    @When("user hide keyboard and scroll down")
    public void user_hide_keyboard_and_scroll_down() {
        appium.hideKeyboard();
        appium.scroll(0.5, 0.60, 0.30, 2000);
    }

    @When("user insert text {string} on {int} room in room name or number field")
    public void user_insert_text_on_room_in_room_name_or_number_field(String text, Integer number) {
        updateKos.inputNthRoomNameNumber(number.toString(), text);
    }

    @When("user insert text {string} on {int} floor in floor \\(optional) field")
    public void user_insert_text_on_floor_in_floor_optional_field(String text, Integer number) {
        updateKos.inputNthFloor(number.toString(), text);
    }
}
