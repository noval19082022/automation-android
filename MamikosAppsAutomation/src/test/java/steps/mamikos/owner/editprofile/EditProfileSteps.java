package steps.mamikos.owner.editprofile;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.editprofile.EditProfilePO;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.List;

public class EditProfileSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private EditProfilePO editProfilePO = new EditProfilePO(driver);

    @When("user click on invitation email banner")
    public void user_click_close_on_invitation_email_banner(){
        editProfilePO.clickOnInviteEmailBanner();
    }

    @And("owner click on edit profile")
    public void user_click_on_edit_profile(){
        editProfilePO.clickOnEditProfile();
    }

    @Then("user verify data profile information")
    public void user_verify_data_profile_information(List<String> information) throws InterruptedException {
        Assert.assertEquals(editProfilePO.getNameEditText(), information.get(0), "Profile name is not equal to " + information.get(0));
        Assert.assertEquals(editProfilePO.getPhoneNumberEditText(), information.get(1), "Phone number is not equal to " + information.get(1));
        Assert.assertEquals(editProfilePO.getEmailEditText(), information.get(2), "Email is not equal to " + information.get(2));
    }

    @And("user click to Payment Menu")
    public void user_click_to_Payment_Menu() {
        editProfilePO.clickOnPaymentMenu();
    }

    @Then("user verify bank account data")
    public void user_verify_bank_account_data(List<String> bankAccount) throws InterruptedException {
        Assert.assertEquals(editProfilePO.getBankNameText(), bankAccount.get(0), "Bank name is not equal to " + bankAccount.get(0));
        Assert.assertEquals(editProfilePO.getBankAccountNumberText(), bankAccount.get(1), "Bank account number is not equal to " + bankAccount.get(1));
        Assert.assertEquals(editProfilePO.getBankAccountNameText(), bankAccount.get(2), "Bank account name is not equal to " + bankAccount.get(2));
    }

    @When("owner change name to {string}")
    public void owner_change_name_to(String name) throws InterruptedException {
        editProfilePO.inputOwnerName(name);
    }

    @When("system display message error to change name")
    public void system_display_message_error_to_change_name(){
        Assert.assertEquals(editProfilePO.getTextInputError(), "Nama harus diisi minimal 4 karakter.", "expected is "+editProfilePO.getTextInputError());
    }

    @When("system display message success update data")
    public void system_display_message_success_update_data(){
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            Assert.assertEquals(editProfilePO.getToastMessageUpdateProfile(), "Berhasil mengupdate data Anda.","expected is "+editProfilePO.getToastMessageUpdateProfile() );
        }else{
            Assert.assertFalse(editProfilePO.isErrorMessageEditNamePresent(),"Message error is present");
        }
    }

}
