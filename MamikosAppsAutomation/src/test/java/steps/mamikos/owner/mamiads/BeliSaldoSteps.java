package steps.mamikos.owner.mamiads;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.mamiads.BeliSaldoPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class BeliSaldoSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private BeliSaldoPO beliSaldoPO = new BeliSaldoPO(driver);
    private SearchPO search = new SearchPO(driver);
    private JavaHelpers java = new JavaHelpers();

    @When("user clicks on Mamiads")
    public void user_clicks_on_Mamiads() {
        beliSaldoPO.clickOnSaldoMamiadsTitle();
    }
    @And("user clicks on beli saldo mamiads")
    public void user_clicks_on_beli_saldo_mamiads() throws InterruptedException {
        beliSaldoPO.clickOnBeliSaldoMamiadsButton();
    }

    @Then("system display page beli saldo mamiads with a title {string}")
    public void system_display_page_beli_saldo_mamiads_with_title(String title) {
        Assert.assertEquals(beliSaldoPO.getTitleBeliSaldoText(), title);
    }

    @And("user clicks on riwayat mamiads")
    public void user_clicks_on_riwayat_mamiads() {
        beliSaldoPO.clickOnRiwayatButton();
    }

    @Then("system display page riwayat mamiads with a title {string}")
    public void system_display_page_riwayat_mamiads_with_title(String title) throws InterruptedException {
        Assert.assertEquals(beliSaldoPO.getTitleRiwayatMamiadsText(), title);
    }

    @And("owner taps button Coba Sekarang mamiads")
    public void owner_taps_button_coba_sekarang_mamiads() throws InterruptedException{
        beliSaldoPO.clickOnCobaSekarangButton();
    }

    @And("owner taps tab statistik iklan")
    public void owner_taps_tab_statistik_iklan() {
        beliSaldoPO.clickOnTabStatistikIklanButton();
    }

    @And("owner taps tab iklan saya")
    public void owner_taps_tab_iklan_saya() {
        beliSaldoPO.clickOnTabIklanSayaButton();
    }

    @When("user click back to page mamiads")
    public void user_click_back_to_page_mamiads() {
        if(Constants.MOBILE_OS == Platform.IOS) {
            beliSaldoPO.clickOnIcnBack();
        }else {
            search.clickDeviceBackButton();
        }
    }
}
