package steps.mamikos.premium;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.premium.BalanceAllocationPO;
import utilities.ThreadManager;

public class BalanceAllocationSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private BalanceAllocationPO balancePO = new BalanceAllocationPO(driver);

    @When("user clicks on view apartment ads")
    public void user_clicks_on_view_apartment_ads() {
        balancePO.clickOnViewApartmentAds();
    }

    @And("user clicks on manage ads button")
    public void user_clicks_on_manage_ads_button() {
        balancePO.clickOnManageAdsButton();
    }

    @When("user fill allocation amount")
    public void user_fill_allocation_amount() {
        balancePO.fillAllocationAmount();
    }

    @And("user clicks on confirmation")
    public void user_clicks_on_confirmation() {
        balancePO.clickOnConfirmationButton();
    }

    @And("user clicks on manage balance allocation")
    public void user_clicks_on_manage_balance_allocation() {
        balancePO.clickOnManageBalanceAllocationButton();
    }

    @Then("user verify balance allocation with {string}")
    public void user_verify_balance_allocation(String balance) {
        Assert.assertEquals(balancePO.getAllocationBalanceText(), balance, "Balance allocation is not equal to " + balance);
    }

}
