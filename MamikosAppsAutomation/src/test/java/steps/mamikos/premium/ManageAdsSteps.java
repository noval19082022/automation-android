package steps.mamikos.premium;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.apartmentads.ApartemenAdsPO;
import pageobjects.mamikos.owner.premium.ManageAdsPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import utilities.ThreadManager;

public class ManageAdsSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ManageAdsPO manageAds = new ManageAdsPO(driver);
    private ApartemenAdsPO apartAds = new ApartemenAdsPO(driver);
    private SearchListingPO searchListing = new SearchListingPO(driver);

    @Then("user will see status top ads and balance")
    public void user_will_see_status_top_ads_and_balance() {
        if(manageAds.isAdsBalancePricePresent()){
            Assert.assertTrue(manageAds.isAdsSwitchViewPresent(), "Switch button enable / disable ads is not present!");
        } else {
            manageAds.activatedPromotedAds();
            Assert.assertTrue(manageAds.isAdsBalancePricePresent(), "Active balance is not present!");
        }
    }

    @And("user clicks on filter time range on manage iklan")
    public void user_clicks_on_filter_time_range_on_manage_iklan() {
        manageAds.clickFilterTime();
    }

    @And("user sees the number of view and like count")
    public void user_sees_the_number_of_view_and_like_count() {
        Assert.assertTrue(manageAds.isLikeCountPropertyTextPresent(), "Like count property is not present!");
        Assert.assertTrue(manageAds.isViewCountPropertyTextPresent(), "View count property is not present!");
    }

    @And("user sees the number of click statistic")
    public void user_sees_the_number_of_click_statistic() {
        searchListing.back();
        Assert.assertTrue(manageAds.isClickStatisticPresent(), "click statistic is not present!");
    }

    @And("user clicks on manage balance allocation button")
    public void user_clicks_on_manage_balance_allocation_button() {
        manageAds.clickManageBalanceAllocation();
    }

    @And("user set balance")
    public void user_set_balance(){
        manageAds.setAllocationBalance();
    }


}
