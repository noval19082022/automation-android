package steps.mamikos.premium;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.premium.PremiumPagePO;
import utilities.ThreadManager;

public class PremiumPageSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private PremiumPagePO premium = new PremiumPagePO(driver);

    @And("user clicks on preview ads kos from Premium page")
    public void user_clicks_on_preview_ads_kos_from_premium_page() throws InterruptedException {
        premium.clickPreviewKostAds();
    }

    @And("user clicks on manage ads from Premium ads page")
    public void user_clicks_on_manage_ads_from_premium_ads_page() {
        premium.clickManageKosButton();
    }

    @Then("user check activate daily balance premium has actived from premium page")
    public void user_check_activate_daily_balance_premium_has_actived_from_premium_page() throws InterruptedException {
        premium.clickActivateDailyBalance();
        Assert.assertTrue(premium.isConfirmNonActivePopUpPresent(), "Confirm Non Active Daily Balance is not present!");
    }

    @And("user click on Nonactivate daily balance premium")
    public void user_click_on_Nonactivate_daily_balance_premium() {
        premium.clickNonactivateDailyBalance();
    }
}
