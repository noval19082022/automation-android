package steps.mamikos.premium;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.premium.UpgradePremiumPO;
import pageobjects.mamikos.tenant.booking.DetailBookingPO;
import utilities.Constants;
import utilities.ThreadManager;

public class UpgradePremiumSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private UpgradePremiumPO premiumPO = new UpgradePremiumPO(driver);
    private DetailBookingPO detailBooking = new DetailBookingPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);

    @Then("user clicks atur premium and navigates to premium page")
    public void user_clicks_atur_premium_and_navigates_to_premium_page() throws InterruptedException {
        premiumPO.clickOnAturPremium();
    }

    @And("user clicks lihat paket")
    public void user_clicks_lihat_paket() throws InterruptedException {
        premiumPO.clickOnLihatPaket();
    }

    @And("user clicks pilih paket")
    public void user_clicks_pilih_paket() throws InterruptedException {
        premiumPO.clickOnPilihPaket();
    }

    @Then("user clicks back to cancel upgrade")
    public void user_clicks_back_to_cancel_upgrade() throws InterruptedException {
        premiumPO.tapbackbtn();
    }

    @Then("user close premium faq")
    public void user_close_premium_faq() throws InterruptedException {
        premiumPO.closefaq();
    }

    @And("user close pop up premium free")
    public void user_close_pop_up_premium_free() throws InterruptedException {
        premiumPO.closePremiumFree();
    }

    @Then("user choose Bank BNI")
    public void user_choose_bank_bni() throws InterruptedException {
        premiumPO.clickBankBNI();
    }

    @And("user clicks pilih metode")
    public void user_clicks_pilih_metode() throws InterruptedException {
        premiumPO.clickPilihMetode();
    }

    @Then("user verify on the page Bank BNI in Aktivasi Premium page")
    public void user_verify_on_the_page_bank_bni_in_aktivasi_premium_page() {
        Assert.assertEquals(premiumPO.getBankName(), "Bank BNI", "Bank is not BNI");
    }

    @And("user clicks on beli saldo")
    public void user_clicks_on_beli_saldo() {
        premiumPO.clickOnBeliSaldo();
    }

    @And("user clicks on konfirmasi pembayaran")
    public void user_clicks_on_konfirmasi_pembayaran() {
        premiumPO.clickOnKonfirmasiPembayaran();
    }

    @And("user clicks on submit button")
    public void user_clicks_on_submit_button() {
        premiumPO.clickOnSubmitBtn();
    }

    @Then("user verify on Nama Pemilik Rekening")
    public void user_verify_on_nama_pemilik_rekening() {
        Assert.assertEquals(premiumPO.getWarningText(),"Masukkan Nama Pemilik Nomor Rekening","Wrong Warning Text");
    }

    @And("user fills on Nama Pemilik Rekening {string}")
    public void user_fills_on_nama_pemilik_rekening(String name) {
        premiumPO.fillNameform(name);
    }

    @Then("user verify on Nama Bank Pengirim")
    public void user_verify_on_nama_bank_pengirim() {
        Assert.assertEquals(premiumPO.getWarningText(),"Masukkan Nama Bank Pengirim","Wrong Warning Text");
    }

    @And("user clicks on ganti paket")
    public void userClicksOnGantiPaket() {
        premiumPO.clickOnChangePackage();
    }

    @And("user choose {string}")
    public void userChoose(String price) {
        premiumPO.chooseOnPackage(price);
    }

    @Then("user verify {string}")
    public void userVerify(String text) {
        Assert.assertEquals(premiumPO.getSaldoIklan(), text , "Amount different");
    }

    @And("user click on FTUE cek properti")
    public void user_click_on_ftue_cek_properti() {
        premiumPO.clickOnFTUE();
    }

    @And("user click on lihat paket button")
    public void user_click_on_lihat_paket_button() throws InterruptedException {
        premiumPO.clickOnLihatPaket();
    }

    @And("user click back button")
    public void user_click_back_button() throws InterruptedException {
        premiumPO.clickOnBack(16, 68);
    }

    @And("user click on onboarding mengapa premium")
    public void user_click_on_onboarding_mengapa_premium() {
        premiumPO.clickOnOnboardingPremium();
    }


    @Then("user verify Bayar button is appear")
    public void user_verify_bayar_button_is_appear() {
        Assert.assertTrue(premiumPO.getBayarSekarang(), "Bayar Sekarang button doesn't appear");
    }

    @Then("user choose package {string}")
    public void userChoosePackage(String text) {
        premiumPO.clickOnPackage(text);
    }

    @Then("user verify name of package is {string}")
    public void userVerifyNameOfPackageIs(String packageName) {
        Assert.assertTrue(premiumPO.getTextPackageName(packageName), "Package name doesn't match!");
    }

    @And("user click on back icon")
    public void userClickOnBackIcon() throws InterruptedException {
        premiumPO.clickOnBackIconButton();
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        }
    }

    @And("user click on back button navigate to chat list")
    public void user_click_back_button_navigate_to_chat_list() throws InterruptedException {
        premiumPO.clickOnBack(16,59);
    }
}
