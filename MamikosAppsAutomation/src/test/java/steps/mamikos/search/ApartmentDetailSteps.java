package steps.mamikos.search;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.tenant.search.ApartmentDetailPO;
import utilities.AppiumHelpers;
import utilities.ThreadManager;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class ApartmentDetailSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ApartmentDetailPO apartmentDetail = new ApartmentDetailPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private LoadingPO loading = new LoadingPO(driver);

    @Then("system display apartment photos")
    public void system_display_apartment_photos() {
        apartmentDetail.isApartmentPhotosPresent();
    }

    @Then("system display apartment furniture type")
    public void system_display_apartment_furniture_type() {
        apartmentDetail.isApartmentFurnitureTypePresent();
    }

    @Then("system display apartment name in detail page")
    public void system_display_apartment_name_in_detail_page() {
        apartmentDetail.isApartmentNameOnDetailPagePresent();
    }

    @Then("system display apartment address")
    public void system_display_apartment_address() {
        apartmentDetail.isApartmentAddressPresent();
    }

    @Then("system display apartment last update")
    public void system_display_apartment_last_update() {
        apartmentDetail.isApartmentLastUpdatePresent();
    }

    @And("user click on share button")
    public void userClickOnShareButton() throws InterruptedException {
        apartmentDetail.clickOnApartmentShareButton();
    }

    @And("user click on love button of apartment")
    public void userClickOnLoveButtonOfApartment() throws InterruptedException {
        apartmentDetail.clickOnApartmentLoveButton();
    }

    @And("user click on contact apartment button")
    public void userClickOnContactApartmentButton() throws InterruptedException {
        apartmentDetail.clickOnContactApartmentButton();
    }

    @Then("user scroll down to Apartment location and verify map view should be shown")
    public void userScrollDownToApartmentLocationAndVerifyMapViewShouldBeShown() throws InterruptedException {
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        appium.scroll(0.5, 0.80, 0.20, 2000);
        loading.waitLoadingFinish();
        Assert.assertTrue(apartmentDetail.isMapViewPresent(), "Map view is not shown");
    }

    @And("user scroll down to Recommended boarding section")
    public void userScrollDownToRecommendedBoardingSection() throws InterruptedException {
        for (int i=0; i<7; i++) {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        loading.waitLoadingFinish();
        appium.hardWait(3);
        apartmentDetail.navigateToRecommendedBoardingSection();

    }

    @Then("verify below apartment list")
    public void verifyBelowApartmentList(List<String> apartmentType) {
        for (String s : apartmentType) {
            assertTrue(apartmentDetail.isApartmentTypePresent(s), s + " is not present");
        }
    }

    @And("user swipe right and verify other apartment list should be shown")
    public void userSwipeRightAndVerifyApartmentListShouldBeShown(List<String> apartmentType) {
        appium.scrollHorizontal(0.5, 0.90, 0.10, 2000);
        for ( int i = 0; i < apartmentType.size() ; i++){
            assertTrue(apartmentDetail.isApartmentTypePresent(apartmentType.get(i)), apartmentType.get(i) + " is not present");
        }
    }

    @And("user scroll down to Interesting other apartment section")
    public void userScrollDownToInterestingOtherApartmentSection() throws InterruptedException {
        for (int i=0; i<6; i++) {
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
        loading.waitLoadingFinish();
        appium.hardWait(3);
        apartmentDetail.navigateToOtherInterestingApartmentSection();
    }

    @And("click on {string} option from share popup")
    public void clickOnOptionFromSharePopup(String shareOption) {
        apartmentDetail.clickOnShareOptionFromShareSheet(shareOption);
    }
}
