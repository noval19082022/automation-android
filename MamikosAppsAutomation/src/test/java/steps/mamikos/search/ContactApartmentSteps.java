package steps.mamikos.search;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.search.ContactApartmentPO;
import utilities.ThreadManager;

public class ContactApartmentSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ContactApartmentPO contactApartment = new ContactApartmentPO(driver);

    @Then("verify contact apartment page gets open with back button and Kirim button")
    public void verifyContactApartmentPageGetsOpenWithBackButtonAndKirimButton() {
        Assert.assertTrue(contactApartment.isContactApartmentPageHeaderDisplayed(), "Contact Apartment page header is not shown");
        Assert.assertTrue(contactApartment.isBackButtonDisplayed(), "Back button is not shown");
        Assert.assertTrue(contactApartment.isSendButtonDisplayed(), "Send button is not shown");
    }

    @And("user click on send button from contact apartment page")
    public void userClickOnSendButtonFromContactApartmentPage() {
        contactApartment.clickOnSendButton();
    }

}
