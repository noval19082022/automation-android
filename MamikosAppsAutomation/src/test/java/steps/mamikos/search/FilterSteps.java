package steps.mamikos.search;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.owner.booking.BookingConfirmContractPO;
import pageobjects.mamikos.tenant.booking.BookingDetailsPO;
import pageobjects.mamikos.tenant.search.FilterPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;

public class FilterSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private SearchPO search = new SearchPO(driver);
    private FilterPO filter = new FilterPO(driver);
    private SearchListingPO searchList = new SearchListingPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private BookingDetailsPO kosDetail = new BookingDetailsPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private BookingConfirmContractPO contract = new BookingConfirmContractPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);


    @Then("system display apartment filter list")
    public void system_display_apartment_filter_list(List<String> filterList) {
        for ( int i = 0; i < filterList.size() ; i++){
            Assert.assertTrue(filter.filterListPresent(filterList.get(i)), "Filter List " + filterList.get(i) + " is not present");
        }
    }

    @Then("System display option button")
    public void system_display_option_button(List<String> button) {
        for ( int i = 0; i < button.size() ; i++){
            Assert.assertTrue(filter.buttonPresent(button.get(i)), "Button " + button.get(i) + " is not present");
        }
    }

    @When("user click back")
    public void user_click_back_() throws InterruptedException{
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            search.clickDeviceBackButton();
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        } else {
            contract.clickOnIcnBack();
        }
    }

    @When("user filter apartment by {string} is {string}")
    public void user_filter_apartment_by_is(String filterBy, String filterValue) throws InterruptedException {
        filter.filterApartment(filterBy, filterValue);
    }

    @Then("system display apartment {string} is {string}")
    public void system_display_apartment_is(String filterBy, String filterValue) throws InterruptedException {
        Assert.assertEquals(filter.getApartmentUnitType(filterBy), filterValue, "Filter Apartment By " + filterBy + " is not " + filterValue);
    }

    @Then("kosan list appear correctly, relevant with the filter")
    public void kosanListAppearCorrectlyRelevantWithTheFilter(List<String> expected){
        // Create list that contain list of status by gender from top
        List<String> gender = filter.getListGender(5);
        // Create list that contain list of renting period from top
        List<String> rentPeriod = searchList.getListOfTimely(5);
        String expectedRent = expected.get(1).substring(0, (expected.get(1)).length() - 2).toLowerCase();
        // Create list that contain list of price from top
        List<Integer> price = searchList.getListOfprice(5);
        int min = Integer.parseInt(expected.get(2));
        int max = Integer.parseInt(expected.get(3));
        for (String s : gender) {
            Assert.assertEquals(s, expected.get(0), "Gender is not " + expected.get(0));
        }
        for (String r : rentPeriod) {
            Assert.assertEquals(r, expectedRent, "Rent period is not " + expected.get(1));
        }
        for (int p : price) {
            Assert.assertTrue((p < max) || (p > min),
                    "Price is not in range " + expected.get(2) + " to " + expected.get(3));
        }
    }

    @Then("kosan list appear correctly by min. payment")
    public void kosan_list_appear_correctly_by_min_payment(List<String> expected) throws InterruptedException {
        String expectedText = expected.get(0).toLowerCase();
        // Create list that contain list of minimum payment from top
        List<String> minPayment = searchList.getListOfTimely(3);
        for (String m : minPayment) {
            Assert.assertTrue(expectedText.contains(m), "Filter Minimum payment is not equals!");
        }
    }

    @Then("kosan list appear correctly by facility, relevant with the filter below :")
    public void kosan_list_appear_correctly_by_facility_relevant_with_the_filterbBelow(List<String> facility) throws InterruptedException {
        for (int i=0; i<facility.size(); i++) {
            searchList.tapTitle();
            loading.waitLoadingFinish();
            kosDetail.tapFasDetail();
            for (String f : facility) {
                kosDetail.scrollToFacilities(facility.get(i));
                Assert.assertTrue(kosDetail.isFacilitiesPresent(facility.get(i)), "Facility " + facility.get(i) + " not found");
            }
            kosDetail.tapBack();
            kosDetail.tapBack();
            appium.scroll(0.5, 0.80, 0.20, 2000);
        }
    }

    @And("user select {string} in kost type by Gender")
    public void userSelectInKostTypeByGender(String gender) throws InterruptedException {
        filter.selectGender(gender);
    }
    @And("user select {string} in Jangka Waktu")
    public void userSelectInJangkaWaktu(String timely) {
        filter.filterTime();
        filter.tapOption(timely);
    }

    @And("user set price range {string} - {string}")
    public void userSetPriceRange(String priceMin, String priceMax) {
        filter.setMinPrice(priceMin);
        filter.setMaxPrice(priceMax);
    }

    @And("user select minimum payment {string}")
    public void userSelectMinimumPayment(String minPay) {
        filter.filterTime();
        filter.tapOption(minPay);
    }

    @And("user select Facility :")
    public void userSelectFacility(List<String> facility) {
        for (String f : facility) {
            appium.scrollToElementByText(f);
            appium.clickElementByText(f);
        }
    }

    @And("user tap Search button")
    public void userTapSearchButton() throws InterruptedException {
        filter.tapSearch();
    }

    @When("user filter apartment by {string} min {string} max {string}")
    public void user_filter_apartment_by_min_max(String filterBy, String minPrice, String maxPrice) throws InterruptedException {
        filter.filterApartment(filterBy, minPrice, maxPrice);
    }

    @Then("system display apartment {string} with min price {string} max price {string} per {string} for {int} highest lists")
    public void system_display_apartment_with_min_price_max_price_per_for_highest_lists(String filterBy, String min, String max, String period, int counter) throws InterruptedException {
        for (int i = 1 ; i <= counter ; i++){
            String actualPeriod =  searchList.getApartmentPeriod(filterBy, i);

            String actualPrice =  searchList.getApartmentPrice(i);
            int actualPriceParse = Integer.parseInt(actualPrice);

            int maxPrice = Integer.parseInt(max);
            int minPrice = Integer.parseInt(min);

            Assert.assertTrue(actualPriceParse >= minPrice && actualPriceParse <= maxPrice, "Price (" + actualPriceParse + ") should be greater than or equal to Minimal Price (" + minPrice + ") &&  less than or equal to Maximal Price (" + maxPrice + ")");
            Assert.assertEquals(period, actualPeriod, "Period doesn't match");
        }
        //scroll up
        appium.scroll(0.5, 0.30, 0.70, 1000);
    }

    @Then("system display close icon")
    public void system_display_close_icon() {
        Assert.assertTrue(filter.isCloseIconPresent(), "Close icon is not present");
    }

    @Then("verify price range heading, Reset button and search button should be shown")
    public void verifyPriceRangeHeadingResetButtonAndSearchButtonShouldBeShown() {
        Assert.assertTrue(filter.isFilterPageHeadingOfMapPresent(), "Page heading is not present");
        Assert.assertTrue(filter.isResetButtonPresent(), "Reset button is not present");
        Assert.assertTrue(filter.isSearchButtonPresent(), "Search button is not present");

    }

    @Then("user verify the elements on filter")
    public void user_verify_the_elements_on_filter() {
        Assert.assertTrue(filter.isResetButtonPresent(), "Reset button is not present");
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            Assert.assertTrue(filter.isCloseIconFilterPresent(), "Close button is not present");
        }
    }

    @Then("user verify the wording on filter menu")
    public void user_verify_the_wording_on_filter_menu(List<String> elements) {
        for (String i : elements) {
            Assert.assertTrue(filter.isWordingElementPresent(i), i + " is not present");
        }
    }

    @When("user click reset button on filter page")
    public void user_click_reset_button_on_filter_page() throws InterruptedException{
        filter.clickResetButton();
    }
}
