package steps.mamikos.search;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.search.GoodAndServicesDetailsPO;
import utilities.ThreadManager;

public class GoodAndServicesSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private GoodAndServicesDetailsPO goodsAndServices = new GoodAndServicesDetailsPO(driver);

    @Then("verify goods and service price {string} should be shown")
    public void verifyGoodsAndServicePriceShouldBeShown(String priceText) throws InterruptedException {
        Assert.assertEquals(goodsAndServices.getMarketPrice(), priceText, "Market Price text doesn't match");
    }

    @Then("user is in detail page")
    public void user_is_in_detail_page() throws InterruptedException{
        goodsAndServices.isOnDetailPage();
    }

    @And("user tap chat button in detail page")
    public void user_tap_chat_button() {
        Assert.assertTrue(goodsAndServices.isChatButtonEnable());
        goodsAndServices.tapChatButton();
    }

    @And("user validate inactive ads")
    public void user_validate_inactive_ads() throws InterruptedException {
        goodsAndServices.validateInactiveAds();
        Assert.assertFalse(goodsAndServices.isChatButtonEnable(),"Chat button is enable" );
    }
}
