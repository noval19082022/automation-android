package steps.mamikos.search;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.goodsandservices.BottomFeatureOnGoodsAndServicesPO;
import pageobjects.mamikos.tenant.booking.BookingDetailsPO;
import pageobjects.mamikos.tenant.search.ApartmentDetailPO;
import pageobjects.mamikos.tenant.search.FilterPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import pageobjects.mamikos.tenant.wishlist.WishlistPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class SearchListingSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private SearchPO searchPage = new SearchPO(driver);
    private FilterPO filter = new FilterPO(driver);
    private BookingDetailsPO KosDetailPO = new BookingDetailsPO(driver);
    private AppiumHelpers appium = new AppiumHelpers(driver);
    private HeaderPO general = new HeaderPO(driver);
    private ApartmentDetailPO apartmentDetail = new ApartmentDetailPO(driver);
    private LoadingPO loading = new LoadingPO(driver);

    private PopUpPO popUp = new PopUpPO(driver);
    private WishlistPO wishlist = new WishlistPO(driver);
    private BottomFeatureOnGoodsAndServicesPO bottomFeature = new BottomFeatureOnGoodsAndServicesPO(driver);

    @And("user click on Pencarian Populer {string}")
    public void user_click_on_Pencarian_Populer(String yk) throws InterruptedException {
        Assert.assertEquals(searchListing.getYogyakartaText(), yk, "Your selected area is not " + yk);
        searchListing.clickOnYogyakarta();
        loading.waitLoadingFinish();
        if (Constants.MOBILE_OS== Platform.IOS) {
            appium.hardWait(5);
            appium.iOSActivateApp(Constants.MAMIKOS_APP_BUNDLED_ID);
        }
    }

    @And("user click on Love Button")
    public void user_click_on_Love_Button() throws InterruptedException {
        searchListing.clickOnLove();
        }

    @Then("user will see pop up {string} or {string}")
    public void user_will_see_pop_up(String login, String Selamat_Datang_di_Mamikos) {
        if (Constants.MOBILE_OS==Platform.ANDROID) {
            Assert.assertEquals(searchListing.getLoginText(), login, "Didn't find any " + login + " pop up");
        } else {

            Assert.assertEquals(searchListing.getLoginText(), Selamat_Datang_di_Mamikos, "Didn't find any " + Selamat_Datang_di_Mamikos + " pop up");
        }
    }

    @And("user back to Explore page")
    public void user_back_to_Explore_page() {
        if (Constants.MOBILE_OS==Platform.ANDROID) {
            searchListing.back();
            searchListing.back();
        } else {
            general.iOSBackButton();
            general.iOSBackButton();
            general.iOSBackButton();
        }
    }

    @And("user on tampilan peta page and click back to Homepage")
    public void user_On_Tampilan_Peta_Page_And_Click_Back_To_Homepage() throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            searchListing.back();
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        }
        Assert.assertTrue(searchListing.tapAndCheckLogoDisplayed(), "Element is not Present!!");
    }

    @Then("^Sorting list is appear by :$")
    public void sortingListIsAppearBy(List<String> sortMenu) throws InterruptedException {
            searchListing.scrollToUrutkanFilter();
        for ( int i = 0; i < sortMenu.size() ; i++){
            Assert.assertTrue(searchListing.getSortText(sortMenu.get(i)), sortMenu.get(i) + " is not present");
        }
    }

    @And("user tap saya mengerti in pop up that appear")
    public void userTapSayaMengertiInPopUpThatAppear() throws InterruptedException {
        if (Constants.MOBILE_OS==Platform.ANDROID)
            searchListing.clickFTUEKosListingPopUp();
    }

    @And("user tap Acak")
    public void userTapAcak() {
        searchListing.userTapAcak();
    }

    @Then("kosan list appear as random from top to bottom, with rule :")
    public void kosanListAppearAsRandomFromTopToBottom(List<String> definedOrder) {
        // create array of all status from listing
        List<String> AllStatus = searchListing.getListAllStatus(10);
        // create the duplicate array from price listing
        List<String> CopyStatus = new ArrayList<>(AllStatus);
        // Create comparator, compare with each of DefinedOrder list
        Comparator<String> statComparator =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(0)));
        Comparator<String> statComparator2 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(1)));
        Comparator<String> statComparator3 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(2)));
        Comparator<String> statComparator4 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(3)));
        Collections.sort(CopyStatus, statComparator4.thenComparing(statComparator3)
                .thenComparing(statComparator2).thenComparing(statComparator));
        Assert.assertEquals(AllStatus, CopyStatus, "Not Sorted by Acak");
    }

    @And("^user tap Lowest Price$")
    public void userTapLowestPrice() {
        searchListing.userTapLowestPrice();
    }

    @Then("^kosan list appear as lowest price from top to higher price$")
    public void kosanListAppearAsLowestPriceFromTopToHigherPrice() {
        // create array of price from listing
        List<Integer> Price = searchListing.getListOfprice(5);
        // create the duplicate array from price listing
        List<Integer> priceSort = new ArrayList<>(Price);
        // sort price ascending
        Collections.sort(priceSort);
        // check whether price in listing is sorted by lowest price or not
        Assert.assertEquals(Price, priceSort, "Listing status is wrong, not sorted by lowest price");
    }

    @And("^user tap Highest Price$")
    public void userTapHighestPrice() {
        searchListing.userTapHighestPrice();
    }

    @Then("^kosan list appear as highest price from top to lower price$")
    public void kosanListAppearAsHighestPriceFromTopToLowerPrice() {
        // create array of price from listing
        List<Integer> Price = searchListing.getListOfprice(5);
        // create the duplicate array from price listing
        List<Integer> priceSort = new ArrayList<>(Price);
        // sort price ascending
        Collections.sort(priceSort);
        // sort price descending
        priceSort.sort(Collections.reverseOrder());
        // check whether price in listing is sorted by highest price or not
        Assert.assertEquals(Price, priceSort, "Listing status is wrong, not sorted by highest price");
    }

    @Then("^kosan list appear as latest update from top to less updated :$")
    public void kosanListAppearAsLatestUpdateFromTopToLessUpdated(List<String> definedOrder) {
        // create list that contain listing status from top
        List<String> Update = searchListing.getListOfUpdate(25);
        // create the duplicate array from listing update status
        List<String> updateCopy = new ArrayList<>(Update);
        // sort duplicate list by number asc
        Collections.sort(updateCopy, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o1) - extractInt(o2);
            }

            int extractInt(String s) {
                String num = s.replaceAll("\\D", "");
                // return 0 if no digits found
                return num.isEmpty() ? 0 : Integer.parseInt(num);
            }
        });
        // Create comparator, compare with each of DefinedOrder list
        Comparator<String> timeComparator =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(0)));
        Comparator<String> timeComparator1 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(1)));
        Comparator<String> timeComparator2 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(2)));
        Comparator<String> timeComparator3 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(3)));
        Comparator<String> timeComparator4 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(4)));
        Comparator<String> timeComparator5 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(5)));
        Collections.sort(updateCopy, timeComparator5.thenComparing(timeComparator4)
                .thenComparing(timeComparator3).thenComparing(timeComparator2)
                .thenComparing(timeComparator1).thenComparing(timeComparator));
        Assert.assertEquals(Update, updateCopy, "Kosan not sorted by latest update");
    }

    @And("^user tap Latest Update$")
    public void userTapLatestUpdate() {
        searchListing.userTapLatestUpdate();
    }

    @And("^user tap Kosong ke penuh$")
    public void userTapKosongKePenuh() {
        searchListing.userTapKosongKePenuh();
    }

    @Then("^kosan list appear as empty from top to fully booked :$")
    public void kosanListAppearAsEmptyFromTopToFullyBooked(List<String> definedOrder) {
        // Create list that contain list of room available from top
        List<String> RoomAvail = KosDetailPO.getListRoomAvail(5);
        // create the duplicate list from room available
        List<String> RoomAvailCopy = new ArrayList<>(RoomAvail);
        // sort list copy by number asc
        Collections.sort(RoomAvailCopy, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o1) - extractInt(o2);
            }

            int extractInt(String s) {
                String num = s.replaceAll("\\D", "");
                // return 0 if no digits found
                return num.isEmpty() ? 0 : Integer.parseInt(num);
            }
        });
        // reverse sort by number
        RoomAvailCopy.sort(Collections.reverseOrder());
        // Create comparator, compare with each of DefinedOrder list
        Comparator<String> RoomAvailComparator =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(0)));
        Comparator<String> RoomAvailComparator2 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(1)));
        Comparator<String> RoomAvailComparator3 =
                Comparator.comparing(
                        c -> c.contains(definedOrder.get(2)));
        Collections.sort(RoomAvailCopy, RoomAvailComparator2.thenComparing(RoomAvailComparator3)
                .thenComparing(RoomAvailComparator));
        Assert.assertEquals(RoomAvail, RoomAvailCopy, "Kosan not sorted by kosong ke penuh");
    }

    @Then("system display back button")
    public void system_display_back_button() {
        searchListing.isBackButtonPresent();
    }

    @Then("system display input text search area contains {string}")
    public void system_display_input_text_search_area_contains(String text) {
        Assert.assertEquals(searchListing.getTextFromEditTextSearchLocation(), text, "Location is not same");
    }

    @Then("System display list view tab")
    public void system_display_list_view_tab() {
        searchListing.isListViewTabPresent();
    }

    @Then("system display map view tab")
    public void system_display_map_view_tab() {
        searchListing.isMapViewTabPresent();
    }

    @Then("system display sorting icon")
    public void system_display_sorting_icon() {
        searchListing.isSortingIconPresent();
    }

    @Then("system display loading animation to load listing")
    public void system_display_loading_animation_to_load_listing() {
        searchListing.waitApartmentAppear();
        for (int i=0; i<41; i++) {
            appium.scroll(0.5, 0.80, 0.20, 3000);
            //only begin checking after scroll 25x
            if (i > 25) {
                if (loading.isLoadingLoadMoreAppear()) {
                    break;
                }
                else if (i == 40) {
                    Assert.assertTrue(loading.isLoadingLoadMoreAppear(), "Loading animation is not present");
                }
            }
        }
    }

    @When("user click apartment name")
    public void user_click_apartment_name() {
        searchListing.clickOnApartmentNameOnListViewApartment();
    }

    @And("user click on Map view button")
    public void userClickOnMapViewButton() throws InterruptedException {
        searchListing.clickFTUEKosListingPopUp();
        bottomFeature.clickOnMapIcon();
    }
    @And("click on green cluster number {string} from map view")
    public void clickOnGreenClusterNumberTFromMapView(String clusterNumber) {
        searchListing.clickOnNumberClusterOfMap(clusterNumber);
    }

    @And("click on price cluster {string} to open kos details")
    public void clickOnPriceClusterToOpenKosDetails(String clusterNumber) throws InterruptedException {
        searchListing.clickOnPriceClusterOfMap(clusterNumber);
    }

    @Then("verify below list of apartment should be shown")
    public void verifyBelowListOfApartmentShouldBeShown(List<String> apartment) {
        List<String> allKosts = searchListing.getListOfLocationForApartment(2);
        for (int i = 0; i < apartment.size(); i++) {
            Assert.assertTrue(allKosts.contains(apartment.get(i)), apartment.get(i) + " is not present");
        }

    }

    @And("user click on apartment to open apartment details")
    public void userClickOnApartmentToOpenApartmentDetails() throws InterruptedException {
        searchListing.clickOnApartmentTitleText();
        apartmentDetail.waitApartmentDetailToLoad();
        loading.waitLoadingFinish();
        appium.hardWait(2);
    }

    @And("click on price cluster {string} to open apartment details")
    public void clickOnPriceClusterToOpenApartmentDetails(String clusterNumber) {
        searchListing.clickOnPriceClusterForApartment(clusterNumber);
    }

    @Then("user click on Filter button and verify below list")
    public void userClickOnFilterButtonAndVerifyBelowList(List<String> filterOption) {
        searchListing.clickOnFilterButton();
        for (int i =0; i<filterOption.size(); i++) {
            Assert.assertTrue(searchListing.isFilterOptionPresent(filterOption.get(i)), filterOption.get(i) + "is not present");
        }
    }

    @And("verify Filter button, Shorting Button, should be shown and map view button should not shown")
    public void verifyFilterButtonShortingButtonShouldBeShownAndMapViewButtonShouldNotShown() throws InterruptedException{
        Assert.assertTrue(searchListing.isFilterIconPresent(), "Filter button is not present");
        Assert.assertTrue(searchListing.isShortingButtonPresent(), "Shorting button is not present");
        Assert.assertFalse(bottomFeature.mapIconIsPresent(), "Map view button is not present");
    }

    @When("user navigate back to previous page")
    public void userNavigateBackToPreviousPage() {
        appium.back();
    }

    @And("user click on the Search bar of map")
    public void userClickOnTheSearchBarOfMap() {
        searchListing.clickOnSearchLocation();
    }

    @Then("verify search field, Good cluster with number {string}, Service cluster with number {string} and Number cluster with number {string}")
    public void verifySearchFieldGoodClusterWithNumberServiceClusterWithNumberAndNumberClusterWithNumber(String goodCluster, String serviceCluster, String numberCluster) {
        Assert.assertTrue(searchListing.isSearchBarOfMapViewPresent(), "Search bar of map view is not present");
        Assert.assertTrue(searchListing.isGoodClusterPresent(goodCluster), "Good cluster is not present");
        Assert.assertTrue(searchListing.isServiceClusterPresent(serviceCluster), "Service cluster is not present");
        Assert.assertTrue(searchListing.isPriceClusterPresent(numberCluster), "Price cluster is not present");

    }

    @Then("verify below goods and services list should be shown")
    public void verifyBelowGoodsAndServicesListShouldBeShown(List<String> goodAndServices) {
        List<String> allServices = searchListing.getListOfGoodAndServices(1);
        for (int i = 0; i < goodAndServices.size(); i++) {
            Assert.assertTrue(allServices.contains(goodAndServices.get(i)), goodAndServices.get(i) + " is not present");
        }
    }

    @And("user click on Filter button")
    public void userClickOnFilterButton() {
        searchListing.clickOnFilterButton();
    }

    @And("user choose random goods and service")
    public void user_choose_random_goods_and_service() throws InterruptedException {
        searchListing.isSearchResultPresent();
        searchListing.tapRandomGoodsAndService();
    }

    @Then("user see Goods and Service search result")
    public void validate_search_element_for_Goods_and_Service() throws InterruptedException {
        Assert.assertTrue(searchListing.isBackButtonPresent(), "Back button is not present");
        Assert.assertTrue(searchListing.isFilterIconPresent(), "Filter button is not present");
        Assert.assertTrue(searchListing.isSortingIconPresent(), "Short button is not present");
        Assert.assertTrue(searchListing.isSearchResultPresent(), "Null Data result List");
    }

    @And("user choose soldout goods and service")
    public void user_choose_soldout_goods_and_service() throws InterruptedException {
        searchListing.isSearchResultPresent();
        searchListing.isSoldoutItemPresent();
        searchListing.tapSoldOutItem();
    }

    @Then("^Sorting goods and service list is appear by :$")
    public void sorting_goods_service_list(List<String> sortMenu) throws InterruptedException {
        for ( int i = 0; i < sortMenu.size() ; i++){
            Assert.assertTrue(searchListing.getSortText(sortMenu.get(i)), sortMenu.get(i) + " is not present");
        }
    }

    @And("^user choose (.*) for result list$")
    public void user_choose_sorting(String sort) throws InterruptedException {
        searchListing.selectOptionForSorting(sort);
    }

    @When("user click favorite icon")
    public void user_click_favorite_icon() {
        wishlist.clickFavoriteIcon();
    }

    @And("user dismiss the pop up that appear")
    public void user_dismiss_the_pop_up_that_appear() throws InterruptedException{
        if (Constants.MOBILE_OS==Platform.ANDROID)
            searchListing.dismissPopUpEmpty();
    }

    @Then("user verify the elements of empty state page")
    public void user_verify_the_elements_of_empty_state_page() throws InterruptedException{
        Assert.assertTrue(searchListing.isBookingLangsungButtonPresent(), "Booking Langsung button is not present");
        Assert.assertTrue(searchPage.isGPButtonPresent(), "Kos Andalan button is not present");
        Assert.assertTrue(searchListing.isEmptyImagePresent(), "Empty State Image is not present");
        Assert.assertTrue(searchListing.isSearchAnotherAreaButtonPresent(), "Search Another Area button is not present");
        Assert.assertTrue(searchListing.isFilterIconPresent(), "Filer button is not present");
        Assert.assertTrue(searchListing.isSortingIconPresent(), "Filer button is not present");
        Assert.assertTrue(bottomFeature.mapIconIsPresent(), "Filer button is not present");
    }

    @And("user verify the wording on empty state page")
    public void user_verify_the_wording_on_empty_state_page(List<String> elements) {
        for (String i : elements){
            Assert.assertTrue(filter.isWordingElementPresent(i), i + "is not present");
        }
    }

    @Then("system display favorite icon")
    public void system_display_favorite_icon() throws InterruptedException{
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            searchListing.isFavoriteIconPresent();
        }
    }

    @And("system display flash sale icon")
    public void system_display_flash_sale_icon() {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            searchListing.isFlashSaleIconPresent();
        }
    }

    @And("system display discount percentage text")
    public void system_display_discount_percentage_icon() {
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            searchListing.isDiscountPercentageTextPresent();
        }
    }

    @And("user select sorting by {string}")
    public void user_select_sorting_by(String sorting) throws InterruptedException{
        searchListing.chooseSorting(sorting);
    }

    @Then("system display {string} checked is {string}")
    public void systemDisplayIsSelected(String sortingSelected, String expected) throws InterruptedException{
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            assertEquals(searchListing.getSortChecked(sortingSelected), expected, "radio button is "+expected+"");
        }
    }

    @And("user click saya mengerti button on search area")
    public void user_click_saya_mengerti_button_on_search_area() throws InterruptedException{
        searchListing.clickSayaMengerti();
    }

    @And("user click ijinkan akses lokasi")
    public void user_click_ijinkan_akses_lokasi() {
        searchListing.clickOnIjinkanButton();
    }
}