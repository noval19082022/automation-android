package steps.mamikos.search;

import io.appium.java_client.AppiumDriver;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.et.Ja;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.goodsandservices.BottomFeatureOnGoodsAndServicesPO;
import pageobjects.mamikos.tenant.explore.ExplorePO;
import pageobjects.mamikos.tenant.search.ApartmentDetailPO;
import pageobjects.mamikos.tenant.search.FilterPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.AppiumHelpers;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.Arrays;
import java.util.List;

public class SearchSteps {
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private ExplorePO explore = new ExplorePO(driver);
	private SearchPO search = new SearchPO(driver);
	private SearchListingPO searchList = new SearchListingPO(driver);
	private FilterPO filter = new FilterPO(driver);
	private PopUpPO popUp = new PopUpPO(driver);
	private LoadingPO loading = new LoadingPO(driver);
	private ApartmentDetailPO AptDetails = new ApartmentDetailPO(driver);
	private AppiumHelpers appium = new AppiumHelpers(driver);
	private BottomFeatureOnGoodsAndServicesPO bottomFeature = new BottomFeatureOnGoodsAndServicesPO(driver);

	//Test Data
	private String bookingPropertyFile="src/test/resources/testdata/mamikos/booking.properties";
	private String serverKayPay = Constants.SERVER_KEY + "_" + Constants.SERVER_PAY;
	private String bookingKoseName = JavaHelpers.getPropertyValueForSquad(bookingPropertyFile,"koseName" );
	private String disableBookingKoseName = JavaHelpers.getPropertyValueForSquad(bookingPropertyFile,"koseNameForBookingDisable" );
	private String femaleKoseName = JavaHelpers.getPropertyValueForSquad(bookingPropertyFile,"femaleKoseName" );
	private String maleKoseName = JavaHelpers.getPropertyValueForSquad(bookingPropertyFile,"maleKoseName" );
	private String priceAndRentDurationKostName = JavaHelpers.getPropertyValue(bookingPropertyFile, "kostNamePriceAndRentDuration_" + serverKayPay);
	private String tenantWaitingForBookingApprovalKostName = JavaHelpers.getPropertyValue(bookingPropertyFile, "kostNameTenantWaitingForApproval_" + serverKayPay);
	private String koseNameForReview = JavaHelpers.getPropertyValueForSquad(bookingPropertyFile,"koseNameForReview" );

	//Test Data Payment
	private String paymentProperties="src/test/resources/testdata/mamikos/payment.properties";
	private String paymentKosName_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"propertyName" );
	private String paymentKosName = JavaHelpers.getPropertyValueForSquad(paymentProperties,"propertyName" );

	//Test Data addVoucher
	private String addVoucherProperties="src/test/resources/testdata/mamikos/addVoucher.properties";
	private String kostNameForAddVoucher1 = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"kostNameForAddVoucher1");
	private String kostNameForAddVoucher2 = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"kostNameForAddVoucher2");
	private String kostNameForAddVoucher3 = JavaHelpers.getPropertyValueForSquad(addVoucherProperties,"kostNameForAddVoucher3");

	//Test Data OB
	private String obTestData = "src/test/resources/testdata/mamikos/OB.properties";
	private String fullKost = JavaHelpers.getPropertyValueForSquad(bookingPropertyFile, "kostFull");
	private String mixKostPasutri = JavaHelpers.getPropertyValueForSquad(bookingPropertyFile, "mixKostPasutri");
	private String additionalPriceOB = JavaHelpers.getPropertyValueForSquad(obTestData, "kostMixName");

    //Test Data DC
    private String DCProperty = "src/test/resources/testdata/mamikos/bookingGrowth.properties";
    private String kostBAR1 = JavaHelpers.getPropertyValueForSquad(DCProperty, "kostBAR1_" + Constants.SERVER_KEY);

    //Test Data DC FTUE Booking Benefit
    private String FTUEProperty="src/test/resources/testdata/mamikos/FTUEBookingBenefit.properties";
    private String FTUEKostName1 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName1");
    private String FTUEKostName2 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName2");
    private String FTUEKostName3 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName3");
    private String FTUEKostName4 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName4");

    //Test Data Essential Test
	private String essentialTestProperty = "src/test/resources/testdata/mamikos/essentialTestOwner.properties";
	private String kostDownPaymentEssentialTest =JavaHelpers.getPropertyValue(essentialTestProperty, "dpEssentialTest_" + Constants.SERVER_KEY+ "_"+Constants.SERVER_PAY);

	private String kostEssentialTest =JavaHelpers.getPropertyValue(essentialTestProperty, "kostEssentialTest_" + Constants.SERVER_KEY+ "_"+Constants.SERVER_PAY);

	@And("user search for Kost with name {string} and selects matching result and navigate to Kost details page")
	public void user_search_for_Kost_with_given_name_and_elects_matching_result_and_navigate_to_Kost_details_page(String type) throws InterruptedException {
		explore.clickOnSearchTextbox();
		String koseName= "";
		if (type.equalsIgnoreCase("booking")){
			koseName = bookingKoseName;
		}else if (type.equalsIgnoreCase("bookingDisable")){
			koseName = disableBookingKoseName;
		}
		else if (type.equalsIgnoreCase("price and rent duration")) {
			koseName = priceAndRentDurationKostName;
		}
		else if (type.equalsIgnoreCase( "tenant waiting for booking approval")) {
			koseName = tenantWaitingForBookingApprovalKostName;
		}
		else if (type.equalsIgnoreCase( "payment")) {
			koseName = paymentKosName_;
		}
		else if (type.equalsIgnoreCase("review")){
			koseName = koseNameForReview;
		}
		else if (type.equalsIgnoreCase("add voucher1")){
			koseName = kostNameForAddVoucher1;
		}
		else if (type.equalsIgnoreCase("add voucher2")){
			koseName = kostNameForAddVoucher2;
		}
		else if (type.equalsIgnoreCase("add voucher3")){
			koseName = kostNameForAddVoucher3;
		}
		else if (type.equalsIgnoreCase("kostfull")) {
			koseName = fullKost;
		}
        else if (type.equalsIgnoreCase("Kost BAR 1")) {
            koseName = kostBAR1;
        }
		else if (type.equalsIgnoreCase("mix kost pasutri")) {
			koseName = mixKostPasutri;
		}
		else if (type.equalsIgnoreCase("additional price ob")) {
			koseName = additionalPriceOB;
		}
		else if (type.equalsIgnoreCase("kost down payment essential test")) {
			koseName = kostDownPaymentEssentialTest;
		}
		else if (type.equalsIgnoreCase("kost essential test")) {
			koseName = kostEssentialTest;
		}
		else {
			koseName=type;
		}
		search.enterTextToSearchTextboxAndSelectResult(koseName);
		loading.waitLoadingFinish();
		popUp.goThroughBookingPopUps();
	}

    @And("user search for Kost with name {string} and selects matching result and navigate to Kost details page for check FTUE")
    public void user_search_for_Kost_with_given_name_and_elects_matching_result_and_navigate_to_Kost_details_page_for_check_FTUE(String type) throws InterruptedException {
        explore.clickOnSearchTextbox();
        String koseName= "";
        if (type.equalsIgnoreCase("booking")){
            koseName = bookingKoseName;
        }else if (type.equalsIgnoreCase("bookingDisable")){
            koseName = disableBookingKoseName;
        }
        else if (type.equalsIgnoreCase("price and rent duration")) {
            koseName = priceAndRentDurationKostName;
        }
        else if (type.equalsIgnoreCase( "tenant waiting for booking approval")) {
            koseName = tenantWaitingForBookingApprovalKostName;
        }
        else if (type.equalsIgnoreCase( "payment")) {
            koseName = paymentKosName_;
        }else if (type.equalsIgnoreCase("review")){
            koseName = koseNameForReview;
        }else if (type.equalsIgnoreCase("add voucher1")){
            koseName = kostNameForAddVoucher1;
        }
        else if (type.equalsIgnoreCase("mix kost pasutri")) {
        	koseName = mixKostPasutri;
		}
        search.enterTextToSearchTextboxAndSelectResult(koseName);
        loading.waitLoadingFinish();
    }

	@When("user search for kos with name {string} and selects matching result and navigate to detail kos")
	public void user_search_for_kos_with_name_and_selects_matching_result_and_navigate_to_detail_kos(String squad) throws InterruptedException {
		explore.clickOnSearchTextbox();
		String koseName= "";
		if (squad.equalsIgnoreCase( "payment")) {
			koseName = paymentKosName_;
		}
		search.enterTextToSearchTextboxAndSelectResult(koseName);
	}

	@Then("should display back button")
	public void should_Display_Back_Button() {
		search.backButtonIsDisplayed();
		Assert.assertTrue(search.backButtonIsDisplayed(), "Back button is not present!");

	}

	@And("should display search icon")
	public void should_Display_Search_Icon() throws InterruptedException {
		Assert.assertTrue(search.searchIconIsDisplayed(), "search icon is not present!");
	}

	@And("should display text field")
	public void should_Display_Text_Field() {
		Assert.assertTrue(search.textFieldIsDisplayed(), "Text field is not present!");
	}

	@When("user tap on main search in Homepage")
	public void user_Tap_On_Main_Search_In_Homepage() throws InterruptedException{
		explore.clickOnSearchTextbox();
	}

	@Then("User type with city name {string} and should display the autocomplete result area")
	public void user_Type_With_City_Name_And_Should_Display_The_Autocomplete_Result_Area(String keyword) throws InterruptedException {
		Assert.assertTrue(search.isSearchResultPresent(keyword), "Expected result is not equals!!");
	}

	@Then("User input with three character {string}")
	public void user_Input_With_Three_Character(String textTrue) throws InterruptedException {
		search.tapAndEnterDataToSearchTextbox(textTrue);
	}

	@And("should display the autocomplete result list")
	public void should_Display_The_Autocomplete_Result_List() {
		Assert.assertTrue(search.textAreaIsDisplayed(), "Text Area title is not Present!!");
		Assert.assertTrue(search.textNameIsDisplayed(), "Text Name title is not Present!!");

	}

	@Then("User input with three character exception {string}")
	public void user_Input_With_Three_Character_Exception(String textFalse) throws InterruptedException {
		search.tapAndEnterDataToSearchTextbox(textFalse);
	}

	@And("should NOT display the autocomplete result list")
	public void should_NOT_Display_The_Autocomplete_Result_List() {
		Assert.assertFalse(search.textAreaIsDisplayed(), "Text Area title is Present!! This is a bug");
		Assert.assertFalse(search.textNameIsDisplayed(), "Text name title is Present!! This is a bug");
	}

	@And("user clear the text box")
	public void user_Clear_The_Text_Box() {
		search.clearText();
	}

	@Then("User input with random character {string}")
	public void user_Input_With_Random_Character(String randomWord) throws InterruptedException {
		search.tapAndEnterDataToSearchTextbox(randomWord);
	}

	@And("should display information {string}")
	public void should_Display_Information(String textInfo) {
		Assert.assertEquals(search.getResultInfo(), textInfo, "Text is not equal to expected result!");
	}

	@And("user choose one of the list result")
	public void user_Choose_One_Of_The_List_Result() throws InterruptedException {
		search.tapAreaResult();
	}

	@And("user choose {string} list university")
	public void user_Choose_One_Of_The_List_University(String text) {
		search.tapAreaOption(text);
	}

	@Then("should display saved keyword {string} on main search")
	public void should_Display_Saved_Keyword_On_Main_Search(String keyword) throws InterruptedException {
		Assert.assertEquals(search.getSearchTextboxText(), keyword, "Text is not equal to expected result!");
	}

	@When("User set off location service")
	public void user_set_off_location_service() {
		search.switchLocationService();
	}

	@When("User click text view my location")
	public void user_click_text_view_my_location() {
		search.clickOnMyLocation();
	}

	@Then("System display {string} on popup message location service")
	public void system_display_on_popup_message_location_service(String message) {
		Assert.assertTrue(search.getPopupMessageLocationService().contentEquals(message), "Message is: " + search.getPopupMessageLocationService());
	}

	@When("User click button {string} on popup message location service")
	public void user_click_button_on_popup_message_location_service(String string) {
		search.clickOkButton();
	}

	@When("User click {string} on popup bisa booking")
	public void user_click_on_popup_bisa_booking(String button) throws InterruptedException {
		if (button.equals("Saya Mengerti")) {
			searchList.clickFTUEKosListingPopUp();
		}
	}

	@When("User click tab {string}")
	public void user_click_tab(String tab) {
		if (tab.equals("Tampilan Daftar")) {
			searchList.userTapTampilanDaftarBoardingHouseList();
		}
	}

	@Then("System display boarding house in {string} and Verify the {int} highest lists")
	public void system_display_boarding_house_in_and_Verify_the_highest_lists(String location, int numberOfBoardingHouse) {
		String[] currentLocation;
		if (location.equals("Sleman")) {
			currentLocation = new String[]{"Depok", "Ngaglik", "Ngemplak", "Yogyakarta", "Mlati", "Cibinong","Cilodong","Kecamatan Rajeg","Rajeg","Kecamatan Kretek"};
		} else {
			currentLocation = new String[]{"Jetis", "Gedong Tengen", "Gondokusuman", "Danurejan", "Ngampilan"};
		}

		for (int boardingCount = 0; boardingCount < numberOfBoardingHouse; boardingCount++) {
			String getLocation = searchList.getBoardingHouseLocationOnListing();

			List<String> list = Arrays.asList(currentLocation);
			if (list.contains(getLocation)) {
				System.out.println("Boarding house is in my location " + getLocation);
			}
			Assert.assertTrue(list.contains(getLocation), "Boarding house isn't in my location: " + getLocation);
		}
	}

	@And("user tap Area")
	public void user_tap_Area() throws InterruptedException{
		search.clickArea();
	}

	@Then("^After user click City name, city name will expand and Area name listed below it.$")
	public void cityNameWillExpandAndAreaNameListedBelowit(DataTable cityName) throws InterruptedException {
		List<List<String>> listCity = cityName.asLists(String.class);
		// Loop through the list of city
		for (int j = 0; j < listCity.size(); j++) {
			// Tap on first list (table header)
			search.userTapCity(listCity.get(0).get(j));
			// Loop all city below header and check appearance in UI
			for (int i = j + 1; i < listCity.size(); i++) {
				Assert.assertTrue(search.checkElementbyText(listCity.get(i).get(j)), "City not appear in dropdown.");
			}
			search.userTapCity(listCity.get(0).get(j));
			// Scroll down to next city
			if (j != listCity.size() - 1) {
				search.scrollToElement(listCity.get(0).get(j + 1));
			}
		}
	}

	@When("user tap search bar")
	public void userTapSearchBar() throws InterruptedException{
		explore.clickOnSearchTextbox();
	}

	@And("Tenant tap on Search input field")
	public void tenant_tap_on_search_input_field() throws InterruptedException{
		explore.clickOnSearchTextbox();
	}

	@And("^Tenant see$")
	public void tenant_see_popular_campus(List<String> list) {
		int index;
		int maxIndex;
		if (search.getPopularSearchText(1).equals("UGM")) {
			index = 1;
			maxIndex = 10;
		} else {
			index = 2;
			maxIndex = 11;
		}
		for (int i = index; i <= maxIndex; i++) {
			String menu;
			if (index == 2) {
				menu = list.get(i - 1);
			} else {
				menu = list.get(i);
			}
			Assert.assertEquals(search.getPopularSearchText(i), menu, "Text not present: " + menu);
		}
	}

	@And("Tenant tap on {string}")
	public void tenant_tap_on_ugm(String campusName) throws InterruptedException {
		search.clickOnPopular(campusName);
	}

	@And("Tenant tap on city {string}")
	public void tenant_tap_on_city_name(String cityName) {
		search.tapOnElementText(cityName);
	}

	@And("Tenant tap one of kampus in the list {string}")
	public void tenant_tap_one_of_kampus_in_the_list(String campusName) {
		search.tapOnElementText(campusName);
	}

	@Then("Tenant should see cari kost page")
	public void tenant_should_see_cari_kost_page() {
		Assert.assertTrue(search.cariKostPageReachedAndDisplayed(), "Bisa Booking button are not displaying");
	}

	@Then("Tenant can see pencarian popular section")
	public void tenant_can_see_pencarian_popular_section() {
		Assert.assertTrue(search.verifyMyCurrentLocationPresent(), "My Current Location element is not present");
		Assert.assertTrue(search.verifyPencarianPopularHeaderPresent(), "Pencarian popular, element is not present");
		Assert.assertTrue(search.verifySearchBasedOnCityPresent(), "Pencarian based on city, element is not present");
	}

	@Then("^Tenant can see city list$")
	public void tenant_can_see_city_name(List<String> list) {
		for (int i = 0; i < list.size(); i++) {
			Assert.assertTrue(search.vefiryCityOrCampusListPresent(list.get(i)), "City parent " + list.get(i) + " are not displaying");
		}
	}

	@Then("^Tenant can see in the city child$")
	public void tenant_can_see_kampus_list(List<String> list) {
		for (int i = 0; i < list.size(); i++) {
			Assert.assertTrue(search.vefiryCityOrCampusListPresent(list.get(i)), "City child list " + list.get(i) + " are not displaying");
		}
	}

	@And("user tap {string}")
	public void userTapCity(String city) throws InterruptedException {
		search.userTapCity(city);
	}

	@And("user tap Tampilan Daftar")
	public void userTapTampilanDaftar() {
        if (Constants.MOBILE_OS== Platform.IOS){
            searchList.TapTampilanDaftarKos();
        }
	}

	@And("user tap Urutkan")
	public void userTapUrutkan() {
		searchList.userTapUrutkan();
	}

	@And("user tap Filter")
	public void userTapFilter() {
		filter.tapFilter();
	}

	@When("user search apartment {string}")
	public void user_search_apartment(String apartmentName) throws InterruptedException {
		if(Constants.MOBILE_OS == Platform.ANDROID){
			loading.waitLoadingFinish();
		}
		search.clickApartmentIcon();
		search.clickOnSearchTextbox();
		search.clickArea();
		search.userTapCity(apartmentName);
		searchList.waitApartmentAppear();
	}

	@When("user search apartment by three character {string}")
	public void user_search_apartment_by_three_character(String text) throws InterruptedException {
		search.clickApartmentIcon();
		search.clickOnSearchTextbox();
		search.enterTextToSearchTextbox(text);
	}

	@When("user select list option area {string}")
	public void user_select_list_option_area(String text) throws InterruptedException {
		search.selectResultOptionListSearchTextbox(text);
	}

	@Then("system display map")
	public void system_display_map() {
		bottomFeature.clickOnMapIcon();
		Assert.assertTrue(searchList.displayMapView(), "Map not display");
	}

	@When("user click icon filter")
	public void user_click_icon_filter() throws InterruptedException {
		search.clickFilterIcon();
	}

	@When("user sorting {string} for apartment list")
	public void user_sorting_for_apartment_list(String sorting) throws InterruptedException {
		searchList.userTapUrutkan();
		searchList.selectOptionForSorting(sorting);
	}

	@Then("system display {int} list of apartment as {string}")
	public void system_display_list_of_apartment_as(int numberOfList, String sort) throws InterruptedException {
		int price1 = 0;

		for (int i = 0; i < numberOfList; i++) {
			String pricePeriod2 = searchList.getApartmentPricePeriod();
			int price2 = JavaHelpers.extractNumber(pricePeriod2);

			if (sort.equals("Harga termurah") && i > 1){
				Assert.assertTrue(price1 <= price2, "Price Property 1 (" + price1 + ") should be less than or equal to Price Property 2 (" + price2 + ")");
			}else if (sort.equals("Harga termahal") && i > 1){
				Assert.assertTrue(price1 >= price2, "Price Property 1 (" + price1 + ") should be greater than or equal to Price Property 2 (" + price2 + ")");
			}
			price1 = price2;
			if ( i == (numberOfList - 1)){
				appium.scroll(0.5, 0.20, 0.80, 2000);
			}
		}
	}

	@When("user click sorting sorting icon")
	public void user_click_sorting_sorting_icon() {
		searchList.userTapUrutkan();
	}

	@And("user search for Kost with name {string} and selects second matching result and navigate to Kost details page")
	public void userSearchForKostWithNameAndSelectsSecondMatchingResultAndNavigateToKostDetailsPage(String type) throws InterruptedException {
		explore.clickOnSearchTextbox();
		String koseName="";
		if (type.equalsIgnoreCase("femaleData")){
			koseName = femaleKoseName;
		} else if (type.equalsIgnoreCase("maleData")){
			koseName = maleKoseName;
		}
		search.enterTextToSearchTextbox(koseName);
		search.selectSecondResultFromSearchList(koseName);
	}

	@Then("verify search field, my current location, Popular Search, search by city option should be displayed")
	public void verify_search_field_current_loc_popular_search_search_by_city_option_should_be_displayed() throws InterruptedException {
		Assert.assertTrue(search.textFieldIsDisplayed(), "Search field  is not displayed");
		Assert.assertTrue(search.verifyMyCurrentLocationPresent(), "My current Location is not displayed");
		Assert.assertTrue(search.verifyPencarianPopularHeaderPresent(), "Popular search header is not displayed");
		Assert.assertTrue(search.verifySearchBasedOnCityPresent(), "Based on city header is not displayed");
	}

	@And("user search for apartment {string} and select matching result and navigate to apartment details page")
	public void userSearchForApartmentAndSelectMatchingResultAndNavigateToApartmentDetailsPage(String apartmentName) throws InterruptedException {
		search.enterTextToSearchTextbox(apartmentName);
		search.selectSecondResultFromSearchList(apartmentName);
	}

	@And("user search for Kost with name {string} and selects matching result to navigate Kost details page")
	public void userSearchForKostWithNameAndSelectsMatchingResultToNavigateKostDetailsPage(String kostName) throws InterruptedException {
		explore.clickOnSearchTextbox();
		search.enterTextToSearchTextboxAndSelectResult(kostName);
	}

	@And("user navigate back via device back button and verify kos list should be shown")
	public void userNavigateBackViaDeviceBackButtonAndVerifyKosListShouldBeShown() {
		appium.back();
		Assert.assertTrue(search.textFieldIsDisplayed(), "Text Field is not shown");
	}

	@And("user input search with {string}")
	public void user_input_search_with(String place) {
		search.searchInput(place);
	}

	@And("user choose from results with address {string}")
	public void user_choose_from_results_with_address(String address) {
		search.clickOneOfSearchResults(address);
	}

	@And("user click on BBK pop up")
	public void user_click_on_BBK_pop_up() {
		search.clickOnBBKPopup();
	}

	@When("user click on available room lists")
	public void user_click_on_available_room_lists() throws InterruptedException {
		search.clickOneOfRoomLists();
	}

	@When("user click on detail facility")
	public void user_click_on_detail_facility() throws InterruptedException {
		search.dismissFTUEScreen();
		search.clickOnDetailFacility();
	}

	@Then("user verify login owner and tenant button")
	public void user_verify_login_owner_and_tenant_button() {
		Assert.assertTrue(search.isLoginOwnerButtonPresent(), "Login owner button is not present");
		Assert.assertTrue(search.isLoginTenantButtonPresent(), "Login tenant button is not present");
	}

    @Then("verify some elements like back button, Filter button, URUTKAN button, booking language button, map view button and kos details should be shown")
    public void verifySomeElementsLikeBackButtonFilterButtonURUTKANButtonBookingLanguageButtonMapViewButtonAndKosDetailsShouldBeShown() throws InterruptedException{
		Assert.assertTrue(searchList.isBackButtonPresent(), "Back button is not present");
        Assert.assertTrue(searchList.isBookingLangsungButtonPresent(), "Booking Langsung button is not present");
        Assert.assertTrue(searchList.isFilterIconPresent(), "Filter button is not present");
        Assert.assertTrue(searchList.isSortingIconPresent(), "Short button is not present");
        Assert.assertTrue(bottomFeature.mapIconIsPresent(), "Map view button is not present");
        Assert.assertTrue(searchList.isKostNamePresent(), "Kose name is not present");
		Assert.assertTrue(searchList.isKostPricePresent(), "Kose price is not present");
		Assert.assertTrue(searchList.isGenderPresent(), "Gender is not present");
		Assert.assertTrue(searchList.isRoomLocationTextPresent(), "Room Location text is not present");
		Assert.assertTrue(searchList.isRoomImagePresent(), "Room image is not present");
		Assert.assertTrue(searchList.isFacilitiesTextPresent(), "Room facilities is not present");
		Assert.assertTrue(searchList.isRatingTextPresent(), "Room rating is not present");
//		Assert.assertTrue(searchList.isFavoriteIconPresent(), "Favorite icon is not present");
    }

	@And("user search apartment {string} and select matching result from area list")
	public void userSearchApartmentAndSelectMatchingResultFromAreaList(String apartmentName) throws InterruptedException {
			search.enterTextToSearchTextboxAndSelectResult(apartmentName);
	}

	@And("user click on room image tooltip")
	public void user_click_on_room_image_tooltip() {
		search.clickOnRoomImageTooltip();
	}


    @When("user search for Kost with name {string} and selects matching result without dimiss FTUE")
    public void user_search_for_kost_with_name_and_selects_matching_result_without_dimiss_FTUE(String kosName) throws InterruptedException{
        if(kosName.equalsIgnoreCase("FTUEKostName1"))
        {
            kosName = FTUEKostName1;
        }
        else if(kosName.equalsIgnoreCase("FTUEKostName2"))
        {
            kosName = FTUEKostName2;
        }
        else if(kosName.equalsIgnoreCase("FTUEKostName3"))
        {
            kosName = FTUEKostName3;
        }
        else if(kosName.equalsIgnoreCase("FTUEKostName4"))
        {
            kosName = FTUEKostName4;
        }
        search.enterTextToSearchTextboxAndSelectResult(kosName);
        loading.waitLoadingFinish();
    }

	@Then("system display search text box is clear")
	public void system_display_search_text_box_is_clear() {
		Assert.assertEquals(search.getTextSearchTextBoxClear(),"Contohnya Tebet Jakarta Selatan");
	}

	@Then("system display filter icon")
	public void system_display_filter_icon() throws InterruptedException{
		Assert.assertTrue(search.isFilterIconPresent(),"Icon filter not present");
	}
}
