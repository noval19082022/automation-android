package steps.mamikos.search.stasiunhalte;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.ThreadManager;

public class StasiunHalteSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private SearchPO stasiunhalte = new SearchPO(driver);

    @When("user click on the Search Bar field")
    public void user_click_on_the_Search_Bar_field(){
        stasiunhalte.clickOnSearchTextbox();
    }

    @And("user click on {string} tab")
    public void user_click_on_Stasiun_And_Halte_tab(String menuText){
        Assert.assertEquals(stasiunhalte.stasiunMenuText(), menuText, "Menu text doesn't match");
        stasiunhalte.clickOnStasiunHalteTab();
    }

    @And("user click on {string} drop down")
    public void user_click_on_yogyakarta_drop_down(String cityName){
        stasiunhalte.clickOnItemDropDown(cityName);
    }
    @Then("user verify stasiun lists")
    public void user_verify_stasiun_lists(List<String> stasiunName) {
        for ( int i = 0; i < stasiunName.size() ; i++){
            Assert.assertTrue(stasiunhalte.isStationPresent(stasiunName.get(i)), stasiunName.get(i) + " is not present");
        }
    }
}
