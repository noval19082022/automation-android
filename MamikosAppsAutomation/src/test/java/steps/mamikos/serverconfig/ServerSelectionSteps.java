package steps.mamikos.serverconfig;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import pageobjects.facebook.account.ExistingLoginConfirmationPO;
import pageobjects.facebook.account.FacebookLoginPO;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.login.LoginOptionPO;
import pageobjects.mamikos.common.login.LoginPO;
import pageobjects.mamikos.common.profile.ProfilePO;
import pageobjects.mamikos.common.serverconfig.ServerSelectionPO;
import utilities.Constants;
import utilities.ThreadManager;

public class ServerSelectionSteps {
	private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
	private ServerSelectionPO serverSelection = new ServerSelectionPO(driver);
	private LoadingPO loading = new LoadingPO(driver);

	@Given("^Server configuration is selected$")
	public void server_configuration_is_selected() throws InterruptedException {
		if (Constants.MOBILE_OS == Platform.ANDROID) {
			serverSelection.selectServerDetailsAndClickOnConfirmButton(Constants.SERVER_KEY, Constants.SERVER_PAY);
			loading.waitLoadingFinish();
		} else {
			serverSelection.selectServerForiOS(Constants.SERVER_KEY, Constants.SERVER_PAY);
		}
	}
}


