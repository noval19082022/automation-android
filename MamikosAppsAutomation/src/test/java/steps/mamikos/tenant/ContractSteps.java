package steps.mamikos.tenant;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.ToastMessagePO;
import pageobjects.mamikos.common.profile.ProfilePO;
import pageobjects.mamikos.tenant.booking.BookingDetailsPO;
import pageobjects.mamikos.tenant.booking.BookingHistoryPO;
import pageobjects.mamikos.tenant.payment.ContractPO;
import utilities.AppiumHelpers;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

import static org.testng.Assert.assertTrue;

public class ContractSteps {
    AppiumHelpers appium;
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private ContractPO contract = new ContractPO(driver);
    private ProfilePO profile = new ProfilePO(driver);
    private BookingHistoryPO booking = new BookingHistoryPO(driver);
    private BookingDetailsPO bookingDetails = new BookingDetailsPO(driver);
    private JavaHelpers java = new JavaHelpers();

    //Test Data Payment
    private String paymentProperties ="src/test/resources/testdata/mamikos/payment.properties";
    private String titleMessageKosEmpty_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"titleMessageKosEmpty");
    private String bodyMessageKosEmpty_ = JavaHelpers.getPropertyValueForSquad(paymentProperties,"bodyMessageKosEmpty");

    @Then("system display page no boarding house was rented")
    public void system_display_page_no_boarding_house_was_rented() {
        Assert.assertEquals(contract.getTitleMessage(), titleMessageKosEmpty_, "Title message is different");
        Assert.assertEquals(contract.getBodyMessage(), bodyMessageKosEmpty_, "Body message is different");
        Assert.assertTrue(contract.isPresentSearchKosButton(), "Search kos button not present");
    }

//    @When("user click check in button with rental period is {string}")
//    public void user_click_check_in_button_with_rental_period_is(String rentPeriod) {
//        Assert.assertEquals(booking.getRentPeriod(), rentPeriod, "Rent period is " + booking.getRentPeriod());
//        booking.checkIn();
//    }

    @When("user check in from booking history page which rental period is {string}")
    public void user_check_in_from_booking_history_page_which_rental_period_is(String rentPeriod) throws InterruptedException {
        Assert.assertEquals(booking.getRentPeriod(), rentPeriod, "Rent period is " + booking.getRentPeriod());
        booking.checkIn();
    }

    @When("system display check in successfully")
    public void system_display_check_in_successfully() {
        Assert.assertTrue(booking.checkInSuccessfullyInfoIsPresent(), "Check in not successfully");
        booking.clickOnFinishCheckInButton();
    }

    @Then("user clicks link terminate contract with rental period is {string}")
    public void user_clicks_link_terminate_contract_with_rental_period_is(String rentDuration) throws InterruptedException {
        Assert.assertEquals(contract.getRentPeriod(), rentDuration, "Rent duration not " + rentDuration);
        contract.clickOnTerminateContract();
    }

    @When("user select reason terminate contract {string}")
    public void user_select_reason_terminate_contract(String reason) {
        contract.selectReasonTerminateContract(reason);
    }

    @When("user select date out from boarding house")
    public void user_select_date_out_from_boarding_house() throws InterruptedException, ParseException {
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        contract.selectTerminateDate();
        bookingDetails.setStartDate(today);
        contract.clickPilihTerminateDate();
    }

    @When("user give review kos")
    public void user_give_review_kos() {
        contract.clickOnGiveReview();
    }

    @When("user give a rating {int} for {string}")
    public void user_give_a_rating_for(Integer rating, String option) {
        contract.setStarRatingBar(rating, option);
    }

    @When("user input {string} on field kost review and click terminate contract button")
    public void user_input_on_field_kost_review_and_click_terminate_contract_button(String text) throws InterruptedException {
        contract.enterKostReview(text);
        contract.clickOnTerminateContractButton();
        contract.clickOnApplyTerminateContract();
    }

    @When("user repayment from booking history page which rental period is {string}")
    public void user_repayment_from_booking_history_page_which_rental_period_is(String rentPeriod) {
        Assert.assertEquals(booking.getRentPeriod(), rentPeriod, "Rent period is " + booking.getRentPeriod());
        booking.clickOnPayNowButton();
    }

    @Then("user click terminate contract {string}")
    public void user_click_terminate_contract(String rentDuration) throws InterruptedException {
        Assert.assertEquals(contract.getRentPeriod(), rentDuration, "Rent duration not " + rentDuration);
        contract.clickOnTerminateContract();
    }
}
