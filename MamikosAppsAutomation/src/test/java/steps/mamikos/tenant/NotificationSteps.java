package steps.mamikos.tenant;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.Platform;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.NotificationPO;
import utilities.Constants;
import utilities.ThreadManager;

public class NotificationSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private NotificationPO notification = new NotificationPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);

    @Given("user click first notification with message {string}")
    public void user_click_first_notification_with_message(String notificationTitle) throws InterruptedException {
        notification.clickOnFirstNotification();
    }

    @Given("user click first notification on tenant home page")
    public void user_click_first_notification_on_tenant_home_page() throws InterruptedException {
        if (Constants.MOBILE_OS == Platform.ANDROID) {
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        }
        notification.clickOnFirstNotification();
    }

    @And("user clicks notification button on tenant home page")
    public void user_clicks_notification_button_on_tenant_home_page() throws InterruptedException{
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            loading.waitLoadingFinish();
            popUp.clickOnLaterButton();
        }
        notification.clickNotificationIcon();
    }

    @Then("user see notification tenant with text {string}")
    public void user_see_notification(String notificationTitle) {
        notification.getFirstNotificationTitle();
        Assert.assertEquals(notification.getFirstNotificationTitle(), notificationTitle, "Notification title is different");
    }

    @Then("user see notification reject booking tenant with text {string}")
    public void user_see_notification_reject_booking(String notificationTitleReject) {
        Assert.assertEquals(notification.getRejectNotificationTitle(), notificationTitleReject, "Notification is : " +notification.getRejectNotificationTitle());
    }
}
