package steps.mamikos.tenant;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import pageobjects.banksimulator.BankChannelSimulatorPO;
import pageobjects.banksimulator.BankMandiriChannelSimulatorPO;
import pageobjects.mamikos.common.profile.ProfilePO;
import pageobjects.mamikos.tenant.NotificationPO;
import pageobjects.mamikos.tenant.payment.PaymentPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.concurrent.TimeUnit;

public class PaymentSteps {
    JavaHelpers java;
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private PaymentPO payment = new PaymentPO(driver);
    private NotificationPO notification = new NotificationPO(driver);
    private ProfilePO profile = new ProfilePO(driver);
    BankChannelSimulatorPO bni = new BankChannelSimulatorPO(driver);
    BankMandiriChannelSimulatorPO mandiri = new BankMandiriChannelSimulatorPO(driver);

    //Test Data Payment
    private String paymentData ="src/test/resources/testdata/mamikos/payment.properties";
    private String bni_payment_ = JavaHelpers.getPropertyValueForSquad(paymentData,"bni_payment");
    private String mandiri_payment_ = JavaHelpers.getPropertyValueForSquad(paymentData,"mandiri_payment");
    private String permata_payment_ = JavaHelpers.getPropertyValueForSquad(paymentData,"permata_payment");
    private String kostPrice_weekly_ = JavaHelpers.getPropertyValueForSquad(paymentData,"kostPrice_weekly");
    private String kostPrice_monthly_ = JavaHelpers.getPropertyValueForSquad(paymentData,"kostPrice_monthly");
    private String kostPrice_quarterly_ = JavaHelpers.getPropertyValueForSquad(paymentData,"kostPrice_quarterly");
    private String kostPrice_sixMonths_ = JavaHelpers.getPropertyValueForSquad(paymentData,"kostPrice_sixMonths");
    private String kostPrice_annually_ = JavaHelpers.getPropertyValueForSquad(paymentData,"kostPrice_annually");
    private String kostAdminFee_ = JavaHelpers.getPropertyValueForSquad(paymentData,"kostAdminFee");
    private String invoiceAdminFee_ = JavaHelpers.getPropertyValueForSquad(paymentData,"invoiceAdminFee");
    private String depositFee_ = JavaHelpers.getPropertyValueForSquad(paymentData,"depositFee");
    private String penaltyFee_ = JavaHelpers.getPropertyValueForSquad(paymentData,"penaltyFee");
    private String additionalCosts_ = JavaHelpers.getPropertyValueForSquad(paymentData,"additionalCosts");
    private String downPayment_ = JavaHelpers.getPropertyValueForSquad(paymentData,"downPayment");

    //Data tenant payment
    private String nameTenant_ = JavaHelpers.getPropertyValueForSquad(paymentData,"nameTenant");

    //Test Data Loyalty
    private String addVoucherData ="src/test/resources/testdata/mamikos/addVoucher.properties";
    private String roomPriceForApplyVoucherBBK_ = JavaHelpers.getPropertyValueForSquad(addVoucherData,"roomPriceForAddVoucher1");
    private String tenantNameApplyVoucher_ = JavaHelpers.getPropertyValueForSquad(addVoucherData,"tenantNameForAddVoucher1");
    private String downPaymentForApplyVoucher_ = JavaHelpers.getPropertyValue(addVoucherData,"downPaymentForApplyVoucher");
    private String tenantNameTargetedVoucher_ = JavaHelpers.getPropertyValueForSquad(addVoucherData,"tenantNameForAddVoucher4");
    private String roomPriceForTargetedVoucher_ = JavaHelpers.getPropertyValueForSquad(addVoucherData,"roomPriceForAddVoucher4");

    @When("user select payment method {string} for {string} case {string}")
    public void user_select_payment_method_for_case(String paymentMethod, String DPorRepayment, String casePayment) throws InterruptedException {
        String button = "Bayar Sekarang";
        String paymentURL = "";
        String getTotalAmount;
        String virtualAccountNumber = "";
        String kodePerusahaanMandiri = "";
        String kodePembayaranPermata = "";

        int totalAmount;
        int kosRent = 0;
        int weeklyPrice,
                monthlyPrice,
                quarterlyPrice,
                sixMonthlyPrice,
                annuallyPrice,
                downPayment,
                kostAdminFee,
                invoiceAdminFee,
                depositFee,
                additionalCosts,
                monthlyPriceKostVoucher,
                downPaymentForApplyVoucher,
                monthlyPriceKostVoucher2;

        payment.selectPaymentMethod(paymentMethod);
        payment.clickOnPayNowButton(button);

        if(paymentMethod.equals("Bank BNI")){
            virtualAccountNumber = payment.getVirtualAccountNumber();
            System.out.println(virtualAccountNumber);
        }else if(paymentMethod.equals("Bank Mandiri")) {
            virtualAccountNumber = payment.getVirtualAccountNumber();
            kodePerusahaanMandiri = payment.getKodePerusahaanMandiri();
        }else if(paymentMethod.equals("Bank Permata")) {
            kodePembayaranPermata = payment.getKodePembayaranPermata();
        }


        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1440x900");
        chromeOptions.addArguments("--proxy-server='direct://'");
        chromeOptions.addArguments("--proxy-bypass-list=*");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        if(paymentMethod.equals("Bank BNI")){
            driver.get(bni_payment_);
            bni.enterTextVirtualAccount(virtualAccountNumber);
            bni.clickOnSearchVirtualAccount();
        }else if(paymentMethod.equals("Bank Mandiri")){
            driver.get(mandiri_payment_);
            System.out.println(virtualAccountNumber);
            System.out.println(kodePerusahaanMandiri);
            mandiri.enterTextBillerCodeMandiri(kodePerusahaanMandiri);
            mandiri.enterTextVirtualAccountMandiri(virtualAccountNumber);
            mandiri.clickOnSearchVirtualAccountMandiri();
        }else if(paymentMethod.equals("Bank Permata")) {
            driver.get(permata_payment_);
            bni.enterKodePembayaranBankPermata(kodePembayaranPermata);
            bni.clickOnSearchKodePembayaran();
        }

        weeklyPrice = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_weekly_)));
        monthlyPrice = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_monthly_)));
        quarterlyPrice = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_quarterly_)));
        sixMonthlyPrice = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_sixMonths_)));
        annuallyPrice = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_annually_)));
        monthlyPriceKostVoucher = Integer.parseInt(String.valueOf(java.extractNumber(roomPriceForApplyVoucherBBK_)));
        monthlyPriceKostVoucher2 = Integer.parseInt(String.valueOf(java.extractNumber(roomPriceForTargetedVoucher_)));
        downPayment = Integer.parseInt(String.valueOf(downPayment_));
        downPaymentForApplyVoucher = Integer.parseInt(String.valueOf(downPaymentForApplyVoucher_));
        kostAdminFee = Integer.parseInt(String.valueOf(kostAdminFee_));
        invoiceAdminFee = Integer.parseInt(String.valueOf(invoiceAdminFee_));
        depositFee = Integer.parseInt(String.valueOf(depositFee_));
        additionalCosts = Integer.parseInt(String.valueOf(additionalCosts_));

        if (DPorRepayment.equals("down payment")) {
            switch (casePayment) {
                case "paymentW2":
                    kosRent = weeklyPrice;
                    break;
                case "paymentB3":
                    kosRent = monthlyPrice;
                    break;
                case "payment3B3":
                    kosRent = quarterlyPrice;
                    break;
                case "payment6B3":
                    kosRent = sixMonthlyPrice;
                    break;
                case "paymentT3":
                    kosRent = annuallyPrice;
                    break;
                case "paymentVoucherkuB1":
                    kosRent = monthlyPriceKostVoucher;
                    break;
            }
            if(casePayment.equals("paymentVoucherkuB1")){
                kosRent = (kosRent * downPaymentForApplyVoucher) / 100;
            }
            else{
                kosRent = (kosRent * downPayment) / 100;
            }
        } else if (DPorRepayment.equals("payment")) {
            switch (casePayment) {
                case "paymentW1":
                    kosRent = weeklyPrice;
                    break;
                case "paymentB1":
                    kosRent = monthlyPrice;
                    break;
                case "payment3B1":
                    kosRent = quarterlyPrice;
                    break;
                case "payment6B1":
                    kosRent = sixMonthlyPrice;
                    break;
                case "paymentT1":
                    kosRent = annuallyPrice;
                    break;
                case "paymentB2":
                    kosRent = monthlyPrice +
                            additionalCosts +
                            depositFee;
                    break;
                case "payment3B2":
                    kosRent = quarterlyPrice +
                            additionalCosts +
                            depositFee;;
                    break;
                case "payment6B2":
                    kosRent = sixMonthlyPrice +
                            additionalCosts +
                            depositFee;;
                    break;
                case "paymentT2":
                    kosRent = annuallyPrice +
                            additionalCosts +
                            depositFee;
                    break;
                case "paymentVoucherkuB1":
                    kosRent = monthlyPriceKostVoucher;
                    break;
                case "paymentVoucherkuB2":
                    kosRent = monthlyPriceKostVoucher2;
                    break;
            }
        } else {
            switch (casePayment) {
                case "paymentW2":
                    kosRent = weeklyPrice ;
                    break;
                case "paymentB3":
                    kosRent = monthlyPrice;
                    break;
                case "payment3B3":
                    kosRent = quarterlyPrice;
                    break;
                case "payment6B3":
                    kosRent = sixMonthlyPrice;
                    break;
                case "paymentT3":
                    kosRent = annuallyPrice;
                    break;

//                case "paymentW1":
//                    kosRent = weeklyPrice +
//                            additionalCosts +
//                            depositFee;
//                    break;
//                case "paymentB1":
//                    kosRent = monthlyPrice +
//                            additionalCosts +
//                            depositFee;
//                    break;
//                case "payment3B1":
//                    kosRent = quarterlyPrice +
//                            additionalCosts +
//                            depositFee;
//                    break;
//                case "payment6B1":
//                    kosRent = sixMonthlyPrice +
//                            additionalCosts +
//                            depositFee;
//                    break;
//                case "paymentT1":
//                    kosRent = annuallyPrice +
//                            additionalCosts +
//                            depositFee;
//                    break;
            }
            downPayment = (kosRent * 10)/100;
            kosRent = kosRent +
                    additionalCosts +
                    depositFee -
                    downPayment;
        }
        totalAmount = kosRent +
                kostAdminFee +
                invoiceAdminFee;

        if(paymentMethod.equals("Bank BNI")){
            if (casePayment.equals("paymentVoucherkuB1")){
                Assert.assertEquals(bni.getName(), tenantNameApplyVoucher_, "Tenant name is " + bni.getName());
            }
            else if (casePayment.equals("paymentVoucherkuB2")){
                Assert.assertEquals(bni.getName(), tenantNameTargetedVoucher_, "Tenant name is " + bni.getName());
            }
            else{
                Assert.assertEquals(bni.getName(), nameTenant_, "Tenant name is " + bni.getName());
            }
            getTotalAmount = String.valueOf(java.extractNumber(bni.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            bni.enterTextPaymentAmount(getTotalAmount);
            bni.clickOnPaymentButton();
            Assert.assertTrue(bni.getPaymentMessage().contains("Transaction success. VA number"), "Payment not success");
            bni.closeBrowser();
        }else if(paymentMethod.equals("Bank Mandiri")){
            getTotalAmount = String.valueOf(java.extractNumber(mandiri.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            mandiri.clickOnPaymentButton();
            Assert.assertTrue(mandiri.getPaymentMessage().contains("Success Transaction"), "Payment not success");
            mandiri.closeBrowser();
        }else if (paymentMethod.equals("Bank Permata")){
            getTotalAmount = String.valueOf(java.extractNumber(bni.getTotalAmountPermataLabel()));
            String getTotalAmountNew = getTotalAmount.substring(0, getTotalAmount.length() - 2);
            Assert.assertEquals(Integer.parseInt(getTotalAmountNew), totalAmount, "Total amount is " + getTotalAmount);
            bni.clickOnPaymentPermataButton();
            bni.closeBrowser();
        }else if(paymentMethod.equals("OVO")){
            payment.clickSayaSudahBayarButton();
            payment.clickConfirmSayaSudahBayarButton();
        }
    }

    @Then("system display payment successfully and back to main page")
    public void system_display_payment_successfully_and_back_to_main_page() throws InterruptedException {
        //payment.clickOnAlreadyPaidButton();
        payment.paymentSuccessfullyViewIsPresentAndCloseWebView();
        notification.clickOnBackImageView();
    }

//    @Then("system display repayment successfully and back to main page")
//    public void system_display_repayment_successfully_and_back_to_main_page() throws InterruptedException {
//        payment.paymentSuccessfullyViewIsPresent();
//    }

    @Then("system display repayment successfully and back to booking history page")
    public void system_display_repayment_successfully_and_back_to_booking_history_page() {
        payment.paymentSuccessfullyViewIsPresent();
    }

    @Then("system display down payment successfully and back to main page")
    public void system_display_down_payment_successfully_and_back_to_main_page() throws InterruptedException {
        //payment.clickOnAlreadyPaidButton();
        payment.paymentSuccessfullyViewIsPresentAndCloseWebView();
        notification.clickOnBackImageView();
    }

    @And("user clicks pilih metode pembayaran")
    public void user_clicks_pilih_metode_pembayaran() {
        payment.clickPilihMetodePembayaran();
    }

    @And("user click {string} button")
    public void user_click_button(String button) throws InterruptedException{
        payment.clickOnPayNowButton(button);
    }

    @Then("system display pembayaran berhasil with text {string}")
    public void system_display_pembayaran_berhasil_with_text(String textSuccess) {
        Assert.assertTrue(payment.getTextPembayaranBerhasil().contains(textSuccess), "Text pembayaran berhasil not " + textSuccess);
    }

    @And("user click selesai button")
    public void user_click_selesai_button() throws InterruptedException{
        payment.clickSelesaiPembayaranButton();
    }

}

