package steps.mamikos.tenant.editprofile;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.tenant.editprofile.EditProfilePO;
import steps.mamikos.search.ContactApartmentSteps;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.List;

public class EditProfileSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private EditProfilePO editProfilePO = new EditProfilePO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private LoadingPO loading = new LoadingPO(driver);

    @When("tenant click on edit profile")
    public void tenant_click_on_edit_profile() throws InterruptedException {
        editProfilePO.clickOnEditProfile();
    }

    @Then("tenant verify data profile information")
    public void tenant_verify_data_profile_information(List<String> information) throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID) {
            loading.waitLoadingFinish();
        }
        Assert.assertEquals(editProfilePO.getProfileName(), information.get(0), "Profile name is not equal to " + information.get(0));
        Assert.assertEquals(editProfilePO.getGender(), information.get(1), "Gender is not equal to " + information.get(1));
        Assert.assertEquals(editProfilePO.getProfession(), information.get(2), "Profession is not equal to " + information.get(2));
        Assert.assertEquals(editProfilePO.getOfficeName(), information.get(3), "Office name is not equal to " + information.get(3));
    }

    @And("Edit tenant name to {string}")
    public void edit_name_tenant(String name){
            editProfilePO.editNameTenant(name);
    }

    @And("Select tenant profession to Karyawan")
    public void select_tenant_profession_to_karyawan(){
        editProfilePO.clickOnKaryawanRadioButton();
    }

    @And("Select tenant profession to Mahasiswa")
    public void select_tenant_profession_to_mahasiswa(){
        editProfilePO.clickOnMahasiswaRadioButton();
    }

    @And ("Edit tenant profession name to {string}")
    public void edit_tenant_profession_name_to(String profession) {
        editProfilePO.editTenantProfession(profession);
        editProfilePO.saveProfile();
    }

    @When("system display message {string}")
    public void system_display_message_success_update_data(String message){
        if(Constants.MOBILE_OS == Platform.ANDROID){
            Assert.assertEquals(editProfilePO.getToastMessageUpdateProfile(), message,"expected is "+editProfilePO.getToastMessageUpdateProfile() );
        }
    }

    @And("user taps on informasi pribadi")
    public void user_taps_on_informasi_pribadi() {
        editProfilePO.clickOnInformasiPribadi();
    }

    @And("tenant clicks oke on popup success edit profile")
    public void tenant_clicks_on_popup_success_edit_profile() {
        editProfilePO.clickOnOkeButton();
    }

    @And("user taps on simpan edit profile")
    public void user_taps_on_simpan_edit_profile() throws InterruptedException {
        if(Constants.MOBILE_OS == Platform.ANDROID){
            loading.waitLoadingFinish();
        }
        editProfilePO.saveProfile();
    }
}
