package steps.mamikos.tenant;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.verification.verificationPO;
import utilities.ThreadManager;

public class verificationSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private verificationPO verification = new verificationPO(driver);

    @And("user tap verification akun")
    public void user_tap_verification_akun() {
        verification.clickOnVerificationAkun();
    }

    @And("user tap kartu identitas")
    public void user_tap_kartu_identitas() {
        verification.clickOnKartuIdentitas();
    }

    @And("user tap kartu identitas with selfie")
    public void user_tap_kartu_identitas_with_selfie() throws InterruptedException {
        verification.clickOnTakeCapturePhotoWithSelfie();
    }

    @And("user tap take capture photo")
    public void user_tap_take_capture_photo() throws InterruptedException {
        verification.clickOnTakeCapturePhoto();
    }

    @And("user tap done")
    public void user_tap_done() throws InterruptedException {
        verification.clickOnDone();
    }

    @And("user tap on term and condition checkbox")
    public void user_tap_term_and_condition_checkbox() throws InterruptedException {
        verification.clickOnCheckboxTermAndCondition();
    }
    @And("user tap on simpan verification akun")
    public void user_tap_simpan_verification_akun() throws InterruptedException {
        verification.clickOnSimpanVerificationAkun();
    }

    @Then("user can sees popup success")
    public void user_can_sees_popup_success() {
        Assert.assertTrue(verification.isElementWithText("Terima kasih, kini kamu dapat menikmati proses Booking Langsung lebih mudah"));
    }
}