package steps.mamikos.voucherku;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.voucherku.VoucherkuPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;

public class VoucherkuSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private VoucherkuPO voucherPO = new VoucherkuPO(driver);

    //Test Data Voucherku
    private String voucherkuProperties="src/test/resources/testdata/mamikos/voucherku.properties";
    private String redDotNumber = JavaHelpers.getPropertyValueForSquad(voucherkuProperties,"redDotNumber");


    @And("user clicks on profile button")
    public void user_clicks_on_profile_button() {
        voucherPO.clickOnProfile();
    }

    @And("user clicks on voucherku")
    public void user_clicks_on_voucherku() {
        voucherPO.clickOnVoucherku();
    }

    @And("user verify on Terpakai tab")
    public void user_verify_on_terpakai_tab() {
        voucherPO.clickOnTerpakaiTab();
    }

    @And("user verify on Kadaluwarsa tab")
    public void user_verify_on_kadaluwarsa_tab() {
        voucherPO.clickOnKedaluwarsaTab();
    }

    @And("user clicks on Promo Lainnya")
    public void user_clicks_on_promo_lainnya() {
        voucherPO.clickOnPromoPage();
    }

    @Then("user verify on Promo page {string}")
    public void user_verify_on_promo_page(String url) {
        Assert.assertEquals(voucherPO.getPromoUrl(), url, "Wrong Page");
    }

    @And("user verify on Tersedia tab")
    public void user_verify_on_tersedia_tab() {
        voucherPO.clickOnTersediaTab();
    }

    @Then("user verify Tersedia empty state")
    public void user_verify_tersedia_empty_state() {
        Assert.assertTrue(voucherPO.verifyTersediaEmptyVoucher(), "voucher is not empty");
    }

    @Then("user verify Terpakai empty state")
    public void user_verify_terpakai_empty_state() {
        Assert.assertTrue(voucherPO.verifyTerpakaiEmptyVoucher(), "voucher is not empty");
    }

    @Then("user verify Kedaluwarsa empty state")
    public void user_verify_kedaluwarsa_empty_state() {
        Assert.assertTrue(voucherPO.verifyKedaluwarsaEmptyVoucher(), "voucher is not empty");
    }

    @When("user click on user voucher")
    public void user_click_on_user_voucher() {
        voucherPO.clickOnVoucher();
    }

    @Then("user verify detail {string} voucher page")
    public void user_verify_detail_voucher_page(String tabName, List<String> detail) throws InterruptedException {
        if(tabName.equalsIgnoreCase("Tersedia")){
            Assert.assertTrue(voucherPO.isCopyCodeVoucherButtonEnabled(), "Button copy voucher code is not enabled");
            voucherPO.clickOnCopyCodeVoucherButton();
        }
        else{
            Assert.assertFalse(voucherPO.isCopyCodeVoucherButtonEnabled(), "Button copy voucher code is not disabled");
        }
        Assert.assertEquals(voucherPO.getTitleToolbarText(), detail.get(0), "Detail voucher title toolbar is not equal to " + detail.get(0));
        Assert.assertTrue(voucherPO.isDetailVoucherImagePresent(), "Detail voucher image banner is not present");
        Assert.assertTrue(voucherPO.isDetailVoucherTitleTextPresent(), "Detail voucher title is not present");
        Assert.assertTrue(voucherPO.isDueDateVoucherPresent(), "Voucher due date is not present");
        Assert.assertEquals(voucherPO.getDueDateVoucherText(), detail.get(2), "Voucher due date is not equal to " + detail.get(2));
        Assert.assertTrue(voucherPO.isTermAndConditionVoucherPresent(), "Terms and condition text is not present");
        Assert.assertTrue(voucherPO.isDetailVoucherInformationTextPresent(), "Detail voucher information is not present");
        Assert.assertEquals(voucherPO.getDetailVoucherTitleText(), detail.get(1), "Detail voucher title is not equal to " + detail.get(1));
        Assert.assertTrue(voucherPO.isCodeVoucherTitleTextPresent(), "Voucher code title is not present");
        Assert.assertEquals(voucherPO.getCodeVoucherText(), detail.get(3), "Voucher code is not equal to " + detail.get(3));
        Assert.assertTrue(voucherPO.isCopyCodeVoucherButtonPresent(), "Copy voucher code button is not present");
    }

    @Then("user verify red dot number")
    public void user_verify_red_dot_number() {
        Assert.assertEquals(voucherPO.getRedDotCountReminderVoucher(), redDotNumber, "Red dot value is not equal to " + redDotNumber);
    }

    @Then("user verify voucher lists")
    public void user_verify_voucher_lists() {
        Assert.assertTrue(voucherPO.countVoucherSize() > 0, "There's no voucher");
    }

    @Then("user verify voucher page")
    public void user_verify_voucher_page(List<String> voucher) throws InterruptedException {
        Assert.assertEquals(voucherPO.getTitleToolbarText(), voucher.get(0), "Voucher title toolbar is not equal to " + voucher.get(0));
        Assert.assertEquals(voucherPO.getOtherPromoText(), voucher.get(1), "Other promo text is not equal to " + voucher.get(1));
        Assert.assertEquals(voucherPO.getDueDateVoucherText(),voucher.get(2),"Due Date voucher text is not equal to " + voucher.get(2));
        Assert.assertTrue(voucherPO.isDueDateVoucherPresent(), "Voucher due date is not present");
        Assert.assertEquals(voucherPO.getCodeVoucherTitleText(), voucher.get(3), "Voucher title is not equal to "+ voucher.get(3));
        Assert.assertEquals(voucherPO.getCodeVoucherText(), voucher.get(4),"Voucher code is not equal to " + voucher.get(4));
        Assert.assertTrue(voucherPO.isTersediaTabPresent(), "Tersedia tab is not present");
        Assert.assertTrue(voucherPO.isTerpakaiTabPresent(), "Terpakai tab is not present");
        Assert.assertTrue(voucherPO.isKadaluwarsaTabPresent(), "Kadaluwarsa tab is not present");
        Assert.assertTrue(voucherPO.isVoucherBannerImagePresent(), "Voucher banner image not present");
        Assert.assertTrue(voucherPO.isCopyCodeVoucherButtonPresent(), "Copy voucher code button is not present");
        Assert.assertTrue(voucherPO.isCopyCodeVoucherButtonEnabled(), "Button copy voucher code is not enabled");
        voucherPO.clickOnCopyCodeVoucherButton();
    }

    @Then("user verify used voucher displayed")
    public void user_verify_used_voucher_displayed(List<String> voucher) throws InterruptedException {
        Assert.assertEquals(voucherPO.getTitleToolbarText(), voucher.get(0), "Voucher title toolbar is not equal to " + voucher.get(0));
        Assert.assertEquals(voucherPO.getOtherPromoText(), voucher.get(1), "Other promo text is not equal to " + voucher.get(1));
        Assert.assertTrue(voucherPO.isDueDateVoucherPresent(), "Voucher due date is not present");
        Assert.assertEquals(voucherPO.getDueDateVoucherText(),voucher.get(2),"Due Date voucher text is not equal to " + voucher.get(2));
        Assert.assertEquals(voucherPO.getCodeVoucherTitleText(), voucher.get(3), "Voucher title is not equal to "+ voucher.get(3));
        Assert.assertEquals(voucherPO.getCodeVoucherText(), voucher.get(4),"Voucher code is not equal to " + voucher.get(4));
        Assert.assertTrue(voucherPO.isTersediaTabPresent(), "Tersedia tab is not present");
        Assert.assertTrue(voucherPO.isTerpakaiTabPresent(), "Terpakai tab is not present");
        Assert.assertTrue(voucherPO.isKadaluwarsaTabPresent(), "Kadaluwarsa tab is not present");
        Assert.assertTrue(voucherPO.isVoucherBannerImagePresent(), "Voucher banner image not present");
        Assert.assertTrue(voucherPO.isCopyCodeVoucherButtonPresent(), "Copy voucher code button is not present");
        Assert.assertFalse(voucherPO.isCopyCodeVoucherButtonEnabled(), "Button copy voucher code is not disabled");
    }

    @Then("user verify expired voucher displayed")
    public void user_verify_expired_voucher_displayed(List<String> voucher) throws InterruptedException {
        Assert.assertEquals(voucherPO.getTitleToolbarText(), voucher.get(0), "Voucher title toolbar is not equal to " + voucher.get(0));
        Assert.assertEquals(voucherPO.getOtherPromoText(), voucher.get(1), "Other promo text is not equal to " + voucher.get(1));
        Assert.assertTrue(voucherPO.isDueDateVoucherPresent(), "Voucher due date is not present");
        Assert.assertEquals(voucherPO.getDueDateVoucherText(),voucher.get(2),"Due Date voucher text is not equal to " + voucher.get(2));
        Assert.assertEquals(voucherPO.getCodeVoucherTitleText(), voucher.get(3), "Voucher title is not equal to "+ voucher.get(3));
        Assert.assertEquals(voucherPO.getCodeVoucherText(), voucher.get(4),"Voucher code is not equal to " + voucher.get(4));
        Assert.assertTrue(voucherPO.isTersediaTabPresent(), "Tersedia tab is not present");
        Assert.assertTrue(voucherPO.isTerpakaiTabPresent(), "Terpakai tab is not present");
        Assert.assertTrue(voucherPO.isKadaluwarsaTabPresent(), "Kadaluwarsa tab is not present");
        Assert.assertTrue(voucherPO.isVoucherBannerImagePresent(), "Voucher banner image not present");
        Assert.assertTrue(voucherPO.isCopyCodeVoucherButtonPresent(), "Copy voucher code button is not present");
        Assert.assertFalse(voucherPO.isCopyCodeVoucherButtonEnabled(), "Button copy voucher code is not disabled");
    }

    @Then ("user verify voucher is displayed")
    public void user_verify_voucher_is_displayed(List<String> voucher) throws InterruptedException {
        for (String s : voucher) {
            Assert.assertTrue(voucherPO.isVoucherTargetedPresent(s), "Voucher is not displayed");
            voucherPO.clickOnBackArrowButton();
            voucherPO.clickOnVoucherku();
        }
    }

    @Then ("user verify voucher is not displayed")
    public void user_verify_voucher_is_not_displayed(List<String> voucher) throws InterruptedException {
        for (String s : voucher) {
            Assert.assertFalse(voucherPO.isMassVoucherTargetedNotPresent(s), "Mass Voucher targeted for other Kos city is displayed");
            voucherPO.clickOnBackArrowButton();
            voucherPO.clickOnVoucherku();
        }
    }

    @And ("user click on back arrow on voucher list")
    public void user_click_on_back_arrow_on_voucher_list() {
        voucherPO.clickOnBackArrowButton();
    }

}
