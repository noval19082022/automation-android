package steps.mamikos.wishlist;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.aspectj.weaver.ConcreteTypeMunger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.common.LoadingPO;
import pageobjects.mamikos.common.PopUpPO;
import pageobjects.mamikos.common.login.LoginPO;
import pageobjects.mamikos.owner.premium.UpgradePremiumPO;
import pageobjects.mamikos.tenant.booking.BookingDetailsPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import pageobjects.mamikos.tenant.wishlist.WishlistPO;
import utilities.Constants;
import utilities.ThreadManager;


public class WishlistSteps {

    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private WishlistPO wishlist = new WishlistPO(driver);
    private LoginPO login = new LoginPO(driver);
    private BookingDetailsPO bookingDetails = new BookingDetailsPO(driver);
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private LoadingPO loading = new LoadingPO(driver);
    private PopUpPO popUp = new PopUpPO(driver);
    private UpgradePremiumPO premiumPO = new UpgradePremiumPO(driver);
    private FooterPO footer = new FooterPO(driver);

    @Then("user sees main Wish list mobile app")
    public void user_sees_main_wish_list_mobile_app() throws InterruptedException {
        Assert.assertTrue(wishlist.isWishlistTitleVisible(), "Wishlist Title is not Visible!");
        Assert.assertTrue(wishlist.isFavoriteTabVisible(), "Favorite tab is not Visible!");
        Assert.assertTrue(wishlist.isHistoryTabVisible(), "History tab is not Visible!");
        Assert.assertTrue(login.isLoginButtonDisplayed(), "Login button is not Visible!");
        if(Constants.MOBILE_OS == Platform.ANDROID){
            Assert.assertTrue(login.isTermTextVisible(), "Term and conditions is not Visible!");
        }
        wishlist.clickHistoryTab();
        Assert.assertTrue(wishlist.isVisitedHintTextVisible(), "Visited Hint text is not Visible!");
        if(wishlist.isSearchOtherKosButtonVisible()){
            Assert.assertTrue(wishlist.isSearchOtherKosButtonVisible(), "Search other button is not Visible!");
        } else {
            Assert.assertTrue(wishlist.isHistoryKosSeenVisible(), "History kos seen is not Visible!");
        }
    }

    @Then("user sees main Wish list mobile app after login")
    public void user_sees_main_wish_list_mobile_app_after_login() throws InterruptedException {
        Assert.assertTrue(wishlist.isWishlistTitleVisible(), "Wishlist Title is not Visible!");
        Assert.assertTrue(wishlist.isFavoriteTabVisible(), "Favorite tab is not Visible!");
        Assert.assertTrue(wishlist.isHistoryTabVisible(), "History tab is not Visible!");
        if(wishlist.isSearchOtherKosButtonVisible()){
            Assert.assertTrue(wishlist.isSearchOtherKosButtonVisible(), "Search other button is not Visible!");
        } else {
            Assert.assertTrue(wishlist.isFavoritePropertyVisible(), "Search other button is not Visible!");
        }
        wishlist.clickHistoryTab();
        Assert.assertTrue(wishlist.isVisitedHintTextVisible(), "Visited Hint text is not Visible!");
        if(wishlist.isSearchOtherKosButtonVisible()){
            Assert.assertTrue(wishlist.isSearchOtherKosButtonVisible(), "Search other button is not Visible!");
        } else {
            Assert.assertTrue(wishlist.isHistoryKosSeenVisible(), "History kos seen is not Visible!");
        }
    }

    @Then("user clicks on property details and verify the property details")
    public void user_clicks_on_property_details_and_verify_the_property_details() throws InterruptedException {
        String propertyName = wishlist.getPropertyName();
        wishlist.clickProperty();
        loading.waitLoadingFinish();
        bookingDetails.dismissFTUE_screen();
        Assert.assertTrue(bookingDetails.getRoomNameText().contains(propertyName), "Room name is not match!");
    }

    @And("user refresh the page")
    public void user_refresh_the_page() throws InterruptedException {
        loading.refreshPage();
    }


    @And("user will see their favorited kosts")
    public void user_will_see_their_favorited_kosts() {
        Assert.assertTrue(searchListing.isGenderPresent(), "Gender is not present");
        Assert.assertTrue(searchListing.isKostPricePresent(), "Kost price is not present");
        Assert.assertTrue(searchListing.isKostNamePresent(), "Kost is not present");
    }

    @Then("user navigate to Seen Tab")
    public void user_navigate_to_Seen_Tab() throws InterruptedException {
        searchListing.clickOnSeenTab();
    }

    @And("user click on Love Button in Kost Detail")
    public void user_click_on_love_button_in_kost_detail() throws InterruptedException {
        wishlist.clickProperty();
        bookingDetails.clickOnLoveButton();
        searchListing.back();
    }

    @And("user see the kost name similar with {string}")
    public void user_see_the_kost_name_similar_with(String kost) {
       Assert.assertTrue(wishlist.getKostName().contains(kost), "Seen kost didn't appear!");
    }

    @And("user click favorite icon and confirm kos in favorite tab")
    public void user_click_favorite_icon_and_confirm_kos_in_favorite_tab() throws InterruptedException {
        String propertyName = wishlist.getPropertyName();
        wishlist.clickFavoriteIcon();
        if(Constants.MOBILE_OS == Platform.ANDROID){
            wishlist.clickHistoryTab();
        }
        loading.waitLoadingFinish();
        String favPropertyName = wishlist.getKostName();
        Assert.assertEquals(propertyName,favPropertyName, "Cannot favorite a property!");
    }

    @And("user clicks on property details and click unfavorite icon")
    public void user_clicks_on_property_details_and_click_unfavorite_icon() throws InterruptedException {
        String propertyName = wishlist.getKostName();
        if (Constants.MOBILE_OS == Platform.ANDROID){
            wishlist.clickProperty();
        }
        bookingDetails.clickOnLoveButton();
        if (Constants.MOBILE_OS== Platform.ANDROID) {
            searchListing.back();
        }else{
            premiumPO.clickOnBackIconButton();
            footer.clickOnExploreMenu();
            footer.clickOnFavoriteMenu();
            wishlist.refreshPageFavorite();
        }
        if (wishlist.isFavoriteKostEmpty()){
            Assert.assertTrue(wishlist.isFavoriteKostEmpty(), "Favorite property still exist!");
        } else {
            if(Constants.MOBILE_OS == Platform.ANDROID){
                loading.waitLoadingFinish();
                popUp.clickOnLaterButton();
                Assert.assertTrue(wishlist.getPropertyName().contains(propertyName), "Favorite property still exist!");
            }
        }
    }

    @Then("user click favorite icon and confirm kos in favorite menu on kos listing")
    public void user_click_favorite_icon_and_confirm_kos_in_favorite_menu_on_kos_listing() throws InterruptedException {
        String propertyName = wishlist.getPropertyName();
        wishlist.clickFavoriteIcon();
        searchListing.clickFavoriteMenu();
        loading.refreshPage();
        String favPropertyName = wishlist.getPropertyName();
        Assert.assertEquals(propertyName,favPropertyName, "Cannot favorite a property!");
    }
}