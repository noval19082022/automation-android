@regression @login @user-growth @iOS
Feature: Login

  @wrongCredentials
  Scenario: Login with invalid credentials and verify validation message
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user fill out form and login with invalid mobile and password info
    Then correct validation message "Nomor HP dan password tidak sesuai" is displayed

  @WrongPass
  Scenario: Login with invalid password and verify validation message
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "wrongpassword"
    Then correct validation message "Nomor HP dan password tidak sesuai" is displayed

  @wrongNumber
  Scenario: Login with invalid mobile number and verify validation message
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "wrongnumber"
    Then correct validation message "Nomor HP dan password tidak sesuai" is displayed

  @invalidPhoneValue
  Scenario: Login As Owner with invalid Phone Number from Already has account page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user click on login button from register page
    And user enter credential "09999999999" and "mamikosignite" and click on Login Button
    Then correct validation message "Mohon diawali dengan 08" is displayed

  @eyeDisplayOrNot
  Scenario: Login as Owner with valid credential and click eye button to see password
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user enter credential "0812345670001" and "qwerty123" and click on Password toggle
    Then user verify visible password field with "qwerty123"
