@regression @newFlowLoginOwner @user-growth
  Feature: New Flow Login Owner

    @fromNotification
    Scenario: New Flow Login Owner - From Notification
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When User clicks Notification Icon on Homepage
      Then user verify "Login dulu yuk!" screen

    @fromDetailKos
    Scenario: New Flow Login Owner - From Detail Kos
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user tap search bar
      And user input search with "Parangtritis Beach"
      And user choose from results with address "Parangtritis Beach, Pantai, Parangtritis, Bantul, Daerah Istimewa Yogyakarta, Indonesia"
      And user click on BBK pop up
      When user click on available room lists
      When user click on detail facility
      Then user verify login owner and tenant button