@regression @register @user-growth
Feature: Register

  @openOwnerRegistrationFromHomepage
  Scenario: Open Owner Registration Page from Home page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Daftar kos gratis link text to navigate Registration Page
    Then Owner Registration page is displayed with heading "Daftar Pemilik Kos"

  @register1 @iOS
  Scenario: Navigate to Owner Registration Page from Login popup
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    Then Owner Registration page is displayed with heading "Daftar Pemilik Kos"

  @UsedMobile
  Scenario: Number has been registered as Tenant validation
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "089245645630", owner Email "test@xyz.com", password "test@123" in registration form and click on Register Button
    Then correct validation message "Nomor yang Anda masukkan tidak valid atau sudah terdaftar." is displayed

  @ownerMobile
  Scenario:  Number has been registerd as Owner validation
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing", Registered Mobile Number, owner Email "test@xyz.com", password "test@123" in registration form and click on Register Button
    Then correct validation message "Nomor yang Anda masukkan tidak valid atau sudah terdaftar." is displayed

  @number
  Scenario: Add Mobile number less then 8 characters and validate error
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "0899999", owner Email "test@xyz.com", password "test@123" in registration form and click on Register Button
    Then correct validation message "Mohon masukkan nomor HP yang valid" is displayed


  @eightDigitNumber
  Scenario: Add Mobile Number with 8 - 14 characters and validate verification screen
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "08123456985264", owner Email "test@xyz.com", password "test@123" in registration form and click on Register Button
    Then user redirect to OTP screen Which has text "Masukkan kode verifikasi"

  @blankPassword
  Scenario: Blank password and validate daftar button is disable
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "08123456985264", owner Email "test@xyz.com", in registration form
    Then validate the Register button is disable

  @passwordValidation
  Scenario: Password with Special characters and validate OTP screen is shown
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "08123456985264", owner Email "test@xyz.com", password "test@123" in registration form and click on Register Button
    Then user redirect to OTP screen Which has text "Masukkan kode verifikasi"

  @passwordError
  Scenario: Enter password less then eight digit and validate error message
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "08123456985264", owner Email "test@xyz.com", password "test@12" in registration form and click on Register Button
    Then correct validation message "Password minimal 8 karakter" is displayed

  @passwordConfirm
  Scenario: Enter password grater or equal to 8 digit and validate next screen
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "08123456985264", owner Email "test@xyz.com", password "test@12345" in registration form and click on Register Button
    Then user redirect to OTP screen Which has text "Masukkan kode verifikasi"

  @otpError
  Scenario: Invalid OTP and validate error message
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing" mobile number "08123456985264", owner Email "test@xyz.com", password "test@12345" in registration form and click on Register Button
    And user enter OTP "1" "2" "3" "4" and click on verify button
    Then system display toast message "Kode tidak valid, harap periksa kembali."

  @passwordCheck
  Scenario: Check eye icon for password field
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "testing", owner Email "test@xyz.com", password "test@12345" in registration form and Click on eye icon to show password
    Then verify enter password "test@12345" is correct

  @wrongEmails
  Scenario Outline: Blank email, Incorrect format email and Email has been registered as owner
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "Rheza Haryo Hanggara" mobile number "08219282129", owner Email "<Email>", password "qwerty123" in registration form and click on Register Button
    Then user verify error messages "<Error Message>"
    Examples:
    | Email               | Error Message                   |
    | incorrect.com       | Mohon masukkan email yang valid |
    | rheza@mamikos.com   | Email ini sudah terdaftar       |

  @blankEmail
  Scenario: Blank email
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "Rheza Haryo Hanggara" mobile number "08219282129", owner Email "", password "qwerty123" in registration form
    Then validate the Register button is disable

  @wrongNames
  Scenario Outline: Input name with special character and number; less than 3 character
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "<Name>" mobile number "08219282129", owner Email "mamikosaye@mamikos.com", password "qwerty123" in registration form and click on Register Button
    Then user verify error messages "<Error Message>"
    Examples:
      | Name                  | Error Message                   |
      | !@#$%3212             | Mohon masukkan karakter alfabet |
      | rh                    | Minimal 3 karakter              |

  @blankName @iOS
  Scenario: Not input value to name field
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    Then user enter data as owner name "" mobile number "08219282129", owner Email "mamikosaye@mamikos.com", password "qwerty123" in registration form and click on Register Button Case 2

  @nameMoreThan20Characters
  Scenario: Input name more than 20 character
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "Rheza Haryo Hanggara Aye Aye" mobile number "08219282129", owner Email "mamikosaye@mamikos.com", password "qwerty123" in registration form and click on Register Button
    Then user verify input verification code page

  @correctEmail
  Scenario: Correct email and redirect to input verification code page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "Rheza Haryo Hanggara" mobile number "0821446518800", owner Email "rheza@xyz.com", password "qwerty123" in registration form and click on Register Button
    Then user verify input verification code page

  @resendCode
  Scenario: Resend verification code after countdown end
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "Rheza Haryo Hanggara" mobile number "0821446518800", owner Email "rheza@xyz.com", password "qwerty123" in registration form and click on Register Button
    When user click resend verification code
    Then user verify resend verification code countdown

  @phoneMoreThan14Character
  Scenario: Phone more than 14 Character
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on Register Account button
    And user enter data as owner name "Rheza Haryo Hanggara" mobile number "08778989898998989898", owner Email "rheza@xyz.com", password "qwerty123" in registration form and click on Register Button
    Then user verify input verification code page