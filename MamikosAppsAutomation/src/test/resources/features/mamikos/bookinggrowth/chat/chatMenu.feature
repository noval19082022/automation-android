@regression @chat

Feature: Chat list page and other menu related to chat

  @emptyChatDetails @ownerExp
  Scenario: Chat details when chat room not exists
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "new owner"
    And user clicks on Cancel option verification when asked on pop up
    And user clicks on Close Email Verification when show on pop up
    When owner user navigates to chat page
    Then user see empty background image
    And user see text "Tidak ada percakapan" in chat list page

    @chatBeforeLogin @bookingGrowth
    Scenario: Try to chat kos before login will show login popup
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user navigates to Explore page
      And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
      And user tap chat button in kos detail
      Then verify login page should be shown
