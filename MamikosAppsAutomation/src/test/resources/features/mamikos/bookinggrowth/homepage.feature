@regression @homepage @bookingGrowth

Feature: Homepage

  @homepageBeforeLogin
  Scenario: Check homepage before login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    Then user sees main Homepage mobile app

  @homepageAfterLogin
  Scenario: Check homepage after login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "DC Automation"
    Then user sees main Homepage mobile app after login

  @thumbnailCategory
  Scenario:  Check thumbnail functionality and redirection
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user click on Kos tab
    Then verify search field, my current location, Popular Search, search by city option should be displayed
    And user click back
    When user click on Apartment Tab from home page
    Then verify search field, my current location, Popular Search, search by city option should be displayed
    And user click back
    When user click on Goods and Services tab
    Then user see DEKATMU pop up announcement
    And user click back
    And user click on Job vacancy Tab
    Then verify search field, my current location, Popular Search, search by city option should be displayed

  @homeButton
  Scenario: Put application background by clicking on device Home button
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    Then click on device home button and verify app should be closed

  @scrollBehavior
  Scenario: Check scrolling page behavior to bottom section
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    Then user scroll down to bottom page

  @registerBoarding @user-growth
  Scenario: Access register from daftar kos gratis
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Daftar kos gratis link text to navigate Registration Page
    Then Owner Registration page is displayed with heading "Daftar Pemilik Kos"

  @testimonial
  Scenario: Check element testimonial section
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    Then user see the testimonial elements

  @RecommendationLoc
  Scenario: Check list of recommendation location
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Location button from home page and verify below location list
      | Jakarta       |
      | Semarang      |
      | Surabaya      |
      | Bandung       |
      | Solo          |
      | Tangerang     |
      | Makassar      |
      | Denpasar Bali |
      | Medan         |
      | Depok         |
      | Malang        |
      | Jogja         |
      | Lokasi Saya   |

  @cityNameFilter
  Scenario: Check list of promoted location
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on see All Promoted Kos linked text from home page and click on city dropdown
    Then user verify city list
      | Semua Kota        |
      | Jakarta Barat     |
      | Jakarta Selatan   |
      | Jakarta Timur     |
      | Jakarta Utara     |
      | Tangerang         |
      | Tangerang Selatan |
      | Bogor             |
      | Bekasi            |
      | Depok             |
      | Yogyakarta        |

  @footerCheck
  Scenario: Verify bottom bar after login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "DC Automation"
    When user navigates to Explore page
    Then user verify bottom bar should be shown with all the tabs

  @recommendation
  Scenario: Check content of recommendation kos
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on location button and select "Jogja"
    Then verify recommendation kos has kos image and contains

  @listKos
  Scenario: Check see all kos redirection and verify list of kos recommendation
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on location button and select "Surabaya"
    And Click on see all recommendation Kos linked
    Then verify below list of kose should be shown city "Surabaya"

  @checkContains
  Scenario: Check list of recommendation kost
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on location button and select "Bandung"
    And Click on see all recommendation Kos linked
    Then verify some elements like back button, Filter button, URUTKAN button, booking language button, map view button and kos details should be shown

  @promoKosWidget
  Scenario: Check promo kos display and functionality
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user scroll down to bottom page
    Then user sees the promo widget elements