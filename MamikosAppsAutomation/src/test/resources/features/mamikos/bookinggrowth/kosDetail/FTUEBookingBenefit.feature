@regression @kostDetail @FTUEBookingBenefit @bookingGrowth

Feature: FTUE Booking Benefit

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up

  @BBKKos
  Scenario: First time open BBK Kos
    When user tap on main search in Homepage
    And user search for Kost with name "FTUEKostName1" and selects matching result without dimiss FTUE
    Then FTUE Booking Benefit appear with slides

  @NonBBKKos
  Scenario: First time open Non BBK Kos, FTUE BB will not appear
    When user tap on main search in Homepage
    And user search for Kost with name "FTUEKostName3" and selects matching result without dimiss FTUE
    Then verify FTUE Element not present on that page

  @apartemen
  Scenario: FTUE will not appear on apartemen
    When user click on Apartment Tab from home page
    And user search for Kost with name "Rane 221 yoy" and selects matching result without dimiss FTUE
    Then verify FTUE Element not present on that page

  @2ndOpenBBKKos
  Scenario: Second time open BBK Kos before finish
    When user tap on main search in Homepage
    And user search for Kost with name "FTUEKostName1" and selects matching result without dimiss FTUE
    And FTUE Booking Benefit appear with slides
    And user back to Home page
    And user click back if mamikos pop up survey appear
    When user navigates to Explore page
    And user tap on main search in Homepage
    And user search for Kost with name "FTUEKostName2" and selects matching result without dimiss FTUE
    Then FTUE Booking Benefit appear with slides

  @2ndOpenBBKKosFinish
  Scenario: Second time open BBK Kos after finish
    When user search for Kost with name "Kost BAR 1" and selects matching result and navigate to Kost details page
    And user back to Home page
    And user click back if mamikos pop up survey appear
    When user navigates to Explore page
    And user tap on main search in Homepage
    And user search for Kost with name "FTUEKostName2" and selects matching result without dimiss FTUE
    Then verify FTUE Element not present on that page
    
  @finishFTUE
  Scenario: Finish FTUE
    When user tap on main search in Homepage
    And user search for Kost with name "FTUEKostName1" and selects matching result without dimiss FTUE
    And FTUE Booking Benefit appear with slides
    And user click Saya Mengerti button FTUE
    Then verify FTUE Element not present on that page

  @outerSideOfFTUE
  Scenario: try to close FTUE by tapping on out side of popup
    When user search for Kost with name "booking" and selects matching result and navigate to Kost details page for check FTUE
    Then user click on outer side of FTUE popup and verify popup does not dismiss

  @FTUEBackButton
  Scenario: verify FTUE popup should be shown with first card
    When user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user navigate back via device back button and verify kos list should be shown
    Then navigate again "Kose Mamiset Automation" kost details page and verify FTUE popup should be shown with first card
