@regression @chat @kostDetail

  Feature: Chat between Tenant and Owner

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page

  @checkQuestions @bookingGrowth
  Scenario: Show all selectable questions before chat
    When user tap chat button in kos detail
    Then user see phone number field and selectable question options :
      | Saya butuh cepat nih. Bisa booking sekarang? |
      | Ada diskon untuk kos ini?                    |
      | Masih ada kamar?                             |
      | Alamat kos di mana?                          |
      | Cara menghubungi pemilik?                    |
      | Boleh tanya-tanya dulu?                      |
      | Bisa pasutri?                                |
      | Boleh bawa hewan ?                           |

  @chatPredefMsg @bookingGrowth
  Scenario: Tenant can see selected and predefined message in chat page
    Given user tap chat button in kos detail
    When user select message "Cara menghubungi pemilik ?"
    And user tap Send
    Then user see text "Cara menghubungi pemilik ?" and autotext from CS Mamikos like below in chat page
      | Chat ini terhubung langsung dengan pemilik kos. Jadi, silakan langsung tanya apapun di sini. Balas di Mamikos - https://mamikos.com |

  @chatToOwner
  Scenario: Tenant can send message to Owner
    Given user tap chat button in kos detail
    And user select message "Cara menghubungi pemilik ?"
    And user tap Send
    And user enter text "What's your phone number?" in chat page
    And user navigates to main page after chat
    And user navigates to profile page
    And user logs out as a Tenant user
    And user logs in as Owner "master"
    When owner user navigates to chat page
    And owner user tap latest chat
    Then user should see tenant chat message : "What's your phone number?"
    When user enter text "My phone is 00000000001" in chat page
    And user navigates to main page after chat
    And owner user navigates to profile page
    And user logs out as a Owner user
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Chat page
    And tenant user tap latest chat
    Then user should see owner chat message : "My phone is 00000000001"

  @checkResponseQuestions @bookingGrowth
  Scenario Outline: Check autoreply text after select question <title>
    When user tap chat button in kos detail
    And user select question "<question>"
    Then chat room appear with latest message "<autoreply text>"
    Examples:
      | title                    | question                   | autoreply text                                                                                                                                                                                                              |
      | Tanya-tanya dulu         | Boleh tanya-tanya dulu?    | Boleh dong. Silakan tanya apapun. Chat ini dibaca langsung oleh pemilik kos. *** Mohon hati-hati, Mamikos tidak bertanggung jawab atas semua transaksi di luar platform Mamikos. *** Balas di Mamikos - https://mamikos.com |
      | Cara menghubungi pemilik | Cara menghubungi pemilik ? | Chatroom ini telah terhubung dengan pemilik kost, Anda dapat mengajukan pertanyaan dan berkomunikasi dengan pemilik iklan secara real time atau hubungi                                                                     |
      | Ada diskon               | Ada diskon untuk kos ini?  | Diskon yang berlaku saat ini:                                                                                                                                                                                               |
      | Tanya alamat             | Alamat kos di mana?        | Kose Mamiset Automation beralamat di:                                                                                                                                                                                       |
      | Bawa Hewan               | Boleh bawa hewan ?         | Kamu tidak boleh membawa hewan ke kos ini.                                                                                                                                                                                  |

