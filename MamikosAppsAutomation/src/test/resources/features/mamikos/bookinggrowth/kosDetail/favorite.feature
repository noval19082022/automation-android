@regression @favoriteKostDetail @bookingGrowth @kostDetail

Feature: Favorite

  @favoriteBeforeLogin @iOS
  Scenario: Check favorite before login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user navigates to Favorite page
    Then verify it should redirect to login page

  @favoriteAfterLogin @iOS
  Scenario: Check favorite after login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user navigates to Favorite page
    Then user sees main Wish list mobile app after login

  @favoriteRedirection
  Scenario: Favorite redirection to property details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user navigates to Favorite page
    And user refresh the page
    Then user clicks on property details and verify the property details

  @haveSeenTab
  Scenario: Check seen tab functionality
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "MamiPoin Tenant"
    When user search for Kost with name "Kost BAR 1" and selects matching result and navigate to Kost details page
    And user back to Home page from kost details
    When user navigates to Favorite page
    And user navigate to Seen Tab
    And user refresh the page
    Then user see the kost name similar with "Kos DC BAR Automation Tipe A"

  @favoriteSeenTab
  Scenario: Check functionality of favorite in seen tab
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "MamiPoin Tenant"
    When user navigates to Favorite page
    And user navigate to Seen Tab
    And user click favorite icon and confirm kos in favorite tab
    Then user clicks on property details and click unfavorite icon

  @favoriteKosListingBeforeLogin
  Scenario: Check functionality of favorite in kos listing before login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user click favorite icon
    Then user verify login owner and tenant button

  @favoriteKosListingAfterLogin
  Scenario: Check functionality of favorite in kos listing after login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    Then user click favorite icon and confirm kos in favorite menu on kos listing





