@regression @kostDetails @bookingGrowth
Feature: Kost Detail

  @backButton
  Scenario: Click on Back button to navigate previous page from kost details page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user click on back button to navigate previous page
    Then should display text field

  @shareButton
  Scenario: Try to share the kost
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    When user click on share button kost
    Then share social media pop up appear

  @otherRoom
  Scenario: Check kost list from Other Interesting Boarding Houses section
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user scroll down to Other Interesting Boarding Houses section
    And user verify other kost list should be shown
      | Uplift Mamirooms 03 |
      | Uplift Mamirooms 04 |

    Then user swipe right end verify below list
      | Kost MamiRooms Tobelo Asri  |
      | Kost Kulit Jeruk  |
      | Kos non aktif aja   |

  @otherKostBackButton
  Scenario: Try to back button from Other Interesting Boarding Houses section
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user scroll down to Other Interesting Boarding Houses section
    Then user click on other kost and click on back button to navigate back to previous kost details page

  @listJobVacancy
  Scenario: Check the list of Recommended Job Vacancies Around
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And scroll down to Job vacancy section and verify below element should shown
      | FULL-TIME         |
      | Gaji dirahasiakan |
    Then click on first job to open vacancy page and click on back button to navigate back to kost details page


  @seeAllReviewAndBackButton
  Scenario: Check Read All Review option and click on back button to navigate back to kost details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user search for Kost with name "review" and selects matching result and navigate to Kost details page
    And user click on read all review button and verify Review page gets open with heading "Reviews"
    Then click on back button of review page to navigate back to Kost details page and verify Booking Button is displayed

  @filterReview
  Scenario: try to change the review shorting
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user search for Kost with name "review" and selects matching result and navigate to Kost details page
    And user click on read all review button and verify Review page gets open with heading "Reviews"
    And user click on filter button and select "Urutkan dari Terlama"
    Then verify below data
      | 14 May 2019 |
      | 14 May 2019 |
      | 14 May 2019 |
      | 14 May 2019 |
      | 03 Apr 2020 |

  @kosIdentity
  Scenario: verify the identity of the Kost
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user navigates to Explore page
    And user search for Kost with name "Kost BAR 1" and selects matching result and navigate to Kost details page
    Then user see the identity on Kost details

  @ownerProfile
  Scenario: verify owner profile section
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user navigates to Explore page
    And user search for Kost with name "Kost BAR 1" and selects matching result and navigate to Kost details page
    And user scroll down to owner profile section
    And user see the elements on owner section
    Then user verify the wording on owner statistic
      | Booking Diproses        |
      | Booking Disetujui       |

  @kosRule
  Scenario: Validate Kos rule element on kos detail
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user navigates to Explore page
    And user search for Kost with name "Kost BAR 1" and selects matching result and navigate to Kost details page
    And user scroll down to kos rule section
    Then user can see kos rule list on detail kos rule
    And user can see kos rule image on detail kos
    And user swipe the image of kos rule