@regression @bookingGrowth @kostDetail @reportKost

Feature: Report kost section on Detail Kost

  @reportBeforeLogin
  Scenario: Try to report kost before login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user click on report kost linked text
    And user will see pop up "Login Akun" or "Selamat Datang di Mamikos"

  @reportAfterLogin
  Scenario: Try to report kost after login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user click on report kost linked text
    And report kos heading "Laporkan Kos" should be shown
    And user select report category and write the description report "Report Kost Automation"
    And system display toast message "Anda sudah memberikan laporan untuk kos ini."
    Then click on back button to navigate back to Kost details page and verify Booking Button is displayed