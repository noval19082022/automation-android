@regression @search @iOS @bookingGrowth
  Feature: Kost Listing

    @kostlisting
    Scenario: Try to Love or Favorite a kost before login
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user tap search bar
      When user tap Area
      And user click on Pencarian Populer "Yogyakarta"
      And user tap saya mengerti in pop up that appear
      When user tap Tampilan Daftar
      And user click on Love Button
      Then user will see pop up "Login Akun" or "Saya ingin login akun sebagai"


      @Kostlistingcase2
    Scenario: Check functionality of kost lists
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user log in as Tenant via phone number as "DC Automation"
      When user navigates to Explore page
      And user tap search bar
      When user tap Area
      And user click on Pencarian Populer "Yogyakarta"
      And user tap saya mengerti in pop up that appear
      When user tap Tampilan Daftar
      And user click on Love Button
      And user back to Explore page
      And user click back if mamikos pop up survey appear
      When user navigates to Favorite page
      And user refresh the page
      And user will see their favorited kosts
      And user click on Love Button in Kost Detail
      Then user navigate to Seen Tab
