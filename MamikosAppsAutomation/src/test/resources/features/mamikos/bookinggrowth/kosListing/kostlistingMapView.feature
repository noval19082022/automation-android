@regression  @search @mapView @bookingGrowth
Feature: mapView

  @numberCluster
  Scenario: Click on number cluster on map view and verify kose list
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap search bar
    When user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user click on Map view button
    And click on green cluster number "38. rooms." from map view
    Then verify below list of kose should be shown
      | Sisa 1 Kamar      |
      | Sisa 1 Kamar      |
      | Sisa 1 Kamar      |
      | Sisa 1 Kamar      |
      | Tersedia 8 Kamar  |
      | Sisa 2 Kamar      |

    @priceCluster
  Scenario: Click on price cluster on map view and verify kose list and details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap search bar
    When user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user click on Map view button
    And click on price cluster "17. rooms." to open kos details
    Then verify kose price should be shown "Rp 500.000 / bulan"

  @numberClusterForApartment
  Scenario: Check number cluster for Apartment and verify apartment details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on Map view button
    And click on green cluster number "8. rooms." from map view
    Then verify below list of apartment should be shown
    |Sukasari|
    |Sukasari|
    |Sukasari|
    |Sukasari|
    And user click on apartment to open apartment details
    Then system display apartment name in detail page

  @priceClusterForApartments
  Scenario: Check price cluster for Apartment and verify apartment details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak Dalam" and select matching result from area list
    And user click on Map view button
    And click on price cluster "13. rooms." to open apartment details
    Then system display apartment name in detail page

  @checkApartmentDetails
  Scenario: Check apartment cluster list view and verify apartment details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on Map view button
    Then user click on Filter button and verify below list
      | Tipe Unit    |
      | Perabotan    |
      | Jangka Waktu |
      | Range Harga  |
    And user navigate back to previous page
    And click on green cluster number "4. rooms." from map view
    And verify Filter button, Shorting Button, should be shown and map view button should not shown
    Then user click on Filter button and verify below list
      | Tipe Unit    |
      | Perabotan    |
      | Jangka Waktu |
      | Range Harga  |
    When user navigate back to previous page
    And user click on apartment to open apartment details
    Then system display apartment name in detail page
