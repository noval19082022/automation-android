@menu @iOS @regression @homepage
Feature: Menu

  @navbarBeforeLogin @bookingGrowth
  Scenario: Navigate to different pages without login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user navigates to Favorite page
    Then verify it should redirect to login page
    And user navigates to Chat page
    Then verify login page should be shown
    And user click back
    And user navigates to Booking Tenant page
    Then verify login page should be shown
    And user click back
    And user navigates to Login page
    Then verify login page should be shown

  @navbarAfterLogin @bookingGrowth
  Scenario: Navigate to different pages with login as tenant
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "DC Automation"
    And user navigates to Favorite page
    And user navigates to Chat page
    And user navigates to Booking Tenant page
    And user navigates to profile page

  Scenario: Navigate to different pages with login as owner
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And owner user navigates to manage page
    And owner user navigates to home page
    And owner user navigates to chat page
    And owner user navigates to profile page
    And user navigates to Booking page
    
  @helpPage
  Scenario: Verify MamiHelp page gets open without login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Favorite page
    And click on mami help button
    Then verify help page should be open

  @OkButton
  Scenario: Verify I Understand button on MamiHelp page without login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Favorite page
    And click on mami help button
    And verify different help option should be shown
    And Click on I Understand button
    Then verify it should redirect to login page

  @checkFields
  Scenario: Check field Contact us via WA, CS and Help Center on Mamihelp page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Favorite page
    And click on mami help button
    And verify different help option should be shown
    Then click on WA help button and verify below option should be shown
      | Seputar Pencari atau Kost  |
      | Seputar Pembayaran Booking |
      | Seputar Booking            |
    And Click on back button to close popup
    Then click on CS help button and verify below option
      | Seputar Pencari atau Kost  |
      | Seputar Pembayaran Booking |
      | Seputar Booking            |
    And Click on back button to close popup
    Then click on Help Center button and verify FAQ page gets open in web view
    