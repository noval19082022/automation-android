@regression @search @bookingGrowth @mainSearch
Feature: Search

  @mainKeyword @iOS
  Scenario: Typing on Search box
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap on main search in Homepage
    Then User type with city name "yogyakarta" and should display the autocomplete result area

  @autocompleteChar
  Scenario: Typing keyword after 3 characters, user will get 2 type of search result
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap on main search in Homepage
    Then User input with three character "jal"
    And should display the autocomplete result list

  @negAutocompleteChar
  Scenario: Type 3 characters in exception
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap on main search in Homepage
    Then User input with three character exception "jln"
    And should NOT display the autocomplete result list

  @randomChar
  Scenario: Input with random keyword
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap on main search in Homepage
    Then User input with random character "wurhqfpegywgcun"
    And should display information "Tidak menemukan nama tempat yang sesuai, Ubah kata kunci pencarian"

  @clearKeyword @iOS
  Scenario: Clear Keyword
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap on main search in Homepage
    Then User type with city name "tebet" and should display the autocomplete result area
    And user clear the text box

  @savedKeywordFromResultList @iOS
  Scenario: Saved keyword from result list
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap on main search in Homepage
    And User type with city name "bandung" and should display the autocomplete result area
    And user choose one of the list result
    And user on tampilan peta page and click back to Homepage
    And user tap on main search in Homepage
    Then should display saved keyword "Bandung" on main search

  @savedKeywordFromList
  Scenario: Saved keyword from list
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user tap on main search in Homepage
    And User type with city name "kaliurang" and should display the autocomplete result area
    And user clear the text box
    And user choose "UGM" list university
    And user on tampilan peta page and click back to Homepage
    And user tap on main search in Homepage
    Then should display saved keyword "UGM" on main search

  @locationOn
  Scenario: Positive case find a boarding house in my location
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When User click field search boarding house
    And User click text view my location
    And User click "Saya Mengerti" on popup bisa booking
    Then System display boarding house in "Sleman" and Verify the 5 highest lists

  @locationOff
  Scenario: Negative case find a boarding house in my location when location service is off
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When User click field search boarding house
    And User set off location service
    And User click text view my location
    Then System display "To continue, turn on device location, which uses Google’s location service" on popup message location service
    And User click button "OK" on popup message location service
    And User click "Saya Mengerti" on popup bisa booking

  @pencarianPopularSection
  Scenario: Tenant can reach main search page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And Tenant tap on Search input field
    Then Tenant can see pencarian popular section

  @topCampus
  Scenario: Tenant can see popular campus list
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And Tenant tap on Search input field
    And Tenant can see pencarian popular section
    Then Tenant see
      | list of popular campus |
      | UGM                    |
      | UNPAD Jatinangor       |
      | STAN Jakarta           |
      | UNAIR                  |
      | UB                     |
      | UNY                    |
      | UI                     |
      | UNDIP                  |
      | ITB                    |
      | UMY                    |

  @topCampusButtonFunctionality
  Scenario: Tenant can tap on popular campus
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And Tenant tap on Search input field
    And Tenant can see pencarian popular section
    And Tenant tap on "UGM"
    Then Tenant should see cari kost page

  @CampusBasedOnCity
  Scenario: Tenant can see City name and after expand tenant will see child campus list
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And Tenant tap on Search input field
    And Tenant can see pencarian popular section
    Then Tenant can see city list
      | Bali    |
      | Bandung |
      | Bogor   |
      | Depok   |
      | Jakarta |
    And Tenant tap on city "Bali"
    Then Tenant can see in the city child
      | ISI Denpasar |
      | Undiksha     |
      | STP Nusa Dua |
      | Unwar        |
      | Undhira      |
    And Tenant tap one of kampus in the list "ISI Denpasar"
    Then Tenant should see cari kost page

  @stationstopbycities @iOS
  Scenario: Search kost using stasiun and halte filtering
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user tap search bar
    And user click on "Stasiun & Halte" tab
    And user click on "Yogyakarta" drop down
    Then user verify stasiun lists
      | Stasiun Wates       |
      | Stasiun Lempuyangan |
      | Stasiun Rewulu      |
      | Stasiun Patukan     |
      | Stasiun Maguwo      |
      | Stasiun Tugu        |
      | Stasiun Sentolo     |

  @nothingKosAround
  Scenario: Search result of nothing kos around
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user tap on main search in Homepage
    Then User type with city name "sadasari" and should display the autocomplete result area
    And user choose one of the list result
    And user dismiss the pop up that appear
    Then user verify the elements of empty state page
    And user verify the wording on empty state page
    | Belum Ada Kos di Area ini                                       |
    | Coba cari di area lain untuk meningkatkan hasil pencarian kos.  |