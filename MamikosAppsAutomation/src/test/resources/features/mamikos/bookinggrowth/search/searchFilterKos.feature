@regression @search @bookingGrowth
Feature: Filter Search Kost

  @filterElement
  Scenario: Check filter
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Filter
    Then user verify the elements on filter
    And user verify the wording on filter menu
      | Tipe Kos                                                                              |
      | Putra                                                                                 |
      | Putri                                                                                 |
      | Campur                                                                                |
      | Spesial dari Mamikos                                                                  |
      | Promo Ngebut                                                                          |
      | Dapat diskon untuk sewa 1 - 6 bulan. Diskon hanya berlaku selama program berlangsung. |
      | Kos Andalan                                                                           |
      | Kos terverifikasi dengan dukungan keamanan dan jaminan dari Mamikos.                  |
      | Booking Langsung                                                                      |
      | Bisa langsung booking dan bayar kos tanpa ketemuan dengan pemilik kos.                |
      | MamiChecker                                                                           |
      | Kondisi kos sesuai dengan iklan. Sudah dikunjungi oleh tim Mamikos.                   |
      | Singgahsini                                                                           |
      | Kos dikelola dengan standar kualitas dari Mamikos.                                    |
      | Harga                                                                                 |
      | Minimal                                                                               |
      | Maksimal                                                                              |
      | Waktu Bayar Sewa                                                                      |
      | Mingguan                                                                              |
      | Bulanan                                                                               |
      | Per 3 Bulan                                                                           |
      | Per 6 Bulan                                                                           |
      | Tahunan                                                                               |
      | Aturan Kos                                                                            |
      | Fasilitas Kamar                                                                       |
      | Fasilitas Bersama                                                                     |

  @filterByGender
  Scenario Outline: Filter kos search result with gender, price and booking by time                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Filter by Gender Male, Payment Weekly, has Price range, and Min Payment 2 month
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Filter
    And user select "<Gender>" in kost type by Gender
    And user select "<Time>" in Jangka Waktu
    And user set price range "<Min Price>" - "<Max Price>"
    And user tap Search button
    Then kosan list appear correctly, relevant with the filter
      | <Gender>    |
      | <Time>      |
      | <Min Price> |
      | <Max Price> |
    Examples:
      | Gender | Time     | Min Price | Max Price |
      | Putri  | Bulanan  | 300000    | 1000000   |
      | Campur | Mingguan | 10000     | 1500000   |

  @filterMinPayment
  Scenario Outline: Filter kos search result with min Payment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               Filter by Gender Male, Payment Weekly, has Price range, and Min Payment 2 month
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Filter
    And user select minimum payment "<Min Payment>"
    And user tap Search button
    Then kosan list appear correctly by min. payment
      | <Min Payment> |
    Examples:
      | Min Payment |
      | Per 3 Bulan |
      | Mingguan    |

  @filterFacilities
  Scenario Outline: Filter kos search result with min Payment, Facility and Others                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Filter by Gender Male, Payment Weekly, has Price range, and Min Payment 2 month
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Filter
    And user select Facility :
      | <Facility1> |
      | <Facility2> |
      | <Others>    |
    And user tap Search button
    Then kosan list appear correctly by facility, relevant with the filter below :
      | <Facility1> |
      | <Facility2> |
      | <Others>    |
    Examples:
      | Facility1    | Facility2      | Others                      |
      | Laundry      | Akses 24 Jam   | Kasur                       |
      | Bisa pasutri | K. Mandi Dalam | Boleh bawa hewan peliharaan |