@regression @search @searchListing @bookingGrowth
Feature: Search

  Scenario: Search Area List
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    Then After user click City name, city name will expand and Area name listed below it.
      | Balikpapan         | Banyumas           | Cilegon    | Tasikmalaya | Yogyakarta      |
      | Balikpapan Kota    | Purwokerto Barat   | Purwakarta | Cibeureum   | Yogyakarta      |
      | Balikpapan Utara   | Purwokerto Timur   | Jombang    | Tawang      | Bantul          |
      | Balikpapan Tengah  | Purwokerto Selatan | Grogol     | Mangkubumi  | Kota Yogyakarta |
      | Balikpapan Selatan | Purwokerto Utara   | Citangkil  | Cihideung   | Sleman          |

  Scenario: Menu sorting sesuai
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Tampilan Daftar
    And user tap Urutkan
    Then Sorting list is appear by :
  | Acak |
  | Harga termurah |
  | Harga termahal |
  | Kosong ke penuh |
  | Update terbaru  |

  Scenario: Sorting kos di tampilan daftar secara Acak
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Tampilan Daftar
    And user tap Urutkan
    And user tap Acak
    Then kosan list appear as random from top to bottom, with rule :
      | BookingSponsored       |
      | SponsoredOnly          |
      | BookingOnly            |
      | NotBookingNotSponsored |

  Scenario: Sorting kos in List View by Lowest Price
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Tampilan Daftar
    And user tap Urutkan
    And user tap Lowest Price
    Then kosan list appear as lowest price from top to higher price

  Scenario: Sorting kos in List View by Highest Price
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Tampilan Daftar
    And user tap Urutkan
    And user tap Highest Price
    Then kosan list appear as highest price from top to lower price

    @searchSorting @iOS
  Scenario: Sorting kos in List View by Latest Update
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Tampilan Daftar
    And user tap Urutkan
    And user tap Latest Update
    Then kosan list appear as latest update from top to less updated :
      | Update Hari Ini   |
      | Update Kemarin    |
      | Hari Lalu         |
      | Minggu Lalu       |
      | Bulan Terakhir    |
      | Lama Tidak Update |

  Scenario: Sorting kos in List View by Kosong ke penuh
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    When user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user tap saya mengerti in pop up that appear
    And user tap Tampilan Daftar
    And user tap Urutkan
    And user tap Kosong ke penuh
    Then kosan list appear as empty from top to fully booked :
    | Ada Kamar       |
    | Tinggal 1 Kamar |
    | Penuh           |