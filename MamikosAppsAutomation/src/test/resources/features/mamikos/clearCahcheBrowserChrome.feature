@regression @clear-cache

Feature: Clear cache on chrome browser

  Scenario: First Tenant Login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "add voucher"
    And user navigates to profile page
    Then user login is "Dorothy Vijayvergiyaberg"

  Scenario: Second Tenant Login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "gender female"
    And user navigates to profile page
    Then user login is "Mamitest List Buking"

  Scenario: Third Tenant Login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "non voucher"
    And user navigates to profile page
    Then user login is "Karen Listein"

  Scenario: Fourth Tenant Login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "voucher"
    And user navigates to profile page
    Then user login is "Olivia Sadanson"

  Scenario: Fifth Tenant Login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user navigates to profile page
    Then user login is "Lingga Marqansyah"