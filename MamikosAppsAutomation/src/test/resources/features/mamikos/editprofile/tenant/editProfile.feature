@regression @editProfileTenant
Feature: Tenant - Edit Profile Page

  Scenario: Go to edit profile and verify page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "UG Edit Profile"
    And user navigates to profile page
    Then tenant verify display profile name
      | Rheza Haryo Hanggara |
      | 0898765432166        |
    When tenant click on edit profile
    Then tenant verify data profile information
      | Rheza Haryo Hanggara        |
      | Laki - laki                 |
      | Karyawan                    |
      | PT. Mama Teknologi Property |

  @changeTenantName
  Scenario: Edit Tenant Name
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "UG Edit Tenant"
    And user navigates to profile page
    And tenant click on edit profile
    And tenant verify data profile information
      | Fathul Khair                |
      | Laki - laki                 |
      | Karyawan                    |
      | PT. Mama Teknologi Property |
    And Edit tenant name to "Ntuls Khair"
    And tenant click on edit profile
    And tenant verify data profile information
      | Ntuls Khair                 |
      | Laki - laki                 |
      | Karyawan                    |
      | PT. Mama Teknologi Property |
    Then Edit tenant name to "Fathul Khair"
