@essentialTest1 @homePage @detailBookingAndPengajuanSewa @ios
Feature: Detail Booking and Pengajuan Sewa Essential Test

  Scenario: Admin Master Terminate Contract
    Given User navigate to "http://pay-jambu.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "Kost andalusia spanyol eropa timur" and click on terminate the Contract Button from action column

  Scenario: Pengajuan Sewa And Detail Booking, Click Chat pemilik and Cancel Booking
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user search for Kost with name "Kost Andalusia Spanyol Eropa Timur" and selects matching result and navigate to Kost details page
    And tenant taps on payment estimation
    And tenant taps on Ajukan Sewa button
    And tenant chooses today date and taps on booking button
    Then tenant can sees down payment rule on booking form
    When user selects T&C checkbox and clicks on Book button

#  Scenario: Detail Booking, Click Chat pemilik and Cancel Booking
    And user tap on lihat status pengajuan booking button
    And user tap chat pemilik in detail booking
    Then user redirect to chat room
    And user click back to booking detail
    And user tap on cancel booking button
    And user tap yes cancel booking button
    Then user can sees success cancel button pop-up
