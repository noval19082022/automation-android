@essentialTest1 @favorite @ios @favoriteEverSeen
Feature: Favorite Kost Essential Test

  Scenario: Favorite Kost on tab Pernah Dilihat
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user search for Kost with name "Kost Andalusia Spanyol Eropa Timur" and selects matching result and navigate to Kost details page
    And user click on love button
    And user back to Home page
    And user navigates to Favorite page
    And user click favorite icon and confirm kos in favorite tab
    Then user see the kost name similar with "Kost andalusia spanyol eropa timur"
    And user clicks on property details and click unfavorite icon