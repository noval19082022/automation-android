@essentialTest1 @homePage @ios @kelolaKostOwner
Feature: Kelola Kost Owner

  Scenario: Owner Update Kost Price
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "085600867992", password "Bismillah@02" and clear text is "false"
    And user goes to update room and price page
    And user taps on kost list with name "kost automation new"
    Then user can sees price list :
      | Harga Per Bulan |
    When user taps on "Lihat harga lainnya"
    Then user can sees price list :
      | Harga Per Hari    |
      | Harga Per Minggu  |
      | Harga Per 3 Bulan |
      | Harga Per 6 Bulan |
      | Harga Per Tahun   |
    When user taps on "Tampilkan lebih sedikit"
    Then user can sees price list :
      | Harga Per Bulan |
    And user cannot sees other price options
    And user updates main price to "500000"
    And user updates main price to "780000"

  Scenario: Owner Update Room Availability
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "085600867992", password "Bismillah@02" and clear text is "false"
    And user goes to update room avaibility
    And user taps on kost list with name "kost automation new"
    Then user updates total room to "6"