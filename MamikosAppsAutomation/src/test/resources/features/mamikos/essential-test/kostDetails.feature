@kost-detail-dp-essential-test @essentialTest1 @bookingwithDP @ios
Feature: Kost Detail Down Payment Essential Test

  Scenario: Admin Master Terminate Contract
    Given User navigate to "http://pay-jambu.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "DP Deposit Irish" and click on terminate the Contract Button from action column

  Scenario: Tenant Booking Kost For Kost With Down Payment Required
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "tenant" go to login form
    * tenant login with phone number "087708777615", password "qwerty123" and clear text is "false"
    * user navigates to Explore page
    * user search for Kost with name "DP Deposit Irish With Add Price Kotagede Yogyakarta" and selects matching result and navigate to Kost details page
    Then tenant can sees title with text "Bisa bayar DP (uang muka) dulu" subtitle with text "Biaya DP adalah 20% dari harga sewa yang dipilih."
    When tenant taps on payment estimation
    Then tenant can sees estimate detail payment prices list below :
      | Pembayaran Pertama            |
      | Uang Muka (DP)                |
      | Pelunasan                     |
      | Pembayaran Selanjutnya        |
      | Harga Sewa Per Bulan          |
      | Biaya Tambahan                |
      | Biaya Layanan Mamikos         |
      | Total Pembayaran Selanjutnya  |
    When user taps on element with text "Uang Muka (DP)" on detail pembayaran
    Then tenant can sees down payment detail
    When tenant taps on tutup button
    * user taps on element with text "Pelunasan" on detail pembayaran
    Then tenant can sees repayment detail
    When tenant taps on tutup button
    * user taps on element with text "Biaya Tambahan" on detail pembayaran
    Then tenant can sees additional price detail
    When tenant taps on tutup button
    * tenant taps on Ajukan Sewa button
    * tenant chooses today date and taps on booking button
    Then tenant can sees down payment information on Ringkasan Pengajuan Sewa
    When user selects T&C checkbox and clicks on Book button

  Scenario: Owner Accept Tenant Booking Essential Test Down Payment
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "081362464341", password "1d0lt3stb4ru" and clear text is "false"
    And owner clicks on Booking Details
    And user clicks on Accept button
    And user clicks on Next button
    And owner choose first available room and choose next button
    And owner taps on save button

    @checkInvoice
  Scenario: Tenant Check Invoice And Booking Details For Down Payment Booking
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "087708777615", password "qwerty123" and clear text is "false"
    And tenant click on bayar disini button
    Then tenant can sees invoice view with price is 401000