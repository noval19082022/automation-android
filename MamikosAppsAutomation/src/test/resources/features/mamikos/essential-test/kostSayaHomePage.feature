@essentialTest1 @homePage @ios @kostSayaHomepage
Feature: Kost Saya Home Page Essential Test

  Scenario: Admin Master Terminate Contract
    Given User navigate to "http://pay-jambu.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "Kost Andalusia Spanyol Eropa Timur" and click on terminate the Contract Button from action column

  Scenario: Kost Saya Home Page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user search for Kost with name "Kost Andalusia Spanyol Eropa Timur" and selects matching result and navigate to Kost details page
    And tenant taps on payment estimation
    And tenant taps on Ajukan Sewa button
    And tenant chooses today date and taps on booking button
    Then tenant can sees down payment rule on booking form
    When user selects T&C checkbox and clicks on Book button

  Scenario: Owner Accept Tenant Booking Essential Test
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "085600867992", password "Bismillah@02" and clear text is "false"
    And owner clicks on Booking Details
    And user clicks on Accept button
    And user clicks on Next button
    And owner choose first available room and choose next button
    And owner taps on save button

  Scenario: Tenant Check Kost After Booking
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user as owner navigates to profile page
    And user navigates to Booking History page
    Then tenant can sees kost name "Kost andalusia spanyol eropa timur" with "Butuh Pembayaran" status from kost index 0
