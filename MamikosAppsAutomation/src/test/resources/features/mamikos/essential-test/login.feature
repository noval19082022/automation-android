@essentialTest2 @staging @regression @essentialTest @ios
Feature: Login Essential Test

  @login-tenant-essential
  Scenario: Tenant Login With Valid And Invalid Phone Number
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "tenant" go to login form
    * tenant login with phone number "087708777680", password "ekwaokaeokreokrawer" and clear text is "false"
    But tenant can see invalid login with message is "Nomor HP dan password tidak sesuai"
    When tenant login with phone number "087708777615", password "ekwoakefokdafoke" and clear text is "true"
    But tenant can see invalid login with message is "Nomor HP dan password tidak sesuai"
    When tenant login with phone number "087708777615", password "qwerty123" and clear text is "true"
    Then tenant can sees profile icon

  @login-owner-essential
  Scenario: Owner Login With Valid And Invalid Phone Number
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "088362464234", password "dlafdjfadfasdf" and clear text is "false"
    But tenant can see invalid login with message is "Nomor HP dan password tidak sesuai"
    When owner login with phone number "081362464341", password "fsdadfadfasddf" and clear text is "true"
    But tenant can see invalid login with message is "Nomor HP dan password tidak sesuai"
    When owner login with phone number "081362464341", password "1d0lt3stb4ru" and clear text is "true"
    Then owner can sees profile icon

  @login-tenant-via-chat
  Scenario: Tenant Login via chat menu
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "0890000000359", password "qwerty123" and clear text is "true"
    Then user redirect to chat list room

  @login-owner-via-chat
  Scenario: Owner Login via chat menu
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Owner"
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "true"
    * owner taps later button on staging
    Then owner can sees profile icon