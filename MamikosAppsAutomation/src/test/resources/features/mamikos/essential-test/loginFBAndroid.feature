@essentialTest2 @staging @regression @essentialTest
Feature: Login Essential Test

  @clear-data-facebook
  Scenario: clear FB
   # Given user clear data FB
    Given user clear cookies web view android

  @login-tenant-by-facebook
  Scenario: Tenant - Login Facebook
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    And user click login by facebook email "lingga_ccabvrn_marqansyah@tfbnw.net", password "joinmamikos" and clear text is "true"
    And user is on main page and click on Later option when asked on pop up
    Then user navigates to Explore page