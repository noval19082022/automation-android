@essentialTest2 @staging @regression @essentialTest @ios2
Feature: Login Essential Test

  @deleteCookies
  Scenario: Delete cookies login web facebook
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And user clear cookies web view ios

  @login-tenant-by-facebook
  Scenario: Tenant - Login Facebook
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And user click login by facebook email "lingga_ccabvrn_marqansyah@tfbnw.net", password "joinmamikos" and clear text is "true"
    And user is on main page and click on Later option when asked on pop up
    Then tenant can sees profile icon