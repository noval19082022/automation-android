@essentialTest2 @notification @ios
Feature: Notification Owner and Tenant

  Scenario: Admin Master Terminate Contract
    Given User navigate to "http://pay-jambu.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "kost automation android only" and click on terminate the Contract Button from action column

  Scenario: Kost Saya Home Page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "0890000000359", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user search for Kost with name "Kost Automation Android Only Tobelo Utara Halmahera Utara" and selects matching result and navigate to Kost details page
    And tenant taps on payment estimation
    And tenant taps on Ajukan Sewa button
    And tenant chooses today date and taps on booking button
    Then tenant can sees down payment rule on booking form
    When user selects T&C checkbox and clicks on Book button

  @notificationRequestBooking
  Scenario: Check notification center when owner get new tenant request booking
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    And user clicks notification button
    Then user see notification with text "Ada Pengajuan Sewa Baru kost automation android only Tobelo Utara Halmahera Utara"
    When user click first notification
    Then system display page Detail Booking
    When user clicks on Accept button
    And user clicks on Next button
    And owner choose first available room and choose next button
    And owner taps on save button

  @notificationBookingAccepted @notificationSuccessPayment
  Scenario: Check notification and information on homepage when owner accept booking and Tenant Pembayaran Berhasil
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "tenant" go to login form
    * tenant login with phone number "0890000000359", password "qwerty123" and clear text is "false"
    And user clicks notification button on tenant home page
    Then user see notification tenant with text "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    When user click first notification on tenant home page
    Then user redirected to Pembayaran page
    When user clicks pilih metode pembayaran
    And user select payment method "OVO"
    And user click "Bayar Sekarang" button
    And user click selesai button

  # Terminate Contract
  Scenario: Admin Master Terminate Contract
    Given User navigate to "http://pay-jambu.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "Kost Automation Android Only Tobelo Utara Halmahera Utara" and click on terminate the Contract Button from action column

  Scenario: Tenant Booking
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "0890000000359", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user search for Kost with name "Kost Automation Android Only Tobelo Utara Halmahera Utara" and selects matching result and navigate to Kost details page
    And tenant taps on payment estimation
    And tenant taps on Ajukan Sewa button
    And tenant chooses today date and taps on booking button
    Then tenant can sees down payment rule on booking form
    When user selects T&C checkbox and clicks on Book button

  Scenario: Owner Reject Booking
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    And user clicks notification button
    Then user see notification with text "Ada Pengajuan Sewa Baru kost automation android only Tobelo Utara Halmahera Utara"
    When user click first notification
    Then system display page Detail Booking
    When user clicks on Reject button
    * user clicks on Next button reject booking
    * owner choose "Kamar kos sudah ada yang booking" on reason to reject booking
    * user click saya mengerti button
    * owner click TnC button
    * user clicks on Next button reject booking

  Scenario: Tenant Check notification and information on homepage when owner Reject booking
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "tenant" go to login form
    * tenant login with phone number "0890000000359", password "qwerty123" and clear text is "false"
    And user clicks notification button on tenant home page
    Then user see notification reject booking tenant with text "Yah, permintaan booking tidak disetujui"

