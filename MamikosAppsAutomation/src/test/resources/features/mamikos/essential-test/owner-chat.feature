@essentialTest2 @chatPage @ios
Feature: Owner Chat

  @owner-chat-lihatiklan
  Scenario: Owner - Lihat Iklan on Chat Page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    * owner navigates to Chat page
    * owner user tap latest chat
    * user taps on element with text "Lihat Iklan"
    Then user redirected to Detail Kost

  @owner-chat-search
  Scenario: Owner - Search on Chat Page - Chat not found
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    And owner navigates to Chat page
    And user search chat with value "agsfdgaks"
    Then user see message chat is not found

#    Scenario: Tenant - Search on Chat Page - Chat available
    When user search chat with value "Android"
    Then user see available chat "Android Test Edit" on list chat

  @tenant-send-chat
  Scenario: Owner - Send Chat to Owner
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "0890000000359", password "qwerty123" and clear text is "false"
    * user search chat with value "kost automation"
    When tenant user tap latest chat
    * user enter text "What's your phone number?" in chat page
    * user click back button
    Then user see text "What's your phone number?" in chat list page

  @owner-chat-filter
  Scenario: Owner - Filter Chat
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    * owner navigates to Chat page
    Then user see available chat "Android Test Edit" on list chat

    #  Scenario: Owner - Filter on Chat Page (Available)
    * user click filter button on chat menu and select "Semua"
    Then user see available chat "Android Test Edit" on list chat
    * owner user tap latest chat

    #  Scenario: Tenant - Filter on Chat Page (Not Found)
    * user click back button
    * user search chat with value "Android Test Edit"
    * user click filter button on chat menu and select "Belum dibuka"
    Then user see message chat is not found

  @owner-send-chat
  Scenario: Owner - Send Chat to Tenant
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    * owner navigates to Chat page
    * user search chat with value "Android Test Edit"
    * owner user tap latest chat
    Then user should see tenant chat message : "What's your phone number?"
    When user enter text "What's your phone number?" in chat page
    * user click back button
    Then user see text "What's your phone number?" in chat list page

  @owner-scroll-detailchat
  Scenario: Owner - Scroll Chat
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "081362464341", password "1d0lt3stb4ru" and clear text is "true"
    * owner navigates to Chat page

    #Scenario Owner - Scroll List Chat
    Then user scroll up to find chat "Nunu And Willump" and down to chat "Raney Upik Bersatu"
    * user search chat with value "Raney Upik Bersatu"
    * tenant user tap latest chat

    #Scenario Owner - Scroll Detail Chat
    Then user scroll up to find chat "Mamikos menjaga keamanan transaksi Anda. Gunakan chat dan pembayaran di platform Mamikos. Baca selengkapnya" and scroll down on chat form

    @owner-open-broadcast
  Scenario: Owner - Open Broadcast
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    * owner navigates to Chat page
    * owner click broadcast icon on list chat page
    Then user redirected to broadcast page

  @owner-open-updateIklan
  Scenario: Owner - Open Update Iklan
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    * owner navigates to Chat page
    * user search chat with value "Android Test Edit"
    * tenant user tap latest chat
    * user taps on element with text "Update Kamar"
    Then user redirected to update kos page
