@essentialTest2 @dashboardIncome
Feature: Owner Dashboard

  @ownerDashboardKostBBK @BNB-4537
  Scenario: Owner Have BBK kost, BBK FTUE Saya Mengerti Button
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user logs in as Owner "owner essential test" for check bbk ftue
    Then owner can sees BBK FTUE pop-up
    When owner taps on Saya Mengerti button
    Then owner can sees BBK FTUE pop-up is dismissed

  @ownerDashboardKostBBK @BNB-4537
  Scenario: Owner Have BBK kost, BBK FTUE Atur Sekarang Button
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user logs in as Owner "owner essential test" for check bbk ftue
    Then owner can sees BBK FTUE pop-up
    When owner taps on Atur Sekarang button
    Then owner can sees Update Harga kost section

  @ownerDashboardIncome @BNB-4548
  Scenario: Owner Have Pendapatan And Filter Pendapatan
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user logs in as Owner "owner essential test"
    Then owner can sees title "Pendapatan" with subtitle "Catatan lengkap seputar pendapatan Anda selama berbisnis kos bersama Mamikos."
    * owner can sees "Pendapatan Total" and "Pendapatan Bulanan" with their income
    * owner can sees month name
    When owner choose previous month on income month filter options
    Then owner can sees "Pendapatan Total" and "Pendapatan Bulanan" with their income