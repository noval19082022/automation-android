@essentialTest2 @fiturPromosi @ios
Feature: Fitur Promosi Owner

  @menuFiturPromosi
  Scenario: Scroll and Open any sub-section page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    * user is on main page and click on Later option when asked on pop up
    * owner user navigates to manage page
    Then user see menu fitur promosi
      |MamiAds              |
      |Pesan Broadcast      |
      |Promo Iklan          |
      |Cek Properti Sekitar |
    When owner scroll "up" and taps on "MamiAds" menu
    * owner taps button Coba Sekarang
    Then system display page MamiAds
    When user click on back icon
    * owner scroll "down" and taps on "Pesan Broadcast" menu
    Then system display page Pesan Broadcast
    When user click on back icon
    * owner scroll "down" and taps on "Promo Iklan" menu
    Then system display page Promo Iklan
    When user click on back icon
    * owner scroll "down" and taps on "Cek Properti Sekitar" menu
    Then system display page Cek Properti Sekitar
    When user click on back icon
    * owner scroll "down" and taps on "MamiFoto" menu
    Then system display page Pro Photo