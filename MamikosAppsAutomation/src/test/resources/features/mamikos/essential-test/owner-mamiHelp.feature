@essentialTest2 @mamiHelpOwner @ios
Feature: Mami Help Owner

  Scenario: Owner Check field Contact us via WA, CS and Help Center on Mamihelp page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    And user goes to pusat bantuan page
    Then owner can sees menu WA help button, CS help button, and Help Center Button