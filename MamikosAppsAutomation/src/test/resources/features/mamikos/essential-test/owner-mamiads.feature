@essentialTest2 @mamiads @ios
Feature: Buy Mamipoin

  Scenario: Owner Click Beli Saldo and Click Riwayat Saldo Mamiads
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    And user clicks on Mamiads
    And owner taps button Coba Sekarang mamiads
    And user clicks on beli saldo mamiads
    Then system display page beli saldo mamiads with a title "Beli Saldo MamiAds"
    When user click back to page mamiads
    And user clicks on riwayat mamiads
    Then system display page riwayat mamiads with a title "Riwayat Saldo"