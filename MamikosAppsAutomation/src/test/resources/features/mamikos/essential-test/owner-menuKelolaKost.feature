@essentialTest3 @homePage @ios @menuKelolaKostOwner
Feature: Menu Kelola Kost Owner

  Scenario: Scroll and Open any sub-section page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "0890000000361", password "qwerty123" and clear text is "false"
    * owner user navigates to manage page
    * owner taps on Pengajuan Booking
    Then system display page Pengajuan Booking
    When user click back
    * owner taps on Laporan Keuangan
    Then system display page Laporan Keuangan
    When user click back
    * owner taps on Penyewa
    Then system display page Penyewa
    When user click back
    * owner taps on Kelola Tagihan
    Then system display page Kelola Tagihan