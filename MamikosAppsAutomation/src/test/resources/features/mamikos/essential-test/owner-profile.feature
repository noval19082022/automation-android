@essentialTest3 @informasiPribadiPage @ios
Feature: Owner - Informasi Pribadi

  @owner-chat-EditName
  Scenario: Owner - Edit Name
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "owner" go to login form
    And owner login with phone number "085600867992", password "Bismillah@02" and clear text is "false"
    And owner user navigates to profile page
    And user taps on informasi pribadi

#  Scenario: Owner - Edit Name (Invalid)
    * owner change name to "asd"
    * user taps on element with text "Simpan nama"
  #  Then system display message error to change name

#  Scenario: Owner - Edit Name (Valid)
    * owner change name to "Upras Hanif A"
    * user taps on element with text "Simpan nama"
    Then system display message success update data
    And user verify data profile information
      | Upras Hanif A          |
      | 085600867992           |
      | uprashanif.m@gmail.com |

    * owner change name to "Upras Hanif"
    * user taps on element with text "Simpan nama"
    Then system display message success update data
    And user verify data profile information
      | Upras Hanif            |
      | 085600867992           |
      | uprashanif.m@gmail.com |


