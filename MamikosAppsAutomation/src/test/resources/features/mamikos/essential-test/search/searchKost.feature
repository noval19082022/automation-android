@essentialTest1 @searchKostArea @ios
Feature: Search

  Scenario: Main Search - Search Area - Check functionality
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user tap search bar
    And user tap Area
    And user tap "Yogyakarta"
    And user click saya mengerti button on search area
    Then system display filter icon
    And system display favorite icon
    And system display flash sale icon
    And system display discount percentage text

#   Scenario: Check functionality
    When user click icon filter
    Then user verify the elements on filter
    And user verify the wording on filter menu
      | Tipe kos                |
      | Putra                   |
      | Putri                   |
      | Campur                  |
    And Sorting list is appear by :
      | Paling direkomendasikan |
      | Harga Termurah          |
      | Harga Termahal          |
    When user select sorting by "Harga Termahal"
    Then system display "Harga Termahal" checked is "true"
    When user click reset button on filter page
    Then system display "Harga Termahal" checked is "false"

  @autocomplete
  Scenario: Typing on Search box
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Explore page
    And user tap on main search in Homepage
    Then User type with city name "yogyakarta" and should display the autocomplete result area

  @3Char
  Scenario: Typing keyword after 3 characters, user will get 2 type of search result
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Explore page
    And user tap on main search in Homepage
    Then User input with three character "jal"
    And should display the autocomplete result list

  @exception
  Scenario: Type 3 characters in exception
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Explore page
    And user tap on main search in Homepage
    Then User input with three character exception "jln"
    And should NOT display the autocomplete result list

  @randomKeyword
  Scenario: Input with random keyword
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Explore page
    And user tap on main search in Homepage
    Then User input with random character "dsgsderteeda"
    And should display information "Tidak menemukan nama tempat yang sesuai, Ubah kata kunci pencarian"

  @nearbyArea
  Scenario: Check functionality and result of search lokasi saya should be kos nearby
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user tap search bar
    And User click text view my location
    And user click ijinkan akses lokasi
    And user click saya mengerti button on search area
    * owner taps later button on staging
    Then System display boarding house in "Sleman" and Verify the 3 highest lists

  @clearKeyword
  Scenario: Clear Keyword
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user navigates to Explore page
    And user tap on main search in Homepage
    Then User type with city name "bogor" and should display the autocomplete result area
    When user clear the text box
    Then system display search text box is clear

  @fromResultList
  Scenario: Saved keyword from result list
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user navigates to Explore page
    And user tap on main search in Homepage
    And User type with city name "medan" and should display the autocomplete result area
    And user choose one of the list result
    And user click saya mengerti button on search area
    And user on tampilan peta page and click back to Homepage
    And user navigates to Explore page
    And user tap on main search in Homepage
    Then should display saved keyword "Medan" on main search

  @fromList
  Scenario: Saved keyword from list
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user navigates to Explore page
    And user tap on main search in Homepage
    And User type with city name "cibinong" and should display the autocomplete result area
    And user clear the text box
    And user choose "ITB" list university
    And user click saya mengerti button on search area
    And user on tampilan peta page and click back to Homepage
    And user navigates to Explore page
    And user tap on main search in Homepage
    Then should display saved keyword "ITB" on main search