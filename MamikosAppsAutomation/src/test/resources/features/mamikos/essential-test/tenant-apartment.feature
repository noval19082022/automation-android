@essentialTest3 @ios
Feature: Apartment

  @searchFilterApartment
  Scenario: Search and Filter by Type Unit
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment "Jakarta"
    And user click icon filter
    Then system display apartment filter list
      |Tipe Unit    |
      |Perabotan    |
      |Jangka Waktu |
      |Range Harga  |
    And System display option button
      |RESET  |
      |CARI   |

#  @searchFilterByRangeHarga
#  Scenario: Filter by Range Harga
    And user filter apartment by "price range" min "350000" max "3000000"
    Then system display apartment "price range" with min price "350000" max price "3000000" per "bulan" for 5 highest lists

#  @searchFilterByTimePeriod
#  Scenario: Filter by Jangka Waktu filter by time period "Mingguan"
    And user click icon filter
    And user filter apartment by "time period" is "Mingguan"
    Then system display apartment "time period" is "minggu"

#  Scenario: filter by time period "Bulanan"
    When user click icon filter
    And user filter apartment by "time period" is "Bulanan"
    Then system display apartment "time period" is "bulan"

#  Scenario: filter by time period "Tahunan"
    When user click icon filter
    And user filter apartment by "time period" is "Tahunan"
    Then system display apartment "time period" is "tahun"

#  @searchFilterByPerabotan
#  Scenario: search apartment filter by perabotan
    And user click icon filter
    And user filter apartment by "furniture" is "Furnished"
#    Then system display apartment "furniture" is "Furnished"