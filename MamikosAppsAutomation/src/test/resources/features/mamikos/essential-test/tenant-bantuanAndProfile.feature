@essentialTest3 @ios @jumat
Feature: Mami Help and Profile Feature

  @bantuan
  Scenario: Check field Contact us via WA, CS and Help Center on Mamihelp page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user navigates to profile page
    And click on mami help button
    Then owner can sees menu WA help button, CS help button, and Help Center Button

  @profile
  Scenario: Tenant Check Profile
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    And user navigates to profile page
    Then tenant verify display profile name
      | Android Test Edit   |
      | 085275574550        |
    When tenant click on edit profile
    Then tenant verify data profile information
      | Android Test Edit   |
      | Laki - laki         |
      | Karyawan            |
      | mamikos             |

#  @tenant-profile-EditName
#  Scenario: Tenant - Edit Name
#  Scenario: Owner - Edit Name (Invalid)
    When Edit tenant name to "as"
    And user taps on simpan edit profile
    Then system display message "Nama user min 3 karakter"
#  Scenario: Owner - Edit Name (Valid)
    When Edit tenant name to "Android Test Edit"
    And user taps on simpan edit profile
    When user is on main page and click on Later option when asked on pop up
  #  And tenant clicks oke on popup success edit profile
    And tenant click on edit profile
    Then tenant verify data profile information
      | Android Test Edit           |
      | Laki - laki                 |
      | Karyawan                    |
      | mamikos                     |
