@essentialTest3 @homePage @ios @sendChat
Feature: Tenant Chat

  Scenario: Owner - Send Chat to Tenant
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * "owner" go to login form
    * owner login with phone number "0890000000356", password "qwerty123" and clear text is "false"
    * owner navigates to Chat page
    * user search chat with value "kost automation android only Tobelo Utara Halmahera Utara"
    * owner user tap latest chat
    Then user enter text "Send Chat To Tenant" in chat page

  @tenant-chat-filter
  Scenario: Tenant - Filter Chat
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"

    #  Scenario: Tenant - Filter on Chat Page (Available)
    * user click filter button on chat menu and select "Belum dibuka"
    Then user see available chat "kost automation android only Tobelo Utara Halmahera Utara" on list chat
    * tenant user tap latest chat

    #  Scenario: Tenant - Filter on Chat Page (Not Found)
    * user click on back button navigate to chat list
    * user click filter button on chat menu and select "Belum dibuka"
    Then user see message chat is not found

  @tenant-chat-search
  Scenario: Tenant - Search on Chat Page - Chat not found
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"

    #  Scenario: Tenant - Search on Chat Page - Not Found
    * user search chat with value "agsfdgaks"
    Then user see message chat is not found

    #  Scenario: Tenant - Search on Chat Page - Available
    * user click filter button on chat menu and select "Semua"
    When user search chat with value "Kost andalusia"
    Then user see available chat "Kost andalusia spanyol eropa timur" on list chat

  @tenant-chat-ajukanSewa
  Scenario: Tenant - Ajukan Sewa on Chat Page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "0890000000360", password "qwerty123" and clear text is "false"
    * tenant user tap latest chat
    * tenant tap Ajukan Sewa button on chat page
    Then user redirected to Pengajuan Sewa Form

  @tenant-chat-detailKost
  Scenario: Tenant - Lihat Iklan on Chat Page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "0890000000360", password "qwerty123" and clear text is "false"
    * tenant user tap latest chat
    * user taps on element with text "Lihat Iklan"
    Then user redirected to Detail Kost

  @tenant-chat-surveykos
  Scenario: Tenant - Survei kos on Chat Page
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "0890867321216", password "mamikosqa123" and clear text is "false"
    * tenant user tap latest chat
    * user taps on element with text "Survei Kos"
    Then user redirected to survey kos

  @tenant-filter-detailchat
  Scenario: Tenant - Filter Chat
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
    * user search chat with value "Mamikose kos"
    * tenant user tap latest chat
    Then user scroll up to find chat "Mamikos menjaga keamanan transaksi Anda. Gunakan chat dan pembayaran di platform Mamikos." and scroll down on chat form

  @tenant-scroll-detailchat
  Scenario: Tenant - Scroll Chat
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    * user navigates to Chat page
    * user click login as "Tenant"
    * tenant login with phone number "081223344550", password "qwerty123" and clear text is "false"

    #Scenario Tenant - Scroll List Chat
    Then user scroll up to find chat "Rane 78" and down to chat "Kos Laris Kretek"
    * user search chat with value "kos upik rani 62"
    * tenant user tap latest chat

    #Scenario Tenant - Scroll Detail Chat
    Then user scroll up to find chat "Alamat kos di mana?" and down to chat "scroll chat automation"
