@essentialTest3 @tenantHomePage @ios
Feature: Tenant Home Page

  @recommendationKos
  Scenario: Check see all kos redirection and verify list of kos recommendation
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And "tenant" go to login form
    And tenant login with phone number "0890000000360", password "qwerty123" and clear text is "false"
    And user navigates to Explore page
    And user click on location button and select "Semua Kota"
    And Click on see all recommendation Kos linked
    Then verify below list of kose should be shown city "Gasim"

  @checkBanner
  Scenario: Check promo kos display and functionality
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user scroll down to bottom page
    Then user sees the promo widget elements

  @checkTestimonial
  Scenario: Check element testimonial section
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    Then user see the testimonial elements