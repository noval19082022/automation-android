@verificationID
Feature: Tenant Chat

  Scenario: Admin reject identity card
    Given User navigate to "https://jambu.kerupux.com/admin/account/verification-identity-card" and login via Admin credential after click search menu identity card, search user "Android Test Edit"

  Scenario: Tenant - Filter Chat
Given Server configuration is selected
When user is on main page and click on Later option when asked on pop up
* user navigates to Chat page
* user click login as "Tenant"
* tenant login with phone number "085275574550", password "qwerty123" and clear text is "false"
  And user navigates to profile page
  And user tap verification akun
  And user tap kartu identitas
  And user tap take capture photo
  And user tap done
  And user tap kartu identitas with selfie
  And user tap take capture photo
  And user tap done
  And user tap on term and condition checkbox
  And user tap on simpan verification akun
  Then user can sees popup success