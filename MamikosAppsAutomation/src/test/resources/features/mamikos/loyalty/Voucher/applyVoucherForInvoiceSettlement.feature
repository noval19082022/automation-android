@regression @applyVoucherForInvoiceSettlement @loyalty

Feature: Apply Voucher For Invoice Settlement

# Deleting existing booking
  Scenario: Delete booking if tenant have booking active
    Given user navigate to mamipay, search contract "add voucher1" and click on terminate contract

  Scenario: Tenant Booking Kost Type BBK
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "add voucher"
    When user navigates to Explore page
    And user search for Kost with name "add voucher1" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user input boarding start date is "tomorrow" and clicks on booking button
    And user input rent duration equals to 3 and click on continue button
    Then user click next button
    And user selects T&C checkbox and clicks on Book button
    And system display message booking successfully

  Scenario: Owner accept booking
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    Then user navigates to Booking Request
    When user clicks on Accept button
    And user clicks on Next button
    Then user select room number "Kamar 1 Lantai 1" and clicks on next button
    And user enter down payment
    And user click save tenant data

  Scenario: Tenant Pay First Invoice for Booking
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "add voucher"
    And user clicks on profile button
    And user navigates to Booking History page
    Then user click pay now button
    And user select payment method "Bank BNI" for "down payment" case "paymentVoucherkuB1"
    Then system display repayment successfully and back to booking history page

#  Scenario: Tenant Apply Voucher For First Paid In Invoice Settlement
    When user repayment from booking history page which rental period is "3 Bulan"
    Then system display remaining payment "before" use voucher for payment "monthlySettlement"
    When user access voucher form
    And input "VTotalUsage" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Tenant Apply Voucher For Reccuring Rule
    When input "VRecOnly2" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Tenant Apply Voucher For Settlement Rule
    When input "VSettOnly2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Tenant Apply Voucher For First Paid and Reccuring Rule
    When user access voucher form
    And input "VBookingAndRecc2" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Tenant Apply Voucher For First Paid and Settlement Rule
    When input "VBookingAndSett2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Tenant Apply Voucher For Reccuring and Settlement Rule
    When user access voucher form
    And input "VReccAndSett2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Tenant Apply Voucher For First Paid, Reccuring and Settlement Rule
    When user access voucher form
    And input "VBookReccSett2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Tenant Apply Voucher For DP
    When user access voucher form
    And input "VDPOnly2" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Tenant Apply Voucher For First Paid and DP
    When input "VFPAndDP2" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Tenant Apply Voucher For DP and Settlement
    When input "VDPAndSett2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Tenant Apply Voucher For DP and Recurring
    When user access voucher form
    And input "VDPAndRecc2" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Tenant Apply Voucher For First Paid, DP and Settlement
    When input "VFPDPAndSett2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Tenant Apply Voucher For First Paid, DP and Recurring
    When user access voucher form
    And input "VFPDPAndRec2" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Tenant Apply Voucher For DP, Settlement and Recurring
    When input "VDPSettAndRec2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Tenant Apply Voucher For First Paid, DP, Settlement and Recurring
    When user access voucher form
    And input "VAllPRules2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlySettlement"
