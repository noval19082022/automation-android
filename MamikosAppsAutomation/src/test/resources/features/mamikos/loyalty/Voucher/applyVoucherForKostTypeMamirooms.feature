@regression @applyVoucherForMamirooms @loyalty

Feature: Apply Voucher For Kost Type Mamirooms

# Deleting existing booking
  Scenario: Delete booking if tenant have booking active
    Given user navigate to mamipay, search contract "add voucher2" and click on terminate contract

  Scenario: Tenant Booking Kost Type Mamirooms
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "add voucher"
    When user navigates to Explore page
    And user search for Kost with name "add voucher2" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user input boarding start date is "tomorrow" and clicks on booking button
    And user input rent duration equals to 3 and click on continue button
    Then user click next button
    And user selects T&C checkbox and clicks on Book button
    And system display message booking successfully

  Scenario: Owner accept booking
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher mamirooms"
    Then user navigates to Booking Request
    When user clicks on Accept button
    And user clicks on Next button
    Then user select room number "Kamar 1 Lantai 1" and clicks on next button
    And user click save tenant data

  Scenario: Tenant Apply Voucher Applicable for Kost Type Mamirooms
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "add voucher"
    And user clicks on profile button
    And user navigates to Booking History page
    And user click pay now button
    Then system display remaining payment "before" use voucher for payment "monthly"
    When user access voucher form
    And input "VMRBooking2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Applicable for Kost Type BBK
    When user access voucher form
    And input "VBBKBooking2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for Kost Type MAMIROOMS
    When user access voucher form
    And input "VNMRBooking2" to field voucher code
    And click use button
    Then user see voucher alert message "Kode voucher tidak bisa digunakan."
