@regression @applyVoucherMinContractDuration @loyalty

Feature: Apply Voucher For Invoice Meet Minimal Contract Duration

 #Deleting existing booking
  Scenario: Delete booking if tenant have booking active
    Given user navigate to mamipay, search contract "add voucher1" and click on terminate contract

  Scenario: Tenant Booking Kost BBK with 3 Monthly Contract
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "add voucher"
    When user navigates to Explore page
    And user search for Kost with name "add voucher1" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user input boarding start date is "tomorrow" and clicks on booking button
    And user select payment period "Per 3 Bulan" and click on continue button
    And user click next button
    And user selects T&C checkbox and clicks on Book button
    Then system display message booking successfully

  Scenario: Owner accept booking
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    Then user navigates to Booking Request
    When user clicks on Accept button
    And user clicks on Next button
    Then user select room number "Kamar 1 Lantai 1" and clicks on next button
    And user click save tenant data

  Scenario: Tenant Apply Voucher Meet Min. Contract Duration
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "add voucher"
    And user clicks on profile button
    And user navigates to Booking History page
    And user click pay now button
    Then system display remaining payment "before" use voucher for payment "3 monthly"
    When user access voucher form
    And input "VDurBooking2" to field voucher code
    And click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "3 monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "3 monthly"

