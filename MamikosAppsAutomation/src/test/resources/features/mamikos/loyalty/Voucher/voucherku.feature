@regression @voucherku @loyalty
Feature: Voucherku

  @voucherAvailableDetailsUnlimitedPeriod
  Scenario: Voucher available details with unlimited period
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "voucher"
    And user clicks on profile button
    And user clicks on voucherku
    When user click on user voucher
    Then user verify detail "Tersedia" voucher page
      | Detail Voucher       |
      | TersediaAuto2        |
      | Berlaku kapan saja   |
      | TersediaAuto2        |

  @voucherAvailableDetailsLimitedPeriod
  Scenario: Voucher available details with limited period
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "add voucher"
    And user clicks on profile button
    And user clicks on voucherku
    When user click on user voucher
    Then user verify detail "Tersedia" voucher page
      | Detail Voucher              |
      | VNotMeetMinForBooking       |
      | Berlaku hingga 24 Jul 2049  |
      | VNotMeetMin1                |

  @voucherUsedandExpiredDetails
  Scenario: Voucher used and expired details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "voucher"
    And user clicks on profile button
    And user clicks on voucherku
    And user verify on Terpakai tab
    When user click on user voucher
    Then user verify detail "Terpakai" voucher page
      | Detail Voucher  |
      | TerpakaiAuto3   |
      | Terpakai        |
      | TerpakaiAuto3   |
    And user click back
    When user verify on Kadaluwarsa tab
    And user click on user voucher
    Then user verify detail "Kedaluwarsa" voucher page
      | Detail Voucher |
      | Kedaluwarsa2   |
      | Kedaluwarsa    |
      | Kedaluwarsa2   |

  @countReminderVoucher
  Scenario: My Voucher - Red dot displayed beside voucherku menu
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "voucher"
    And user clicks on profile button
    Then user verify red dot number

  @voucherLists
  Scenario: My Voucher - Voucher Lists
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "voucher"
    And user clicks on profile button
    And user clicks on voucherku
    Then user verify voucher lists
    And user verify on Terpakai tab
    And user verify voucher lists
    And user verify on Kadaluwarsa tab
    And user verify voucher lists

  @voucherPageDisplay
  Scenario: My Voucher - Voucher Displayed
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "voucher"
    And user clicks on profile button
    And user clicks on voucherku
    Then user verify voucher page
      | Voucherku           |
      | Promo Lainnya       |
      | Berlaku kapan saja |
      | Kode Voucher        |
      | TersediaAuto2      |
    And user verify on Terpakai tab
    Then user verify used voucher displayed
      | Voucherku           |
      | Promo Lainnya       |
      | Terpakai            |
      | Kode Voucher        |
      | TerpakaiAuto3       |
    And user verify on Kadaluwarsa tab
    Then user verify expired voucher displayed
      | Voucherku           |
      | Promo Lainnya       |
      | Kedaluwarsa         |
      | Kode Voucher        |
      | Kedaluwarsa2        |

  @emptyVoucher @iOS
  Scenario: Checking empty state voucher and promo page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "non voucher"
    And user clicks on profile button
    And user clicks on voucherku
    And user verify on Tersedia tab
    Then user verify Tersedia empty state
    And user verify on Terpakai tab
    Then user verify Terpakai empty state
    And user verify on Kadaluwarsa tab
    Then user verify Kedaluwarsa empty state
    And user clicks on Promo Lainnya
    Then user verify on Promo page "‎promo.mamikos.com"

  @voucherTargetedDisplay
  # Deleting existing booking
  Scenario: Delete booking if tenant have booking active
    Given user navigate to mamipay, search contract "add voucher1" and click on terminate contract

  Scenario: Tenant Booking Kost Type BBK
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "voucher targeted"
    When user navigates to Explore page
    And user search for Kost with name "add voucher1" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user input boarding start date is "today" and clicks on booking button
    And user input rent duration equals to 1 and click on continue button
    Then user click next button
    And user selects T&C checkbox and clicks on Book button
    And system display message booking successfully

  Scenario: Owner accept booking
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    Then user navigates to Booking Request
    When user clicks on Accept button
    And user clicks on Next button
    Then user select room number "Kamar 1 Lantai 1" and clicks on next button
    And user click save tenant data

  Scenario: Tenant Pay First Invoice for Booking
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user log in as Tenant via phone number as "voucher targeted"
    And user clicks on profile button
    And user navigates to Booking History page
    Then user click pay now button
    And user select payment method "Bank BNI" for "payment" case "paymentVoucherkuB2"
    Then system display repayment successfully and back to booking history page

    # Scenario: Tenant Check In Kost
    When user check in from booking history page which rental period is "2 Bulan"
    Then system display check in successfully

  Scenario: Edit Tenant profession to Karyawan
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "voucher targeted"
    And user navigates to profile page
    And tenant click on edit profile

    # Scenario: Edit Tenant profession to Karyawan
    When Select tenant profession to Karyawan
    And Edit tenant profession name to "PT. Mama Teknologi Property"

    # Scenario: My Voucher - Voucher targeted to tenant with profession karyawan displayed
    When user clicks on voucherku
    Then user verify voucher is displayed
      # Verify Mass Voucher targeted for Kos City is displayed
      | MassKostCity  |
      # Verify Mass Voucher targeted for Kos Name is displayed
      | MassKostName  |
      # Verify Mass Voucher targeted for Profession Karyawan & Companies is displayed
      | MassCompanies |
      # Verify Mass Voucher targeted for Email Domain is displayed
      | MassDomain    |
      # Verify Mass & Single Voucher targeted for Email is displayed
      | MassEmail     |
      | SingEmail1    |

    # Scenario: My Voucher - Voucher targeted to tenant with profession karyawan not displayed
    Then user verify voucher is not displayed
      # Verify Mass Voucher targeted for Other Kos City is not displayed
      | MassKostCity2  |
      # Verify Mass Voucher targeted for Other Kos Name is not displayed
      | MassKostName2  |
      # Verify Mass Voucher targeted for Other Companies is not displayed
      | MassUniv       |
      # Verify Mass Voucher not targeted for Profession Mahasiswa is not displayed
      | MassCompanies2 |
      # Verify Mass Voucher targeted for Other Email Domain is not displayed
      | MassDomain2    |
      # Verify Mass & Single Voucher targeted for Other Email is not displayed
      | MassEmail2     |
      | SingEmail2     |

    # Scenario: Edit Tenant profession to Mahasiswa
    When user click on back arrow on voucher list
    And tenant click on edit profile
    And Select tenant profession to Mahasiswa
    And Edit tenant profession name to "Universitas Mamikos"
    And user clicks on voucherku

    # Scenario: My Voucher - Voucher targeted to tenant with profession Mahasiswa displayed
    Then user verify voucher is displayed
      # Verify Mass Voucher targeted for Profession Mahasiswa & Universities is displayed
      | MassUniv |

    # Scenario: My Voucher - Voucher targeted to tenant with profession Mahasiswa not displayed
    Then user verify voucher is not displayed
      # Verify Mass Voucher targeted for Other Universities is not displayed
      | MassUniv2     |
      # Verify Mass Voucher not targeted for Profession Karyawan is not displayed
      | MassCompanies |