@regression @detailRedemption @loyalty @iOS

Feature: Detail Redemption

  @contactUs
  Scenario: Verify Contact Us
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "mamipoin"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Reward History
    And user clicks on "all" Filter
    When user clicks on reward "PLN ADR testing"
    Then user verify Contact Us section displayed

  @faq
  Scenario: Verify FAQ
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "mamipoin"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Reward History
    And user clicks on "all" Filter
    When user clicks on reward "PLN ADR testing"
    Then user verify FAQ title
    And user verify question
      | Setelah saya menukarkan poin untuk hadiah, apa yang harus saya lakukan?             |
      | Kapan hadiah akan saya terima?                                                      |
      | Apa yang harus saya lakukan jika proses sudah berhasil namun hadiah belum diterima? |
      | Apakah penukaran bisa dibatalkan?                                                   |
    When user expand "first" question
    Then user verify answer displayed
    When user expand "second" question
    Then user verify answer displayed
    When user collapse "second" question
    Then user verify question collapsed

  @tnc
  Scenario: Verify Term and Condition
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "mamipoin"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    When user clicks on Term and Condition
    Then user verify back button displayed
    And user verify Term and Condition header
    And user verify Term and Condition list
    And user verify Point Scheme section
    When user click on "current window" link
    Then user verify on mamikos page "mamikos.com"

