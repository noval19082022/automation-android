@regression @mamipoinOwnerLandingPage @loyalty @iOS

Feature: MamiPoin

  @inactiveMamiPoin
  Scenario: Verify Inactive MamiPoin
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user logs in as Owner "add voucher mamirooms"
    Then user verify MamiPoin entry point not displayed

  @activeMamiPoin
  Scenario: Verify Active MamiPoin
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user clicks on Login option on Menu
    When user clicks on Login as Owner
    And user enter credential "0895359416345" and "qwerty12345" and click on Login Button
    Then user verify MamiPoin entry point displayed

  @mamiPoinLandingPageDisplay
  Scenario: Verify MamiPoin Landing Page Display
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "mamipoin"
    And user clicks on MamiPoin Owner entry point
    Then user verify mamipoin onboarding
    And user verify mamipoin displayed
    And user verify Reward History Button
    And user verify Point History Button
    And user verify Term and Condition Button
    And user verify Redeem Point Button
