@regression @rejectedRedeemReward @loyalty @iOS

Feature: Rejected Redeem Reward

  Scenario: Redeem Reward
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    When user redeem reward "PLN ADR testing"
    And user verify redeem confirmation pop up and click "Ya, Tukar"
    Then user redirect to "Detail Status" page
    And user verify reward "processing" date
    And user verify redemption id displayed

#  Scenario: Verify On Process Reward
    Given user back to Mamipoin Landing Page from detail status
    When user clicks on Reward History
    Then user verify reward "PLN ADR testing" status changed to "Sedang Diproses"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "processing" date
    And user verify redemption id displayed
    When user clicks on back button
    And user clicks on "on process" Filter
    Then user verify reward "PLN ADR testing" status changed to "Sedang Diproses"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "processing" date
    And user verify redemption id displayed

  Scenario: Reject Reward Redemption
    Given user navigate to mamikos admin, search reward "PLN ADR testing" and change status to "Rejected"

  Scenario: Verify Rejected Reward
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    When user clicks on Reward History
#   Verify status on All filter tab
    Then user verify reward "PLN ADR testing" status changed to "Penukaran Gagal"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "rejected" date
    And user verify redemption id displayed
    And user verify "rejected" redemption notes
    And user verify status "Gagal" on redemption detail
#   Verify status on Rejected filter tab
    When user clicks on back button
    And user clicks on "rejected" Filter
    Then user verify reward "PLN ADR testing" status changed to "Penukaran Gagal"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "rejected" date
    And user verify redemption id displayed
    And user verify "rejected" redemption notes
    And user verify status "Gagal" on redemption detail

  @redeemRewardForNotEligibleUser
  Scenario: Redeem Reward For Not Eligible User
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    When user clicks on reward "Pulsa API testing GP2"
    Then user verify Tukar Poin Button is disable

  @verifyRedemptionForm
  Scenario: Verify Redemption Form
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    #Verify Reward Type Goods with Shipping
    When user clicks on reward "PLN ADR testing"
    And user clicks on Tukar Poin Button on detail reward
    Then user verify field "Nama" autofilled with "Amanda"
    And user verify field "No HP yang bisa dihubungi" autofilled with "0895359416718"
    And user verify field "Alamat Pengiriman Hadiah" displayed
    And user verify field "Catatan" displayed
    #Verify Reward Type PLN
    When user clicks on back button
    And user clicks on back button
    And user clicks on reward "PLN API test"
    And user clicks on Tukar Poin Button on detail reward
    Then user verify field "Nama" autofilled with "Amanda"
    And user verify field "No HP yang bisa dihubungi" autofilled with "0895359416718"
    And user verify field "No Meter Listrik Anda" displayed
    #Verify Reward Type Pulsa
    When user clicks on back button
    And user clicks on back button
    And user clicks on reward "Pulsa API testing GP1"
    And user clicks on Tukar Poin Button on detail reward
    Then user verify field "Nama" autofilled with "Amanda"
    And user verify field "No HP yang bisa dihubungi" autofilled with "0895359416718"
    And user verify field "No HP Penerima Voucher Pulsa" displayed
    And user verify checkbox "No HP penerima sama dengan Pemilik Akun" displayed
    #Verify Reward Type Other
    When user clicks on back button
    And user clicks on back button
    And user clicks on reward "Testing API New GP1"
    And user clicks on Tukar Poin Button on detail reward
    Then user verify redeem confirmation pop up and click "Batal"

