@regression @reward @loyalty @iOS

Feature: Reward

  @rewardListOnboarding
  Scenario: Reward List Onboarding
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    When user clicks on MamiPoin Owner entry point
    Then user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    Then user verify reward list onboarding
    When user clicks on back button
    Then user verify mamipoin onboarding not displayed
    And user clicks on Tukar Poin Button on mamipoin landing page
    Then user verify reward list onboarding not displayed
    When user clicks on Help Button
    Then user verify reward list onboarding

  @rewardList
  Scenario: Reward List Display
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "mamipoin"
    When user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    Then user verify back button displayed
    And user verify owner mamipoint displayed
    And user verify Help button displayed
    And user verify targetted reward list displayed

  @rewardScrolling
  Scenario: Reward Scrolling Behaviour
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    When user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    And user scroll down reward list
    Then user verify MamiPoin header is floating

  @rewardSorting
  Scenario: Reward Sorting
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "mamipoin"
    When user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    Then user verify sorted reward based on point ascending

  @emptyRewardQuota
  Scenario: Reward Total Quota, Total Each User Quota, Expired, and Not Started Yet is not Displayed
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    When user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    Then user verify "empty total quota" reward is not displayed
    And user verify "empty total each user quota" reward is not displayed
    And user verify "expired" reward is not displayed
    And user verify "not started yet" reward is not displayed

  @emptyDailyEachUserQuota
  Scenario: Reward Daily Each User Quota is Empty
#    Redeem Reward
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    When user redeem reward "Sugar Daily"
    Then user verify redeem confirmation pop up and click "Ya, Tukar"
    And user redirect to "Detail Status" page
#    Verify Reward
    When user back to Mamipoin Landing Page from detail status
    And user clicks on Tukar Poin Button on mamipoin landing page
    Then user verify "empty daily each user quota" reward is not displayed

  @emptyDailyQuota
  Scenario: Reward Daily Quota is Empty
#    Redeem Reward
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    When user redeem reward "Margarin Daily"
    Then user verify redeem confirmation pop up and click "Ya, Tukar"
    And user redirect to "Detail Status" page
#    Verify Reward
    When user back to Mamipoin Landing Page from detail status
    And user clicks on Tukar Poin Button on mamipoin landing page
    Then user verify "empty daily quota" reward is not displayed

  @detailRewardWhenDontHaveEnoughPoint
  Scenario: Detail Reward When Don't Have Enough Point
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "empty reward history"
    When user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    And user clicks on reward "automate lack point"
    Then user redirect to detail reward
    And user verify Tukar Poin button is not displayed
    And user verify warning Poin Anda tidak cukup is displayed