@regression @rewardHistory @loyalty @iOS

Feature: Reward History

  @rewardHistory
  Scenario: Reward History
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    When user clicks on Reward History
    Then user verify Reward History header
    And user verify Reward History filter
    And user verify list of reward redemption history
    When user clicks on see status
    Then user redirect to "Detail Status" page

  @rewardHistoryFilter
  Scenario: Verify Reward History Filter
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "mamipoin"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Reward History
    When user clicks on "all" Filter
    Then user see "all" redeemed reward
    When user clicks on "on process" Filter
    Then user see "on process" redeemed reward
    When user clicks on "success" Filter
    Then user see "success" redeemed reward
    When user clicks on "rejected" Filter
    Then user see "rejected" redeemed reward

  @rewardHistoryEmpty
  Scenario: Empty Reward History
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "empty reward history"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    When user clicks on Reward History
    Then user verify empty "all" redemption reward
    When user clicks on "on process" Filter
    Then user verify empty "on process" redemption reward
    When user clicks on "success" Filter
    Then user verify empty "success" redemption reward
    When user clicks on "rejected" Filter
    Then user verify empty "rejected" redemption reward




