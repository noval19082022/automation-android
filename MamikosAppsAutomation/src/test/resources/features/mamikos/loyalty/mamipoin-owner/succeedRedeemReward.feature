@regression @succeedRedeemReward @loyalty @iOS

Feature: Succeed Redeem Reward

  Scenario: Redeem Reward
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "add voucher"
    And user clicks on MamiPoin Owner entry point
    And user verify mamipoin onboarding
    And user clicks on Tukar Poin Button on mamipoin landing page
    And user verify reward list onboarding
    When user redeem reward "PLN ADR testing"
    And user verify redeem confirmation pop up and click "Ya, Tukar"
    Then user redirect to "Detail Status" page
    And user verify reward "processing" date
    And user verify redemption id displayed

#   Verify On Process Reward
    Given user back to Mamipoin Landing Page from detail status
    When user clicks on Reward History
    Then user verify reward "PLN ADR testing" status changed to "Sedang Diproses"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "processing" date
    And user verify redemption id displayed
    When user clicks on back button
    And user clicks on "on process" Filter
    Then user verify reward "PLN ADR testing" status changed to "Sedang Diproses"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "processing" date
    And user verify redemption id displayed

#   Succeed Reward Redemption
    Given user navigate to mamikos admin, search reward "PLN ADR testing" and change status to "Succeed"

#   Verify Succeed Reward
    When user back to Mamipoin Landing Page from detail status
    And user clicks on Reward History
#   Verify status on All filter tab
    Then user verify reward "PLN ADR testing" status changed to "Berhasil Diterima"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "succeed" date
    And user verify redemption id displayed
    And user verify "succeed" redemption notes
    And user verify status "Berhasil" on redemption detail
#   Verify status on Succeed filter tab
    When user clicks on back button
    And user clicks on "success" Filter
    Then user verify reward "PLN ADR testing" status changed to "Berhasil Diterima"
    When user clicks on reward "PLN ADR testing"
    Then user verify reward "succeed" date
    And user verify redemption id displayed
    And user verify "succeed" redemption notes
    And user verify status "Berhasil" on redemption detail
#   Verify point deducted
    When user back to Mamipoin Landing Page from detail status
    Then user verify point deducted
