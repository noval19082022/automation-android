@regression @mamipoinTenantInformasiPoin @loyalty

Feature: MamiPoin Tenant Informasi Poin

  @emptyInformasiPoin
  Scenario: Empty Informasi Poin when the user has no poin history
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "Empty MamiPoin Tenant"
    When user clicks on profile button
    And user clicks on MamiPoin Tenant entry point button
    And user clicks on informasi poin button
    Then user verify tidak ada poin yang tersedia is displayed
    And user verify lihat caranya button is displayed

  @detailInformasiPoin
  Scenario: Detail Informasi Poin when the user has poin history
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "MamiPoin Tenant"
    When user clicks on profile button
    And user clicks on MamiPoin Tenant entry point button
    And user clicks on informasi poin button
    Then user verify Poin Saya is displayed
    And user verify description on informasi poin tenant is displayed
    And user verify table title on informasi poin tenant is displayed
    And user verify expired date on informasi poin tenant is displayed
    And user verify expired poin on informasi poin tenant is displayed
