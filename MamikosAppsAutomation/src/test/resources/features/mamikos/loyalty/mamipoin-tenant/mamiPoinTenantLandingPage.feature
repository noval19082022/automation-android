@regression @mamipoinTenantLandingPage @loyalty

Feature: MamiPoin Tenant Landing Page

  @informasiPoinMenuRedirection
  Scenario: Informasi Poin Menu Redirection
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "MamiPoin Tenant"
    When user clicks on profile button
    And user clicks on MamiPoin Tenant entry point button
    And user clicks on informasi poin button
    Then user verify Poin Saya is displayed

  @riwayatPoinMenuRedirection
  Scenario: Riwayat Poin Menu Redirection
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "MamiPoin Tenant"
    When user clicks on profile button
    And user clicks on MamiPoin Tenant entry point button
    And user clicks on riwayat poin button
    Then user verify Riwayat Poin is displayed

  @dapatkanPoinMenuRedirection
  Scenario: Dapatkan Poin Menu Redirection
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "MamiPoin Tenant"
    When user clicks on profile button
    And user clicks on MamiPoin Tenant entry point button
    And user clicks on dapatkan poin button
    Then user verify Dapatkan Poin Section is displayed