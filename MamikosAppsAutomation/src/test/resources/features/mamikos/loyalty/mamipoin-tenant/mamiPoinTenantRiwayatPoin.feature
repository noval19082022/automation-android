@regression @mamipoinTenantRiwayatPoin @loyalty

Feature: MamiPoin Tenant Riwayat Poin

  @emptyRiwayatPoin
  Scenario: Empty Riwayat Poin when the user has no poin history
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "Empty MamiPoin Tenant"
    When user clicks on profile button
    And user clicks on MamiPoin Tenant entry point button
    And user clicks on riwayat poin button
    Then user verify Riwayat Poin is not displayed
    And user verify the image of empty Riwayat Poin is displayed