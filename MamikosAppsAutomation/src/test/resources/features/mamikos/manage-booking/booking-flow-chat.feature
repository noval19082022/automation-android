@regression @manageBooking @booking @occupancyAndBilling
Feature: Booking flow from chat
  #Deleting existing booking
  Background:
    Given User navigate to "http://padang2.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "Kose Mamiset Automation" and click on Cancel the Contract Button from action column

    @iNeedFastBookingChatFlow
    Scenario: Check booking button while "Saya butuh cepat nih. Bisa booking sekarang?" selected
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "master"
      When user navigates to Explore page
      And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
      And user tap chat button in kos detail
      Then tenant can sees chat " Saya butuh cepat nih. Bisa booking sekarang?" is selected
      And tenant can sees send button is Booking
      When user tap Send
      Then tenant should reach booking form section
      And user fill out date as tomorrow date
      And user fills out rent duration equals to 4 Bulan and clicks on Continue button
      Then booking confirmation screen displayed with  Kost name, Room price, Tenant Name, Gender, Phone for "booking" and click on next button
      And user selects T&C checkbox and clicks on Book button
      And user navigates to main page after booking
      And user navigates to profile page
      And user logs out as a Tenant user
      And user logs in as Owner "master"
      And user navigates to Booking page
      Then booking is displayed with Name, Status, Room name, Checkin Date as tomorrow date, Duration, Booking text for "booking" on Booking listing page
      When user clicks on Booking Details button
      Then booking is displayed with Booking ID starting with "MAMI" , Room name, Room price, Order Date as today date, Booking Date as today date , Duration, Exit Date as today date ,  Name, Phone, Status on Booking details page
      When user clicks on Accept button
      Then user sees a confirmation message with correct text
      When user clicks on Next button
      And owner choose first available room and choose next button
      And user enters deposite panalty info as deposite fee "100" , panalty fee "200" , panalty fee duration "1" , panalty fee duration type "Bulan" and click on Save
      And owner user navigates to profile page
      And user logs out as a Tenant user
      And user logs in as Tenant via Facebook credentails "master"
      And user navigates to profile page
      #Then user sees "1 Permintaan Diterima" text beside History Booking option
      When user navigates to Booking History page
      Then booking is displayed with Status, Room name, Checkin Date as tomorrow date, Duration, Payment expiry as tomorrow date for "booking" on Booking listing page
