@regression @manageBooking @booking @occupancyAndBilling
  Feature: Booking Summary

    #Deleting existing booking
    Background:
      Given User navigate to "http://padang2.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "Kose Mamiset Automation" and click on Cancel the Contract Button from action column

    @checkContentBookingDetails
    Scenario: Check content booking details
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "master"
      When user navigates to Explore page
      And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      And user fill out date as tomorrow date
      And user fills out rent duration equals to 4 Bulan and clicks on Continue button
      Then booking confirmation screen displayed with  Kost name, Room price, Tenant Name, Gender, Phone for "booking" and click on next button
      And tenant can sees booking summary displayed with kost name, kost gender, room price, check in date, check out date, rent duration