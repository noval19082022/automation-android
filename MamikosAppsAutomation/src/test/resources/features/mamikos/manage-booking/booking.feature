@regression @booking @manageBooking @occupancyAndBilling
Feature: Booking

  #Deleting existing booking
  Background:
    Given User navigate to "http://padang2.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "Kose Mamiset Automation" and click on Cancel the Contract Button from action column

  @test1
  Scenario: Booking a Kost as a Tenant , approved by owner successfully
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user fill out date as tomorrow date
    And user fills out rent duration equals to 4 Bulan and clicks on Continue button
    Then booking confirmation screen displayed with  Kost name, Room price, Tenant Name, Gender, Phone for "booking" and click on next button
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to profile page
    And user logs out as a Tenant user
    And user logs in as Owner "master"
    And user navigates to Booking page
    Then booking is displayed with Name, Status, Room name, Checkin Date as tomorrow date, Duration, Booking text for "booking" on Booking listing page
    When user clicks on Booking Details button
    Then booking is displayed with Booking ID starting with "MAMI" , Room name, Room price, Order Date as today date, Booking Date as today date , Duration, Exit Date as today date ,  Name, Phone, Status on Booking details page
    When user clicks on Accept button
    Then user sees a confirmation message with correct text
    When user clicks on Next button
    And owner choose first available room and choose next button
    And user enters deposite panalty info as deposite fee "100" , panalty fee "200" , panalty fee duration "1" , panalty fee duration type "Bulan" and click on Save
    And owner user navigates to profile page
    And user logs out as a Tenant user
    And user logs in as Tenant via Facebook credentails "master"
    And user navigates to profile page
    #Then user sees "1 Permintaan Diterima" text beside History Booking option
    When user navigates to Booking History page
    Then booking is displayed with Status, Room name, Checkin Date as tomorrow date, Duration, Payment expiry as tomorrow date for "booking" on Booking listing page

  @bookingNavigation
  Scenario: Go back from booking page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user tap back button in Booking form page
    Then user redirect to Kost Details page

  @bookingFormPrefilled
  Scenario: Data prefilled in Booking page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user fill out date as tomorrow date
    And user fills out rent duration equals to 4 Bulan and clicks on Continue button
    Then user see full name and phone number

  @bookingFormValidation
  Scenario: Field validation in booking form page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user tap save without filling the gender
    Then user see validation message "Jenis Kelamin harus sama dengan Tipe Kost"
    When user tap save without filling full name
    Then user see toast message "Nama tidak boleh kosong"
    When user fill phone number with invalid number, example : "0110190" then tap save
    Then user see toast message "Nomor handphone tidak valid"

  @bookingPrice
  Scenario: Check popup action and validation
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    Then user click on price of details page and price details popup should be shown

  @priceDetailsCheck
  Scenario: Check contain of price popup
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    Then user click on price of details page and validate price details, minimum payment, electricity cost, rental price heading should be shown

  @dismisspopup
  Scenario: Close price popup from kost details page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user click on price of details page and price details popup should be shown
    Then click out side of popup and popup should be dismiss

  @closePricePopup
  Scenario: Close price popup by click on see all price link text
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user click on price of details page and price details popup should be shown
    Then user click on see all price link text and price popup view should be dismiss

  @dailyPriceInfo
  Scenario: Daily price validation in price popup
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user click on price of details page and price details popup should be shown
    And Per Day price option should be shown in price popup and click on info button
    Then tooltip should be shown


  @bookingButtonDisable
  Scenario: Booking button disable for Full Boarding House
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "bookingDisable" and selects matching result and navigate to Kost details page
    Then booking button should be disable on Kost details page

  @koseputri
  Scenario: Booking for Kost Putri with gender is "Men"
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "femaleData" and selects second matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    Then validation message "male kose" should be shown

  @koseputra
  Scenario: Booking for Kost Putra with gender is Woman
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "gender female"
    When user navigates to Explore page
    And user search for Kost with name "maleData" and selects second matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    Then validation message "female kose" should be shown

  @campur
  Scenario: Booking for Kost Campur with gender Men
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    Then user should navigate to booking details page

  @rentCountCheck
  Scenario: Check rent price and verify daily price is not shown
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    Then verify selected rent type per bulan shown kost own price and also verify daily price option Per Hari should not shown

  @checkRentValue
  Scenario: Choose rent type and verify rent count updated according to rent type
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And verify selected rent type information "month" should be shown
    Then verify rent count value is shown "month" according to selected rent type

  @updateRentType
  Scenario: Update rent type and verify rent duration update
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user click on "week" rent type
    When verify selected rent type information "week" should be shown
    Then  verify rent count value is shown "week" according to selected rent type

  @rentDurationValue
  Scenario: Check default rent duration value
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    When user clicks on Booking button on Kost details page
    Then verify rent count value is shown "month" according to selected rent type
    And user click on "week" rent type
    Then  verify rent count value is shown "week" according to selected rent type
    And user click on "3 month" rent type
    Then verify rent count value is shown "3 month" according to selected rent type
    And user click on "6 month" rent type
    Then verify rent count value is shown "6 month" according to selected rent type
    And user click on "year" rent type
    Then verify rent count value is shown "year" according to selected rent type

  @maxWeeklyRentValue
  Scenario: Select Weekly rent option and check maximum duration value
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user click on "week" rent type
    And user click on increase duration button maximum time
    Then verify rent duration value is shown maximum count of selected rent type "week"

  @maxMontlyRentValue
  Scenario: Select Monthly rent option and check maximum duration value
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user click on increase duration button maximum time
    Then verify rent duration value is shown maximum count of selected rent type "month"

  @checkIncreaseDuration
  Scenario: Click on increase rent duration button and verify rent duration value
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user increase rent duration
    Then verify rent count value is shown "increaseCount" according to selected rent type

  @increaseButtonDisable
  Scenario: Check increase rent duration button is disabled after reached maximum count
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user click on increase duration button maximum time
    Then verify increase button should be disabled

    @decreaseDurationButtonDisable
  Scenario: Check decrease rent duration button is disabled after decreasing rent count
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user increase rent duration
    And user decrease rent duration
    Then verify decrease button should be disabled

  @checkUpdatedRentDuration
  Scenario: Update rent duration and verify that duration on preview page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user fill out date as tomorrow date
    And user fills out rent duration equals to 4 Bulan and clicks on Continue button
    Then click on next button to navigate preview and verify rent duration