@regression @occupancyAndBilling @manageBooking @occupancyAndBilling

  Feature: OB Booking Active Booking

    #Deleting existing booking
    Background:
      Given User navigate to "http://padang2.kerupux.com/pin2blkang" and login via Admin credential after that Click on search contract menu from left bar, search for "Kose Mamiset Automation" and click on Cancel the Contract Button from action column

    @bookingWithNeedConfirmationStatus
    Scenario: Booking a Kost as a Tenant , approved by owner successfully
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "master"
      When user navigates to Explore page
      And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      And user fill out date as tomorrow date
      And user fills out rent duration equals to 4 Bulan and clicks on Continue button
      Then booking confirmation screen displayed with  Kost name, Room price, Tenant Name, Gender, Phone for "booking" and click on next button
      And user selects T&C checkbox and clicks on Book button
      And user navigates to main page after booking
      And user navigates to Booking Tenant page
      Then user can sees first list is in waiting for confirmation stat
      When user navigates to Explore page
      And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      Then user can see booking restriction pop-up
      When user dismiss booking restriction pop-up
      And user back to Home page from kost details
      And user navigates to Explore page
      And user navigates to Booking Tenant page
      And user tap on first booking list
      And user tap on cancel booking button
      Then user should sees cancel booking section
      When user tap yes cancel booking button
      Then user can sees success cancel button pop-up