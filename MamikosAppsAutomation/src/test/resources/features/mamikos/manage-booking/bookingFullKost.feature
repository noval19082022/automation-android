@regression @booking @manageBooking @occupancyAndBilling

Feature: OB Booking Full Kost

  @bookingFullKostDetails
  Scenario: Booking Full Kost From Kost Details
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    When user navigates to Explore page
    And user search for Kost with name "kostfull" and selects matching result and navigate to Kost details page
    And user navigates trough FTUE pop up
    Then user can see Ajukan Sewa button is disabled
