@regression @booking @manageBooking @occupancyAndBilling

Feature: OB Booking Mix Kost

  @clickBookingOnMixKostAsFemale @MB-2614
  Scenario: Click Booking On Mix Kost As Female
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "gender female"
    When user navigates to Explore page
    And user search for Kost with name "mix kost pasutri" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user fill out date as tomorrow date
    And user fills out rent duration equals to 4 Bulan and clicks on Continue button
    Then user can sees female and male button are changeable
    And user can sees "kost upras sembilanlapan, Halmahera Utara Halmahera bisa di tempati Pasangan Suami Istri" as alert message
    And user can sees renter amount options
    When user "increase" renter amount equal to 2
    And user can sees pasangan suami istri checkbox

  @blankAndDuplicatePhoneNumberMixKost @mb-2627
  Scenario: Blank Phone Number On Booking Form Mix Kost
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "gender female"
    When user navigates to Explore page
    And user search for Kost with name "mix kost pasutri" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user fill out date as tomorrow date
    And user fills out rent duration equals to 4 Bulan and clicks on Continue button
    And user deletes phone number
    And user can sees "Nomor handphone tidak valid" as empty phone number invalid text
    When user input phone number with "087764546721"
    And user tap on Save & Continue button
    And user selects T&C checkbox and clicks on Book button
    Then user should still in booking confirmation page

  @blankAndDuplicatePhoneNumberMixKostEdit @mb-2628
  Scenario: Blank Phone Number On Booking Form Mix Kost Edit
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "gender female"
    When user navigates to Explore page
    And user search for Kost with name "mix kost pasutri" and selects matching result and navigate to Kost details page
    And user clicks on Booking button on Kost details page
    And user fill out date as tomorrow date
    And user fills out rent duration equals to 4 Bulan and clicks on Continue button
    And user tap on Save & Continue button
    And user edits booking form
    And user deletes phone number edit
    And user can sees "Nomor handphone tidak valid" as empty phone number invalid text edit
    When user input phone number with "087764546721" edit
    And user tap on save button
    And user selects T&C checkbox and clicks on Book button
    #Then user see toast message "Nomor sudah digunakan"
    Then user should still in booking confirmation page

    @incorrectPhoneNumberFormatAndAlphabetInput @MB-2623
    Scenario: Incorrect Phone Number And Alphabet Input
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "gender female"
      When user navigates to Explore page
      And user search for Kost with name "mix kost pasutri" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      And user fill out date as tomorrow date
      And user fills out rent duration equals to 4 Bulan and clicks on Continue button
      When user input phone number with "764546721833"
      Then user can sees "Nomor handphone tidak valid" as empty phone number invalid text
      When user input phone number with "76454672Rini"
      Then user can sees "Nomor handphone tidak valid" as empty phone number invalid text
      And user can sees phone number input value is "76454672"
      And user can sees continue button is disabled

    @bookingWithoutCheckOnTerm&Conditions @MB-2639
    Scenario: Booking Without Check On Term & Conditions
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "gender female"
      When user navigates to Explore page
      And user search for Kost with name "mix kost pasutri" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      And user fill out date as tomorrow date
      And user fills out rent duration equals to 4 Bulan and clicks on Continue button
      And user tap on Save & Continue button
      Then user can sees submit booking button is disabled