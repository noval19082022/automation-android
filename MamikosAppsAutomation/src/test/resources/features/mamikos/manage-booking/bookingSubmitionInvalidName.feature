# Author: Sakti
# Testcase: MB-2619

@regression @manageBooking @occupancyAndBilling

  Feature: OB - Booking Submission Name Section

    @bookingInvalidName
    Scenario: Booking Invalid Name
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "master"
      When user navigates to Explore page
      And user search for Kost with name "booking" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      And user fill out date as tomorrow date
      And user fills out rent duration equals to 4 Bulan and clicks on Continue button
      And user input "" with delete char is true
      Then user can see validation name is "Mohon masukkan nama"
      And user can see Continue button is not clickable
      When user input "R" with delete char is false
      Then user can see validation name is "Minimal 2 text"
      And user can see Continue button is not clickable
      When user input "RiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRiniRini" with delete char is false
      But user could not input for more than 50 character