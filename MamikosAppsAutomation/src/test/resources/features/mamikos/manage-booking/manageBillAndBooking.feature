@regression @manageBillAndBooking @manageBooking @occupancyAndBilling
  Feature: Manage Bill And Booking

    @ownerDidntHaveKostList @iOS
    Scenario: Kost list I don't have kost
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "no property list"
      Then I can see empty list of Kelola Tagihan dan Booking
      Then I can see in empty list text
        | anda belum mendaftarkan kos \n silakan tambahkan kos anda terlebih dahulu. |
        | Tambah Kos                                                                 |
        | Tambah Kost                                                                |

    @kostListAvailableKost
    Scenario: I have kost list
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "have property list"
      And I tap on Kelola Tagihan Dan Booking button
      Then I can see kost list
      Then I can see box kost list number 1
      And Box kost list number 1 contain "image thumbnail"
      And Box kost list number 1 contain "kost name"
      And Box kost list number 1 contain "available room"
      And Box kost list number 1 contain "kost location"
      And Box kost list number 1 contain "last update date"
      And I tap in the box kost list number 1.
      Then I can reach kost detail page for mamipay

    @tenantWaitForApproval
    Scenario: Kost list has tenant waiting for approval
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "tenant waiting for booking approval"
      When user navigates to Explore page
      And user search for Kost with name "tenant waiting for booking approval" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      And user fill out date as tomorrow date
      And user fills out rent duration equals to 4 Bulan and clicks on Continue button
      And user selects T&C checkbox and clicks on Book button
      And user navigates to main page after booking
      And user navigates to profile page
      And user logs out as a Tenant user
      And user logs in as Owner "owner have tenant wait for booking approval"
      And I tap on Kelola Tagihan Dan Booking button
      Then I can see kost list
      Then I can see box kost list number 2
      And I can see box kost list number 2 have tenant waiting for approval
      And I tap on tenant waiting for approval text on kost list number 2
      Then I can reach owner "Booking" section
