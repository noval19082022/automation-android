@regression @manageBooking @occupancyAndBilling
Feature: OB Additional Price Deposit

  @activatedDendaWithPriceAs4DigitAndInputDibebankanSetelahAndChooseAtTheTimeDenda @MB-3318
  Scenario: Activated Denda with Price as 4 Digit and Input Dibebankan Setelah and Choose at the Time Denda
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Kose Putri Automation"
    And user activates denda toggle
    Then user can sees "denda" price popup
    And user inputs denda price with "30000" and biaya dibebankan price with "4"
    And user taps on save price button
    Then user can sees "denda" added to the list
    When user taps on change denda price button
    Then user can sees "denda" price popup
    And user inputs denda price with "15000"
    And user taps on save price button
    Then user can sees "denda" added to the list
    When user taps on delete button on denda section
    Then confirmation popup for deleting "denda" price is appear
    When user taps on yes delete button
    Then user cannot sees price list in the "denda" section