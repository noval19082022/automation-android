@regression @manageBooking @occupancyAndBilling
Feature: OB Additional Price Deposit

  @activateDepositAndInputCorrectData @MB-3323
  Scenario: Activate Deposit And Input Correct Data
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Kose Mamiset Automation"
    And user activates deposit toggle
    Then user can sees "deposit" price popup
    When user inputs deposit price for "200000"
    And user taps on save price button
    Then user can sees "deposit" added to the list
    And user can sees deposit price is "Rp200.000"
    When user taps on change deposit price button
    And user inputs deposit price for "300000"
    And user taps on save price button
    And user can sees deposit price is "Rp300.000"
    When user taps on delete button
    Then confirmation popup for deleting "deposit" price is appear
    When user taps on yes delete button
    Then user cannot sees price list in the "deposit" section