@regression @manageBooking @occupancyAndBilling @downPayment
Feature: OB Down Payment DP

  @MB-3327
  Scenario: Activate DP And Check Default Price On Kost Detail Price List
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "ob owner"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Ancient Fuelweaver Automation"
    And user taps on down payment toggle
    Then user can sees "down payment" price popup
    And user can sees down payment input price and detail price view
    And user can sees default percentage is "10%"
    And user can sees down payment price list :
      | DP Sewa Per Bulan   |
      | DP Sewa Per 3 Bulan |
      | DP Sewa Per 6 Bulan |
      | DP Sewa Per Tahun   |
      | DP Sewa Per Minggu  |
    When user taps on save price button
    Then user can sees down payment is in active state

  @MB-3330 @dpActiveNonActive
  Scenario: Kost Detail Price Check And Nonactive Down Payment
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user log in as Tenant via phone number as "DC Automation"
    And user navigates to Explore page
    And user search for Kost with name "additional price ob" and selects matching result and navigate to Kost details page
    And user taps see all price button
    Then user can sees down payment text price
    When user back to Home page
    And user navigates to profile page
    And user logs out as a Tenant user
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "ob owner"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Ancient Fuelweaver Automation"
    Then user can sees down payment percentage is "10%" with price range "Rp30.000 - Rp1.100.000"
    When user taps on down payment toggle
    Then user can sees message confirmation is "Biaya berhasil di-nonaktifkan"
    When user taps on down payment toggle
    Then user can sees message confirmation is "Biaya berhasil di-aktifkan"
    And user can sees down payment percentage is "10%" with price range "Rp30.000 - Rp1.100.000"
    When user taps on update price button
    Then user can sees message confirmation is "Harga berhasil diupdate"
    When user back to owner dashboard
    And owner user navigates to profile page
    And user logs out as a Owner user
    And user log in as Tenant via phone number as "DC Automation"
    And user navigates to Explore page
    And user search for Kost with name "additional price ob" and selects matching result and navigate to Kost details page
    And user taps see all price button
    Then user can sees dp is active with price are data below :
      | Rp101.000   |
      | Rp281.000   |
      | Rp561.000   |
      | Rp1.101.000 |

  @MB-3328 @dpDelete
  Scenario: Edit DP And Check Price On Kost Detail
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "ob owner"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Ancient Fuelweaver Automation"
    And user taps on down payment change's button
    Then user can sees "down payment" price popup
    When user choose "30%" as down payment price
    And user taps on save price button
    Then user can sees down payment percentage is "30%" with price range "Rp90.000 - Rp3.300.000"
    When user back to owner dashboard
    And owner user navigates to profile page
    And user logs out as a Owner user
    And user log in as Tenant via phone number as "DC Automation"
    And user navigates to Explore page
    And user search for Kost with name "additional price ob" and selects matching result and navigate to Kost details page
    And user taps see all price button
    Then user can sees dp is active with price are data below :
      | Rp301.000   |
      | Rp841.000   |
      | Rp1.681.000 |
      | Rp3.301.000 |

  @dpDelete @MB-3329
  Scenario: Disable, Enable, And Delete Down Payment
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "ob owner"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Ancient Fuelweaver Automation"
    And user taps on down payment toggle
    Then user can sees message confirmation is "Biaya berhasil di-nonaktifkan"
    When user taps on down payment toggle
    Then user can sees message confirmation is "Biaya berhasil di-aktifkan"
    When user taps on delete down payment button
    Then confirmation popup for deleting "down payment" price is appear
    When user taps on yes delete button
    Then user can see down payment state is "OFF"