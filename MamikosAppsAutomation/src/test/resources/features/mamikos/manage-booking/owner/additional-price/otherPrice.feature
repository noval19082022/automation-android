@regression @manageBooking @occupancyAndBilling @otherPrices
Feature: OB Additional Price Biaya Lainnya

  @MB-3310 @MB-3311
  Scenario: Activate Other Price With 20 character Description And Correct Input Price
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Kose Putri Automation"
    And user activates other price toggle
    Then user can sees other price's input box
    When user inputs other price name with "Biaya listrik klein", total price with 50000
    And user taps on save price button
    Then user can sees other price is in the active state
    And user can sees other price name is "Biaya listrik klein", with price is "Rp50.000"
    When user change "Biaya listrik klein" name to "Biaya listrik kami" and price to 51000
    And user taps on save price button
    Then user can sees other price is in the active state
    And user can sees other price name is "Biaya listrik kami", with price is "Rp51.000"
    When user deletes "Biaya listrik kami" from other price
    Then confirmation popup for deleting "Biaya listrik kami" price is appear
    When user taps on yes delete button
    Then "Biaya listrik kami" is disappear from the list

  @MB-3309
  Scenario: Activate Second Other Price With Name Contains Number And Special Character
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Kose Putri Automation"
    And user activates other price toggle
    Then user can sees other price's input box
    When user inputs other price name with "Biaya listrik klein", total price with 50000
    And user taps on save price button
    Then user can sees other price is in the active state
    When user taps on add more other price button
    Then user can sees other price's input box
    When user inputs other price name with "@Jimpitan RT26/RW27", total price with 30000
    And user taps on save price button
    Then user can sees other price is in the active state
    And user can sees other price name is "Biaya listrik klein", with price is "Rp50.000"
    And user can sees other price name is "@Jimpitan RT26/RW27", with price is "Rp30.000"

  @MB-3313
  Scenario: Delete Active Other Prices
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Kose Putri Automation"
    When user deletes "Biaya listrik klein" from other price
    Then confirmation popup for deleting "Biaya listrik klein" price is appear
    When user taps on yes delete button
    Then "Biaya listrik kami" is disappear from the list
    When user deletes "@Jimpitan RT26/RW27" from other price
    Then confirmation popup for deleting "@Jimpitan RT26/RW27" price is appear
    When user taps on yes delete button
    Then "@Jimpitan RT26/RW27" is disappear from the list