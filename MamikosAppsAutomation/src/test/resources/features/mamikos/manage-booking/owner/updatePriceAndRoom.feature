@regression @manageBooking @occupancyAndBilling
Feature: OB Update Price And Room

  @seeOtherPriceOnUpdatePriceAndRoom @3306
  Scenario: See Other Price on Update Price And Room
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user dismiss pop-up
    And user goes to update room and price page
    And user taps on kost list with name "Kose Mamiset Automation"
    Then user can sees price list :
      | Harga Per Bulan |
    And user taps on "Tampilkan lebih banyak"
    And user can sees price list :
      | Harga Per Minggu  |
      | Harga Per 3 Bulan |
      | Harga Per 6 Bulan |
      | Harga Per Tahun   |
    And user taps on "Tampilkan lebih sedikit"
    And user can sees price list :
      | Harga Per Bulan |
    And user cannot sees other price options
    And user updates main price to "2000000"
    And user updates main price to "3000000"