@regression @manageBooking @booking @occupancyAndBilling
  Feature: Price And Rent Duration

    @checkPriceList
    Scenario: Check price list on booking confirmation
      Given Server configuration is selected
      When user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "price and rent duration"
      And user navigates to Explore page
      And user search for Kost with name "price and rent duration" and selects matching result and navigate to Kost details page
      Then user click on price of details page and price details popup should be shown
      And tenant can see price list
        |daily              |
        |weekly             |
        |monthly            |
        |semiannually       |
        |yearly             |
      When user clicks on Booking button on Kost details page
      Then tenant can see price list on booking confirmation
        |Per Minggu           |
        |Per Bulan            |
        |Per 6 Bulan          |
        |Per Tahun            |
      And tenant can not see daily price


    @chooseRentalPrice
    Scenario: Choose rental price
      Given Server configuration is selected
      When user is on main page and click on Later option when asked on pop up
      And user logs in as Tenant via Facebook credentails "price and rent duration"
      And user navigates to Explore page
      And user search for Kost with name "price and rent duration" and selects matching result and navigate to Kost details page
      And user clicks on Booking button on Kost details page
      Then user verify rent paid with "Dibayar sebulan sekali"

     @updateRentalPrice
     Scenario: Update rental price
       Given Server configuration is selected
       When user is on main page and click on Later option when asked on pop up
       And user logs in as Tenant via Facebook credentails "price and rent duration"
       And user navigates to Explore page
       And user search for Kost with name "price and rent duration" and selects matching result and navigate to Kost details page
       And user clicks on Booking button on Kost details page
       And user verify rent paid with "Dibayar sebulan sekali"
       Then user verify each rental price
       | Per Bulan              | Per Minggu              | Per 3 Bulan            | Per 6 Bulan            | Per Tahun
       | Dibayar sebulan sekali | Dibayar seminggu sekali | Dibayar 3 bulan sekali | Dibayar 6 bulan sekali | Dibayar setahun sekali

     @defaultRentalDuration
     Scenario: Default rental duration
       Given Server configuration is selected
       When user is on main page and click on Later option when asked on pop up
       And user logs in as Tenant via Facebook credentails "price and rent duration"
       And user navigates to Explore page
       And user search for Kost with name "price and rent duration" and selects matching result and navigate to Kost details page
       And user clicks on Booking button on Kost details page
       And user verify rent paid with "Dibayar sebulan sekali"
       Then user verify each rental duration
       | Per Bulan | Per Minggu | Per 3 Bulan | Per 6 Bulan | Per Tahun |
       | 1 Bulan   | 1 Minggu   | 3 Bulan     | 6 Bulan     | 1 Tahun   |