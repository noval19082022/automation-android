@regression @apartmentDetails @martech
Feature: Apartment Details

  @shareApartment
  Scenario: Click on share button from apartment details and verify user share apartment details successfully
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on apartment to open apartment details
    And user click on share button
    And click on "Bluetooth" option from share popup
    Then verify bluetooth popup should be shown

  @loveApartment
  Scenario: Try to make favorite and un-favourite the apartment
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on apartment to open apartment details
    And user click on love button of apartment
    Then system display toast message "Anda akan mendapat notifikasi ketika Apartemen ini kosong"
    And user click on love button of apartment
    Then system display toast message "Apartemen berhasil Tidak di Sukai"

  @contactApartment
  Scenario: Click on contact apartment button without login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on apartment to open apartment details
    And user click on contact apartment button
    Then verify login page should be shown

  @contactApartmentWithLogin
  Scenario: Click on contact apartment button with login
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "master"
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on apartment to open apartment details
    And user click on contact apartment button
    Then verify contact apartment page gets open with back button and Kirim button
    And user click on send button from contact apartment page
    And user is on main page and click on Later option when asked on pop up
    Then verify chat group gets open between "Lingga Marqansyah, CS Mamikos Ruri, Pemilik Kost"

  @apartmentMapView
  Scenario: Check map view is available on apartment details page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on apartment to open apartment details
    Then user scroll down to Apartment location and verify map view should be shown

  @apartmentRecommended
  Scenario: Check recommended boarding around section
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak" and select matching result from area list
    And user click on apartment to open apartment details
    And user scroll down to Recommended boarding section
    Then verify below apartment list
      | Campur |
      | Putri |
      | Putri  |
    And user swipe right and verify other apartment list should be shown
      | Putri  |
      | Campur |

  @otherApartment
  Scenario: Check interesting other apartment
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user click on Apartment Tab from home page
    And user search apartment "Jalan Cilandak Dalam" and select matching result from area list
    And user click on apartment to open apartment details
    And user scroll down to Interesting other apartment section
    Then verify below apartment list
      | 3 BR |
      | 2 BR |
      | 3 BR |
    And user swipe right and verify other apartment list should be shown
      | 3 BR |
      | 2 BR |