@regression @goodAndServices @martech
Feature: Search Good and Service

  Background:
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up

  @searchBox
  Scenario: Check search box of good and service
    And user click on Goods and Services tab
    And user tap Area
    And user tap "Yogyakarta"
    And user click on Map view button
    And user click on the Search bar of map
    Then should display text field

  @maoContent
  Scenario: Check map view content
    And user click on Goods and Services tab
    And user tap Area
    And user tap "Yogyakarta"
    And user click on Map view button
    Then verify search field, Good cluster with number "7. rooms.", Service cluster with number "33. rooms." and Number cluster with number "28. rooms."

  @greenCluster
  Scenario: Tap on number cluster and verify Good and Services list should be shown
    And user click on Goods and Services tab
    And user tap Area
    And user tap "Yogyakarta"
    And user click on Map view button
    And click on green cluster number "31. rooms." from map view
    Then verify below goods and services list should be shown
      | Barang |
      | Barang |
      | Barang |
      | Jasa   |
      | Jasa   |
      | Jasa   |
      | Jasa   |

  @priceClusterForGoodsAndServices
  Scenario: Tap on price cluster and verify goods and services details
    And user click on Goods and Services tab
    And user tap Area
    And user tap "Yogyakarta"
    And user click on Map view button
    And click on price cluster "6. rooms." to open kos details
    Then verify goods and service price "Rp. 400.000" should be shown

    @filterCheck
  Scenario: Check Filter content of map view
    And user click on Goods and Services tab
    And user tap Area
    And user tap "Yogyakarta"
    And user click on Map view button
    And user click on Filter button
    Then verify price range heading, Reset button and search button should be shown

  @resultList
  Scenario: Search Result Barang dan Jasa
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    Then user see Goods and Service search result

  @reachDetailPage
  Scenario: Access Barang dan Jasa Detail Page
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user choose random goods and service
    Then user is in detail page

    @accessSoldoutAds
  Scenario: Access Barang dan Jasa Page with soldout ads
    #And user logs in as Tenant via Facebook credentails "lingga_ccabvrn_marqansyah@tfbnw.net" and "joinmamikos"
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user choose soldout goods and service
    Then user is in detail page
    And user validate inactive ads

  @accessChatfromDetailPage
  Scenario: Access Chat from Barang dan Jasa Detail Page
    #And user logs in as Tenant via Facebook credentails "lingga_ccabvrn_marqansyah@tfbnw.net" and "joinmamikos"
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user choose random goods and service
    Then user is in detail page
    And user tap chat button in detail page

  @filterResult
  Scenario Outline: Sort Search Result Barang dan Jasa <Sorting>
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    Then user see Goods and Service search result
    And user tap Urutkan
    Then Sorting goods and service list is appear by :
      | Acak |
      | Harga termurah |
      | Harga termahal |
    And user choose <Sorting> for result list
    Examples:
      |Sorting|
      | Acak |
      | Harga termurah |
      | Harga termahal |

  @priceRange
  Scenario: Check filter Range Harga
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user click filter icon on page list goods and services
    And user input price range min "1000000" and max "10000000" and click search button
    Then system display the 5 highest goods and services for price "1000000" to "10000000"

  @resetFilter
  Scenario: Check filter reset function
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user click map icon on page list goods and services
    And user click filter icon on page list goods and services
    And user input price range min "1000000" and max "10000000"
    And user click reset button on filter page for filter goods and services
    Then system display default price range

  @checkContentListGoodsAndServices
  Scenario: Check content on list goods and services
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    Then system display content each list goods and services

  @backButtonArrow
  Scenario: Access list of goods and services using back button arrow on page goods and services details
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    And user choose random goods and service
    And user is in detail page
    And user click back button arrow on page goods and services details
    Then system display content each list goods and services

  @swipeUpDownGoodsAndServices
  Scenario: Swipe up and down for load new list Goods and Services
    And user click on Goods and Services tab
    And user tap Area
    And user click on Pencarian Populer "Yogyakarta"
    Then user swipe up and verify new List
    Then user swipe down and verify new List