@regression @martech @vacancy

Feature: Sort Job Vacancies

  @vacancy-sort
  Scenario: Sort job vacancies
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user click on Job vacancy Tab
    And user click on first popular search
    And user sort job vacancies "Via mamikos"
    Then system display job vacancies "Via mamikos"

#  Scenario: Sort job vacancies by Lowongan Terbaru
    When user sort job vacancies "Lowongan Terbaru"
    Then system display job vacancies "Lowongan Terbaru"

#  Scenario: Sort job vacancies by Lowongan Terlama
    When user sort job vacancies "Lowongan Terlama"
    Then system display job vacancies "Lowongan Terlama"

  @cancelApplying
  Scenario: Cancel Applying
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user logs in as Tenant via Facebook credentails "firstMartechAccount"
    And user click on Job vacancy Tab
    And user click on first popular search
    And user sort job vacancies "Lowongan Terbaru"
    And user clicks on the top job on the list
    And user click on apply now button
    And user input job applicant data and click continue button
    And user input competency of job applicant and click continue button
    And user click on arrow button on page competency of job applicant
    Then system display job vacancy list

  @checkContentJobVacancyDetail
  Scenario: Verify content on page job vacancy details
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user click on Job vacancy Tab
    And user click on first popular search
    And user sort job vacancies "Lowongan Terbaru"
    And user clicks on the top job on the list
    And user verify content job vacancy details
    And user click back button arrow on page job vacancy details
    Then system display job vacancy list

  @filterJobVacancyByType
  Scenario: Filter job vacancy type freelance
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user click on Job vacancy Tab
    And user tap Area
    And user click on first popular search
    And user filter job vacancies by "Tipe Loker" and "Freelance"
    Then system display job vacancies type "Freelance"

#  Scenario: Filter job vacancy type freelance
    And user filter job vacancies by "Tipe Loker" and "Full-time"
    Then system display job vacancies type "Full-Time"

#  Scenario: Filter job vacancy type part-time
    And user filter job vacancies by "Tipe Loker" and "Part-time"
    Then system display job vacancies type "Part-Time"

#  Scenario: Filter job vacancy type magang
    And user filter job vacancies by "Tipe Loker" and "Magang"
    Then system display job vacancies type "Magang"

  @filterJobVacancyByEducation
  Scenario: Verify content on page job vacancy details
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user click on Job vacancy Tab
    And user tap Area
    And user click on first popular search
    And user filter job vacancies by "Edukasi" and "SMA/SMK"
    Then system display job vacancies last education "SMA/SMK"

#  Scenario: Filter job vacancy by last-education D1/D2/D3
    And user filter job vacancies by "Edukasi" and "D1/D2/D3"
    Then system display job vacancies last education "Full-Time"

#  Scenario: Filter job vacancy by last-education Sarjana/S1
    And user filter job vacancies by "Edukasi" and "Sarjana/S1"
    Then system display job vacancies last education "Part-Time"

#  Scenario: Filter job vacancy by last-education Master/S2
    And user filter job vacancies by "Edukasi" and "Master/S2"
    Then system display job vacancies last education "Magang"

#  Scenario: Filter job vacancy by last-education Doktor/S3
    And user filter job vacancies by "Edukasi" and "Doktor/S3"
    Then system display job vacancies last education "Full-Time"

#  Scenario: Filter job vacancy by last-education Mahasiswa
    And user filter job vacancies by "Edukasi" and "Mahasiswa"
    Then system display job vacancies last education "Part-Time"

#  Scenario: Filter job vacancy by last-education Fresh Graduate
    And user filter job vacancies by "Edukasi" and "Fresh Graduate"
    Then system display job vacancies last education "Fresh Graduate"

  @sortJobVacancy
  Scenario: Sort job vacancy by Via MamiKos
    Given Server configuration is selected
    When user is on main page and click on Later option when asked on pop up
    And user click on Job vacancy Tab
    And user tap Area
    And user click on first popular search
    And user filter job vacancies by "Urutkan" and "Via MamiKos"
    Then system display job vacancies last education "Via MamiKos"

#  Scenario: Sort job vacancy by Lowongan terbaru
    And user filter job vacancies by "Urutkan" and "Lowongan terbaru"
    Then system display job vacancies "Lowongan Terbaru"

#  Scenario: Sort job vacancy by Lowongan Lama
    And user filter job vacancies by "Urutkan" and "Lowongan Lama"
    Then system display job vacancies "Lowongan Terlama"