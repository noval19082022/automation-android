@regression @search @martech
Feature: Search and Filter Apartment

  @searchApartment
  Scenario: Positive case filter apartment by unit type
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment "Jalan Cilandak"
    And user click icon filter
    Then system display apartment filter list
      |Tipe Unit   |
      |Perabotan    |
      |Jangka Waktu |
      |Range Harga  |
    And System display option button
      |RESET  |
      |CARI   |

#  Scenario: Positive case search apartment filter by unit type "1-Room Studio"
    When user filter apartment by "unit type" is "1-Room Studio"
    Then system display apartment "unit type" is "Tipe Unit 1-Room Studio"

#  Scenario: Positive case search apartment filter by unit type "1-Room Studio"
    When user click back
    And user click icon filter
    And user filter apartment by "unit type" is "1-Room Studio"
    Then system display apartment "unit type" is "Tipe Unit 1-Room Studio"

#  Scenario: Positive case search apartment filter by unit type "1 BR"
    When user click back
    And user click icon filter
    And user filter apartment by "unit type" is "1 BR"
    Then system display apartment "unit type" is "Tipe Unit 1 BR"

#  Scenario: Positive case search apartment filter by unit type "2 BR"
    When user click back
    And user click icon filter
    And user filter apartment by "unit type" is "2 BR"
    Then system display apartment "unit type" is "Tipe Unit 2 BR"

#  Scenario: Positive case search apartment filter by unit type "3 BR"
    When user click back
    And user click icon filter
    And user filter apartment by "unit type" is "3 BR"
    Then system display apartment "unit type" is "Tipe Unit 3 BR"

#  Scenario: Positive case search apartment filter by unit type "4 BR"
    When user click back
    And user click icon filter
    And user filter apartment by "unit type" is "4 BR"
    Then system display apartment "unit type" is "Tipe Unit 4 BR"

  Scenario: Positive case search apartment filter by furniture
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment "Jalan Cilandak"
    And user click icon filter
    And user filter apartment by "furniture" is "Furnished"
    Then system display apartment "furniture" is "Furnished"

#  Scenario: Positive case search apartment filter by furniture "Semi Furnished"
    When user click back
    And user click icon filter
    And user filter apartment by "furniture" is "Semi Furnished"
    Then system display apartment "furniture" is "Semi Furnished"

#  Scenario: Positive case search apartment filter by furniture "Not Furnished"
    When user click back
    And user click icon filter
    And user filter apartment by "furniture" is "Not Furnished"
    Then system display apartment "furniture" is "Not Furnished"

  Scenario: Positive case search apartment filter by time period "Harian"
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment "Sentra Timur Residence Apartment"
    And user click icon filter
    And user filter apartment by "time period" is "Harian"
    Then system display apartment "time period" is "hari"

#  Scenario: Positive case search apartment filter by time period "Mingguan"
    When user click icon filter
    And user filter apartment by "time period" is "Mingguan"
    Then system display apartment "time period" is "minggu"

#  Scenario: Positive case search apartment filter by time period "Bulanan"
    When user click icon filter
    And user filter apartment by "time period" is "Bulanan"
    Then system display apartment "time period" is "bulan"

#  Scenario: Positive case search apartment filter by time period "Tahunan"
    When user click icon filter
    And user filter apartment by "time period" is "Tahunan"
    Then system display apartment "time period" is "tahun"

  Scenario: Negative case search apartment filter by price range min "6000000" max "3500000"
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment "Jalan Cilandak"
    And user click icon filter
    And user filter apartment by "price range" min "6000000" max "3500000"
    Then system display toast message "Anggaran minimum tidak boleh lebih besar dari anggaran maksimum"

#  Scenario: Positive case search apartment filter by price range min "3500000" max "6000000"
    And user filter apartment by "price range" min "3500000" max "6000000"
    Then system display apartment "price range" with min price "3500000" max price "6000000" per "bulan" for 5 highest lists

  Scenario: Positive case redirect to cluster map and search by three character
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment by three character "jak"
    And user select list option area "Jakarta Selatan"
    Then system display map

  Scenario: Positive case sort the apartment list randomly
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment "Jalan Cilandak"
    And user sorting "Acak" for apartment list
    Then system display 5 list of apartment as "Acak"

#  Scenario: Positive sort the list of apartments from the cheapest
    When user sorting "Harga termurah" for apartment list
    Then system display 5 list of apartment as "Harga termurah"

#  Scenario: Positive case sort the list of apartments most expensive
    When user sorting "Harga termahal" for apartment list
    Then system display 5 list of apartment as "Harga termahal"

  Scenario: Positive case go to apartment list view using search autocomplete
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment by three character "jak"
    And user select list option area "Jakarta Selatan"
    Then system display back button
    And system display input text search area contains "Jakarta Selatan"
    And System display list view tab
    And system display map view tab
    And system display filter icon
    And system display sorting icon

#  Scenario: Positive case verify element must display on sorting screen
    When user click sorting sorting icon
    Then system display close icon

  Scenario: Positive case swipe down for lihat lebih banyak
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment by three character "jak"
    And user select list option area "Jakarta Selatan"
    Then system display loading animation to load listing

  Scenario: Positive case redirect to apartment detail page when click apartment name
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user search apartment by three character "jak"
    And user select list option area "Jakarta Selatan"
    And user click apartment name
    Then system display apartment photos
    And system display apartment furniture type
    And system display apartment name in detail page
    And system display apartment address
    And system display apartment last update