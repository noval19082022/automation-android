@regression @mamipay @mamipayDashboard

  Feature: Mamipay Dashboard

    @newJoin @ownerExp
    Scenario: Check screen dashboard owner new join
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "new owner"
      And user clicks on Cancel option verification when asked on pop up
      And user clicks on Close Email Verification when show on pop up
      Then screen dashboard is shown elements
      And User clicks on Icon Notification
      Then screen detail notification with tab Update and tap Tagihan is shown
      And user tap back to Homepage
      And User clicks on Add Kost button
      Then screen detail Tambah Kost is shown

    @hasProperty @iOS
    Scenario: Check screen owner has property
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "property owner"
      And user clicks on Close Owner Invitation Banner when show on pop up
      And user is on main page and click on Later option when asked on pop up
      And user Close Update Property Banner when show on pop up
      Then screen dashboard is shown elements Bisa Booking and Empty Finance

      @hasPropertyNotification @iOS
      Scenario: Check screen notification owner has property
        Given Server configuration is selected
        And user is on main page and click on Later option when asked on pop up
        And user logs in as Owner "property owner"
        And user clicks on Close Owner Invitation Banner when show on pop up
        And user is on main page and click on Later option when asked on pop up
        And user Close Update Property Banner when show on pop up
        And user clicks on Close Instant Booking Tooltip when show on pop up
        And user clicks on Close Email Verification when show on pop up
        And User clicks on Icon Notification
        Then screen detail notification with tab Update and tap Tagihan is shown

    @premiumActive @iOS
    Scenario: Check dashboard owner Non BBK with premium active
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "premium owner"
      And user is on main page and click on Later option when asked on pop up
      And user Close Update Property Banner when show on pop up
      And user clicks on Close Instant Booking Tooltip when show on pop up
      And user Close Update Property Banner when show on pop up
      And user clicks on Close Instant Booking Tooltip when show on pop up
      Then screen detail active premium widget

    @financeActive @iOS
    Scenario: Check dashboard Finance is active and has balance
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "owner having balance"
      And user is on main page and click on Later option when asked on pop up
      And user Close Update Property Banner when show on pop up
      And user clicks on Close Instant Booking Tooltip when show on pop up
      And user Close Update Property Banner when show on pop up
      And user clicks on Close Instant Booking Tooltip when show on pop up
      Then screen detail widget Finance with active balance

    @tenantList
    Scenario: Check screen when owner has tenant list
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "owner having balance"
      And user is on main page and click on Later option when asked on pop up
      And user Close Update Property Banner when show on pop up
      And user clicks on Close Instant Booking Tooltip when show on pop up
      And user Close Update Property Banner when show on pop up
      And user clicks on Close Instant Booking Tooltip when show on pop up
      Then screen detail widget Renter with active tenant