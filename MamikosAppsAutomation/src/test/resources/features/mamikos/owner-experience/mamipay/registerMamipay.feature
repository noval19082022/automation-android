@regression @regMamipay @ownerExp

Feature: Register Mamipay

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "owner BBK Reject"
    And user tap register to Instant Booking in Manage section
    When user click activate now instant booking in dashboard
    And user click next button
    And user click next in pop up page

  @invalidFNameRegMamipay
  Scenario: Full name is inputed with invalid value in register mamipay
    Given user fill full name in register BBK form with ""
    And user click bank account field in register BBK form
    Then user see error message "Nama lengkap tidak boleh kosong." below field
    When user fill full name in register BBK form with "123456 abc"
    Then user see error message "Hanya diisi dengan huruf" below field
    When user fill full name in register BBK form with "a!@#$%^*()"
    Then user see error message "Hanya diisi dengan huruf" below field
    When user fill full name in register BBK form with "ab"
    Then user see error message "Minimal 3 Karakter" below field
    When user fill full name in register BBK form with " "
    Then user see error message "Nama lengkap tidak boleh kosong." below field
    When user fill full name in register BBK form with "aaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaa cccccccccV"
    Then user see error message "Maksimal 50 Karakter" below field
    When user hide keyboard and scroll down
    And user click on term and condition checkbox
    Then system display toast message "Anda belum melengkapi data"


  @invalidBankAccNoMamipay
  Scenario: Bank account number is inputed with invalid value in register mamipay
    Given user fill full name in register BBK form with "tes data"
    When user fill bank account number in register BBK form with ""
    And user click full name field in register BBK form
    Then user see error message "Nomor rekening tidak boleh kosong." below field
    When user fill bank account number in register BBK form with "12223333333312222221321231"
    Then user see error message "Maksimal 25 Karakter" below field
    When user fill bank account number in register BBK form with "9999"
    Then user see error message "Minimal 5 Karakter" below field
    When user hide keyboard and scroll down
    And user click on term and condition checkbox
    Then system display toast message "Anda belum melengkapi data"
    When user fill bank account number in register BBK form with "abc defghi"
    Then user don't see text "abc defghi" in bank account number field

  @invalidBankNameMamipay
  Scenario: Bank Name invalid value in activation form mamipay
    Given user click on drop down list Bank Name in form mamipay
    And user click back
    And user input Bank Name with " " in form activate mamipay
    Then user see error message "Nama bank harus dipilih" in form activate mamipay
    And user input Bank Name with "Bank Test" in form activate mamipay
    Then user see error message "Nama bank harus dipilih" in form activate mamipay
    When user hide keyboard and scroll down
    And user click on term and condition checkbox
    Then system display toast message "Anda belum melengkapi data"