@regression @apartmentads @ownerExp
  Feature: My Ads

    @addapartmentads
    Scenario: Apartment Ads - Add Apartment Ads As Apartment Owner
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "master"
      And owner user navigates to manage page
      And user tap arrange button under my property
      When user click on Apartment Tab
      And User clicks on Add Property button in bottom
      And user click on As An Owner
      And user input their Apartment Name "Automation Ignite", Unit Name "Automation Ignite", Unit Number "2", Room Type "1-Room Studio", Unit Floor "6", Unit Area "3000", Daily Price "1000000", Weekly Price "1000000", Monthly Price "10000000", Yearly Price "100000000", Description "Automation Ignite", Minimal Rent "Min. 1 Hari", Apartment Maintenance Fee "100000" and Apartment Parking Fee "100000"
      Then user validate Apartment Title should be "Apartemen", Apartment Price should be "10 jt/bl", Apartment Name should be "Automation Ignite", Apartment Size should be "1-Room Studio - 3000 m2", Apartement Furnishing should be "Furnished", Apartment Bed should be "1", Apartment Bath should be "1", Apartment Floor should be "6"