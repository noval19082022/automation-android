@kosads
Feature: My Ads

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user Close Update Property Banner when show on pop up
    And user clicks on Close Instant Booking Tooltip when show on pop up
    And owner user navigates to ads page

  @roomFacMandatoryEditKos
  Scenario: Room facility is mandatory and editable in edit kos
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user click next button in form data
    And user click next button in form data
    When user pick room facility :
      | Kasur |
    Then user see "Kasur" is selected in bathroom facility
    When user pick facility again :
      | Kasur                 |
      | Kamar mandi luar Room |
    Then user see "Kasur" is not selected in bathroom facility
    And user click next button in form data
    Then system display toast message "Minimal Satu Fasilitas Kamar Dipilih"

  @batroomFacMandatoryEditKos
  Scenario: Bathoom facility is mandatory and editable in edit kos
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user click next button in form data
    And user click next button in form data
    And user pick bathroom facility :
      | Kloset jongkok |
    Then user see "Kloset jongkok" is selected in bathroom facility
    When user pick facility again :
      | Bathup         |
      | Kloset jongkok |
    Then user see "Kloset jongkok" is not selected in bathroom facility
    And user click next button in form data
    Then system display toast message "Minimal Satu Fasilitas Kamar Mandi Dipilih"

  @totalRoomDisabled
  Scenario: Text box total and available room isn't editable
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user click next button in form data
    When user click next button in form data
    And user scroll down until property policy
    Then user see error message "Edit jumlah kamar di menu update kamar." below room field
    And user see total room field is disabled
    And user see total available room field is disabled

  @privacyPolicyRequiredEditKos
  Scenario: Checkbox privasi policy is mandatory in edit kos
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user click next button in form data
    When user click next button in form data
    And user click next button in form data
    Then system display toast message "Anda harus menyetujui kost anda boleh di review"

  @monthlyPriceRequiredEditKos
  Scenario: Text box monthly price rent is mandatory in edit kos
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user click next button in form data
    When user input monthly price ""
    And user click next button in form data
    Then user see error message "Harga Bulanan tidak Boleh kosong" below monthly price field

  @priceFieldValidationEditKos
  Scenario: Renting price field is inputed with invalid value in edit kos
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user click next button in form data
    When user input monthly price "100"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below monthly price field
    When user input monthly price "1000"
    And user input daily price "100"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below daily price field
    When user input daily price "1000"
    And user input weekly price "999"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below weekly price field
    When user input weekly price "1000"
    And user input minimum 3 monthly price "99"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below 3 monthly price field
    When user input minimum 3 monthly price "1000"
    And user input minimum 6 monthly price "99"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below 6 monthly price field
    When user input minimum 6 monthly price "1000"
    And user input yearly price "99"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below yearly price field

  @editKosTillUploadPic
  Scenario: Edit kos until upload photo
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user input Kost Name "Automation EditKos", Kost Address "sriwijaya", Kost Owner Name "Pak Java", Kost Manager Name "Mr.Bon1(,r)", and Kost Manager phone number "088222226666"
    And user select boarder type "Putra", input daily price "100000", weekly price "700000", and monthly price "2800000"
    And user input 3 month min price "8000000", 6 month min price "16000000", and yearly price "30000000"
    And user select minimum payment "Minimal 3 Bln", input additional other cost "100000"
    And user click next button in form data
    And user pick room facility :
      | Kasur          |
      | TV             |
    And user pick bathroom facility :
      | Kloset jongkok |
      | Ember mandi    |
    And user pick public facility :
      | Dapur      |
      | Mesin cuci |
      | Security   |
    And user pick parking facility :
      | Parkir motor VIP |
      | Parkir mobil     |
    And user pick environmental facilities :
      | Masjid          |
      | ATM / Bank      |
    And user select room space "4 X 4"
    And user select year built "1990"
    And user scroll down until property policy
    When user click on privacy policy checkbox in add kos form
    And user click next button in form data
    Then user is in add photos page in add kos form

  @kosNameInvalidEditKos
  Scenario: Kos name validation with invalid value in edit kos
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user input Kost Name ""
    And user click next button in form data
    Then user see error message "Nama Kos Tidak Boleh Kosong" below field
    When user input Kost Name "<>???"
    Then user see error message "Nama kos harus berupa huruf, angka, dan karakter spesial seperti / ( ) - . , ' dan \"" below field
    # Fill with existing kos name
    When user input Kost Name "Kose Mamiset Automation"
    And user click next button in form data
    Then user see error message "Nama Kost Sudah Digunakan" below field

  @kosAddressMandatoryEditKos
  Scenario: Text box kos address is mandatory in edit kos
    Given user search kos "Mamites Kos coba baru" in my ads page and select matching result
    And user clicks on expand kost in kost ads page
    And user click on edit kos data
    And user input Kost Address ""
    And user click next button in form data
    Then user see error message "Masukkan Alamat Kost" below field
    And user see error message "Masukkan Alamat Kost" above address field