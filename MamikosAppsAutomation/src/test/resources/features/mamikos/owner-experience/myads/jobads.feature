@regression @jobads @ownerExp
  Feature: My Ads

    @addjobvacancy
    Scenario: My Ads - Job Vacancy - Add Job Vacancy
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      And user logs in as Owner "master"
      And owner user navigates to manage page
      And user tap arrange button under my property
      And user click on Vacancy Tab
      And User clicks on Add Property button in bottom
      And user input their Company Name "Automation Ignite" and Company Address "Automation Ignite Street"
      Then user input their Job Name "Automation Job" and random name, Minimum Study "Sarjana/S1", Job Status "Full-time", Job Description "Automate mobile app with Appium", Minimum Salary "5000000", Maximum Salary "10000000", and Salary Type "Bulanan"
      And user input their Person In Charge "Rheza", Phone Number "082144651880", Email "rheza@mamiteam.com" and Job Expired Date 7 days after
      And user click on Edit and Promote Label
      Then user verify Job Status "Tunggu Disetujui", Job Name "Automation Ignite", Job Description "Automation Job" and random name, and Salary Status "Gaji dirahasiakan"

#    @editjobvacancydata
#    Scenario: My Ads - Job Vacancy - Edit Job Vacancy
#      Given Server configuration is selected
#      And user is on main page and click on Later option when asked on pop up
#      And user logs in as Owner "master"
#      And user Close Update Property Banner when show on pop up
#      And user clicks on Close Instant Booking Tooltip when show on pop up
#      And owner user navigates to ads page
##      And user click Later on Iklan Saya Menu
#      And user click on Vacancy Tab
#      And user click on Edit and Promote Label
#      When user click on Edit Ads Data
#      And user click on Edit Vacancy Button
#      And user input their Company Name "Automation Ignite (edit test)" and Company Address "Automation Ignite Street (edit test)"
#      Then user input their Job Name "Automation Ignite Job (edit test)", Minimum Study "Mahasiswa", Job Status "Part-time", Job Description "Automate mobile app with Appium", Minimum Salary "20000", Maximum Salary "120000", and Salary Type "Mingguan"
#      And user input their Person In Charge "Rheza (edit test)", Phone Number "082144651880", Email "rheza@mamiteam.com" and Job Expired Date 12 days after
#      And user click on Edit and Promote Label
#      Then user verify Job Status "Tunggu Disetujui", Job Name "Automation Ignite (edit test)", Job Description "Automation Ignite Job (edit test)" and Salary Status "Gaji dirahasiakan"
#
#     @editjobvacancy
#     Scenario: My Ads - Job Vacancy - Edit Job Status
#       Given Server configuration is selected
#       And user is on main page and click on Later option when asked on pop up
#       And user logs in as Owner "master"
#       And user Close Update Property Banner when show on pop up
#       And user clicks on Close Instant Booking Tooltip when show on pop up
#       And owner user navigates to ads page
##       And user click Later on Iklan Saya Menu
#       And user click on Vacancy Tab
#       And user click on Edit and Promote Label
#       When user click on Edit Ads Data
#       And user click on Reactive Vacancy
#       When user click on Edit Ads Data
#       Then user verify Vacancy Status should be "DITUTUP"
#       And user click on Delete to Delete Job Vacancy