@kosads
Feature: My Ads

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user Close Update Property Banner when show on pop up
    And user clicks on Close Instant Booking Tooltip when show on pop up
    And owner user navigates to manage page
    And user tap arrange button under my property

  @addkosads
  Scenario: Add New Kos Ads As Owner
    Given User clicks on Add Property button in bottom
    And user click on add New Kos
    And user click on Add as New Kos
    And user clicks on start button beside Kos Data
    And user input Kost Name "Automation TestAddKos", Kost Address "majapahit", Kost Owner Name "Pak Oke", Kost Manager Name "Mr.Bon1(,r)", and Kost Manager phone number "085757575757"
    And user select boarder type "Campur", input daily price "100000", weekly price "700000", and monthly price "2800000"
    And user input 3 month min price "8000000", 6 month min price "16000000", and yearly price "30000000"
    And user select minimum payment "Minimal 3 Bln", input additional other cost "100000"
    And user insert inputer name "Cucumber", inputer email "cucumber@mamiteam.com", inputer phone "085777777777", and click next
    And user pick room facility :
      | Kasur          |
      | TV             |
      | Lemari pakaian |
    And user pick bathroom facility :
      | Bathup         |
      | Kloset jongkok |
      | Ember mandi    |
    And user pick public facility :
      | Dapur      |
      | Mesin cuci |
      | Security   |
    And user pick parking facility :
      | Parkir motor VIP |
      | Parkir mobil     |
    And user pick environmental facilities :
      | Masjid          |
      | ATM / Bank      |
      | Apotek / Klinik |
    And user select room space "3 X 4"
    And user select year built "1986"
    And user input total rooms "51", total available rooms "31", notes "no pet allowed", kos description "anti mager mager club", and click next
    When user add cover photo, building photo, room picture, bathroom picture, other facility photo, kos description "ruang tamu", and then click save
    Then user see pop up page content title is "Permintaan Aktivasi Dikirimkan" and description "Data berhasil dikirim dan akan diverifikasi admin dalam 2x24 jam. Mohon menunggu, ya."
    And user click done in pop up page
    And user see newly created kos is in most top of the list, with correct name, address "majapahit", status "Diperiksa Admin" and type "Kos Campur"

  @addKosMinChar @ownerExp @iOS
  Scenario: Add new kos with less than minimal characters in some entity on screen data kos
    Given User clicks on Add Property button in bottom
    And user click on add New Kos
    And user clicks on start button beside Kos Data
    When user input name kos with "Sana"
    And user clicks checkbox room type
    And user input room type with "x"
    Then user see warning kos name "Tidak boleh kurang dari 5 karakter."
    And user see warning room type "Tidak boleh kurang dari 2 karakter."
    When user input name kos with "Kost Sana"
    And user input room type with "Tipe A"
    And user click kos name field
    And user select room type with "female"
    And user input kos description with "Kos yang dijamin bagus, karena dilengkapi dengan fasilitas yang lengkap"
    And user select kos rule "Laundry"
    And user clicks checkbox administrator kos
    And user input administrator name with "A"
    Then user see warning administrator name "Tidak boleh kurang dari 2 karakter."
    When user input administrator name with "Aws"
    And user input mobile phone number with "0891234"
    Then user see warning mobile phone number administrator "Tidak boleh kurang dari 8 karakter."

  @addKostNoMandatory @ownerExp
  Scenario: Add new kos and without input mandatory field in some entity on screen data kos
    Given User clicks on Add Property button in bottom
    And user click on add New Kos
    And user clicks on start button beside Kos Data
    And user clicks checkbox room type
    When user click next button in form data
    Then user see warning kos name "Anda belum mengisi nama kos ini."
    And user see warning room type "Anda belum mengisi nama tipe kamar ini."
    When user input name kos with "Kost Sana"
    And user input room type with "Tipe A"
    And user click kos name field
    And user click next button in form data
    Then user see warning kos type "Pilih salah satu"
    When user select room type with "female"
    And user click next button in form data
    Then user see warning kos description "Anda belum mengisi deskripsi kos."
    When user input kos description with "Kos yang dijamin bagus, karena dilengkapi dengan fasilitas yang lengkap"
    And user click next button in form data
    And user click next button in form data
    Then user see warning kos year built "Pilih tahun kos dibangun."
    When user select kos year built with "2020"
    And user clicks checkbox administrator kos
    And user click next button in form data
    Then user see warning administrator name "Silakan isi nama pengelola."
    And user see warning mobile phone number administrator "Silakan isi no. HP pengelola."

  @addKostWithSpace @ownerExp
  Scenario: Add new kos and input mandatory field with space in some entity on screen data kos
    Given User clicks on Add Property button in bottom
    And user click on add New Kos
    And user clicks on start button beside Kos Data
    And user clicks checkbox room type
    When user input name kos with "      "
    And user input room type with "      "
    And user click next button in form data
    Then user see warning kos name "Anda belum mengisi nama kos ini."
    And user see warning room type "Anda belum mengisi nama tipe kamar ini."
    When user input name kos with "Kost Sana"
    And user input room type with "Tipe A"
    And user click kos name field
    And user select room type with "female"
    And user input kos description with "               "
    And user click next button in form data
    Then user see warning kos description "Anda belum mengisi deskripsi kos."
    When user input kos description with "Kos yang dijamin bagus oke bebe"
    And user select kos rule "Laundry"
    And user select kos year built with "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "        "
    And user click next button in form data
    Then user see warning administrator name "Silakan isi nama pengelola."

  @addKostAlreadyExist @ownerExp @iOS
  Scenario: Add new kos and input name kos already exist
    Given User clicks on Add Property button in bottom
    And user click on add New Kos
    And user clicks on start button beside Kos Data
    When user input name kos with "Kose Full Automation"
    And user click kos name field
    And user select room type with "male"
    And user input kos description with "Kos yang dijamin bagus oke bebe"
    And user select kos rule "Laundry"
    And user select kos year built with "2020"
    And user click next button in form data
    Then user see warning kos name "Nama ini telah digunakan kos lain."

  @addKostMaxChar @ownerExp @iOS
  Scenario: Add new kos with more than maximal characters in some entity on screen data kos
    Given User clicks on Add Property button in bottom
    And user click on add New Kos
    And user clicks on start button beside Kos Data
    When user input name kos with "Kost sana sani sini sana sani sini sana sani sini sana sani sini sana sani sini sana sani sini sana 1"
    And user clicks checkbox room type
    And user input room type with "Tipe abcdefghijklmnopqrstu abcdefghijklmnopqrstu abcdefghijklmnopqrstu abcdefghijklmnopqrstu 12345678"
    Then user see warning kos name "Tidak boleh lebih dari 100 karakter."
    And user see warning room type "Tidak boleh lebih dari 100 karakter."
    When user input name kos with "Kost Sana"
    And user input room type with "Tipe A"
    And user click kos name field
    And user select room type with "female"
    And user input kos description with "Kos yang dijamin bagus, karena dilengkapi dengan fasilitas yang lengkap"
    And user select kos rule "Laundry"
    And user clicks checkbox administrator kos
    And user input administrator name with "Jajang sunandar slamet riyadhii"
    And user input mobile phone number with "089123456789012345678"
    Then user see warning administrator name "Tidak boleh lebih dari 30 karakter."
    And user see warning mobile phone number administrator "Tidak boleh lebih dari 20 karakter."

  @addKostPriceFieldValidation
  Scenario: Text box Harga Sewa" is inputed with invalid value
    Given owner user navigates to manage page
    And User clicks on Add Kost button in bottom
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation TestAddKos", Kost Address "majapahit", Kost Owner Name "Pak Oke", Kost Manager Name "Mr.Bondan", and Kost Manager phone number "085757575757"
    And user select boarder type "Putri"
    When user input monthly price "100"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below monthly price field
    When user input monthly price "1000"
    And user input daily price "100"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below daily price field
    When user input daily price "1000"
    And user input weekly price "999"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below weekly price field
    When user input weekly price "1000"
    And user input minimum 3 monthly price "99"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below 3 monthly price field
    When user input minimum 3 monthly price "1000"
    And user input minimum 6 monthly price "99"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below 6 monthly price field
    When user input minimum 6 monthly price "1000"
    And user input yearly price "99"
    And user click next button in form data
    Then user see error message "Harga Terlalu Rendah" below yearly price field

  @monthlyPriceMandatory
  Scenario: Text box monthly price rent is mandatory
    Given owner user navigates to manage page
    And User clicks on Add Kost button in bottom
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation TestAddKos", Kost Address "majapahit", Kost Owner Name "Pak Oke", Kost Manager Name "Mr.Bon1(,r)", and Kost Manager phone number "085757575757"
    And user select boarder type "Campur"
    When user input monthly price ""
    And user click next button in form data
    Then user see error message "Harga Bulanan tidak Boleh kosong" below monthly price field
    When user input monthly price "300000"
    And user input monthly price ""
    And user click next button in form data
    Then user see error message "Harga Bulanan tidak Boleh kosong" below monthly price field

  @roomFacMandatory
  Scenario: Room facility is mandatory
    Given owner user navigates to manage page
    And User clicks on Add Kost button in bottom
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation TestAddKos", Kost Address "majapahit", Kost Owner Name "Pak Oke", Kost Manager Name "Mr.Bon1(,r)", and Kost Manager phone number "085757575757"
    And user select boarder type "Campur"
    When user input monthly price "500000"
    And user click next button in form data
    And user pick room facility :
      | Kasur |
    Then user see "Kasur" is selected in bathroom facility
    When user pick facility again :
      | Kasur |
    Then user see "Kasur" is not selected in bathroom facility
    When user select room space "3 X 4"
    And user input total rooms "5"
    And user input total available rooms "3"
    And user click next button in form data
    Then system display toast message "Minimal Satu Fasilitas Kamar Dipilih"

  @bathroomFacMandatory
  Scenario: Bathroom facility is mandatory
    Given owner user navigates to manage page
    And User clicks on Add Kost button in bottom
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation TestAddKos", Kost Address "majapahit", Kost Owner Name "Pak Oke", Kost Manager Name "Mr.Bon1(,r)", and Kost Manager phone number "085757575757"
    And user select boarder type "Campur"
    When user input monthly price "500000"
    And user click next button in form data
    And user pick room facility :
      | Kasur |
    And user pick bathroom facility :
      | Bathup |
    Then user see "Bathup" is selected in bathroom facility
    When user pick facility again :
      | Bathup |
    Then user see "Bathup" is not selected in bathroom facility
    When user select room space "3 X 4"
    And user input total rooms "5"
    And user input total available rooms "3"
    And user click next button in form data
    Then system display toast message "Minimal Satu Fasilitas Kamar Mandi Dipilih"

  @roomNoInvalidAddKos
  Scenario: Room number is inputed with invalid value
    Given owner user navigates to ads page
    And User clicks on Add Kost button in bottom
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation TestAddKos", Kost Address "majapahit", Kost Owner Name "Pak Oke", Kost Manager Name "Mr.Bon1(,r)", and Kost Manager phone number "085757575757"
    And user select boarder type "Campur"
    When user input monthly price "500000"
    And user click next button in form data
    And user click next button in form data
    Then system display toast message "Masukkan Jumlah Kamar Kost"
    And user see error message "Masukkan Jumlah Kamar Kost" below room field
    When user input total rooms "501"
    And user click next button in form data
    Then user see error message "Jumlah kamar tidak bisa lebih dari 500." below room field
    When user input total rooms "0"
    And user click next button in form data
    Then system display toast message "Jumlah Kamar Tidak Valid"
    And user see error message "Jumlah Kamar Tidak Valid" below room field
    When user input total rooms "500"
    Then user see error message "Jika Anda memiliki lebih dari 50 kamar, silakan lengkapi data lebih dahulu. Admin akan menghubungi Anda untuk verifikasi." below room field

  @privacyPolicyRequired
  Scenario: Checkbox privasi policy is mandatory
    Given owner user navigates to ads page
    And User clicks on Add Kost button in bottom
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation oke"
    And user input Kost Address "Yogyakarta"
    And user click next button in form data
    And user select boarder type "Campur"
    When user input monthly price "500000"
    And user click next button in form data
    And user pick room facility :
      | Kasur          |
    And user pick bathroom facility :
      | Bathup         |
    And user select room space "3 X 4"
    And user select year built "1986"
    And user input total rooms "5"
    And user input total available rooms "3"
    And user click next button in form data
    Then system display toast message "Anda harus menyetujui kost anda boleh di review"

  @availableRoomInvalidAddKos
  Scenario: Available room is inputed with invalid value
    Given owner user navigates to ads page
    And User clicks on Add Kost button in bottom
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation TestAddKos", Kost Address "majapahit", Kost Owner Name "Pak Oke", Kost Manager Name "Mr.Bon1(,r)", and Kost Manager phone number "085757575757"
    And user select boarder type "Campur"
    When user input monthly price "500000"
    And user click next button in form data
    And user pick room facility :
      | Kasur          |
    And user pick bathroom facility :
      | Bathup         |
    And user select room space "3 X 4"
    And user input total rooms "5"
    And user click next button in form data
    Then system display toast message "Masukkan Jumlah Kamar Kost Tersedia"
    When user input total available rooms "6"
    And user click on privacy policy checkbox in add kos form
    And user click next button in form data
    Then system display toast message "Jumlah Kamar tersedia TIDAK BISA LEBIH BESAR dari jumlah kamar yang ada"
    When user input total available rooms "5"
    And user click next button in form data
    And user navigate back to previous page
    When user input total available rooms "0"
    And user click next button in form data
    Then user is in add photos page in add kos form