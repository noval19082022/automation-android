@addkos
Feature: Add Kos from Home

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "nonkostditolak"
    And user Close Update Property Banner when show on pop up
    And user clicks on Close Instant Booking Tooltip when show on pop up

  @addKosFromHome
  Scenario: Add New Kos from Home when property more than 0 and not approved yet by admin
    Given user click add kos in Home
    And user click on New Kos
    And user click on Add as New Kos
    And user input Kost Name "Automation 12., /", Kost Address "add 554.,() '", Kost Owner Name "Pak Oke", Kost Manager Name "", and Kost Manager phone number ""
    And user select boarder type "Putra", input daily price "100000", weekly price "700000", and monthly price "2800000"
    And user input 3 month min price "8000000", 6 month min price "16000000", and yearly price "30000000"
    And user select minimum payment "Minimal 3 Bln", input additional other cost ""
    And user insert inputer name "", inputer email "", inputer phone "", and click next
    And user pick room facility :
      | Kasur          |
      | TV             |
      | Lemari pakaian |
    And user pick bathroom facility :
      | Bathup |
      | Kloset jongkok   |
      | Ember mandi      |
    And user select room space "3 X 4"
    And user select year built "1986"
    And user input total rooms "1", total available rooms "0", notes "no pet allowed", kos description "", and click next
    When user add cover photo, building photo, room picture, bathroom picture, other facility photo, kos description "ruang tamu", and then click save
    Then user see page title is "Aktivasi Booking Langsung" after add kos
    And user click activate instant booking
    And user see pop up page content title is "Permintaan Aktivasi Dikirimkan" and description "Data berhasil dikirim dan akan diverifikasi admin dalam 2x24 jam. Mohon menunggu, ya."
    And user click next in pop up page
    And user see newly created kos is in most top of the list, with correct name, address "add 554.,() '", status "Diperiksa Admin" and type "Kos Putra"
