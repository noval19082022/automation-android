@regression @updatePrice @ownerExp
Feature: Update Price

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user tap update price in Manage section

  @updatePriceValidation
  Scenario: Text box pricelist is inputed with invalid value
    Given user select Kos "Mamites Kos coba baru" in update room page
    When user click check box in price per month
    Then system display toast message "Harga Per Bulan wajib diisi"
    When user click see other prices
    And user input price "" in update price monthly
    Then user see error message "Harga tidak boleh kosong." below price field in update price page
    When user input price "49999" in update price monthly
    Then user see error message "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000." below price field in update price page
    When user input price "100000001" in update price monthly
    Then user see error message "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000." below price field in update price page
    When user input price "100000000" in update price monthly
    And user click check box in "daily" price
    And user input price "9999" in update price "daily"
    Then user see error message "Harga per hari min. Rp10.000 dan maks. Rp10.000.000." below price field in update price page
    When user input price "10000001" in update price "daily"
    Then user see error message "Harga per hari min. Rp10.000 dan maks. Rp10.000.000." below price field in update price page
    When user input price "" in update price "daily"
    Then user see error message "Harga tidak boleh kosong." below price field in update price page
    When user input price "10000" in update price "daily"
    And user click check box in "weekly" price
    And user input price "49999" in update price "weekly"
    Then user see error message "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000." below price field in update price page
    When user input price "" in update price "weekly"
    Then user see error message "Harga tidak boleh kosong." below price field in update price page
    When user input price "50000001" in update price "weekly"
    Then user see error message "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000." below price field in update price page
    When user input price "100000" in update price "weekly"
    And user click check box in "3 monthly" price
    And user input price "99999" in update price "3 monthly"
    Then user see error message "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000." below price field in update price page
    When user input price "" in update price "3 monthly"
    Then user see error message "Harga tidak boleh kosong." below price field in update price page
    When user input price "100000001" in update price "3 monthly"
    Then user see error message "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000." below price field in update price page
    When user input price "100000" in update price "3 monthly"
    And user click check box in "6 monthly" price
    And user input price "99999" in update price "6 monthly"
    Then user see error message "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000." below price field in update price page
    When user input price "" in update price "6 monthly"
    Then user see error message "Harga tidak boleh kosong." below price field in update price page
    When user input price "100000001" in update price "6 monthly"
    Then user see error message "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000." below price field in update price page
    When user input price "100000" in update price "6 monthly"
    And user click check box in "yearly" price
    And user input price "99999" in update price "yearly"
    Then user see error message "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000." below price field in update price page
    When user input price "" in update price "yearly"
    Then user see error message "Harga tidak boleh kosong." below price field in update price page
    When user input price "100000001" in update price "yearly"
    Then user see error message "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000." below price field in update price page

  @updatePriceSuccess
  Scenario: Successfully update price
    Given user select Kos "Kose Mamiset Automation" in update room page
    When user click update price button
    Then system display toast message "Harga berhasil diupdate"
    When user click see other prices
    And user input price "3000000" in update price monthly
    Then user didn't see error message below price field in update price page
    When user input price "150000" in update price "daily"
    Then user didn't see error message below price field in update price page
    When user input price "500000" in update price "weekly"
    Then user didn't see error message below price field in update price page
    When user input price "9000000" in update price "3 monthly"
    Then user didn't see error message below price field in update price page
    When user input price "18000000" in update price "6 monthly"
    Then user didn't see error message below price field in update price page
    When user input price "36000000" in update price "yearly"
    Then user didn't see error message below price field in update price page
    When user click update price button
    Then system display toast message "Harga berhasil diupdate"
