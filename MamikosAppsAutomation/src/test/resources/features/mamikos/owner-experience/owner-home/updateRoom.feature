@regression @updateRoom @ownerExp
Feature: Update Room

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And user tap update room in Manage section

  @updateRommAllotment
  Scenario: Update room allotment in update room
    Given user select Kos "Mamites Kos coba baru" in update room page
    And user update room total to "3"
    And user search room number "2"
    When user insert text "a234567890123456789@123 5678901234567890123456789A" then click done in room name or number field
    And user insert text "5678901234567890123456789A a234567890123456789@123" then click done in floor (optional) field
    And user click done button in room availability page
    And user navigate back to previous page
    And user select Kos "Mamites Kos coba baru" in update room page
    And user search room number "a234567890123456789@123 5678901234567890123456789A"
    Then user see text "a234567890123456789@123 5678901234567890123456789A" in room name or number field
    And user see text "5678901234567890123456789A a234567890123456789@123" in floor (optional) field
    And user insert text "2" then click done in room name or number field
    And user insert text "1" then click done in floor (optional) field
    And user click done button in room availability page

  @roomNoInvalid
  Scenario: Text box "Number/Room Name?" is inputed with invalid value
    Given user select Kos "Mamites Kos coba baru" in update room page
    When user insert text " " then click done in room name or number field
    Then user see error message "Nomor/ nama masih kosong." in room allotment page
    When user insert text "2" then click done in room name or number field
    Then user see error message "Nomor/ nama sudah dipakai kamar lain." in room allotment page
    When user insert text "jyxoqeajmfxmrjzxpivfvbojunhmtsayxplkaiviaxitqgxkkseidtwchuqf" then click done in room name or number field
    Then user see error message "Maks. 50 karakter." in room allotment page

  @floorInvalid
  Scenario: Text box "Floor (Optional)" is inputed with invalid value
    Given user select Kos "Mamites Kos coba baru" in update room page
    When user insert text "jyxoqeajmfxmrjzxpivfvbojunhmtsayxplkaiviaxitqgxkkseidtwchuqf" then click done in floor (optional) field
    Then user see error message "Maks. 50 karakter." in floor field room allotment page
    When user click done button in room availability page
    Then system display toast message "Maks. 50 karakter."

  @addDeleteRoomAllotment
  Scenario: Add and delete room allotment in update room
    Given user select Kos "Kose Full Automation" in update room page
    And user tap on 3 dots button
    When user tap on Add Room button
    And user insert text "99" on 2 room in room name or number field
    And user hide keyboard and scroll down
    And user insert text "one" on 2 floor in floor (optional) field
    And user click done button in room availability page
    And user navigate back to previous page
    And user select Kos "Kose Full Automation" in update room page
    Then user see text "99" in room name or number field
    And user see text "one" in floor (optional) field
    # Delete room
    When user tap on 3 dots button
    And user tap on Delete Room button
    And user click done button in room availability page
    And user navigate back to previous page
    And user select Kos "Kose Full Automation" in update room page
    Then user see text "1" in room name or number field
    And user see text "1" in floor (optional) field