@regression @editProfileOwner
Feature: Edit Profile

  @nonActiveMamipay @user-growth
  Scenario: Check user information on edit profile
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "nonactivemamipay"
    And owner user navigates to profile page
    When user click on invitation email banner
    And owner click on edit profile
    Then user verify data profile information
      | tiara             |
      | 0812345670001     |
      | rheza@mamikos.com |

  @activeMamipay @user-growth
  Scenario: Check user information on edit profile - Bank Account
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "activemamipay"
    And owner user navigates to profile page
    When user click on invitation email banner
    And user click to Payment Menu
    Then user verify bank account data
      | BNI (Bank Negara Indonesia) & BNI Syariah |
      | 978687675765                              |
      | tiara tiga                                |

  @ownerExp @termandservice
  Scenario: Check terms and service in owner profile
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And owner user navigates to profile page
    When user click on terms and service
    Then user redirect to "Syarat dan Ketentuan" page

  @ownerExp @privacypolicy
  Scenario: Check privacy policy in owner profile
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And owner user navigates to profile page
    When user click on privacy policy
    Then user redirect to "Kebijakan Privasi" page

  @ownerExp @notifOption
  Scenario Outline: Check notif option
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "master"
    And owner user navigates to profile page
    When user click on settings
    And user click notification dropdown
    And user click "<Notification Option>" checkbox in settings page
    Then system display toast message "<Message>"
    And user click "<Notification Option>" checkbox in settings page
    Then system display toast message "<Message>"
    Examples:
      | Notification Option    | Message                     |
      | Rekomendasi via email  | Berhasil Update Setting App |
      | Notifikasi via chat    | Berhasil Update Setting App |
      | Notifikasi kos via SMS | Berhasil Update Setting App |