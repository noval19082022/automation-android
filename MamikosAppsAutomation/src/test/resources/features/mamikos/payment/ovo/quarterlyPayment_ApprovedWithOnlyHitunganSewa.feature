@regression @payment @quarterlyPayment
Feature: Quarterly rent payment - Approved by only calculating rent

  Scenario: Delete booking if tenant have booking active
    Given user navigate to mamipay, search contract "payment" and click on terminate contract
#    When user navigate to web mamikos, login via facebook as "tenantPayment", and cancel booking

  Scenario: Booking successfully by tenant for quarterly period
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user logs in as Tenant via Facebook credentails "tenantPayment"
    And user search for kos with name "payment" and selects matching result and navigate to detail kos
    And user clicks on Booking button on Kost details page
    And user input boarding start date is "today" and clicks on booking button
    And user select payment period "Per 3 Bulan" and click on continue button
    And user click next button
    And user selects T&C checkbox and clicks on Book button
    Then system display message booking successfully

  Scenario: Owner accept quarterly rent
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user logs in as Owner "payment"
    And user accept booking form squad "payment" with booking duration "3 bulan"
    And user select room number "Kamar 2 Lantai 1" and clicks on next button
    And user click save tenant data
    Then system display empty booking data with status need confirmation

  Scenario: Tenant pay boarding house for quarterly rent
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user logs in as Tenant via Facebook credentails "tenantPayment"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "OVO" for "payment" case "payment3B1"
    Then system display repayment successfully and back to booking history page

  Scenario: Tenant check-in for quarterly rent
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "tenantPayment"
    And user navigates to profile page
    When user navigates to Booking History page
    And user check in from booking history page which rental period is "6 Bulan"
    Then system display check in successfully

#  Scenario: Tenant terminate contract
    When system display page kos saya
    When user click terminate contract "6 Bulan"
    And user select reason terminate contract "Ingin Pindah"
    And user select date out from boarding house
    And user give review kos
    And user give a rating 3 for "Kebersihan"
    And user give a rating 3 for "Kenyamanan"
    And user give a rating 3 for "Keamanan"
    And user give a rating 3 for "Harga"
    And user give a rating 3 for "Fasilitas Kamar"
    And user give a rating 3 for "Fasilitas Umum"
    And user input "Bagus" on field kost review and click terminate contract button
    Then system display toast message "Berhasil Mengajukan ke Pemilik"

  Scenario: Owner confirmation contract terminate
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user logs in as Owner "payment"
    And user as owner click first notification with message "mengajukan pemberhentian sewa"
    And user confirm terminate kos
    Then system display terminate contract success "Kontrak sewa telah berakhir"