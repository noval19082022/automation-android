@regression @payment @iOS
  Feature: Home Owner

    Scenario: Page Tagihan Penyewa
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user logs in as Owner "paymentiOS"
      Then system display tagihan penyewa tab
      And system display belum bayar button
      And system display di mamikos button
      And system display ditransfer button
      And system display lihat tagihan penyewa button

#    Scenario: Page Booking Langsung
      When system display booking langsung tab
      Then system display lihat booking button
      And system display permintaan sewa button
      And system display kelola booking langsung button

#    Scenario: check tab Belum Bayar
      When user click belum bayar button
      Then system display belum bayar tab

#    Scenario: check tab di mamikos
      When user click back button
      And user click di mamikos button
      Then system display di mamikos tab

#    Scenario: check tab ditransfer
      When user click back button
      And user click ditransfer button
      Then system display ditransfer tab

#    Scenario: check tab lihat tagihan penyewa
#      When user click back on ios
#      And user click lihat tagihan penyewa button
      Then system display belum bayar tab

#    Scenario: User change tagihan kost name
      When user click tagihan kost name dropdown
      And user choose "Pondok Kencana Indah squad payment" list kost
      Then system display detail tagihan kost "Pondok Kencana Indah squad payment"

#    Scenario: User change hitungan sewa
      When user choose "Per Hari" hitungan sewa
      Then system display "Per Minggu" hitungan sewa
