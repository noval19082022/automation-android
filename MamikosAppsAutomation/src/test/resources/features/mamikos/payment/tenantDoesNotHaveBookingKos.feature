@regression @payment2

Feature: Tenant does not have booking

  Scenario: Tenant does not have booking
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Tenant via Facebook credentails "TenantDoesNotHaveKos"
    When user navigates to Explore page
    When user navigates to profile page
    And user click contract menu
    Then system display page no boarding house was rented