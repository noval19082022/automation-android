Feature: Balance Allocation

  @ownerEditBalanceAllocation
  Scenario: Owner Edit Balance Allocation
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "premium"
    And user click on balance per day popup
    And user clicks atur premium and navigates to premium page
    When user clicks on view apartment ads
    And user clicks on manage ads button
    And user clicks on manage balance allocation
    When user fill allocation amount
    And user clicks on confirmation
    Then user verify balance allocation with "Rp20.000"