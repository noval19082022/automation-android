@regression @premium @tingkatkanKlik

Feature: Tingkatkan klik

  Background:
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "premium"
    And user is on main page and click on Later option when asked on pop up
    And user click on balance per day popup

  @ApartTingkatkanKlik
  Scenario: Growth click apartemen - display the time range statistic
    When owner user navigates to ads page
    And user click on Apartment Tab
    And user clicks on statistic time range
    Then will display statistic by time
    | Hari ini      |
    | Kemarin       |
    | 7 Hari Lalu   |
    | 14 Hari Lalu  |
    | 30 Hari Lalu  |
    | Semua         |
  And user sees the number of click statistic

  Scenario: Manage Ads Apartemen - display status top ads
    When owner user navigates to ads page
    And user click on Apartment Tab
    And user clicks on growth click button
    Then user will see status top ads and balance

  Scenario: Manage Ads Apartemen - Add balance
    When owner user navigates to ads page
    And user click on Apartment Tab
    And user clicks on growth click button
    And user clicks on manage balance allocation button
    And user set balance
    Then user will see the allocation balance is equals with "10.000"

  @KostTingkatkanKlik
  Scenario: Growth click kost - display the time range statistic
    When owner user navigates to ads page
    And user clicks on expand kost in kost ads page
    And user clicks on kost statistic time range
    Then will display statistic by time
      | Hari ini          |
      | Kemarin           |
      | 7 hari terakhir   |
      | 14 hari terakhir  |
      | 30 hari terakhir  |
      | Semua waktu       |
    And user sees the number of click, chat, favorite, and review statistic

  Scenario: Manage Ads kost - display status top ads
    When owner user navigates to ads page
    And user clicks on expand kost in kost ads page
    And user clicks on set premium button
    Then user will see status top ads and balance

  Scenario: Manage Ads Kost - Add balance
    When owner user navigates to ads page
    And user clicks on expand kost in kost ads page
    And user clicks on set premium button
    And user clicks on manage balance allocation button
    And user set balance
    Then user will see the allocation balance is equals with "10.000"

  @ownerEditedBalance
  Scenario: Owner edited balance allocation
    When user clicks on manage premium from homescreen
    And user clicks on preview ads kos from Premium page
    And user clicks on manage ads from Premium ads page
    And user clicks on manage balance allocation button
    And user set balance
    Then user will see the allocation balance is equals with "10.000"

  @activateDailyBalance
  Scenario: Activate Daily Balance from homescreen
    When user switch activate daily balance premium
    And user clicks on manage premium from homescreen
    Then user check activate daily balance premium has actived from premium page
    And user click on Nonactivate daily balance premium
