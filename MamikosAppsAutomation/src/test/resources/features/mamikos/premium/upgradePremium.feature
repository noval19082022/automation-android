@regression @upgradepremium

Feature: Upgrade Premium Account
  @cancelbuypackage
  Scenario: Cancel buy package
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "premium"
    And user is on main page and click on Later option when asked on pop up
    Then user clicks atur premium and navigates to premium page
    And user click on FTUE cek properti
    And user click on back icon
    And user click on lihat paket button
    And user click on onboarding mengapa premium
    Then user choose package "Paket GP 4"
    Then user verify name of package is "Paket GP 4"
    Then user verify Bayar button is appear
    Then user click back
    
  Scenario: Success choose package with Bank BNI
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "premiumpayment"
    And user close pop up premium free
    And user is on main page and click on Later option when asked on pop up
    And user clicks lihat paket
    Then user close premium faq
    Then user choose package 12 Bulan Premium
    And user clicks pilih paket
    Then user choose Bank BNI
    And user clicks pilih metode
    Then user verify on the page Bank BNI in Aktivasi Premium page

  Scenario: Confirm Payment without input data
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "premiumsaldo"
    And user is on main page and click on Later option when asked on pop up
    And user clicks on beli saldo
    And user clicks on konfirmasi pembayaran
    And user clicks on submit button
    Then user verify on Nama Pemilik Rekening
    And user fills on Nama Pemilik Rekening "Miyamoto"
    And user clicks on submit button
    Then user verify on Nama Bank Pengirim


  Scenario: Success change saldo package
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    And user logs in as Owner "premiumsaldo"
    And user is on main page and click on Later option when asked on pop up
    And user clicks on beli saldo
    And user clicks on ganti paket
    And user choose "25.000"
    Then user verify "25000"