@regression @ownerForgotPassword @user-growth @iOS

  Feature: Owner - Forgot Password
    @sendOTPViaSMSOwner
    Scenario: Forgot Password - Send OTP via SMS
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via SMS
      Then user redirect to "Verifikasi Akun Pemilik Kos" OTP Screen

    @sendOTPViaWhatsAppOwner
    Scenario: Forgot Password - Send OTP via WhatsApp
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via WA
      Then user redirect to "Verifikasi Akun Pemilik Kos" OTP Screen

    @resendOTPViaSMSOwner
    Scenario: Forgot Password - Resend OTP via SMS
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via SMS
      Then user verify resend OTP button and click resend OTP button

    @resendOTPViaWhatsAppOwner
    Scenario: Forgot Password - Resend OTP via WhatsApp
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via WA
      Then user verify resend OTP button and click resend OTP button

    @invalidOTPSMSOwner
    Scenario: Forgot Password - Invalid OTP SMS
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via SMS
      And user input OTP with invalid value "1" "1" "1" "1"
      Then user verify OTP error messages "Kode tidak valid, harap periksa kembali."

    @invalidOTPWAOwner
    Scenario: Forgot Password - Invalid OTP WhatsApp
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via WA
      And user input OTP with invalid value "1" "1" "1" "1"
      Then user verify OTP error messages "Kode tidak valid, harap periksa kembali."

    @backFromOTPPageViaSMSOwner
    Scenario: Forgot Password - Back to Choose Verification Method from SMS OTP Page
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via SMS
      And user click back button on OTP page
      And user click "Ya, Batalkan" from confirmation dialog
      Then user redirect to choose verification method screen
      | Pilih metode verifikasi |
      | Pilih Metode Verifikasi |

    @backFromOTPPageViaWAOwner
    Scenario: Forgot Password - Back to Choose Verification Method from WhatsApp OTP Page
      Given Server configuration is selected
      And user is on main page and click on Later option when asked on pop up
      When user clicks on Login option on Menu
      And user click on forgot password button from login page
      And user fill out forgot password as "Owner Forgot Password"
      When user click on send verification code via WA
      And user click back button on OTP page
      And user click "Ya, Batalkan" from confirmation dialog
      Then user redirect to choose verification method screen
      | Pilih metode verifikasi |
      | Pilih Metode Verifikasi |