@tenantForgotPassword @user-growth

Feature: Tenant - Forgot Password
  @sendOTPViaSMSTenant
  Scenario: Forgot Password - Send OTP via SMS
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via SMS
    Then user redirect to "Verifikasi Akun Pencari Kos" OTP Screen

  @sendOTPViaWhatsAppTenant
  Scenario: Forgot Password - Send OTP via WhatsApp
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via WA
    Then user redirect to "Verifikasi Akun Pencari Kos" OTP Screen

  @resendOTPViaSMSTenant
  Scenario: Forgot Password - Resend OTP via SMS
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via SMS
    Then user verify resend OTP button and click resend OTP button

  @resendOTPViaWhatsAppTenant
  Scenario: Forgot Password - Resend OTP via WhatsApp
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via WA
    Then user verify resend OTP button and click resend OTP button

  @invalidOTPSMSTenant
  Scenario: Forgot Password - Invalid OTP SMS
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via SMS
    And user input OTP with invalid value "1" "1" "1" "1"
    Then user verify OTP error messages "Kode tidak valid, harap periksa kembali."

  @invalidOTPWATenant
  Scenario: Forgot Password - Invalid OTP WhatsApp
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via WA
    And user input OTP with invalid value "1" "1" "1" "1"
    Then user verify OTP error messages "Kode tidak valid, harap periksa kembali."

  @backFromOTPPageViaSMSTenant
  Scenario: Forgot Password - Back to Choose Verification Method from SMS OTP Page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via SMS
    And user click back button on OTP page
    And user click "Ya, Batalkan" from confirmation dialog
    Then user redirect to "Pilih metode verifikasi" OTP Screen

  @backFromOTPPageViaWATenant
  Scenario: Forgot Password - Back to Choose Verification Method from WhatsApp OTP Page
    Given Server configuration is selected
    And user is on main page and click on Later option when asked on pop up
    When user clicks on Login option on Menu
    And user click on tenant forgot password button from login page
    And user fill out forgot password as "Tenant Forgot Password"
    When user click on send verification code via WA
    And user click back button on OTP page
    And user click "Ya, Batalkan" from confirmation dialog
    Then user redirect to "Pilih metode verifikasi" OTP Screen